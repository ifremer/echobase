.. -
.. * #%L
.. * EchoBase
.. * %%
.. * Copyright (C) 2011 Ifremer, Codelutin
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU Affero General Public License as published by
.. * the Free Software Foundation, either version 3 of the License, or
.. * (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Public License for more details.
.. * 
.. * You should have received a copy of the GNU Affero General Public License
.. * along with this program.  If not, see <http://www.gnu.org/licenses/>.
.. * #L%
.. -

========
Echobase
========

Création d'une application embarquée
------------------------------------

L'application permet de créer une application autonome comprenant un jeu de
données sélectionné par l'utilisateur.

Procédure de création
=====================

- Aller dans le menu *Créer une application embarquée*
- Sélectionner la ou les campagnes à inclure (ou rien si vous voulez juste le référentiel)
- Changer si besoin est le nom de l'archive à créer
- Appuyer sur le bouton Créer l'application.

Une fois l'archive créée (l'opération peut être longue selon la quantité de
données), elle est téléchargeable depuis une page de résultat de l'opération
(normalement l'archive se met toute seule en téléchargement).

Utilisation d'une application embarquée
=======================================

L'application embarquée est en fait une version *mode autonome* d'EchoBase
avec en plus un jeu de données.

Consultez la `page d'utilisation`_ pour les détails d'utilisation et
configuration d'EchoBase.

.. _page d'utilisation: ./usage.html#utiliser-la-version-mode-portable
