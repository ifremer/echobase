.. -
.. * #%L
.. * EchoBase
.. * %%
.. * Copyright (C) 2011 - 2012 Ifremer, Codelutin
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU Affero General Public License as published by
.. * the Free Software Foundation, either version 3 of the License, or
.. * (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Public License for more details.
.. * 
.. * You should have received a copy of the GNU Affero General Public License
.. * along with this program.  If not, see <http://www.gnu.org/licenses/>.
.. * #L%
.. -

====================
Exporter des données
====================

Abstract
========

Ce document explique comment exporter les données depuis EchoBase.

Création de requêtes
--------------------

Il est possible d'exporter les données via des requêtes sql. Pour ce faire aller
sur le menu **exporter les données**.

On peut ensuite créer (en enregistrer) des requêtes sql. Le résultat s'affiche
alors dans un tableau qui est exportable au format csv.

A noter aussi qu'il est possible d'utiliser des requêtes imbriquées en insérant
dans le code sql une référence au nom de la requête.

Voici un exemple :

::

  SELECT * FROM Voyage where topiaid IN ${NomRequête}

Import de requête depuis Libre Office
-------------------------------------

Il est possible d'importer une requête construite depuis Libre Office.
Pour ce faire il faut utiliser l'action Importer depuis Libre Office et y coller
le code SQL généré dans Libre Office.

Le code SQL sera processé et mis à jour dans votre requête SQL EchoBase.

Pour utiliser postgresql avec Libre Office, il suffit d'intaller le paquet **libreoffice-sdbc-postgresql**, 
puis de choisir comme type de base de donnée **postgresql** pour connecter une base existante.

Export de données vers le site des indicateurs du Système d'Information Halieutique
-----------------------------------------------------------------------------------
Le bouton "Exporter vers Coser" permet d'exporter les séries d'indicateurs de l'état des populations et communautés et les cartes de distribution moyennes stockées dans EchoBase pour chaque mission.

Les indicateurs sont exportés au format Coser pour intégration dans le `site des indicateurs du Système d'Information Halieutique`_ .   

Export de données vers la base acoustique halieutique du Conseil International pour l'Exploration de la Mer
-----------------------------------------------------------------------------------------------------------
Le bouton "Exporter vers ICES" permet d'exporter les données d'échotypage et de pêche d'une campagne au format d'échange .xml du Conseil International pour l'Exploration de la Mer (CIEM). 

Ces données peuvent ensuite être importées directement dans la `base acoustique halieutique du CIEM`_. 


.. _site des indicateurs du Système d'Information Halieutique: http://www.ifremer.fr/SIH-indices-campagnes/
.. _base acoustique halieutique du CIEM: http://acoustic.ices.dk/

