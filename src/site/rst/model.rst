.. -
.. * #%L
.. * EchoBase
.. * %%
.. * Copyright (C) 2011 Ifremer, Codelutin
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU Affero General Public License as published by
.. * the Free Software Foundation, either version 3 of the License, or
.. * (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Public License for more details.
.. * 
.. * You should have received a copy of the GNU Affero General Public License
.. * along with this program.  If not, see <http://www.gnu.org/licenses/>.
.. * #L%
.. -

===============
Modèle Echobase
===============

Présentation
------------

On donne ici des explications sur le modèle.

Téléchargement
==============

- `Télécharger le modèle au format argoUML`_.

- `Télécharger le modèle au format freeMind`_.

Modèle du référentiel
=====================

- `Vue générale du référentiel`_
- `Référentiel AcousticInstrument`_
- `Référentiel AreaOfOperation`_
- `Référentiel DataMetadata`_
- `Référentiel DepthStratum`_
- `Référentiel Echotype`_
- `Référentiel Gear`_
- `Référentiel Mission`_
- `Référentiel ReferenceDatum`_
- `Référentiel TSParameter`_
- `Référentiel Vessel`_
- `Référentiel Port`_

Modèle thématique
=================

Partie commune
~~~~~~~~~~~~~~

- `Partie commune des données thématiques`_
- `Partie commune des données bouées`_


Sous modèle acoustique
~~~~~~~~~~~~~~~~~~~~~~

- `Sous modèle acoustique`_

Sous modèle pêcherie
~~~~~~~~~~~~~~~~~~~~

- `Sous modèle pêcherie`_

.. _Télécharger le modèle au format argoUML: https://gitlab.nuiton.org/codelutin/echobase/raw/develop/echobase-domain/src/main/xmi/echobase.zargo
.. _Télécharger le modèle au format freeMind: https://gitlab.nuiton.org/codelutin/echobase/raw/develop/src/doc/model/EchoBase_structure_V5.mm

.. _Vue générale du référentiel: model/referenceAll.png
.. _Référentiel AcousticInstrument: model/referenceAcousticInstrument.png
.. _Référentiel AreaOfOperation: model/referenceAreaOfOperation.png
.. _Référentiel DataMetadata: model/referenceDataMetadata.png
.. _Référentiel DepthStratum: model/referenceDepthStratum.png
.. _Référentiel Echotype: model/referenceEchotype.png
.. _Référentiel Gear: model/referenceGear.png
.. _Référentiel Mission: model/referenceMission.png
.. _Référentiel ReferenceDatum: model/referenceReferenceDatum.png
.. _Référentiel TSParameter: model/referenceTSParameter.png
.. _Référentiel Vessel: model/referenceVessel.png
.. _Référentiel Port: model/referencePort.png

.. _Partie commune des données thématiques: model/dataCommon.png
.. _Partie commune des données bouées: model/dataMooring.png
.. _Sous modèle acoustique: model/dataAcoustic.png
.. _Sous modèle pêcherie: model/dataCatches.png
