.. -
.. * #%L
.. * EchoBase
.. * %%
.. * Copyright (C) 2011 Ifremer, Codelutin
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU Affero General Public License as published by
.. * the Free Software Foundation, either version 3 of the License, or
.. * (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Public License for more details.
.. * 
.. * You should have received a copy of the GNU Affero General Public License
.. * along with this program.  If not, see <http://www.gnu.org/licenses/>.
.. * #L%
.. -

========
Echobase
========

Présentation
------------

L'Institut Français pour l'Exploration de la Mer (Ifremer) fournit une suite de logiciels libres pour stocker des données 
collectées lors de campagnes halieutiques intégrées ou par des observatoires fixes (mouillages), et calculer des indicateurs sur la base de ces données. L'objectif est de contribuer à la
mise en place de systèmes de surveillance des écosystèmes marins alimentés par des observations scientifiques à la mer. 
La suite logicielle comprend une base de données postgreSQL dédiée au stockage des métadonnées, données acoustiques et de pêche issues
de campagnes écosystémiques ou de mouillages (Echobase) et de codes R (EchoR) permettant de produire et d'analyser des indicateurs et des cartes renseignant sur l'état des populations d'organismes marins, à partir des données stockées dans Echobase.
Ce site décrit la base de données Echobase. 

Les caractéristiques d'Echobase incluent : 

- une interface basée accessible via un navigateur internet pour importer, éditer et extraire des données ;  

- une grande capacité de stockage et un système de gestion de bases multiples ;

- une intégration facile avec des Systèmes d'Information Géographiques et ou des sites internet dynamiques ;

- une structure de métadonnées acoustiques en accord avec le format défini par le `groupe de travail CIEM WGFAST`_.

Des codes permettant d'exporter et importer des données vers/de EchoBase sont disponibles dans le `package R EchoR`_.

.. _package R EchoR: https://gitlab.ifremer.fr/md0276b/echor
.. _groupe de travail CIEM WGFAST: http://www.acoustics.washington.edu/FAST

