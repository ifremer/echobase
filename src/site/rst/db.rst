.. -
.. * #%L
.. * EchoBase
.. * %%
.. * Copyright (C) 2011 - 2012 Ifremer, Codelutin
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU Affero General Public License as published by
.. * the Free Software Foundation, either version 3 of the License, or
.. * (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Public License for more details.
.. * 
.. * You should have received a copy of the GNU Affero General Public License
.. * along with this program.  If not, see <http://www.gnu.org/licenses/>.
.. * #L%
.. -

==========================
Echobase - Base de données
==========================

Les bases de données dans EchoBase
----------------------------------

*EchoBase* permet l'utilisation de plusieurs bases de données que nous appelons
*bases de travail*.

Il est nécessaire de se connecter à une telle base de travail avant tout
import/consultation/... de données dans l'application.

Il est possible de gérer les paramètres de connexion à une base de travail.

On distinque deux types de base :

- les bases de données de type *H2*
- les bases de données de type *Postgresql*

Utilisation d'une base H2
-------------------------

L'avantage de ce type base est de pouvoir les créer facilement sans aucune
maintenance (la base est stockée dans un simple fichier). Par contre elles sont
moins performantes et perenne que les bases de type *Postgresql*.

Pour utiliser une telle base, il suffit de créer une configuration de type *H2*
en spécifiant une url sous ce format :

::

  jdbc:h2:file:/cheminVersLaBase/nomDeLaBase

Vous pouvez ensuite utiliser cette base qui va être crée automatiquement à
l'endroit spécifié.

Il est recommandé de fournir un mot de passe bien que cela ne soit pas
obligatoire (mais cela peut être requis par certains outils externes).

Par convention, nous utilisons les identifiants suivants :

::

  login: sa
  password: sa

Utilisation d'une base Postgresql
---------------------------------

C'est ce genre de base que l'on doit utiliser sur un serveur pour conserver de
manière perenne les données.

Pour utiliser une telle base il faut configurer le serveur postgres comme
indiqué dans la section suivante.

Une fois la base créée sur le serveur, il suffit de créer une configuration de
base de travail dans *EchoBase* de type *Postgresql* avec une url sous ce format :

::

  jdbc:postgresql://serverAddres/dbName

Les identifiants vous sont fournit par l'administrateur qui a installé la base
postgresql sur le serveur.

Création d'une base postgresql
------------------------------

Pré-requis :

 - **postgres** doit être installé sur la machine (voir `page d'installation`_)

 - être connecté avec l'utilisateur (postgres par défaut) qui a les droits
   d'administrer cette base de donnée dans une console shell.

::

  su postgres

- Création de l'utilisateur de la base *userName* (avec des droits de création
  de base)

::

  createuser -U postgres -sdRP userName

- Création de la base de donnée *dbName*

Depuis la version *2.4*, vous pouvez directement créer les bases depuis
l'interface web :

Après avoir créé une configuration de base de travail postgresql, une action
**Créer la base** est disponible (pour les utilisateurs *administrateur*).

A noter que l'utilisateur pour se connecter à la base doit avoir des droits de
création de base.

Spatialiser une base postgresql
-------------------------------

Pour rendre la base spatiale, connectez-vous via l'application et cliquer sur
le bouton **Spatialiser la base**.

*Et voila!*.

A noter que cette action est aussi disponible sur la page de visualisation des
données spatiales.

.. _page d'installation: ./install.html#Installation_de_la_base_de_donnes

