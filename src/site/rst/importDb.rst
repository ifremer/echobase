.. -
.. * #%L
.. * EchoBase
.. * %%
.. * Copyright (C) 2011 - 2012 Ifremer, Codelutin
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU Affero General Public License as published by
.. * the Free Software Foundation, either version 3 of the License, or
.. * (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Public License for more details.
.. * 
.. * You should have received a copy of the GNU Affero General Public License
.. * along with this program.  If not, see <http://www.gnu.org/licenses/>.
.. * #L%
.. -

=================
Import d'une base
=================

Abstract
--------

Ce document explique comment importer des bases au format EchoBase.

Import Referential
------------------

Ce mode permet d'importer uniquement des données du référentiel.

Il suffit de renseigner le fichier *.echobase* à importer et de lancer l'import.

Import Free
-----------

Ce mode permet d'importer n'importe quelle type de données (référentiel ou/et
données thématiques).

Il suffit de renseigner le fichier *.echobase* à importer et de lancer l'import.

