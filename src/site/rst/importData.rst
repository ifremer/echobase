.. -
.. * #%L
.. * EchoBase
.. * %%
.. * Copyright (C) 2011 Ifremer, Codelutin
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU Affero General Public License as published by
.. * the Free Software Foundation, either version 3 of the License, or
.. * (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Public License for more details.
.. * 
.. * You should have received a copy of the GNU Affero General Public License
.. * along with this program.  If not, see <http://www.gnu.org/licenses/>.
.. * #L%
.. -

==================================
Importer des données dans EchoBase
==================================

.. contents::

Abstract
========

- Ce document donne la définition de tous les imports de données.

Format des colonnes
~~~~~~~~~~~~~~~~~~~

+---------------+---------------------------------------------------------------+
| Notation      | Type                                                          |
+===============+===============================================================+
| text          |une chaine de caractères                                       |
+---------------+---------------------------------------------------------------+
| FK(XXX#yyy)   |désigne une clef étrangère requise (XXX sur la propriété yyy)  |
+---------------+---------------------------------------------------------------+
| (int)         |un entier (pas du nullité possible)                            |
+---------------+---------------------------------------------------------------+
| (float)       |un décimal (pas de nullité possible)                           |
+---------------+---------------------------------------------------------------+
| (Integer)     |un entier avec nullité possible                                |
+---------------+---------------------------------------------------------------+
| (Integer-NA)  |un entier avec nullité possible (et nullité si NA)             |
+---------------+---------------------------------------------------------------+
| Float         |un décimale avec nullité possible                              |
+---------------+---------------------------------------------------------------+
| (Float-NA)    |un décimale avec nullité possible (et nullité si NA)           |
+---------------+---------------------------------------------------------------+
| (date)        |unique format de date : *yyyy-MM-dd HH:mm:ss.SSSS*             |
+---------------+---------------------------------------------------------------+
| (esdu cell)   |nom de cellule esdu : <date>_<num>[_(S|B)]                     |
|               | avec date au format yyyy-MM-dd HH:mm:ss.SSSS                  |
|               | avec num pour le numéro de cellule                            |
|               | avec S (surface) ou B (bottom) pour les cellules élémentaires |
+---------------+---------------------------------------------------------------+

Notes importantes
~~~~~~~~~~~~~~~~~

- Le nom des colonnes est sensible à la casse, il faut donc bien respecter les
  noms donnés dans cette page.

- Le contenu des cellules (et aussi les nom de colonnes) peuvent être encapsulés
  par des **"**.
  
Vérification des données importées
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

- A la fin de l'import, les données importées dans EchoBase sont vérifiées, afin que les données importées soient identiques aux données du fichier texte fourni en entrée. 
- Si les données dans le fichier texte et dans la base ne sont pas identiques, un message d'erreur est affiché. Ceci peut provenir de problèmes de format ou de consistence des données d'entrée. 
- Afin de détecter ces problèmes, les fichiers texte d'entrée pour chaque import et le fichier "check" correspondant au contenu dans EchoBase sont disponibles dans le dossier "/var/local/echobase/data" du serveur où EchoBase est installé. Fichier d'import = "nomFichierImport-processed.csv" ; Fichier check = "nomFichierImport-checked.csv"


Voyage / Transit / Transect
===========================

A noter que depuis la version 2.2, il est possible de faire les imports
séparement.

Voyage
~~~~~~

Colonnes requises
-----------------

::

  name;startDate;endDate;startPort;endPort

Format des colonnes
-------------------

+------------+----------------+
| Colonne    | Type           |
+============+================+
| name       |text            |
+------------+----------------+
| startDate  |(date)          |
+------------+----------------+
| endDate    |(date)          |
+------------+----------------+
| startPort  |FK(Port#code)   |
+------------+----------------+
| endPort    |FK(Port#code)   |
+------------+----------------+

Les Port#codes sont référencés sur le `site Seadatanet`_. 


Exemple
-------

::

  name;startDate;endDate;startPort;endPort
  PELGAS2011;2011-04-26 08:00:00.0000;2011-06-04 17:00:01.0000;BSH189;BSH4265

Transit
~~~~~~~

Colonnes requises
-----------------

::

  voyage;description;startTime;endTime;startLocality;endLocality

Format des colonnes
-------------------

+----------------+----------------+
| Colonne        | Type           |
+================+================+
| voyage         |FK(voyage#name) |
+----------------+----------------+
| description    |text            |
+----------------+----------------+
| startTime      |(date)          |
+----------------+----------------+
| endTime        |(date)          |
+----------------+----------------+
| startLocality  |text            |
+----------------+----------------+
| endLocality    |text            |
+----------------+----------------+

Exemple
-------

::

  voyage;description;startTime;endTime;startLocality;endLocality
  PELGAS2011;PELGAS11CAMP1;2011-04-26 08:00:01.0000;2011-05-08 20:00:00.0000;Santander;La Rochelle

Transect
~~~~~~~~

Colonnes requises
-----------------

::

  title;transectAbstract;stratum;comments;voyage;vesselName;dateCreated;timeCoverageStart;timeCoverageEnd;geospatialLonMin;geospatialLatMin;geospatialVerticalMin;geospatialLonMax;geospatialLatMax;geospatialVerticalMax;linestring

Format des colonnes
-------------------

+------------------------+----------------+
| Colonne                | Type           |
+========================+================+
| voyage                 |FK(Voyage#name) |
+------------------------+----------------+
| vesselName             |FK(Vessel#name) |
+------------------------+----------------+
| title                  |text            |
+------------------------+----------------+
| transectAbstract       |text            |
+------------------------+----------------+
| stratum                |text            |
+------------------------+----------------+
| comments               |text            |
+------------------------+----------------+
| dateCreated            |(date)          |
+------------------------+----------------+
| timeCoverageStart      |(date)          |
+------------------------+----------------+
| timeCoverageEnd        |(date)          |
+------------------------+----------------+
| geospatialLonMin       |(float)         |
+------------------------+----------------+
| geospatialLonMax       |(float)         |
+------------------------+----------------+
| geospatialLatMin       |(float)         |
+------------------------+----------------+
| geospatialLatMax       |(float)         |
+------------------------+----------------+
| geospatialVerticalMin  |(float)         | 
+------------------------+----------------+
| geospatialVerticalMax  |(float)         |
+------------------------+----------------+
| linestring             |text            |
+------------------------+----------------+

Exemple
-------

::

  title;transectAbstract;stratum;comments;voyage;vesselName;dateCreated;timeCoverageStart;timeCoverageEnd;geospatialLonMin;geospatialLatMin;geospatialVerticalMin;geospatialLonMax;geospatialLatMax;geospatialVerticalMax;linestring
  Arlequin2 / la colombine942OBS01 SUR;942OBS;SUR;942OBS,P5001,SUR;PELGAS2011;Arlequin2 / la colombine;2011-04-27 09:10:00.0000;2011-04-27 09:10:00.0000;2011-04-27 10:39:00.0000;-1.5896666667;43.7055;110;-1.541;43.7606666667;110;-176453.0000037 4851310.5,-171051 4857434.0000037

Ancillary instrumentation
~~~~~~~~~~~~~~~~~~~~~~~~~

Colonnes requises
-----------------

::

  voyage;vessel;ancillaryInstrumentation

Format des colonnes
-------------------

+-----------------------------+----------------------------------+
| Colonne                     | Type                             |
+=============================+==================================+
| voyage                      |FK(Voyage#name)                   |
+-----------------------------+----------------------------------+
| vessel                      |FK(Vessel#name)                   |
+-----------------------------+----------------------------------+
| ancillaryInstrumentation    |FK(AncillaryInstrumentation#name) |
+-----------------------------+----------------------------------+

Exemple
-------

::

  voyage;vessel;ancillaryInstrumentation
  PELGAS2011;Arlequin2 / la colombine;Inst1

Operation / OperationMetadataValue / GearMetadataValue
======================================================

Operation
~~~~~~~~~

Colonnes requises
-----------------

::

  vesselName;operationId;depthStratumId;gearShootingStartTime;midHaulLatitude;midHaulLongitude;gearShootingStartLatitude;gearShootingStartLongitude;gearShootingEndTime;gearHaulingEndLatitude;gearHaulingEndLongitude;gearCode

Format des colonnes
-------------------

+-----------------------------+------------------------+
| Colonne                     | Type                   |
+=============================+========================+
| vesselName                  |FK(Vessel#name)         |
+-----------------------------+------------------------+
| gearCode                    |FK(Gear#casinoGearName) |
+-----------------------------+------------------------+
| depthStratumId              |FK(DepthStratum#id)     |
+-----------------------------+------------------------+
| operationId                 |text                    |
+-----------------------------+------------------------+
| midHaulLatitude             |(float)                 |
+-----------------------------+------------------------+
| midHaulLongitude            |(float)                 |
+-----------------------------+------------------------+
| gearShootingStartTime       |(date)                  |
+-----------------------------+------------------------+
| gearShootingEndTime         |(date)                  |
+-----------------------------+------------------------+
| gearShootingStartLatitude   |(float)                 |
+-----------------------------+------------------------+
| gearHaulingEndLatitude      |(float)                 |
+-----------------------------+------------------------+
| gearShootingStartLongitude  |(float)                 |
+-----------------------------+------------------------+
| gearHaulingEndLongitude     |(float)                 |
+-----------------------------+------------------------+

Exemple
-------

::

  vesselName;operationId;depthStratumId;gearShootingStartTime;midHaulLatitude;midHaulLongitude;gearShootingStartLatitude;gearShootingStartLongitude;gearShootingEndTime;gearHaulingEndLatitude;gearHaulingEndLongitude;gearCode
  THALASSA II;P0422;CLAS;2011-05-05 06:49:50.0000;45.4165605;-1.4492292;45.4029733;-1.4860902;2011-05-05 07:42:19.0000;45.4301477;-1.4123682;57x52

OperationMetadataValue
~~~~~~~~~~~~~~~~~~~~~~

Colonnes requises
-----------------

::

  vesselName;operationId;metadataType;operationMetadataValue

Format des colonnes
-------------------

+-------------------------+---------------------------+
| Colonne                 | Type                      |
+=========================+===========================+
| vesselName              |FK(Vessel#name)            |
+-------------------------+---------------------------+
| operationId             |FK(Operation#id)           |
+-------------------------+---------------------------+
| metadataType            |FK(OperationMetadata#name) |
+-------------------------+---------------------------+
| operationMetadataValue  |text                       |
+-------------------------+---------------------------+

Exemple
-------

::

  vesselName;operationId;metadataType;operationMetadataValue
  THALASSA II;P0422;MeanWaterDepth;43.42

GearMetadataValue
~~~~~~~~~~~~~~~~~

Colonnes requises
-----------------

::

  vesselName;operationId;gearCode;metadataType;gearMetadataValue

Format des colonnes
-------------------

+---------------------+------------------------+
| Colonne             | Type                   |
+=====================+========================+
| vesselName          |FK(Vessel#name)         |
+---------------------+------------------------+
| operationId         |FK(Operation#id)        |
+---------------------+------------------------+
| metadataType        |FK(GearMetadata#name)   |
+---------------------+------------------------+
| gearCode            |FK(Gear#casinoGearName) |
+---------------------+------------------------+
| gearMetadataValue   |text                    |
+---------------------+------------------------+

Exemple
-------

::

  vesselName;operationId;gearCode;metadataType;gearMetadataValue
  THALASSA II;P0422;57x52;CableLength;31.6666666666667

GearCharacteristicValues
~~~~~~~~~~~~~~~~~~~~~~~~

Colonnes requises
-----------------

::

  gear;characteristic;dataValue

Format des colonnes
-------------------

+-------------------+------------------------------+
| Colonne           | Type                         |
+===================+==============================+
+-------------------+------------------------------+
| gear              |FK(Gear#casinoGearName)       |
+-------------------+------------------------------+
| characteristic    |FK(GearCharacteristic#name)   |
+-------------------+------------------------------+
| dataValue         |text                          |
+-------------------+------------------------------+

Exemple
-------

::

  gear;characteristic;dataValue
  casinoName;characteristicName;33

TotalSample / SubSample / BiometrySample
========================================

TotalSample
~~~~~~~~~~~

Colonnes requises
-----------------

::

  operationId;baracoudaCode;sizeCategory;sampleWeight;numberSampled;meanLength;meanWeight;noPerKg;sortedWeight

Format des colonnes
-------------------

+-----------------+--------------------------+
| Colonne         | Type                     |
+=================+==========================+
| operationId     |FK(Operation#id)          |
+-----------------+--------------------------+
| baracoudaCode   |FK(Species#baracoudaCode) |
+-----------------+--------------------------+
| sizeCategory    |FK(SizeCategory#name)     |
+-----------------+--------------------------+
| sampleWeight    |(float)                   |
+-----------------+--------------------------+
| numberSampled   |(Integer-NA)              |
+-----------------+--------------------------+
| meanLength      |(Float-NA)                |
+-----------------+--------------------------+
| meanWeight      |(Float-NA)                |
+-----------------+--------------------------+
| noPerKg         |(Float-NA)                |
+-----------------+--------------------------+
| sortedWeight    |(float)                   |
+-----------------+--------------------------+

Exemple
-------

::

  operationId;baracoudaCode;sizeCategory;sampleWeight;numberSampled;meanLength;meanWeight;noPerKg;sortedWeight
  P0435;ALLO-TEZ;0;0.21;36;6.4;6;166.66;0.03

SubSample
~~~~~~~~~

Colonnes requises
-----------------

::

  operationId;baracoudaCode;sizeCategory;sexCategory;sampleWeight;numberSampled;lengthClass;numberAtLength;weightAtLength;round

Format des colonnes
-------------------

+------------------+--------------------------+
| Colonne          | Type                     |
+==================+==========================+
| operationId      |FK(Operation#id)          |
+------------------+--------------------------+
| baracoudaCode    |FK(Species#baracoudaCode) |
+------------------+--------------------------+
| sizeCategory     |FK(SizeCategory#name)     |
+------------------+--------------------------+
| sexCategory      |FK(SexCategory#name)      |
+------------------+--------------------------+
| sampleWeight     |(float)                   |
+------------------+--------------------------+
| numberSampled    |(int)                     |
+------------------+--------------------------+
| numberAtLength   |(float)                   |
+------------------+--------------------------+
| weightAtLength   |(Float-NA)                |
+------------------+--------------------------+
| lengthClass      |text                      |
+------------------+--------------------------+

(colonnes ignorées) subHaul round

Exemple
-------

::

  operationId;baracoudaCode;sizeCategory;sexCategory;sampleWeight;numberSampled;lengthClass;numberAtLength;weightAtLength;units;round
  P0372;ENGR-ENC;0;N;3.37;198;12;7;0.08;0;5

BiometrySample
~~~~~~~~~~~~~~

Colonnes requises
-----------------

::

  operationId;baracoudaCode;numFish;dataLabel;dataValue;name;sizeCategory

Format des colonnes
-------------------

+----------------+--------------------------+
| Colonne        | Type                     |
+================+==========================+
| operationId    |FK(Operation#id)          |
+----------------+--------------------------+
| baracoudaCode  |FK(Species#baracoudaCode) |
+----------------+--------------------------+
| numFish        |(int)                     |
+----------------+--------------------------+
| name           |FK(SampleDataType#name)   |
+----------------+--------------------------+
| dataLabel      |text                      |
+----------------+--------------------------+
| dataValue      |(Float-NA)                |
+----------------+--------------------------+
| sizeCategory   |FK(SizeCategory#name)     |
+----------------+--------------------------+

Exemple
-------

::

  operationId;baracoudaCode;numFish;dataLabel;dataValue;name;sizeCategory
  P5002;ENGR-ENC;9394;NA;-1;Age;1

Acoustic (voyage ou bouée)
==========================

Acoustic
~~~~~~~~

Colonnes requises
-----------------

::

  MOVIES_EILayer;MOVIES_EILayer\sndset;MOVIES_EILayer\sndset\sndname;MOVIES_EILayer\sndset\sndident;MOVIES_EILayer\sndset\softChannelId;MOVIES_EILayer\sndset\channelName;MOVIES_EILayer\sndset\dataType;MOVIES_EILayer\sndset\beamType;MOVIES_EILayer\sndset\acousticFrequency;MOVIES_EILayer\sndset\startSample;MOVIES_EILayer\sndset\mainBeamAlongSteeringAngle;MOVIES_EILayer\sndset\mainBeamAthwartSteeringAngle;MOVIES_EILayer\sndset\absorptionCoef;MOVIES_EILayer\sndset\transmissionPower;MOVIES_EILayer\sndset\beamAlongAngleSensitivity;MOVIES_EILayer\sndset\beamAthwartAngleSensitivity;MOVIES_EILayer\sndset\beam3dBWidthAlong;MOVIES_EILayer\sndset\beam3dBWidthAthwart;MOVIES_EILayer\sndset\beamEquTwoWayAngle;MOVIES_EILayer\sndset\beamGain;MOVIES_EILayer\sndset\beamSACorrection;MOVIES_EILayer\sndset\bottomDetectionMinDepth;MOVIES_EILayer\sndset\bottomDetectionMaxDepth;MOVIES_EILayer\sndset\bottomDetectionMinLevel;MOVIES_EILayer\sndset\AlongTXRXWeightId;MOVIES_EILayer\sndset\AthwartTXRXWeightId;MOVIES_EILayer\sndset\SplitBeamAlongTXRXWeightId;MOVIES_EILayer\sndset\SplitBeamAthwartTXRXWeightId;MOVIES_EILayer\sndset\bandWidth;MOVIES_EILayer\sndset\tvgminrange;MOVIES_EILayer\sndset\tvgmaxrange;MOVIES_EILayer\sndset\pulseduration;MOVIES_EILayer\shipnav;MOVIES_EILayer\shipnav\lat;MOVIES_EILayer\shipnav\long;MOVIES_EILayer\shipnav\alt;MOVIES_EILayer\shipnav\gndspeed;MOVIES_EILayer\shipnav\gndcourse;MOVIES_EILayer\shipnav\surfspeed;MOVIES_EILayer\shipnav\surfcourse;MOVIES_EILayer\shipnav\driftspeed;MOVIES_EILayer\shipnav\driftcourse;MOVIES_EILayer\shipnav\heading;MOVIES_EILayer\shipnav\roll;MOVIES_EILayer\shipnav\pitch;MOVIES_EILayer\shipnav\heave;MOVIES_EILayer\shipnav\depth;MOVIES_EILayer\shipnav\draught;MOVIES_EILayer\cellset;MOVIES_EILayer\cellset\cellnum;MOVIES_EILayer\cellset\celltype;MOVIES_EILayer\cellset\depthstart;MOVIES_EILayer\cellset\depthend;MOVIES_EILayer\cellset\indexstart;MOVIES_EILayer\cellset\indexend;MOVIES_EILayer\cellset\datestart;MOVIES_EILayer\cellset\dateend;MOVIES_EILayer\cellset\lat;MOVIES_EILayer\cellset\long;MOVIES_EILayer\cellset\volume;MOVIES_EILayer\cellset\area;MOVIES_EILayer\cellset\diststart;MOVIES_EILayer\cellset\distend;MOVIES_EILayer\cellset\thresholdup;MOVIES_EILayer\cellset\thresholdlow;MOVIES_EILayer\eilayer;MOVIES_EILayer\eilayer\sa;MOVIES_EILayer\eilayer\sv;MOVIES_EILayer\eilayer\ni;MOVIES_EILayer\eilayer\nt;MOVIES_EILayer\boterr;MOVIES_EILayer\boterr\sa;MOVIES_EILayer\boterr\ni;MOVIES_EILayer\sndset\soundcelerity;dataQuality;label

Format des colonnes
-------------------

+---------------------------------------------------------+------------------------------------------+
| Colonne                                                 | Type                                     |
+=========================================================+==========================================+
| MOVIES_EILayer                                          |text                                  //A |
+---------------------------------------------------------+------------------------------------------+
| MOVIES_EILayer\\sndset                                  |(colonne ignorée)                     //B |
+---------------------------------------------------------+------------------------------------------+
| MOVIES_EILayer\\sndset\\sndname                         |(colonne ignorée)                     //C |
+---------------------------------------------------------+------------------------------------------+
| MOVIES_EILayer\\sndset\\sndident                        |(colonne ignorée)                     //D |
+---------------------------------------------------------+------------------------------------------+
| MOVIES_EILayer\\sndset\\softChannelId                   |FK(AcousticInstrument#id)             //E |
+---------------------------------------------------------+------------------------------------------+
| MOVIES_EILayer\\sndset\\channelName                     |(colonne ignorée)                     //F |
+---------------------------------------------------------+------------------------------------------+
| MOVIES_EILayer\\sndset\\dataType                        |(colonne ignorée)                     //G |
+---------------------------------------------------------+------------------------------------------+
| MOVIES_EILayer\\sndset\\beamType                        |(colonne ignorée)                     //H |
+---------------------------------------------------------+------------------------------------------+
| MOVIES_EILayer\\sndset\\acousticFrequency               |(colonne ignorée)                     //I |
+---------------------------------------------------------+------------------------------------------+
| MOVIES_EILayer\\sndset\\startSample                     |(colonne ignorée)                     //J |
+---------------------------------------------------------+------------------------------------------+
| MOVIES_EILayer\\sndset\\mainBeamAlongSteeringAngle      |(colonne ignorée)                     //K |
+---------------------------------------------------------+------------------------------------------+
| MOVIES_EILayer\\sndset\\mainBeamAthwartSteeringAngle    |(colonne ignorée)                     //L |
+---------------------------------------------------------+------------------------------------------+
| MOVIES_EILayer\\sndset\\absorptionCoef                  |(Float-NA)                            //M |
+---------------------------------------------------------+------------------------------------------+
| MOVIES_EILayer\\sndset\\transmissionPower               |(int)                                 //N |
+---------------------------------------------------------+------------------------------------------+
| MOVIES_EILayer\\sndset\\beamAlongAngleSensitivity       |(Float)                               //O |
+---------------------------------------------------------+------------------------------------------+
| MOVIES_EILayer\\sndset\\beamAthwartAngleSensitivity     |(Float)                               //P |
+---------------------------------------------------------+------------------------------------------+
| MOVIES_EILayer\\sndset\\beam3dBWidthAlong               |(colonne ignorée)                     //Q |
+---------------------------------------------------------+------------------------------------------+
| MOVIES_EILayer\\sndset\\beam3dBWidthAthwart             |(colonne ignorée)                     //R |
+---------------------------------------------------------+------------------------------------------+
| MOVIES_EILayer\\sndset\\beamEquTwoWayAngle              |(Float)                               //S |
+---------------------------------------------------------+------------------------------------------+
| MOVIES_EILayer\\sndset\\beamGain                        |(Float)                               //T |
+---------------------------------------------------------+------------------------------------------+
| MOVIES_EILayer\\sndset\\beamSACorrection                |(Float)                               //U |
+---------------------------------------------------------+------------------------------------------+
| MOVIES_EILayer\\sndset\\bottomDetectionMinDepth         |(colonne ignorée)                     //V |
+---------------------------------------------------------+------------------------------------------+
| MOVIES_EILayer\\sndset\\bottomDetectionMaxDepth         |(colonne ignorée)                     //W |
+---------------------------------------------------------+------------------------------------------+
| MOVIES_EILayer\\sndset\\bottomDetectionMinLevel         |(colonne ignorée)                     //X |
+---------------------------------------------------------+------------------------------------------+
| MOVIES_EILayer\\sndset\\AlongTXRXWeightId               |(colonne ignorée)                     //Y |
+---------------------------------------------------------+------------------------------------------+
| MOVIES_EILayer\\sndset\\AthwartTXRXWeightId             |(colonne ignorée)                     //Z |
+---------------------------------------------------------+------------------------------------------+
| MOVIES_EILayer\\sndset\\SplitBeamAlongTXRXWeightId      |(colonne ignorée)                    //AA |
+---------------------------------------------------------+------------------------------------------+
| MOVIES_EILayer\\sndset\\SplitBeamAthwartTXRXWeightId    |(colonne ignorée)                    //AB |
+---------------------------------------------------------+------------------------------------------+
| MOVIES_EILayer\\sndset\\bandWidth                       |(colonne ignorée)                    //AC |
+---------------------------------------------------------+------------------------------------------+
| MOVIES_EILayer\\sndset\\tvgminrange                     |(colonne ignorée)                    //AD |
+---------------------------------------------------------+------------------------------------------+
| MOVIES_EILayer\\sndset\\tvgmaxrange                     |(colonne ignorée)                    //AE |
+---------------------------------------------------------+------------------------------------------+
| MOVIES_EILayer\\sndset\\pulseduration                   |(Float)                              //AF |
+---------------------------------------------------------+------------------------------------------+
| MOVIES_EILayer\\shipnav                                 |(colonne ignorée)                    //AG |
+---------------------------------------------------------+------------------------------------------+
| MOVIES_EILayer\\shipnav\\lat                            |(colonne ignorée)                    //AH |
+---------------------------------------------------------+------------------------------------------+
| MOVIES_EILayer\\shipnav\\long                           |(colonne ignorée)                    //AI |
+---------------------------------------------------------+------------------------------------------+
| MOVIES_EILayer\\shipnav\\alt                            |(colonne ignorée)                    //AJ |
+---------------------------------------------------------+------------------------------------------+
| MOVIES_EILayer\\shipnav\\gndspeed                       |(colonne ignorée)                    //AK |
+---------------------------------------------------------+------------------------------------------+
| MOVIES_EILayer\\shipnav\\gndcourse                      |(colonne ignorée)                    //AL |
+---------------------------------------------------------+------------------------------------------+
| MOVIES_EILayer\\shipnav\\surfspeed                      |(colonne ignorée)                    //AM |
+---------------------------------------------------------+------------------------------------------+
| MOVIES_EILayer\\shipnav\\surfcourse                     |(colonne ignorée)                    //AN |
+---------------------------------------------------------+------------------------------------------+
| MOVIES_EILayer\\shipnav\\driftspeed                     |(colonne ignorée)                    //AO |
+---------------------------------------------------------+------------------------------------------+
| MOVIES_EILayer\\shipnav\\driftcourse                    |(colonne ignorée)                    //AP |
+---------------------------------------------------------+------------------------------------------+
| MOVIES_EILayer\\shipnav\\heading                        |(colonne ignorée)                    //AQ |
+---------------------------------------------------------+------------------------------------------+
| MOVIES_EILayer\\shipnav\\roll                           |(colonne ignorée)                    //AR |
+---------------------------------------------------------+------------------------------------------+
| MOVIES_EILayer\\shipnav\\pitch                          |(colonne ignorée)                    //AS |
+---------------------------------------------------------+------------------------------------------+
| MOVIES_EILayer\\shipnav\\heave                          |(colonne ignorée)                    //AT |
+---------------------------------------------------------+------------------------------------------+
| MOVIES_EILayer\\shipnav\\depth                          |text                                 //AU |
+---------------------------------------------------------+------------------------------------------+
| MOVIES_EILayer\\shipnav\\draught                        |(colonne ignorée)                    //AV |
+---------------------------------------------------------+------------------------------------------+
| MOVIES_EILayer\\cellset                                 |(colonne ignorée)                    //AW |
+---------------------------------------------------------+------------------------------------------+
| MOVIES_EILayer\\cellset\\cellnum                        |(int)                                //AX |
+---------------------------------------------------------+------------------------------------------+
| MOVIES_EILayer\\cellset\\celltype                       |(int)                                //AY |
+---------------------------------------------------------+------------------------------------------+
| MOVIES_EILayer\\cellset\\depthstart                     |(float)                              //AZ |
+---------------------------------------------------------+------------------------------------------+
| MOVIES_EILayer\\cellset\\depthend                       |(float)                              //BA |
+---------------------------------------------------------+------------------------------------------+
| MOVIES_EILayer\\cellset\\indexstart                     |(colonne ignorée)                    //BB |
+---------------------------------------------------------+------------------------------------------+
| MOVIES_EILayer\\cellset\\indexend                       |(colonne ignorée)                    //BC |
+---------------------------------------------------------+------------------------------------------+
| MOVIES_EILayer\\cellset\\datestart                      |(date)                               //BD |
+---------------------------------------------------------+------------------------------------------+
| MOVIES_EILayer\\cellset\\dateend                        |(date)                               //BE |
+---------------------------------------------------------+------------------------------------------+
| MOVIES_EILayer\\cellset\\lat                            |(Float)                              //BF |
+---------------------------------------------------------+------------------------------------------+
| MOVIES_EILayer\\cellset\\long                           |(Float)                              //BG |
+---------------------------------------------------------+------------------------------------------+
| MOVIES_EILayer\\cellset\\volume                         |(Integer-NA)                         //BH |
+---------------------------------------------------------+------------------------------------------+
| MOVIES_EILayer\\cellset\\area                           |(int)                                //BI |
+---------------------------------------------------------+------------------------------------------+
| MOVIES_EILayer\\cellset\\diststart                      |(colonne ignorée)                    //BJ |
+---------------------------------------------------------+------------------------------------------+
| MOVIES_EILayer\\cellset\\distend                        |(colonne ignorée)                    //BK |
+---------------------------------------------------------+------------------------------------------+
| MOVIES_EILayer\\cellset\\thresholdup                    |(int)                                //BL |
+---------------------------------------------------------+------------------------------------------+
| MOVIES_EILayer\\cellset\\thresholdlow                   |(int)                                //BM |
+---------------------------------------------------------+------------------------------------------+
| MOVIES_EILayer\\eilayer                                 |(colonne ignorée)                    //BN |
+---------------------------------------------------------+------------------------------------------+
| MOVIES_EILayer\\eilayer\\sa                             |(Float-NA)                           //BO |
+---------------------------------------------------------+------------------------------------------+
| MOVIES_EILayer\\eilayer\\sv                             |(colonne ignorée)                    //BP |
+---------------------------------------------------------+------------------------------------------+
| MOVIES_EILayer\\eilayer\\ni                             |(Integer-NA)                         //BQ |
+---------------------------------------------------------+------------------------------------------+
| MOVIES_EILayer\\eilayer\\nt                             |(Integer-NA)                         //BR |
+---------------------------------------------------------+------------------------------------------+
| MOVIES_EILayer\\boterr                                  |(colonne ignorée)                    //BR |
+---------------------------------------------------------+------------------------------------------+
| MOVIES_EILayer\\boterr\\sa                              |(colonne ignorée)                    //BT |
+---------------------------------------------------------+------------------------------------------+
| MOVIES_EILayer\\boterr\\ni                              |(colonne ignorée)                    //BU |
+---------------------------------------------------------+------------------------------------------+
| MOVIES_EILayer\\sndset\\soundcelerity                   |text                                 //BV |
+---------------------------------------------------------+------------------------------------------+
| dataQuality                                             |FK(DataQuality#qualityDataFlagValues) //E |
+---------------------------------------------------------+------------------------------------------+
| label                                                   |text                                 //BW |
+---------------------------------------------------------+------------------------------------------+

Notes importantes
-----------------

- Il faut trier les lignes sur la date (MOVIES_EILayer) et sur le numéro de cellule (MOVIES_EILayer\\cellset\\cellnum).

Exemple
-------

::

  MOVIES_EILayer;MOVIES_EILayer\sndset;MOVIES_EILayer\sndset\sndname;MOVIES_EILayer\sndset\sndident;MOVIES_EILayer\sndset\softChannelId;MOVIES_EILayer\sndset\channelName;MOVIES_EILayer\sndset\dataType;MOVIES_EILayer\sndset\beamType;MOVIES_EILayer\sndset\acousticFrequency;MOVIES_EILayer\sndset\startSample;MOVIES_EILayer\sndset\mainBeamAlongSteeringAngle;MOVIES_EILayer\sndset\mainBeamAthwartSteeringAngle;MOVIES_EILayer\sndset\absorptionCoef;MOVIES_EILayer\sndset\transmissionPower;MOVIES_EILayer\sndset\beamAlongAngleSensitivity;MOVIES_EILayer\sndset\beamAthwartAngleSensitivity;MOVIES_EILayer\sndset\beam3dBWidthAlong;MOVIES_EILayer\sndset\beam3dBWidthAthwart;MOVIES_EILayer\sndset\beamEquTwoWayAngle;MOVIES_EILayer\sndset\beamGain;MOVIES_EILayer\sndset\beamSACorrection;MOVIES_EILayer\sndset\bottomDetectionMinDepth;MOVIES_EILayer\sndset\bottomDetectionMaxDepth;MOVIES_EILayer\sndset\bottomDetectionMinLevel;MOVIES_EILayer\sndset\AlongTXRXWeightId;MOVIES_EILayer\sndset\AthwartTXRXWeightId;MOVIES_EILayer\sndset\SplitBeamAlongTXRXWeightId;MOVIES_EILayer\sndset\SplitBeamAthwartTXRXWeightId;MOVIES_EILayer\sndset\bandWidth;MOVIES_EILayer\sndset\tvgminrange;MOVIES_EILayer\sndset\tvgmaxrange;MOVIES_EILayer\sndset\pulseduration;MOVIES_EILayer\shipnav;MOVIES_EILayer\shipnav\lat;MOVIES_EILayer\shipnav\long;MOVIES_EILayer\shipnav\alt;MOVIES_EILayer\shipnav\gndspeed;MOVIES_EILayer\shipnav\gndcourse;MOVIES_EILayer\shipnav\surfspeed;MOVIES_EILayer\shipnav\surfcourse;MOVIES_EILayer\shipnav\driftspeed;MOVIES_EILayer\shipnav\driftcourse;MOVIES_EILayer\shipnav\heading;MOVIES_EILayer\shipnav\roll;MOVIES_EILayer\shipnav\pitch;MOVIES_EILayer\shipnav\heave;MOVIES_EILayer\shipnav\depth;MOVIES_EILayer\shipnav\draught;MOVIES_EILayer\cellset;MOVIES_EILayer\cellset\cellnum;MOVIES_EILayer\cellset\celltype;MOVIES_EILayer\cellset\depthstart;MOVIES_EILayer\cellset\depthend;MOVIES_EILayer\cellset\indexstart;MOVIES_EILayer\cellset\indexend;MOVIES_EILayer\cellset\datestart;MOVIES_EILayer\cellset\dateend;MOVIES_EILayer\cellset\lat;MOVIES_EILayer\cellset\long;MOVIES_EILayer\cellset\volume;MOVIES_EILayer\cellset\area;MOVIES_EILayer\cellset\diststart;MOVIES_EILayer\cellset\distend;MOVIES_EILayer\cellset\thresholdup;MOVIES_EILayer\cellset\thresholdlow;MOVIES_EILayer\eilayer;MOVIES_EILayer\eilayer\sa;MOVIES_EILayer\eilayer\sv;MOVIES_EILayer\eilayer\ni;MOVIES_EILayer\eilayer\nt;MOVIES_EILayer\boterr;MOVIES_EILayer\boterr\sa;MOVIES_EILayer\boterr\ni;MOVIES_EILayer\sndset\soundcelerity;dataQuality;label
  2011/06/23 10:09:00.0000;NA;NA;NA;47;38000;NA;NA;NA;NA;NA;NA;8.47;2000;21.8;21.8;NA;NA;-20.6;25.27;-0.529999;NA;NA;NA;NA;NA;NA;NA;NA;NA;NA;1.024;NA;NA;NA;NA;NA;NA;NA;NA;NA;NA;NA;NA;NA;NA;199.9;NA;NA;0;0;10;20;NA;NA;2011-04-26 08:24:02.0000;2011-04-26 08:30:02.0000;43.666505;-3.48812666666667;NA;1;NA;NA;0;-60;NA;1.444008;NA;71;43424;NA;NA;NA;variable;1;1

Calibration
~~~~~~~~~~~

Colonnes requises
-----------------

::

  topiaId;accuracyEstimate;aquisitionMethod;comments;date;processingMethod;report;acousticInstrument

Format des colonnes
-------------------

+------------------------+------------------------------------------+
| Colonne                | Type                                     |
+========================+==========================================+
| topiaId                |(colonne ignorée)                     //A |
+------------------------+------------------------------------------+
| accuracyEstimate       |text                                  //B |
+------------------------+------------------------------------------+
| aquisitionMethod       |text                                  //C |
+------------------------+------------------------------------------+
| comments               |text                                  //D |
+------------------------+------------------------------------------+
| date                   |(date)                                //E |
+------------------------+------------------------------------------+
| processingMethod       |text                                  //F |
+------------------------+------------------------------------------+
| report                 |text                                  //G |
+------------------------+------------------------------------------+
| acousticinstrument     |FK(AcousticInstrument#id)             //H |
+------------------------+------------------------------------------+

Notes importantes
-----------------

- Pour que la calibration soit prise en compte la date doit être comprise entre les dates de début et de fin de voyage.

Exemple
-------

::

  accuracyEstimate;aquisitionMethod;comments;date;processingMethod;report;acousticInstrument
  ;0.26;ER60;Mission PELGAS2015 - 30 avril 2015;2015-04-30 07:00:00;ER60;http://madida/madida/documents/etalonnage/TL_EK60_etalonnage_avril2015__v1.html;47


Résultats voyage
================

Echotype
~~~~~~~~

Colonnes requises
-----------------

::

  voyage;echotypeName;depthStratumId;meaning;baracoudaCode;id

Format des colonnes
-------------------

+-----------------+--------------------------------+
| Colonne         | Type                           |
+=================+================================+
| echotypeName    |text                            |
+-----------------+--------------------------------+
| meaning         |text                            |
+-----------------+--------------------------------+
| voyage          |FK(Voyage#name)                 |
+-----------------+--------------------------------+
| depthStratumId  |FK(DepthStratum#id)             |
+-----------------+--------------------------------+
| baracoudaCode   |FK(Species#baracoudaCode)       |
+-----------------+--------------------------------+
| id              |integer (species category code) |
+-----------------+--------------------------------+

Exemple
-------

::

  voyage;echotypeName;depthStratumId;meaning;baracoudaCode;id
  PELGAS2011;D1;CLAS;Maquereaux, Chinchards et gadidés présents dans les couches à proximité du fond;COMP-LEM;0

LengthWeightKey
~~~~~~~~~~~~~~~

Colonnes requises
-----------------

::

  voyage;sizeCategory;aParameter;bParameter;baracoudaCode;strata

Format des colonnes
-------------------

+----------------+--------------------------+
| Colonne        | Type                     |
+================+==========================+
| aParameter     |(float)                   |
+----------------+--------------------------+
| bParameter     |(float)                   |
+----------------+--------------------------+
| voyage         |FK(Voyage#name)           |
+----------------+--------------------------+
| sizeCategory   |FK(SizeCategory#name)     |
+----------------+--------------------------+
| baracoudaCode  |FK(Species#baracoudaCode) |
+----------------+--------------------------+
| strata         |FK(Strata#name)           |
+----------------+--------------------------+

Exemple
-------

::

  voyage;sizeCategory;aParameter;bParameter;baracoudaCode;strata
  PELGAS2011;0;2.48253468289872;3.37866337729714;ENGR-ENC;Golfe de Gascogne

LengthAgeKey
~~~~~~~~~~~~

Colonnes requises
-----------------

::

  voyage;baracoudaCode;age;length;percentAtAge;metadata;strata

Format des colonnes
-------------------

+----------------+--------------------------+
| Colonne        | Type                     |
+================+==========================+
| voyage         |FK(Voyage#name)           |
+----------------+--------------------------+
| age            |(int)                     |
+----------------+--------------------------+
| length         |(float)                   |
+----------------+--------------------------+
| percentAtAge   |(float)                   |
+----------------+--------------------------+
| metadata       |text                      |
+----------------+--------------------------+
| strata         |FK(Strata#name)           |
+----------------+--------------------------+
| baracoudaCode  |FK(Species#baracoudaCode) |
+----------------+--------------------------+

Exemple
-------

::

  voyage;baracoudaCode;age;length;percentAtAge;metadata;strata
  PELGAS2011;ENGR-ENC;1;7.5;1;Ifremer reading;Golfe de Gascogne

Résultats Esdu pour les voyages
===============================

Esdu / Echotype
~~~~~~~~~~~~~~~

Colonnes requises
-----------------

::

  voyage;name;echotype;NASC;ReferenceStationCatch;dataQuality

Format des colonnes
-------------------

+----------------+-------------------------------------------------------------+
| Colonne        | Type                                                        |
+================+=============================================================+
| voyage         |FK(Voyage#name)                                              |
+----------------+-------------------------------------------------------------+
| name           |FK(esdu cell)                                                |
+----------------+-------------------------------------------------------------+
| echotype       |FK(Echotype#name)                                            |
+----------------+-------------------------------------------------------------+
| dataQuality    |FK(DataQuality#qualityDataFlagValues)                        |
+----------------+-------------------------------------------------------------+
| méta-données   |FK(DataMetadata#name) (une colonne par méta à importer)      |
+----------------+-------------------------------------------------------------+

Exemple
-------

::

  voyage;name;echotype;NASC;ReferenceStationCatch;dataQuality
  PELGAS2011;2011-04-26 13:29:12.0000_16;D1;0;P0379;1

Esdu / Echotype / Species Category
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Colonnes requises
-----------------

::

  voyage;name;echotype;baracoudaCode;sizeCategory;ReferenceStationCatch;Biomass;MeanLength;MeanWeight;Abundance;SigmaSp;NASC;pondBiomass;pondAbundance;dataQuality

Format des colonnes
-------------------

+----------------+-------------------------------------------------------------+
| Colonne        | Type                                                        |
+================+=============================================================+
| voyage         |FK(Voyage#name)                                              |
+----------------+-------------------------------------------------------------+
| name           |FK(esdu cell)                                                |
+----------------+-------------------------------------------------------------+
| echotype       |FK(Echotype#name)                                            |
+----------------+-------------------------------------------------------------+
| sizeCategory   |FK(SizeCategory#name)                                        |
+----------------+-------------------------------------------------------------+
| baracoudaCode  |FK(Species#baracoudaCode)                                    |
+----------------+-------------------------------------------------------------+
| dataQuality    |FK(DataQuality#qualityDataFlagValues)                        |
+----------------+-------------------------------------------------------------+
| méta-données   |FK(DataMetadata#name) (une colonne par méta à importer)      |
+----------------+-------------------------------------------------------------+

Exemple
-------

::

  voyage;name;echotype;baracoudaCode;sizeCategory;ReferenceStationCatch;Biomass;MeanLength;MeanWeight;Abundance;SigmaSp;NASC;pondBiomass;pondAbundance;dataQuality
  PELGAS2011;2011-04-26 13:29:12.0000_16;D1;COMP-LEM;0;4;0;20;0.05;0;0.00100292822891053;0;0;0;1

Esdu / Echotype / Species Category / Length
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Colonnes requises
-----------------

::

  name;echotype;baracoudaCode;lengthClass;sizeCategory;Biomass;Abundance;lengthClassMeaning;dataQuality;voyage

Format des colonnes
-------------------

+----------------------+--------------------------------------------------------+
| Colonne              | Type                                                   |
+======================+========================================================+
| voyage               |FK(Voyage#name)                                         |
+----------------------+--------------------------------------------------------+
| name                 |FK(esdu cell)                                           |
+----------------------+--------------------------------------------------------+
| echotype             |FK(Echotype#name)                                       |
+----------------------+--------------------------------------------------------+
| baracoudaCode        |FK(Species#baracoudaCode)                               |
+----------------------+--------------------------------------------------------+
| lengthClass          |(float)                                                 |
+----------------------+--------------------------------------------------------+
| sizeCategory         |text                                                    |
+----------------------+--------------------------------------------------------+
| lengthClassMeaning   |text                                                    |
+----------------------+--------------------------------------------------------+
| dataQuality          |FK(DataQuality#qualityDataFlagValues)                   |
+----------------------+--------------------------------------------------------+
| méta-données         |FK(DataMetadata#name) (une colonne par méta à importer) |
+----------------------+--------------------------------------------------------+

Exemple
-------

::

  name;echotype;baracoudaCode;lengthClass;sizeCategory;Biomass;Abundance;lengthClassMeaning;dataQuality;voyage
  2011-04-26 13:35:19.0000_16;D1;COMP-LEM;10.5;4;5190.7652880528;1297691.3220132;mid point of 1 cm size bin;1;PELGAS2011

Esdu / Species / Age Category
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Colonnes requises
-----------------

::

  name;baracoudaCode;ageCategory;Abundance;ageCategoryMeaning;dataQuality;voyage

Format des colonnes
-------------------

+----------------------+--------------------------------------------------------+
| Colonne              | Type                                                   |
+======================+========================================================+
| voyage               |FK(Voyage#name)                                         |
+----------------------+--------------------------------------------------------+
| name                 |FK(esdu cell)                                           |
+----------------------+--------------------------------------------------------+
| baracoudaCode        |FK(Species#baracoudaCode)                               |
+----------------------+--------------------------------------------------------+
| ageCategory          |text                                                    |
+----------------------+--------------------------------------------------------+
| ageCategoryMeaning   |text                                                    |
+----------------------+--------------------------------------------------------+
| dataQuality          |FK(DataQuality#qualityDataFlagValues)                   |
+----------------------+--------------------------------------------------------+
| méta-données         |FK(DataMetadata#name) (une colonne par méta à importer) |
+----------------------+--------------------------------------------------------+

Exemple
-------

::

  name;baracoudaCode;ageCategory;Abundance;ageCategoryMeaning;dataQuality;voyage
  2011-04-26 13:35:19.0000_16;ENGR-ENC;3;3377.00492644042;fish age-group (years);1;PELGAS2011

Region
======

Region
~~~~~~

Colonnes requises
-----------------

::

  voyage;name;cellType;regionEnvCoordinates;surface;dataQuality

Format des colonnes
-------------------

+-----------------------+------------------------------------------------------+
| Colonne               | Type                                                 |
+=======================+======================================================+
| voyage                |FK(Voyage#name)                                       |
+-----------------------+------------------------------------------------------+
| cellType              |FK(CellType)                                          |
+-----------------------+------------------------------------------------------+
| dataQuality           |FK(DataQuality#qualityDataFlagValues)                 |
+-----------------------+------------------------------------------------------+
| name                  |text                                                  |
+-----------------------+------------------------------------------------------+
| regionEnvCoordinates  |text                                                  |
+-----------------------+------------------------------------------------------+
| surface               |(float)                                               |
+-----------------------+------------------------------------------------------+

Exemple
-------

::

  voyage;name;cellType;regionEnvCoordinates;surface;dataQuality
  PELGAS2011;1;RegionCLAS;-1.40644893137726 43.8651260867463 50;1224.75678263326;1

Region Association
~~~~~~~~~~~~~~~~~~

Colonnes requises
-----------------

::

  voyage;regionName;esduName

Format des colonnes
-------------------

+-----------------------+------------------------------------------------------+
| Colonne               | Type                                                 |
+=======================+======================================================+
| voyage                |FK(Voyage)                                            |
+-----------------------+------------------------------------------------------+
| regionName            |FK(Cell#name)                                         |
+-----------------------+------------------------------------------------------+
| esduName              |FK(Cell#name)                                         |
+-----------------------+------------------------------------------------------+

Notes importantes
-----------------

- Il faut trier les lignes sur regionName.

Exemple
-------

::

  voyage;regionName;esduName
  PELGAS2011;1;2011-05-02 09:36:54.0000_16

Region results
~~~~~~~~~~~~~~

Colonnes requises
-----------------

::

  voyage;name;baracoudaCode;sizeCategory;echotype;VarianceXe;VarianceNASC;MeanXe;MeanNASC;NASCWeightedMeanXe;Nesdu;Nhauls;MeanNASCWeightedBiomassDensity;NASCWeightedEstimationVariance;NASCWeightedEstimationCV;MeanBiomassDensity;EstimationVariance;EstimationCV;TotalEstimationVariance;TotalNASCWeightedEstimationVariance;ProportionOfTotalEstimationVariance;ProportionOfNASCWeightedTotalEstimationVariance;Biomass;NASCWeightedBiomass;dataQuality

Format des colonnes
-------------------

+----------------------+--------------------------------------------------------+
| Colonne              | Type                                                   |
+======================+========================================================+
| voyage               |FK(Voyage#name)                                         |
+----------------------+--------------------------------------------------------+
| name                 |FK(Cell#name)                                           |
+----------------------+--------------------------------------------------------+
| baracoudaCode        |FK(Species#baracoudaCode)                               |
+----------------------+--------------------------------------------------------+
| echotype             |FK(Echotype#name)                                       |
+----------------------+--------------------------------------------------------+
| sizeCategory         |FK(SizeCategory#name)                                   |
+----------------------+--------------------------------------------------------+
| dataQuality          |FK(DataQuality#qualityDataFlagValues)                   |
+----------------------+--------------------------------------------------------+
| méta-données         |FK(DataMetadata#name) (une colonne par méta à importer) |
+----------------------+--------------------------------------------------------+

Exemple
-------

::

  voyage;name;baracoudaCode;sizeCategory;echotype;VarianceXe;VarianceNASC;MeanXe;MeanNASC;NASCWeightedMeanXe;Nesdu;Nhauls;MeanNASCWeightedBiomassDensity;NASCWeightedEstimationVariance;NASCWeightedEstimationCV;MeanBiomassDensity;EstimationVariance;EstimationCV;TotalEstimationVariance;TotalNASCWeightedEstimationVariance;ProportionOfTotalEstimationVariance;ProportionOfNASCWeightedTotalEstimationVariance;Biomass;NASCWeightedBiomass;dataQuality
  PELGAS2011;6;CLUP-HAR;0;D2;NA;8963.28501789053;0.127534943481966;19.0945142790179;0.127534943481966;448;1;2.43521779939014;NA;NA;2.43521779939014;NA;NA;0;0;NA;NA;14380.2052341515;14380.2052341515;1

Map (Poisson)
=============

Map (Poisson)
~~~~~~~~~~~~~

Colonnes requises
-----------------

::

  voyage;name;baracoudaCode;sizeCategory;ageCategory;gridCellLongitude;gridCellLatitude;gridCellDepth;gridLongitudeLag;gridLatitudeLag;gridDepthLag;KrigedXe;dataQuality

Format des colonnes
-------------------

+----------------------+--------------------------------------------------------+
| Colonne              | Type                                                   |
+======================+========================================================+
| voyage               |FK(Voyage#name)                                         |
+----------------------+--------------------------------------------------------+
| name                 |text                                                    |
+----------------------+--------------------------------------------------------+
| baracoudaCode        |FK(Species#baracoudaCode)                               |
+----------------------+--------------------------------------------------------+
| sizeCategory         |FK(SizeCategory#name)                                   |
+----------------------+--------------------------------------------------------+
| ageCategory          |FK(AgeCategory#name)                                    |
+----------------------+--------------------------------------------------------+
| dataQuality          |FK(DataQuality#qualityDataFlagValues)                   |
+----------------------+--------------------------------------------------------+
| gridCellLongitude    |(float)                                                 |
+----------------------+--------------------------------------------------------+
| gridCellLatitude     |(float)                                                 |
+----------------------+--------------------------------------------------------+
| gridCellDepth        |(float)                                                 |
+----------------------+--------------------------------------------------------+
| gridLongitudeLag     |(float)                                                 |
+----------------------+--------------------------------------------------------+
| gridLatitudeLag      |(float)                                                 |
+----------------------+--------------------------------------------------------+
| gridDepthLag         |(float)                                                 |
+----------------------+--------------------------------------------------------+
| méta-données         |FK(DataMetadata#name) (une colonne par méta à importer) |
+----------------------+--------------------------------------------------------+

Notes importantes
-----------------

- Il faut trier les lignes sur name.

Exemple
-------

::

  voyage;name;baracoudaCode;sizeCategory;ageCategory;gridCellLongitude;gridCellLatitude;gridCellDepth;gridLongitudeLag;gridLatitudeLag;gridDepthLag;KrigedXe;dataQuality
  PELGAS2011;-6 43.5 0;ENGR-ENC;0;;-6;43.5;0;0.25;0.25;0;0;1

Map (Autre)
===========

Depuis la version 2.2.

Map (Autre)
~~~~~~~~~~~

Colonnes requises
-----------------

::

  voyage;name;gridCellLongitude;gridCellLatitude;gridCellDepth;gridLongitudeLag;gridLatitudeLag;gridDepthLag;KrigedXe;dataQuality

Format des colonnes
-------------------

+----------------------+--------------------------------------------------------+
| Colonne              | Type                                                   |
+======================+========================================================+
| voyage               |FK(Voyage#name)                                         |
+----------------------+--------------------------------------------------------+
| name                 |text                                                    |
+----------------------+--------------------------------------------------------+
| dataQuality          |FK(DataQuality#qualityDataFlagValues)                   |
+----------------------+--------------------------------------------------------+
| gridCellLongitude    |(float)                                                 |
+----------------------+--------------------------------------------------------+
| gridCellLatitude     |(float)                                                 |
+----------------------+--------------------------------------------------------+
| gridCellDepth        |(float)                                                 |
+----------------------+--------------------------------------------------------+
| gridLongitudeLag     |(float)                                                 |
+----------------------+--------------------------------------------------------+
| gridLatitudeLag      |(float)                                                 |
+----------------------+--------------------------------------------------------+
| gridDepthLag         |(float)                                                 |
+----------------------+--------------------------------------------------------+
| méta-données         |FK(DataMetadata#name) (une colonne par méta à importer) |
+----------------------+--------------------------------------------------------+

Exemple
-------

::

  voyage;name;gridCellLongitude;gridCellLatitude;gridCellDepth;gridLongitudeLag;gridLatitudeLag;gridDepthLag;KrigedXe;dataQuality
  PELGAS2011;-6 43.5 0;-6;43.5;0;0.25;0.25;0;0;1

Bouée
=====

Bouée
~~~~~

Colonnes requises
-----------------

::

  mission;code;description;depth;northLimit;eastLimit;southLimit;upLimit;downLimit;units;zunits;projection;deploymentDate;retrievalDate;siteName;operator;comments

Format des colonnes
-------------------

+-----------------------------+------------------------+
| Colonne                     | Type                   |
+=============================+========================+
| mission                     |FK(Mission#name)        |
+-----------------------------+------------------------+
| code                        |text                    |
+-----------------------------+------------------------+
| description                 |text                    |
+-----------------------------+------------------------+
| depth                       |(float)                 |
+-----------------------------+------------------------+
| northLimit                  |(float)                 |
+-----------------------------+------------------------+
| eastLimit                   |(float)                 |
+-----------------------------+------------------------+
| southLimit                  |(float)                 |
+-----------------------------+------------------------+
| upLimit                     |(float)                 |
+-----------------------------+------------------------+
| downLimit                   |(float)                 |
+-----------------------------+------------------------+
| units                       |text                    |
+-----------------------------+------------------------+
| zunits                      |text                    |
+-----------------------------+------------------------+
| projection                  |text                    |
+-----------------------------+------------------------+
| deploymentDate              |(date)                  |
+-----------------------------+------------------------+
| retrievalDate               |(date)                  |
+-----------------------------+------------------------+
| siteName                    |text                    |
+-----------------------------+------------------------+
| operator                    |text                    |
+-----------------------------+------------------------+
| comments                    |text                    |
+-----------------------------+------------------------+

Exemple
-------

::

  mission;code;description;depth;northLimit;eastLimit;southLimit;upLimit;downLimit;units;zunits;projection;deploymentDate;retrievalDate;siteName;operator;comments
  PELGAS;MOORING1;Mooring 1;1;1.1;1.2;1.3;1.4;1.5;m;m;None;26-04-2011 08:00:00.0000;04-06-2011 08:00:00.0000;CodeLutin;Moi;RAS


Ancillary instrumentation
~~~~~~~~~~~~~~~~~~~~~~~~~

Colonnes requises
-----------------

::

  mooring;ancillaryInstrumentation

Format des colonnes
-------------------

+-----------------------------+----------------------------------+
| Colonne                     | Type                             |
+=============================+==================================+
| mooring                     |FK(Mooring#code)                  |
+-----------------------------+----------------------------------+
| ancillaryInstrumentation    |FK(AncillaryInstrumentation#name) |
+-----------------------------+----------------------------------+

Exemple
-------

::

  mooring;ancillaryInstrumentation
  MOORING1;Inst1

Résultats bouée
===============

Echotype
~~~~~~~~

Colonnes requises
-----------------

::

  mooring;echotypeName;depthStratumId;meaning;baracoudaCode

Format des colonnes
-------------------

+-----------------+--------------------------+
| Colonne         | Type                     |
+=================+==========================+
| echotypeName    |text                      |
+-----------------+--------------------------+
| meaning         |text                      |
+-----------------+--------------------------+
| mooring         |FK(Mooring#code)          |
+-----------------+--------------------------+
| depthStratumId  |FK(DepthStratum#id)       |
+-----------------+--------------------------+
| baracoudaCode   |FK(Species#baracoudaCode) |
+-----------------+--------------------------+

Exemple
-------

::

  mooring;echotypeName;depthStratumId;meaning;baracoudaCode
  MOORING1;D1;CLAS;Maquereaux, Chinchards et gadidés présents dans les couches à proximité du fond;COMP-LEM

Résultats Esdu pour les bouées
==============================

Esdu / Echotype
~~~~~~~~~~~~~~~

Colonnes requises
-----------------

::

  mooring;name;echotype;NASC;ReferenceStationCatch;dataQuality

Format des colonnes
-------------------

+----------------+-------------------------------------------------------------+
| Colonne        | Type                                                        |
+================+=============================================================+
| mooring        |FK(Mooring#code)                                             |
+----------------+-------------------------------------------------------------+
| name           |FK(esdu cell)                                                |
+----------------+-------------------------------------------------------------+
| echotype       |FK(Echotype#name)                                            |
+----------------+-------------------------------------------------------------+
| dataQuality    |FK(DataQuality#qualityDataFlagValues)                        |
+----------------+-------------------------------------------------------------+
| méta-données   |FK(DataMetadata#name) (une colonne par méta à importer)      |
+----------------+-------------------------------------------------------------+

Exemple
-------

::

  mooring;name;echotype;NASC;ReferenceStationCatch;dataQuality
  MOORING1;2011-04-26 13:29:12.0000_16;D1;0;P0379;1

Esdu / Echotype / Species Category
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Colonnes requises
-----------------

::

  mooring;name;echotype;baracoudaCode;sizeCategory;ReferenceStationCatch;Biomass;MeanLength;MeanWeight;Abundance;SigmaSp;NASC;pondBiomass;pondAbundance;dataQuality

Format des colonnes
-------------------

+----------------+-------------------------------------------------------------+
| Colonne        | Type                                                        |
+================+=============================================================+
| mooring        |FK(Mooring#code)                                             |
+----------------+-------------------------------------------------------------+
| name           |FK(esdu cell)                                                |
+----------------+-------------------------------------------------------------+
| echotype       |FK(Echotype#name)                                            |
+----------------+-------------------------------------------------------------+
| sizeCategory   |FK(SizeCategory#name)                                        |
+----------------+-------------------------------------------------------------+
| baracoudaCode  |FK(Species#baracoudaCode)                                    |
+----------------+-------------------------------------------------------------+
| dataQuality    |FK(DataQuality#qualityDataFlagValues)                        |
+----------------+-------------------------------------------------------------+
| méta-données   |FK(DataMetadata#name) (une colonne par méta à importer)      |
+----------------+-------------------------------------------------------------+

Exemple
-------

::

  voyage;name;echotype;baracoudaCode;sizeCategory;ReferenceStationCatch;Biomass;MeanLength;MeanWeight;Abundance;SigmaSp;NASC;pondBiomass;pondAbundance;dataQuality
  MOORING1;2011-04-26 13:29:12.0000_16;D1;COMP-LEM;0;4;0;20;0.05;0;0.00100292822891053;0;0;0;1

.. _site Seadatanet: http://seadatanet.maris2.nl/v_bodc_vocab/search.asp?name=(C381)%20Ports+Gazetteer&l=C381 