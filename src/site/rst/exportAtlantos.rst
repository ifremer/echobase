.. -
.. * #%L
.. * EchoBase
.. * %%
.. * Copyright (C) 2020 Ifremer, Codelutin
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU Affero General Public License as published by
.. * the Free Software Foundation, either version 3 of the License, or
.. * (at your option) any later version.
.. *
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Public License for more details.
.. *
.. * You should have received a copy of the GNU Affero General Public License
.. * along with this program.  If not, see <http://www.gnu.org/licenses/>.
.. * #L%
.. -

=================================================
Export vers la base acoustique-chalutages du CIEM
=================================================

Résumé
======

Ce document explique comment exporter les données au format .xml de la base acoustique-chalutages du CIEM (https://www.ices.dk/data/data-portals/Pages/acoustic.aspx).

Exporter
--------

Les données peuvent être exportées au format xml CIEM en cliquant sur l'entrée de menu dédiée (**Exporter vers ICES**).
Vous devrez entrer la campagne et le navire à exporter. Vous obtiendrez alors les fichiers d'export xml (biotic et acoustic) dans un zip. Vous pourrez alors importer ces fichiers dans la base acoustique-chalutages du CIEM.

Espèces et engins exportées
---------------------------

Seules les espèces qui ont le champ **icesExport** renseigné à **true** seront exportées.
Seules les opérations avec un métier qui a le champs **icesExport** renseigné à **true** seront exportées.

Remerciements
-------------
Cette fonctionnalité a été ajoutée grâce au soutien financier du projet H2020 Atlantos (https://www.atlantos-h2020.eu/).
