.. -
.. * #%L
.. * EchoBase
.. * %%
.. * Copyright (C) 2011 - 2012 Ifremer, Codelutin
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU Affero General Public License as published by
.. * the Free Software Foundation, either version 3 of the License, or
.. * (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Public License for more details.
.. * 
.. * You should have received a copy of the GNU Affero General Public License
.. * along with this program.  If not, see <http://www.gnu.org/licenses/>.
.. * #L%
.. -

=======================
Suppression des imports
=======================

Abstract
--------

Ce document décrit ce qui se passe lors de la suppression d'un import.

Import Voyage complêt
---------------------

Données à supprimer
~~~~~~~~~~~~~~~~~~~

Le voyage en question et tout ce qui y est rattaché (Transit,...) et aussi :

- category (utilise EchoType)
- echotype

Imports à retirer
~~~~~~~~~~~~~~~~~

Seront supprimés de la table des imports les imports suivants qui aurait pu
être effectué suite à cet import, à savoir :

- import Transit
- import Transect
- import Operation
- import Catches
- import Acoustic
- import Résultats Voyage
- import Résultats Esdu
- import Region
- import Map (Poisson)
- import Map (Autre)

Import Voyage / Transit / Transect
----------------------------------

Données à supprimer
~~~~~~~~~~~~~~~~~~~

Le voyage en question et tout ce qui y est rattaché (Transit,...) et aussi :

- category (utilise EchoType)
- echotype

Imports à retirer
~~~~~~~~~~~~~~~~~

Seront supprimés de la table des imports les imports suivants qui aurait pu
être effectué suite à cet import, à savoir :

- import Transit
- import Transect
- import Operation
- import Catches
- import Acoustic
- import Résultats Voyage
- import Résultats Esdu
- import Region
- import Map (Poisson)
- import Map (Autre)

Import Voyage / Transit
-----------------------

Données à supprimer
~~~~~~~~~~~~~~~~~~~

Le voyage en question et tout ce qui y est rattaché (Transit,...) et aussi :

- category (utilise EchoType)
- echotype

Imports à retirer
~~~~~~~~~~~~~~~~~

Seront supprimés de la table des imports les imports suivants qui aurait pu
être effectué suite à cet import, à savoir :

- import Transit
- import Transect
- import Operation
- import Catches
- import Acoustic
- import Résultats Voyage
- import Résultats Esdu
- import Region
- import Map (Poisson)
- import Map (Autre)

Import Transit
--------------

Données à supprimer
~~~~~~~~~~~~~~~~~~~

Le transit en question et tout ce qui y est rattaché (Transect,...).

Imports à retirer
~~~~~~~~~~~~~~~~~

Seront supprimés de la table des imports les imports suivants qui aurait pu
être effectué suite à cet import, à savoir :

- import Transect (*)
- import Operation (*)
- import Catches (*)
- import Acoustic (*)
- import Résultats Esdu (**)
- import Region (**)
- import Map (Poisson) (**)
- import Map (Autre) (**)

(*) tous ceux sur la branche du transect
(**) tous ceux du voyage


Import Transect
---------------

Données à supprimer
~~~~~~~~~~~~~~~~~~~

Le transect en question et tout ce qui y est rattaché, ainsi que les *postCell*
et leur résultats.

Imports à retirer
~~~~~~~~~~~~~~~~~

Seront supprimés de la table des imports les imports suivants qui aurait pu
être effectué suite à cet import, à savoir :

- import Operation (*)
- import Catches (*)
- import Acoustic (*)
- import Résultats Esdu (**)
- import Region (**)
- import Map (Poisson) (**)
- import Map (Autre) (**)

(*) tous ceux sur la branche du transect
(**) tous ceux du voyage

Import Operation
----------------

Données à supprimer
~~~~~~~~~~~~~~~~~~~

Les opérations de pêche et tout ce qui y est rattaché (OperationMetadata et
GearMetadata et Sample).

Imports à retirer
~~~~~~~~~~~~~~~~~

Seront supprimés de la table des imports les imports suivants qui aurait pu
être effectué suite à cet import, à savoir :

- import Catches (*)

(*) tous ceux sur la branche des opérations

Import Catches
--------------

Données à supprimer
~~~~~~~~~~~~~~~~~~~

Les échantillons captures (Sample) en question et tout ce qui y est rattaché
(SampleData).

Imports à retirer
~~~~~~~~~~~~~~~~~

Aucun sous import possible à retirer.

Import Acoustic
---------------

Données à supprimer
~~~~~~~~~~~~~~~~~~~

Les données acoustiques et tout ce qui y est rattaché (DataAcquisition, Cell, Result),
ainsi que les *postCell* et leur résultats.

Imports à retirer
~~~~~~~~~~~~~~~~~

Seront supprimés de la table des imports les imports suivants qui aurait pu
être effectué sur ce voyage, à savoir :

- import Résultats Esdu (*)
- import Region (*)
- import Map (Poisson) (*)
- import Map (Autre) (*)

(*) tous ceux du voyage

Import Résultats voyage
-----------------------

Données à supprimer
~~~~~~~~~~~~~~~~~~~

Les données rattachés à un voyage après-coup, à savoir :

- echotype
- lengthageKey
- lengthWeightKey

Mais aussi tous les résultats (car il peuvent dépendre des données précédentes).

Imports à retirer
~~~~~~~~~~~~~~~~~

Seront supprimés de la table des imports les imports suivants qui aurait pu
être effectué suite à cet import, à savoir :

- import Résultats Esdu (*)
- import Region (*)
- import Map (Poisson) (*)
- import Map (Autre) (*)

(*) tous ceux du voyage

Immort Résultats Esdu
---------------------

Données à supprimer
~~~~~~~~~~~~~~~~~~~

Les résultats attachés aux cellules de type *ESDU*.

Imports à retirer
~~~~~~~~~~~~~~~~~

Aucun sous import possible à retirer.

Import Region
-------------

Données à supprimer
~~~~~~~~~~~~~~~~~~~

Les postCells de type *Region* et tout ce qui y est rattaché (Result)

Imports à retirer
~~~~~~~~~~~~~~~~~

Aucun sous import possible à retirer.

Import Map (Poisson)
--------------------

Données à supprimer
~~~~~~~~~~~~~~~~~~~

Les postCells de type *Map* et tout ce qui y est rattaché (Result)

Imports à retirer
~~~~~~~~~~~~~~~~~

Aucun sous import possible à retirer.

Import Map (Autre)
------------------

Données à supprimer
~~~~~~~~~~~~~~~~~~~

Les postCells de type *Map* et tout ce qui y est rattaché (Result)

Imports à retirer
~~~~~~~~~~~~~~~~~

Aucun sous import possible à retirer.
