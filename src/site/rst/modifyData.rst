.. -
.. * #%L
.. * EchoBase
.. * %%
.. * Copyright (C) 2011 - 2012 Ifremer, Codelutin
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU Affero General Public License as published by
.. * the Free Software Foundation, either version 3 of the License, or
.. * (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Public License for more details.
.. * 
.. * You should have received a copy of the GNU Affero General Public License
.. * along with this program.  If not, see <http://www.gnu.org/licenses/>.
.. * #L%
.. -

====================
Modifier des données
====================

Cliquer sur l'îcone "modifier les données" dans la barre d'icônes à gauche

Afficher les données contenues dans une table
=============================================

- Sélectionner la table à inspecter/modifier
- Le contenu d'une ligne de la table s'affiche en dessous lorsque l'on clique sur la ligne

Pour modifier ou ajouter des données dans une table (de référence)
==================================================================

Aller dans l'onglet exporter la table pour sauver la table dans un fichier texte.
Modifier le fichier texte
Pour ajouter une nouvelle ligne : 

- renseigner les champs de la nouvelle ligne, sans renseigner le champ topiaId

- sauver le fichier texte et l'importer dans EchoBase dans l'onglet "importer une table"


