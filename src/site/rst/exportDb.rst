.. -
.. * #%L
.. * EchoBase
.. * %%
.. * Copyright (C) 2011 - 2012 Ifremer, Codelutin
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU Affero General Public License as published by
.. * the Free Software Foundation, either version 3 of the License, or
.. * (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Public License for more details.
.. * 
.. * You should have received a copy of the GNU Affero General Public License
.. * along with this program.  If not, see <http://www.gnu.org/licenses/>.
.. * #L%
.. -

=================
Export d'une base
=================

Abstract
--------

EchoBase permet d'export une partie ou l'ensemble d'une base de travail au
format *.echobase* qui est ensuite réimportable dans une autre instance d'EchoBase.

- Cliquer sur le menu *Exporter une base*.

- Configurer l'export

Une fichier au format *.echobase* sera créé, il est ensuite possible de
l'importer sur une autre instance d'EchoBase.

Il existe plusieurs modes d'export que nous détaillons ci-dessous.

Export referential
------------------

Dans ce mode on exporte uniquement le référentiel de la base de travail.

Il vous suffit de renseigner le nom du fichier d'export (l'extension
*.echobase*) sera automatiquement rajoutée, puis de cliquer sur *Exporter*.

Une fois l'export réalisé, le fichier sera téléchargeable.

Export referential and data
---------------------------

Dans ce mode on exporte le référentiel et les données sélectionnées
de la base de travail.

Il vous suffit de renseigner le nom du fichier d'export (l'extension
*.echobase*) sera automatiquement rajoutée, de sélectionner les données 
à exporter puis de cliquer sur *Exporter*.

Une fois l'export réalisé, le fichier sera téléchargeable.

Export All
----------

Dans ce mode, on exporte toute la base de travail.

Il vous suffit de renseigner le nom du fichier d'export (l'extension
*.echobase*) sera automatiquement rajoutée, puis de cliquer sur *Exporter*.

Une fois l'export réalisé, le fichier sera téléchargeable.
