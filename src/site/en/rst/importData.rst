.. -
.. * #%L
.. * EchoBase
.. * %%
.. * Copyright (C) 2011 - 2012 Ifremer, Codelutin
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU Affero General Public License as published by
.. * the Free Software Foundation, either version 3 of the License, or
.. * (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Public License for more details.
.. * 
.. * You should have received a copy of the GNU Affero General Public License
.. * along with this program.  If not, see <http://www.gnu.org/licenses/>.
.. * #L%
.. -

==========================
Importing data in EchoBase
==========================

.. contents::

Abstract
========

Data can be imported in Echobase as text files.
This document provides details on the formats to be used to import text files in Echobase.

Columns format
~~~~~~~~~~~~~~

+---------------+---------------------------------------------------------------+
| Format name   | Description                                                   |
+===============+===============================================================+
| text          |character string                                               |
+---------------+---------------------------------------------------------------+
| FK(XXX#yyy)   |required foreign key (XXX on yyy property)                     |
+---------------+---------------------------------------------------------------+
| (int)         |a non-null integer                                             |
+---------------+---------------------------------------------------------------+
| (float)       |a non-null float                                               |
+---------------+---------------------------------------------------------------+
| (Integer)     |a potentially null integer                                     |
+---------------+---------------------------------------------------------------+
| (Integer-NA)  |a potentially null integer (set to null if NA)                 |
+---------------+---------------------------------------------------------------+
| Float         |a potentially null float                                       |
+---------------+---------------------------------------------------------------+
| (Float-NA)    |a potentially null float (set to null if NA)                   |
+---------------+---------------------------------------------------------------+
| (date)        |ISO date format: *yyyy-MM-dd HH:mm:ss.SSSS*                    |
+---------------+---------------------------------------------------------------+
| (esdu cell)   |esdu cell name: <date>_<num>[_(S|B)]                           |
|               | with date format yyyy-MM-dd HH:mm:ss.SSSS                     |
|               | with num for cell number                                      |
|               | with S (surface) or B (bottom) for the elementary cell        |
+---------------+---------------------------------------------------------------+

Important
~~~~~~~~~

- Column names are case sensitive, one must strictly use the column names given in this page
  
- The cell contents (as well as the column names) can be encapsulated between **"**.

Data check
~~~~~~~~~~

- The data imported into EchoBase are checked at the end of the import, to ensure that they are identical to the data in the input text file. 
- Si les données dans le fichier texte et dans la base ne sont pas identiques, un message d'erreur est affiché. Ceci peut provenir de problèmes de format ou de consistence des données d'entrée. 
- Afin de détecter ces problèmes, les fichiers texte d'entrée pour chaque import et le fichier "check" correspondant au contenu dans EchoBase sont disponibles dans le dossier "/var/local/echobase/data" du serveur où EchoBase est installé. Fichier d'import = "nomFichierImport-processed.csv" ; Fichier check = "nomFichierImport-checked.csv"


Voyage / Transit / Transect
===========================

Voyage
~~~~~~

Required columns
----------------

::

  name;startDate;endDate;startPort;endPort

Columns formats
---------------

+------------+----------------+
| Column     | Type           |
+============+================+
| name       |text            |
+------------+----------------+
| startDate  |(date)          |
+------------+----------------+
| endDate    |(date)          |
+------------+----------------+
| startPort  |FK(Port#code)   |
+------------+----------------+
| endPort    |FK(Port#code)   |
+------------+----------------+

Example
-------

::

  name;startDate;endDate;startPort;endPort
  PELGAS2011;2011-04-26 08:00:00.0000;2011-06-04 17:00:01.0000;BSH189;BSH4265

Transit
~~~~~~~

Required columns
----------------

::

  voyage;description;startTime;endTime;startLocality;endLocality

Columns formats
---------------

+----------------+----------------+
| Column         | Type           |
+================+================+
| voyage         |FK(voyage#name) |
+----------------+----------------+
| description    |text            |
+----------------+----------------+
| startTime      |(date)          |
+----------------+----------------+
| endTime        |(date)          |
+----------------+----------------+
| startLocality  |text            |
+----------------+----------------+
| endLocality    |text            |
+----------------+----------------+

Example
-------

::

  voyage;description;startTime;endTime;startLocality;endLocality
  PELGAS2011;PELGAS11CAMP1;2011-04-26 08:00:01.0000;2011-05-08 20:00:00.0000;Santander;La Rochelle

Transect
~~~~~~~~

Required columns
----------------

::

  title;transectAbstract;stratum;comments;voyage;vesselName;dateCreated;timeCoverageStart;timeCoverageEnd;geospatialLonMin;geospatialLatMin;geospatialVerticalMin;geospatialLonMax;geospatialLatMax;geospatialVerticalMax;linestring

Columns formats
---------------

+------------------------+----------------+
| Column                 | Type           |
+========================+================+
| voyage                 |FK(voyage#name) |
+------------------------+----------------+
| vesselName             |FK(Vessel#name) |
+------------------------+----------------+
| title                  |text            |
+------------------------+----------------+
| transectAbstract       |text            |
+------------------------+----------------+
| stratum                |text            |
+------------------------+----------------+
| comments               |text            |
+------------------------+----------------+
| dateCreated            |(date)          |
+------------------------+----------------+
| timeCoverageStart      |(date)          |
+------------------------+----------------+
| timeCoverageEnd        |(date)          |
+------------------------+----------------+
| geospatialLonMin       |(float)         |
+------------------------+----------------+
| geospatialLonMax       |(float)         |
+------------------------+----------------+
| geospatialLatMin       |(float)         |
+------------------------+----------------+
| geospatialLatMax       |(float)         |
+------------------------+----------------+
| geospatialVerticalMin  |(float)         |
+------------------------+----------------+
| geospatialVerticalMax  |(float)         |
+------------------------+----------------+
| linestring             |text            |
+------------------------+----------------+

Example
-------

::

  title;transectAbstract;stratum;comments;voyage;vesselName;dateCreated;timeCoverageStart;timeCoverageEnd;geospatialLonMin;geospatialLatMin;geospatialVerticalMin;geospatialLonMax;geospatialLatMax;geospatialVerticalMax;linestring
  Arlequin2 / la colombine942OBS01 SUR;942OBS;SUR;942OBS,P5001,SUR;PELGAS2011;Arlequin2 / la colombine;2011-04-27 09:10:00.0000;2011-04-27 09:10:00.0000;2011-04-27 10:39:00.0000;-1.5896666667;43.7055;110;-1.541;43.7606666667;110;-176453.0000037 4851310.5,-171051 4857434.0000037


Ancillary instrumentation
~~~~~~~~~~~~~~~~~~~~~~~~~

Required columns
----------------

::

  voyage;vessel;ancillaryInstrumentation

Columns formats
---------------

+-----------------------------+----------------------------------+
| Column                      | Type                             |
+=============================+==================================+
| voyage                      |FK(Voyage#name)                   |
+-----------------------------+----------------------------------+
| vessel                      |FK(Vessel#name)                   |
+-----------------------------+----------------------------------+
| ancillaryInstrumentation    |FK(AncillaryInstrumentation#name) |
+-----------------------------+----------------------------------+

Example
-------

::

  voyage;vessel;ancillaryInstrumentation
  PELGAS2011;Arlequin2 / la colombine;Inst1

Operation / OperationMetadataValue / GearMetadataValue
======================================================

Operation
~~~~~~~~~

Required columns
----------------

::

  vesselName;operationId;depthStratumId;gearShootingStartTime;midHaulLatitude;midHaulLongitude;gearShootingStartLatitude;gearShootingStartLongitude;gearShootingEndTime;gearHaulingEndLatitude;gearHaulingEndLongitude;gearCode

Columns formats
---------------

+-----------------------------+------------------------+
| Column                      | Type                   |
+=============================+========================+
| vesselName                  |FK(Vessel#name)         |
+-----------------------------+------------------------+
| gearCode                    |FK(Gear#casinoGearName) |
+-----------------------------+------------------------+
| depthStratumId              |FK(DepthStratum#id)     |
+-----------------------------+------------------------+
| operationId                 |text                    |
+-----------------------------+------------------------+
| midHaulLatitude             |(float)                 |
+-----------------------------+------------------------+
| midHaulLongitude            |(float)                 |
+-----------------------------+------------------------+
| gearShootingStartTime       |(date)                  |
+-----------------------------+------------------------+
| gearShootingEndTime         |(date)                  |
+-----------------------------+------------------------+
| gearShootingStartLatitude   |(float)                 |
+-----------------------------+------------------------+
| gearHaulingEndLatitude      |(float)                 |
+-----------------------------+------------------------+
| gearShootingStartLongitude  |(float)                 |
+-----------------------------+------------------------+
| gearHaulingEndLongitude     |(float)                 |
+-----------------------------+------------------------+

Example
-------

::

  vesselName;operationId;depthStratumId;gearShootingStartTime;midHaulLatitude;midHaulLongitude;gearShootingStartLatitude;gearShootingStartLongitude;gearShootingEndTime;gearHaulingEndLatitude;gearHaulingEndLongitude;gearCode
  THALASSA II;P0422;CLAS;2011-05-05 06:49:50.0000;45.4165605;-1.4492292;45.4029733;-1.4860902;2011-05-05 07:42:19.0000;45.4301477;-1.4123682;57x52

OperationMetadataValue
~~~~~~~~~~~~~~~~~~~~~~

Required columns
----------------

::

  vesselName;operationId;metadataType;operationMetadataValue

Columns formats
---------------

+-------------------------+---------------------------+
| Column                  | Type                      |
+=========================+===========================+
| vesselName              |FK(Vessel#name)            |
+-------------------------+---------------------------+
| operationId             |FK(Operation#id)           |
+-------------------------+---------------------------+
| metadataType            |FK(OperationMetadata#name) |
+-------------------------+---------------------------+
| operationMetadataValue  |text                       |
+-------------------------+---------------------------+

Example
-------

::

  vesselName;operationId;metadataType;operationMetadataValue
  THALASSA II;P0422;MeanWaterDepth;43.42

GearMetadataValue
~~~~~~~~~~~~~~~~~

Required columns
----------------

::

  vesselName;operationId;gearCode;metadataType;gearMetadataValue

Columns formats
---------------

+---------------------+------------------------+
| Column              | Type                   |
+=====================+========================+
| vesselName          |FK(Vessel#name)         |
+---------------------+------------------------+
| operationId         |FK(Operation#id)        |
+---------------------+------------------------+
| metadataType        |FK(GearMetadata#name)   |
+---------------------+------------------------+
| gearCode            |FK(Gear#casinoGearName) |
+---------------------+------------------------+
| gearMetadataValue   |text                    |
+---------------------+------------------------+

Example
-------

::

  vesselName;operationId;gearCode;metadataType;gearMetadataValue
  THALASSA II;P0422;57x52;CableLength;31.6666666666667

GearCharacteristicValues
~~~~~~~~~~~~~~~~~~~~~~~~

Required columns
----------------

::

  gear;characteristic;dataValue

Columns formats
---------------

+-------------------+------------------------------+
| Colonne           | Type                         |
+===================+==============================+
+-------------------+------------------------------+
| gear              |FK(Gear#casinoGearName)       |
+-------------------+------------------------------+
| characteristic    |FK(GearCharacteristic#name)   |
+-------------------+------------------------------+
| dataValue         |text                          |
+-------------------+------------------------------+

Example
-------

::

  gear;characteristic;dataValue
  casinoName;characteristicName;33

TotalSample / SubSample / BiometrySample
========================================

TotalSample
~~~~~~~~~~~

Required columns
----------------

::

  operationId;baracoudaCode;sizeCategory;sampleWeight;numberSampled;meanLength;meanWeight;noPerKg;sortedWeight

Columns formats
---------------

+-----------------+--------------------------+
| Column          | Type                     |
+=================+==========================+
| operationId     |FK(Operation#id)          |
+-----------------+--------------------------+
| baracoudaCode   |FK(Species#baracoudaCode) |
+-----------------+--------------------------+
| sizeCategory    |FK(SizeCategory#name)     |
+-----------------+--------------------------+
| sampleWeight    |(float)                   |
+-----------------+--------------------------+
| numberSampled   |(Integer-NA)              |
+-----------------+--------------------------+
| meanLength      |(Float-NA)                |
+-----------------+--------------------------+
| meanWeight      |(Float-NA)                |
+-----------------+--------------------------+
| noPerKg         |(Float-NA)                |
+-----------------+--------------------------+
| sortedWeight    |(float)                   |
+-----------------+--------------------------+

Example
-------

::

  operationId;baracoudaCode;sizeCategory;sampleWeight;numberSampled;meanLength;meanWeight;noPerKg;sortedWeight
  P0435;ALLO-TEZ;0;0.21;36;6.4;6;166.66;0.03

SubSample
~~~~~~~~~

Required columns
----------------

::

  operationId;baracoudaCode;sizeCategory;sexCategory;sampleWeight;numberSampled;lengthClass;numberAtLength;weightAtLength;round

Columns formats
---------------

+------------------+--------------------------+
| Column           | Type                     |
+==================+==========================+
| operationId      |FK(Operation#id)          |
+------------------+--------------------------+
| baracoudaCode    |FK(Species#baracoudaCode) |
+------------------+--------------------------+
| sizeCategory     |FK(SizeCategory#name)     |
+------------------+--------------------------+
| sexCategory      |FK(SexCategory#name)      |
+------------------+--------------------------+
| sampleWeight     |(float)                   |
+------------------+--------------------------+
| numberSampled    |(int)                     |
+------------------+--------------------------+
| numberAtLength   |(float)                   |
+------------------+--------------------------+
| weightAtLength   |(Float-NA)                |
+------------------+--------------------------+
| lengthClass      |text                      |
+------------------+--------------------------+

(ignored columns) subHaul round

Example
-------

::

  operationId;baracoudaCode;sizeCategory;sexCategory;sampleWeight;numberSampled;lengthClass;numberAtLength;weightAtLength;units;round
  P0372;ENGR-ENC;0;N;3.37;198;12;7;0.08;0;5

BiometrySample
~~~~~~~~~~~~~~

Required columns
----------------

::

  operationId;baracoudaCode;numFish;dataLabel;dataValue;name;sizeCategory

Columns formats
---------------

+----------------+--------------------------+
| Column         | Type                     |
+================+==========================+
| operationId    |FK(Operation#id)          |
+----------------+--------------------------+
| baracoudaCode  |FK(Species#baracoudaCode) |
+----------------+--------------------------+
| numFish        |(int)                     |
+----------------+--------------------------+
| name           |FK(SampleDataType#name)   |
+----------------+--------------------------+
| dataLabel      |text                      |
+----------------+--------------------------+
| dataValue      |(Float-NA)                |
+----------------+--------------------------+
| sizeCategory   |FK(SizeCategory#name)     |
+----------------+--------------------------+

Example
-------

::

  operationId;baracoudaCode;numFish;dataLabel;dataValue;name;sizeCategory
  P5002;ENGR-ENC;9394;NA;-1;Age;0

Acoustic (voyage or mooring)
============================

Acoustic
~~~~~~~~

Required columns
----------------

::

  MOVIES_EILayer;MOVIES_EILayer\sndset;MOVIES_EILayer\sndset\sndname;MOVIES_EILayer\sndset\sndident;MOVIES_EILayer\sndset\softChannelId;MOVIES_EILayer\sndset\channelName;MOVIES_EILayer\sndset\dataType;MOVIES_EILayer\sndset\beamType;MOVIES_EILayer\sndset\acousticFrequency;MOVIES_EILayer\sndset\startSample;MOVIES_EILayer\sndset\mainBeamAlongSteeringAngle;MOVIES_EILayer\sndset\mainBeamAthwartSteeringAngle;MOVIES_EILayer\sndset\absorptionCoef;MOVIES_EILayer\sndset\transmissionPower;MOVIES_EILayer\sndset\beamAlongAngleSensitivity;MOVIES_EILayer\sndset\beamAthwartAngleSensitivity;MOVIES_EILayer\sndset\beam3dBWidthAlong;MOVIES_EILayer\sndset\beam3dBWidthAthwart;MOVIES_EILayer\sndset\beamEquTwoWayAngle;MOVIES_EILayer\sndset\beamGain;MOVIES_EILayer\sndset\beamSACorrection;MOVIES_EILayer\sndset\bottomDetectionMinDepth;MOVIES_EILayer\sndset\bottomDetectionMaxDepth;MOVIES_EILayer\sndset\bottomDetectionMinLevel;MOVIES_EILayer\sndset\AlongTXRXWeightId;MOVIES_EILayer\sndset\AthwartTXRXWeightId;MOVIES_EILayer\sndset\SplitBeamAlongTXRXWeightId;MOVIES_EILayer\sndset\SplitBeamAthwartTXRXWeightId;MOVIES_EILayer\sndset\bandWidth;MOVIES_EILayer\sndset\tvgminrange;MOVIES_EILayer\sndset\tvgmaxrange;MOVIES_EILayer\sndset\pulseduration;MOVIES_EILayer\shipnav;MOVIES_EILayer\shipnav\lat;MOVIES_EILayer\shipnav\long;MOVIES_EILayer\shipnav\alt;MOVIES_EILayer\shipnav\gndspeed;MOVIES_EILayer\shipnav\gndcourse;MOVIES_EILayer\shipnav\surfspeed;MOVIES_EILayer\shipnav\surfcourse;MOVIES_EILayer\shipnav\driftspeed;MOVIES_EILayer\shipnav\driftcourse;MOVIES_EILayer\shipnav\heading;MOVIES_EILayer\shipnav\roll;MOVIES_EILayer\shipnav\pitch;MOVIES_EILayer\shipnav\heave;MOVIES_EILayer\shipnav\depth;MOVIES_EILayer\shipnav\draught;MOVIES_EILayer\cellset;MOVIES_EILayer\cellset\cellnum;MOVIES_EILayer\cellset\celltype;MOVIES_EILayer\cellset\depthstart;MOVIES_EILayer\cellset\depthend;MOVIES_EILayer\cellset\indexstart;MOVIES_EILayer\cellset\indexend;MOVIES_EILayer\cellset\datestart;MOVIES_EILayer\cellset\dateend;MOVIES_EILayer\cellset\lat;MOVIES_EILayer\cellset\long;MOVIES_EILayer\cellset\volume;MOVIES_EILayer\cellset\area;MOVIES_EILayer\cellset\diststart;MOVIES_EILayer\cellset\distend;MOVIES_EILayer\cellset\thresholdup;MOVIES_EILayer\cellset\thresholdlow;MOVIES_EILayer\eilayer;MOVIES_EILayer\eilayer\sa;MOVIES_EILayer\eilayer\sv;MOVIES_EILayer\eilayer\ni;MOVIES_EILayer\eilayer\nt;MOVIES_EILayer\boterr;MOVIES_EILayer\boterr\sa;MOVIES_EILayer\boterr\ni;MOVIES_EILayer\sndset\soundcelerity;dataQuality;label

Columns formats
---------------

+---------------------------------------------------------+------------------------------------------+
| Column                                                  | Type                                     |
+=========================================================+==========================================+
| MOVIES_EILayer                                          |text                                  //A |
+---------------------------------------------------------+------------------------------------------+
| MOVIES_EILayer\\sndset                                  |(unused)                              //B |
+---------------------------------------------------------+------------------------------------------+
| MOVIES_EILayer\\sndset\\sndname                         |(unused)                              //C |
+---------------------------------------------------------+------------------------------------------+
| MOVIES_EILayer\\sndset\\sndident                        |(unused)                              //D |
+---------------------------------------------------------+------------------------------------------+
| MOVIES_EILayer\\sndset\\softChannelId                   |FK(AcousticInstrument#id)             //E |
+---------------------------------------------------------+------------------------------------------+
| MOVIES_EILayer\\sndset\\channelName                     |(unused)                              //F |
+---------------------------------------------------------+------------------------------------------+
| MOVIES_EILayer\\sndset\\dataType                        |(unused)                              //G |
+---------------------------------------------------------+------------------------------------------+
| MOVIES_EILayer\\sndset\\beamType                        |(unused)                              //H |
+---------------------------------------------------------+------------------------------------------+
| MOVIES_EILayer\\sndset\\acousticFrequency               |(unused)                              //I |
+---------------------------------------------------------+------------------------------------------+
| MOVIES_EILayer\\sndset\\startSample                     |(unused)                              //J |
+---------------------------------------------------------+------------------------------------------+
| MOVIES_EILayer\\sndset\\mainBeamAlongSteeringAngle      |(unused)                              //K |
+---------------------------------------------------------+------------------------------------------+
| MOVIES_EILayer\\sndset\\mainBeamAthwartSteeringAngle    |(unused)                              //L |
+---------------------------------------------------------+------------------------------------------+
| MOVIES_EILayer\\sndset\\absorptionCoef                  |(Float-NA)                            //M |
+---------------------------------------------------------+------------------------------------------+
| MOVIES_EILayer\\sndset\\transmissionPower               |(int)                                 //N |
+---------------------------------------------------------+------------------------------------------+
| MOVIES_EILayer\\sndset\\beamAlongAngleSensitivity       |(Float)                               //O |
+---------------------------------------------------------+------------------------------------------+
| MOVIES_EILayer\\sndset\\beamAthwartAngleSensitivity     |(Float)                               //P |
+---------------------------------------------------------+------------------------------------------+
| MOVIES_EILayer\\sndset\\beam3dBWidthAlong               |(unused)                              //Q |
+---------------------------------------------------------+------------------------------------------+
| MOVIES_EILayer\\sndset\\beam3dBWidthAthwart             |(unused)                              //R |
+---------------------------------------------------------+------------------------------------------+
| MOVIES_EILayer\\sndset\\beamEquTwoWayAngle              |(Float)                               //S |
+---------------------------------------------------------+------------------------------------------+
| MOVIES_EILayer\\sndset\\beamGain                        |(Float)                               //T |
+---------------------------------------------------------+------------------------------------------+
| MOVIES_EILayer\\sndset\\beamSACorrection                |(Float)                               //U |
+---------------------------------------------------------+------------------------------------------+
| MOVIES_EILayer\\sndset\\bottomDetectionMinDepth         |(unused)                              //V |
+---------------------------------------------------------+------------------------------------------+
| MOVIES_EILayer\\sndset\\bottomDetectionMaxDepth         |(unused)                              //W |
+---------------------------------------------------------+------------------------------------------+
| MOVIES_EILayer\\sndset\\bottomDetectionMinLevel         |(unused)                              //X |
+---------------------------------------------------------+------------------------------------------+
| MOVIES_EILayer\\sndset\\AlongTXRXWeightId               |(unused)                              //Y |
+---------------------------------------------------------+------------------------------------------+
| MOVIES_EILayer\\sndset\\AthwartTXRXWeightId             |(unused)                              //Z |
+---------------------------------------------------------+------------------------------------------+
| MOVIES_EILayer\\sndset\\SplitBeamAlongTXRXWeightId      |(unused)                             //AA |
+---------------------------------------------------------+------------------------------------------+
| MOVIES_EILayer\\sndset\\SplitBeamAthwartTXRXWeightId    |(unused)                             //AB |
+---------------------------------------------------------+------------------------------------------+
| MOVIES_EILayer\\sndset\\bandWidth                       |(unused)                             //AC |
+---------------------------------------------------------+------------------------------------------+
| MOVIES_EILayer\\sndset\\tvgminrange                     |(unused)                             //AD |
+---------------------------------------------------------+------------------------------------------+
| MOVIES_EILayer\\sndset\\tvgmaxrange                     |(unused)                             //AE |
+---------------------------------------------------------+------------------------------------------+
| MOVIES_EILayer\\sndset\\pulseduration                   |(Float)                              //AF |
+---------------------------------------------------------+------------------------------------------+
| MOVIES_EILayer\\shipnav                                 |(unused)                             //AG |
+---------------------------------------------------------+------------------------------------------+
| MOVIES_EILayer\\shipnav\\lat                            |(unused)                             //AH |
+---------------------------------------------------------+------------------------------------------+
| MOVIES_EILayer\\shipnav\\long                           |(unused)                             //AI |
+---------------------------------------------------------+------------------------------------------+
| MOVIES_EILayer\\shipnav\\alt                            |(unused)                             //AJ |
+---------------------------------------------------------+------------------------------------------+
| MOVIES_EILayer\\shipnav\\gndspeed                       |(unused)                             //AK |
+---------------------------------------------------------+------------------------------------------+
| MOVIES_EILayer\\shipnav\\gndcourse                      |(unused)                             //AL |
+---------------------------------------------------------+------------------------------------------+
| MOVIES_EILayer\\shipnav\\surfspeed                      |(unused)                             //AM |
+---------------------------------------------------------+------------------------------------------+
| MOVIES_EILayer\\shipnav\\surfcourse                     |(unused)                             //AN |
+---------------------------------------------------------+------------------------------------------+
| MOVIES_EILayer\\shipnav\\driftspeed                     |(unused)                             //AO |
+---------------------------------------------------------+------------------------------------------+
| MOVIES_EILayer\\shipnav\\driftcourse                    |(unused)                             //AP |
+---------------------------------------------------------+------------------------------------------+
| MOVIES_EILayer\\shipnav\\heading                        |(unused)                             //AQ |
+---------------------------------------------------------+------------------------------------------+
| MOVIES_EILayer\\shipnav\\roll                           |(unused)                             //AR |
+---------------------------------------------------------+------------------------------------------+
| MOVIES_EILayer\\shipnav\\pitch                          |(unused)                             //AS |
+---------------------------------------------------------+------------------------------------------+
| MOVIES_EILayer\\shipnav\\heave                          |(unused)                             //AT |
+---------------------------------------------------------+------------------------------------------+
| MOVIES_EILayer\\shipnav\\depth                          |text                                 //AU |
+---------------------------------------------------------+------------------------------------------+
| MOVIES_EILayer\\shipnav\\draught                        |(unused)                             //AV |
+---------------------------------------------------------+------------------------------------------+
| MOVIES_EILayer\\cellset                                 |(unused)                             //AW |
+---------------------------------------------------------+------------------------------------------+
| MOVIES_EILayer\\cellset\\cellnum                        |(int)                                //AX |
+---------------------------------------------------------+------------------------------------------+
| MOVIES_EILayer\\cellset\\celltype                       |(int)                                //AY |
+---------------------------------------------------------+------------------------------------------+
| MOVIES_EILayer\\cellset\\depthstart                     |(float)                              //AZ |
+---------------------------------------------------------+------------------------------------------+
| MOVIES_EILayer\\cellset\\depthend                       |(float)                              //BA |
+---------------------------------------------------------+------------------------------------------+
| MOVIES_EILayer\\cellset\\indexstart                     |(unused)                             //BB |
+---------------------------------------------------------+------------------------------------------+
| MOVIES_EILayer\\cellset\\indexend                       |(unused)                             //BC |
+---------------------------------------------------------+------------------------------------------+
| MOVIES_EILayer\\cellset\\datestart                      |(date)                               //BD |
+---------------------------------------------------------+------------------------------------------+
| MOVIES_EILayer\\cellset\\dateend                        |(date)                               //BE |
+---------------------------------------------------------+------------------------------------------+
| MOVIES_EILayer\\cellset\\lat                            |(Float)                              //BF |
+---------------------------------------------------------+------------------------------------------+
| MOVIES_EILayer\\cellset\\long                           |(Float)                              //BG |
+---------------------------------------------------------+------------------------------------------+
| MOVIES_EILayer\\cellset\\volume                         |(Integer-NA)                         //BH |
+---------------------------------------------------------+------------------------------------------+
| MOVIES_EILayer\\cellset\\area                           |(int)                                //BI |
+---------------------------------------------------------+------------------------------------------+
| MOVIES_EILayer\\cellset\\diststart                      |(unused)                             //BJ |
+---------------------------------------------------------+------------------------------------------+
| MOVIES_EILayer\\cellset\\distend                        |(unused)                             //BK |
+---------------------------------------------------------+------------------------------------------+
| MOVIES_EILayer\\cellset\\thresholdup                    |(int)                                //BL |
+---------------------------------------------------------+------------------------------------------+
| MOVIES_EILayer\\cellset\\thresholdlow                   |(int)                                //BM |
+---------------------------------------------------------+------------------------------------------+
| MOVIES_EILayer\\eilayer                                 |(unused)                             //BN |
+---------------------------------------------------------+------------------------------------------+
| MOVIES_EILayer\\eilayer\\sa                             |(Float-NA)                           //BO |
+---------------------------------------------------------+------------------------------------------+
| MOVIES_EILayer\\eilayer\\sv                             |(unused)                             //BP |
+---------------------------------------------------------+------------------------------------------+
| MOVIES_EILayer\\eilayer\\ni                             |(Integer-NA)                         //BQ |
+---------------------------------------------------------+------------------------------------------+
| MOVIES_EILayer\\eilayer\\nt                             |(Integer-NA)                         //BR |
+---------------------------------------------------------+------------------------------------------+
| MOVIES_EILayer\\boterr                                  |(unused)                             //BR |
+---------------------------------------------------------+------------------------------------------+
| MOVIES_EILayer\\boterr\\sa                              |(unused)                             //BT |
+---------------------------------------------------------+------------------------------------------+
| MOVIES_EILayer\\boterr\\ni                              |(unused)                             //BU |
+---------------------------------------------------------+------------------------------------------+
| MOVIES_EILayer\\sndset\\soundcelerity                   |text                                 //BV |
+---------------------------------------------------------+------------------------------------------+
| dataQuality                                             |FK(DataQuality#qualityDataFlagValues) //E |
+---------------------------------------------------------+------------------------------------------+
| label                                                   |text                                 //BW |
+---------------------------------------------------------+------------------------------------------+

Important
---------

- You have to sort the line on the date (MOVIES_EILayer) and on the cell number (MOVIES_EILayer\\cellset\\cellnum).

Example
-------

::

  MOVIES_EILayer;MOVIES_EILayer\sndset;MOVIES_EILayer\sndset\sndname;MOVIES_EILayer\sndset\sndident;MOVIES_EILayer\sndset\softChannelId;MOVIES_EILayer\sndset\channelName;MOVIES_EILayer\sndset\dataType;MOVIES_EILayer\sndset\beamType;MOVIES_EILayer\sndset\acousticFrequency;MOVIES_EILayer\sndset\startSample;MOVIES_EILayer\sndset\mainBeamAlongSteeringAngle;MOVIES_EILayer\sndset\mainBeamAthwartSteeringAngle;MOVIES_EILayer\sndset\absorptionCoef;MOVIES_EILayer\sndset\transmissionPower;MOVIES_EILayer\sndset\beamAlongAngleSensitivity;MOVIES_EILayer\sndset\beamAthwartAngleSensitivity;MOVIES_EILayer\sndset\beam3dBWidthAlong;MOVIES_EILayer\sndset\beam3dBWidthAthwart;MOVIES_EILayer\sndset\beamEquTwoWayAngle;MOVIES_EILayer\sndset\beamGain;MOVIES_EILayer\sndset\beamSACorrection;MOVIES_EILayer\sndset\bottomDetectionMinDepth;MOVIES_EILayer\sndset\bottomDetectionMaxDepth;MOVIES_EILayer\sndset\bottomDetectionMinLevel;MOVIES_EILayer\sndset\AlongTXRXWeightId;MOVIES_EILayer\sndset\AthwartTXRXWeightId;MOVIES_EILayer\sndset\SplitBeamAlongTXRXWeightId;MOVIES_EILayer\sndset\SplitBeamAthwartTXRXWeightId;MOVIES_EILayer\sndset\bandWidth;MOVIES_EILayer\sndset\tvgminrange;MOVIES_EILayer\sndset\tvgmaxrange;MOVIES_EILayer\sndset\pulseduration;MOVIES_EILayer\shipnav;MOVIES_EILayer\shipnav\lat;MOVIES_EILayer\shipnav\long;MOVIES_EILayer\shipnav\alt;MOVIES_EILayer\shipnav\gndspeed;MOVIES_EILayer\shipnav\gndcourse;MOVIES_EILayer\shipnav\surfspeed;MOVIES_EILayer\shipnav\surfcourse;MOVIES_EILayer\shipnav\driftspeed;MOVIES_EILayer\shipnav\driftcourse;MOVIES_EILayer\shipnav\heading;MOVIES_EILayer\shipnav\roll;MOVIES_EILayer\shipnav\pitch;MOVIES_EILayer\shipnav\heave;MOVIES_EILayer\shipnav\depth;MOVIES_EILayer\shipnav\draught;MOVIES_EILayer\cellset;MOVIES_EILayer\cellset\cellnum;MOVIES_EILayer\cellset\celltype;MOVIES_EILayer\cellset\depthstart;MOVIES_EILayer\cellset\depthend;MOVIES_EILayer\cellset\indexstart;MOVIES_EILayer\cellset\indexend;MOVIES_EILayer\cellset\datestart;MOVIES_EILayer\cellset\dateend;MOVIES_EILayer\cellset\lat;MOVIES_EILayer\cellset\long;MOVIES_EILayer\cellset\volume;MOVIES_EILayer\cellset\area;MOVIES_EILayer\cellset\diststart;MOVIES_EILayer\cellset\distend;MOVIES_EILayer\cellset\thresholdup;MOVIES_EILayer\cellset\thresholdlow;MOVIES_EILayer\eilayer;MOVIES_EILayer\eilayer\sa;MOVIES_EILayer\eilayer\sv;MOVIES_EILayer\eilayer\ni;MOVIES_EILayer\eilayer\nt;MOVIES_EILayer\boterr;MOVIES_EILayer\boterr\sa;MOVIES_EILayer\boterr\ni;MOVIES_EILayer\sndset\soundcelerity;dataQuality;label
  2011/06/23 10:09:00.0000;NA;NA;NA;47;38000;NA;NA;NA;NA;NA;NA;8.47;2000;21.8;21.8;NA;NA;-20.6;25.27;-0.529999;NA;NA;NA;NA;NA;NA;NA;NA;NA;NA;1.024;NA;NA;NA;NA;NA;NA;NA;NA;NA;NA;NA;NA;NA;NA;199.9;NA;NA;0;0;10;20;NA;NA;2011-04-26 08:24:02.0000;2011-04-26 08:30:02.0000;43.666505;-3.48812666666667;NA;1;NA;NA;0;-60;NA;1.444008;NA;71;43424;NA;NA;NA;variable;1;0

Calibration
~~~~~~~~~~~

Required columns
----------------

::

  topiaId;accuracyEstimate;aquisitionMethod;comments;date;processingMethod;report;acousticInstrument

Columns format
--------------

+------------------------+------------------------------------------+
| Column                 | Type                                     |
+========================+==========================================+
| topiaId                |(ignored column)                      //A |
+------------------------+------------------------------------------+
| accuracyEstimate       |text                                  //B |
+------------------------+------------------------------------------+
| aquisitionMethod       |text                                  //C |
+------------------------+------------------------------------------+
| comments               |text                                  //D |
+------------------------+------------------------------------------+
| date                   |(date)                                //E |
+------------------------+------------------------------------------+
| processingMethod       |text                                  //F |
+------------------------+------------------------------------------+
| report                 |text                                  //G |
+------------------------+------------------------------------------+
| acousticinstrument     |FK(AcousticInstrument#id)             //H |
+------------------------+------------------------------------------+

Notes importantes
-----------------

- For the calibration to match a trip, the date must be within the trip start-end date period.

Example
-------

::

  accuracyEstimate;aquisitionMethod;comments;date;processingMethod;report;acousticInstrument
  ;0.26;ER60;Mission PELGAS2015 - 30 avril 2015;2015-04-30 07:00:00;ER60;http://madida/madida/documents/etalonnage/TL_EK60_etalonnage_avril2015__v1.html;47


Results per voyage
==================

Echotype
~~~~~~~~

Required columns
----------------

::

  voyage;echotypeName;depthStratumId;meaning;baracoudaCode;id

Columns formats
---------------

+-----------------+-------------------------------+
| Column          | Type                          |
+=================+===============================+
| echotypeName    |text                           |
+-----------------+-------------------------------+
| meaning         |text                           |
+-----------------+-------------------------------+
| voyage          |FK(Voyage#name)                |
+-----------------+-------------------------------+
| depthStratumId  |FK(DepthStratum#id)            |
+-----------------+-------------------------------+
| baracoudaCode   |FK(Species#baracoudaCode)      |
+-----------------+-------------------------------+
| id              |integer (species category code |
+-----------------+-------------------------------+

Example
-------

::

  voyage;echotypeName;depthStratumId;meaning;baracoudaCode;id
  PELGAS2011;D1;CLAS;Maquereaux, Chinchards et gadidés présents dans les couches à proximité du fond;COMP-LEM;1

LengthWeightKey
~~~~~~~~~~~~~~~

Required columns
----------------

::

  voyage;sizeCategory;aParameter;bParameter;baracoudaCode;strata

Columns formats
---------------

+----------------+--------------------------+
| Column         | Type                     |
+================+==========================+
| aParameter     |(float)                   |
+----------------+--------------------------+
| bParameter     |(float)                   |
+----------------+--------------------------+
| voyage         |FK(Voyage#name)           |
+----------------+--------------------------+
| sizeCategory   |FK(SizeCategory#name)     |
+----------------+--------------------------+
| baracoudaCode  |FK(Species#baracoudaCode) |
+----------------+--------------------------+
| strata         |FK(Strata#name)           |
+----------------+--------------------------+

Example
-------

::

  voyage;sizeCategory;aParameter;bParameter;baracoudaCode;strata
  PELGAS2011;0;2.48253468289872;3.37866337729714;ENGR-ENC;Golfe de Gascogne

LengthAgeKey
~~~~~~~~~~~~

Required columns
----------------

::

  voyage;baracoudaCode;age;length;percentAtAge;metadata;strata

Columns formats
---------------

+----------------+--------------------------+
| Column         | Type                     |
+================+==========================+
| voyage         |FK(Voyage#name)           |
+----------------+--------------------------+
| age            |(int)                     |
+----------------+--------------------------+
| length         |(float)                   |
+----------------+--------------------------+
| percentAtAge   |(float)                   |
+----------------+--------------------------+
| metadata       |text                      |
+----------------+--------------------------+
| strata         |FK(Strata#name)           |
+----------------+--------------------------+
| baracoudaCode  |FK(Species#baracoudaCode) |
+----------------+--------------------------+

Example
-------

::

  voyage;baracoudaCode;age;length;percentAtAge;metadata;strata
  PELGAS2011;ENGR-ENC;1;7.5;1;Ifremer reading;Golfe de Gascogne

Results per ESDU for voyage
===========================

Esdu / Echotype
~~~~~~~~~~~~~~~

Required columns
----------------

::

  voyage;name;echotype;NASC;ReferenceStationCatch;dataQuality

Columns formats
---------------

+----------------+-------------------------------------------------------------+
| Column         | Type                                                        |
+================+=============================================================+
| voyage         |FK(Voyage#name)                                              |
+----------------+-------------------------------------------------------------+
| name           |FK(esdu cell)                                                |
+----------------+-------------------------------------------------------------+
| echotype       |FK(Echotype#name)                                            |
+----------------+-------------------------------------------------------------+
| dataQuality    |FK(DataQuality#qualityDataFlagValues)                        |
+----------------+-------------------------------------------------------------+
| méta-données   |FK(DataMetadata#name) (one column per metadata)              |
+----------------+-------------------------------------------------------------+

Example
-------

::

  voyage;name;echotype;NASC;ReferenceStationCatch;dataQuality
  PELGAS2011;2011-04-26 13:29:12.0000_16;D1;0;P0379;1

Esdu / Echotype / Species Category
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Required columns
----------------

::

  voyage;name;echotype;baracoudaCode;sizeCategory;ReferenceStationCatch;Biomass;MeanLength;MeanWeight;Abundance;SigmaSp;NASC;pondBiomass;pondAbundance;dataQuality

Columns formats
---------------

+----------------+-------------------------------------------------------------+
| Column         | Type                                                        |
+================+=============================================================+
| voyage         |FK(Voyage#name)                                              |
+----------------+-------------------------------------------------------------+
| name           |FK(esdu cell)                                                |
+----------------+-------------------------------------------------------------+
| echotype       |FK(Echotype#name)                                            |
+----------------+-------------------------------------------------------------+
| sizeCategory   |FK(SizeCategory#name)                                        |
+----------------+-------------------------------------------------------------+
| baracoudaCode  |FK(Species#baracoudaCode)                                    |
+----------------+-------------------------------------------------------------+
| dataQuality    |FK(DataQuality#qualityDataFlagValues)                        |
+----------------+-------------------------------------------------------------+
| méta-données   |FK(DataMetadata#name) (one column per metadata)              |
+----------------+-------------------------------------------------------------+

Example
-------

::

  voyage;name;echotype;baracoudaCode;sizeCategory;ReferenceStationCatch;Biomass;MeanLength;MeanWeight;Abundance;SigmaSp;NASC;pondBiomass;pondAbundance;dataQuality
  PELGAS2011;2011-04-26 13:29:12.0000_16;D1;COMP-LEM;0;4;0;20;0.05;0;0.00100292822891053;0;0;0;1

Esdu / Echotype / Species Category / Length
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Required columns
----------------

::

  name;echotype;baracoudaCode;lengthClass;sizeCategory;Biomass;Abundance;lengthClassMeaning;dataQuality;voyage

Columns formats
---------------

+----------------------+-------------------------------------------------------+
| Column               | Type                                                  |
+======================+=======================================================+
| voyage               |FK(Voyage#name)                                        |
+----------------------+-------------------------------------------------------+
| name                 |FK(esdu cell)                                          |
+----------------------+-------------------------------------------------------+
| echotype             |FK(Echotype#name)                                      |
+----------------------+-------------------------------------------------------+
| baracoudaCode        |FK(Species#baracoudaCode)                              |
+----------------------+-------------------------------------------------------+
| lengthClass          |(float)                                                |
+----------------------+-------------------------------------------------------+
| sizeCategory         |text                                                   |
+----------------------+-------------------------------------------------------+
| lengthClassMeaning   |text                                                   |
+----------------------+-------------------------------------------------------+
| dataQuality          |FK(DataQuality#qualityDataFlagValues)                  |
+----------------------+-------------------------------------------------------+
| méta-données         |FK(DataMetadata#name) (one column per metadata)        |
+----------------------+-------------------------------------------------------+

Example
-------

::

  name;echotype;baracoudaCode;lengthClass;sizeCategory;Biomass;Abundance;lengthClassMeaning;dataQuality;voyage
  2011-04-26 13:35:19.0000_16;D1;COMP-LEM;10.5;4;5190.7652880528;1297691.3220132;mid point of 1 cm size bin;1;PELGAS2011

Esdu / Species / Age Category
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Required columns 
----------------

::

  name;baracoudaCode;ageCategory;Abundance;ageCategoryMeaning;dataQuality;voyage

Columns formats
---------------

+----------------------+-------------------------------------------------------+
| Column               | Type                                                  |
+======================+=======================================================+
| voyage               |FK(Voyage#name)                                        |
+----------------------+-------------------------------------------------------+
| name                 |FK(esdu cell)                                          |
+----------------------+-------------------------------------------------------+
| baracoudaCode        |FK(Species#baracoudaCode)                              |
+----------------------+-------------------------------------------------------+
| ageCategory          |text                                                   |
+----------------------+-------------------------------------------------------+
| ageCategoryMeaning   |text                                                   |
+----------------------+-------------------------------------------------------+
| dataQuality          |FK(DataQuality#qualityDataFlagValues)                  |
+----------------------+-------------------------------------------------------+
| méta-données         |FK(DataMetadata#name) (one column per metadata)        |
+----------------------+-------------------------------------------------------+

Example
-------

::

  name;baracoudaCode;ageCategory;Abundance;ageCategoryMeaning;dataQuality;voyage
  2011-04-26 13:35:19.0000_16;ENGR-ENC;3;3377.00492644042;fish age-group (years);1;PELGAS2011

Region
======

Region
~~~~~~

Required columns 
----------------

::

  voyage;name;cellType;regionEnvCoordinates;surface;dataQuality

Columns formats
---------------

+-----------------------+------------------------------------------------------+
| Column                | Type                                                 |
+=======================+======================================================+
| voyage                |FK(Voyage#name)                                       |
+-----------------------+------------------------------------------------------+
| cellType              |FK(CellType)                                          |
+-----------------------+------------------------------------------------------+
| dataQuality           |FK(DataQuality#qualityDataFlagValues)                 |
+-----------------------+------------------------------------------------------+
| name                  |text                                                  |
+-----------------------+------------------------------------------------------+
| regionEnvCoordinates  |text                                                  |
+-----------------------+------------------------------------------------------+
| surface               |(float)                                               |
+-----------------------+------------------------------------------------------+

Example
-------

::

  voyage;name;cellType;regionEnvCoordinates;surface;dataQuality
  PELGAS2011;1;RegionCLAS;-1.40644893137726 43.8651260867463 50;1224.75678263326;1

Region Association
~~~~~~~~~~~~~~~~~~

Required columns 
----------------

::

  voyage;regionName;esduName

Columns formats
---------------

+-----------------------+------------------------------------------------------+
| Column                | Type                                                 |
+=======================+======================================================+
| voyage                |FK(Voyage)                                            |
+-----------------------+------------------------------------------------------+
| regionName            |FK(Cell#name)                                         |
+-----------------------+------------------------------------------------------+
| esduName              |FK(Cell#name)                                         |
+-----------------------+------------------------------------------------------+

Important
---------

- You have to sort the line on the regionName.

Example
-------

::

  voyage;regionName;esduName
  PELGAS2011;1;2011-05-02 09:36:54.0000_16

Results per regions
~~~~~~~~~~~~~~~~~~~

Required columns 
----------------

::

  voyage;name;baracoudaCode;sizeCategory;echotype;VarianceXe;VarianceNASC;MeanXe;MeanNASC;NASCWeightedMeanXe;Nesdu;Nhauls;MeanNASCWeightedBiomassDensity;NASCWeightedEstimationVariance;NASCWeightedEstimationCV;MeanBiomassDensity;EstimationVariance;EstimationCV;TotalEstimationVariance;TotalNASCWeightedEstimationVariance;ProportionOfTotalEstimationVariance;ProportionOfNASCWeightedTotalEstimationVariance;Biomass;NASCWeightedBiomass;dataQuality

Columns formats
---------------

+----------------------+-------------------------------------------------------+
| Column               | Type                                                  |
+======================+=======================================================+
| voyage               |FK(Voyage#name)                                        |
+----------------------+-------------------------------------------------------+
| name                 |FK(Cell#name)                                          |
+----------------------+-------------------------------------------------------+
| baracoudaCode        |FK(Species#baracoudaCode)                              |
+----------------------+-------------------------------------------------------+
| echotype             |FK(Echotype#name)                                      |
+----------------------+-------------------------------------------------------+
| sizeCategory         |FK(SizeCategory#name)                                  |
+----------------------+-------------------------------------------------------+
| dataQuality          |FK(DataQuality#qualityDataFlagValues)                  |
+----------------------+-------------------------------------------------------+
| méta-données         |FK(DataMetadata#name) (one column per metadata)        |
+----------------------+-------------------------------------------------------+

Example
-------

::

  voyage;name;baracoudaCode;sizeCategory;echotype;VarianceXe;VarianceNASC;MeanXe;MeanNASC;NASCWeightedMeanXe;Nesdu;Nhauls;MeanNASCWeightedBiomassDensity;NASCWeightedEstimationVariance;NASCWeightedEstimationCV;MeanBiomassDensity;EstimationVariance;EstimationCV;TotalEstimationVariance;TotalNASCWeightedEstimationVariance;ProportionOfTotalEstimationVariance;ProportionOfNASCWeightedTotalEstimationVariance;Biomass;NASCWeightedBiomass;dataQuality
  PELGAS2011;6;CLUP-HAR;0;D2;NA;8963.28501789053;0.127534943481966;19.0945142790179;0.127534943481966;448;1;2.43521779939014;NA;NA;2.43521779939014;NA;NA;0;0;NA;NA;14380.2052341515;14380.2052341515;1

Map
===

Map
~~~

Required columns 
----------------

::

  voyage;name;baracoudaCode;sizeCategory;ageCategory;gridCellLongitude;gridCellLatitude;gridCellDepth;gridLongitudeLag;gridLatitudeLag;gridDepthLag;KrigedXe;dataQuality

Columns formats
---------------

+----------------------+-------------------------------------------------------+
| Column               | Type                                                  |
+======================+=======================================================+
| voyage               |FK(Voyage#name)                                        |
+----------------------+-------------------------------------------------------+
| name                 |text                                                   |
+----------------------+-------------------------------------------------------+
| baracoudaCode        |FK(Species#baracoudaCode)                              |
+----------------------+-------------------------------------------------------+
| sizeCategory         |FK(SizeCategory#name)                                  |
+----------------------+-------------------------------------------------------+
| ageCategory          |FK(AgeCategory#name)                                   |
+----------------------+-------------------------------------------------------+
| dataQuality          |FK(DataQuality#qualityDataFlagValues)                  |
+----------------------+-------------------------------------------------------+
| gridCellLongitude    |(float)                                                |
+----------------------+-------------------------------------------------------+
| gridCellLatitude     |(float)                                                |
+----------------------+-------------------------------------------------------+
| gridCellDepth        |(float)                                                |
+----------------------+-------------------------------------------------------+
| gridLongitudeLag     |(float)                                                |
+----------------------+-------------------------------------------------------+
| gridLatitudeLag      |(float)                                                |
+----------------------+-------------------------------------------------------+
| gridDepthLag         |(float)                                                |
+----------------------+-------------------------------------------------------+
| metadata             |FK(DataMetadata#name) (one column per metadata)        |
+----------------------+-------------------------------------------------------+

Important
---------

- You have to sort the line on the name.

Example
-------

::

  voyage;name;baracoudaCode;sizeCategory;ageCategory;gridCellLongitude;gridCellLatitude;gridCellDepth;gridLongitudeLag;gridLatitudeLag;gridDepthLag;KrigedXe;dataQuality
  PELGAS2011;-6 43.5 0;ENGR-ENC;0;;-6;43.5;0;0.25;0.25;0;0;1

Mooring
=======

Mooring
~~~~~~~

Required columns
----------------

::

  mission;code;description;depth;northLimit;eastLimit;southLimit;upLimit;downLimit;units;zunits;projection;deploymentDate;retrievalDate;siteName;operator;comments

Columns formats
---------------

+-----------------------------+------------------------+
| Colonne                     | Type                   |
+=============================+========================+
| mission                     |FK(Mission#name)        |
+-----------------------------+------------------------+
| code                        |text                    |
+-----------------------------+------------------------+
| description                 |text                    |
+-----------------------------+------------------------+
| depth                       |(float)                 |
+-----------------------------+------------------------+
| northLimit                  |(float)                 |
+-----------------------------+------------------------+
| eastLimit                   |(float)                 |
+-----------------------------+------------------------+
| southLimit                  |(float)                 |
+-----------------------------+------------------------+
| upLimit                     |(float)                 |
+-----------------------------+------------------------+
| downLimit                   |(float)                 |
+-----------------------------+------------------------+
| units                       |text                    |
+-----------------------------+------------------------+
| zunits                      |text                    |
+-----------------------------+------------------------+
| projection                  |text                    |
+-----------------------------+------------------------+
| deploymentDate              |(date)                  |
+-----------------------------+------------------------+
| retrievalDate               |(date)                  |
+-----------------------------+------------------------+
| siteName                    |text                    |
+-----------------------------+------------------------+
| operator                    |text                    |
+-----------------------------+------------------------+
| comments                    |text                    |
+-----------------------------+------------------------+

Example
-------

::

  mission;code;description;depth;northLimit;eastLimit;southLimit;upLimit;downLimit;units;zunits;projection;deploymentDate;retrievalDate;siteName;operator;comments
  PELGAS;MOORING1;Mooring 1;1;1.1;1.2;1.3;1.4;1.5;m;m;None;26-04-2011 08:00:00.0000;04-06-2011 08:00:00.0000;CodeLutin;Moi;RAS

Ancillary instrumentation
~~~~~~~~~~~~~~~~~~~~~~~~~

Required columns
----------------

::

  mooring;ancillaryInstrumentation

Columns formats
---------------

+-----------------------------+----------------------------------+
| Column                      | Type                             |
+=============================+==================================+
| mooring                     |FK(Mooring#code)                  |
+-----------------------------+----------------------------------+
| ancillaryInstrumentation    |FK(AncillaryInstrumentation#name) |
+-----------------------------+----------------------------------+

Example
-------

::

  mooring;ancillaryInstrumentation
  MOORING1;Inst1

Results per mooring
===================

Echotype
~~~~~~~~

Required columns
----------------

::

  mooring;echotypeName;depthStratumId;meaning;baracoudaCode

Columns formats
---------------

+-----------------+--------------------------+
| Column          | Type                     |
+=================+==========================+
| echotypeName    |text                      |
+-----------------+--------------------------+
| meaning         |text                      |
+-----------------+--------------------------+
| mooring         |FK(Mooring#code)          |
+-----------------+--------------------------+
| depthStratumId  |FK(DepthStratum#id)       |
+-----------------+--------------------------+
| baracoudaCode   |FK(Species#baracoudaCode) |
+-----------------+--------------------------+

Example
-------

::

  mooring;echotypeName;depthStratumId;meaning;baracoudaCode
  MOORING1;D1;CLAS;Maquereaux, Chinchards et gadidés présents dans les couches à proximité du fond;COMP-LEM

Results per ESDU for mooring
============================

Esdu / Echotype
~~~~~~~~~~~~~~~

Required columns
----------------

::

  mooring;name;echotype;NASC;ReferenceStationCatch;dataQuality

Columns formats
---------------

+----------------+-------------------------------------------------------------+
| Column         | Type                                                        |
+================+=============================================================+
| mooring        |FK(Mooring#code)                                             |
+----------------+-------------------------------------------------------------+
| name           |FK(esdu cell)                                                |
+----------------+-------------------------------------------------------------+
| echotype       |FK(Echotype#name)                                            |
+----------------+-------------------------------------------------------------+
| dataQuality    |FK(DataQuality#qualityDataFlagValues)                        |
+----------------+-------------------------------------------------------------+
| méta-données   |FK(DataMetadata#name) (une colonne par méta à importer)      |
+----------------+-------------------------------------------------------------+

Example
-------

::

  mooring;name;echotype;NASC;ReferenceStationCatch;dataQuality
  MOORING1;2011-04-26 13:29:12.0000_16;D1;0;P0379;1

Esdu / Echotype / Species Category
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Required columns
----------------

::

  mooring;name;echotype;baracoudaCode;sizeCategory;ReferenceStationCatch;Biomass;MeanLength;MeanWeight;Abundance;SigmaSp;NASC;pondBiomass;pondAbundance;dataQuality

Columns formats
---------------

+----------------+-------------------------------------------------------------+
| Column         | Type                                                        |
+================+=============================================================+
| mooring        |FK(Mooring#code)                                             |
+----------------+-------------------------------------------------------------+
| name           |FK(esdu cell)                                                |
+----------------+-------------------------------------------------------------+
| echotype       |FK(Echotype#name)                                            |
+----------------+-------------------------------------------------------------+
| sizeCategory   |FK(SizeCategory#name)                                        |
+----------------+-------------------------------------------------------------+
| baracoudaCode  |FK(Species#baracoudaCode)                                    |
+----------------+-------------------------------------------------------------+
| dataQuality    |FK(DataQuality#qualityDataFlagValues)                        |
+----------------+-------------------------------------------------------------+
| méta-données   |FK(DataMetadata#name) (une colonne par méta à importer)      |
+----------------+-------------------------------------------------------------+

Example
-------

::

  voyage;name;echotype;baracoudaCode;sizeCategory;ReferenceStationCatch;Biomass;MeanLength;MeanWeight;Abundance;SigmaSp;NASC;pondBiomass;pondAbundance;dataQuality
  MOORING1;2011-04-26 13:29:12.0000_16;D1;COMP-LEM;0;4;0;20;0.05;0;0.00100292822891053;0;0;0;1

