.. -
.. * #%L
.. * EchoBase
.. * %%
.. * Copyright (C) 2011 - 2012 Ifremer, Codelutin
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU Affero General Public License as published by
.. * the Free Software Foundation, either version 3 of the License, or
.. * (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Public License for more details.
.. * 
.. * You should have received a copy of the GNU Affero General Public License
.. * along with this program.  If not, see <http://www.gnu.org/licenses/>.
.. * #L%
.. -

========================
Import / Export database
========================

.. contents:: Index
    :depth: 2

.. sectnum::
    :start: 1
    :depth: 2

Abstract
--------

This document explains how to immport databases in EchoBase format.


Import Referential
------------------

In this mode, only the reference data will be imported.

Select an *.echobase* file with reference data and start the import.

Import Free
-----------

In this mode, all (reference and/or data tables) data will be imported.

Select an *.echobase* file with reference and/or data and start the import.

