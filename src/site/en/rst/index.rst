.. -
.. * #%L
.. * EchoBase
.. * %%
.. * Copyright (C) 2011 Ifremer, Codelutin
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU Affero General Public License as published by
.. * the Free Software Foundation, either version 3 of the License, or
.. * (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Public License for more details.
.. * 
.. * You should have received a copy of the GNU Affero General Public License
.. * along with this program.  If not, see <http://www.gnu.org/licenses/>.
.. * #L%
.. -

========
Echobase
========

Presentation
------------

The French Institute for the Exploitation of the Sea (Ifremer) has designed an open software suite to store fisheries data 
and produce and analyse standard ec(h)osystemic indices and maps for survey-based ecosystem monitoring. It comprises a postgreSQL 
database designed to store metadata, acoustic and fishing data collected during integrated fisheries surveys and on moorings (EchoBase) 
and a suite of R codes (EchoR) to produce and analyse standard ec(h)osystemic indices and maps based on Echobase data. 
This site describes the Echobase database. 

EchoBase is built upon an open source PostgreSQL database, ensuring large storage capacities and easy integration into Geographical Information Systems or dynamic websites. 
It has been designed to store data collected during both acoustic-trawl and bottom trawl vessel-borne surveys, or by moorings. 
The EchoBase data model is based on the `ICES WGFAST`_ Topic Group on metadata standards convention.
It comprises a main « cruise metadata branch » on which are plugged acoustic and fishing (« Operation ») data. 
Biomass estimation results can also be stored in the database at both the acoustic cell (small scale results) and voyage (large scale results)levels. 
The database also allows for the storage of length-weight and Target Strength-length reference relationships, as well as map data at the voyage level.

EchoBase features a Java web-browser interface for selecting working databases, managing users, importing, editing and querying data. 
It also can be easily connected to `LibreOffice base`_, the `R statistical software`_ or Geographic Information Systems such as `QuantumGIS`_ for querying and mapping data. 
Portable H2 databases can be extracted from the server-based EchoBase instance and stored on a local harddrive.

Codes to import/export data from/to EchoBase are available in the `EchoR R package`_.


Documentation
-------------


- How to `install`_ Echobase.

- The Echobase `model`_.

- How to `import data`_ in Echobase.

- How to create a `local Echobase database`_.

.. _EchoR R package: https://gitlab.ifremer.fr/md0276b/echor
.. _ICES WGFAST: http://www.acoustics.washington.edu/FAST
.. _install: ./install.html
.. _model: ./model.html
.. _import data: ./imports.html
.. _local Echobase database: ./embedded.html
.. _R statistical software: https://www.r-project.org/
.. _LibreOffice base: http://www.libreoffice.org/download/libreoffice-fresh/
.. _QuantumGIS: http://www.qgis.org/en/site/
