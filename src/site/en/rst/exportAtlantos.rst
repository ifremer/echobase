.. -
.. * #%L
.. * EchoBase
.. * %%
.. * Copyright (C) 2020 Ifremer, Codelutin
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU Affero General Public License as published by
.. * the Free Software Foundation, either version 3 of the License, or
.. * (at your option) any later version.
.. *
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Public License for more details.
.. *
.. * You should have received a copy of the GNU Affero General Public License
.. * along with this program.  If not, see <http://www.gnu.org/licenses/>.
.. * #L%
.. -

====================
Export to ICES acoustic-trawl database
====================

Abstract
========

This document describes how to export Echobase data in .xml format to the ICES acoustic-trawl database (https://www.ices.dk/data/data-portals/Pages/acoustic.aspx).

Data export
----------------

ICES acoustic-trawl database xml export files (biotic and acoustic) can be generated by clicking on the appropriate menu entry (**ICES export**). You will be
asked to choose the cruise and vessel to export. xml export files will available in a .zip folder after a short period of time. Those xml files can be imported into the ICES acoustic-trawl database. 

Exported species and gears
----------------

Only the species that have the value icesExport to true will be exported during the process.
Only the operations that have a gear with the value icesExport to true will be exported during the process.

Acknowledgements
-------------
This functionnality has been funded by the H2020 project Atlantos (https://www.atlantos-h2020.eu/).
