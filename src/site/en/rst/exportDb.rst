.. -
.. * #%L
.. * EchoBase
.. * %%
.. * Copyright (C) 2011 - 2012 Ifremer, Codelutin
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU Affero General Public License as published by
.. * the Free Software Foundation, either version 3 of the License, or
.. * (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Public License for more details.
.. * 
.. * You should have received a copy of the GNU Affero General Public License
.. * along with this program.  If not, see <http://www.gnu.org/licenses/>.
.. * #L%
.. -

===============
Database export
===============

Abstract
--------

Echobase allows for the export of all or some parts of a working database in a *.echobase* file. This file can therefore be imported in another Echobase instance. 

To export a database in Echobase format:

- In the menu on the left, click on *Export database*.

- Configurate the export

A file in *.echobase* format will be created, that can be imported in an other EchoBase instance.

Several export modes are available:

Export referential
------------------

In this mode, only the reference data of the working database are exported.

Define the name of the export file (the *.echobase* will be automatically added) and click on *Export* to export the reference tables.

Once the reference tables are exported in .echobase format, they can be imported in another Echobase instance.

Export referential and data
---------------------------

In this mode, only reference table and other data of the working database are exported.

Define the name of the export file (the *.echobase* will be automatically added) and click on *Export* to export the reference tables.

Once the reference tables are exported in .echobase format, they can be imported in another Echobase instance.

Export All
----------

In this mode, all the data of the working database are exported.

Define the name of the export file (the *.echobase* will be automatically added) and click on *Export* to export the reference tables.

Once the reference tables are exported in .echobase format, they can be imported in another Echobase instance.
