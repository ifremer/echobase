.. -
.. * #%L
.. * EchoBase
.. * %%
.. * Copyright (C) 2011 - 2012 Ifremer, Codelutin
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU Affero General Public License as published by
.. * the Free Software Foundation, either version 3 of the License, or
.. * (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Public License for more details.
.. * 
.. * You should have received a copy of the GNU Affero General Public License
.. * along with this program.  If not, see <http://www.gnu.org/licenses/>.
.. * #L%
.. -

===============================
Echobase - Databases management
===============================

Database management in EchoBase
-------------------------------

*EchoBase* handles multiple databases, called
*working databases*.

One must connect to a working database before importing or querying data
with the application.

One can parameterize the connection to working databases, which may be of two types:

- a *H2* database
- a *Postgresql* database

Using a H2 database 
-------------------

A H2 database can be created very easily, without maintenance (the database is stored in a single file). A H2 database is however less powerful than e.g. a *Postgresql* database.

In Echobase, a H2 database can be created by defining a new *H2* working database configuration. The new database will be automatically created at the location:

::

  jdbc:h2:file:/pathToNewDatabase/NewDataBaseName

If not mandatory, it is advisable to create a password for the new H2 database, as it can be required by other applications to access the database.

The following credentials are created by default in new H2 databases:

::

  login: sa
  password: sa

Postgresql database creation
----------------------------

Requirements:
 - **postgres** must be installed on the system (by executing e.g.: **sudo apt-get install postgresql** on a Debian system)
 - one must be logged in the postgres application in a console with admistrative rights (e.g. as postgres default user).

1. echobase user creation

::

  createuser -U postgres -sdRP echobase

2. "echobase" database creation

::

  createdb -U postgres -E UTF-8 -O echobase echobase

3. Grant administration rights to echobase user (system dependent, check your system documentation). Completed when one can access the echobase database while typing:

::

   psql -h localhost -U echobase echobase



Using a Postgres database
-------------------------

Postgres databases must be used on a server to safely save large volume of data.

Once the database is created on the server, one shall create a new *Postgresql* working database configuration in *EchoBase*, with an url formatted as:

::

  jdbc:postgresql://serverAdress/DatabaseName

The database login and password shall be provided by the administrator who created the postgresql database on the server.

