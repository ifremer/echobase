.. -
.. * #%L
.. * EchoBase
.. * %%
.. * Copyright (C) 2011 Ifremer, Codelutin
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU Affero General Public License as published by
.. * the Free Software Foundation, either version 3 of the License, or
.. * (at your option) any later version.
.. *
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Public License for more details.
.. *
.. * You should have received a copy of the GNU Affero General Public License
.. * along with this program.  If not, see <http://www.gnu.org/licenses/>.
.. * #L%
.. -

============================
Echobase - Visualisation Gis
============================

Comment ça marche ?
-------------------

La solution technique pour visualiser les données spatialisées dans EchoBase s'appuie sur QGis map server et Lizmap.

Nous expliquons dans cette page comment ça fonctionne.

Organisation physique
~~~~~~~~~~~~~~~~~~~~~

Tous les fichiers utilisés sont placées

::

  /var/local/echobase/
  |-- data
  |   |-- ...
  |   `-- lizmap
  |       |-- resources                   (1. ressources utilisées par le projet QGis et lizmap)
  |       |   |-- France&Spain.shp
  |       |   |-- pelgas09.shp
  |       |   `-- ...
  |       |-- projects                    (2. les projets lizmap generés)
  |       `-- templates                   (3. les templates pour générer les projets lizmap)
  |           |-- QgisTemplate.qgs
  |           |-- LizmapTemplate.qgs.cfg
  |           `-- ...
  |-- echobase.war
  `-- war
      `-- echobase-full-${project.version}.war

Répertoire resources
~~~~~~~~~~~~~~~~~~~~

Ce répertoire contient les différentes ressources utilisés par le projet QGis.

Son contenu est rempli au démarrage de l'application.

Répertoire templates
~~~~~~~~~~~~~~~~~~~~

Ce répertoire contient les templates utilisées pour générer les projets lizmap associés à une base et à une campagne.

Son contenu est rempli au démarrage de l'application.

Plus précisement deux fichiers sont utilisés :

  *  **QgisTemplate.qgs** est la template du project Qgis
  *  **LizmapTemplate.qgs.cfg** est généré par le plugin lizmap de Qgis.

Ces deux fichiers sont paramétrables, chaque paramètre est identifié entre une double accolade (par exemple **{{dbName}}**).

Voici la liste exhaustive des paramètres possibles :

  * *{{dbName}}* : le nom de la base de données,
  * *{{host}}* : l'adresse du serveur de base de données,
  * *{{port}}* : le port d'écoute de la base de données,
  * *{{userName}}* : l'identitfiant de connexion à la base de données,
  * *{{password}}* : le mot de passe de connexion à la base de données,
  * *{{voyageName}}* : le nom de la campagne,
  * *{{voyageId}}* : l'identifiant technique de la campagne,
  * *{{resourcesPath}}* : le chemin du répertoire des ressources.

Répertoire projects
~~~~~~~~~~~~~~~~~~~

Ce répertoire contient les projets lizmap générés par l'application.

Chaque fichier correspond à une campagne d'une base de donné.

Les fichiers sont générés à la demande de l'utilisateur (s'ils n'existent pas) lors d'une demande de visualisation
des données spatialisées d'une campagne.