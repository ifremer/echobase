.. -
.. * #%L
.. * EchoBase
.. * %%
.. * Copyright (C) 2011 - 2012 Ifremer, Codelutin
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU Affero General Public License as published by
.. * the Free Software Foundation, either version 3 of the License, or
.. * (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Public License for more details.
.. * 
.. * You should have received a copy of the GNU Affero General Public License
.. * along with this program.  If not, see <http://www.gnu.org/licenses/>.
.. * #L%
.. -

==========
Data query
==========

Abstract
========

This document describes how to retrieve data in EchoBase.

Creating queries
----------------

Data can be retrieved using SQL queries in the **export data** Echobase menu.

SQL queries can be created and saved in the **export data** page. Results are displayed online in a table that can be exported in .csv format.

One can use nested queries by inserting a saved query name in a new query, e.g.:


::

  SELECT * FROM Voyage where topiaid IN ${QueryName}

Importing queries from LibreOffice Base
---------------------------------------

One can import SQL queries built using the user-friendly LibreOffice Base graphical SQL query builder.
To do so:

- run LibreOffice Base,

- connect to an Echobase working database to display the Echobase data model in the LibreOffice Base graphical SQL query builder,

- define your SQL query and copy its SQL code,

- click "Import from Libre Office" in the Echobase data query menu and paste the SQL code from LibreOffice.

The LibreOffice SQL code is then imported in your EchoBase SQL query.

To use postgresql in Libre Office, you have to install **libreoffice-sdbc-postgresql** package, 
then choose **postgresql** database type for your new database connection.
