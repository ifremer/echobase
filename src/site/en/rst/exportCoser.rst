.. -
.. * #%L
.. * EchoBase
.. * %%
.. * Copyright (C) 2011 - 2014 Ifremer, Codelutin
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU Affero General Public License as published by
.. * the Free Software Foundation, either version 3 of the License, or
.. * (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Public License for more details.
.. * 
.. * You should have received a copy of the GNU Affero General Public License
.. * along with this program.  If not, see <http://www.gnu.org/licenses/>.
.. * #L%
.. -

================================
Export to Indicator site (Coser)
================================

Abstract
========

*TODO Translation me.*

Ce document explique comment exporter les données d'une mission vers le site des indicateurs.

Création de l'archive Coser
---------------------------

Il faut dans un premier temps générer une archive compatible avec le client *Coser*.

Publication sur le site des indicateurs
---------------------------------------

Fournir à un des administrateurs Coser l'archive générée, il devra alors
la décompresser dans le dossier des projets *EchoBase* du Coser puis de
publier le nouveau contenu sur le site des indicateurs depuis l'outil dédié.
