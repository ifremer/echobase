.. -
.. * #%L
.. * EchoBase
.. * %%
.. * Copyright (C) 2011 - 2012 Ifremer, Codelutin
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU Affero General Public License as published by
.. * the Free Software Foundation, either version 3 of the License, or
.. * (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Public License for more details.
.. * 
.. * You should have received a copy of the GNU Affero General Public License
.. * along with this program.  If not, see <http://www.gnu.org/licenses/>.
.. * #L%
.. -

===========
Modify data
===========

Click on the "modify data" icon to access the screen.

Display table data
==================

- Select the table to inspect/modify in the droplist
- Click on a table row to display its content below

Modify or add data in a (reference) table
=========================================

To modify data:

- go to the export table tab to export the table in a text file;

- modify the text file and save it;

- import the modified text file via the "import table" tab.

To add a new row in a table:

- fill in the new row required data fields. DO NOT fill the "topiaId" field;

- Import the modified text file via the "import table" tab.
