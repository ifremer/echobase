.. -
.. * #%L
.. * EchoBase
.. * %%
.. * Copyright (C) 2011 Ifremer, Codelutin
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU Affero General Public License as published by
.. * the Free Software Foundation, either version 3 of the License, or
.. * (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Public License for more details.
.. * 
.. * You should have received a copy of the GNU Affero General Public License
.. * along with this program.  If not, see <http://www.gnu.org/licenses/>.
.. * #L%
.. -

========
Echobase
========

.. contents:: Table des matières
    :depth: 2

.. sectnum::
    :start: 1
    :depth: 2

Création d'une application embarquée
------------------------------------

*TODO Translate me*

L'application permet de créer une application autonome comprenant un jeu de
données sélectionné par l'utilisateur.

Procédure de création
=====================

- Aller dans le menu *Créer une application embarquée*
- Sélectionner la ou les campagnes à inclure (ou rien si vous voulez juste le référentiel)
- Changer si besoin est le nom de l'archive à créer
- Appuyer sur le bouton Créer l'application.

Une fois l'archive créée (l'opération peut être longue selon la quantité de
données), elle est téléchargeable depuis une page de résultat de l'opération
(normalement l'archive se met toute seule en téléchargement).

Architecture de l'application embarquée
=======================================

L'archive une fois décompressé à la forme suivante :

::

  echobase-embedded-0.6-pelgas2010
  ├── db                                    (1) contient la base de données au format h2.
  │   └── echobase.h2.db
  ├── drivers                               (2) contient les drivers jdbc à utiliser dans LibreOffice
  │   ├── h2-1.3.165.jar
  │   └── postgresql-9.1-901-1.jdbc4.jar
  ├── echobase.properties                   (3) Fichier de configuration de l'application embarquée
  ├── echobase-ui-0.6.war                   (4) Application embarquée (war auto-exécutable)
  ├── logs                                  (5) contient les logs de l'application
  │   └── echobase.log
  ├── README.txt
  ├── startEchobase.bat                     (6) Script de démarrage (windows)
  └── startEchobase.sh                      (7) Script de démarrage (linux)

Utilisateurs
============

Deux utilisateurs sont fournis dans la base embarquée :

-  admin / admin (avec des droits d'administration)
-  user / user (pour la consultation)


Utilisation de l'application embarquée (windows)
------------------------------------------------

Pré-requis
==========

Installer un environnement java de développement (une JDK) et pas une JRE
qui ne suffit pas.

`Page de téléchargement Oracle`_

Une fois la JDK téléchargée et installée, ajouter la variable d'environnement
**JDK_HOME** qui est le chemin vers où a été installé Java.

::

  JDK_HOME -> C:/Program Files/Java/jdk1.7.0_03

Installation d'echobase
=======================

- Dézipper l'archive précédemment téléchargée du site central.

- Ouvrir une console (Touche Windows + R , taper *cmd* , Touche Entrée)
- Se placer dans le répertoire où a été dézippée l'archive

::

  cd Chemin vers l'archive dézippée

- Enfin lancer le script

::

  startEchobase.bat

- Dans la console on doit voir l'application démarré et produire des logs
- Lorsque l'application est prête, un navigateur s'ouvre sur la page
  de login.

Utilisation de l'application embarquée (linux)
----------------------------------------------

Pré-requis
==========

Installer un environnement java de développement (une JDK) et pas une JRE
qui ne suffit pas.

`Page de téléchargement Oracle`_

Une fois la JDK téléchargée et installée, ajouter dans les variables
d'environnement :

::

  JDK_HOME -> chemin vers où a été installé la JDK
  
  Pour ajouter une variable d'environnement, écrire dans ton fichier /home/Utilisateur/.bashrc la ligne :

export JDK_HOME=/path/vers/jdk 

Installation d'echobase
=======================

- Dézipper l'archive précédemment téléchargée du site central.
- Ouvrir une console
- Se placer dans le répertoire où a été dézippée l'archive

::

  cd Chemin vers l'archive dézippée

- Rendre le script de démarrage exécutable

::

  chmod +x startEchobase.sh

- Enfin lancer le script

::

  ./startEchobase.sh
  
- Dans la console on doit voir l'application démarré et produire des logs
- Lorsque l'application est prête, un navigateur s'ouvre sur la page
  de login.

.. _Page de téléchargement Oracle: http://www.oracle.com/technetwork/java/javase/downloads/index.html
