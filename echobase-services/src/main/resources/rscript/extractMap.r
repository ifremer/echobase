## To extract maps from EchoR
## Args are
## missionName
## exportPrefixPath
## dbHost
## dbPort
## dbName
## dbUser
## dbPwd

args <- commandArgs(TRUE)
missionName      <- args[1]
exportPrefixPath <- args[2]
dbHost           <- args[3]
dbPort           <- args[4]
dbName           <- args[5]
dbUser           <- args[6]
dbPwd            <- args[7]

library('EchoR');
GridMaps4Echobase(mission=missionName,
                  voyage='%',
                  path.export=exportPrefixPath,
                  host=dbHost,
                  port=dbPort,
                  dbname=dbName,
                  user=dbUser,
                  password=dbPwd,
                  ux11=TRUE);