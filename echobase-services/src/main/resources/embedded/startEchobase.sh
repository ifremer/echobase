#!/bin/sh

# Test if java exists
java -version

if [ ! $? -eq 0 ]; then
  echo "Do not find java, please install java on your computer"
  exit 1
fi

OLDPWD=`pwd`
cd `dirname $0`
CURRENTPWD=`pwd`
ECHOBASE_OPTS="$JAVA_OPTS -Xms512m -Xmx1024m -Dechobase.log.dir=$CURRENTPWD/logs"
java $ECHOBASE_OPTS -jar ${embeddedWarName}.war $*
cd "$OLDPWD"

