/*
 * #%L
 * EchoBase :: Services
 * %%
 * Copyright (C) 2011 - 2012 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ifremer.echobase.services.service.importdata.csv;

import fr.ifremer.echobase.entities.data.Cell;
import fr.ifremer.echobase.entities.data.Voyage;
import fr.ifremer.echobase.services.csv.EchoBaseCsvUtil;
import fr.ifremer.echobase.services.service.importdata.contexts.VoyageResultsImportDataContext;

/**
 * Model to import cells associations from region cells and esdu cells.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 0.3
 */
public class VoyageResultsRegionCellAssociationImportExportModel extends EchoBaseImportExportModelSupport<VoyageResultsRegionCellAssociationImportRow> {

    public static final String HEADER_REGION_NAME = "regionName";
    public static final String HEADER_ESDU_NAME = "esduName";


    private VoyageResultsRegionCellAssociationImportExportModel(char separator) {
        super(separator);
    }

    public static VoyageResultsRegionCellAssociationImportExportModel forImport(VoyageResultsImportDataContext importDataContext) {

        VoyageResultsRegionCellAssociationImportExportModel model = new VoyageResultsRegionCellAssociationImportExportModel(importDataContext.getCsvSeparator());
        model.newForeignKeyColumn(VoyageResultsRegionCellAssociationImportRow.PROPERTY_VOYAGE, Voyage.class, Voyage.PROPERTY_NAME, importDataContext.getVoyagesByName());
        model.newForeignKeyColumn(HEADER_REGION_NAME, VoyageResultsRegionCellAssociationImportRow.PROPERTY_REGION_CELL, Cell.class, Cell.PROPERTY_NAME, importDataContext.getVoyageRegionsByName());
        model.newMandatoryColumn(HEADER_ESDU_NAME, VoyageResultsRegionCellAssociationImportRow.PROPERTY_ESDU_CELL, importDataContext.newCellValueParser());
        return model;

    }

    public static VoyageResultsRegionCellAssociationImportExportModel forExport(VoyageResultsImportDataContext importDataContext) {

        VoyageResultsRegionCellAssociationImportExportModel model = new VoyageResultsRegionCellAssociationImportExportModel(importDataContext.getCsvSeparator());
        model.newColumnForExport(VoyageResultsRegionCellAssociationImportRow.PROPERTY_VOYAGE, EchoBaseCsvUtil.VOYAGE_FORMATTER);
        model.newColumnForExport(HEADER_REGION_NAME, VoyageResultsRegionCellAssociationImportRow.PROPERTY_REGION_CELL, EchoBaseCsvUtil.CELL_FORMATTER);
        model.newColumnForExport(HEADER_ESDU_NAME, VoyageResultsRegionCellAssociationImportRow.PROPERTY_ESDU_CELL, importDataContext.newCellValueFormatter());
        return model;

    }

    @Override
    public VoyageResultsRegionCellAssociationImportRow newEmptyInstance() {
        return new VoyageResultsRegionCellAssociationImportRow();
    }
}
