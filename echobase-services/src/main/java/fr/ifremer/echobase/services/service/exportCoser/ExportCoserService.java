package fr.ifremer.echobase.services.service.exportCoser;

/*
 * #%L
 * EchoBase :: Services
 * %%
 * Copyright (C) 2011 - 2014 Ifremer, Codelutin, Chemit Tony
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.base.Charsets;
import com.google.common.base.Preconditions;
import com.google.common.base.Predicate;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import fr.ifremer.coser.CoserTechnicalException;
import fr.ifremer.coser.bean.EchoBaseProject;
import fr.ifremer.echobase.EchoBaseTechnicalException;
import fr.ifremer.echobase.entities.data.Result;
import fr.ifremer.echobase.entities.data.Voyage;
import fr.ifremer.echobase.entities.references.DataMetadata;
import fr.ifremer.echobase.entities.references.Mission;
import fr.ifremer.echobase.entities.references.SampleDataType;
import fr.ifremer.echobase.entities.references.Species;
import fr.ifremer.echobase.entities.references.Species2;
import fr.ifremer.echobase.io.CommandLineUtils;
import fr.ifremer.echobase.io.EchoBaseIOUtil;
import fr.ifremer.echobase.persistence.JdbcConfiguration;
import fr.ifremer.echobase.services.EchoBaseServiceSupport;
import fr.ifremer.echobase.services.service.UserDbPersistenceService;
import org.apache.commons.exec.CommandLine;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.csv.Export;
import org.nuiton.util.TimeLog;

import javax.inject.Inject;
import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created on 3/1/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.6
 */
public class ExportCoserService extends EchoBaseServiceSupport {

    /** Logger. */
    private static final Log log = LogFactory.getLog(ExportCoserService.class);

    public static final TimeLog timeLog = new TimeLog(ExportCoserService.class);

    public static final String EXTRACT_MAP_R_SCRIPT = "extractMap.r";

    @Inject
    private UserDbPersistenceService persistenceService;

    public void doExport(ExportCoserConfiguration model) throws IOException {

        Preconditions.checkNotNull(model);
        Preconditions.checkNotNull(model.getDbConfiguration());
        Preconditions.checkNotNull(model.getMissionId());
        Preconditions.checkNotNull(model.getPopulationIndicator());
        Preconditions.checkNotNull(model.getCommunityIndicator());

        int nbSteps = 3;

        boolean extractMaps = model.isExtractMap();
        if (extractMaps) {
            nbSteps += 2;
        }

        boolean extractRawData = model.isExtractRawData();
        if (extractRawData) {
            nbSteps += 4;
        }

        boolean extractPopulationIndicators = model.isExtractPopulationIndicator();
        if (extractPopulationIndicators) {
            nbSteps += model.getPopulationIndicator().size() + 1;
        }

        boolean extractCommunityIndicators = model.isExtractCommunityIndicator();
        if (extractCommunityIndicators) {
            nbSteps += model.getCommunityIndicator().size() + 1;
        }

        model.setNbSteps(nbSteps);

        Mission mission = persistenceService.getMission(model.getMissionId());
        Preconditions.checkNotNull(mission);

        File tempDirectory = model.getWorkingDirectory();

        // 1 - create echobase project
        EchoBaseProject project = createProject(tempDirectory,
                                                model,
                                                mission.getName());
        model.incrementsProgress();

        // Extract maps (invoking EchoR)
        if (extractMaps) {

            File echRMapsDir = generateMaps(mission,
                                            tempDirectory,
                                            model.getDbConfiguration());

            model.incrementsProgress();

            copyMaps(project, echRMapsDir);
            model.incrementsProgress();

        }

        // Extract source files
        if (extractRawData) {

            extractRawData(project, mission, model);

        }

        // Extract population indicators
        if (extractPopulationIndicators) {

            extractPopulationIndicators(model, project, mission);

        }

        // Extract community indicators
        if (extractCommunityIndicators) {

            extractCommunityIndicators(model, project, mission);

        }

        // 2 - Export species file
        extractSpeciesFile(project);
        model.incrementsProgress();

        // 3 - create archive
        File zipFile = generateArchive(
                project,
                tempDirectory
        );
        model.setExportFile(zipFile);

        model.incrementsProgress();
    }

    //------------------------------------------------------------------------//
    //--- Create / Zip coser project -----------------------------------------//
    //------------------------------------------------------------------------//

    protected EchoBaseProject createProject(File tempDirectory,
                                            ExportCoserConfiguration model,
                                            String missionName) throws IOException {

        // Get current Year
        Date currentDate = serviceContext.newDate();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(currentDate);
        int currentYear = calendar.get(Calendar.YEAR);

        // create project directory
        File projectDirectory = new File(tempDirectory, missionName + "-" + currentYear);
        EchoBaseIOUtil.forceMkdir(projectDirectory);

        // create project
        EchoBaseProject project = new EchoBaseProject(projectDirectory);

        project.setFacadeName(model.getFacade());
        project.setZoneName(model.getZone());
        project.setSurveyName(missionName);
        project.setAuthor(model.getUserName());
        project.setCreationDate(serviceContext.newDate());
        project.setComment(model.getComment());
        project.setPubliableResult(model.isPublishable());

        project.save();

        if (log.isInfoEnabled()) {
            log.info("Project created at: " + project.getBasedir() + " with name: " + project.getName());
        }
        return project;
    }

    protected File generateArchive(EchoBaseProject project,
                                   File tempDirectory) throws IOException {


        String fileName = project.getZoneName() + "-" + project.getName() + ".zip";
        File zipFile = new File(tempDirectory, fileName);

        EchoBaseIOUtil.compressZipFile(zipFile, project.getBasedir());

        FileUtils.deleteDirectory(project.getBasedir());
        zipFile.deleteOnExit();
        return zipFile;
    }

    //------------------------------------------------------------------------//
    //--- Extract indicators -------------------------------------------------//
    //------------------------------------------------------------------------//

    protected void extractPopulationIndicators(ExportCoserConfiguration model,
                                               EchoBaseProject echoBaseProject,
                                               Mission mission) {

        File exportFile = echoBaseProject.getPopulationIndicatorsFile();
        if (log.isInfoEnabled()) {
            log.info("Generate " + exportFile);
        }

        IndicatorExportModel exportModel = new IndicatorExportModel(getCsvSeparator());

        List<IndicatorExportRow> rows = Lists.newArrayList();

        Predicate<Species> speciesPredicate = Species2.newPopulationIndicatorSpeciesPredicate();

        Set<String> indicators = model.getPopulationIndicator();
        for (String indicator : indicators) {
            loadIndicatorValues(mission, indicator, speciesPredicate, rows);
            model.incrementsProgress();
        }
        Export<IndicatorExportRow> export = Export.newExport(exportModel, rows);
        try {
            export.write(exportFile, Charsets.UTF_8);
        } catch (Exception e) {
            throw new EchoBaseTechnicalException("Can not export population indicators", e);
        }
        model.incrementsProgress();
    }

    protected void extractCommunityIndicators(ExportCoserConfiguration model,
                                              EchoBaseProject echoBaseProject,
                                              Mission mission) {

        File exportFile = echoBaseProject.getCommunityIndicatorsFile();

        if (log.isInfoEnabled()) {
            log.info("Generate " + exportFile);
        }
        IndicatorExportModel exportModel = new IndicatorExportModel(getCsvSeparator());

        List<IndicatorExportRow> rows = Lists.newArrayList();

        Predicate<Species> speciesPredicate = Species2.newCommunityIndicatorSpeciesPredicate();

        Set<String> indicators = model.getCommunityIndicator();
        for (String indicator : indicators) {
            loadIndicatorValues(mission, indicator, speciesPredicate, rows);
            model.incrementsProgress();
        }
        Export<IndicatorExportRow> export = Export.newExport(exportModel, rows);
        try {
            export.write(exportFile, Charsets.UTF_8);
        } catch (Exception e) {
            throw new EchoBaseTechnicalException("Can not export community indicators", e);
        }

        model.incrementsProgress();
    }

    protected void loadIndicatorValues(Mission mission,
                                       String indicator,
                                       Predicate<Species> speciesPredicate,
                                       List<IndicatorExportRow> rows) {

        DataMetadata dataMetadata = persistenceService.getDataMetadata(indicator);
        if (log.isInfoEnabled()) {
            log.info("Extract indicator: " + dataMetadata.getName());
        }

        DataMetadata standardDeviationDataMetadata =
                persistenceService.tryToGetDataMetadataByName(dataMetadata.getName() + "_stdev");

        Map<Species, IndicatorExportRow> rowsBySpecies = Maps.newHashMap();

        List<Result> results = persistenceService.getResultsForMissionAndDatametadata(mission, dataMetadata);

        for (Result result : results) {

            Species species = result.getCategory().getSpeciesCategory().getSpecies();

            if (speciesPredicate.apply(species)) {

                IndicatorExportRow row = rowsBySpecies.get(species);
                if (row == null) {
                    row = new IndicatorExportRow();
                    rowsBySpecies.put(species, row);
                    row.setMission(mission);
                    row.setIndicator(dataMetadata);
                    row.setSpecies(species);
                    row.setStratum("Total");
                    row.setDate(result.getCell().getVoyage().getEndDate());
                    row.setStandardDeviation(0);
                }
                Float resultValue = Float.valueOf(result.getResultValue());
                row.setEstimation(row.getEstimation() + resultValue);

            }

        }

        if (standardDeviationDataMetadata != null) {

            // find standard deviation values

            List<Result> deviationResults = persistenceService.getResultsForMissionAndDatametadata(
                    mission, standardDeviationDataMetadata);

            for (Result result : deviationResults) {

                Species species = result.getCategory().getSpeciesCategory().getSpecies();

                if (speciesPredicate.apply(species)) {

                    IndicatorExportRow row = rowsBySpecies.get(species);
                    if (row == null) {
                        log.warn(String.format("Could not find result for meta %s / species %s",
                                               dataMetadata.getName(),
                                               species.getBaracoudaCode()));
                        continue;
                    }
                    Float resultValue = Float.valueOf(result.getResultValue());
                    row.setStandardDeviation(row.getStandardDeviation() + resultValue);

                }

            }
        }

        rows.addAll(rowsBySpecies.values());
    }

    //------------------------------------------------------------------------//
    //--- Generate Maps ------------------------------------------------------//
    //------------------------------------------------------------------------//

    protected File generateMaps(Mission mission,
                                File tempDirectory,
                                JdbcConfiguration dbConfiguration) throws IOException {

        String missionName = mission.getName();

        File rExecutableFile = getConfiguration().getRscriptExecutablePath();
        if (!EchoBaseIOUtil.isExecutableFile(rExecutableFile)) {
            throw new EchoBaseTechnicalException("Could not find R executable at: " + rExecutableFile);
        }

        if (log.isInfoEnabled()) {
            log.info("Will invoke EchoR on mission: " + missionName);
            log.info("Will connect to db: " + dbConfiguration.getUrl());
        }
        File workingDirectory = new File(tempDirectory, "r");
        EchoBaseIOUtil.forceMkdir(workingDirectory);

        // copy script to temporary directory
        String scriptPrefixPath = new File(workingDirectory, "EchoR").getAbsolutePath();
        File scriptFile = new File(tempDirectory, EXTRACT_MAP_R_SCRIPT);
        EchoBaseIOUtil.copyResource("/rscript/" + EXTRACT_MAP_R_SCRIPT, scriptFile, true);

        // Extract params from jdbcurl
        // example jdbc:postgresql://demo.codelutin.com/echobase-latest-2011-2.6 (port=5432)
        // example jdbc:postgresql://demo.codelutin.com:5433/echobase-latest-2011-2.6
        String url = StringUtils.substringAfter(dbConfiguration.getUrl(), "jdbc:postgresql://");
        if (!url.contains("/")) {
            throw new CoserTechnicalException("Invalid jdbc url: " + url + " (should contains a /");
        }
        String dbHost = StringUtils.substringBefore(url, "/");
        String dbName = StringUtils.substringAfter(url, "/");
        String dbPort = "5432"; // default pg port
        if (dbHost.contains(":")) {
            // a port is given
            dbPort = StringUtils.substringAfter(dbHost, ":");
            dbHost = StringUtils.substringBefore(dbHost, ":");
        }
        if (log.isInfoEnabled()) {
            log.info("mission:     " + missionName);
            log.info("extractPath: " + scriptPrefixPath);
            log.info("dbHost:      " + dbHost);
            log.info("dbPort:      " + dbPort);
            log.info("dbName:      " + dbName);
        }

        CommandLine commandLine = CommandLineUtils.newCommand(
                rExecutableFile,
                scriptFile,
                missionName,
                scriptPrefixPath,
                dbHost,
                dbPort,
                dbName,
                dbConfiguration.getLogin(),
                dbConfiguration.getPassword());

        try {
            CommandLineUtils.invokeCommandeLine(log, commandLine, workingDirectory);
        } catch (IOException e) {

            throw new GenerateCoserMapException("Impossible de générer les cartes en utilisant EchoR", e.getMessage());
        }

        return new File(scriptPrefixPath + "meanMaps");
    }

    protected void copyMaps(EchoBaseProject coserProject, File mapsDirectory) throws IOException {

        File mapDir = coserProject.getMapsDirectory();

        EchoBaseIOUtil.forceMkdir(mapDir);

        FilenameFilter filter =
                EchoBaseProject.newMapSpeciesFilenameFilter(coserProject.getSurveyName());

        // copy all of them in mapDir
        File[] files = mapsDirectory.listFiles(filter);
        if (files != null) {
            for (File file : files) {
                FileUtils.copyFileToDirectory(file, mapDir);
            }
        }
    }

    //------------------------------------------------------------------------//
    //--- Extract Raw Data ---------------------------------------------------//
    //------------------------------------------------------------------------//

    protected void extractRawData(EchoBaseProject project,
                                  Mission mission,
                                  ExportCoserConfiguration model) throws IOException {
        File rawDataDirectory = project.getRawDataDirectory();
        EchoBaseIOUtil.forceMkdir(rawDataDirectory);

        if (log.isInfoEnabled()) {
            log.info("Extract raw data for mission: " + model.getMissionId() +
                     " to : " + rawDataDirectory);
        }

        List<Voyage> voyages = persistenceService.getVoyagesForMission(mission);

        {   // create catch file
            File file = new File(rawDataDirectory, "Captures.csv");
            if (log.isInfoEnabled()) {
                log.info("generate " + file);
            }
            RawDataCatchExportModel exportModel = new RawDataCatchExportModel(getCsvSeparator());

            List<RawDataCatchExportRow> rows = exportModel.prepareRows(voyages);
            try {
                Export.exportToFile(exportModel, rows, file);
            } catch (Exception e) {
                throw new EchoBaseTechnicalException("Could not export catches to " + file, e);
            }
            model.incrementsProgress();
        }

        { // create size file

            SampleDataType weightSampleDataType = persistenceService.getSampleDataTypeByName("LTcm1");
            SampleDataType lengthSampleDataType = persistenceService.getSampleDataTypeByName("WeightAtLengthkg");

            File file = new File(rawDataDirectory, "Tailles.csv");
            if (log.isInfoEnabled()) {
                log.info("generate " + file);
            }
            RawDataSizeExportModel exportModel = new RawDataSizeExportModel(getCsvSeparator());

            List<RawDataSizeExportRow> rows = exportModel.prepareRows(voyages,
                                                                      weightSampleDataType,
                                                                      lengthSampleDataType);
            try {
                Export.exportToFile(exportModel, rows, file);
            } catch (Exception e) {
                throw new EchoBaseTechnicalException("Could not export size to " + file, e);
            }
            model.incrementsProgress();
        }
        { // create strate file

            DataMetadata surfaceMetdata = persistenceService.getDataMetadataByName("Surface");

            File file = new File(rawDataDirectory, "Strates.csv");
            if (log.isInfoEnabled()) {
                log.info("generate " + file);
            }
            RawDataStrataExportModel exportModel = new RawDataStrataExportModel(getCsvSeparator());

            List<RawDataStrataExportRow> rows = exportModel.prepareRows(voyages, surfaceMetdata);
            try {
                Export.exportToFile(exportModel, rows, file);
            } catch (Exception e) {
                throw new EchoBaseTechnicalException("Could not export strata to " + file, e);
            }
            model.incrementsProgress();
        }

        // create fishingOperation file
        // TODO
        model.incrementsProgress();
    }

    //------------------------------------------------------------------------//
    //--- Extract Species File -----------------------------------------------//
    //------------------------------------------------------------------------//

    protected void extractSpeciesFile(EchoBaseProject project) {
        File file = project.getSpeciesDefinitionFile();
        if (log.isInfoEnabled()) {
            log.info("generate " + file);
        }
        SpeciesExportModel exportModel = new SpeciesExportModel(getCsvSeparator());
        List<Species> speciesList = persistenceService.getSpecies();
        List<SpeciesExportRow> rows = exportModel.toRows(speciesList);
        try {
            Export.exportToFile(exportModel, rows, file);
        } catch (Exception e) {
            throw new EchoBaseTechnicalException("Could not export species list to " + file, e);
        }
    }
}
