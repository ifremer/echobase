package fr.ifremer.echobase.services.service.importdata.actions;

/*
 * #%L
 * EchoBase :: Services
 * %%
 * Copyright (C) 2011 - 2016 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.base.Preconditions;
import fr.ifremer.echobase.entities.EchoBaseUserEntityEnum;
import fr.ifremer.echobase.entities.data.Cell;
import fr.ifremer.echobase.entities.data.Voyage;
import fr.ifremer.echobase.io.InputFile;
import fr.ifremer.echobase.services.service.importdata.ImportDataFileResult;
import fr.ifremer.echobase.services.service.importdata.contexts.VoyageResultsImportDataContext;
import fr.ifremer.echobase.services.service.importdata.csv.VoyageResultsRegionCellAssociationImportExportModel;
import fr.ifremer.echobase.services.service.importdata.csv.VoyageResultsRegionCellAssociationImportRow;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.csv.Import;

import java.util.LinkedHashSet;
import java.util.Set;

/**
 * Created on 25/03/16.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 4.0
 */
public class VoyageResultsRegionAssociationImportAction extends VoyageResultsImportDataActionSupport<VoyageResultsRegionCellAssociationImportRow> {

    /** Logger. */
    private static final Log log = LogFactory.getLog(VoyageResultsRegionAssociationImportAction.class);
    private final Set<String> regionCellIds = new LinkedHashSet<>();

    public VoyageResultsRegionAssociationImportAction(VoyageResultsImportDataContext importDataContext) {
        super(importDataContext, importDataContext.getConfiguration().getRegionAssociationFile());
    }

    @Override
    protected VoyageResultsRegionCellAssociationImportExportModel createCsvImportModel(VoyageResultsImportDataContext importDataContext) {
        return VoyageResultsRegionCellAssociationImportExportModel.forImport(importDataContext);
    }

    @Override
    protected VoyageResultsRegionCellAssociationImportExportModel createCsvExportModel(VoyageResultsImportDataContext importDataContext) {
        return VoyageResultsRegionCellAssociationImportExportModel.forExport(importDataContext);
    }

    @Override
    protected void performImport(VoyageResultsImportDataContext importDataContext, InputFile inputFile, ImportDataFileResult result) {
        if (log.isInfoEnabled()) {
            log.info("Starts import of Region cells association from file " + inputFile.getFileName());
        }

        try (Import<VoyageResultsRegionCellAssociationImportRow> importer = open()) {

            incrementsProgress();
            int rowNumber = 0;
            for (VoyageResultsRegionCellAssociationImportRow row : importer) {

                doFlushTransaction(++rowNumber);

                Cell regionCell = row.getRegionCell();
                Cell esduCell = row.getEsduCell();
                regionCell.addChilds(esduCell);

                result.incrementsNumberUpdated(EchoBaseUserEntityEnum.Cell);

                addProcessedRow(result, row);
                regionCellIds.add(regionCell.getTopiaId());
            }
        }
    }

    @Override
    protected void computeImportedExport(VoyageResultsImportDataContext importDataContext, ImportDataFileResult result) {

        String voyageId = importDataContext.getConfiguration().getVoyageId();
        Voyage voyage = persistenceService.getVoyage(voyageId);

        for (String regionCellId : regionCellIds) {

            if (log.isInfoEnabled()) {
                log.info("Treat imported region regionCell: " + regionCellId);
            }
            Cell regionCell = persistenceService.getCell(regionCellId);
            Preconditions.checkNotNull(regionCell);

            for (Cell esduCell : regionCell.getChilds()) {

                VoyageResultsRegionCellAssociationImportRow row = VoyageResultsRegionCellAssociationImportRow.of(voyage, regionCell, esduCell);
                addImportedRow(result, row);

            }

        }

    }

}
