package fr.ifremer.echobase.services.service.removedata;

/*
 * #%L
 * EchoBase :: Services
 * %%
 * Copyright (C) 2011 - 2012 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.base.Optional;
import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import fr.ifremer.echobase.EchoBaseTechnicalException;
import fr.ifremer.echobase.entities.EchoBaseUser;
import fr.ifremer.echobase.entities.ImportFile;
import fr.ifremer.echobase.entities.ImportFileId;
import fr.ifremer.echobase.entities.ImportLog;
import fr.ifremer.echobase.entities.ImportType;
import fr.ifremer.echobase.services.EchoBaseServiceSupport;
import fr.ifremer.echobase.services.service.DecoratorService;
import fr.ifremer.echobase.services.service.UserDbPersistenceService;
import fr.ifremer.echobase.services.service.removedata.strategy.AbstractRemoveDataStrategy;
import fr.ifremer.echobase.services.service.removedata.strategy.AcousticRemoveDataStrategy;
import fr.ifremer.echobase.services.service.removedata.strategy.CatchesRemoveDataStrategy;
import fr.ifremer.echobase.services.service.removedata.strategy.CommonAllRemoveDataStrategy;
import fr.ifremer.echobase.services.service.removedata.strategy.CommonTransectRemoveDataStrategy;
import fr.ifremer.echobase.services.service.removedata.strategy.CommonTransitRemoveDataStrategy;
import fr.ifremer.echobase.services.service.removedata.strategy.CommonVoyageRemoveDataStrategy;
import fr.ifremer.echobase.services.service.removedata.strategy.LegacyVoyageRemoveDataStrategy;
import fr.ifremer.echobase.services.service.removedata.strategy.MooringAcousticRemoveDataStrategy;
import fr.ifremer.echobase.services.service.removedata.strategy.MooringRemoveDataStrategy;
import fr.ifremer.echobase.services.service.removedata.strategy.OperationRemoveDataStrategy;
import fr.ifremer.echobase.services.service.removedata.strategy.ResultEsduRemoveDataStrategy;
import fr.ifremer.echobase.services.service.removedata.strategy.ResultMapFishRemoveDataStrategy;
import fr.ifremer.echobase.services.service.removedata.strategy.ResultMapOtherRemoveDataStrategy;
import fr.ifremer.echobase.services.service.removedata.strategy.ResultRegionRemoveDataStrategy;
import fr.ifremer.echobase.services.service.removedata.strategy.ResultVoyageRemoveDataStrategy;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.decorator.Decorator;
import org.nuiton.topia.persistence.TopiaException;
import org.nuiton.util.TimeLog;

import javax.inject.Inject;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import fr.ifremer.echobase.entities.data.DataAcousticProvider;
import fr.ifremer.echobase.services.service.removedata.strategy.CommonAncillaryInstrumentationRemoveDataStrategy;
import fr.ifremer.echobase.services.service.removedata.strategy.MooringAncillaryInstrumentationRemoveDataStrategy;
import fr.ifremer.echobase.services.service.removedata.strategy.MooringResultEsduRemoveDataStrategy;
import fr.ifremer.echobase.services.service.removedata.strategy.MooringResultsRemoveDataStrategy;

/**
 * Service to remove an import data.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.2
 */
public class RemoveDataService extends EchoBaseServiceSupport {

    /** Logger. */
    private static final Log log = LogFactory.getLog(RemoveDataService.class);

    public static final TimeLog TIME_LOG = new TimeLog(RemoveDataService.class);

    protected static Map<ImportType, Class<? extends AbstractRemoveDataStrategy>> strategies;

    public static final Comparator<ImportLog> IMPORT_LOG_COMPARATOR = new Comparator<ImportLog>() {
        @Override
        public int compare(ImportLog o1, ImportLog o2) {
            String entityId1 = o1.getEntityId();
            String entityId2 = o2.getEntityId();
            
            int result = 0;
            if (entityId1 != null && entityId2 != null) {
                result = entityId1.compareTo(entityId2);
            }
            if (result == 0) {
                result = o1.getImportType().ordinal() - o2.getImportType().ordinal();
            }
            if (result == 0) {
                result = o1.getImportDate().compareTo(o2.getImportDate());
            }
            return result;
        }
    };

    protected static class ImportContext {

        final String importLogId;

        final long nbSteps;

        final AbstractRemoveDataStrategy strategy;

        final DataAcousticProvider provider;

        public ImportContext(String importLogId,
                             long nbSteps,
                             AbstractRemoveDataStrategy strategy,
                             DataAcousticProvider provider) {
            this.importLogId = importLogId;
            this.nbSteps = nbSteps;
            this.strategy = strategy;
            this.provider = provider;
        }
    }

    @Inject
    private UserDbPersistenceService persistenceService;

    @Inject
    private DecoratorService decoratorService;

    public String removeImport(RemoveDataConfiguration model, EchoBaseUser user) {

        // build map of strategy to apply
        // and compute nb steps
        int nbSteps = 0;

        Map<ImportLog, ImportContext> toTreat = Maps.newTreeMap(IMPORT_LOG_COMPARATOR);

        for (String id : model.getImportLogIds()) {
            Optional<ImportLog> optionalImportLog = persistenceService.getOptionalImportLog(id);

            if (optionalImportLog.isPresent()) {
                ImportLog importLog = optionalImportLog.get();
                Class<? extends AbstractRemoveDataStrategy> strategyType = getStrategy(importLog);

                AbstractRemoveDataStrategy strategy = newService(strategyType);
                strategy.setProgressModel(model);

                DataAcousticProvider provider = null;
                String entityId = importLog.getEntityId();
                if (entityId != null) {
                    provider = (DataAcousticProvider) persistenceService.getEntity(entityId);
                }
                
                long l = strategy.computeNbSteps(provider, importLog);
                
                ImportContext importContext = new ImportContext(id, l + 3, strategy, provider);
                toTreat.put(importLog, importContext);
                if (log.isInfoEnabled()) {
                    log.info("Nb steps for importLog " + id + ": " + l);
                }
                nbSteps += importContext.nbSteps;

            }

        }
        model.setNbSteps(nbSteps);

        StringBuilder result = new StringBuilder();
        for (Map.Entry<ImportLog, ImportContext> entry : toTreat.entrySet()) {
            String importResult = removeImport(model, entry.getValue(), user);
            result.append(importResult).append('\n');
        }
        return result.toString().trim();
    }

    public String removeImport(RemoveDataConfiguration model,
                               ImportContext importContext,
                               EchoBaseUser user) {

        // check that importLog still exists
        Optional<ImportLog> optionalImportLog = persistenceService.getOptionalImportLog(importContext.importLogId);

        AbstractRemoveDataStrategy strategy = importContext.strategy;

        if (!optionalImportLog.isPresent()) {

            // import was already removed (from a previously remove I guess)
            long nbStreps = importContext.nbSteps;
            for (long i = 0; i < nbStreps; i++) {
                strategy.incrementOp(
                        "Skip steps (importLog was already removed)");
            }
            return "\n";
        }

        ImportLog importLog = optionalImportLog.get();

        long s0 = TimeLog.getTime();

        try {
            strategy.doRemove(importContext.provider, importLog);
        } catch (TopiaException e) {
            throw new EchoBaseTechnicalException("Could not remove data", e);
        }

        s0 = TIME_LOG.log(s0, "removeData");

        List<ImportLog> removed = removeObsoleteImportLogs(strategy, importLog);
        strategy.incrementOp("Remove import Log " + importLog.getTopiaId());
        s0 = TIME_LOG.log(s0, "removeObsoleteImportLogs");

        // add result in log book and compute resume to show in result
        String result = computeLogBookEntry(importLog, removed, user);
        strategy.incrementOp("Log book entry for " + importLog.getTopiaId());

        // do commit
        persistenceService.commit();
        strategy.incrementOp("Commit importLog " + importLog.getTopiaId());
        TIME_LOG.log(s0, "Commit importLog " + importLog.getTopiaId());
        return result;
    }

    protected List<ImportLog> removeObsoleteImportLogs(AbstractRemoveDataStrategy strategy,
                                                       ImportLog importLog) {

        List<ImportLog> result = Lists.newArrayList();

        Set<ImportType> possibleSubImportType = strategy.getPossibleSubImportType();

        for (ImportLog logEntry : persistenceService.getImportLogs()) {

            if (importLog.equals(logEntry)) {

                // for sure remove me
                // but do not add to result
                persistenceService.deleteImportLog(logEntry);
                continue;
            }

            if (possibleSubImportType.contains(logEntry.getImportType())) {

                // ok can try to remove this import log
                // try to find out if first id is still exists

                String firstId = null;

                if (logEntry.isImportFileNotEmpty()) {

                    ImportFile importFile = logEntry.getImportFile().iterator().next();
                    Iterator<ImportFileId> importFileIdsForImportFile = persistenceService.getImportFileIdsForImportFile(importFile).iterator();

                    if (importFileIdsForImportFile.hasNext()) {
                        firstId = importFileIdsForImportFile.next().getEntityId();
                    }
                }

                if (firstId != null && !persistenceService.isIdExists(firstId)) {

                    // one of id does not exist, can safely remove importLog

                    if (log.isInfoEnabled()) {
                        log.info("Will remove obsolete importLog " +
                                         logEntry.getImportType() + ": " + logEntry.getTopiaId());
                    }

                    persistenceService.deleteImportLog(logEntry);

                    // add it in result
                    result.add(logEntry);
                }
            }
        }
        return result;
    }

    protected Class<? extends AbstractRemoveDataStrategy> getStrategy(ImportLog importLog) {

        if (strategies == null) {
            strategies = Maps.newEnumMap(ImportType.class);
            strategies.put(ImportType.VOYAGE, LegacyVoyageRemoveDataStrategy.class);
            strategies.put(ImportType.COMMON_ALL, CommonAllRemoveDataStrategy.class);
            strategies.put(ImportType.COMMON_VOYAGE, CommonVoyageRemoveDataStrategy.class);
            strategies.put(ImportType.COMMON_TRANSIT, CommonTransitRemoveDataStrategy.class);
            strategies.put(ImportType.COMMON_TRANSECT, CommonTransectRemoveDataStrategy.class);
            strategies.put(ImportType.COMMON_ANCILLARY_INSTRUMENTATION, CommonAncillaryInstrumentationRemoveDataStrategy.class);
            strategies.put(ImportType.OPERATION, OperationRemoveDataStrategy.class);
            strategies.put(ImportType.CATCHES, CatchesRemoveDataStrategy.class);
            strategies.put(ImportType.ACOUSTIC, AcousticRemoveDataStrategy.class);
            strategies.put(ImportType.RESULT_VOYAGE, ResultVoyageRemoveDataStrategy.class);
            strategies.put(ImportType.RESULT_ESDU, ResultEsduRemoveDataStrategy.class);
            strategies.put(ImportType.RESULT_MAP_FISH, ResultMapFishRemoveDataStrategy.class);
            strategies.put(ImportType.RESULT_MAP_OTHER, ResultMapOtherRemoveDataStrategy.class);
            strategies.put(ImportType.RESULT_REGION, ResultRegionRemoveDataStrategy.class);
            strategies.put(ImportType.MOORING_COMMONS, MooringRemoveDataStrategy.class);
            strategies.put(ImportType.MOORING_ANCILLARY_INSTRUMENTATION, MooringAncillaryInstrumentationRemoveDataStrategy.class);
            strategies.put(ImportType.MOORING_ACOUSTIC, MooringAcousticRemoveDataStrategy.class);
            strategies.put(ImportType.RESULT_MOORING, MooringResultsRemoveDataStrategy.class);
            strategies.put(ImportType.RESULT_MOORING_ESDU, MooringResultEsduRemoveDataStrategy.class);
            Preconditions.checkState(
                    ImportType.values().length == strategies.size(),
                    "It miss some remove data strategies...");
        }
        return strategies.get(importLog.getImportType());
    }

    protected String computeLogBookEntry(ImportLog importLog,
                                         List<ImportLog> obsoleteImportLogs,
                                         EchoBaseUser user) {

        Decorator<ImportLog> decorator = decoratorService.getDecorator(ImportLog.class, null);

        StringBuilder buffer = new StringBuilder();
        buffer.append("Suppression import ");
        buffer.append(decorator.toString(importLog));
        for (ImportLog obsoleteImportLog : obsoleteImportLogs) {

            buffer.append("\n- Suppression en cascade de l'import ");
            buffer.append(decorator.toString(obsoleteImportLog));
        }

        String result = buffer.toString();

        if (log.isDebugEnabled()) {
            log.debug("Log text: " + result);
        }
        persistenceService.createEntityModificationLog(
                "Removed Import",
                importLog.getTopiaId(),
                user.getEmail(),
                newDate(),
                result
        );
        return result;
    }
}
