/*
 * #%L
 * EchoBase :: Services
 * %%
 * Copyright (C) 2011 - 2012 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ifremer.echobase.services.service.importdata.csv;

import fr.ifremer.echobase.entities.references.AncillaryInstrumentation;
import fr.ifremer.echobase.entities.references.AncillaryInstrumentationImpl;

import java.io.Serializable;

/**
 * @author Julien Ruchaud - ruchaud@codelutin.com
 * @since 4.0
 */
public abstract class AncillaryInstrumentationImportRow implements Serializable {

    private static final long serialVersionUID = 1L;

    public static final String PROPERTY_ANCILLARY_INSTRUMENTATION = "ancillaryInstrumentation";

    protected AncillaryInstrumentation ancillaryInstrumentation;
    
    public AncillaryInstrumentationImportRow() {
        this(new AncillaryInstrumentationImpl());
    }

    public AncillaryInstrumentationImportRow(AncillaryInstrumentation ancillaryInstrumentation) {
        this.ancillaryInstrumentation = ancillaryInstrumentation;
    }

    public AncillaryInstrumentation getAncillaryInstrumentation() {
        return ancillaryInstrumentation;
    }

    public void setAncillaryInstrumentation(AncillaryInstrumentation ancillaryInstrumentation) {
        this.ancillaryInstrumentation = ancillaryInstrumentation;
    }
}
