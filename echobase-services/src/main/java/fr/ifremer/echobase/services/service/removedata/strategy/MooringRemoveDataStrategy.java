package fr.ifremer.echobase.services.service.removedata.strategy;

/*
 * #%L
 * EchoBase :: Services
 * %%
 * Copyright (C) 2011 - 2012 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.Sets;
import fr.ifremer.echobase.entities.ImportLog;
import fr.ifremer.echobase.entities.ImportType;
import fr.ifremer.echobase.entities.data.Mooring;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.persistence.TopiaException;

import java.util.Set;
import fr.ifremer.echobase.entities.data.DataAcousticProvider;

/**
 * Remove a {@link ImportType#MOORING_COMMONS} import.
 *
 * Can remove only {@link Mooring}.
 * 
 * /!\ The provider is null in this case /!\
 *
 * @author Julien Ruchaud - ruchaud@codelutin.com
 * @since 4.0
 */
public class MooringRemoveDataStrategy extends AbstractRemoveDataStrategy<Mooring> {

    /** Logger. */
    private static final Log log = LogFactory.getLog(MooringRemoveDataStrategy.class);


    @Override
    public long computeNbSteps(DataAcousticProvider<Mooring> provider, ImportLog importLog) {
        long result = getImportFileIdsCount(importLog);
        return result;
    }

    @Override
    protected void removePostData(DataAcousticProvider<Mooring> provider) throws TopiaException {
        // remove orphans cells
        removeOrphanCells();
    }
    
    @Override
    protected void removeImportData(DataAcousticProvider<Mooring> provider, String id) throws TopiaException {

        if (id.startsWith(Mooring.class.getName())) {

            // get entity to delete
            Mooring entity = persistenceService.getMooring(id);

            // delete it
            persistenceService.deleteMooring(entity);

            if (log.isDebugEnabled()) {
                log.debug(entity.getTopiaId() + " was removed");
            }
        } else {
            canNotDealWithId(id);
        }
    }

    @Override
    public Set<ImportType> getPossibleSubImportType() {
        return Sets.newHashSet(ImportType.MOORING_ANCILLARY_INSTRUMENTATION,
                               ImportType.MOORING_ACOUSTIC,
                               ImportType.RESULT_MOORING,
                               ImportType.RESULT_MOORING_ESDU);
    }

}
