package fr.ifremer.echobase.services.service.importdb.strategy;

/*
 * #%L
 * EchoBase :: Services
 * %%
 * Copyright (C) 2011 - 2012 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import fr.ifremer.echobase.entities.data.DataAcousticProvider;
import fr.ifremer.echobase.entities.EchoBaseUser;
import fr.ifremer.echobase.entities.EchoBaseUserEntityEnum;
import fr.ifremer.echobase.entities.EchoBaseUserPersistenceContext;
import fr.ifremer.echobase.io.EchoBaseIOUtil;
import fr.ifremer.echobase.persistence.EchoBaseDbMeta;
import fr.ifremer.echobase.services.EchoBaseServiceSupport;
import fr.ifremer.echobase.services.service.DbEditorService;
import fr.ifremer.echobase.services.service.UserDbPersistenceService;
import fr.ifremer.echobase.services.service.importdata.ImportException;
import fr.ifremer.echobase.services.service.importdb.EchoBaseImportModelFactory;
import fr.ifremer.echobase.services.service.importdb.ImportDbConfiguration;
import org.apache.commons.io.IOUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.persistence.TopiaException;
import org.nuiton.topia.persistence.metadata.AssociationMeta;
import org.nuiton.topia.persistence.metadata.TableMeta;
import org.nuiton.topia.service.csv.in.CsvImportResult;
import org.nuiton.topia.service.csv.in.ImportStrategy;
import org.nuiton.topia.service.csv.in.TopiaCsvImports;
import org.nuiton.util.TimeLog;

import javax.inject.Inject;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import org.nuiton.topia.persistence.TopiaEntity;

/**
 * Abstract import db strategy.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.2
 */
public abstract class AbstractImportDbStrategy extends EchoBaseServiceSupport {

    /** Logger. */
    private static final Log log =
            LogFactory.getLog(AbstractImportDbStrategy.class);

    public static final TimeLog TIME_LOG =
            new TimeLog(AbstractImportDbStrategy.class);

    @Inject
    protected UserDbPersistenceService persistenceService;

    @Inject
    private DbEditorService dbEditorService;

    @Inject
    private EchoBaseUserPersistenceContext persistenceContext;

//    @Override
//    public void setServiceContext(EchoBaseServiceContext serviceContext) {
//        super.setServiceContext(serviceContext);
//        persistenceService = serviceContext.newService(UserDbPersistenceService.class);
//        persistenceContext = serviceContext.getEchoBaseUserPersistenceContext();
//    }

    public final void doImport(ImportDbConfiguration model,
                               EchoBaseUser user) throws IOException, ImportException, TopiaException {
        File file = model.getInput().getFile();

        ZipFile zipFile = new ZipFile(file);
        try {

            List<String> missingEntries = Lists.newArrayList();

            EchoBaseDbMeta dbMeta = getDbMeta();

            Map<TableMeta<EchoBaseUserEntityEnum>, ZipEntry> tables =
                    TopiaCsvImports.discoverEntries(
                            "echobase/", dbMeta.getAllTables(),
                            zipFile, missingEntries);

            validateTableEntries(dbMeta, tables);

            Map<AssociationMeta<EchoBaseUserEntityEnum>, ZipEntry> associations =
                    TopiaCsvImports.discoverEntries(
                            "echobase/", dbMeta.getAllAssociations(),
                            zipFile, missingEntries);

            validateAssociationEntries(dbMeta, associations);

            if (model.isComputeSteps()) {

                long size = EchoBaseIOUtil.countLines(zipFile, tables.values()) +
                            EchoBaseIOUtil.countLines(zipFile, associations.values()) -
                            tables.size() -
                            associations.size();

                if (log.isInfoEnabled()) {
                    log.info("NB Steps: " + size);
                }
                model.setNbSteps(size);
            }

            for (String missingEntry : missingEntries) {
                if (log.isDebugEnabled()) {
                    log.debug("Skip not found entry " + missingEntry);
                }
            }

            ImportStrategy<EchoBaseUserEntityEnum> strategy = new EchoBaseImportStrategy(
                    EchoBaseImportModelFactory.newFactory(dbEditorService),
                    persistenceContext,
                    1000
            );

            List<DataAcousticProvider> importedEntities = importTables(strategy,
                                                            model,
                                                            zipFile,
                                                            tables
            );

            importAssociations(strategy, model, zipFile, associations);

            // add a log entry of import db
            if (log.isInfoEnabled()) {
                log.info("Import done with user " + user.getEmail());
            }

            if (importedEntities != null &&
                !Iterables.isEmpty(importedEntities)) {
                createImportLogEntry(user, file, importedEntities);
            }
            createLogBookEntry(user, file);

            zipFile.close();

            persistenceService.commit();

        } finally {

            IOUtils.closeQuietly(zipFile);
        }
    }


    protected abstract void validateTableEntries(EchoBaseDbMeta dbMeta,
                                                 Map<TableMeta<EchoBaseUserEntityEnum>, ZipEntry> tables) throws ImportException;

    protected abstract void validateAssociationEntries(EchoBaseDbMeta dbMeta,
                                                       Map<AssociationMeta<EchoBaseUserEntityEnum>, ZipEntry> associations) throws ImportException;

    protected abstract void createImportLogEntry(EchoBaseUser user,
                                                 File file,
                                                 List<DataAcousticProvider> importedEntities) throws TopiaException;

    protected abstract void createLogBookEntry(EchoBaseUser user,
                                               File file) throws TopiaException;

    protected List<DataAcousticProvider> importTables(ImportStrategy<EchoBaseUserEntityEnum> strategy,
                                            ImportDbConfiguration model,
                                            ZipFile zipFile,
                                            Map<TableMeta<EchoBaseUserEntityEnum>, ZipEntry> entriestoConsume) throws IOException, TopiaException {

        boolean commitAfterEachFile = model.isCommitAfterEachFile();

        List<DataAcousticProvider> result = new ArrayList<>();

        for (Map.Entry<TableMeta<EchoBaseUserEntityEnum>, ZipEntry> entry :
                entriestoConsume.entrySet()) {

            TableMeta<EchoBaseUserEntityEnum> entryDef = entry.getKey();
            EchoBaseUserEntityEnum source = entryDef.getSource();
            ZipEntry value = entry.getValue();
            
            CsvImportResult<EchoBaseUserEntityEnum> csvResult =
                    CsvImportResult.newResult(source,
                            value.getName(),
                            false,
                            model
                    );

            Reader reader = IOUtils.toBufferedReader(
                    new InputStreamReader(zipFile.getInputStream(value)));
            try {

                long s0 = TimeLog.getTime();

                if (log.isInfoEnabled()) {
                    log.info("Will import " + entryDef);
                }

                if (EchoBaseUserEntityEnum.Voyage == source || EchoBaseUserEntityEnum.Mooring == source) {
                    Iterable<TopiaEntity> importEntities = TopiaCsvImports.importTableAndReturn(reader, strategy, entryDef, csvResult);
                    for (TopiaEntity importEntity : importEntities) {
                        DataAcousticProvider dataAccousticProvider = (DataAcousticProvider) importEntity;
                        result.add(dataAccousticProvider);
                    }

                } else {
                    TopiaCsvImports.importTable(reader,
                                                strategy,
                                                entryDef,
                                                csvResult
                    );
                }

                s0 = TIME_LOG.log(s0, "importFile::done");

                persistenceService.flush();

                TIME_LOG.log(s0, "importFile::flushTransaction");
                reader.close();
            } finally {
                IOUtils.closeQuietly(reader);
                if (commitAfterEachFile) {
                    persistenceService.commit();
                }

                persistenceService.clear();
            }
        }

        return result;
    }

    protected void importAssociations(ImportStrategy<EchoBaseUserEntityEnum> strategy,
                                      ImportDbConfiguration model,
                                      ZipFile zipFile,
                                      Map<AssociationMeta<EchoBaseUserEntityEnum>, ZipEntry> associations) throws IOException, TopiaException {

        for (Map.Entry<AssociationMeta<EchoBaseUserEntityEnum>, ZipEntry> entry :
                associations.entrySet()) {

            AssociationMeta<EchoBaseUserEntityEnum> entryDef = entry.getKey();
            ZipEntry value = entry.getValue();

            CsvImportResult<EchoBaseUserEntityEnum> csvResult =
                    CsvImportResult.newResult(
                            entryDef.getSource(),
                            value.getName(),
                            false,
                            model
                    );

            Reader reader = IOUtils.toBufferedReader(
                    new InputStreamReader(zipFile.getInputStream(value)));
            try {
                long s0 = TimeLog.getTime();

                if (log.isInfoEnabled()) {
                    log.info("Will import " + entryDef);
                }

                TopiaCsvImports.importAssociation(reader,
                                                  strategy,
                                                  entryDef,
                                                  csvResult);

                s0 = TIME_LOG.log(s0, "importFile::done");

                persistenceService.flush();

                TIME_LOG.log(s0, "importFile::flushTransaction");
            } finally {
                reader.close();
            }
        }
    }

}
