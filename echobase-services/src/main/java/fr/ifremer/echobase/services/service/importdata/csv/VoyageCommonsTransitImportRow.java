/*
 * #%L
 * EchoBase :: Services
 * %%
 * Copyright (C) 2011 - 2012 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ifremer.echobase.services.service.importdata.csv;

import fr.ifremer.echobase.entities.data.Transit;
import fr.ifremer.echobase.entities.data.TransitImpl;
import fr.ifremer.echobase.entities.data.Voyage;

import java.util.Date;

/**
 * Bean used as a row for import of {@link VoyageCommonsTransitImportExportModel}.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 0.3
 */
public class VoyageCommonsTransitImportRow {

    public static final String PROPERTY_VOYAGE = "voyage";

    protected final Transit transit;

    protected Voyage voyage;

    public VoyageCommonsTransitImportRow() {
        this(new TransitImpl());
    }

    public VoyageCommonsTransitImportRow(Transit transit) {
        this.transit = transit;
    }

    public Transit getTransit() {
        return transit;
    }

    public Voyage getVoyage() {
        return voyage;
    }

    public void setVoyage(Voyage voyage) {
        this.voyage = voyage;
    }

    public String getDescription() {
        return transit.getDescription();
    }

    public void setDescription(String description) {
        transit.setDescription(description);
    }

    public Date getStartTime() {
        return transit.getStartTime();
    }

    public void setStartTime(Date startTime) {
        transit.setStartTime(startTime);
    }

    public Date getEndTime() {
        return transit.getEndTime();
    }

    public void setEndTime(Date endTime) {
        transit.setEndTime(endTime);
    }

    public String getStartLocality() {
        return transit.getStartLocality();
    }

    public void setStartLocality(String startLocality) {
        transit.setStartLocality(startLocality);
    }

    public String getEndLocality() {
        return transit.getEndLocality();
    }

    public void setEndLocality(String endLocality) {
        transit.setEndLocality(endLocality);
    }

    public static VoyageCommonsTransitImportRow of(Voyage voyage, Transit transit) {
        VoyageCommonsTransitImportRow row = new VoyageCommonsTransitImportRow(transit);
        row.setVoyage(voyage);
        return row;
    }
}
