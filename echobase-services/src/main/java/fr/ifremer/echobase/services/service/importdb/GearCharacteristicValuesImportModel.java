package fr.ifremer.echobase.services.service.importdb;

/*
 * #%L
 * EchoBase :: Services
 * %%
 * Copyright (C) 2011 - 2016 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.echobase.entities.references.Gear;
import fr.ifremer.echobase.entities.references.GearCharacteristic;
import fr.ifremer.echobase.services.service.importdata.csv.EchoBaseImportExportModelSupport;

import java.util.Map;

/**
 * Created on 14/05/16.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 4.0
 */
public class GearCharacteristicValuesImportModel extends EchoBaseImportExportModelSupport<GearCharacteristicValuesImportRow> {

    public static GearCharacteristicValuesImportModel forImport(char separator,
                                                                Map<String, Gear> gearsByCasinoGearName,
                                                                Map<String, GearCharacteristic> gearCharacteristicsByName) {

        GearCharacteristicValuesImportModel model = new GearCharacteristicValuesImportModel(separator);
        model.newForeignKeyColumn(GearCharacteristicValuesImportRow.PROPERTY_GEAR, Gear.class, Gear.PROPERTY_CASINO_GEAR_NAME, gearsByCasinoGearName);
        model.newForeignKeyColumn(GearCharacteristicValuesImportRow.PROPERTY_CHARACTERISTIC, GearCharacteristic.class, GearCharacteristic.PROPERTY_NAME, gearCharacteristicsByName);
        model.newMandatoryColumn(GearCharacteristicValuesImportRow.PROPERTY_DATA_VALUE);

        return model;

    }

    @Override
    public GearCharacteristicValuesImportRow newEmptyInstance() {
        return new GearCharacteristicValuesImportRow();
    }

    protected GearCharacteristicValuesImportModel(char separator) {
        super(separator);
    }

}
