package fr.ifremer.echobase.services.service.importdata.actions;

/*
 * #%L
 * EchoBase :: Services
 * %%
 * Copyright (C) 2011 - 2016 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.echobase.entities.data.DataAcousticProvider;
import fr.ifremer.echobase.entities.data.Transect;
import fr.ifremer.echobase.entities.data.Transit;
import fr.ifremer.echobase.entities.data.Voyage;
import fr.ifremer.echobase.entities.references.Vessel;
import fr.ifremer.echobase.services.service.importdata.TransectNotFoundException;
import fr.ifremer.echobase.services.service.importdata.TransitNotFoundException;
import fr.ifremer.echobase.services.service.importdata.configurations.VoyageAcousticsImportConfiguration;
import fr.ifremer.echobase.services.service.importdata.contexts.VoyageAcousticsImportDataContext;
import fr.ifremer.echobase.services.service.importdata.csv.AcousticImportExportModel;
import fr.ifremer.echobase.services.service.importdata.csv.AcousticImportRow;
import java.util.Date;
import java.util.Locale;
import java.util.Map;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Created on 25/03/16.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 4.0
 */
public class VoyageAcousticsImportAction extends ImportAcousticsActionSupport<VoyageAcousticsImportConfiguration, VoyageAcousticsImportDataContext, AcousticImportRow> {

    /** Logger. */
    private static final Log log = LogFactory.getLog(VoyageAcousticsImportAction.class);

    public VoyageAcousticsImportAction(VoyageAcousticsImportDataContext importDataContext) {
        super(importDataContext);
    }

    @Override
    protected AcousticImportExportModel createCsvImportModel(VoyageAcousticsImportDataContext importDataContext) {
        return AcousticImportExportModel.forImport(importDataContext);
    }

    @Override
    protected AcousticImportExportModel createCsvExportModel(VoyageAcousticsImportDataContext importDataContext) {
        return AcousticImportExportModel.forExport(importDataContext);
    }

    @Override
    protected DataAcousticProvider getDataProvider(VoyageAcousticsImportDataContext importDataContext, AcousticImportRow row, int rowNumber) {
        Locale locale = getLocale();
        
        Voyage voyage = importDataContext.getVoyage();
        Vessel vessel = importDataContext.getVessel();
        
        Date startDate = row.getCellDateStart();
                
        // get transect to use
        Transit transit = voyage.getTransit(startDate);
        if (transit == null) {
            // can not find correct transit
            throw new TransitNotFoundException(locale, voyage, startDate, rowNumber);
        }
        
        Map<Transit, Transect> transects = importDataContext.getTransects();
        Transect transect = transects.get(transit);
        if (transect == null) {
            // can not find correct transect
            throw new TransectNotFoundException(locale, voyage, vessel, startDate, rowNumber);
        }
        //XXX to prevent future lazy initialisation error
        transect.getDataAcquisition();
        return transect;
    }

}
