/*
 * #%L
 * EchoBase :: Services
 * %%
 * Copyright (C) 2011 - 2012 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ifremer.echobase.services.service.importdata.configurations;

import fr.ifremer.echobase.entities.ImportType;
import fr.ifremer.echobase.io.EchoBaseIOUtil;
import fr.ifremer.echobase.io.InputFile;
import fr.ifremer.echobase.services.AbstractEchobaseActionConfiguration;

import java.io.File;

/**
 * Common import data configuration.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 0.3
 */
public abstract class ImportDataConfigurationSupport extends AbstractEchobaseActionConfiguration {

    private static final long serialVersionUID = 1L;

    /** Directory where to store temporary files. */
    protected File workingDirectory;

    /** Notes about this import to add in log book. */
    protected String importNotes;

    /**
     * Import type.
     *
     * This will be saved in the ImportLog table.
     *
     * @since 1.2
     */
    protected ImportType importType;

    public File getWorkingDirectory() {
        return workingDirectory;
    }

    public void setWorkingDirectory(File workingDirectory) {
        this.workingDirectory = workingDirectory;
    }

    public String getImportNotes() {
        return importNotes;
    }

    public void setImportNotes(String importNotes) {
        this.importNotes = importNotes;
    }

    public final ImportType getImportType() {
        return importType;
    }

    public final void setImportType(ImportType importType) {
        this.importType = importType;
    }

    /** @return all the input files defined for the import (some can be empty). */
    public abstract InputFile[] getInputFiles();

    /** @return the number of lines to imports from all the import files. */
    public long computeNbSteps() {
        long result = 0;
        for (InputFile inputFile : getInputFiles()) {

            if (inputFile.hasFile()) {
                int currentLines = EchoBaseIOUtil.countLines(
                        inputFile.getFile());
                result += currentLines;
            }
        }
        setNbSteps(result);
        return result;
    }

}
