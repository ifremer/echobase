/*
 * #%L
 * EchoBase :: Services
 * %%
 * Copyright (C) 2011 - 2012 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ifremer.echobase.services.service.importdata.csv;

import fr.ifremer.echobase.entities.data.Operation;
import fr.ifremer.echobase.entities.references.DepthStratum;
import fr.ifremer.echobase.entities.references.Gear;
import fr.ifremer.echobase.entities.references.Vessel;
import fr.ifremer.echobase.services.csv.EchoBaseCsvUtil;
import fr.ifremer.echobase.services.service.importdata.contexts.VoyageOperationsImportDataContext;

/**
 * Model to import {@link Operation}.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 0.3
 */
public class VoyageOperationsOperationImportExportModel extends EchoBaseImportExportModelSupport<VoyageOperationsOperationImportRow> {

    private VoyageOperationsOperationImportExportModel(char separator) {
        super(separator);
    }

    public static VoyageOperationsOperationImportExportModel forImport(VoyageOperationsImportDataContext importDataContext) {

        VoyageOperationsOperationImportExportModel model = new VoyageOperationsOperationImportExportModel(importDataContext.getCsvSeparator());
        model.newForeignKeyColumn(EchoBaseCsvUtil.VESSEL_NAME, VoyageOperationsOperationImportRow.PROPERTY_VESSEL, Vessel.class, Vessel.PROPERTY_NAME, importDataContext.getVesselsByName());
        model.newForeignKeyColumn(EchoBaseCsvUtil.GEAR_CODE, Operation.PROPERTY_GEAR, Gear.class, Gear.PROPERTY_CASINO_GEAR_NAME, importDataContext.getGearsByCasinoGearName());
        model.newForeignKeyColumn(EchoBaseCsvUtil.DEPTH_STRATUM_ID, Operation.PROPERTY_DEPTH_STRATUM, DepthStratum.class, DepthStratum.PROPERTY_ID, importDataContext.getDepthStratumsById());
        model.newMandatoryColumn(EchoBaseCsvUtil.OPERATION_ID, Operation.PROPERTY_ID);
        model.newMandatoryColumn(Operation.PROPERTY_MID_HAUL_LATITUDE, EchoBaseCsvUtil.PRIMITIVE_FLOAT);
        model.newMandatoryColumn(Operation.PROPERTY_MID_HAUL_LONGITUDE, EchoBaseCsvUtil.PRIMITIVE_FLOAT);
        model.newMandatoryColumn(Operation.PROPERTY_GEAR_SHOOTING_START_TIME, EchoBaseCsvUtil.IMPORT_DAY_TIME_ECHOBASE);
        model.newMandatoryColumn(Operation.PROPERTY_GEAR_SHOOTING_END_TIME, EchoBaseCsvUtil.IMPORT_DAY_TIME_ECHOBASE);
        model.newMandatoryColumn(Operation.PROPERTY_GEAR_SHOOTING_START_LATITUDE, EchoBaseCsvUtil.PRIMITIVE_FLOAT);
        model.newMandatoryColumn(Operation.PROPERTY_GEAR_HAULING_END_LATITUDE, EchoBaseCsvUtil.PRIMITIVE_FLOAT);
        model.newMandatoryColumn(Operation.PROPERTY_GEAR_SHOOTING_START_LONGITUDE, EchoBaseCsvUtil.PRIMITIVE_FLOAT);
        model.newMandatoryColumn(Operation.PROPERTY_GEAR_HAULING_END_LONGITUDE, EchoBaseCsvUtil.PRIMITIVE_FLOAT);
        return model;

    }

    public static VoyageOperationsOperationImportExportModel forExport(VoyageOperationsImportDataContext importDataContext) {

        VoyageOperationsOperationImportExportModel model = new VoyageOperationsOperationImportExportModel(importDataContext.getCsvSeparator());
        model.newColumnForExport(EchoBaseCsvUtil.VESSEL_NAME, VoyageOperationsOperationImportRow.PROPERTY_VESSEL, EchoBaseCsvUtil.VESSEL_FORMATTER);
        model.newColumnForExport(EchoBaseCsvUtil.GEAR_CODE, Operation.PROPERTY_GEAR, EchoBaseCsvUtil.GEAR_FORMATTER);
        model.newColumnForExport(EchoBaseCsvUtil.DEPTH_STRATUM_ID, Operation.PROPERTY_DEPTH_STRATUM, EchoBaseCsvUtil.DEPTH_STRATUM_FORMATTER);
        model.newColumnForExport(EchoBaseCsvUtil.OPERATION_ID, Operation.PROPERTY_ID);
        model.newColumnForExport(Operation.PROPERTY_MID_HAUL_LATITUDE, EchoBaseCsvUtil.PRIMITIVE_FLOAT);
        model.newColumnForExport(Operation.PROPERTY_MID_HAUL_LONGITUDE, EchoBaseCsvUtil.PRIMITIVE_FLOAT);
        model.newColumnForExport(Operation.PROPERTY_GEAR_SHOOTING_START_TIME, EchoBaseCsvUtil.IMPORT_DAY_TIME_ECHOBASE);
        model.newColumnForExport(Operation.PROPERTY_GEAR_SHOOTING_END_TIME, EchoBaseCsvUtil.IMPORT_DAY_TIME_ECHOBASE);
        model.newColumnForExport(Operation.PROPERTY_GEAR_SHOOTING_START_LATITUDE, EchoBaseCsvUtil.PRIMITIVE_FLOAT);
        model.newColumnForExport(Operation.PROPERTY_GEAR_HAULING_END_LATITUDE, EchoBaseCsvUtil.PRIMITIVE_FLOAT);
        model.newColumnForExport(Operation.PROPERTY_GEAR_SHOOTING_START_LONGITUDE, EchoBaseCsvUtil.PRIMITIVE_FLOAT);
        model.newColumnForExport(Operation.PROPERTY_GEAR_HAULING_END_LONGITUDE, EchoBaseCsvUtil.PRIMITIVE_FLOAT);
        return model;

    }

    @Override
    public VoyageOperationsOperationImportRow newEmptyInstance() {
        return new VoyageOperationsOperationImportRow();
    }
}
