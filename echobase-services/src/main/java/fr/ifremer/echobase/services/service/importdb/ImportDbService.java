/*
 * #%L
 * EchoBase :: Services
 * %%
 * Copyright (C) 2011 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ifremer.echobase.services.service.importdb;

import fr.ifremer.echobase.EchoBaseTechnicalException;
import fr.ifremer.echobase.entities.EchoBaseUser;
import fr.ifremer.echobase.services.EchoBaseServiceSupport;
import fr.ifremer.echobase.services.service.importdata.ImportException;
import fr.ifremer.echobase.services.service.importdb.strategy.AbstractImportDbStrategy;
import fr.ifremer.echobase.services.service.importdb.strategy.FreeImportDbStrategy;
import fr.ifremer.echobase.services.service.importdb.strategy.ReferentialImportDbStrategy;
import org.nuiton.topia.persistence.TopiaException;

import java.io.IOException;

/**
 * Service to import a complete db.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 0.2
 */
public class ImportDbService extends EchoBaseServiceSupport {

    public void doImport(ImportDbConfiguration model, EchoBaseUser user) throws ImportException {

        ImportDbMode importDbMode = model.getImportDbMode();

        AbstractImportDbStrategy strategy;
        switch (importDbMode) {

            case REFERENTIAL:
                strategy = newService(ReferentialImportDbStrategy.class);
                break;
            case FREE:
                strategy = newService(FreeImportDbStrategy.class);
                break;
            default:
                throw new EchoBaseTechnicalException(
                        "Can not deal with this importDbMode: " + importDbMode);
        }

        try {
            strategy.doImport(model, user);
        } catch (IOException | TopiaException e) {
            throw new ImportException("Could not import db", e);
        }

    }

}
