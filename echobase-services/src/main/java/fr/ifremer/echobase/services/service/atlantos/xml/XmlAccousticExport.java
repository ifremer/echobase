package fr.ifremer.echobase.services.service.atlantos.xml;

import com.google.common.base.Function;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;
import fr.ifremer.echobase.entities.data.*;
import fr.ifremer.echobase.entities.references.AcousticInstrument;
import fr.ifremer.echobase.entities.references.Calibration;
import fr.ifremer.echobase.entities.references.Vessel;
import fr.ifremer.echobase.services.EchoBaseService;
import fr.ifremer.echobase.services.csv.EchoBaseCsvUtil;
import java.io.IOException;
import java.util.*;
import java.util.function.Predicate;
import javax.inject.Inject;
import org.apache.commons.lang3.StringUtils;
import org.nuiton.topia.persistence.TopiaEntity;

/*
 * #%L
 * EchoBase :: Services
 * %%
 * Copyright (C) 2011 - 2014 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

/**
 * @author Julien Ruchaud - ruchaud@codelutin.com
 * @since 4
 */
public class XmlAccousticExport implements EchoBaseService {
    
    @Inject
    protected VocabularyExport vocabulary;
    
    public static String getFormatedTopiaId(TopiaEntity entity) {
        return entity.getTopiaId().replaceAll("#", "_");
    }
    
    public void doExport(Voyage voyage, Vessel vessel, XmlWriter xmlHead, XmlWriter xmlVoca, XmlWriter xmlCruise) throws IOException {
        boolean exportCruiseDone = false;
        vocabulary.init(xmlVoca);
        
        // EXPORT ACCOUSTIC
        xmlHead.append("<?xml version=\"1.0\"?>\n");
        xmlHead.open("Acoustic", 
                 "xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance",
                 "xmlns:xsd", "http://www.w3.org/2001/XMLSchema");
        
        Collection<Transit> transits = voyage.getTransit();
        for (Transit transit : transits) {
            
            Collection<Transect> transects = transit.getTransect();
            for (Transect transect : transects) {
                if (vessel.equals(transect.getVessel())) {
                    
                    Collection<DataAcquisition> dataAcquisitions = transect.getDataAcquisition();
                    for (DataAcquisition dataAcquisition : dataAcquisitions) {

                        // EXPORT INSTRUMENT
                        AcousticInstrument acousticInstrument = dataAcquisition.getAcousticInstrument();
                        this.exportInstrument(acousticInstrument, xmlHead);

                        // EXPORT CALIBRATION
                        Collection<Calibration> calibrations = acousticInstrument.getCalibration();
                        Date startDate = voyage.getStartDate();
                        Date endDate = voyage.getEndDate();
                        for (Calibration calibration : calibrations) {
                            Date date = calibration.getDate();
                            if (date != null && date.after(startDate) && date.before(endDate)) {
                                this.exportCalibration(calibration, xmlHead);
                                break;
                            }
                        }

                        // EXPORT DATA ACQUISITION
                        exportDataAcquisition(dataAcquisition, xmlHead);

                        Collection<DataProcessing> dataProcessings = dataAcquisition.getDataProcessing();
                        for (DataProcessing dataProcessing : dataProcessings) {

                            // EXPORT DATA PROCESSING
                            exportDataProcessing(dataProcessing,
                                                 dataAcquisition.getTransceiverAcquisitionPower(),
                                                 dataAcquisition.getTransceiverAcquisitionPulseLength(),
                                                 xmlHead);

                            // EXPORT CRUISE
                            if (!exportCruiseDone) {
                                exportCruise(voyage, vessel, transect, xmlCruise);
                                exportCruiseDone = true;
                            }

                            Collection<Cell> cells = dataProcessing.getCell();
                            List<Cell> orderingCells = new ArrayList<>(cells);
                            orderingCells.removeIf(new Predicate<Cell>() {
                                @Override
                                public boolean test(Cell cell) {
                                    return !cell.getCellType().getId().equals("Esdu");
                                }
                            });

                            orderingCells.sort(new Comparator<Cell>() {
                                @Override
                                public int compare(Cell o1, Cell o2) {
                                    return o1.getName().compareTo(o2.getName());
                                }
                            });

                            int position = 0;
                            for (Cell cell : orderingCells) {

                                // EXPORT LOG
                                exportLog(cell, position++, xmlCruise);

                                List<Result> resultsWithoutD4 = new ArrayList<Result>();
                                Result resultForD4 = null;

                                Collection<Result> results = cell.getResult();
                                for (Result result : results) {
                                    if ("NASC".equals(result.getDataMetadata().getName())) {

                                        if (result.getCategory().getEchotype().getName().equals("D4")) {
                                            //Add category to vocabulary (to get speciesCategories in echotype vocabulary export)
                                            vocabulary.addCategory(result.getCategory());
                                            resultForD4 = result;
                                        } else {
                                            //Add category to vocabulary (to get speciesCategories in echotype vocabulary export)
                                            vocabulary.addCategory(result.getCategory());
                                            //Export only samples without speciesCategory
                                            if (result.getCategory().getSpeciesCategory() == null) {
                                                resultsWithoutD4.add(result);
                                            }
                                        }
                                    }
                                }

                                // EXPORT SAMPLE
                                if (resultForD4 != null && exportSample(voyage, cell, resultForD4, xmlCruise)) {
                                    // EXPORT DATA
                                    exportData(voyage, resultForD4, xmlCruise);
                                    xmlCruise.close("Sample");
                                }

                                if (!resultsWithoutD4.isEmpty()) {
                                    // EXPORT SAMPLE
                                    exportSample(voyage, cell, null, xmlCruise);

                                    for (Result result : resultsWithoutD4) {
                                        // EXPORT DATA
                                        exportData(voyage, result, xmlCruise);
                                    }

                                    xmlCruise.close("Sample");
                                }

                                xmlCruise.close("Log");
                            }
                        }
                    } 
                }
            }
        }

        if (exportCruiseDone) {
            xmlCruise.close("Cruise");
        }
        
        xmlCruise.close("Acoustic");
        vocabulary.generate();
    }
    
    public void exportInstrument(AcousticInstrument instrument, XmlWriter xml) throws IOException {
        xml.open("Instrument", 
                 "ID", XmlAccousticExport.getFormatedTopiaId(instrument));
        
        xml.create("Frequency",                             
                Integer.parseInt(instrument.getFrequency())/1000);
        
        xml.create("TransducerLocation",                    
                "IDREF", vocabulary.getVocabularyCode(instrument.getTransducerLocation(), "AC_TransducerLocation_AA"));
        
        xml.create("TransducerManufacturer",                
                instrument.getTransducerBeamManufactuer());
        xml.create("TransducerModel",                       
                instrument.getTransducerModel());
        xml.create("TransducerSerial",                      
                instrument.getTransducerSerial());
        xml.create("TransducerBeamType",                    
                "IDREF", vocabulary.getVocabularyCode(instrument.getTransducerAperture(), "AC_TransducerBeamType_S2"));
        xml.create("TransducerDepth",                       
                instrument.getTransducerDepth());
        
        Float azimuth = instrument.getTransducerAzimuth();
        float elevation = instrument.getTransducerElevation();
        xml.create("TransducerOrientation",
                "elevation " + elevation + "° azimuth " + (azimuth == null ? "90°" : azimuth + "°") + " rotation 0°");

        xml.create("TransducerPSI",                         
                instrument.getTransducerPsi());
        xml.create("TransducerBeamAngleMajor",              
                instrument.getTransducerBeamAngleMajor());
        xml.create("TransducerBeamAngleMinor",              
                instrument.getTransducerBeamAngleMinor());
        xml.create("TransceiverManufacturer",               
                instrument.getTransceiverManufacturer());
        xml.create("TransceiverModel",                      
                instrument.getTransceiverModel());
        xml.create("TransceiverSerial",                     
                instrument.getTransceiverSerial());
        xml.create("TransceiverFirmware",                   
                instrument.getTransceiverFirmware());
        xml.create("Comments",                              
                instrument.getComments());
        
        xml.close("Instrument");
    }
    
    public void exportCalibration(Calibration calibration, XmlWriter xml) throws IOException {
        xml.open("Calibration", 
                 "ID", XmlAccousticExport.getFormatedTopiaId(calibration));
        
        xml.create("Date",                                  
                EchoBaseCsvUtil.ISO8611_DATE_FORMATTER.format(calibration.getDate()));
        xml.create("AcquisitionMethod",                     
                "IDREF", vocabulary.getVocabularyCode(calibration.getAquisitionMethod(), "AC_AcquisitionMethod_SS"));
        vocabulary.getVocabularyCode("AC_AcquisitionMethod_SS");
        xml.create("ProcessingMethod",                      
                "IDREF", vocabulary.getVocabularyCode("Method_" + calibration.getProcessingMethod(), "AC_ProcessingMethod_ER60"));
        xml.create("AccuracyEstimate",                      
                calibration.getAccuracyEstimate());
        xml.create("Report",                                
                calibration.getReport());
        xml.create("Comments",                              
                calibration.getComments());
        
        xml.close("Calibration");
    }
    
    public void exportDataAcquisition(DataAcquisition dataAcquisition, XmlWriter xml) throws IOException {
        xml.open("DataAcquisition", 
                 "ID", XmlAccousticExport.getFormatedTopiaId(dataAcquisition));
        
        xml.create("SoftwareName",                          
                "IDREF", vocabulary.getVocabularyCode("Software_" + dataAcquisition.getSoftwareName(), "AC_DataAcquisitionSoftwareName_ER60"));
        xml.create("SoftwareVersion",
                dataAcquisition.getAcquisitionSoftwareVersion());
        xml.create("StoredDataFormat",                      
                "IDREF", vocabulary.getVocabularyCode(dataAcquisition.getLoggedDataFormat(), "AC_StoredDataFormat_HAC"));
        xml.create("PingDutyCycle",                         
                dataAcquisition.getPingDutyCycle());
        xml.create("Comments",                              
                dataAcquisition.getComments());
        
        xml.close("DataAcquisition");
    }
    
    public void exportDataProcessing(DataProcessing dataProcessing, float transceiverPower, float transmitPulseLength, XmlWriter xml) throws IOException {
        xml.open("DataProcessing", 
                 "ID", XmlAccousticExport.getFormatedTopiaId(dataProcessing));
        
        xml.create("SoftwareName",
                "IDREF", vocabulary.getVocabularyCode(dataProcessing.getSoftwareName(), "AC_DataProcessingSoftwareName_test1"));
        xml.create("SoftwareVersion",                       
                dataProcessing.getProcessingSoftwareVersion());
        xml.create("TriwaveCorrection", "IDREF",            
                vocabulary.getVocabularyCode("AC_TriwaveCorrection_NA"));
        xml.create("ChannelID",                             
                dataProcessing.getChannelId());
        xml.create("Bandwidth",                             
                dataProcessing.getBandwidth());
        xml.create("Frequency",                             
                dataProcessing.getFrequency());
        xml.create("TransceiverPower", transceiverPower);
        xml.create("TransmitPulseLength", transmitPulseLength);
        xml.create("OnAxisGain",                            
                dataProcessing.getTransceiverProcessingGain());
        xml.create("OnAxisGainUnit", "IDREF",               
                vocabulary.getVocabularyCode("AC_OnAxisGainUnit_dB"));
        xml.create("SaCorrection",                          
                dataProcessing.getTransceiverProcessingSacorrection());
        xml.create("Absorption",                            
                dataProcessing.getTransceiverProcessingAbsorption());
        xml.create("AbsorptionDescription",                 
                dataProcessing.getTransceiverProcessingAbsorptionDescription());
        xml.create("SoundSpeed",                            
                dataProcessing.getEchosounderSoundSpeed());
        xml.create("SoundSpeedDescription",                 
                dataProcessing.getSoundSpeedCalculations());
        xml.create("TransducerPSI",                         
                dataProcessing.getTransducerProcessingPsi());
        xml.create("Comments",                              
                dataProcessing.getComments());
        
        xml.close("DataProcessing");
    }

    public void exportCruise(Voyage voyage, Vessel vessel, Transect transect, XmlWriter xml) throws IOException {
        xml.open("Cruise");

        xml.open("Survey");
        xml.create("Code",
                "IDREF", vocabulary.getVocabularyCode(voyage.getMission().getName(), "AC_Survey_PELGAS"));
        xml.close("Survey");
        xml.create("Country", 
                "IDREF", vocabulary.getVocabularyCode(voyage.getMission().getCountry(), "ISO_3166_FR"));
        xml.create("Platform", 
                "IDREF", vocabulary.getVocabularyCode(vessel.getCode(), "SHIPC_35HT"));
        xml.create("StartDate",
                EchoBaseCsvUtil.ISO8611_DATE_FORMATTER.format(voyage.getStartDate()));
        xml.create("EndDate",
                EchoBaseCsvUtil.ISO8611_DATE_FORMATTER.format(voyage.getEndDate()));
        xml.create("Organisation", 
                "IDREF", vocabulary.getVocabularyCode(voyage.getMission().getInstitution(), "EDMO_541"));
        xml.create("LocalID",
                voyage.getName());
    }
    
    public void exportLog(Cell cell, int position, XmlWriter xml) throws IOException {
        xml.open("Log");
        
        Collection<Data> datas = cell.getData();
        ImmutableMap<String, Data> dataValues = Maps.uniqueIndex(datas, new Function<Data, String>() {
            @Override
            public String apply(Data value) {
                return value.getDataMetadata().getName();
            }
        });

        //Convention il all app, always take End as reference by default
        //If no End information present, then take start
        String prefix = "End";
        if (!dataValues.containsKey("LatitudeStart")) {
            prefix = "Start";
        }
        
        Data time = dataValues.get("Time" + prefix);
        Data latitude = dataValues.get("LatitudeStart");
        Data longitude = dataValues.get("LongitudeStart");
        
        xml.create("Distance", position);
        xml.create("Time",
                time != null ? StringUtils.substring(time.getDataValue(), 0, -8) : "");
        xml.create("Latitude",
                latitude != null ? latitude.getDataValue() : 0);
        xml.create("Longitude",
                longitude != null ? longitude.getDataValue() : 0);
        xml.create("Origin", 
                "IDREF", vocabulary.getVocabularyCode("AC_LogOrigin_" + prefix));
        xml.create("Validity",
                "IDREF", vocabulary.getVocabularyCode("AC_LogValidity_V"));
        //xml.create("BottomDepth");
    }
    
    public boolean exportSample(Voyage voyage, Cell cell, Result result, XmlWriter xml) throws IOException {
        
        // Utiliser la date de début du voyage
        Collection<Calibration> calibrations = cell.getDataProcessing().getDataAcquisition().getAcousticInstrument().getCalibration();
        Date startDate = voyage.getStartDate();
        Date endDate = voyage.getEndDate();
        Calibration calibrationFound = null;
        for (Calibration calibration : calibrations) {
            Date date = calibration.getDate();
            if (date != null && date.after(startDate) && date.before(endDate)) {
                calibrationFound = calibration;
                break;
            }
        }
        
        Collection<Data> datas = cell.getData();
        ImmutableMap<String, Data> dataValues = Maps.uniqueIndex(datas, new Function<Data, String>() {
            @Override
            public String apply(Data value) {
                return value.getDataMetadata().getName();
            }
        });
        
        String prefix = "start";
        if (!dataValues.containsKey("LatitudeStart")) {
            prefix = "end";
        }

        Float upperDepth = 10f;
        Float lowerDepth = 30f;
        
        Data depthData = dataValues.get("ESDUstartDepth");
        Float depth = depthData != null ? Float.parseFloat(depthData.getDataValue()) : 0f;
        
        if (result == null) {
            lowerDepth = depth;
            
            if (depth >= 50) {
                upperDepth = 30f;
            }
        } else if (depth <= 50) {
            return false;
        }
        
//        Float upperDepth = 10f;
//        Data depthData = dataValues.get("ESDUstartDepth");
//        Float lowerDepth = depthData != null ? Float.parseFloat(depthData.getDataValue()) : 0f;
        
        if (upperDepth >= lowerDepth) {
            upperDepth = 0f;
        }
        
        xml.open("Sample");

        xml.create("ChannelDepthUpper",
                upperDepth.intValue());
        xml.create("ChannelDepthLower",
                lowerDepth.intValue());
        
        xml.create("PingAxisInterval",
                1);
        xml.create("PingAxisIntervalType", 
                "IDREF", vocabulary.getVocabularyCode("AC_PingAxisIntervalType_distance"));
        xml.create("PingAxisIntervalUnit", 
                "IDREF", vocabulary.getVocabularyCode("AC_PingAxisIntervalUnit_nmi"));
        xml.create("SvThreshold",
                cell.getDataProcessing().geteIThresholdLow());
        
        xml.create("Instrument", 
                "IDREF", XmlAccousticExport.getFormatedTopiaId(cell.getDataProcessing().getDataAcquisition().getAcousticInstrument()));
        if (calibrationFound != null){
            xml.create("Calibration",
                    "IDREF", XmlAccousticExport.getFormatedTopiaId(calibrationFound));
        } else {
            xml.create("Calibration");
        }
        xml.create("DataAcquisition",
                "IDREF", XmlAccousticExport.getFormatedTopiaId(cell.getDataProcessing().getDataAcquisition()));
        xml.create("DataProcessing", 
                "IDREF", XmlAccousticExport.getFormatedTopiaId(cell.getDataProcessing()));
        xml.create("PingAxisIntervalOrigin", 
                "IDREF", vocabulary.getVocabularyCode("AC_PingAxisIntervalOrigin_" + prefix));
        
        return true;
    }
    
    public void exportData(Voyage voyage, Result result, XmlWriter xml) throws IOException {
        xml.open("Data");
        xml.create("EchoType",
                "IDREF", result.getCategory().getEchotype().getName());
        xml.create("Type",
                "IDREF", vocabulary.getVocabularyCode("AC_AcousticDataType_C"));
        xml.create("Unit",
                "IDREF", vocabulary.getVocabularyCode("AC_DataUnit_m2nmi-2"));

        xml.create("Value", result.getResultValue().equals("NA")?0:result.getResultValue());

        xml.close("Data");
    }    
}
