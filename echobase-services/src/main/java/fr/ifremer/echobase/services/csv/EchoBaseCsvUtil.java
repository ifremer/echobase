/*
 * #%L
 * EchoBase :: Domain
 * %%
 * Copyright (C) 2011 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ifremer.echobase.services.csv;

import com.google.common.base.Function;
import fr.ifremer.echobase.entities.data.Cell;
import fr.ifremer.echobase.entities.data.Echotype;
import fr.ifremer.echobase.entities.data.Mooring;
import fr.ifremer.echobase.entities.data.Operation;
import fr.ifremer.echobase.entities.data.Result;
import fr.ifremer.echobase.entities.data.Voyage;
import fr.ifremer.echobase.entities.references.AcousticInstrument;
import fr.ifremer.echobase.entities.references.AgeCategory;
import fr.ifremer.echobase.entities.references.AncillaryInstrumentation;
import fr.ifremer.echobase.entities.references.CellType;
import fr.ifremer.echobase.entities.references.DataMetadata;
import fr.ifremer.echobase.entities.references.DataQuality;
import fr.ifremer.echobase.entities.references.DepthStratum;
import fr.ifremer.echobase.entities.references.Gear;
import fr.ifremer.echobase.entities.references.GearMetadata;
import fr.ifremer.echobase.entities.references.Mission;
import fr.ifremer.echobase.entities.references.OperationMetadata;
import fr.ifremer.echobase.entities.references.Port;
import fr.ifremer.echobase.entities.references.SampleDataType;
import fr.ifremer.echobase.entities.references.SexCategory;
import fr.ifremer.echobase.entities.references.SizeCategory;
import fr.ifremer.echobase.entities.references.Species;
import fr.ifremer.echobase.entities.references.Strata;
import fr.ifremer.echobase.entities.references.Vessel;
import org.nuiton.csv.ValueFormatter;
import org.nuiton.csv.ValueGetter;
import org.nuiton.csv.ValueParser;
import org.nuiton.csv.ValueParserFormatter;
import org.nuiton.csv.ValueSetter;
import org.nuiton.topia.service.csv.TopiaCsvCommons;

import java.util.Arrays;
import java.util.Date;
import java.util.Map;
import java.util.Set;

/**
 * Usefull class to build csv import-export models.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 0.2
 */
public class EchoBaseCsvUtil extends TopiaCsvCommons {

    public static final String CELLULE_DATE_FORMAT = "yyyy-MM-dd HH:mm:ss.SSSS";
    public static final String ISO8611_DATE_FORMAT = "YYYY-MM-dd";
    public static final String ISO8611_DATETIME_FORMAT = "YYYY-MM-dd HH:mm";

    public static final ValueParserFormatter<Date> IMPORT_DAY_TIME_ECHOBASE = new DateValue(CELLULE_DATE_FORMAT);
    public static final ValueParserFormatter<Date> ISO8611_DATE_FORMATTER = new DateValue(ISO8611_DATE_FORMAT);
    public static final ValueParserFormatter<Date> ISO8611_DATETIME_FORMATTER = new DateValue(ISO8611_DATETIME_FORMAT);

//    public static final ValueParser<Date> IMPORT_DAY_TIME_ECHOBASE2 = new DateValue("dd/MM/yyyy HH:mm:ss.SSSS");

    public static final ValueFormatter<Species> SPECIES_TO_COSER_CODE = new ValueFormatter<Species>() {
        @Override
        public String format(Species value) {
            return value.getBaracoudaCode().replaceAll("-", "");
        }
    };

    public static final ValueFormatter<DataQuality> DATA_QUALITY_FORMATTER = newValueFormatterByFunction(new Function<DataQuality, String>() {
        @Override
        public String apply(DataQuality input) {
            return "" + (input == null ? "" : input.getQualityDataFlagValues());
        }
    });

    public static final ValueFormatter<AcousticInstrument> ACOUSTIC_INSTRUMENT_FORMATTER = newValueFormatterByFunction(
            new Function<AcousticInstrument, String>() {

                @Override
                public String apply(AcousticInstrument input) {
                    return input.getId();
                }
            }
    );
    public static final ValueFormatter<Voyage> VOYAGE_FORMATTER = newValueFormatterByFunction(
            new Function<Voyage, String>() {

                @Override
                public String apply(Voyage input) {
                    return input.getName();
                }
            }
    );
    public static final ValueFormatter<Vessel> VESSEL_FORMATTER = newValueFormatterByFunction(
            new Function<Vessel, String>() {

                @Override
                public String apply(Vessel input) {
                    return input.getName();
                }
            }
    );
    public static final ValueFormatter<Gear> GEAR_FORMATTER = newValueFormatterByFunction(
            new Function<Gear, String>() {

                @Override
                public String apply(Gear input) {
                    return input.getCasinoGearName();
                }
            }
    );
    public static final ValueFormatter<GearMetadata> GEAR_METADATA_FORMATTER = newValueFormatterByFunction(
            new Function<GearMetadata, String>() {

                @Override
                public String apply(GearMetadata input) {
                    return input.getName();
                }
            }
    );
    public static final ValueFormatter<DepthStratum> DEPTH_STRATUM_FORMATTER = newValueFormatterByFunction(
            new Function<DepthStratum, String>() {

                @Override
                public String apply(DepthStratum input) {
                    return input.getId();
                }
            }
    );
    public static final ValueFormatter<Species> SPECIES_FORMATTER = newValueFormatterByFunction(
            new Function<Species, String>() {

                @Override
                public String apply(Species input) {
                    return input.getBaracoudaCode();
                }
            }
    );
    public static final ValueFormatter<SampleDataType> SAMPLE_DATA_TYPE_FORMATTER = newValueFormatterByFunction(
            new Function<SampleDataType, String>() {

                @Override
                public String apply(SampleDataType input) {
                    return input.getName();
                }
            }
    );
    public static final ValueFormatter<Strata> STRATA_FORMATTER = newValueFormatterByFunction(
            new Function<Strata, String>() {

                @Override
                public String apply(Strata input) {
                    return input.getName();
                }
            }
    );
    public static final ValueFormatter<Operation> OPERATION_FORMATTER = newValueFormatterByFunction(
            new Function<Operation, String>() {

                @Override
                public String apply(Operation input) {
                    return input.getId();
                }
            }
    );
    public static final ValueFormatter<OperationMetadata> OPERATION_FMETADATA_ORMATTER = newValueFormatterByFunction(
            new Function<OperationMetadata, String>() {

                @Override
                public String apply(OperationMetadata input) {
                    return input.getName();
                }
            }
    );
    public static final ValueFormatter<SizeCategory> SIZE_CATEGORY_FORMATTER = newValueFormatterByFunction(
            new Function<SizeCategory, String>() {

                @Override
                public String apply(SizeCategory input) {
                    return input.getName();
                }
            }
    );
    public static final ValueFormatter<SexCategory> SEX_CATEGORY_FORMATTER = newValueFormatterByFunction(
            new Function<SexCategory, String>() {

                @Override
                public String apply(SexCategory input) {
                    return input.getName();
                }
            }
    );
    public static final ValueFormatter<AgeCategory> AGE_CATEGORY_FORMATTER = newValueFormatterByFunction(
            new Function<AgeCategory, String>() {

                @Override
                public String apply(AgeCategory input) {
                    return input == null ? "" : input.getName();
                }
            }
    );
    public static final ValueFormatter<Cell> CELL_FORMATTER = newValueFormatterByFunction(
            new Function<Cell, String>() {

                @Override
                public String apply(Cell input) {
                    return input.getName();
                }
            }
    );
    public static final ValueFormatter<Echotype> ECHOTYPE_FORMATTER = newValueFormatterByFunction(
            new Function<Echotype, String>() {

                @Override
                public String apply(Echotype input) {
                    return input == null ? "" : input.getName();
                }
            }
    );
    public static final ValueFormatter<CellType> CELL_TYPE_FORMATTER = newValueFormatterByFunction(
            new Function<CellType, String>() {

                @Override
                public String apply(CellType input) {
                    return input.getId();
                }
            }
    );
    public static final ValueFormatter<Port> PORT_FORMATTER = newValueFormatterByFunction(
            new Function<Port, String>() {

                @Override
                public String apply(Port input) {
                    return input.getCode();
                }
            }
    );

    public static final ValueFormatter<Mission> MISSION_FORMATTER = newValueFormatterByFunction(
            new Function<Mission, String>() {
                @Override
                public String apply(Mission input) {
                    return input.getName();
                }
            }
    );
    
    public static final ValueFormatter<Mooring> MOORING_FORMATTER = newValueFormatterByFunction(
            new Function<Mooring, String>() {

                @Override
                public String apply(Mooring input) {
                    return input.getCode();
                }
            }
    );
    
    public static final ValueFormatter<AncillaryInstrumentation> ANCILLARY_INSTRUMENTATION_FORMATTER = newValueFormatterByFunction(
            new Function<AncillaryInstrumentation, String>() {

                @Override
                public String apply(AncillaryInstrumentation input) {
                    return input.getName();
                }
            }
    );

    public static <E> ValueFormatter<E> newValueFormatterByFunction(Function<E, String> function) {
        return new ValueFormatterByFunction<>(function);
    }

    public static ValueParser<Cell> newCellValueParser(Map<String, Cell> esduCellMap) {
        return new CellValueParser(esduCellMap);
    }

    public static ValueFormatter<Cell> newCellValueFormatter(Set<Cell> esduCells, CellType esduCellType, CellType elementaryCellType) {
        return new CellValueFormatter(esduCells, esduCellType, elementaryCellType);
    }

    public static <B extends ResultAble> ValueSetter<B, Result> newResultValueSetter() {
        return new ResultValueSetter<>();
    }

    public static <B extends ResultAble> ValueGetter<B, Result> newResultValueGetter(DataMetadata metadata) {
        return new ResultValueGetter<>(metadata);
    }

    public static ValueParser<Result> newResultValueParser(DataMetadata metadata, boolean useFillValue) {
        return new ResultValueParser(metadata, useFillValue);
    }

    public static ValueFormatter<Result> newResultValueFormatter(DataMetadata metadata, boolean useFillValue) {
        return new ResultValueFormatter(metadata, useFillValue);
    }


//    public static final ValueParser<Boolean> INT_TO_BOOLEAN_PARSER = new ValueParser<Boolean>() {
//
//        @Override
//        public Boolean parse(String value) {
//            return "1".equals(value);
//        }
//    };

    public static final ValueParserFormatter<Float> NA_TO_FLOAT_PARSER_FORMATTER = new FloatParserFormatter(null, true) {

        @Override
        public String format(Float value) {
            return value == null ? NA : super.format(value);
        }

        @Override
        protected Float parseNoneEmptyValue(String value) {
            Float result = null;
            if (!NA.equals(value)) {
                result = super.parseNoneEmptyValue(value);
            }
            return result;
        }

    };

//    public static final ValueParser<Float> NA_TO_PRIMITIVE_FLOAT_PARSER = new FloatParserFormatter(0f, false) {
//
//        @Override
//        protected Float parseNoneEmptyValue(String value) {
//            Float result;
//            if (!NA.equals(value)) {
//                result = super.parseNoneEmptyValue(value);
//            } else {
//                result = defaultValue;
//            }
//            return result;
//        }
//
//    };

    public static final ValueParserFormatter<Integer> NA_TO_INTEGER_PARSER_FORMATTER = new IntegerParserFormatter(null, true) {

        @Override
        public String format(Integer value) {
            return value == null ? NA : super.format(value);
        }

        @Override
        protected Integer parseNoneEmptyValue(String value) {
            Integer result = null;
            if (!NA.equals(value)) {
                result = super.parseNoneEmptyValue(value);
            }
            return result;
        }

    };

    public static final ValueParserFormatter<String> NA_TO_STRING_PARSER_FORMATTER = new NullableParserFormatter<String>(null, true) {

        @Override
        public String format(String value) {
            return value == null ? NA : value;
        }

        @Override
        protected String parseNoneEmptyValue(String value) {
            String result = null;
            if (!NA.equals(value)) {
                result = value;
            }
            return result;
        }

    };

    public static final ValueParser<Date> DATE_TIME_VALUE_PARSER = new ChainValueParser<>(Arrays.asList(TopiaCsvCommons.DAY_TIME_SECOND_WITH_TIMESTAMP,
                                                                                                                           TopiaCsvCommons.DAY_TIME_SECOND,
                                                                                                                           new DateValue("yyyy-MM-dd HH:mm:ss"),
                                                                                                                           TopiaCsvCommons.DAY_TIME,
                                                                                                                           TopiaCsvCommons.DAY));

    public static final String CELL_NAME = "name";

    public static final String OPERATION_ID = "operationId";

    public static final String VESSEL_NAME = "vesselName";
    
    public static final String PORT_CODE = "portCode";

    public static final String GEAR_CODE = "gearCode";

    public static final String DEPTH_STRATUM_ID = "depthStratumId";

    public static final String NA = "NA";

    protected EchoBaseCsvUtil() {
        // avoid instanciation on helper class
    }

}
