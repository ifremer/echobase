package fr.ifremer.echobase.services.service.importdata.actions;

/*
 * #%L
 * EchoBase :: Services
 * %%
 * Copyright (C) 2011 - 2016 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;
import fr.ifremer.echobase.entities.data.Operation;
import fr.ifremer.echobase.entities.data.Sample;
import fr.ifremer.echobase.entities.data.SampleData;
import fr.ifremer.echobase.entities.data.SampleDatas;
import fr.ifremer.echobase.entities.references.SampleDataType;
import fr.ifremer.echobase.entities.references.SampleType;
import fr.ifremer.echobase.entities.references.SizeCategories;
import fr.ifremer.echobase.entities.references.SizeCategory;
import fr.ifremer.echobase.entities.references.Species;
import fr.ifremer.echobase.entities.references.SpeciesCategory;
import fr.ifremer.echobase.io.InputFile;
import fr.ifremer.echobase.services.service.importdata.DuplicatedSampleException;
import fr.ifremer.echobase.services.service.importdata.ImportDataFileResult;
import fr.ifremer.echobase.services.service.importdata.SpeciesCategoryCache;
import fr.ifremer.echobase.services.service.importdata.contexts.VoyageCatchesImportDataContext;
import fr.ifremer.echobase.services.service.importdata.csv.VoyageCatchesTotalSampleImportExportModel;
import fr.ifremer.echobase.services.service.importdata.csv.VoyageCatchesTotalSampleImportRow;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.csv.Import;

import java.util.Collection;

/**
 * Created on 25/03/16.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 4.0
 */
public class VoyageCatchesTotalSampleImportAction extends VoyageCatchesImportDataActionSupport<VoyageCatchesTotalSampleImportRow> {

    /** Logger. */
    private static final Log log = LogFactory.getLog(VoyageCatchesTotalSampleImportAction.class);


    public VoyageCatchesTotalSampleImportAction(VoyageCatchesImportDataContext importDataContext) {
        super(importDataContext, importDataContext.getConfiguration().getTotalSampleFile());
    }

    @Override
    protected VoyageCatchesTotalSampleImportExportModel createCsvImportModel(VoyageCatchesImportDataContext importDataContext) {
        return VoyageCatchesTotalSampleImportExportModel.forImport(importDataContext);
    }

    @Override
    protected VoyageCatchesTotalSampleImportExportModel createCsvExportModel(VoyageCatchesImportDataContext importDataContext) {
        return VoyageCatchesTotalSampleImportExportModel.forExport(importDataContext);
    }

    @Override
    protected void performImport(VoyageCatchesImportDataContext importDataContext, InputFile inputFile, ImportDataFileResult result) {

        if (log.isInfoEnabled()) {
            log.info("Starts import of totalSample from file " + inputFile.getFileName());
        }

        SpeciesCategoryCache speciesCategoryCache = importDataContext.getSpeciesCategoryCache();

        SampleType sampleTypeTotal = importDataContext.getSampleTypeTotal();
        SampleType sampleTypeUnsorted = importDataContext.getSampleTypeUnsorted();
        SampleType sampleTypeSorted = importDataContext.getSampleTypeSorted();

        SampleDataType sampleDataTypeMeanLength = importDataContext.getSampleDataTypeMeanLength();
        SampleDataType sampleDataTypeMeanWeight = importDataContext.getSampleDataTypeMeanWeight();
        SampleDataType sampleDataTypeNoPerKg = importDataContext.getSampleDataTypeNoPerKg();

        try (Import<VoyageCatchesTotalSampleImportRow> importer = open()) {

            int rowNumber = 0;
            incrementsProgress();
            for (VoyageCatchesTotalSampleImportRow row : importer) {

                doFlushTransaction(++rowNumber);
                Operation operation = row.getOperation();

                Species species = row.getSpecies();
                SizeCategory sizeCategory = row.getSizeCategory();

                SpeciesCategory category = speciesCategoryCache.getSpeciesCategory(species, null, sizeCategory, null, null, result);

                SampleType sampleType;

                if (SizeCategories.IS_HORS_VRAC.apply(sizeCategory)) {

                    // hors vrac case
                    sampleType = sampleTypeUnsorted;

                } else {
                    // none hors vrac case
                    sampleType = sampleTypeTotal;
                }

                Sample sample = operation.getSample(category, sampleType);

                if (sample != null) {

                    // can not have twice same sample
                    throw new DuplicatedSampleException(getLocale(), rowNumber, operation, sampleType, species, sizeCategory);

                }

                // must create it

                sample = row.getSample();
                sample.setSpeciesCategory(category);
                sample.setSampleType(sampleType);
                sample = addSample(operation, sample, result, rowNumber);
                addProcessedRow(result, row);

                // create datas

                if (row.getMeanLength() != null) {

                    //create meanLength data
                    addSampleData(sampleDataTypeMeanLength, null, row.getMeanLength(), sample, result, false, rowNumber);
                }

                if (row.getMeanWeight() != null) {

                    //create meanWeight data
                    addSampleData(sampleDataTypeMeanWeight, null, row.getMeanWeight(), sample, result, false, rowNumber);
                }

                if (row.getNoPerKg() != null) {

                    //create noPerKg data
                    addSampleData(sampleDataTypeNoPerKg, null, row.getNoPerKg(), sample, result, false, rowNumber);
                }

                Sample createdSortedSample = persistenceService.newSample();
                createdSortedSample.setSampleType(sampleTypeSorted);
                createdSortedSample.setSampleWeight(row.getSortedWeight());

                // create sorted sample
                addSample(operation, createdSortedSample, result, rowNumber);

            }

        }

    }

    @Override
    protected void computeImportedExport(VoyageCatchesImportDataContext importDataContext, ImportDataFileResult result) {

        SampleType sampleTypeSorted = importDataContext.getSampleTypeSorted();

        SampleDataType sampleDataTypeMeanLength = importDataContext.getSampleDataTypeMeanLength();
        SampleDataType sampleDataTypeMeanWeight = importDataContext.getSampleDataTypeMeanWeight();
        SampleDataType sampleDataTypeNoPerKg = importDataContext.getSampleDataTypeNoPerKg();

        Sample sample = null;

        for (Sample currentSample : getImportedEntities(Sample.class, result)) {

            boolean currentSampleIsSorted = sampleTypeSorted.equals(currentSample.getSampleType());

            if (currentSampleIsSorted) {

                // flush samples

                Preconditions.checkState(sample != null);
                Preconditions.checkState(currentSample.isSampleDataEmpty());

                VoyageCatchesTotalSampleImportRow importedRow = VoyageCatchesTotalSampleImportRow.of(sample.getOperation(), sample.getSpeciesCategory(), sample, currentSample.getSampleWeight());

                if (sample.isSampleDataNotEmpty()) {

                    Collection<SampleData> sampleData = sample.getSampleData();
                    ImmutableMap<SampleDataType, SampleData> sampleDataByType = Maps.uniqueIndex(sampleData, SampleDatas.TO_SAMPLE_DATA_TYPE);

                    {
                        SampleData sampleData1 = sampleDataByType.get(sampleDataTypeMeanLength);
                        if (sampleData1 != null) {
                            importedRow.setMeanLength(sampleData1.getDataValue());
                        }
                    }
                    {
                        SampleData sampleData1 = sampleDataByType.get(sampleDataTypeMeanWeight);
                        if (sampleData1 != null) {
                            importedRow.setMeanWeight(sampleData1.getDataValue());
                        }
                    }
                    {
                        SampleData sampleData1 = sampleDataByType.get(sampleDataTypeNoPerKg);
                        if (sampleData1 != null) {
                            importedRow.setNoPerKg(sampleData1.getDataValue());
                        }
                    }
                }

                addImportedRow(result, importedRow);

                continue;

            }

            sample = currentSample;

        }

    }

}
