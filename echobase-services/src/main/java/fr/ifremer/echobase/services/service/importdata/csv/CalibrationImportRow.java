/*
 * #%L
 * EchoBase :: Services
 * %%
 * Copyright (C) 2011 - 2019 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ifremer.echobase.services.service.importdata.csv;

import fr.ifremer.echobase.entities.references.AcousticInstrument;
import fr.ifremer.echobase.entities.references.Calibration;

import java.util.Date;

/**
 * Bean of a row for {@link CalibrationImportExportModel} import.
 *
 * @author Jean Couteau - couteau@codelutin.com
 * @since 4.2
 */
public class CalibrationImportRow {

    public static final String PROPERTY_ACOUSTIC_INSTRUMENT = "acousticInstrument";
    public static final String PROPERTY_ACQUISITION_METHOD = "acquisitionMethod";
    public static final String PROPERTY_COMMENTS = "comments";
    public static final String PROPERTY_DATE = "date";
    public static final String PROPERTY_PROCESSING_METHOD = "processingMethod";
    public static final String PROPERTY_REPORT = "report";
    public static final String PROPERTY_ACCURACY_ESTIMATE = "accuracyEstimate";

    protected String accuracyEstimate;

    protected String acquisitionMethod;

    protected String comments;

    protected Date date;

    protected String processingMethod;

    protected String report;

    protected AcousticInstrument acousticInstrument;

    public static CalibrationImportRow of(Calibration calibration) {
        CalibrationImportRow row = new CalibrationImportRow();
        row.setAcousticInstrument(calibration.getAcousticInstrument());
        row.setAccuracyEstimate(calibration.getAccuracyEstimate());
        row.setAcquisitionMethod(calibration.getAquisitionMethod());
        row.setComments(calibration.getComments());
        row.setDate(calibration.getDate());
        row.setProcessingMethod(calibration.getProcessingMethod());
        row.setReport(calibration.getReport());
        return row;
    }

    public String getAccuracyEstimate() {
        return accuracyEstimate;
    }

    public void setAccuracyEstimate(String accuracyEstimate) {
        this.accuracyEstimate = accuracyEstimate;
    }

    public String getAcquisitionMethod() {
        return acquisitionMethod;
    }

    public void setAcquisitionMethod(String acquisitionMethod) {
        this.acquisitionMethod = acquisitionMethod;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getProcessingMethod() {
        return processingMethod;
    }

    public void setProcessingMethod(String processingMethod) {
        this.processingMethod = processingMethod;
    }

    public String getReport() {
        return report;
    }

    public void setReport(String report) {
        this.report = report;
    }

    public AcousticInstrument getAcousticInstrument() {
        return acousticInstrument;
    }

    public void setAcousticInstrument(AcousticInstrument acousticInstrument) {
        this.acousticInstrument = acousticInstrument;
    }
}
