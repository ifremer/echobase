package fr.ifremer.echobase.services.service.removedata.strategy;

/*
 * #%L
 * EchoBase :: Services
 * %%
 * Copyright (C) 2011 - 2012 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.base.Optional;
import com.google.common.collect.Sets;
import fr.ifremer.echobase.EchoBaseTechnicalException;
import fr.ifremer.echobase.entities.ImportFile;
import fr.ifremer.echobase.entities.ImportFileId;
import fr.ifremer.echobase.entities.ImportLog;
import fr.ifremer.echobase.entities.ImportType;
import fr.ifremer.echobase.entities.data.Category;
import fr.ifremer.echobase.entities.data.Cell;
import fr.ifremer.echobase.entities.data.Mooring;
import fr.ifremer.echobase.entities.data.Result;
import fr.ifremer.echobase.entities.data.Voyage;
import fr.ifremer.echobase.services.EchoBaseServiceSupport;
import fr.ifremer.echobase.services.ProgressModel;
import fr.ifremer.echobase.services.service.UserDbPersistenceService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.persistence.TopiaException;

import javax.inject.Inject;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import org.nuiton.topia.persistence.TopiaEntity;
import fr.ifremer.echobase.entities.data.DataAcousticProvider;

/**
 * Abstract service to remove import data.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.2
 */
public abstract class AbstractRemoveDataStrategy<E extends TopiaEntity> extends EchoBaseServiceSupport {

    /** Logger. */
    private static final Log log =
            LogFactory.getLog(AbstractRemoveDataStrategy.class);

    protected String importLabel;

    protected ProgressModel progressModel;

    @Inject
    protected UserDbPersistenceService persistenceService;

    protected long getImportFileIdsCount(ImportLog importLog) {

        long result = 0;
        for (ImportFile importFile : importLog.getImportFile()) {
            result += persistenceService.getImportFileIdsCount(importFile);
        }
        return result;

    }

    public String getImportLabel() {
        return importLabel;
    }

//    @Override
//    public void setServiceContext(EchoBaseServiceContext serviceContext) {
//        super.setServiceContext(serviceContext);
//
//        persistenceService = serviceContext.newService(UserDbPersistenceService.class);
//    }

    public void setProgressModel(ProgressModel progressModel) {
        this.progressModel = progressModel;
    }

    public abstract long computeNbSteps(DataAcousticProvider<E> provider, ImportLog importLog);

    protected abstract void removeImportData(DataAcousticProvider<E> provider,
                                             String id) throws TopiaException;

    public abstract Set<ImportType> getPossibleSubImportType();

    public void doRemove(DataAcousticProvider<E> provider, ImportLog importLog) throws TopiaException {

        removePreData(provider);

        persistenceService.flush();

        for (ImportFile importFile : importLog.getImportFile()) {


            for (ImportFileId importFileId : persistenceService.getImportFileIdsForImportFile(importFile)) {

                String importId = importFileId.getEntityId();

                boolean exists = persistenceService.isIdExists(importId);
                if (exists) {
                    try {
                        removeImportData(provider, importId);
                    } catch (TopiaException e) {
                        throw new EchoBaseTechnicalException(e);
                    }
                }

                incrementOp("Import file id:" + importId + " removed.");

            }

        }

        persistenceService.flush();

        removePostData(provider);

        persistenceService.flush();
    }

    long opIndex;

    protected void removePreData(DataAcousticProvider<E> provider) throws TopiaException {
        // by default nothing to remove
    }

    protected void removePostData(DataAcousticProvider<E> provider) throws TopiaException {
        // by default nothing to remove
    }

    protected void canNotDealWithId(String id) {
        throw new IllegalStateException(
                "Can not deal with this type of id " + id +
                        " from service " + this);
    }

    protected void removeVoyage(Voyage entity) throws TopiaException {

        // get categories to remove after
        List<Category> categories = persistenceService.getCategoryUsingEchotype(entity);

        // dettach obsolete categories from their echotypes
        // FIXME This with topia 3.0 ?
        for (Category category : categories) {
            category.setEchotype(null);
        }

        // delete it
        persistenceService.deleteVoyage(entity);

        // remove obsolete categories
        for (Category category : categories) {
            persistenceService.deleteCategory(category);
            incrementOp("Remove orphan category " + category.getTopiaId());
        }
    }

    protected void removeVoyagePostCell(Voyage entity) throws TopiaException {


        if (!entity.isPostCellEmpty()) {

            for (Cell cell : Sets.newHashSet(entity.getPostCell())) {

                // remove it from the voyage
                entity.removePostCell(cell);

                // delete postcell
                persistenceService.deleteCell(cell);

                // increments operation
                incrementOp("Remove postCell " + cell.getTopiaId());
            }

            // clean this data
            entity.clearPostCell();
        }
    }

    protected void removeVoyageCellResults(Voyage voyage) throws TopiaException {
        List<String> cellIds = persistenceService.getVoyageCellIds(voyage);
        this.removeCellResults(cellIds);
    }
        
    protected void removeMooringCellResults(Mooring mooring) throws TopiaException {
        List<String> cellIds = persistenceService.getMooringCellIds(mooring);
        this.removeCellResults(cellIds);
    }
        
    protected void removeCellResults(List<String> cellIds) throws TopiaException {
        // remove all result of cells
        for (String cellId : cellIds) {
            Optional<Cell> optionalCell = persistenceService.getOptionalCell(cellId);

            if (optionalCell.isPresent()) {
                Cell cell = optionalCell.get();
                Collection<Result> cellResult = cell.getResult();
                if (cellResult != null) {
                    persistenceService.deleteResults(cellResult);
                }
                cell.clearResult();
            }
            incrementOp("Remove cell " + cellId + " results");
        }
    }
    
    protected void removeOrphanCells() throws TopiaException {
        List<String> cellIds = persistenceService.getOrphanCellIds();
        
        for (String cellId : cellIds) {
            Cell cell = persistenceService.getCell(cellId);
            persistenceService.deleteCell(cell);
            incrementOp("Remove orphan cell " + cellId);
        }
    }

    public void incrementOp(String message) {
        opIndex++;
        if (opIndex % 1000 == 0) {
            persistenceService.flush();
        }
        progressModel.incrementsProgress();

        if (opIndex % 100 == 0) {
            if (log.isInfoEnabled()) {
                log.info(String.format(
                        "[%1$4f] %2$s", progressModel.getProgress(), message));
            }
        }
    }
}
