package fr.ifremer.echobase.services.service.importdata;

/*
 * #%L
 * EchoBase :: Services
 * %%
 * Copyright (C) 2011 - 2016 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

/**
 * Created on 07/05/16.
 *
 * @author Tony Chemit - chemit@codelutin.com
 */
public class CheckFileException extends RuntimeException {

    private final String importFilename;
    private final String expectedFileSuffix;
    private final String actualFileSuffix;

    public CheckFileException(String message, String importFilename, String expectedFileSuffix, String actualFileSuffix) {
        super(message);
        this.importFilename = importFilename;
        this.expectedFileSuffix = expectedFileSuffix;
        this.actualFileSuffix = actualFileSuffix;
    }

    public String getImportFilename() {
        return importFilename;
    }

    public String getExpectedFileSuffix() {
        return expectedFileSuffix;
    }

    public String getActualFileSuffix() {
        return actualFileSuffix;
    }
}
