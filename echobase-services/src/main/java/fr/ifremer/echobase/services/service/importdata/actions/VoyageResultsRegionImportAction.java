package fr.ifremer.echobase.services.service.importdata.actions;

/*
 * #%L
 * EchoBase :: Services
 * %%
 * Copyright (C) 2011 - 2016 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.base.Optional;
import com.google.common.base.Preconditions;
import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;
import fr.ifremer.echobase.entities.EchoBaseUserEntityEnum;
import fr.ifremer.echobase.entities.data.Cell;
import fr.ifremer.echobase.entities.data.Data;
import fr.ifremer.echobase.entities.data.Datas;
import fr.ifremer.echobase.entities.data.Voyage;
import fr.ifremer.echobase.entities.references.CellType;
import fr.ifremer.echobase.entities.references.DataMetadata;
import fr.ifremer.echobase.entities.references.DataQuality;
import fr.ifremer.echobase.io.InputFile;
import fr.ifremer.echobase.services.service.importdata.DuplicatedRegionCellException;
import fr.ifremer.echobase.services.service.importdata.ImportDataFileResult;
import fr.ifremer.echobase.services.service.importdata.MismatchProviderException;
import fr.ifremer.echobase.services.service.importdata.contexts.VoyageResultsImportDataContext;
import fr.ifremer.echobase.services.service.importdata.csv.VoyageResultsRegionCellImportExportModel;
import fr.ifremer.echobase.services.service.importdata.csv.VoyageResultsRegionCellImportRow;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.csv.Import;

/**
 * Created on 25/03/16.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 4.0
 */
public class VoyageResultsRegionImportAction extends VoyageResultsImportDataActionSupport<VoyageResultsRegionCellImportRow> {

    /** Logger. */
    private static final Log log = LogFactory.getLog(VoyageResultsRegionImportAction.class);

    public VoyageResultsRegionImportAction(VoyageResultsImportDataContext importDataContext) {
        super(importDataContext, importDataContext.getConfiguration().getRegionsFile());
    }

    @Override
    protected VoyageResultsRegionCellImportExportModel createCsvImportModel(VoyageResultsImportDataContext importDataContext) {
        return VoyageResultsRegionCellImportExportModel.forImport(importDataContext);
    }

    @Override
    protected VoyageResultsRegionCellImportExportModel createCsvExportModel(VoyageResultsImportDataContext importDataContext) {
        return VoyageResultsRegionCellImportExportModel.forExport(importDataContext);
    }

    @Override
    protected void performImport(VoyageResultsImportDataContext importDataContext, InputFile inputFile, ImportDataFileResult result) {

        DataMetadata dataCoordinateMeta = importDataContext.getRegionEnvCoordinateMeta();
        DataMetadata dataSurfaceMeta = importDataContext.getSurfaceMeta();

        if (log.isInfoEnabled()) {
            log.info("Starts import of Region cells from file " + inputFile.getFileName());
        }

        Voyage expectedVoyage = importDataContext.getVoyage();
        try (Import<VoyageResultsRegionCellImportRow> importer = open()) {

            Cell cell = null;

            incrementsProgress();
            int rowNumber = 0;
            for (VoyageResultsRegionCellImportRow row : importer) {

                doFlushTransaction(++rowNumber);

                Voyage voyage = row.getVoyage();

                if (!expectedVoyage.equals(voyage)) {
                    throw new MismatchProviderException(getLocale(), rowNumber, voyage.getName());
                }

                DataQuality dataQuality = row.getDataQuality();

                if (cell == null || !row.getName().equals(cell.getName())) {

                    String cellName = row.getName();
                    CellType cellType = row.getCellType();

                    boolean exists = persistenceService.containsPostVoyageCellByNameAndType(expectedVoyage, cellName, cellType);
                    if (exists) {
                        throw new DuplicatedRegionCellException(getLocale(), rowNumber, cellName, cellType.getName(), voyage.getName());
                    }

                    cell = persistenceService.createCell(cellType, cellName, dataQuality);
                    voyage.addPostCell(cell);

                    if (log.isInfoEnabled()) {
                        log.info("Create new region cell: " + cell);
                    }

                    // collect ids
                    addId(result, EchoBaseUserEntityEnum.Cell, cell, rowNumber);

                    // add surface data
                    createCellData(cell,
                                   dataSurfaceMeta,
                                   String.valueOf(row.getDataSurface()),
                                   dataQuality,
                                   result,
                                   true, rowNumber);

                }

                // add coordinate data
                createCellData(cell,
                               dataCoordinateMeta,
                               row.getDataCoordinate(),
                               dataQuality,
                               result,
                               true, rowNumber);

                addProcessedRow(result, row);

            }

        }

    }

    @Override
    protected void computeImportedExport(VoyageResultsImportDataContext importDataContext, ImportDataFileResult result) {

        DataMetadata dataCoordinateMeta = importDataContext.getRegionEnvCoordinateMeta();
        DataMetadata dataSurfaceMeta = importDataContext.getSurfaceMeta();

        Predicate<Data> dataPredicate = Datas.newPredicateByDataMetadata(dataSurfaceMeta);

        String voyageId = importDataContext.getConfiguration().getVoyageId();
        Voyage voyage = persistenceService.getVoyage(voyageId);

        for (Data coordinateCellData : getImportedEntities(Data.class, result)) {

            if (!dataCoordinateMeta.equals(coordinateCellData.getDataMetadata())) {

                // Treat only coordindate data, since there is exactly one per row
                // The surface data is the same for all rows of a same region cell
                continue;
            }

            Cell cell = coordinateCellData.getCell();
            if (log.isInfoEnabled()) {
                log.info("Treat imported coordinate data: " + coordinateCellData.getTopiaId() + " for cell: " + cell.getTopiaId());
            }


            Optional<Data> optionalSurfaceData = Iterables.tryFind(cell.getData(), dataPredicate);
            Preconditions.checkState(optionalSurfaceData.isPresent());

            String surfaceDataValue = optionalSurfaceData.get().getDataValue();

            VoyageResultsRegionCellImportRow row = VoyageResultsRegionCellImportRow.of(voyage, coordinateCellData, Float.valueOf(surfaceDataValue));

            addImportedRow(result, row);

        }

    }

}
