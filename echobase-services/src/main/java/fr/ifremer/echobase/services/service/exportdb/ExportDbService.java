/*
 * #%L
 * EchoBase :: Services
 * %%
 * Copyright (C) 2011 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ifremer.echobase.services.service.exportdb;

import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.inject.Inject;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.persistence.TopiaEntity;
import org.nuiton.topia.persistence.TopiaException;
import org.nuiton.topia.persistence.metadata.AssociationMeta;
import org.nuiton.topia.persistence.metadata.TableMeta;
import org.nuiton.topia.service.csv.out.ExportEntityVisitor;
import org.nuiton.topia.service.csv.out.TopiaCsvExports;
import org.nuiton.util.FileUtil;
import org.nuiton.util.TimeLog;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

import fr.ifremer.echobase.entities.EchoBaseUserEntityEnum;
import fr.ifremer.echobase.entities.data.Cell;
import fr.ifremer.echobase.entities.data.Result;
import fr.ifremer.echobase.entities.data.Voyage;
import fr.ifremer.echobase.io.EchoBaseIOUtil;
import fr.ifremer.echobase.persistence.EchoBaseDbMeta;
import fr.ifremer.echobase.persistence.EchoBasePersistenceHelper;
import fr.ifremer.echobase.services.EchoBaseServiceSupport;
import fr.ifremer.echobase.services.service.UserDbPersistenceService;

/**
 * Service to import / export a complete db.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 0.2
 */
public class ExportDbService extends EchoBaseServiceSupport {

    /** Logger. */
    private static final Log log = LogFactory.getLog(ExportDbService.class);

    public static final TimeLog timeLog = new TimeLog(ExportDbService.class);

    @Inject
    private UserDbPersistenceService persistenceService;

    @Inject
    private ExportService exportService;

    public void doExport(ExportDbConfiguration model) throws IOException {

        String fileName = model.getFileName();

        File tempDirectory = model.getWorkingDirectory();

        File zipFile = new File(tempDirectory, fileName + ".echobase");

        if (log.isInfoEnabled()) {
            log.info("Will export db to " + zipFile);
        }
        model.setExportFile(zipFile);

        File dir = new File(tempDirectory, "echobase");

        FileUtil.createDirectoryIfNecessary(dir);

        ExportDbMode exportDbMode = model.getExportDbMode();

        boolean exportVoyagesByVisitor = exportDbMode.isExportData();

        if (exportVoyagesByVisitor) {

            long nbVoyages = persistenceService.countVoyage();
            if (exportDbMode == ExportDbMode.ALL ||
                model.getVoyageIds().length == nbVoyages) {

                // no need to export via visitor, can export all data tables
                exportVoyagesByVisitor = false;
            }
        }

        List<TableMeta<EchoBaseUserEntityEnum>> tablesToExport = Lists.newArrayList();
        List<AssociationMeta<EchoBaseUserEntityEnum>> associationsToExport = Lists.newArrayList();

        EchoBaseDbMeta dbMeta = getDbMeta();

        if (exportDbMode.isExportReferential()) {

            // add referential tables
            tablesToExport.addAll(dbMeta.getReferenceTables());
            associationsToExport.addAll(dbMeta.getReferenceAssociations());
        }

        if (exportDbMode.isExportData() && !exportVoyagesByVisitor) {

            // will export complete data tables
            tablesToExport.addAll(dbMeta.getDataTables());
            associationsToExport.addAll(dbMeta.getDataAssociations());
        }

        if (model.isComputeSteps()) {

            int nbSteps = tablesToExport.size() + associationsToExport.size();

            if (exportVoyagesByVisitor) {
                nbSteps += model.getVoyageIds().length;
            }

            if (log.isInfoEnabled()) {
                log.info("NB steps: " + nbSteps);
            }
            model.setNbSteps(nbSteps);
        }

        exportTables(model, dir, tablesToExport, exportService);

        exportAssociations(model, dir, associationsToExport, exportService);

        if (exportVoyagesByVisitor) {

            exportVoyages(model, dir, exportService);
        }

        EchoBaseIOUtil.compressZipFile(zipFile, dir);
    }

    private void exportTables(ExportDbConfiguration model,
                              File dir,
                              List<TableMeta<EchoBaseUserEntityEnum>> tablesToExport,
                              ExportService exportService) {
        for (TableMeta<EchoBaseUserEntityEnum> meta : tablesToExport) {

            model.incrementsProgress();

            File entryFile = new File(dir, meta.getFilename());
            exportService.exportData(meta, entryFile);
        }
    }

    private void exportAssociations(ExportDbConfiguration model,
                                    File dir,
                                    List<AssociationMeta<EchoBaseUserEntityEnum>> associationsToExport,
                                    ExportService exportService) {
        for (AssociationMeta<EchoBaseUserEntityEnum> associationMeta : associationsToExport) {

            model.incrementsProgress();

            File entryFile = new File(dir, associationMeta.getFilename());
            exportService.exportData(associationMeta, entryFile);
        }
    }

    private void exportVoyages(ExportDbConfiguration model,
                               File dir,
                               ExportService exportService) throws IOException {

        EchoBaseDbMeta dbMeta = getDbMeta();

        Map<EchoBaseUserEntityEnum, TopiaCsvExports.EntityExportContext<EchoBaseUserEntityEnum>> contexts =
                TopiaCsvExports.createReplicateEntityVisitorContexts(
                        exportService.getModelFactory(false),
                        dbMeta.getDataTables(),
                        dbMeta.getDataAssociations(),
                        dir);

        ReplicateEntityVisitor visitor = new ReplicateEntityVisitor(
                dbMeta.getPersistenceHelper(),
                contexts);

        try {
            for (String voyageId : model.getVoyageIds()) {

                model.incrementsProgress();

                Voyage voyage = persistenceService.getVoyage(voyageId);
                visitor.export(voyage);
            }
        } finally {

            // close visitor to close export files
            visitor.close();
        }
    }

    /**
     * entity visitor to export data to csv files.
     *
     * @author Tony Chemit - chemit@codelutin.com
     * @since 0.3
     */
    static class ReplicateEntityVisitor extends ExportEntityVisitor<EchoBaseUserEntityEnum> {

        protected final Set<String> categoryIds;

        public ReplicateEntityVisitor(EchoBasePersistenceHelper typeProvider,
                                      Map<EchoBaseUserEntityEnum, TopiaCsvExports.EntityExportContext<EchoBaseUserEntityEnum>> entityExporters) {
            super(typeProvider, entityExporters);
            categoryIds = Sets.newHashSet();
        }

        @Override
        public void visit(TopiaEntity entity, String propertyName,
                          Class<?> type, Object value) {
            if (Result.PROPERTY_CATEGORY.equals(propertyName) &&
                entity instanceof Result) {

                // export category
                try {
                    TopiaEntity topiaEntity = (TopiaEntity) value;
                    if (categoryIds.add(topiaEntity.getTopiaId())) {
                        // add this new category
                        topiaEntity.accept(this);
                    }

                } catch (TopiaException e) {
                    if (log.isErrorEnabled()) {
                        log.error("Can not visit entity " + value, e);
                    }
                }
            }
        }

        @Override
        protected void visitEntityCollection(TopiaEntity entity,
                                             String propertyName,
                                             Class<?> collectionType,
                                             Class<?> type,
                                             Collection<?> cValue) {
            if (Voyage.PROPERTY_POST_CELL.equals(propertyName) && entity instanceof Voyage) {

                // special case, we don not want to visit childs of cells
                for (Object o : cValue) {
                    Cell cell = (Cell) o;

                    cell.acceptWithNoChild(this);
                }
            } else {
                // normal entity collection visit
                super.visitEntityCollection(entity,
                                            propertyName,
                                            collectionType,
                                            type,
                                            cValue);
            }
        }

        @Override
        public void close() throws IOException {
            categoryIds.clear();
            super.close();
        }
    }
}
