package fr.ifremer.echobase.services.service.importdata.actions;

/*
 * #%L
 * EchoBase :: Services
 * %%
 * Copyright (C) 2011 - 2016 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.echobase.entities.data.DataAcousticProvider;
import fr.ifremer.echobase.io.InputFile;
import fr.ifremer.echobase.services.csv.CellAble;
import fr.ifremer.echobase.services.csv.ResultAble;
import fr.ifremer.echobase.services.service.importdata.configurations.VoyageResultsImportConfiguration;
import fr.ifremer.echobase.services.service.importdata.contexts.VoyageResultsImportDataContext;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Created on 25/03/16.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 4.0
 */
public abstract class VoyageResultsCellImportDataActionSupport<E extends ResultAble & CellAble> extends ImportResultsCellDataActionSupport<VoyageResultsImportConfiguration, VoyageResultsImportDataContext, E> {

    /** Logger. */
    private static final Log log = LogFactory.getLog(VoyageResultsCellImportDataActionSupport.class);

    protected VoyageResultsCellImportDataActionSupport(VoyageResultsImportDataContext importDataContext, InputFile inputFile, String... columnNamesToExclude) {
        super(importDataContext, inputFile, columnNamesToExclude);
    }

    @Override
    protected DataAcousticProvider getDataProvider(VoyageResultsImportDataContext importDataContext) {
        return importDataContext.getVoyage();
    }

}
