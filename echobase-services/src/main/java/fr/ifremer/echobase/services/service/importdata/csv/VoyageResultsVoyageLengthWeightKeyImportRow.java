/*
 * #%L
 * EchoBase :: Services
 * %%
 * Copyright (C) 2011 - 2012 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ifremer.echobase.services.service.importdata.csv;

import fr.ifremer.echobase.entities.data.LengthWeightKey;
import fr.ifremer.echobase.entities.data.LengthWeightKeyImpl;
import fr.ifremer.echobase.entities.data.Voyage;
import fr.ifremer.echobase.entities.references.SizeCategory;
import fr.ifremer.echobase.entities.references.Species;
import fr.ifremer.echobase.entities.references.Strata;

import java.io.Serializable;

/**
 * Bean used as a row for import of {@link VoyageResultsVoyageLengthWeightKeyImportExportModel}.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 0.3
 */
public class VoyageResultsVoyageLengthWeightKeyImportRow implements Serializable {

    private static final long serialVersionUID = 1L;

    public static final String PROPERTY_VOYAGE = "voyage";
    public static final String PROPERTY_SPECIES = "species";
    public static final String PROPERTY_SIZE_CATEGORY = "sizeCategory";
    public static final String PROPERTY_STRATA = "strata";

    protected final LengthWeightKey lengthWeightKey;
    protected Species species;
    protected SizeCategory sizeCategory;
    protected Voyage voyage;

    public static VoyageResultsVoyageLengthWeightKeyImportRow of(Voyage voyage, LengthWeightKey lengthWeightKey) {
        VoyageResultsVoyageLengthWeightKeyImportRow row = new VoyageResultsVoyageLengthWeightKeyImportRow(lengthWeightKey);
        row.setVoyage(voyage);
        row.setSpecies(lengthWeightKey.getSpeciesCategory().getSpecies());
        row.setSizeCategory(lengthWeightKey.getSpeciesCategory().getSizeCategory());
        return row;
    }

    public VoyageResultsVoyageLengthWeightKeyImportRow(LengthWeightKey lengthWeightKey) {
        this.lengthWeightKey = lengthWeightKey;
    }

    public VoyageResultsVoyageLengthWeightKeyImportRow() {
        this(new LengthWeightKeyImpl());
    }

    public LengthWeightKey getLengthWeightKey() {
        return lengthWeightKey;
    }

    public Species getSpecies() {
        return species;
    }

    public void setSpecies(Species species) {
        this.species = species;
    }

    public SizeCategory getSizeCategory() {
        return sizeCategory;
    }

    public void setSizeCategory(SizeCategory sizeCategory) {
        this.sizeCategory = sizeCategory;
    }

    public Voyage getVoyage() {
        return voyage;
    }

    public void setVoyage(Voyage voyage) {
        this.voyage = voyage;
    }

    public float getAParameter() {
        return lengthWeightKey.getAParameter();
    }

    public void setAParameter(float AParameter) {
        lengthWeightKey.setAParameter(AParameter);
    }

    public float getBParameter() {
        return lengthWeightKey.getBParameter();
    }

    public void setBParameter(float BParameter) {
        lengthWeightKey.setBParameter(BParameter);
    }

    public Strata getStrata() {
        return lengthWeightKey.getStrata();
    }

    public void setStrata(Strata strata) {
        lengthWeightKey.setStrata(strata);
    }
}
