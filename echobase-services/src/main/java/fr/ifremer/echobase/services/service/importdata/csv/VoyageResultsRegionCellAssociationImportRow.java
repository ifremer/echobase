/*
 * #%L
 * EchoBase :: Services
 * %%
 * Copyright (C) 2011 - 2012 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ifremer.echobase.services.service.importdata.csv;

import fr.ifremer.echobase.entities.data.Cell;
import fr.ifremer.echobase.entities.data.Voyage;

/**
 * Bean used as a row for import of {@link VoyageResultsRegionCellAssociationImportExportModel}.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 0.3
 */
public class VoyageResultsRegionCellAssociationImportRow {

    public static final String PROPERTY_VOYAGE = "voyage";
    public static final String PROPERTY_ESDU_CELL = "esduCell";
    public static final String PROPERTY_REGION_CELL = "regionCell";

    protected Voyage voyage;

    protected Cell esduCell;

    protected Cell regionCell;

    public static VoyageResultsRegionCellAssociationImportRow of(Voyage voyage, Cell regionCell, Cell esduCell) {
        VoyageResultsRegionCellAssociationImportRow row = new VoyageResultsRegionCellAssociationImportRow();
        row.setVoyage(voyage);
        row.setRegionCell(regionCell);
        row.setEsduCell(esduCell);
        return row;
    }

    public VoyageResultsRegionCellAssociationImportRow() {
    }

    public Voyage getVoyage() {
        return voyage;
    }

    public void setVoyage(Voyage voyage) {
        this.voyage = voyage;
    }

    public Cell getRegionCell() {
        return regionCell;
    }

    public void setRegionCell(Cell regionCell) {
        this.regionCell = regionCell;
    }

    public Cell getEsduCell() {
        return esduCell;
    }

    public void setEsduCell(Cell esduCell) {
        this.esduCell = esduCell;
    }
}
