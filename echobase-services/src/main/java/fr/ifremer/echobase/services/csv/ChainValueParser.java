package fr.ifremer.echobase.services.csv;

/*
 * #%L
 * EchoBase :: Domain
 * %%
 * Copyright (C) 2011 - 2012 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.csv.ValueParser;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * To chain some {@link ValueParser} (different format supported).
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.3
 */
class ChainValueParser<E> implements ValueParser<E> {

    /** Logger. */
    private static final Log log = LogFactory.getLog(ChainValueParser.class);

    private final List<ValueParser<E>> parsers;

    protected ChainValueParser(List<ValueParser<E>> parsers) {
        this.parsers = new ArrayList<>(parsers);
    }

    @Override
    public E parse(String value) throws ParseException {
        E result = null;
        Iterator<ValueParser<E>> iterator = parsers.iterator();
        while (iterator.hasNext()) {
            ValueParser<E> parser = iterator.next();
            try {
                result = parser.parse(value);
                break;
            } catch (Exception e) {
                if (log.isErrorEnabled()) {
                    log.error("Could not parse value " + value, e);
                }
            }
        }

        if (result == null && !iterator.hasNext()) {

            // all parsers were used and no one gives a good result
            throw new ParseException(
                    "Could not parse value " + value + " with any parsers", 0);
        }
        return result;
    }
}
