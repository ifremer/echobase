/*
 * #%L
 * EchoBase :: Domain
 * %%
 * Copyright (C) 2011 - 2012 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ifremer.echobase.services.csv;

import fr.ifremer.echobase.entities.data.Cell;
import fr.ifremer.echobase.entities.data.CellTopiaDao;
import fr.ifremer.echobase.entities.references.CellType;
import org.nuiton.csv.ImportRuntimeException;
import org.nuiton.csv.ValueFormatter;

import java.util.LinkedHashSet;
import java.util.Set;

public class CellValueFormatter implements ValueFormatter<Cell> {

    private final Set<Cell> esduCells;
    private final CellTopiaDao cellDAO;
    private final CellType esduCellType;
    private final CellType elementaryCellType;

    public CellValueFormatter(CellTopiaDao cellDAO, CellType esduCellType, CellType elementaryCellType) {
        this.cellDAO = cellDAO;
        this.esduCellType = esduCellType;
        this.elementaryCellType = elementaryCellType;
        this.esduCells = new LinkedHashSet<>();
    }

    protected CellValueFormatter(Set<Cell> esduCells, CellType esduCellType, CellType elementaryCellType) {
        this.esduCells = esduCells;
        this.esduCellType = esduCellType;
        this.elementaryCellType = elementaryCellType;
        this.cellDAO = null;
    }

    @Override
    public String format(Cell value) {
        CellType cellType = value.getCellType();

        if (esduCellType.equals(cellType)) {

            // cellule de type esdu
            return value.getName();

        }

        if (elementaryCellType.equals(cellType)) {

            // cellule de type élémentaire

            // récupération de la cellule de type esdu qui la contient

            Cell esduCell = getEsduCell(value);

            return esduCell + "_" + value.getName();
        }

        //FIXME Use a real exception
        return null;
    }

    protected Cell getEsduCell(Cell elementaryCell) {

        Cell result = getEsduCellFromCache(elementaryCell);

        if (result == null && cellDAO != null) {

            result = cellDAO.forChildsContains(elementaryCell).findAnyOrNull();

            if (result != null) {
                esduCells.add(result);
            }

        }

        if (result == null) {
            //FIXME USe a real exception
            throw new ImportRuntimeException(
                    "Can not find esdu cell for elementary cell " + elementaryCell.getName());
        }

        return result;
    }

    protected Cell getEsduCellFromCache(Cell elementaryCell) {

        for (Cell cell : esduCells) {

            if (cell.getChildsByTopiaId(elementaryCell.getTopiaId()) != null) {

                return cell;
            }
        }

        return null;
    }

}
