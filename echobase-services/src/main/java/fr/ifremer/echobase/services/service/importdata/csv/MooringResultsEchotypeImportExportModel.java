/*
 * #%L
 * EchoBase :: Services
 * %%
 * Copyright (C) 2011 - 2012 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ifremer.echobase.services.service.importdata.csv;

import fr.ifremer.echobase.entities.data.Echotype;
import fr.ifremer.echobase.entities.data.Mooring;
import fr.ifremer.echobase.entities.data.Voyage;
import fr.ifremer.echobase.entities.references.DepthStratum;
import fr.ifremer.echobase.entities.references.Species;
import fr.ifremer.echobase.services.csv.EchoBaseCsvUtil;
import fr.ifremer.echobase.services.service.importdata.contexts.MooringResultsImportDataContext;
import static fr.ifremer.echobase.services.service.importdata.csv.ResultsEchotypeImportRow.HEADER_ECHOTYPE_NAME;
import static fr.ifremer.echobase.services.service.importdata.csv.ResultsEchotypeImportRow.HEADER_SPECIES;

/**
 * Model to import echotypes.
 *
 * @author Julien Ruchaud - ruchaud@codelutin.com
 * @since 4.0
 */
public class MooringResultsEchotypeImportExportModel extends EchoBaseImportExportModelSupport<MooringResultsEchotypeImportRow> {

    private MooringResultsEchotypeImportExportModel(char separator) {
        super(separator);
    }

    public static MooringResultsEchotypeImportExportModel forImport(MooringResultsImportDataContext importDataContext) {

        MooringResultsEchotypeImportExportModel model = new MooringResultsEchotypeImportExportModel(importDataContext.getCsvSeparator());
        model.newMandatoryColumn(HEADER_ECHOTYPE_NAME, Echotype.PROPERTY_NAME);
        model.newMandatoryColumn(MooringResultsEchotypeImportRow.PROPERTY_MEANING, Echotype.PROPERTY_MEANING);
        model.newForeignKeyColumn(MooringResultsEchotypeImportRow.PROPERTY_MOORING, Mooring.class, Mooring.PROPERTY_CODE, importDataContext.getMooringsByCode());
        model.newForeignKeyColumn(EchoBaseCsvUtil.DEPTH_STRATUM_ID, Echotype.PROPERTY_DEPTH_STRATUM, DepthStratum.class, DepthStratum.PROPERTY_ID, importDataContext.getDepthStratumsById());
        model.newForeignKeyColumn(HEADER_SPECIES, Echotype.PROPERTY_SPECIES, Species.class, Species.PROPERTY_BARACOUDA_CODE, importDataContext.getSpeciesByBaracoudaCode());
        return model;

    }

    public static MooringResultsEchotypeImportExportModel forExport(MooringResultsImportDataContext importDataContext) {

        MooringResultsEchotypeImportExportModel model = new MooringResultsEchotypeImportExportModel(importDataContext.getCsvSeparator());
        model.newColumnForExport(HEADER_ECHOTYPE_NAME, Echotype.PROPERTY_NAME);
        model.newColumnForExport(MooringResultsEchotypeImportRow.PROPERTY_MEANING, Echotype.PROPERTY_MEANING);
        model.newColumnForExport(MooringResultsEchotypeImportRow.PROPERTY_MOORING, EchoBaseCsvUtil.MOORING_FORMATTER);
        model.newColumnForExport(EchoBaseCsvUtil.DEPTH_STRATUM_ID, Echotype.PROPERTY_DEPTH_STRATUM, EchoBaseCsvUtil.DEPTH_STRATUM_FORMATTER);
        model.newColumnForExport(HEADER_SPECIES, Echotype.PROPERTY_SPECIES, EchoBaseCsvUtil.SPECIES_FORMATTER);
        return model;

    }

    @Override
    public MooringResultsEchotypeImportRow newEmptyInstance() {
        return new MooringResultsEchotypeImportRow();
    }
}
