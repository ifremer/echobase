package fr.ifremer.echobase.services.service.importdata.actions;

/*
 * #%L
 * EchoBase :: Services
 * %%
 * Copyright (C) 2011 - 2016 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.echobase.entities.data.DataAcousticProvider;
import fr.ifremer.echobase.services.service.importdata.configurations.MooringAcousticsImportConfiguration;
import fr.ifremer.echobase.services.service.importdata.contexts.MooringAcousticsImportDataContext;
import fr.ifremer.echobase.services.service.importdata.csv.AcousticImportExportModel;
import fr.ifremer.echobase.services.service.importdata.csv.AcousticImportRow;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Do the import for acoustics data for the mooring.
 * 
 * @author Julien Ruchaud - ruchaud@codelutin.com
 * @since 4.0
 */
public class MooringAcousticsImportAction extends ImportAcousticsActionSupport<MooringAcousticsImportConfiguration, MooringAcousticsImportDataContext, AcousticImportRow> {
    /** Logger. */
    private static final Log log = LogFactory.getLog(MooringAcousticsImportAction.class);

    public MooringAcousticsImportAction(MooringAcousticsImportDataContext importDataContext) {
        super(importDataContext);
    }

    @Override
    protected AcousticImportExportModel createCsvImportModel(MooringAcousticsImportDataContext importDataContext) {
        return AcousticImportExportModel.forImport(importDataContext);
    }

    @Override
    protected AcousticImportExportModel createCsvExportModel(MooringAcousticsImportDataContext importDataContext) {
        return AcousticImportExportModel.forExport(importDataContext);
    }

    @Override
    protected DataAcousticProvider getDataProvider(MooringAcousticsImportDataContext importDataContext, AcousticImportRow row, int rowNumber) {
        return (DataAcousticProvider) importDataContext.getMooring();
    }
    
}
