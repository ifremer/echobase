package fr.ifremer.echobase.services.service.importdata.actions;

/*
 * #%L
 * EchoBase :: Services
 * %%
 * Copyright (C) 2011 - 2016 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.echobase.io.InputFile;
import fr.ifremer.echobase.services.service.importdata.configurations.VoyageCommonsImportConfiguration;
import fr.ifremer.echobase.services.service.importdata.contexts.VoyageCommonsImportDataContext;

/**
 * Created on 30/03/16.
 *
 * @author Tony Chemit - chemit@codelutin.com
 */
public abstract class VoyageCommonsImportDataActionSupport<E> extends ImportDataActionSupport<VoyageCommonsImportConfiguration, VoyageCommonsImportDataContext, E> {

    protected VoyageCommonsImportDataActionSupport(VoyageCommonsImportDataContext importDataContext, InputFile inputFile) {
        super(importDataContext, inputFile);
    }

}
