package fr.ifremer.echobase.services.service.exportCoser;

/*
 * #%L
 * EchoBase :: Services
 * %%
 * Copyright (C) 2011 - 2014 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.echobase.entities.references.Species;

/**
 * Created on 4/5/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.6
 */
public class SpeciesExportRow {

    public static final String PROPERTY_SPECIES = "species";

    protected Species species;

    public Species getSpecies() {
        return species;
    }

    public String getTaxaCode() {
        return "";
    }

    public String getGenusSpecies() {
        return species.getGenusSpecies();
    }

    public String getTaxonCode() {
        return species.getTaxonCode();
    }

    public String getTaxonSystematicOrder() {
        return species.getTaxonSystematicOrder();
    }

    public String getTaxonSystematicLevel() {
        return species.getTaxonSystematicLevel();
    }

    public String getTaxonFatherMemocode() {
        return species.getTaxonFatherMemocode();
    }

    public String getAuthorReference() {
        return species.getAuthorReference();
    }

    public void setSpecies(Species species) {
        this.species = species;
    }
}
