/*
 * #%L
 * EchoBase :: Services
 * %%
 * Copyright (C) 2011 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ifremer.echobase.services.service.exportquery;

import com.google.common.base.Charsets;
import fr.ifremer.echobase.EchoBaseTechnicalException;
import fr.ifremer.echobase.entities.EchoBaseInternalPersistenceContext;
import fr.ifremer.echobase.entities.EchoBaseUser;
import fr.ifremer.echobase.entities.ExportQueries;
import fr.ifremer.echobase.entities.ExportQuery;
import fr.ifremer.echobase.entities.ExportQueryTopiaDao;
import fr.ifremer.echobase.services.EchoBaseServiceSupport;
import fr.ifremer.echobase.services.service.DecoratorService;
import fr.ifremer.echobase.services.service.UserDbPersistenceService;
import org.apache.commons.lang3.StringUtils;
import org.nuiton.csv.Export;
import org.nuiton.topia.persistence.TopiaEntity;
import org.nuiton.topia.persistence.TopiaException;
import org.nuiton.topia.persistence.pager.TopiaPagerBean;
import org.nuiton.util.RecursiveProperties;

import javax.inject.Inject;
import java.util.List;
import java.util.Map;

/**
 * Service to deal with sql export.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 0.1
 */
public class ExportQueryService extends EchoBaseServiceSupport {

    @Inject
    EchoBaseInternalPersistenceContext echoBaseInternalPersistenceContext;

    @Inject
    private UserDbPersistenceService persistenceService;

    @Inject
    private DecoratorService decoratorService;

    public ExportQuery getExportQuery(String topiaId) throws ExportQueryNotFoundException {

        ExportQuery exportQuery = getDao().forTopiaIdEquals(topiaId).findUnique();;

        if (exportQuery == null) {
            throw new ExportQueryNotFoundException();
        }
        ExportQuery entityToSave = newExportQuery();
        entityToSave.setTopiaId(exportQuery.getTopiaId());
        entityToSave.setName(exportQuery.getName());
        entityToSave.setDescription(exportQuery.getDescription());
        entityToSave.setSqlQuery(exportQuery.getSqlQuery());
        entityToSave.setLastModifiedDate(exportQuery.getLastModifiedDate());
        entityToSave.setLastModifiedUser(exportQuery.getLastModifiedUser());
        return entityToSave;
    }


    public boolean isQueryNameValid(ExportQuery exportQuery) {
        String queryName = exportQuery.getName();
        return ExportQueries.isQueryNameValid(queryName);
    }

    public boolean isQueryNameAvailable(ExportQuery exportQuery) {
        String queryName = exportQuery.getName();
        String id = exportQuery.getTopiaId();
        boolean mustCreate = StringUtils.isEmpty(id);

        boolean result;
        if (mustCreate) {

            result = !getDao().isQueryExists(queryName);
        } else {
            result = !getDao().isQueryExists(id, queryName);
        }
        return result;
    }

    public ExportQuery createOrUpdate(ExportQuery exportQuery,
                                      EchoBaseUser user) throws ExportQueryNameAlreadyExistException, ExportQueryInvalidNameException {
        try {

            // No id, creating new one entity
            String id = exportQuery.getTopiaId();

            boolean mustCreate = StringUtils.isEmpty(id);

            String queryName = exportQuery.getName();

            if (!isQueryNameValid(exportQuery)) {

                // can not accept a non alpha numeric name
                throw new ExportQueryInvalidNameException();
            }

            // check query does not already exists with this name
            if (!isQueryNameAvailable(exportQuery)) {
                throw new ExportQueryNameAlreadyExistException();
            }

            ExportQuery entityToSave;

            exportQuery.setLastModifiedDate(newDate());
            exportQuery.setLastModifiedUser(user.getEmail());

            if (mustCreate) {
                entityToSave = getDao().create(exportQuery);
            } else {
                entityToSave = getDao().forTopiaIdEquals(id).findUnique();;
                entityToSave.setName(queryName);
                entityToSave.setDescription(exportQuery.getDescription());
                entityToSave.setSqlQuery(exportQuery.getSqlQuery());
                entityToSave.setLastModifiedDate(exportQuery.getLastModifiedDate());
                entityToSave.setLastModifiedUser(exportQuery.getLastModifiedUser());
            }

            echoBaseInternalPersistenceContext.commit();
            return entityToSave;
        } catch (TopiaException eee) {
            throw new EchoBaseTechnicalException(eee);
        }
    }

    public void delete(String topiaId) {
        try {
            ExportQuery entityToDelete = getDao().forTopiaIdEquals(topiaId).findUnique();;
            getDao().delete(entityToDelete);

            echoBaseInternalPersistenceContext.commit();
        } catch (TopiaException eee) {
            throw new EchoBaseTechnicalException(eee);
        }
    }

    public String getSqlQuery(String queryId) throws ExportQueryNotFoundException {

        ExportQuery exportQuery = getExportQuery(queryId);

        return getSafeSql(exportQuery.getSqlQuery());
    }

    public <E extends TopiaEntity> Map<String, String> loadSortAndDecorate(Class<E> beanType) {

        List<E> beans = echoBaseInternalPersistenceContext.getDao(beanType).findAll();
        return decoratorService.sortAndDecorate(beans, null);
    }

    protected String getSafeSql(String sql) {
        RecursiveProperties sqls = new RecursiveProperties();
        List<ExportQuery> queries = echoBaseInternalPersistenceContext.getExportQueryDao().findAll();
        for (ExportQuery query : queries) {
            sqls.put(query.getName(), query.getSqlQuery());
        }
        if (!sqls.containsKey(sql)) {

            sqls.put(sql, sql);
        }

        return sqls.getProperty(sql);
    }

    public void testSql(String sql) throws TopiaException {

        String safeSql = getSafeSql(sql);
        GenericSQLQuery sqlQuery = new GenericSQLQuery(safeSql, null);
        persistenceService.findSingleResult(sqlQuery);
        sqlQuery.getColumnNames();
    }

    public Map<String, Object>[] executeExportQuery(String queryId,
                                                    TopiaPagerBean pager) throws ExportQueryNotFoundException {

        String sql = getSqlQuery(queryId);

        // get a query to count all rows for the request
        GenericSQLQuery sqlQuery = new GenericSQLQuery(sql, pager);
        try {
            List<Map<String, Object>> result =
                    persistenceService.findMultipleResult(sqlQuery);
            return result.toArray(new Map[result.size()]);
        } catch (TopiaException eee) {
            throw new EchoBaseTechnicalException(
                    "Could not execute sql query", eee);
        }
    }

    public String[] getColumnNames(String sql) {

        String limitSql = sql.trim();
        if (limitSql.endsWith(";")) {
            limitSql = limitSql.substring(0, limitSql.length() - 1);
            limitSql += " LIMIT 1";
        }
        try {
            // do a limit query to one result and obtain the column names
            // from the meta data of the result set
            GenericSQLQuery sqlQuery = new GenericSQLQuery(limitSql, null);
            persistenceService.findSingleResult(sqlQuery);
            return sqlQuery.getColumnNames();
        } catch (TopiaException eee) {
            throw new EchoBaseTechnicalException(
                    "Could not execute query " + limitSql + " for reason " + eee.getCause().getMessage(), eee);
        } catch (Exception eee) {
            throw new EchoBaseTechnicalException(
                    "Could not execute query " + limitSql + " for reason " + eee.getMessage(), eee);
        }
    }

    public String createCsvFileContent(String queryId) throws ExportQueryNotFoundException {

        String sql = getSqlQuery(queryId);

        GenericSQLQuery sqlQuery = new GenericSQLQuery(sql, null);
        List<Map<String, Object>> rows;
        try {
            rows = persistenceService.findMultipleResult(sqlQuery);
        } catch (TopiaException eee) {
            throw new EchoBaseTechnicalException(
                    "Could not execute sql query", eee);
        }
        char csvSeparator = getConfiguration().getCsvSeparator();
        ExportQueryCsvModel csvModel = sqlQuery.generateCsvModel(csvSeparator);
        Export<Map<String, Object>> exporter = Export.newExport(csvModel, rows);
        try {
            return exporter.toString(Charsets.UTF_8);
        } catch (Exception eee) {
            throw new EchoBaseTechnicalException("Could not export sql", eee);
        }
    }

    public ExportQuery newExportQuery() {
        try {
            return getDao().newInstance();
        } catch (TopiaException e) {
            throw new EchoBaseTechnicalException(e);
        }
    }

    public String processLibreOfficeSqlQuery(String libreOfficeQuery) {

        return libreOfficeQuery.replaceAll("\\\"", "");
    }

    protected ExportQueryTopiaDao getDao() {
        return echoBaseInternalPersistenceContext.getExportQueryDao();
    }
}
