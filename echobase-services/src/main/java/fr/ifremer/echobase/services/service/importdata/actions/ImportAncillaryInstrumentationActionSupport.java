package fr.ifremer.echobase.services.service.importdata.actions;

/*
 * #%L
 * EchoBase :: Services
 * %%
 * Copyright (C) 2011 - 2016 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.base.Preconditions;
import fr.ifremer.echobase.entities.EchoBaseUserEntityEnum;
import fr.ifremer.echobase.entities.data.DataAcousticProvider;
import fr.ifremer.echobase.entities.references.AncillaryInstrumentation;
import fr.ifremer.echobase.io.InputFile;
import fr.ifremer.echobase.services.service.importdata.DuplicatedAncillaryInstrumentationAssociationException;
import fr.ifremer.echobase.services.service.importdata.ImportDataFileResult;
import fr.ifremer.echobase.services.service.importdata.configurations.ImportDataConfigurationSupport;
import fr.ifremer.echobase.services.service.importdata.contexts.ImportDataContextSupport;
import fr.ifremer.echobase.services.service.importdata.csv.AncillaryInstrumentationImportRow;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.csv.Import;
import org.nuiton.topia.persistence.TopiaEntity;

/**
 * @author Julien Ruchaud - ruchaud@codelutin.com
 * @since 4.0
 */
public abstract class ImportAncillaryInstrumentationActionSupport<M extends ImportDataConfigurationSupport, C extends ImportDataContextSupport<M>, E extends AncillaryInstrumentationImportRow> extends ImportDataActionSupport<M, C, E> {

    /** Logger. */
    private static final Log log = LogFactory.getLog(ImportAncillaryInstrumentationActionSupport.class);
    
    private final LinkedList<Pair<String, String>> ancillaryInstrumentations = new LinkedList<>();
    private final Set<String> providerIds = new HashSet<>();
    
    public ImportAncillaryInstrumentationActionSupport(C importDataContext, InputFile inputFile) {
        super(importDataContext, inputFile);
    }
    
    protected abstract E newImportedRow(DataAcousticProvider provider, AncillaryInstrumentation ancillaryInstrumentation);

    protected abstract List<DataAcousticProvider> getDataProviders(C importDataContext, E row, int rowNumber);

    @Override
    protected void performImport(C importDataContext, InputFile inputFile, ImportDataFileResult result) {
        if (log.isInfoEnabled()) {
            log.info("Starts import of ancillary instrumentation from file " + inputFile.getFileName());
        }

        try (Import<E> importer = open()) {

            incrementsProgress();

            int rowNumber = 0;
            for (E row : importer) {

                doFlushTransaction(++rowNumber);

                boolean firstLine = true;
                List<DataAcousticProvider> dataProviders = getDataProviders(importDataContext, row, rowNumber);
                for (DataAcousticProvider provider : dataProviders) {
                    
                    AncillaryInstrumentation ancillaryInstrumentation = row.getAncillaryInstrumentation();

                    AncillaryInstrumentation existingAncillaryInstrumentation = provider.getAncillaryInstrumentationByTopiaId(ancillaryInstrumentation.getTopiaId());
                    if (existingAncillaryInstrumentation != null) {
                        throw new DuplicatedAncillaryInstrumentationAssociationException(getLocale(), rowNumber, provider.getName(), ancillaryInstrumentation.getName());
                    }

                    // add it
                    provider.addAncillaryInstrumentation(ancillaryInstrumentation);
                    
                    TopiaEntity entity = provider.getEntity();
                    String providerId = entity.getTopiaId();
                    boolean already = providerIds.add(providerId);
                    if (!already) {
                        updateId(result, EchoBaseUserEntityEnum.AncillaryInstrumentation, entity, rowNumber);
                    } else {
                        result.incrementsNumberUpdated(EchoBaseUserEntityEnum.AncillaryInstrumentation);
                    }
                    
                    if (firstLine) {
                        ancillaryInstrumentations.add(Pair.of(ancillaryInstrumentation.getTopiaId(), providerId));
                        firstLine = false;
                    }
                }

                addProcessedRow(result, row);
            }
        }
    }

    @Override
    protected void computeImportedExport(C importDataContext, ImportDataFileResult result) {
        for (Pair<String, String> entry : ancillaryInstrumentations) {
            String ancillaryInstrumentationId = entry.getKey();
            AncillaryInstrumentation ancillaryInstrumentation = persistenceService.getAncillaryInstrumentation(ancillaryInstrumentationId);
            Preconditions.checkNotNull(ancillaryInstrumentation);

            String providerId = entry.getValue();
            TopiaEntity provider = persistenceService.getEntity(providerId);
            Preconditions.checkNotNull(provider);
            
            E row = newImportedRow((DataAcousticProvider) provider, ancillaryInstrumentation);
            addImportedRow(result, row);
        }
    }

}
