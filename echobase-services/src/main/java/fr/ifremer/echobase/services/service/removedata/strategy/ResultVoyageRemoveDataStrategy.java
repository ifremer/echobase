package fr.ifremer.echobase.services.service.removedata.strategy;

/*
 * #%L
 * EchoBase :: Services
 * %%
 * Copyright (C) 2011 - 2012 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.Sets;
import fr.ifremer.echobase.entities.ImportLog;
import fr.ifremer.echobase.entities.ImportType;
import fr.ifremer.echobase.entities.data.Category;
import fr.ifremer.echobase.entities.data.Echotype;
import fr.ifremer.echobase.entities.data.LengthAgeKey;
import fr.ifremer.echobase.entities.data.LengthWeightKey;
import fr.ifremer.echobase.entities.data.Voyage;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.persistence.TopiaException;

import java.util.List;
import java.util.Set;
import fr.ifremer.echobase.entities.data.DataAcousticProvider;

/**
 * Remove a {@link ImportType#OPERATION} import.
 *
 * Can remove only {@link Echotype}, {@link LengthAgeKey} or
 * {@link LengthWeightKey}.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.2
 */
public class ResultVoyageRemoveDataStrategy extends AbstractRemoveDataStrategy<Voyage> {

    /** Logger. */
    private static final Log log =
            LogFactory.getLog(ResultVoyageRemoveDataStrategy.class);

    @Override
    public long computeNbSteps(DataAcousticProvider<Voyage> provider, ImportLog importLog) {
        Voyage voyage = provider.getEntity();

        long result = getImportFileIdsCount(importLog);

        // add all cell results
        result += persistenceService.countVoyageCellResults(voyage);

        // add all postCell
        result += voyage.sizePostCell();
        return result;
    }

    @Override
    protected void removePreData(DataAcousticProvider<Voyage> provider) throws TopiaException {
        Voyage voyage = provider.getEntity();

        removeVoyageCellResults(voyage);
        removeVoyagePostCell(voyage);
    }

    @Override
    protected void removeImportData(DataAcousticProvider<Voyage> provider, String id) throws TopiaException {
        Voyage voyage = provider.getEntity();

        if (id.startsWith(Echotype.class.getName())) {

            // remove echotype
            Echotype echotype = persistenceService.getEchotype(id);

            // delete all categories using this echotype
            List<Category> allByEchotype = persistenceService.getCategorysByEchotype(echotype);
            persistenceService.deleteCategories(allByEchotype);

            if (voyage != null) {

                // remove it from the voyage
                voyage.removeEchotype(echotype);
            }

            // delete echotype
            persistenceService.deleteEchotype(echotype);

            incrementOp("Remove echotype " + echotype.getTopiaId());

            if (log.isDebugEnabled()) {
                log.debug(echotype.getTopiaId() + " was removed");
            }
        } else if (id.startsWith(LengthAgeKey.class.getName())) {

            // remove lengthAgeKey
            LengthAgeKey lengthAgeKey = persistenceService.getLengthAgeKey(id);

            persistenceService.deleteLengthAgeKey(lengthAgeKey);

            if (voyage != null) {

                // remove it from the voyage
                voyage.removeLengthAgeKey(lengthAgeKey);
            }
            incrementOp("Remove lengthAgeKey " + lengthAgeKey.getTopiaId());

            if (log.isDebugEnabled()) {
                log.debug(lengthAgeKey.getTopiaId() + " was removed");
            }
        } else if (id.startsWith(LengthWeightKey.class.getName())) {

            // remove lengthWeightKey
            LengthWeightKey lengthWeightKey = persistenceService.getLengthWeightKey(id);

            persistenceService.deleteLengthWeightKey(lengthWeightKey);

            if (voyage != null) {

                // remove it from the voyage
                voyage.removeLengthWeightKey(lengthWeightKey);
            }
            incrementOp("Remove lengthWeightKey " + lengthWeightKey.getTopiaId());

            if (log.isDebugEnabled()) {
                log.debug(lengthWeightKey.getTopiaId() + " was removed");
            }
        } else {
            canNotDealWithId(id);
        }
    }

    @Override
    public Set<ImportType> getPossibleSubImportType() {
        return Sets.newHashSet(ImportType.RESULT_ESDU,
                               ImportType.RESULT_MAP_FISH,
                               ImportType.RESULT_MAP_OTHER,
                               ImportType.RESULT_REGION);
    }
}
