/*
 * #%L
 * EchoBase :: Services
 * %%
 * Copyright (C) 2011 - 2012 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ifremer.echobase.services.service.importdata.csv;

import fr.ifremer.echobase.entities.data.LengthWeightKey;
import fr.ifremer.echobase.entities.data.Voyage;
import fr.ifremer.echobase.entities.references.SizeCategory;
import fr.ifremer.echobase.entities.references.Species;
import fr.ifremer.echobase.entities.references.Strata;
import fr.ifremer.echobase.services.csv.EchoBaseCsvUtil;
import fr.ifremer.echobase.services.service.importdata.contexts.VoyageResultsImportDataContext;

/**
 * Model to import {@link LengthWeightKey}.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 0.3
 */
public class VoyageResultsVoyageLengthWeightKeyImportExportModel extends EchoBaseImportExportModelSupport<VoyageResultsVoyageLengthWeightKeyImportRow> {

    public static final String HEADER_A_PARAMETER = "aParameter";
    public static final String HEADER_B_PARAMETER = "bParameter";
    protected static final String HEADER_SPECIES = "baracoudaCode";

    private VoyageResultsVoyageLengthWeightKeyImportExportModel(char separator) {
        super(separator);
    }

    public static VoyageResultsVoyageLengthWeightKeyImportExportModel forImport(VoyageResultsImportDataContext importDataContext) {

        VoyageResultsVoyageLengthWeightKeyImportExportModel model = new VoyageResultsVoyageLengthWeightKeyImportExportModel(importDataContext.getCsvSeparator());
        model.newMandatoryColumn(HEADER_A_PARAMETER, LengthWeightKey.PROPERTY_APARAMETER, EchoBaseCsvUtil.PRIMITIVE_FLOAT);
        model.newMandatoryColumn(HEADER_B_PARAMETER, LengthWeightKey.PROPERTY_BPARAMETER, EchoBaseCsvUtil.PRIMITIVE_FLOAT);
        model.newForeignKeyColumn(VoyageResultsVoyageLengthWeightKeyImportRow.PROPERTY_VOYAGE, Voyage.class, Voyage.PROPERTY_NAME, importDataContext.getVoyagesByName());
        model.newForeignKeyColumn(VoyageResultsVoyageLengthWeightKeyImportRow.PROPERTY_SIZE_CATEGORY, SizeCategory.class, SizeCategory.PROPERTY_NAME, importDataContext.getSizeCategoriesByName());
        model.newForeignKeyColumn(HEADER_SPECIES, VoyageResultsVoyageLengthWeightKeyImportRow.PROPERTY_SPECIES, Species.class, Species.PROPERTY_BARACOUDA_CODE, importDataContext.getSpeciesByBaracoudaCode());
        model.newForeignKeyColumn(VoyageResultsVoyageLengthWeightKeyImportRow.PROPERTY_STRATA, Strata.class, Strata.PROPERTY_NAME, importDataContext.getStratasByName());
        return model;

    }

    public static VoyageResultsVoyageLengthWeightKeyImportExportModel forExport(VoyageResultsImportDataContext importDataContext) {

        VoyageResultsVoyageLengthWeightKeyImportExportModel model = new VoyageResultsVoyageLengthWeightKeyImportExportModel(importDataContext.getCsvSeparator());
        model.newColumnForExport(HEADER_A_PARAMETER, LengthWeightKey.PROPERTY_APARAMETER, EchoBaseCsvUtil.PRIMITIVE_FLOAT);
        model.newColumnForExport(HEADER_B_PARAMETER, LengthWeightKey.PROPERTY_BPARAMETER, EchoBaseCsvUtil.PRIMITIVE_FLOAT);
        model.newColumnForExport(VoyageResultsVoyageLengthWeightKeyImportRow.PROPERTY_VOYAGE, EchoBaseCsvUtil.VOYAGE_FORMATTER);
        model.newColumnForExport(VoyageResultsVoyageLengthWeightKeyImportRow.PROPERTY_SIZE_CATEGORY, EchoBaseCsvUtil.SIZE_CATEGORY_FORMATTER);
        model.newColumnForExport(HEADER_SPECIES, VoyageResultsVoyageLengthWeightKeyImportRow.PROPERTY_SPECIES, EchoBaseCsvUtil.SPECIES_FORMATTER);
        model.newColumnForExport(VoyageResultsVoyageLengthWeightKeyImportRow.PROPERTY_STRATA, EchoBaseCsvUtil.STRATA_FORMATTER);
        return model;

    }

    @Override
    public VoyageResultsVoyageLengthWeightKeyImportRow newEmptyInstance() {
        return new VoyageResultsVoyageLengthWeightKeyImportRow();
    }
}
