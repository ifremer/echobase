/*
 * #%L
 * EchoBase :: Services
 * %%
 * Copyright (C) 2011 - 2012 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ifremer.echobase.services.service.importdata.configurations;

import fr.ifremer.echobase.io.InputFile;
import fr.ifremer.echobase.services.csv.EchoBaseCsvUtil;
import fr.ifremer.echobase.services.service.importdata.CellPositionReference;

import java.util.Locale;

import static org.nuiton.i18n.I18n.l;

/**
 * Configuration of a "accoustic datas" import.
 *
 * @author Julien Ruchaud - ruchaud@codelutin.com
 * @since 4.0
 */
public class ImportAcousticsConfiguration extends ImportDataConfigurationSupport {

    private static final long serialVersionUID = 1L;
    /** Movies file to import. */
    protected final InputFile moviesFile;
    /** Calibrations file to import. */
    protected final InputFile calibrationsFile;
    /** Flag to always add new dataAcquisition when a new instrument is found in movies file. */
    protected boolean addDataAcquisition = true;
    /** Manual transceiverAcquisitionAbsorptionDescription. */
    protected String transceiverAcquisitionAbsorptionDescription = "(i) Equation: Francois and garrison 1982,(ii) CTD, (iii) nominal value for entire data set";
    /** Manual acquisitionSoftwareName. */
    protected String acquisitionSoftwareName = "ER60";
    /** Manual acquisitionSoftwareVersion (ER60 instrument). */
    protected String acquisitionSoftwareVersionER60;
    /** Manual acquisitionSoftwareVersion (ME70 instrument). */
    protected String acquisitionSoftwareVersionME70;
    /** Manual loggedDataFormat. */
    protected String loggedDataFormat = ".hac and .raw formats";
    /** Manual loggedDataDatatype. */
    protected String loggedDataDatatype = "‘raw’ digitisation samples";
    /** Manual pingDutyCycle. */
    protected String pingDutyCycle = "Ifremer's standard ping duty cycle";
    /** Manual soundSpeedCalculations (ER60 instrument). */
    protected String soundSpeedCalculationsER60 = "(i) Equation: Mackenzie (1980), (ii) CTD, (iii) nominal value for entire data set";
    /** Manual soundSpeedCalculations (ME70 instrument). */
    protected String soundSpeedCalculationsME70 = "(i) Equation: Mackenzie (1980), (ii) Hull-mounted thermosalinometer, (iii) surface absorption value recomputed every 30s and applied to the entire data set";
    /** Manual sounderConstant. */
    protected String sounderConstant = EchoBaseCsvUtil.NA;
    /** Manual processingSoftwareName. */
    protected String processingSoftwareName = "Movies3D";
    /** Manual processingSoftwareVersion. */
    protected String processingSoftwareVersion;
    /** Manual processingTemplate. */
    protected String processingTemplate;
    /** Manual processingDescription. */
    protected String processingDescription;
    /** Manual digitThreshold. */
    protected float digitThreshold = -100f;
    /** Manual acousticDensityUnit. */
    protected String acousticDensityUnit = "sA";
    /** Manual notes. */
    protected String notes;
    /** Cell position reference for esdu cell datas. */
    protected CellPositionReference cellPositionReference;

    public ImportAcousticsConfiguration(Locale locale) {
        moviesFile = InputFile.newFile(l(locale, "echobase.common.moviesFile"));
        calibrationsFile = InputFile.newFile(l(locale, "echobase.common.calibrationsFile"));
    }

    public boolean isAddDataAcquisition() {
        return addDataAcquisition;
    }

    public void setAddDataAcquisition(boolean addDataAcquisition) {
        this.addDataAcquisition = addDataAcquisition;
    }

    public String getTransceiverAcquisitionAbsorptionDescription() {
        return transceiverAcquisitionAbsorptionDescription;
    }

    public void setTransceiverAcquisitionAbsorptionDescription(String transceiverAcquisitionAbsorptionDescription) {
        this.transceiverAcquisitionAbsorptionDescription = transceiverAcquisitionAbsorptionDescription;
    }

    public String getAcquisitionSoftwareVersionER60() {
        return acquisitionSoftwareVersionER60;
    }

    public void setAcquisitionSoftwareVersionER60(String acquisitionSoftwareVersionER60) {
        this.acquisitionSoftwareVersionER60 = acquisitionSoftwareVersionER60;
    }

    public String getAcquisitionSoftwareVersionME70() {
        return acquisitionSoftwareVersionME70;
    }

    public void setAcquisitionSoftwareVersionME70(String acquisitionSoftwareVersionME70) {
        this.acquisitionSoftwareVersionME70 = acquisitionSoftwareVersionME70;
    }

    public String getLoggedDataFormat() {
        return loggedDataFormat;
    }

    public void setLoggedDataFormat(String loggedDataFormat) {
        this.loggedDataFormat = loggedDataFormat;
    }

    public String getLoggedDataDatatype() {
        return loggedDataDatatype;
    }

    public void setLoggedDataDatatype(String loggedDataDatatype) {
        this.loggedDataDatatype = loggedDataDatatype;
    }

    public String getPingDutyCycle() {
        return pingDutyCycle;
    }

    public void setPingDutyCycle(String pingDutyCycle) {
        this.pingDutyCycle = pingDutyCycle;
    }

    public String getSoundSpeedCalculationsER60() {
        return soundSpeedCalculationsER60;
    }

    public void setSoundSpeedCalculationsER60(String soundSpeedCalculationsER60) {
        this.soundSpeedCalculationsER60 = soundSpeedCalculationsER60;
    }

    public String getSoundSpeedCalculationsME70() {
        return soundSpeedCalculationsME70;
    }

    public void setSoundSpeedCalculationsME70(String soundSpeedCalculationsME70) {
        this.soundSpeedCalculationsME70 = soundSpeedCalculationsME70;
    }

    public String getSounderConstant() {
        return sounderConstant;
    }

    public void setSounderConstant(String sounderConstant) {
        this.sounderConstant = sounderConstant;
    }

    public float getDigitThreshold() {
        return digitThreshold;
    }

    public void setDigitThreshold(float digitThreshold) {
        this.digitThreshold = digitThreshold;
    }

    public String getAcousticDensityUnit() {
        return acousticDensityUnit;
    }

    public void setAcousticDensityUnit(String acousticDensityUnit) {
        this.acousticDensityUnit = acousticDensityUnit;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public InputFile getMoviesFile() {
        return moviesFile;
    }

    public InputFile getCalibrationsFile() {
        return calibrationsFile;
    }

    public String getProcessingTemplate() {
        return processingTemplate;
    }

    public void setProcessingTemplate(String processingTemplate) {
        this.processingTemplate = processingTemplate;
    }

    public String getProcessingDescription() {
        return processingDescription;
    }

    public void setProcessingDescription(String processingDescription) {
        this.processingDescription = processingDescription;
    }

    public CellPositionReference getCellPositionReference() {
        return cellPositionReference;
    }

    public void setCellPositionReference(CellPositionReference cellPositionReference) {
        this.cellPositionReference = cellPositionReference;
    }

    public String getAcquisitionSoftwareName() {
        return acquisitionSoftwareName;
    }

    public void setAcquisitionSoftwareName(String acquisitionSoftwareName) {
        this.acquisitionSoftwareName = acquisitionSoftwareName;
    }

    public String getProcessingSoftwareName() {
        return processingSoftwareName;
    }

    public void setProcessingSoftwareName(String processingSoftwareName) {
        this.processingSoftwareName = processingSoftwareName;
    }

    public String getProcessingSoftwareVersion() {
        return processingSoftwareVersion;
    }

    public void setProcessingSoftwareVersion(String processingSoftwareVersion) {
        this.processingSoftwareVersion = processingSoftwareVersion;
    }
    
    @Override
    public InputFile[] getInputFiles() {
        return new InputFile[]{moviesFile, calibrationsFile};
    }
}
