/*
 * #%L
 * EchoBase :: Services
 * %%
 * Copyright (C) 2011 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ifremer.echobase.services.service;

import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import fr.ifremer.echobase.I18nAble;
import fr.ifremer.echobase.entities.EchoBaseUser;
import fr.ifremer.echobase.entities.ExportQuery;
import fr.ifremer.echobase.entities.ImportLog;
import fr.ifremer.echobase.entities.WorkingDbConfiguration;
import fr.ifremer.echobase.entities.data.Category;
import fr.ifremer.echobase.entities.data.Cell;
import fr.ifremer.echobase.entities.data.Data;
import fr.ifremer.echobase.entities.data.DataAcquisition;
import fr.ifremer.echobase.entities.data.DataProcessing;
import fr.ifremer.echobase.entities.data.Echotype;
import fr.ifremer.echobase.entities.data.GearMetadataValue;
import fr.ifremer.echobase.entities.data.LengthAgeKey;
import fr.ifremer.echobase.entities.data.LengthWeightKey;
import fr.ifremer.echobase.entities.data.Mooring;
import fr.ifremer.echobase.entities.data.Operation;
import fr.ifremer.echobase.entities.data.OperationMetadataValue;
import fr.ifremer.echobase.entities.data.Result;
import fr.ifremer.echobase.entities.data.Sample;
import fr.ifremer.echobase.entities.data.SampleData;
import fr.ifremer.echobase.entities.data.Transect;
import fr.ifremer.echobase.entities.data.Transit;
import fr.ifremer.echobase.entities.data.Voyage;
import fr.ifremer.echobase.entities.references.AcousticInstrument;
import fr.ifremer.echobase.entities.references.AgeCategory;
import fr.ifremer.echobase.entities.references.AncillaryInstrumentation;
import fr.ifremer.echobase.entities.references.AreaOfOperation;
import fr.ifremer.echobase.entities.references.Calibration;
import fr.ifremer.echobase.entities.references.CategoryMeaning;
import fr.ifremer.echobase.entities.references.CategoryRef;
import fr.ifremer.echobase.entities.references.CategoryType;
import fr.ifremer.echobase.entities.references.CellMethod;
import fr.ifremer.echobase.entities.references.CellType;
import fr.ifremer.echobase.entities.references.DataMetadata;
import fr.ifremer.echobase.entities.references.DataProtocol;
import fr.ifremer.echobase.entities.references.DataQuality;
import fr.ifremer.echobase.entities.references.DataType;
import fr.ifremer.echobase.entities.references.DepthStratum;
import fr.ifremer.echobase.entities.references.EchotypeCategory;
import fr.ifremer.echobase.entities.references.Gear;
import fr.ifremer.echobase.entities.references.GearCharacteristic;
import fr.ifremer.echobase.entities.references.GearCharacteristicValue;
import fr.ifremer.echobase.entities.references.GearMetadata;
import fr.ifremer.echobase.entities.references.Impacte;
import fr.ifremer.echobase.entities.references.MeasureType;
import fr.ifremer.echobase.entities.references.MeasurementMetadata;
import fr.ifremer.echobase.entities.references.Mission;
import fr.ifremer.echobase.entities.references.OperationEvent;
import fr.ifremer.echobase.entities.references.OperationMetadata;
import fr.ifremer.echobase.entities.references.Port;
import fr.ifremer.echobase.entities.references.ReferenceDatum;
import fr.ifremer.echobase.entities.references.ReferenceDatumType;
import fr.ifremer.echobase.entities.references.ReferencingMethod;
import fr.ifremer.echobase.entities.references.SampleDataType;
import fr.ifremer.echobase.entities.references.SampleType;
import fr.ifremer.echobase.entities.references.SexCategory;
import fr.ifremer.echobase.entities.references.SizeCategory;
import fr.ifremer.echobase.entities.references.Species;
import fr.ifremer.echobase.entities.references.SpeciesCategory;
import fr.ifremer.echobase.entities.references.Strata;
import fr.ifremer.echobase.entities.references.TSParameters;
import fr.ifremer.echobase.entities.references.Vessel;
import fr.ifremer.echobase.entities.references.VesselType;
import fr.ifremer.echobase.entities.references.VocabularyCIEM;
import fr.ifremer.echobase.services.EchoBaseServiceSupport;
import org.apache.commons.collections4.CollectionUtils;
import org.nuiton.decorator.Decorator;
import org.nuiton.decorator.DecoratorMulti18nProvider;
import org.nuiton.decorator.DecoratorUtil;
import org.nuiton.decorator.JXPathDecorator;
import org.nuiton.topia.persistence.TopiaEntity;

import java.util.Collection;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import static org.nuiton.i18n.I18n.l;

/**
 * Service to decorate entities.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 0.1
 */
public class DecoratorService extends EchoBaseServiceSupport {

    public static final String DATE_ONLY = "dateOnly";

    protected final DecoratorMulti18nProvider decoratorProvider;

    public DecoratorService() {
        decoratorProvider = new EchoBaseDecoratorProvider();
    }

    public <O> Decorator<O> getDecorator(Class<O> type,
                                         String context) {
        Preconditions.checkNotNull(type, "Decorator type can not be null!");
        Decorator<O> decorator = decoratorProvider.getDecoratorByType(getLocale(), type, context);
        Preconditions.checkNotNull(
                decorator,
                "Could not find decorator for type " + type +
                " and context " + context
        );
        return decorator;
    }

    public String decorate(Object o, String context) {
        Decorator<?> decorator = getDecorator(o.getClass(), context);
        return decorator.toString(o);
    }

    public final <E extends Enum<E> & I18nAble> Map<String, String> decorateEnums(E... enumValues) {
        Locale locale = getLocale();
        Map<String, String> result = Maps.newLinkedHashMap();
        for (E enumValue : enumValues) {
            result.put(enumValue.name(), l(locale, enumValue.getI18nKey()));
        }
        return result;
    }

//    public <O> List<O> sortToList(Locale locale, Collection<O> beans, String context) {
//        Preconditions.checkNotNull(beans);
//        List<O> list = Lists.newArrayList(beans);
//        getDecoratorAndSort(locale, context, list);
//        return list;
//    }

    public <E extends TopiaEntity> Map<String, String> sortAndDecorate(Collection<E> beans,
                                                                       String context) {
        Preconditions.checkNotNull(beans);
        List<E> list = Lists.newArrayList(beans);

        Decorator<E> decorator = getDecoratorAndSort(context, list);
        Map<String, String> result = Maps.newLinkedHashMap();
        for (E bean : list) {
            result.put(bean.getTopiaId(), decorator.toString(bean));
        }
        return result;
    }

    public <E extends TopiaEntity> void decorateForeignKey(
            Map data,
            String property,
            E entity,
            String decoratorContext) {

        String voyageToString = decorate(entity, decoratorContext);
        data.put(property + "_lbl", voyageToString);
    }


    protected <O> Decorator<O> getDecoratorAndSort(String context,
                                                   List<O> list) {
        Decorator<O> decorator = null;
        if (CollectionUtils.isNotEmpty(list)) {
            O object = list.get(0);
            Preconditions.checkNotNull(object);
            decorator = decoratorProvider.getDecorator(getLocale(), object, context);
            Preconditions.checkNotNull(
                    decorator,
                    "Could not find decorator for type " + object.getClass() +
                    " and context " + context
            );
            DecoratorUtil.sort((JXPathDecorator<O>) decorator, list, 0);
        }
        return decorator;
    }

    static class EchoBaseDecoratorProvider extends DecoratorMulti18nProvider {

        @Override
        protected void loadDecorators(Locale locale) {

            // EchoBaseUser decorator
            registerJXPathDecorator(locale, EchoBaseUser.class, "${email}$s");

            // ExportQuery decorator
            registerJXPathDecorator(locale, ExportQuery.class, "${name}$s - ${description}$s");

            // ImportLog decorator
            registerJXPathDecorator(locale, ImportLog.class, "${importType}$s - ${importDate}$s - ${importText}$s");

            // ImportLog decorator
            registerJXPathDecorator(locale, ImportLog.class, DATE_ONLY, "${importDate}$s");


            // WorkingDbConfiguration decorator
            registerJXPathDecorator(locale, WorkingDbConfiguration.class, "${url}$s - (${description}$s)");

            // AcousticInstrument decorator
            registerJXPathDecorator(locale, AcousticInstrument.class, "${id}$s");

            // AgeCategory decorator
            registerJXPathDecorator(locale, AgeCategory.class, "${name}$s");

            // AreaOfOperation decorator
            registerJXPathDecorator(locale, AreaOfOperation.class, "${name}$s");

            // Calibration decorator
            registerJXPathDecorator(locale, Calibration.class, "${date}$s - acquisition ${aquisitionMethod}$s");

            // Category decorator
            registerJXPathDecorator(locale, Category.class, "${echotypeLabel}$s ${speciesLabel}$s ${processedAgeLabel}$s ${processedLengthLabel}$s");

            // CategoryMeaning decorator
            registerJXPathDecorator(locale, CategoryMeaning.class, "${name}$s");

            // CategoryRef decorator
            registerJXPathDecorator(locale, CategoryRef.class, "${genusSpecies}$s");

            // CategoryType decorator
            registerJXPathDecorator(locale, CategoryType.class, "${name}$s");

            // CellMethod decorator
            registerJXPathDecorator(locale, CellMethod.class, "${name}$s");

            // CellType decorator
            registerJXPathDecorator(locale, CellType.class, "${id}$s (${name}$s)");

            // DataMetadata decorator
            registerJXPathDecorator(locale, DataMetadata.class, "${name}$s (${longName}$s)");
            //registerJXPathDecorator(locale, DataMetadata.class, "${name}$s");

            // DataProtocol decorator
            registerJXPathDecorator(locale, DataProtocol.class, "${description}$s [ ${validSince}$td/%2$tm/%2$tY - ${invalidSince}$td/%3$tm/%3$tY ]");

            // DataQuality decorator
            //registerJXPathDecorator(locale, DataQuality.class, "${qualityDataFlagValues}$s - ${flagMeanings}$s");
            registerJXPathDecorator(locale, DataQuality.class, "${flagMeanings}$s");

            // DataType decorator
            registerJXPathDecorator(locale, DataType.class, "${name}$s");

            // DepthStratum decorator
            //registerJXPathDecorator(locale, DepthStratum.class, "${id}$s - ${meaning}$s");
            registerJXPathDecorator(locale, DepthStratum.class, "${meaning}$s");

            // Echotype decorator
            //registerJXPathDecorator(locale, Echotype.class, "${id}$s - ${name}$s");
            registerJXPathDecorator(locale, Echotype.class, "${name}$s");

            // EchotypeCategory decorator
            registerJXPathDecorator(locale, EchotypeCategory.class, "${name}$s");

            // Gear decorator
            //registerJXPathDecorator(locale, Gear.class, "${name}$s [ ${validSince}$td/%2$tm/%2$tY -  ${invalidSince}$td/%3$tm/%3$tY ]");
            registerJXPathDecorator(locale, Gear.class, "${name}$s");

            // GearCaracteristic decorator
            registerJXPathDecorator(locale, GearCharacteristic.class, "${name}$s");

            // GearMetadata decorator
            //registerJXPathDecorator(locale, GearMetadata.class, "${name}$s (${operationEvent/name}$s)");
            registerJXPathDecorator(locale, GearMetadata.class, "${name}$s");

            // GearCharacteristicValue decorator
            registerJXPathDecorator(locale, GearCharacteristicValue.class, "${dataValue}$s");

            // GearMetadataValue decorator
            registerJXPathDecorator(locale, GearMetadataValue.class, "${dataValue}$s");

            // Impacte decorator
            //registerJXPathDecorator(locale, Impacte.class, "${measurementUnit}$s - ${species/genusSpecies}");
            registerJXPathDecorator(locale, Impacte.class, "${measurementUnit}$s");

            // LengthAgeKey decorator
            registerJXPathDecorator(locale, LengthAgeKey.class, "Age ${age}$s - Length ${length}");

            // MeasurementMetadata decorator
            registerJXPathDecorator(locale, MeasurementMetadata.class, "${name}$s");

            // MeasureType decorator
            registerJXPathDecorator(locale, MeasureType.class, "${name}$s");

            // Mission decorator
            registerJXPathDecorator(locale, Mission.class, "${name}$s");

            // OperationEvent decorator
            registerJXPathDecorator(locale, OperationEvent.class, "${name}$s");

            // OperationMetadata decorator
            registerJXPathDecorator(locale, OperationMetadata.class, "${name}$s");

            // ReferenceDatum decorator
            registerJXPathDecorator(locale, ReferenceDatum.class, "${id}$s");

            // ReferenceDatumType decorator
            registerJXPathDecorator(locale, ReferenceDatumType.class, "${name}$s");

            // ReferencingMethod decorator
            registerJXPathDecorator(locale, ReferencingMethod.class, "${name}$s");

            // SampleDataType decorator
            registerJXPathDecorator(locale, SampleDataType.class, "${name}$s");

            // SampleType decorator
            //registerJXPathDecorator(locale, SampleType.class, "${name}$s - level ${level}$s");
            registerJXPathDecorator(locale, SampleType.class, "${name}$s");

            // SexCategory decorator
            registerJXPathDecorator(locale, SexCategory.class, "${name}$s");

            // SizeCategory decorator
            registerJXPathDecorator(locale, SizeCategory.class, "${name}$s");

            // Species decorator
            //registerJXPathDecorator(locale, Species.class, "${genusSpecies}$s (${baracoudaCode}$s)");
            registerJXPathDecorator(locale, Species.class, "${baracoudaCode}$s (${genusSpecies}$s)");

            // SpeciesCategory decorator
            registerJXPathDecorator(locale, SpeciesCategory.class, "${species/genusSpecies}$s ${sizeCategoryLabel}$s ${ageCategoryLabel}$s ${sexCategoryLabel}$s");

            // Strata decorator
            registerJXPathDecorator(locale, Strata.class, "Latitude <${minLatitude}$s - ${maxLatitude}$s>, Longitude <${minLongitude}$s - ${maxLongitude}$s>");

            //TODO
            // TSParameters decorator
            registerJXPathDecorator(locale, TSParameters.class, "${}$s");

            // Vessel decorator
            registerJXPathDecorator(locale, Vessel.class, "${name}$s");

            // VesselType decorator
            registerJXPathDecorator(locale, VesselType.class, "${name}$s");

            // LengthWeightKey decorator
            registerJXPathDecorator(locale, LengthWeightKey.class, "Species ${species/genusSpecies}$s - a=${aParameter}$s, b=${bParameter}$s");

            // Cell decorator
            registerJXPathDecorator(locale, Cell.class, "${name}$s");

            // Data decorator
            registerJXPathDecorator(locale, Data.class, "${dataValue}$s");

            // DataAcquisition decorator
            registerJXPathDecorator(locale, DataAcquisition.class, "${acousticInstrument/id}$s ");

            // DataProcessing decorator
            registerJXPathDecorator(locale, DataProcessing.class, "${id}$s");

            // Operation decorator
            registerJXPathDecorator(locale, Operation.class, "${startDate}$td/%1$tm/%1$tY - ${endDate}$td/%2$tm/%2$tY");

            // OperationMetadataValue decorator
            registerJXPathDecorator(locale, OperationMetadataValue.class, "${operationMetadata/name} - ${dataValue}$s");

            // Result decorator
            registerJXPathDecorator(locale, Result.class, "${resultvalue}$s");

            // Sample decorator
            registerJXPathDecorator(locale, Sample.class, "${resultvalue}$s");

            // SampleData decorator
            registerJXPathDecorator(locale, SampleData.class, "${resultvalue}$s");

            // Transect decorator
            registerJXPathDecorator(locale, Transect.class, "${title}$s - Vessel ${vessel/name}$s");

            // Transit decorator
            registerJXPathDecorator(locale, Transit.class, "${startTime}$td/%1$tm/%1$tY - ${endTime}$td/%2$tm/%2$tY");

            // Voyage decorator
            registerJXPathDecorator(locale, Voyage.class, "${name}$s [ ${startDate}$td/%2$tm/%2$tY - ${endDate}$td/%3$tm/%3$tY ]");

            // Port decorator
            registerJXPathDecorator(locale, Port.class, "${name}$s");
            
            // Mooring decorator
            registerJXPathDecorator(locale, Mooring.class, "${code}$s");
                        
            // AncillaryInstrumentation decorator
            registerJXPathDecorator(locale, AncillaryInstrumentation.class, "${name}$s");
                        
            // AncillaryInstrumentation decorator
            registerJXPathDecorator(locale, VocabularyCIEM.class, "${label}$s");
        }
    }

}
