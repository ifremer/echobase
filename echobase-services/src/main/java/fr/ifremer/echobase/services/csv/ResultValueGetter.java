package fr.ifremer.echobase.services.csv;

/*
 * #%L
 * EchoBase :: Services
 * %%
 * Copyright (C) 2011 - 2016 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.echobase.entities.data.Result;
import fr.ifremer.echobase.entities.references.DataMetadata;
import org.nuiton.csv.ValueGetter;

/**
 * Created on 30/03/16.
 *
 * @author Tony Chemit - chemit@codelutin.com
 */
class ResultValueGetter<B extends ResultAble> implements ValueGetter<B, Result> {

    private final DataMetadata metadata;

    ResultValueGetter(DataMetadata metadata) {
        this.metadata = metadata;
    }

    @Override
    public Result get(B object) throws Exception {
        for (Result result : object.getResult()) {
            if (metadata.equals(result.getDataMetadata())) {
                return result;
            }
        }
        return null;
    }
}
