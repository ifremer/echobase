package fr.ifremer.echobase.services.service.importdata.actions;

/*
 * #%L
 * EchoBase :: Services
 * %%
 * Copyright (C) 2011 - 2016 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.echobase.entities.data.DataAcousticProvider;
import fr.ifremer.echobase.entities.data.Category;
import fr.ifremer.echobase.entities.data.Cell;
import fr.ifremer.echobase.entities.data.Result;
import fr.ifremer.echobase.services.service.importdata.ImportDataFileResult;
import fr.ifremer.echobase.services.service.importdata.ResultCategoryCache;
import fr.ifremer.echobase.services.service.importdata.contexts.MooringResultsImportDataContext;
import fr.ifremer.echobase.services.service.importdata.csv.MooringResultsEsduByEchotypeAndSpeciesCategoryImportExportModel;
import fr.ifremer.echobase.services.service.importdata.csv.MooringResultsEsduByEchotypeAndSpeciesCategoryImportRow;

import java.util.List;

/**
 * @author Julien Ruchaud - ruchaud@codelutin.com
 * @since 4.0
 */
public class MooringResultsEsduByEchotypeAndSpeciesCategoryCellImportAction extends MooringResultsCellImportDataActionSupport<MooringResultsEsduByEchotypeAndSpeciesCategoryImportRow> {

    public MooringResultsEsduByEchotypeAndSpeciesCategoryCellImportAction(MooringResultsImportDataContext importDataContext) {
        super(importDataContext, importDataContext.getConfiguration().getEsduByEchotypeAndSpeciesCategoryFile(), MooringResultsEsduByEchotypeAndSpeciesCategoryImportExportModel.COLUMN_NAMES_TO_EXCLUDE);
    }

    @Override
    protected MooringResultsEsduByEchotypeAndSpeciesCategoryImportExportModel createCsvImportModel(MooringResultsImportDataContext importDataContext) {
        return MooringResultsEsduByEchotypeAndSpeciesCategoryImportExportModel.forImport(importDataContext, metas);
    }

    @Override
    protected MooringResultsEsduByEchotypeAndSpeciesCategoryImportExportModel createCsvExportModel(MooringResultsImportDataContext importDataContext) {
        return MooringResultsEsduByEchotypeAndSpeciesCategoryImportExportModel.forExport(importDataContext, metas);
    }

    @Override
    protected Category getResultCategory(ImportDataFileResult result, ResultCategoryCache resultCategoryCache, MooringResultsEsduByEchotypeAndSpeciesCategoryImportRow row) {
        return resultCategoryCache.getResultCategory(row.getEchotype(),
                                                     row.getSpecies(),
                                                     null,
                                                     row.getSizeCategory(),
                                                     null,
                                                     result);
    }

    @Override
    protected MooringResultsEsduByEchotypeAndSpeciesCategoryImportRow newImportedRow(DataAcousticProvider voyage, Cell cell, Category category, List<Result> cellResults) {
        return MooringResultsEsduByEchotypeAndSpeciesCategoryImportRow.of(voyage, cell, category, cellResults);
    }

}
