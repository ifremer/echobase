package fr.ifremer.echobase.services.service.exportdb;

/*
 * #%L
 * EchoBase :: Services
 * %%
 * Copyright (C) 2011 - 2012 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.echobase.I18nAble;

import static org.nuiton.i18n.I18n.n;

/**
 * Db export mode.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.2
 */
public enum ExportDbMode implements I18nAble {

    /** Export only referential. */
    REFERENTIAL(n("echobase.common.exportDbMode.referential"), true, false),

    /** Export selected data + referential. */
    REFERENTIAL_AND_DATA(n("echobase.common.exportDbMode.referentialAndData"), true, true),

    /** Export all the db. */
    ALL(n("echobase.common.exportDbMode.all"), true, true);

    private final String i18nKey;

    private final boolean exportReferential;

    private final boolean exportData;

    ExportDbMode(String i18nKey, boolean exportReferential, boolean exportData) {
        this.i18nKey = i18nKey;
        this.exportReferential = exportReferential;
        this.exportData = exportData;
    }

    @Override
    public String getI18nKey() {
        return i18nKey;
    }

    public boolean isExportReferential() {
        return exportReferential;
    }

    public boolean isExportData() {
        return exportData;
    }
}
