/*
 * #%L
 * EchoBase :: Services
 * %%
 * Copyright (C) 2011 - 2012 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ifremer.echobase.services.service.importdata.configurations;

import fr.ifremer.echobase.entities.ImportType;
import fr.ifremer.echobase.io.InputFile;
import java.util.Locale;
import static org.nuiton.i18n.I18n.l;

/**
 * Configuration mooring import.
 *
 * @author Julien Ruchaud - ruchaud@codelutin.com
 * @since 4.0
 */
public class MooringCommonsAncillaryInstrumentationImportConfiguration extends MooringImportDataConfigurationSupport {

    private static final long serialVersionUID = 1L;
    
    protected final InputFile ancillaryInstrumentationFile;

    public MooringCommonsAncillaryInstrumentationImportConfiguration(Locale locale) {
        ancillaryInstrumentationFile = InputFile.newFile(l(locale, "echobase.common.ancillaryInstrumentationFile"));
        importType = ImportType.MOORING_ANCILLARY_INSTRUMENTATION;
    }

    public InputFile getAncillaryInstrumentationFile() {
        return ancillaryInstrumentationFile;
    }

    @Override
    public InputFile[] getInputFiles() {
        return new InputFile[]{ancillaryInstrumentationFile};
    }
}
