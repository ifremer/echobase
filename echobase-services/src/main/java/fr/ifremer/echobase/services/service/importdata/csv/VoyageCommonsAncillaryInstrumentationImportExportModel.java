/*
 * #%L
 * EchoBase :: Services
 * %%
 * Copyright (C) 2011 - 2012 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ifremer.echobase.services.service.importdata.csv;

import fr.ifremer.echobase.entities.data.Voyage;
import fr.ifremer.echobase.entities.references.AncillaryInstrumentation;
import fr.ifremer.echobase.entities.references.Vessel;
import fr.ifremer.echobase.services.csv.EchoBaseCsvUtil;
import fr.ifremer.echobase.services.service.importdata.contexts.VoyageCommonsAncillaryInstrumentationImportDataContext;

/**
 * @author Julien Ruchaud - ruchaud@codelutin.com
 * @since 4.0
 */
public class VoyageCommonsAncillaryInstrumentationImportExportModel extends EchoBaseImportExportModelSupport<VoyageCommonsAncillaryInstrumentationImportRow> {

    private VoyageCommonsAncillaryInstrumentationImportExportModel(char separator) {
        super(separator);
    }

    public static VoyageCommonsAncillaryInstrumentationImportExportModel forImport(VoyageCommonsAncillaryInstrumentationImportDataContext importDataContext) {
        VoyageCommonsAncillaryInstrumentationImportExportModel model = new VoyageCommonsAncillaryInstrumentationImportExportModel(importDataContext.getCsvSeparator());

        model.newForeignKeyColumn(VoyageCommonsAncillaryInstrumentationImportRow.PROPERTY_VOYAGE, Voyage.class, Voyage.PROPERTY_NAME, importDataContext.getVoyagesByName());
        model.newForeignKeyColumn(VoyageCommonsAncillaryInstrumentationImportRow.PROPERTY_VESSEL, Vessel.class, Vessel.PROPERTY_NAME, importDataContext.getVesselsByName());
        model.newForeignKeyColumn(VoyageCommonsAncillaryInstrumentationImportRow.PROPERTY_ANCILLARY_INSTRUMENTATION, AncillaryInstrumentation.class, AncillaryInstrumentation.PROPERTY_NAME, importDataContext.getAncillaryInstrumentationsByName());

        return model;
    }

    public static VoyageCommonsAncillaryInstrumentationImportExportModel forExport(VoyageCommonsAncillaryInstrumentationImportDataContext importDataContext) {
        VoyageCommonsAncillaryInstrumentationImportExportModel model = new VoyageCommonsAncillaryInstrumentationImportExportModel(importDataContext.getCsvSeparator());

        model.newColumnForExport(VoyageCommonsAncillaryInstrumentationImportRow.PROPERTY_VOYAGE, EchoBaseCsvUtil.VOYAGE_FORMATTER);
        model.newColumnForExport(VoyageCommonsAncillaryInstrumentationImportRow.PROPERTY_VESSEL, EchoBaseCsvUtil.VESSEL_FORMATTER);
        model.newColumnForExport(VoyageCommonsAncillaryInstrumentationImportRow.PROPERTY_ANCILLARY_INSTRUMENTATION, EchoBaseCsvUtil.ANCILLARY_INSTRUMENTATION_FORMATTER);

        return model;
    }

    @Override
    public VoyageCommonsAncillaryInstrumentationImportRow newEmptyInstance() {
        return new VoyageCommonsAncillaryInstrumentationImportRow();
    }
}
