/*
 * #%L
 * EchoBase :: Services
 * %%
 * Copyright (C) 2011 - 2012 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ifremer.echobase.services.service.importdata.csv;

import fr.ifremer.echobase.entities.data.DataAcousticProvider;
import fr.ifremer.echobase.entities.data.Echotype;
import fr.ifremer.echobase.entities.data.Mooring;
import fr.ifremer.echobase.entities.references.Species;

/**
 * Bean used as a row for import of {@link MooringResultsEchotypeImportExportModel}.
 *
 * @author Julien Ruchaud - ruchaud@codelutin.com
 * @since 4.0
 */
public class MooringResultsEchotypeImportRow extends ResultsEchotypeImportRow<Mooring> {

    private static final long serialVersionUID = 1L;

    public static final String PROPERTY_MOORING = "mooring";
    protected Mooring mooring;

    public static MooringResultsEchotypeImportRow of(DataAcousticProvider provider, Echotype echotype, Species species) {
        MooringResultsEchotypeImportRow row = new MooringResultsEchotypeImportRow(echotype);
        row.setProvider(provider);
        row.setSpecies(species);
        return row;
    }

    public MooringResultsEchotypeImportRow() {
        super();
    }

    public MooringResultsEchotypeImportRow(Echotype echotype) {
        super(echotype);
    }

    public Mooring getMooring() {
        return mooring;
    }

    public void setMooring(Mooring mooring) {
        this.mooring = mooring;
    }

    @Override
    public DataAcousticProvider<Mooring> getProvider() {
        return mooring;
    }

    @Override
    public void setProvider(DataAcousticProvider<Mooring> provider) {
        this.mooring = provider.getEntity();
    }
}
