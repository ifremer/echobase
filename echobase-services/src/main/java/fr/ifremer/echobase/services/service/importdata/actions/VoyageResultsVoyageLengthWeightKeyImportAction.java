package fr.ifremer.echobase.services.service.importdata.actions;

/*
 * #%L
 * EchoBase :: Services
 * %%
 * Copyright (C) 2011 - 2016 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.echobase.entities.EchoBaseUserEntityEnum;
import fr.ifremer.echobase.entities.data.LengthWeightKey;
import fr.ifremer.echobase.entities.data.Voyage;
import fr.ifremer.echobase.entities.references.SpeciesCategory;
import fr.ifremer.echobase.io.InputFile;
import fr.ifremer.echobase.services.service.importdata.DuplicatedResultsVoyageLengthWeightKeyException;
import fr.ifremer.echobase.services.service.importdata.ImportDataFileResult;
import fr.ifremer.echobase.services.service.importdata.MismatchProviderException;
import fr.ifremer.echobase.services.service.importdata.SpeciesCategoryCache;
import fr.ifremer.echobase.services.service.importdata.contexts.VoyageResultsImportDataContext;
import fr.ifremer.echobase.services.service.importdata.csv.VoyageResultsVoyageLengthWeightKeyImportExportModel;
import fr.ifremer.echobase.services.service.importdata.csv.VoyageResultsVoyageLengthWeightKeyImportRow;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.csv.Import;

import java.util.Set;
import java.util.TreeSet;

/**
 * Created on 25/03/16.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 4.0
 */
public class VoyageResultsVoyageLengthWeightKeyImportAction extends VoyageResultsImportDataActionSupport<VoyageResultsVoyageLengthWeightKeyImportRow> {

    /** Logger. */
    private static final Log log = LogFactory.getLog(VoyageResultsVoyageLengthWeightKeyImportAction.class);

    public VoyageResultsVoyageLengthWeightKeyImportAction(VoyageResultsImportDataContext importDataContext) {
        super(importDataContext, importDataContext.getConfiguration().getLengthWeightKeyFile());
    }

    @Override
    protected VoyageResultsVoyageLengthWeightKeyImportExportModel createCsvImportModel(VoyageResultsImportDataContext importDataContext) {
        return VoyageResultsVoyageLengthWeightKeyImportExportModel.forImport(importDataContext);
    }

    @Override
    protected VoyageResultsVoyageLengthWeightKeyImportExportModel createCsvExportModel(VoyageResultsImportDataContext importDataContext) {
        return VoyageResultsVoyageLengthWeightKeyImportExportModel.forExport(importDataContext);
    }

    @Override
    protected void performImport(VoyageResultsImportDataContext importDataContext, InputFile inputFile, ImportDataFileResult result) {

        if (log.isInfoEnabled()) {
            log.info("Starts import of lenthWeightKey from file " + inputFile.getFileName());
        }

        SpeciesCategoryCache speciesCategoryCache = importDataContext.getSpeciesCategoryCache();

        Voyage expectedVoyage = importDataContext.getVoyage();

        Set<String> cacheKeys = new TreeSet<>();

        for (LengthWeightKey lengthWeightKey : expectedVoyage.getLengthWeightKey()) {
            cacheKeys.add(cacheKey(lengthWeightKey));
        }

        try (Import<VoyageResultsVoyageLengthWeightKeyImportRow> importer = open()) {

            incrementsProgress();
            int rowNumber = 0;
            for (VoyageResultsVoyageLengthWeightKeyImportRow row : importer) {

                doFlushTransaction(++rowNumber);

                Voyage voyage = row.getVoyage();

                if (!expectedVoyage.equals(voyage)) {
                    throw new MismatchProviderException(getLocale(), rowNumber, voyage.getName());
                }

                // find speciesCategory
                SpeciesCategory speciesCategory = speciesCategoryCache.getSpeciesCategory(row.getSpecies(), null, row.getSizeCategory(), null, null, result);

                LengthWeightKey lengthWeightKeyToCreate = row.getLengthWeightKey();
                lengthWeightKeyToCreate.setSpeciesCategory(speciesCategory);

                String cacheKey = cacheKey(lengthWeightKeyToCreate);
                if (cacheKeys.contains(cacheKey)) {
                    throw new DuplicatedResultsVoyageLengthWeightKeyException(getLocale(),
                                                                              rowNumber,
                                                                              voyage.getName(),
                                                                              lengthWeightKeyToCreate.getSpeciesCategory().getSpecies().getBaracoudaCode(),
                                                                              lengthWeightKeyToCreate.getSpeciesCategory().getSizeCategory().getName());
                }

                LengthWeightKey lengthWeightKey = persistenceService.createLengthWeightKey(lengthWeightKeyToCreate);

                //TODO should we import it ?
                //lengthWeightKey.setMetadata(row.getMetadata());

                // attach it to voyage
                voyage.addLengthWeightKey(lengthWeightKey);

                // collect ids
                addId(result, EchoBaseUserEntityEnum.LengthWeightKey, lengthWeightKey, rowNumber);

                addProcessedRow(result, row);

            }
        }

    }

    @Override
    protected void computeImportedExport(VoyageResultsImportDataContext importDataContext, ImportDataFileResult result) {

        String voyageId = importDataContext.getConfiguration().getVoyageId();
        Voyage voyage = persistenceService.getVoyage(voyageId);

        for (LengthWeightKey lengthWeightKey : getImportedEntities(LengthWeightKey.class, result)) {

            VoyageResultsVoyageLengthWeightKeyImportRow row = VoyageResultsVoyageLengthWeightKeyImportRow.of(voyage, lengthWeightKey);
            addImportedRow(result, row);

        }

    }

    private String cacheKey(LengthWeightKey lengthWeightKey) {
        SpeciesCategory speciesCategory = lengthWeightKey.getSpeciesCategory();
        return speciesCategory.getSpecies().getTopiaId() + "~" + speciesCategory.getSizeCategory().getTopiaId();
    }

}
