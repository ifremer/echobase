package fr.ifremer.echobase.services.service.importdata.actions;

/*
 * #%L
 * EchoBase :: Services
 * %%
 * Copyright (C) 2011 - 2016 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.echobase.entities.data.DataAcousticProvider;
import fr.ifremer.echobase.io.InputFile;
import fr.ifremer.echobase.services.csv.CellAble;
import fr.ifremer.echobase.services.csv.ResultAble;
import fr.ifremer.echobase.services.service.importdata.configurations.MooringResultsImportConfiguration;
import fr.ifremer.echobase.services.service.importdata.contexts.MooringResultsImportDataContext;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * @author Julien Ruchaud - ruchaud@codelutin.com
 * @since 4.0
 */
public abstract class MooringResultsCellImportDataActionSupport<E extends ResultAble & CellAble> extends ImportResultsCellDataActionSupport<MooringResultsImportConfiguration, MooringResultsImportDataContext, E> {

    /** Logger. */
    private static final Log log = LogFactory.getLog(MooringResultsCellImportDataActionSupport.class);

    protected MooringResultsCellImportDataActionSupport(MooringResultsImportDataContext importDataContext, InputFile inputFile, String... columnNamesToExclude) {
        super(importDataContext, inputFile, columnNamesToExclude);
    }

    @Override
    protected DataAcousticProvider getDataProvider(MooringResultsImportDataContext importDataContext) {
        return importDataContext.getMooring();
    }

}
