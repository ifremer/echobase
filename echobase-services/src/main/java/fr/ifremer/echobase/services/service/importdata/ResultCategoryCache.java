package fr.ifremer.echobase.services.service.importdata;

/*
 * #%L
 * EchoBase :: Services
 * %%
 * Copyright (C) 2011 - 2016 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.echobase.entities.EchoBaseUserEntityEnum;
import fr.ifremer.echobase.entities.data.Category;
import fr.ifremer.echobase.entities.data.Echotype;
import fr.ifremer.echobase.entities.references.AgeCategory;
import fr.ifremer.echobase.entities.references.SizeCategory;
import fr.ifremer.echobase.entities.references.Species;
import fr.ifremer.echobase.entities.references.SpeciesCategory;
import fr.ifremer.echobase.services.service.UserDbPersistenceService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.Map;
import java.util.TreeMap;

/**
 * Created on 29/03/16.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 4.0
 */
public class ResultCategoryCache {

    /** Logger. */
    private static final Log log = LogFactory.getLog(ResultCategoryCache.class);

    private final Map<String, Category> cache;
    private final UserDbPersistenceService persistenceService;
    private final SpeciesCategoryCache speciesCategoryCache;

    public ResultCategoryCache(UserDbPersistenceService persistenceService, SpeciesCategoryCache speciesCategoryCache) {
        this.persistenceService = persistenceService;
        this.speciesCategoryCache = speciesCategoryCache;
        this.cache = new TreeMap<>();
    }

    public Category getResultCategory(Echotype echotype,
                                      Species species,
                                      Float lengthClass,
                                      SizeCategory sizeCategory,
                                      AgeCategory ageCategory,
                                      ImportDataFileResult importResult) {

        SpeciesCategory speciesCategory = speciesCategoryCache.getSpeciesCategory(species,
                                                                                  lengthClass,
                                                                                  sizeCategory,
                                                                                  ageCategory,
                                                                                  null,
                                                                                  importResult);
        return getResultCategory(echotype, speciesCategory, importResult);

    }

    public Category getResultCategory(Echotype echotype, SpeciesCategory speciesCategory, ImportDataFileResult importResult) {

        String key
                = (speciesCategory == null ? "" : speciesCategory.getTopiaId())
                + "#" + (echotype == null ? "" : echotype.getName());

        Category category = cache.get(key);

        if (category == null) {

            // try to find it in db
            if (log.isInfoEnabled()) {
                log.info("Result category (" + key + ") not found in cache.");
            }

            category = persistenceService.getCategoryByEchotypeAndSpeciesCategory(echotype, speciesCategory);

            if (category == null) {

                // not found in db, create it
                if (log.isInfoEnabled()) {
                    log.info("Result category (" + key + ") not found in database, create it.");
                }
                category = persistenceService.createCategory(echotype, speciesCategory);
                importResult.incrementsNumberCreated(EchoBaseUserEntityEnum.Category);
            } else {

                if (log.isInfoEnabled()) {
                    log.info("Result category (" + key + ") found in database, add it to cache.");
                }
            }

            cache.put(key, category);

        }

        return category;

    }

}
