/*
 * #%L
 * EchoBase :: Services
 * %%
 * Copyright (C) 2011 - 2012 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ifremer.echobase.services.service.importdata.csv;

import fr.ifremer.echobase.entities.data.GearMetadataValue;
import fr.ifremer.echobase.entities.data.Operation;
import fr.ifremer.echobase.entities.references.Gear;
import fr.ifremer.echobase.entities.references.GearMetadata;
import fr.ifremer.echobase.entities.references.Vessel;
import fr.ifremer.echobase.services.csv.EchoBaseCsvUtil;
import fr.ifremer.echobase.services.service.importdata.contexts.VoyageOperationsImportDataContext;

/**
 * Model to import {@link GearMetadataValue}.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 0.3
 */
public class VoyageOperationsGearMetadataValueImportExportModel extends EchoBaseImportExportModelSupport<VoyageOperationsGearMetadataValueImportRow> {


    private VoyageOperationsGearMetadataValueImportExportModel(char separator) {
        super(separator);
    }

    public static VoyageOperationsGearMetadataValueImportExportModel forImport(VoyageOperationsImportDataContext importDataContext) {

        VoyageOperationsGearMetadataValueImportExportModel model = new VoyageOperationsGearMetadataValueImportExportModel(importDataContext.getCsvSeparator());
        model.newForeignKeyColumn(EchoBaseCsvUtil.VESSEL_NAME, VoyageOperationsGearMetadataValueImportRow.PROPERTY_VESSEL, Vessel.class, Vessel.PROPERTY_NAME, importDataContext.getVesselsByName());
        model.newForeignKeyColumn(EchoBaseCsvUtil.OPERATION_ID, VoyageOperationsGearMetadataValueImportRow.PROPERTY_OPERATION, Operation.class, Operation.PROPERTY_ID, importDataContext.getVoyageOperationsById());
        model.newForeignKeyColumn(VoyageOperationsGearMetadataValueImportRow.PROPERTY_METADATA_TYPE, GearMetadataValue.PROPERTY_GEAR_METADATA, GearMetadata.class, GearMetadata.PROPERTY_NAME, importDataContext.getGearMetadatasByName());
        model.newForeignKeyColumn(EchoBaseCsvUtil.GEAR_CODE, GearMetadataValue.PROPERTY_GEAR, Gear.class, Gear.PROPERTY_CASINO_GEAR_NAME, importDataContext.getGearsByCasinoGearName());
        model.newMandatoryColumn(VoyageOperationsGearMetadataValueImportRow.PROPERTY_GEAR_METADATA_VALUE, GearMetadataValue.PROPERTY_DATA_VALUE);
        return model;

    }

    public static VoyageOperationsGearMetadataValueImportExportModel forExport(VoyageOperationsImportDataContext importDataContext) {

        VoyageOperationsGearMetadataValueImportExportModel model = new VoyageOperationsGearMetadataValueImportExportModel(importDataContext.getCsvSeparator());
        model.newColumnForExport(EchoBaseCsvUtil.VESSEL_NAME, VoyageOperationsGearMetadataValueImportRow.PROPERTY_VESSEL, EchoBaseCsvUtil.VESSEL_FORMATTER);
        model.newColumnForExport(EchoBaseCsvUtil.OPERATION_ID, VoyageOperationsGearMetadataValueImportRow.PROPERTY_OPERATION, EchoBaseCsvUtil.OPERATION_FORMATTER);
        model.newColumnForExport(VoyageOperationsGearMetadataValueImportRow.PROPERTY_METADATA_TYPE, GearMetadataValue.PROPERTY_GEAR_METADATA, EchoBaseCsvUtil.GEAR_METADATA_FORMATTER);
        model.newColumnForExport(EchoBaseCsvUtil.GEAR_CODE, GearMetadataValue.PROPERTY_GEAR, EchoBaseCsvUtil.GEAR_FORMATTER);
        model.newColumnForExport(VoyageOperationsGearMetadataValueImportRow.PROPERTY_GEAR_METADATA_VALUE, GearMetadataValue.PROPERTY_DATA_VALUE);
        return model;

    }

    @Override
    public VoyageOperationsGearMetadataValueImportRow newEmptyInstance() {
        return new VoyageOperationsGearMetadataValueImportRow();
    }
}
