package fr.ifremer.echobase.services.service.importdata.actions;

/*
 * #%L
 * EchoBase :: Services
 * %%
 * Copyright (C) 2011 - 2016 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.Lists;
import fr.ifremer.echobase.entities.data.DataAcousticProvider;
import fr.ifremer.echobase.entities.data.Mooring;
import fr.ifremer.echobase.entities.references.AncillaryInstrumentation;
import fr.ifremer.echobase.services.service.importdata.MismatchProviderException;
import fr.ifremer.echobase.services.service.importdata.configurations.MooringCommonsAncillaryInstrumentationImportConfiguration;
import fr.ifremer.echobase.services.service.importdata.contexts.MooringCommonsAncillaryInstrumentationImportDataContext;
import fr.ifremer.echobase.services.service.importdata.csv.MooringCommonsAncillaryInstrumentationImportExportModel;
import fr.ifremer.echobase.services.service.importdata.csv.MooringCommonsAncillaryInstrumentationImportRow;
import java.util.List;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * @author Julien Ruchaud - ruchaud@codelutin.com
 * @since 4.0
 */
public class MooringCommonsAncillaryInstrumentationImportAction extends ImportAncillaryInstrumentationActionSupport<MooringCommonsAncillaryInstrumentationImportConfiguration, MooringCommonsAncillaryInstrumentationImportDataContext, MooringCommonsAncillaryInstrumentationImportRow> {

    /** Logger. */
    private static final Log log = LogFactory.getLog(MooringCommonsAncillaryInstrumentationImportAction.class);

    public MooringCommonsAncillaryInstrumentationImportAction(MooringCommonsAncillaryInstrumentationImportDataContext importDataContext) {
        super(importDataContext, importDataContext.getConfiguration().getAncillaryInstrumentationFile());
    }

    @Override
    protected MooringCommonsAncillaryInstrumentationImportRow newImportedRow(DataAcousticProvider provider, AncillaryInstrumentation ancillaryInstrumentation) {
        return MooringCommonsAncillaryInstrumentationImportRow.of(provider, ancillaryInstrumentation);
    }

    @Override
    protected List<DataAcousticProvider> getDataProviders(MooringCommonsAncillaryInstrumentationImportDataContext importDataContext, MooringCommonsAncillaryInstrumentationImportRow row, int rowNumber) {
        Mooring expectedMooring = importDataContext.getMooring();
        DataAcousticProvider mooring = row.getMooring();

        if (!expectedMooring.equals(mooring)) {
            throw new MismatchProviderException(getLocale(), rowNumber, mooring.getName());
        }

        return Lists.newArrayList(mooring);
    }

    @Override
    protected MooringCommonsAncillaryInstrumentationImportExportModel createCsvImportModel(MooringCommonsAncillaryInstrumentationImportDataContext importDataContext) {
        return MooringCommonsAncillaryInstrumentationImportExportModel.forImport(importDataContext);
    }

    @Override
    protected MooringCommonsAncillaryInstrumentationImportExportModel createCsvExportModel(MooringCommonsAncillaryInstrumentationImportDataContext importDataContext) {
        return MooringCommonsAncillaryInstrumentationImportExportModel.forExport(importDataContext);
    }

}
