/*
 * #%L
 * EchoBase :: Services
 * %%
 * Copyright (C) 2011 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ifremer.echobase.services.service.embeddedapplication;

import com.google.common.base.Preconditions;
import fr.ifremer.echobase.EchoBaseTechnicalException;
import fr.ifremer.echobase.entities.EchoBaseInternalPersistenceContext;
import fr.ifremer.echobase.entities.EchoBaseInternalTopiaApplicationContext;
import fr.ifremer.echobase.entities.EchoBaseUser;
import fr.ifremer.echobase.entities.EchoBaseUserPersistenceContext;
import fr.ifremer.echobase.entities.EchoBaseUserTopiaApplicationContext;
import fr.ifremer.echobase.entities.ExportQuery;
import fr.ifremer.echobase.entities.WorkingDbConfiguration;
import fr.ifremer.echobase.io.EchoBaseIOUtil;
import fr.ifremer.echobase.persistence.EchoBaseEntityHelper;
import fr.ifremer.echobase.persistence.JdbcConfiguration;
import fr.ifremer.echobase.services.DefaultEchoBaseServiceContext;
import fr.ifremer.echobase.services.EchoBaseServiceContext;
import fr.ifremer.echobase.services.EchoBaseServiceSupport;
import fr.ifremer.echobase.services.service.UserService;
import fr.ifremer.echobase.services.service.exportdb.ExportDbConfiguration;
import fr.ifremer.echobase.services.service.exportdb.ExportDbMode;
import fr.ifremer.echobase.services.service.exportdb.ExportDbService;
import fr.ifremer.echobase.services.service.exportquery.ExportQueryInvalidNameException;
import fr.ifremer.echobase.services.service.exportquery.ExportQueryNameAlreadyExistException;
import fr.ifremer.echobase.services.service.exportquery.ExportQueryService;
import fr.ifremer.echobase.services.service.importdata.ImportException;
import fr.ifremer.echobase.services.service.importdb.ImportDbConfiguration;
import fr.ifremer.echobase.services.service.importdb.ImportDbMode;
import fr.ifremer.echobase.services.service.importdb.ImportDbService;
import fr.ifremer.echobase.services.service.workingDb.WorkingDbConfigurationAlreadyExistException;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.persistence.TopiaException;
import org.nuiton.util.FileUtil;

import javax.inject.Inject;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;

/**
 * To create embedded application.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @author Sylvain Letellier
 * @since 0.1
 */
public class EmbeddedApplicationService extends EchoBaseServiceSupport {

    /** Logger. */
    private static final Log log =
            LogFactory.getLog(EmbeddedApplicationService.class);

    public static final String EMBEDDED_PATH = "/embedded/";

    @Inject
    EchoBaseInternalPersistenceContext echoBaseInternalPersistenceContext;

    @Inject
    private ExportDbService exportDbService;

    public File createEmbeddedApplication(EmbeddedApplicationConfiguration model) {

        int nbSteps = computeNbSteps(model);
        model.setNbSteps(nbSteps);

        String fileName = model.getFileName();

        File tempDirectory = model.getWorkingDirectory();
        tempDirectory.deleteOnExit();

        File zipFile = new File(tempDirectory, fileName + ".zip");
        model.setEmbeddedApplicationFile(zipFile);

        File dir = new File(tempDirectory, fileName);

        if (log.isInfoEnabled()) {
            log.info("Creates embedded application to file [" + zipFile + "]");
        }

        // export db to csv
        File exportZipFile = exportDb(model, tempDirectory);

        // create static archive structure
        createStaticArchiveStructure(model, dir);

        model.incrementsProgress();

        // create new service context
        EchoBaseServiceContext newServiceContext =
                DefaultEchoBaseServiceContext.newContext(
                        getLocale(),
                        getConfiguration(),
                        getDbMeta());

        EchoBaseUser admin;
        try {
            admin = importInternalDb(model, dir, newServiceContext);
        } catch (Exception e) {
            throw new EchoBaseTechnicalException(
                    "Could not create internal db", e);
        }

        // create working db in /db
        try {
            importWorkingDb(model, dir, exportZipFile, newServiceContext, admin);

        } catch (Exception eee) {

            throw new EchoBaseTechnicalException(
                    "Could not create h2 working database at " + dir, eee);
        }

        try {
            EchoBaseIOUtil.compressZipFile(zipFile, dir);
        } catch (IOException eee) {
            throw new EchoBaseTechnicalException(
                    "Could not create zip file at " + zipFile, eee);
        }

        return zipFile;
    }

    protected EchoBaseUser importInternalDb(EmbeddedApplicationConfiguration model,
                                            File dir,
                                            EchoBaseServiceContext newServiceContext) throws TopiaException, ExportQueryNameAlreadyExistException, WorkingDbConfigurationAlreadyExistException, ExportQueryInvalidNameException {

        File internalDir = new File(dir, "internaldb");

        EchoBaseInternalTopiaApplicationContext newInternalRootContext =
                EchoBaseInternalTopiaApplicationContext.newApplicationContext(internalDir);
        EchoBaseInternalPersistenceContext newPersistenceContext = newInternalRootContext.newPersistenceContext();
        newServiceContext.setEchoBaseInternalPersistenceContext(
                newPersistenceContext);

        try {
            // get user service from h2 db
            UserService userService = newServiceContext.newService(UserService.class);

            // create admin user
            userService.createDefaultUsers();

            // get admin from h2 db
            EchoBaseUser admin = userService.getUserByEmail(
                    UserService.DEFAULT_ADMIN_EMAIL);

            model.incrementsProgress();

            // get all export queries from application
            List<ExportQuery> queries = echoBaseInternalPersistenceContext.getExportQueryDao().findAll();

            // replicate queries
            echoBaseInternalPersistenceContext.replicateEntities(
                    newPersistenceContext, queries);

            // create export sql service from h2 db
            ExportQueryService exportQueryService =
                    newServiceContext.newService(ExportQueryService.class);

            // get all queries from h2 db
            queries = newPersistenceContext.getExportQueryDao().findAll();

            // attach them to default created admin user in the db
            for (ExportQuery query : queries) {
                exportQueryService.createOrUpdate(query, admin);
            }

            model.incrementsProgress();

            // get all working db configuration from application

            List<WorkingDbConfiguration> confs =
                    echoBaseInternalPersistenceContext.getWorkingDbConfigurationDao().findAll();

            // replicate configs
            echoBaseInternalPersistenceContext.replicateEntities(
                    newPersistenceContext, confs);

            model.incrementsProgress();

            return admin;
        } finally {
            newServiceContext.setEchoBaseInternalPersistenceContext(null);
            EchoBaseEntityHelper.releaseApplicationContext(newInternalRootContext);
        }
    }

    protected void importWorkingDb(EmbeddedApplicationConfiguration model,
                                   File dir,
                                   File exportZipFile,
                                   EchoBaseServiceContext newServiceContext,
                                   EchoBaseUser admin) throws IOException, ImportException, TopiaException {

        JdbcConfiguration dbConf = JdbcConfiguration.newEmbeddedConfig(dir);
        EchoBaseUserTopiaApplicationContext applicationContext =
                EchoBaseUserTopiaApplicationContext.newApplicationContext(dbConf);
        try {

            // inject a new transaction in service context
            newServiceContext.setEchoBaseUserPersistenceContext(
                    applicationContext.newPersistenceContext());

            model.incrementsProgress();

            // prepare import configuration
            ImportDbConfiguration importConfiguration =
                    new ImportDbConfiguration(model, getLocale());
            importConfiguration.getInput().setFile(exportZipFile);
            File importTempDir = new File(model.getWorkingDirectory(), "importDb");
            importConfiguration.setWorkingDirectory(importTempDir);
            importConfiguration.setImportDbMode(ImportDbMode.FREE);
            importConfiguration.setComputeSteps(false);
            importConfiguration.setCommitAfterEachFile(true);

            // do import to embedded working db
            newServiceContext.newService(ImportDbService.class).doImport(
                    importConfiguration, admin);

        } finally {
            // remove current transaction
            newServiceContext.setEchoBaseUserPersistenceContext(null);

            // release any connexion to the working db
            EchoBaseEntityHelper.releaseApplicationContext(applicationContext);
        }
    }

    protected int computeNbSteps(EmbeddedApplicationConfiguration model) {
        int nbSteps = 5;

        // export db steps

        nbSteps += getDbMeta().getReferenceEntriesSize();
        if (model.getVoyageIds() != null) {
            nbSteps += model.getVoyageIds().length;
        }

        // import db stesp
        nbSteps += getDbMeta().getEntriesSize();
        return nbSteps;
    }

    protected void createStaticArchiveStructure(EmbeddedApplicationConfiguration model, File dir) {

        try {

            // create /
            FileUtil.createDirectoryIfNecessary(dir);

            // copy embedded files to /
            copyEmbeddedTextFile("echobase.properties", dir, false);
            copyEmbeddedTextFile("startEchobase.bat", dir, true);
            copyEmbeddedTextFile("startEchobase.sh", dir, true);
            copyEmbeddedTextFile("README.txt", dir, false);

            // copy war to /
            File warLocation = model.getWarLocation();
            FileUtils.copyFile(warLocation,
                               new File(dir, warLocation.getName()));
        } catch (Exception eee) {
            throw new EchoBaseTechnicalException(
                    "Could not create embedded zip structure at " + dir, eee);
        }
    }

    protected File exportDb(EmbeddedApplicationConfiguration model, File dir) {

        try {
            // create export zip file
            ExportDbConfiguration exportconfiguration =
                    new ExportDbConfiguration(model);
            exportconfiguration.setFileName("echobase-export");
            File exportTempDir = new File(dir, "exportDb");
            exportconfiguration.setWorkingDirectory(exportTempDir);
            exportconfiguration.setVoyageIds(model.getVoyageIds());
            exportconfiguration.setComputeSteps(false);
            exportconfiguration.setExportDbMode(ExportDbMode.REFERENTIAL_AND_DATA);

            EchoBaseUserPersistenceContext persistenceContext = serviceContext.getEchoBaseUserPersistenceContext();
            EchoBaseUserTopiaApplicationContext applicationContext = serviceContext.getEchoBaseUserApplicationContext();

            EchoBaseUserPersistenceContext newPersistenceContext = applicationContext.newPersistenceContext();

            try {
                serviceContext.setEchoBaseUserPersistenceContext(newPersistenceContext);
                exportDbService.doExport(exportconfiguration);
            } catch (IOException eee) {
                newPersistenceContext.rollback();
                throw eee;
            } catch (RuntimeException eee) {
                newPersistenceContext.rollback();
                throw eee;
            } finally {
                serviceContext.setEchoBaseUserPersistenceContext(persistenceContext);
                newPersistenceContext.close();
            }
            File exportZipFile = exportconfiguration.getExportFile();
            if (log.isInfoEnabled()) {
                log.info("Export zip file = " + exportZipFile);
            }
            return exportZipFile;
        } catch (IOException eee) {
            throw new EchoBaseTechnicalException(
                    "Could not export db to zip", eee);
        }
    }

    public void copyEmbeddedTextFile(String resourceName,
                                     File targetDirectory,
                                     boolean executable) throws IOException {
        String resourcePath = EMBEDDED_PATH + resourceName;
        File outputFile = new File(targetDirectory, resourceName);
        EchoBaseIOUtil.copyResource(resourcePath, outputFile, executable);
    }

    public static void copyEmbeddedBinaryFile(String resourceName,
                                              File targetDirectory) throws IOException {
        String resourcePath = EMBEDDED_PATH + resourceName;
        InputStream inputStream =
                EmbeddedApplicationService.class.getResourceAsStream(resourcePath);
        Preconditions.checkNotNull(inputStream,
                                   "could not find resource " + resourcePath);
        try {
            File outputFile = new File(targetDirectory, resourceName);
            if (log.isDebugEnabled()) {
                log.debug("Copy binary file from " + resourceName + " to " +
                          outputFile);
            }
            OutputStream outputStream = new FileOutputStream(outputFile);
            try {
                IOUtils.copy(inputStream, outputStream);
                outputStream.close();
            } finally {
                IOUtils.closeQuietly(outputStream);
            }
            inputStream.close();
        } finally {
            IOUtils.closeQuietly(inputStream);
        }
    }
}
