/*
 * #%L
 * EchoBase :: Domain
 * %%
 * Copyright (C) 2011 - 2012 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ifremer.echobase.services.csv;

import fr.ifremer.echobase.entities.data.Result;
import fr.ifremer.echobase.entities.data.ResultImpl;
import fr.ifremer.echobase.entities.references.DataMetadata;
import org.nuiton.csv.ValueParser;

import java.text.ParseException;

/**
 * TODO
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 0.3
 */
class ResultValueParser implements ValueParser<Result> {

    private final DataMetadata metadata;
    private final boolean useFillValue;

    public ResultValueParser(DataMetadata metadata, boolean useFillValue) {
        this.metadata = metadata;
        this.useFillValue = useFillValue;
    }

    @Override
    public Result parse(String value) throws ParseException {

        Result result = new ResultImpl();
        result.setDataMetadata(metadata);
        result.setResultValue(value);
        if (EchoBaseCsvUtil.NA.equals(value) && useFillValue) {

            // use metadata fillValue
            result.setResultValue(String.valueOf(metadata.getFillValue()));
        }
        return result;
    }
}
