/*
 * #%L
 * EchoBase :: Services
 * %%
 * Copyright (C) 2011 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package fr.ifremer.echobase.services;

import fr.ifremer.echobase.config.EchoBaseConfiguration;
import fr.ifremer.echobase.entities.EchoBaseInternalPersistenceContext;
import fr.ifremer.echobase.entities.EchoBaseUserPersistenceContext;
import fr.ifremer.echobase.entities.EchoBaseUserTopiaApplicationContext;
import fr.ifremer.echobase.persistence.EchoBaseDbMeta;

import java.net.URL;
import java.util.Date;
import java.util.Locale;

/**
 * This contract represents objects you must provide when asking for a service.
 * Objects provided may be injected in services returned by
 * {@link #newService(Class)}
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 0.1
 */
public interface EchoBaseServiceContext {

    EchoBaseUserTopiaApplicationContext getEchoBaseUserApplicationContext();

    void setEchoBaseUserApplicationContext(EchoBaseUserTopiaApplicationContext applicationContext);

    EchoBaseUserPersistenceContext getEchoBaseUserPersistenceContext();

    void setEchoBaseUserPersistenceContext(EchoBaseUserPersistenceContext userTopiaPersistenceContext);

    EchoBaseInternalPersistenceContext getEchoBaseInternalPersistenceContext();

    void setEchoBaseInternalPersistenceContext(EchoBaseInternalPersistenceContext internalTopiaPersistenceContext);

    Locale getLocale();

    EchoBaseConfiguration getConfiguration();

    EchoBaseDbMeta getDbMeta();

    Date newDate();

    String getUserDbUrl();

    <S extends EchoBaseService> S newService(Class<S> serviceClass);

    URL getCoserApiURL();
}
