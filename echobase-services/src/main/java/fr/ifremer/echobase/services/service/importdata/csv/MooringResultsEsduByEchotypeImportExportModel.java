/*
 * #%L
 * EchoBase :: Services
 * %%
 * Copyright (C) 2011 - 2012 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ifremer.echobase.services.service.importdata.csv;

import fr.ifremer.echobase.entities.data.Echotype;
import fr.ifremer.echobase.entities.data.Mooring;
import fr.ifremer.echobase.entities.data.Result;
import fr.ifremer.echobase.entities.references.DataMetadata;
import fr.ifremer.echobase.entities.references.DataQuality;
import fr.ifremer.echobase.services.csv.EchoBaseCsvUtil;
import fr.ifremer.echobase.services.service.importdata.contexts.MooringResultsImportDataContext;

import java.util.List;

/**
 * Model to import {@link Result} for esdu cell and echotypes.
 *
 * @author Julien Ruchaud - ruchaud@codelutin.com
 * @since 4.0
 */
public class MooringResultsEsduByEchotypeImportExportModel extends EchoBaseImportExportModelSupport<MooringResultsEsduByEchotypeImportRow> {

    public static final String[] COLUMN_NAMES_TO_EXCLUDE = {
            EchoBaseCsvUtil.CELL_NAME,
            MooringResultsEsduByEchotypeImportRow.PROPERTY_ECHOTYPE,
            MooringResultsEsduByEchotypeImportRow.PROPERTY_DATA_QUALITY,
            MooringResultsEsduByEchotypeImportRow.PROPERTY_MOORING
    };

    private MooringResultsEsduByEchotypeImportExportModel(char separator) {
        super(separator);
    }

    public static MooringResultsEsduByEchotypeImportExportModel forImport(MooringResultsImportDataContext importDataContext, List<DataMetadata> dataMetadatas) {

        MooringResultsEsduByEchotypeImportExportModel model = new MooringResultsEsduByEchotypeImportExportModel(importDataContext.getCsvSeparator());
        model.newForeignKeyColumn(MooringResultsEsduByEchotypeImportRow.PROPERTY_MOORING, Mooring.class, Mooring.PROPERTY_CODE, importDataContext.getMooringsByCode());
        model.newForeignKeyColumn(MooringResultsEsduByEchotypeImportRow.PROPERTY_ECHOTYPE, Echotype.class, Echotype.PROPERTY_NAME, importDataContext.getMooringEchotypesByName());
        model.newMandatoryColumn(EchoBaseCsvUtil.CELL_NAME, VoyageResultsEsduByEchotypeImportRow.PROPERTY_CELL, importDataContext.getCellValueParser());
        model.newForeignKeyColumn(MooringResultsEsduByEchotypeImportRow.PROPERTY_DATA_QUALITY, DataQuality.class, DataQuality.PROPERTY_QUALITY_DATA_FLAG_VALUES, importDataContext.getDataQualitiesByName());

        addResultsColumnsForImport(model, dataMetadatas);
        return model;

    }

    public static MooringResultsEsduByEchotypeImportExportModel forExport(MooringResultsImportDataContext importDataContext, List<DataMetadata> dataMetadatas) {

        MooringResultsEsduByEchotypeImportExportModel model = new MooringResultsEsduByEchotypeImportExportModel(importDataContext.getCsvSeparator());
        model.newColumnForExport(MooringResultsEsduByEchotypeImportRow.PROPERTY_MOORING, EchoBaseCsvUtil.MOORING_FORMATTER);
        model.newColumnForExport(MooringResultsEsduByEchotypeImportRow.PROPERTY_ECHOTYPE, EchoBaseCsvUtil.ECHOTYPE_FORMATTER);
        model.newColumnForExport(EchoBaseCsvUtil.CELL_NAME, MooringResultsEsduByEchotypeImportRow.PROPERTY_CELL, importDataContext.getCellValueFormatter());
        model.newColumnForExport(MooringResultsEsduByEchotypeImportRow.PROPERTY_DATA_QUALITY, EchoBaseCsvUtil.DATA_QUALITY_FORMATTER);

        addResultsColumns(model, dataMetadatas);
        return model;

    }

    @Override
    public MooringResultsEsduByEchotypeImportRow newEmptyInstance() {
        return new MooringResultsEsduByEchotypeImportRow();
    }

}
