package fr.ifremer.echobase.services.service;

/*
 * #%L
 * EchoBase :: Services
 * %%
 * Copyright (C) 2011 - 2014 Ifremer, Codelutin, Chemit Tony
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.base.Charsets;
import com.google.common.base.Preconditions;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import fr.ifremer.echobase.EchoBaseTechnicalException;
import fr.ifremer.echobase.services.EchoBaseServiceSupport;
import org.apache.commons.io.IOUtils;
import org.apache.http.NameValuePair;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.methods.RequestBuilder;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicNameValuePair;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Type;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Collections;
import java.util.Map;
import java.util.Set;

/**
 * To request Coser API.
 *
 * Created on 3/23/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.5
 */
public class CoserApiService extends EchoBaseServiceSupport {

    public Map<String, String> getFacades() {

        Type type = new TypeToken<Map<String, Map<String, String>>>() {
        }.getType();
        Map<String, String> result = executeCoserRequest("get-facades", type);
        return Collections.unmodifiableMap(result);
    }

    public Map<String, String> getZonesForFacade(String facade) {
        Preconditions.checkNotNull(facade);
        Type type = new TypeToken<Map<String, Map<String, String>>>() {
        }.getType();
        Map<String, String> result = executeCoserRequest("get-zones-for-facade", type, new BasicNameValuePair("facade", facade));
        return Collections.unmodifiableMap(result);
    }

    public Set<String> getIndicators() {

        Type type = new TypeToken<Map<String, Set<String>>>() {
        }.getType();
        Set<String> result = executeCoserRequest("get-indicators", type);
        return Collections.unmodifiableSet(result);
    }

    protected <T> T executeCoserRequest(String action, Type type, NameValuePair... nvps) {

        URI endPoint;
        try {
            URL endPointUrl = serviceContext.getCoserApiURL();
            String url = endPointUrl.toURI().toString();
            if (!url.endsWith("/")) {
                url += "/";
            }

            endPoint = new URI(url + action);

        } catch (URISyntaxException e) {
            throw new EchoBaseTechnicalException(e);
        }

        HttpUriRequest build = RequestBuilder.get().
                setUri(endPoint).
                addParameters(nvps).
                build();

        CloseableHttpResponse response;
        try {
            response = HttpClientBuilder.create().build().execute(build);

        } catch (IOException e) {
            throw new EchoBaseTechnicalException("Could not execute request: " + build, e);
        }
        String stringContent;
        InputStream content = null;
        try {
            content = response.getEntity().getContent();
            stringContent = IOUtils.toString(content, Charsets.UTF_8);
            content.close();
        } catch (IOException e) {
            throw new EchoBaseTechnicalException("Could not obtain response content", e);
        } finally {
            IOUtils.closeQuietly(content);
        }

        try {
            Map<String, T> resultcontent = new GsonBuilder().create().fromJson(stringContent, type);
            return resultcontent.get("data");
        } catch (Exception e) {
            throw new EchoBaseTechnicalException("Could not obtain response content from: " + action, e);
        }
    }
}
