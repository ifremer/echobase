package fr.ifremer.echobase.services.service.importdata.actions;

/*
 * #%L
 * EchoBase :: Services
 * %%
 * Copyright (C) 2011 - 2016 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.base.Preconditions;
import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;
import fr.ifremer.echobase.entities.data.DataAcousticProvider;
import fr.ifremer.echobase.entities.EchoBaseUserEntityEnum;
import fr.ifremer.echobase.entities.data.Echotype;
import fr.ifremer.echobase.entities.data.Echotypes;
import fr.ifremer.echobase.entities.references.Species;
import fr.ifremer.echobase.io.InputFile;
import fr.ifremer.echobase.services.service.importdata.DuplicatedEchotypeAssociationException;
import fr.ifremer.echobase.services.service.importdata.ImportDataFileResult;
import fr.ifremer.echobase.services.service.importdata.MismatchProviderException;
import fr.ifremer.echobase.services.service.importdata.configurations.ImportDataConfigurationSupport;
import fr.ifremer.echobase.services.service.importdata.contexts.ImportDataContextSupport;
import fr.ifremer.echobase.services.service.importdata.csv.ResultsEchotypeImportRow;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.csv.Import;

import java.util.Collection;
import java.util.LinkedList;

/**
 * @author Julien Ruchaud - ruchaud@codelutin.com
 * @since 4.0
 */
public abstract class ImportResultsEchotypeActionSupport<M extends ImportDataConfigurationSupport, C extends ImportDataContextSupport<M>, E extends ResultsEchotypeImportRow> extends ImportDataActionSupport<M, C, E> {

    /** Logger. */
    private static final Log log = LogFactory.getLog(ImportResultsEchotypeActionSupport.class);
    private final LinkedList<Pair<String, String>> speciesIdToEchotypeId = new LinkedList<>();

    public ImportResultsEchotypeActionSupport(C importDataContext, InputFile inputFile) {
        super(importDataContext, inputFile);
    }
    
    protected abstract E newImportedRow(DataAcousticProvider provider, Echotype echotype, Species species);

    protected abstract DataAcousticProvider getDataProvider(C importDataContext);

    @Override
    protected void performImport(C importDataContext, InputFile inputFile, ImportDataFileResult result) {
        if (log.isInfoEnabled()) {
            log.info("Starts import of echotypes from file " + inputFile.getFileName());
        }

        DataAcousticProvider expectedProvider = getDataProvider(importDataContext);

        try (Import<E> importer = open()) {

            incrementsProgress();

            int rowNumber = 0;
            for (E row : importer) {

                doFlushTransaction(++rowNumber);

                DataAcousticProvider provider = row.getProvider();

                if (!expectedProvider.equals(provider)) {
                    throw new MismatchProviderException(getLocale(), rowNumber, provider.getName());
                }

                Echotype rowEchotype = row.getEchotype();

                String echotypeName = rowEchotype.getName();

                // check if there is a echotype for the voyage and this name
                Echotype echotype = null;

                if (!provider.isEchotypeEmpty()) {

                    Collection<Echotype> echotypes = provider.getEchotype();
                    Predicate<Echotype> predicate = Echotypes.newEchotypeByNamePredicate(echotypeName);
                    echotype = Iterables.find(echotypes, predicate, null);

                }

                Species species = row.getSpecies();

                if (echotype == null) {

                    // creates it
                    echotype = persistenceService.createEchotype(rowEchotype);

                    // add this species
                    echotype.addSpecies(species);

                    // attach it to voyage
                    provider.addEchotype(echotype);

                    // collect ids
                    addId(result, EchoBaseUserEntityEnum.Echotype, echotype, rowNumber);

                    speciesIdToEchotypeId.add(Pair.of(species.getTopiaId(), echotype.getTopiaId()));

                } else {

                    Species existingSpecies = echotype.getSpeciesByTopiaId(species.getTopiaId());

                    if (existingSpecies != null) {
                        throw new DuplicatedEchotypeAssociationException(getLocale(), rowNumber, provider.getName(), echotypeName, species.getBaracoudaCode());
                    }

                    // add this species
                    echotype.addSpecies(species);

                    speciesIdToEchotypeId.add(Pair.of(species.getTopiaId(), echotype.getTopiaId()));

                }

                addProcessedRow(result, row);
            }
        }
    }

    @Override
    protected void computeImportedExport(C importDataContext, ImportDataFileResult result) {
        DataAcousticProvider provider = getDataProvider(importDataContext);

        for (Pair<String, String> speciesIdToEchotypeIdEntry : speciesIdToEchotypeId) {
            String speciesId = speciesIdToEchotypeIdEntry.getKey();
            Species species = persistenceService.getSpecies(speciesId);
            Preconditions.checkNotNull(species);

            String echotypeId = speciesIdToEchotypeIdEntry.getValue();
            Echotype echotype = persistenceService.getEchotype(echotypeId);
            Preconditions.checkNotNull(echotype);

            E row = newImportedRow(provider, echotype, species);
            addImportedRow(result, row);
        }
    }

}
