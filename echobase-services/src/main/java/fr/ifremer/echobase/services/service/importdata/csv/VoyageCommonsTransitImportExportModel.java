/*
 * #%L
 * EchoBase :: Services
 * %%
 * Copyright (C) 2011 - 2012 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ifremer.echobase.services.service.importdata.csv;

import fr.ifremer.echobase.entities.data.Transit;
import fr.ifremer.echobase.entities.data.Voyage;
import fr.ifremer.echobase.services.csv.EchoBaseCsvUtil;
import fr.ifremer.echobase.services.service.importdata.contexts.VoyageCommonsImportDataContext;

/**
 * Model to import Transits.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 0.3
 */
public class VoyageCommonsTransitImportExportModel extends EchoBaseImportExportModelSupport<VoyageCommonsTransitImportRow> {

    private VoyageCommonsTransitImportExportModel(char separator) {
        super(separator);
    }

    public static VoyageCommonsTransitImportExportModel forImport(VoyageCommonsImportDataContext importDataContext) {

        VoyageCommonsTransitImportExportModel model = new VoyageCommonsTransitImportExportModel(importDataContext.getCsvSeparator());
        model.newForeignKeyColumn(VoyageCommonsTransitImportRow.PROPERTY_VOYAGE, Voyage.class, Voyage.PROPERTY_NAME, importDataContext.getVoyagesByName());
        model.newMandatoryColumn(Transit.PROPERTY_DESCRIPTION);
        model.newMandatoryColumn(Transit.PROPERTY_START_TIME, EchoBaseCsvUtil.IMPORT_DAY_TIME_ECHOBASE);
        model.newMandatoryColumn(Transit.PROPERTY_END_TIME, EchoBaseCsvUtil.IMPORT_DAY_TIME_ECHOBASE);
        model.newMandatoryColumn(Transit.PROPERTY_START_LOCALITY);
        model.newMandatoryColumn(Transit.PROPERTY_END_LOCALITY);
        return model;

    }

    public static VoyageCommonsTransitImportExportModel forExport(VoyageCommonsImportDataContext importDataContext) {

        VoyageCommonsTransitImportExportModel model = new VoyageCommonsTransitImportExportModel(importDataContext.getCsvSeparator());
        model.newColumnForExport(VoyageCommonsTransitImportRow.PROPERTY_VOYAGE, EchoBaseCsvUtil.VOYAGE_FORMATTER);
        model.newColumnForExport(Transit.PROPERTY_DESCRIPTION);
        model.newColumnForExport(Transit.PROPERTY_START_TIME, EchoBaseCsvUtil.IMPORT_DAY_TIME_ECHOBASE);
        model.newColumnForExport(Transit.PROPERTY_END_TIME, EchoBaseCsvUtil.IMPORT_DAY_TIME_ECHOBASE);
        model.newColumnForExport(Transit.PROPERTY_START_LOCALITY);
        model.newColumnForExport(Transit.PROPERTY_END_LOCALITY);
        return model;

    }

    @Override
    public VoyageCommonsTransitImportRow newEmptyInstance() {
        return new VoyageCommonsTransitImportRow();
    }
}
