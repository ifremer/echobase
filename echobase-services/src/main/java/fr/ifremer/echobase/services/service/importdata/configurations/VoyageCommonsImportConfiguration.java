/*
 * #%L
 * EchoBase :: Services
 * %%
 * Copyright (C) 2011 - 2012 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ifremer.echobase.services.service.importdata.configurations;

import fr.ifremer.echobase.io.InputFile;

import java.util.Locale;

import static org.nuiton.i18n.I18n.l;

/**
 * Configuration of a "common data complete" import.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 0.3
 */
public class VoyageCommonsImportConfiguration extends VoyageImportDataConfigurationSupport {

    private static final long serialVersionUID = 1L;
    /** Voyage file to import. */
    protected final InputFile voyageFile;
    /** Transit file to import. */
    protected final InputFile transitFile;
    /** Transect file to import. */
    protected final InputFile transectFile;
    /** Ancillary instrumentation file to import. */
    protected final InputFile ancillaryInstrumentationFile;
    /** Selected mission id to use in voyage. */
    protected String missionId;
    /** Selected area of operation to use for voyage. */
    protected String areaOfOperationId;
    /** Manual description of voyage. */
    protected String voyageDescription;
    /** transit related activity. */
    protected String transitRelatedActivity;
    /** transect license. */
    protected String transectLicence;
    /** transect geospatialVerticalPositive. */
    protected String transectGeospatialVerticalPositive = "down";
    /** transect binUnitsPingAxis. */
    protected String transectBinUnitsPingAxis = "1 nautical mile";
    /** Manual datum to use in voyage. */
    protected String datum = "WGS84";

    public VoyageCommonsImportConfiguration(Locale locale) {
        voyageFile = InputFile.newFile(l(locale, "echobase.common.voyageFile"));
        transitFile = InputFile.newFile(l(locale, "echobase.common.transitFile"));
        transectFile = InputFile.newFile(l(locale, "echobase.common.transectFile"));
        ancillaryInstrumentationFile = InputFile.newFile(l(locale, "echobase.common.ancillaryInstrumentationFile"));
    }

    public String getMissionId() {
        return missionId;
    }

    public void setMissionId(String missionId) {
        this.missionId = missionId;
    }

    public String getAreaOfOperationId() {
        return areaOfOperationId;
    }

    public void setAreaOfOperationId(String areaOfOperationId) {
        this.areaOfOperationId = areaOfOperationId;
    }

    public String getVoyageDescription() {
        return voyageDescription;
    }

    public void setVoyageDescription(String voyageDescription) {
        this.voyageDescription = voyageDescription;
    }

    public String getDatum() {
        return datum;
    }

    public void setDatum(String datum) {
        this.datum = datum;
    }

    public String getTransitRelatedActivity() {
        return transitRelatedActivity;
    }

    public void setTransitRelatedActivity(String transitRelatedActivity) {
        this.transitRelatedActivity = transitRelatedActivity;
    }

    public String getTransectLicence() {
        return transectLicence;
    }

    public void setTransectLicence(String transectLicence) {
        this.transectLicence = transectLicence;
    }

    public String getTransectGeospatialVerticalPositive() {
        return transectGeospatialVerticalPositive;
    }

    public void setTransectGeospatialVerticalPositive(String transectGeospatialVerticalPositive) {
        this.transectGeospatialVerticalPositive = transectGeospatialVerticalPositive;
    }

    public String getTransectBinUnitsPingAxis() {
        return transectBinUnitsPingAxis;
    }

    public void setTransectBinUnitsPingAxis(String transectBinUnitsPingAxis) {
        this.transectBinUnitsPingAxis = transectBinUnitsPingAxis;
    }

    public InputFile getVoyageFile() {
        return voyageFile;
    }

    public InputFile getTransitFile() {
        return transitFile;
    }

    public InputFile getTransectFile() {
        return transectFile;
    }

    public InputFile getAncillaryInstrumentationFile() {
        return ancillaryInstrumentationFile;
    }
    
    @Override
    public InputFile[] getInputFiles() {
        return new InputFile[]{voyageFile, transitFile, transectFile, ancillaryInstrumentationFile};
    }
}
