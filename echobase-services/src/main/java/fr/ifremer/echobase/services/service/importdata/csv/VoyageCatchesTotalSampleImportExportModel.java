/*
 * #%L
 * EchoBase :: Services
 * %%
 * Copyright (C) 2011 - 2012 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ifremer.echobase.services.service.importdata.csv;

import fr.ifremer.echobase.entities.data.Operation;
import fr.ifremer.echobase.entities.data.Sample;
import fr.ifremer.echobase.entities.references.SizeCategory;
import fr.ifremer.echobase.entities.references.Species;
import fr.ifremer.echobase.services.csv.EchoBaseCsvUtil;
import fr.ifremer.echobase.services.service.importdata.contexts.VoyageCatchesImportDataContext;

/**
 * Model to import {@link Sample} of total type.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 0.3
 */
public class VoyageCatchesTotalSampleImportExportModel extends EchoBaseImportExportModelSupport<VoyageCatchesTotalSampleImportRow> {

    private VoyageCatchesTotalSampleImportExportModel(char separator) {
        super(separator);
    }

    public static VoyageCatchesTotalSampleImportExportModel forImport(VoyageCatchesImportDataContext importDataContext) {

        VoyageCatchesTotalSampleImportExportModel model = new VoyageCatchesTotalSampleImportExportModel(importDataContext.getCsvSeparator());
        model.newForeignKeyColumn(EchoBaseCsvUtil.OPERATION_ID, VoyageCatchesTotalSampleImportRow.PROPERTY_OPERATION, Operation.class, Operation.PROPERTY_ID, importDataContext.getVoyageOperationsById());
        model.newForeignKeyColumn(Species.PROPERTY_BARACOUDA_CODE, VoyageCatchesTotalSampleImportRow.PROPERTY_SPECIES, Species.class, Species.PROPERTY_BARACOUDA_CODE, importDataContext.getSpeciesByBaracoudaCode());
        model.newForeignKeyColumn(VoyageCatchesTotalSampleImportRow.PROPERTY_SIZE_CATEGORY, SizeCategory.class, SizeCategory.PROPERTY_NAME, importDataContext.getSizeCategoriesByName());
        model.newMandatoryColumn(Sample.PROPERTY_SAMPLE_WEIGHT, EchoBaseCsvUtil.PRIMITIVE_FLOAT);
        model.newMandatoryColumn(Sample.PROPERTY_NUMBER_SAMPLED, EchoBaseCsvUtil.NA_TO_INTEGER_PARSER_FORMATTER);
        model.newMandatoryColumn(VoyageCatchesTotalSampleImportRow.PROPERTY_MEAN_LENGTH, EchoBaseCsvUtil.NA_TO_FLOAT_PARSER_FORMATTER);
        model.newMandatoryColumn(VoyageCatchesTotalSampleImportRow.PROPERTY_MEAN_WEIGHT, EchoBaseCsvUtil.NA_TO_FLOAT_PARSER_FORMATTER);
        model.newMandatoryColumn(VoyageCatchesTotalSampleImportRow.PROPERTY_NO_PER_KG, EchoBaseCsvUtil.NA_TO_FLOAT_PARSER_FORMATTER);
        model.newMandatoryColumn(VoyageCatchesTotalSampleImportRow.PROPERTY_SORTED_WEIGHT, EchoBaseCsvUtil.PRIMITIVE_FLOAT);
        return model;

    }

    public static VoyageCatchesTotalSampleImportExportModel forExport(VoyageCatchesImportDataContext importDataContext) {

        VoyageCatchesTotalSampleImportExportModel model = new VoyageCatchesTotalSampleImportExportModel(importDataContext.getCsvSeparator());
        model.newColumnForExport(EchoBaseCsvUtil.OPERATION_ID, VoyageCatchesTotalSampleImportRow.PROPERTY_OPERATION, EchoBaseCsvUtil.OPERATION_FORMATTER);
        model.newColumnForExport(Species.PROPERTY_BARACOUDA_CODE, VoyageCatchesTotalSampleImportRow.PROPERTY_SPECIES, EchoBaseCsvUtil.SPECIES_FORMATTER);
        model.newColumnForExport(VoyageCatchesTotalSampleImportRow.PROPERTY_SIZE_CATEGORY, EchoBaseCsvUtil.SIZE_CATEGORY_FORMATTER);
        model.newColumnForExport(Sample.PROPERTY_SAMPLE_WEIGHT, EchoBaseCsvUtil.PRIMITIVE_FLOAT);
        model.newColumnForExport(Sample.PROPERTY_NUMBER_SAMPLED, EchoBaseCsvUtil.NA_TO_INTEGER_PARSER_FORMATTER);
        model.newColumnForExport(VoyageCatchesTotalSampleImportRow.PROPERTY_MEAN_LENGTH, EchoBaseCsvUtil.NA_TO_FLOAT_PARSER_FORMATTER);
        model.newColumnForExport(VoyageCatchesTotalSampleImportRow.PROPERTY_MEAN_WEIGHT, EchoBaseCsvUtil.NA_TO_FLOAT_PARSER_FORMATTER);
        model.newColumnForExport(VoyageCatchesTotalSampleImportRow.PROPERTY_NO_PER_KG, EchoBaseCsvUtil.NA_TO_FLOAT_PARSER_FORMATTER);
        model.newColumnForExport(VoyageCatchesTotalSampleImportRow.PROPERTY_SORTED_WEIGHT, EchoBaseCsvUtil.PRIMITIVE_FLOAT);
        return model;

    }

    @Override
    public VoyageCatchesTotalSampleImportRow newEmptyInstance() {
        return new VoyageCatchesTotalSampleImportRow();
    }
}
