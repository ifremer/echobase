package fr.ifremer.echobase.services.service.removedata.strategy;

/*
 * #%L
 * EchoBase :: Services
 * %%
 * Copyright (C) 2011 - 2012 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.echobase.entities.ImportLog;
import fr.ifremer.echobase.entities.ImportType;
import fr.ifremer.echobase.entities.data.Cell;
import fr.ifremer.echobase.entities.data.Result;
import fr.ifremer.echobase.entities.data.Voyage;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.persistence.TopiaException;

import java.util.Collections;
import java.util.Set;
import fr.ifremer.echobase.entities.data.DataAcousticProvider;

/**
 * Remove a {@link ImportType#RESULT_ESDU} import.
 *
 * Can remove only {@link Result}.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.2
 */
public class ResultEsduRemoveDataStrategy extends AbstractRemoveDataStrategy<Voyage> {

    /** Logger. */
    private static final Log log =
            LogFactory.getLog(ResultEsduRemoveDataStrategy.class);

    @Override
    public long computeNbSteps(DataAcousticProvider<Voyage> provider, ImportLog importLog) {
        return getImportFileIdsCount(importLog);
    }

    @Override
    protected void removeImportData(DataAcousticProvider<Voyage> provider, String id) throws TopiaException {
        if (id.startsWith(Result.class.getName())) {

            // get result
            Result result = persistenceService.getResult(id);

            // remove it from cell
            Cell cell = persistenceService.getCellContainsResult(result);
            cell.removeResult(result);

            // remove result
            persistenceService.deleteResult(result);
            if (log.isDebugEnabled()) {
                log.debug(result.getTopiaId() + " was removed");
            }
        } else {
            canNotDealWithId(id);
        }
    }

    @Override
    public Set<ImportType> getPossibleSubImportType() {
        return Collections.emptySet();
    }
}
