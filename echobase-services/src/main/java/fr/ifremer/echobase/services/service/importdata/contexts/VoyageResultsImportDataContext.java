package fr.ifremer.echobase.services.service.importdata.contexts;

/*
 * #%L
 * EchoBase :: Services
 * %%
 * Copyright (C) 2011 - 2016 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.Maps;
import fr.ifremer.echobase.entities.EchoBaseUser;
import fr.ifremer.echobase.entities.data.Cell;
import fr.ifremer.echobase.entities.data.Cells;
import fr.ifremer.echobase.entities.data.Echotype;
import fr.ifremer.echobase.entities.data.Echotypes;
import fr.ifremer.echobase.entities.data.Voyage;
import fr.ifremer.echobase.entities.data.Voyages;
import fr.ifremer.echobase.services.service.UserDbPersistenceService;
import fr.ifremer.echobase.services.service.importdata.configurations.VoyageResultsImportConfiguration;
import java.util.Collections;

import java.util.Date;
import java.util.Locale;
import java.util.Map;

/**
 * Created on 30/03/16.
 *
 * @author Tony Chemit - chemit@codelutin.com
 */
public class VoyageResultsImportDataContext extends ImportResultsDataContext<VoyageResultsImportConfiguration> {

    private Voyage voyage;
    private Map<String, Voyage> voyagesByName;
    private Map<String, Echotype> voyageEchotypesByName;
    private Map<String, Cell> voyageRegionsByName;

    public VoyageResultsImportDataContext(UserDbPersistenceService persistenceService, Locale locale, char csvSeparator, VoyageResultsImportConfiguration configuration, EchoBaseUser user, Date importDate) {
        super(persistenceService, locale, csvSeparator, configuration, user, importDate);
    }
    
    public final Voyage getVoyage() {
        if (voyage == null) {
            voyage = persistenceService.getVoyage(configuration.getVoyageId());
        }
        return voyage;
    }

    @Override
    public String getEntityId() {
        return configuration.getVoyageId();
    }

    public final Map<String, Echotype> getVoyageEchotypesByName() {
        if (voyageEchotypesByName == null) {
            voyageEchotypesByName = Maps.uniqueIndex(getVoyage().getEchotype(), Echotypes.ECHOTYPE_NAME);
        }
        return voyageEchotypesByName;
    }

    public final Map<String, Voyage> getVoyagesByName() {
        if (voyagesByName == null) {
            voyagesByName = Maps.uniqueIndex(Collections.singletonList(getVoyage()), Voyages.VOYAGE_NAME);
        }
        return voyagesByName;
    }

    public final Map<String, Cell> getVoyageRegionsByName() {
        if (voyageRegionsByName == null) {
            voyageRegionsByName = Maps.uniqueIndex(getVoyage().getRegionCells(), Cells.CELL_BY_NAME);
        }
        return voyageRegionsByName;
    }

}
