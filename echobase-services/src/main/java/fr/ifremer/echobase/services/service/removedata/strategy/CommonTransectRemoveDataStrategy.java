package fr.ifremer.echobase.services.service.removedata.strategy;

/*
 * #%L
 * EchoBase :: Services
 * %%
 * Copyright (C) 2011 - 2012 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.Sets;
import fr.ifremer.echobase.entities.ImportLog;
import fr.ifremer.echobase.entities.ImportType;
import fr.ifremer.echobase.entities.data.Transect;
import fr.ifremer.echobase.entities.data.Transit;
import fr.ifremer.echobase.entities.data.Voyage;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.persistence.TopiaException;

import java.util.Set;
import fr.ifremer.echobase.entities.data.DataAcousticProvider;

/**
 * Remove a {@link ImportType#COMMON_TRANSECT} import.
 *
 * Can remove only {@link Transect}.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.2
 */
public class CommonTransectRemoveDataStrategy extends AbstractRemoveDataStrategy<Voyage> {

    /** Logger. */
    private static final Log log =
            LogFactory.getLog(CommonTransectRemoveDataStrategy.class);

    @Override
    public long computeNbSteps(DataAcousticProvider<Voyage> provider, ImportLog importLog) {
        Voyage voyage = provider.getEntity();

        long result = getImportFileIdsCount(importLog);

        // add all cell results
        result += persistenceService.countVoyageCellResults(voyage);

        // add all postCell
        result += voyage.sizePostCell();

        // add all orphan cells
        result += persistenceService.countVoyageOrphanCells(voyage);
        return result;
    }

    @Override
    protected void removePreData(DataAcousticProvider<Voyage> provider) throws TopiaException {
        Voyage voyage = provider.getEntity();

        // remove all cell results
        removeVoyageCellResults(voyage);

        // remove postCell from voyage
        removeVoyagePostCell(voyage);
    }

    @Override
    protected void removePostData(DataAcousticProvider<Voyage> provider) throws TopiaException {
        // remove orphans cells
        removeOrphanCells();
    }

    @Override
    protected void removeImportData(DataAcousticProvider<Voyage> provider, String id) throws TopiaException {

        if (id.startsWith(Transect.class.getName())) {

            // remove transect
            Transect transect = persistenceService.getTransect(id);

            // remove it from transit
            Transit transit = persistenceService.getTransitContainsTransect(transect);
            transit.removeTransect(transect);

            // delete it
            persistenceService.deleteTransect(transect);

            if (log.isDebugEnabled()) {
                log.debug(transect.getTopiaId() + " was removed");
            }

        } else {
            canNotDealWithId(id);
        }
    }

    @Override
    public Set<ImportType> getPossibleSubImportType() {
        return Sets.newHashSet(ImportType.COMMON_ANCILLARY_INSTRUMENTATION,
                               ImportType.OPERATION,
                               ImportType.CATCHES,
                               ImportType.ACOUSTIC,
                               ImportType.RESULT_ESDU,
                               ImportType.RESULT_MAP_FISH,
                               ImportType.RESULT_MAP_OTHER,
                               ImportType.RESULT_REGION);
    }
}
