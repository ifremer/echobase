/*
 * #%L
 * EchoBase :: Services
 * %%
 * Copyright (C) 2011 - 2012 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ifremer.echobase.services.service.importdata.csv;

import fr.ifremer.echobase.entities.data.Transect;
import fr.ifremer.echobase.entities.data.TransectImpl;
import fr.ifremer.echobase.entities.data.Voyage;
import fr.ifremer.echobase.entities.references.Vessel;

import java.io.Serializable;
import java.util.Date;

/**
 * Bean used as a row for import of {@link VoyageCommonsTransectImportExportModel}.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 0.3
 */
public class VoyageCommonsTransectImportRow implements Serializable {

    private static final long serialVersionUID = 1L;

    public static final String PROPERTY_VOYAGE = "voyage";

    protected Voyage voyage;

    protected final Transect transect;

    public static VoyageCommonsTransectImportRow of(Voyage voyage, Transect transect) {
        VoyageCommonsTransectImportRow row = new VoyageCommonsTransectImportRow(transect);
        row.setVoyage(voyage);
        return row;
    }

    public VoyageCommonsTransectImportRow() {
        this(new TransectImpl());
    }

    public VoyageCommonsTransectImportRow(Transect transect) {
        this.transect = transect;
    }

    public Transect getTransect() {
        return transect;
    }

    public Voyage getVoyage() {
        return voyage;
    }

    public void setVoyage(Voyage voyage) {
        this.voyage = voyage;
    }

    public String getVesselName() {
        return transect.getVessel().getName();
    }

    public void setVessel(Vessel vessel) {
        transect.setVessel(vessel);
    }

    public String getTitle() {
        return transect.getTitle();
    }

    public void setTitle(String title) {
        transect.setTitle(title);
    }

    public String getTransectAbstract() {
        return transect.getTransectAbstract();
    }

    public void setTransectAbstract(String transectAbstract) {
        transect.setTransectAbstract(transectAbstract);
    }

    public String getHistory() {
        return transect.getHistory();
    }

    public String getComments() {
        return transect.getComments();
    }

    public void setComments(String comments) {
        transect.setComments(comments);
    }

    public String getMetadata() {
        return transect.getMetadata();
    }

    public String getCitation() {
        return transect.getCitation();
    }

    public String getLicence() {
        return transect.getLicence();
    }

    public Date getDateCreated() {
        return transect.getDateCreated();
    }

    public void setDateCreated(Date dateCreated) {
        transect.setDateCreated(dateCreated);
    }

    public float getGeospatialLonMin() {
        return transect.getGeospatialLonMin();
    }

    public void setGeospatialLonMin(float geospatialLonMin) {
        transect.setGeospatialLonMin(geospatialLonMin);
    }

    public float getGeospatialLonMax() {
        return transect.getGeospatialLonMax();
    }

    public void setGeospatialLonMax(float geospatialLonMax) {
        transect.setGeospatialLonMax(geospatialLonMax);
    }

    public float getGeospatialLatMin() {
        return transect.getGeospatialLatMin();
    }

    public void setGeospatialLatMin(float geospatialLatMin) {
        transect.setGeospatialLatMin(geospatialLatMin);
    }

    public float getGeospatialLatMax() {
        return transect.getGeospatialLatMax();
    }

    public void setGeospatialLatMax(float geospatialLatMax) {
        transect.setGeospatialLatMax(geospatialLatMax);
    }

    public String getLinestring() {
        return transect.getLinestring();
    }

    public void setLinestring(String linestring) {
        transect.setLinestring(linestring);
    }

    public float getGeospatialVerticalMin() {
        return transect.getGeospatialVerticalMin();
    }

    public void setGeospatialVerticalMin(float geospatialVerticalMin) {
        transect.setGeospatialVerticalMin(geospatialVerticalMin);
    }

    public float getGeospatialVerticalMax() {
        return transect.getGeospatialVerticalMax();
    }

    public void setGeospatialVerticalMax(float geospatialVerticalMax) {
        transect.setGeospatialVerticalMax(geospatialVerticalMax);
    }

    public String getGeospatialVerticalPositive() {
        return transect.getGeospatialVerticalPositive();
    }

    public Date getTimeCoverageStart() {
        return transect.getTimeCoverageStart();
    }

    public void setTimeCoverageStart(Date timeCoverageStart) {
        transect.setTimeCoverageStart(timeCoverageStart);
    }

    public Date getTimeCoverageEnd() {
        return transect.getTimeCoverageEnd();
    }

    public void setTimeCoverageEnd(Date timeCoverageEnd) {
        transect.setTimeCoverageEnd(timeCoverageEnd);
    }

    public String getBinUnitsPingAxis() {
        return transect.getBinUnitsPingAxis();
    }

    public float getBinSizePingAxis() {
        return transect.getBinSizePingAxis();
    }

    public String getBinSizeRangeAxis() {
        return transect.getBinSizeRangeAxis();
    }

    public String getStratum() {
        return transect.getStratum();
    }

    public void setStratum(String stratum) {
        transect.setStratum(stratum);
    }
}
