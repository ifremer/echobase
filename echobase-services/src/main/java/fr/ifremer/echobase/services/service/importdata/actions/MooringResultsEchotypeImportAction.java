package fr.ifremer.echobase.services.service.importdata.actions;

/*
 * #%L
 * EchoBase :: Services
 * %%
 * Copyright (C) 2011 - 2016 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.echobase.entities.data.DataAcousticProvider;
import fr.ifremer.echobase.entities.data.Echotype;
import fr.ifremer.echobase.entities.references.Species;
import fr.ifremer.echobase.services.service.importdata.configurations.MooringResultsImportConfiguration;
import fr.ifremer.echobase.services.service.importdata.contexts.MooringResultsImportDataContext;
import fr.ifremer.echobase.services.service.importdata.csv.MooringResultsEchotypeImportExportModel;
import fr.ifremer.echobase.services.service.importdata.csv.MooringResultsEchotypeImportRow;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * @author Julien Ruchaud - ruchaud@codelutin.com
 * @since 4.0
 */
public class MooringResultsEchotypeImportAction extends ImportResultsEchotypeActionSupport<MooringResultsImportConfiguration, MooringResultsImportDataContext, MooringResultsEchotypeImportRow> {

    /** Logger. */
    private static final Log log = LogFactory.getLog(MooringResultsEchotypeImportAction.class);

    public MooringResultsEchotypeImportAction(MooringResultsImportDataContext importDataContext) {
        super(importDataContext, importDataContext.getConfiguration().getEchotypeFile());
    }

    @Override
    protected MooringResultsEchotypeImportExportModel createCsvImportModel(MooringResultsImportDataContext importDataContext) {
        return MooringResultsEchotypeImportExportModel.forImport(importDataContext);
    }

    @Override
    protected MooringResultsEchotypeImportExportModel createCsvExportModel(MooringResultsImportDataContext importDataContext) {
        return MooringResultsEchotypeImportExportModel.forExport(importDataContext);
    }

    @Override
    protected DataAcousticProvider getDataProvider(MooringResultsImportDataContext importDataContext) {
        return importDataContext.getMooring();
    }

    @Override
    protected MooringResultsEchotypeImportRow newImportedRow(DataAcousticProvider mooring, Echotype echotype, Species species) {
        return MooringResultsEchotypeImportRow.of(mooring, echotype, species);
    }
    
}
