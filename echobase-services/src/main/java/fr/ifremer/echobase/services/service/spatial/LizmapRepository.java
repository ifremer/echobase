package fr.ifremer.echobase.services.service.spatial;

/*
 * #%L
 * EchoBase :: Services
 * %%
 * Copyright (C) 2011 - 2015 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.base.Charsets;
import com.google.common.base.Preconditions;
import com.google.common.io.Files;
import fr.ifremer.echobase.EchoBaseTechnicalException;
import fr.ifremer.echobase.config.EchoBaseConfiguration;
import fr.ifremer.echobase.entities.data.Voyage;
import fr.ifremer.echobase.persistence.JdbcConfiguration;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.HierarchicalINIConfiguration;
import org.apache.commons.configuration.SubnodeConfiguration;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * Created on 1/14/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.8
 */
public class LizmapRepository {

    /** Logger. */
    private static final Log log = LogFactory.getLog(LizmapRepository.class);

    public static class LizmapRepositoryConfiguration {

        private final JdbcConfiguration jdbcConfiguration;

        private final EchoBaseConfiguration configuration;

        LizmapRepositoryConfiguration(JdbcConfiguration jdbcConfiguration, EchoBaseConfiguration configuration) {
            this.jdbcConfiguration = jdbcConfiguration;
            this.configuration = configuration;
        }

        public File getLizmapTemplateFile() {
            return configuration.getLizmapTemplateFile();
        }

        public File getLizmapProjectsDirectory() {
            return configuration.getLizmapProjectsDirectory();
        }

        public File getLizmapApplicationConfigFile() {
            return configuration.getLizmapApplicationConfigFile();
        }

        public String getLizmapApplicationJdbcUrl() {
            return configuration.getLizmapApplicationJdbcUrl();
        }

        public String getLizmapApplicationUrl() {
            return configuration.getLizmapApplicationMapUrl();
        }

        public String getLizmapRepositoryName() {
            return configuration.getLizmapRepositoryName();
        }

        public String getUrl() {
            return jdbcConfiguration.getUrl();
        }

        public String getLogin() {
            return jdbcConfiguration.getLogin();
        }

        public String getPassword() {
            return jdbcConfiguration.getPassword();
        }
    }

    private static final String[] TO_REPLACE = new String[]{"-", "."};

    private static final String[] REPLACEMENT_LIST = new String[]{"", ""};

    private final LizmapRepositoryConfiguration configuration;

    private String repositoryName;

    private File repositoryDirectory;


    public static LizmapRepository newLizmapRepository(EchoBaseConfiguration configuration, JdbcConfiguration conf) {

        LizmapRepositoryConfiguration lizmapRepositoryConfiguration = new LizmapRepositoryConfiguration(conf, configuration);

        File lizmapTarget = configuration.getLizmapProjectsDirectory();

        if (!lizmapTarget.isDirectory()) {
            throw new EchoBaseTechnicalException("Map target (" + lizmapTarget.getAbsolutePath() + ") is not directory");
        }

        return new LizmapRepository(lizmapRepositoryConfiguration);

    }

    protected LizmapRepository(LizmapRepositoryConfiguration configuration) {

        this.configuration = configuration;

    }

    public String getRepositoryName() {

        if (repositoryName == null) {

            String url = configuration.getUrl();

            PgJdbcUrl jdbcUrl = new PgJdbcUrl(url);

            String host = jdbcUrl.getHost();
            String port = jdbcUrl.getPort();
            String dbname = jdbcUrl.getDatabaseName();

            repositoryName = configuration.getLizmapRepositoryName() + host + port + dbname;
            repositoryName = StringUtils.replaceEach(repositoryName, TO_REPLACE, REPLACEMENT_LIST);

        }

        return repositoryName;

    }

    public File getRepositoryDirectory() {

        if (repositoryDirectory == null) {

            String repoName = getRepositoryName();
            repositoryDirectory = new File(configuration.getLizmapProjectsDirectory(), repoName);

        }

        return repositoryDirectory;

    }

    public String getVoyageMapUrl(Voyage voyage) {

        String repoName = getRepositoryName();
        return configuration.getLizmapApplicationUrl() + "?" + "repository=" + repoName + "&project=" + voyage.getName();

    }

    public void register() {

        String repoName = getRepositoryName();

        if (log.isDebugEnabled()) {
            log.debug("Check if repository named " + repoName + " need to be registered by lizmap");
        }

        File configurationPath = configuration.getLizmapApplicationConfigFile();

        if (log.isDebugEnabled()) {
            log.debug("Use Lizmap configuration file: " + configurationPath);
        }
        HierarchicalINIConfiguration iniConfiguration;

        try {
            iniConfiguration = new HierarchicalINIConfiguration(configurationPath);
        } catch (ConfigurationException e) {
            throw new EchoBaseTechnicalException("Could not load Lizmap config", e);
        }

        SubnodeConfiguration section = iniConfiguration.getSection("repository:" + repoName);

        if (section.isEmpty()) {

            if (log.isInfoEnabled()) {
                log.info("Register repository " + repoName + " to Lizmap.");
            }

            // Add repository in Lizmap config
            updateLizmapConfigFile(iniConfiguration, section, repoName);

            // Add authorization in Lizmap database
            updateLizmapAuthorizations();

        }

    }

    public File getQGisFile(Voyage voyage) {

        return new File(getRepositoryDirectory(), voyage.getName() + ".qgs");


    }

    public File getLizmapFile(Voyage voyage) {

        return new File(getRepositoryDirectory(), voyage.getName() + ".qgs.cfg");

    }

    protected void updateLizmapConfigFile(HierarchicalINIConfiguration iniConfiguration, SubnodeConfiguration section, String repoName) {

        Preconditions.checkArgument(section.isEmpty());

        File configurationPath = configuration.getLizmapApplicationConfigFile();

        if (log.isInfoEnabled()) {
            log.info("Add Section " + repoName + " to lizmap configuration file.");
        }

        section.setProperty("label", repoName);
        section.setProperty("path", getRepositoryDirectory().getAbsolutePath() + "/");

        if (log.isInfoEnabled()) {
            log.info("Save lizmap configuration file at " + configurationPath);
        }

        try (BufferedWriter writer = Files.newWriter(configurationPath, Charsets.UTF_8)) {

            iniConfiguration.save(writer);

        } catch (IOException | ConfigurationException e) {
            throw new EchoBaseTechnicalException("Could not save Lizmap config", e);
        }

    }

    protected void updateLizmapAuthorizations() {

        try (Connection connection = DriverManager.getConnection(configuration.getLizmapApplicationJdbcUrl())) {

            connection.setAutoCommit(false);

            String repoName = getRepositoryName();

            if (log.isInfoEnabled()) {
                log.info("Add credentials for repository " + repoName + " in Lizmap.");
            }

            executeQuery(connection, "INSERT OR REPLACE INTO jacl2_rights " +
                                     "(id_aclsbj, id_aclgrp, id_aclres, canceled) " +
                                     "VALUES " +
                                     "('lizmap.repositories.view', '__anonymous', '" + repoName + "', 0);");

            executeQuery(connection, "INSERT OR REPLACE INTO jacl2_rights " +
                                     "(id_aclsbj, id_aclgrp, id_aclres, canceled) " +
                                     "VALUES " +
                                     "('lizmap.repositories.view', 'admins', '" + repoName + "', 0);");

            connection.commit();

        } catch (SQLException e) {
            throw new EchoBaseTechnicalException("Could not create rights in lizmap", e);
        }
    }

    protected void executeQuery(Connection connection, String query) throws SQLException {

        if (log.isDebugEnabled()) {
            log.debug("Execute sql query to lizmap: " + query);
        }

        try (Statement statement = connection.createStatement()){
            statement.execute(query);
        }

    }
}
