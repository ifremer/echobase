package fr.ifremer.echobase.services.service.removedata.strategy;

/*
 * #%L
 * EchoBase :: Services
 * %%
 * Copyright (C) 2011 - 2012 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.Sets;
import fr.ifremer.echobase.entities.ImportLog;
import fr.ifremer.echobase.entities.ImportType;
import fr.ifremer.echobase.entities.data.Category;
import fr.ifremer.echobase.entities.data.Echotype;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.persistence.TopiaException;

import java.util.List;
import java.util.Set;
import fr.ifremer.echobase.entities.data.DataAcousticProvider;
import fr.ifremer.echobase.entities.data.Mooring;

/**
 * Remove a {@link ImportType#RESULT_MOORING} import.
 *
 * Can remove only {@link Echotype}.
 *
 * @author Julien Ruchaud - ruchaud@codelutin.com
 * @since 4.0
 */
public class MooringResultsRemoveDataStrategy extends AbstractRemoveDataStrategy<Mooring> {

    /** Logger. */
    private static final Log log =
            LogFactory.getLog(MooringResultsRemoveDataStrategy.class);

    @Override
    public long computeNbSteps(DataAcousticProvider<Mooring> provider, ImportLog importLog) {
        Mooring mooring = provider.getEntity();

        long result = getImportFileIdsCount(importLog);

        // add all cell results
        result += persistenceService.countMooringCellResults(mooring);

        return result;
    }

    @Override
    protected void removePreData(DataAcousticProvider<Mooring> provider) throws TopiaException {
        Mooring mooring = provider.getEntity();
        removeMooringCellResults(mooring);
    }

    @Override
    protected void removeImportData(DataAcousticProvider<Mooring> provider, String id) throws TopiaException {
        Mooring mooring = provider.getEntity();

        if (id.startsWith(Echotype.class.getName())) {

            // remove echotype
            Echotype echotype = persistenceService.getEchotype(id);

            // delete all categories using this echotype
            List<Category> allByEchotype = persistenceService.getCategorysByEchotype(echotype);
            persistenceService.deleteCategories(allByEchotype);

            if (mooring != null) {
                // remove it from the voyage
                mooring.removeEchotype(echotype);
            }

            // delete echotype
            persistenceService.deleteEchotype(echotype);

            incrementOp("Remove echotype " + echotype.getTopiaId());

            if (log.isDebugEnabled()) {
                log.debug(echotype.getTopiaId() + " was removed");
            }
        } else {
            canNotDealWithId(id);
        }
    }

    @Override
    public Set<ImportType> getPossibleSubImportType() {
        return Sets.newHashSet(ImportType.RESULT_MOORING_ESDU);
    }
}
