package fr.ifremer.echobase.services.service.importdata;

/*
 * #%L
 * EchoBase :: Services
 * %%
 * Copyright (C) 2011 - 2016 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.echobase.EchoBaseTechnicalException;
import fr.ifremer.echobase.entities.EchoBaseUser;
import fr.ifremer.echobase.entities.ImportFile;
import fr.ifremer.echobase.entities.ImportLog;
import fr.ifremer.echobase.entities.ImportType;
import fr.ifremer.echobase.io.InputFile;
import fr.ifremer.echobase.services.EchoBaseServiceSupport;
import fr.ifremer.echobase.services.service.UserDbPersistenceService;
import fr.ifremer.echobase.services.service.importdata.actions.*;
import fr.ifremer.echobase.services.service.importdata.configurations.ImportDataConfigurationSupport;
import fr.ifremer.echobase.services.service.importdata.configurations.MooringAcousticsImportConfiguration;
import fr.ifremer.echobase.services.service.importdata.configurations.MooringCommonsAncillaryInstrumentationImportConfiguration;
import fr.ifremer.echobase.services.service.importdata.configurations.MooringCommonsMooringImportConfiguration;
import fr.ifremer.echobase.services.service.importdata.configurations.MooringResultsImportConfiguration;
import fr.ifremer.echobase.services.service.importdata.configurations.VoyageAcousticsImportConfiguration;
import fr.ifremer.echobase.services.service.importdata.configurations.VoyageCatchesImportConfiguration;
import fr.ifremer.echobase.services.service.importdata.configurations.VoyageCommonsAncillaryInstrumentationImportConfiguration;
import fr.ifremer.echobase.services.service.importdata.configurations.VoyageCommonsImportConfiguration;
import fr.ifremer.echobase.services.service.importdata.configurations.VoyageOperationsImportConfiguration;
import fr.ifremer.echobase.services.service.importdata.configurations.VoyageResultsImportConfiguration;
import fr.ifremer.echobase.services.service.importdata.contexts.ImportDataContextSupport;
import fr.ifremer.echobase.services.service.importdata.contexts.MooringAcousticsImportDataContext;
import fr.ifremer.echobase.services.service.importdata.contexts.MooringCommonsAncillaryInstrumentationImportDataContext;
import fr.ifremer.echobase.services.service.importdata.contexts.MooringCommonsMooringImportDataContext;
import fr.ifremer.echobase.services.service.importdata.contexts.MooringResultsImportDataContext;
import fr.ifremer.echobase.services.service.importdata.contexts.VoyageAcousticsImportDataContext;
import fr.ifremer.echobase.services.service.importdata.contexts.VoyageCatchesImportDataContext;
import fr.ifremer.echobase.services.service.importdata.contexts.VoyageCommonsAncillaryInstrumentationImportDataContext;
import fr.ifremer.echobase.services.service.importdata.contexts.VoyageCommonsImportDataContext;
import fr.ifremer.echobase.services.service.importdata.contexts.VoyageOperationsImportDataContext;
import fr.ifremer.echobase.services.service.importdata.contexts.VoyageResultsImportDataContext;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.util.TimeLog;

import javax.inject.Inject;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.regex.Pattern;

import static org.nuiton.i18n.I18n.l;

/**
 * Created on 26/03/16.
 *
 * @author Tony Chemit - chemit@codelutin.com
 */
public class ImportDataService extends EchoBaseServiceSupport {

    public static final Pattern REMOVE_DOUBLE_QUOTES_PATTERN = Pattern.compile("\"(.+)\"");
    public static final TimeLog TIME_LOG = new TimeLog(ImportDataService.class);
    /** Logger. */
    private static final Log log = LogFactory.getLog(ImportDataService.class);

    @Inject
    protected UserDbPersistenceService persistenceService;

    public ImportDataResult<VoyageCommonsImportConfiguration> doImportVoyageCommons(VoyageCommonsImportConfiguration configuration, EchoBaseUser user) throws ImportException {

        VoyageCommonsImportDataContext importDataContext = newVoyageCommonsImportContext(configuration, user);

        Collection<ImportDataActionSupport> importActions = new LinkedHashSet<>();

        ImportType importMode = configuration.getImportType();
        switch (importMode) {

            case COMMON_ALL:
                importActions.add(new VoyageCommonsVoyageImportAction(importDataContext));
                importActions.add(new VoyageCommonsTransitImportAction(importDataContext));
                importActions.add(new VoyageCommonsTransectImportAction(importDataContext));
                break;

            case COMMON_VOYAGE:
                importActions.add(new VoyageCommonsVoyageImportAction(importDataContext));
                break;

            case COMMON_TRANSIT:
                importActions.add(new VoyageCommonsTransitImportAction(importDataContext));
                break;

            case COMMON_TRANSECT:
                importActions.add(new VoyageCommonsTransectImportAction(importDataContext));

                break;

            default:
                throw new EchoBaseTechnicalException("Can not treate import result of type " + importMode);

        }

        return doImport(importDataContext, importActions);

    }

    public ImportDataResult<VoyageOperationsImportConfiguration> doImportVoyageOperations(VoyageOperationsImportConfiguration configuration, EchoBaseUser user) throws ImportException {

        VoyageOperationsImportDataContext importDataContext = newVoyageOperationsImportContext(configuration, user);

        Collection<ImportDataActionSupport> importActions = new LinkedHashSet<>();
        importActions.add(new VoyageOperationsOperationImportAction(importDataContext));
        importActions.add(new VoyageOperationsOperationMetadataImportAction(importDataContext));
        importActions.add(new VoyageOperationsGearMetadataImportAction(importDataContext));

        return doImport(importDataContext, importActions);

    }

    public ImportDataResult<VoyageCatchesImportConfiguration> doImportVoyageCatches(VoyageCatchesImportConfiguration configuration, EchoBaseUser user) throws ImportException {

        VoyageCatchesImportDataContext importDataContext = newVoyageCatchesImportContext(configuration, user);

        Collection<ImportDataActionSupport> importActions = new LinkedHashSet<>();

        InputFile totalSampleFile = configuration.getTotalSampleFile();
        if (totalSampleFile.hasFile()) {

            importActions.add(new VoyageCatchesTotalSampleImportAction(importDataContext));
        }

        InputFile subSampleFile = configuration.getSubSampleFile();
        if (subSampleFile.hasFile()) {

            importActions.add(new VoyageCatchesSubSampleImportAction(importDataContext));
        }

        InputFile biometrySampleFile = configuration.getBiometrySampleFile();
        if (biometrySampleFile.hasFile()) {

            importActions.add(new VoyageCatchesBiometrySampleImportAction(importDataContext));
        }

        return doImport(importDataContext, importActions);

    }

    public ImportDataResult<VoyageAcousticsImportConfiguration> doImportVoyageAcoustics(VoyageAcousticsImportConfiguration configuration, EchoBaseUser user) throws ImportException {
        VoyageAcousticsImportDataContext importDataContext = newVoyageAcousticsImportContext(configuration, user);
        Collection<ImportDataActionSupport> importActions = new LinkedHashSet<>();
        importActions.add(new VoyageAcousticsImportAction(importDataContext));

        if (configuration.getCalibrationsFile().hasFile()) {
            importActions.add(new VoyageAcousticsCalibrationImportAction(importDataContext));
        }

        return doImport(importDataContext, importActions);

    }
    
    public ImportDataResult<VoyageCommonsAncillaryInstrumentationImportConfiguration> doImportVoyageCommonsAncillaryInstrumentation(VoyageCommonsAncillaryInstrumentationImportConfiguration configuration, EchoBaseUser user) throws ImportException {
        VoyageCommonsAncillaryInstrumentationImportDataContext importDataContext = newVoyageCommonsAncillaryInstrumentationImportContext(configuration, user);
        Set<VoyageCommonsAncillaryInstrumentationImportAction> importActions = Collections.singleton(new VoyageCommonsAncillaryInstrumentationImportAction(importDataContext));
        return doImport(importDataContext, importActions);
    }
    
    public ImportDataResult<MooringCommonsMooringImportConfiguration> doImportMooringCommonsMooring(MooringCommonsMooringImportConfiguration configuration, EchoBaseUser user) throws ImportException {
        MooringCommonsMooringImportDataContext importDataContext = newMooringCommonsMooringImportContext(configuration, user);
        Set<MooringCommonsMooringImportAction> importActions = Collections.singleton(new MooringCommonsMooringImportAction(importDataContext));
        return doImport(importDataContext, importActions);
    }
    
    public ImportDataResult<MooringCommonsAncillaryInstrumentationImportConfiguration> doImportMooringCommonsAncillaryInstrumentation(MooringCommonsAncillaryInstrumentationImportConfiguration configuration, EchoBaseUser user) throws ImportException {
        MooringCommonsAncillaryInstrumentationImportDataContext importDataContext = newMooringCommonsAncillaryInstrumentationImportContext(configuration, user);
        Set<MooringCommonsAncillaryInstrumentationImportAction> importActions = Collections.singleton(new MooringCommonsAncillaryInstrumentationImportAction(importDataContext));
        return doImport(importDataContext, importActions);
    }

    public ImportDataResult<MooringAcousticsImportConfiguration> doImportMooringAcoustics(MooringAcousticsImportConfiguration configuration, EchoBaseUser user) throws ImportException {
        MooringAcousticsImportDataContext importDataContext = newMooringAcousticsImportContext(configuration, user);
        Set<MooringAcousticsImportAction> importActions = Collections.singleton(new MooringAcousticsImportAction(importDataContext));
        return doImport(importDataContext, importActions);
    }
    
    public ImportDataResult<MooringResultsImportConfiguration> doImportMooringResults(MooringResultsImportConfiguration configuration, EchoBaseUser user) throws ImportException {
        MooringResultsImportDataContext importDataContext = newMooringResultsImportContext(configuration, user);
        
        Collection<ImportDataActionSupport> importActions = new LinkedHashSet<>();
        
        ImportType importMode = configuration.getImportType();
        switch (importMode) {
            case RESULT_MOORING:
                // check if there is a such same import on this voyage (with a different id)
                String importLogId = importDataContext.getImportLog().getTopiaId();
                boolean importExists = persistenceService.isImportLogFor(configuration.getMooringId(), importLogId, ImportType.RESULT_MOORING);
                if (importExists) {
                    if (log.isWarnEnabled()) {
                        log.warn("there is already a mooring result import for this voyage, won't import.");
                    }
                    throw new ResultsImportAlreadyExistException(getLocale());
                }

                if (configuration.getEchotypeFile().hasFile()) {
                    importActions.add(new MooringResultsEchotypeImportAction(importDataContext));
                }
                break;

            case RESULT_MOORING_ESDU:
                if (configuration.getEsduByEchotypeFile().hasFile()) {
                    importActions.add(new MooringResultsEsduByEchotypeCellImportAction(importDataContext));
                }

                if (configuration.getEsduByEchotypeAndSpeciesCategoryFile().hasFile()) {
                    importActions.add(new MooringResultsEsduByEchotypeAndSpeciesCategoryCellImportAction(importDataContext));
                }

                break;
                
            default:
                throw new EchoBaseTechnicalException("Can not treate import result of type " + importMode);
        }
        
        return doImport(importDataContext, importActions);
    }

    public ImportDataResult<VoyageResultsImportConfiguration> doImportVoyageResults(VoyageResultsImportConfiguration configuration, EchoBaseUser user) throws ImportException {

        VoyageResultsImportDataContext importDataContext = newVoyageResultsImportContext(configuration, user);

        Collection<ImportDataActionSupport> importActions = new LinkedHashSet<>();

        ImportType importMode = configuration.getImportType();
        switch (importMode) {
            case RESULT_VOYAGE:
                // check if there is a such same import on this voyage (with a different id)
                String importLogId = importDataContext.getImportLog().getTopiaId();
                boolean importExists = persistenceService.isImportLogFor(configuration.getVoyageId(), importLogId, ImportType.RESULT_VOYAGE);
                if (importExists) {
                    if (log.isWarnEnabled()) {
                        log.warn("there is already a voyage result import for this voyage, won't import.");
                    }
                    throw new ResultsImportAlreadyExistException(getLocale());
                }

                if (configuration.getLengthAgeKeyFile().hasFile()) {
                    importActions.add(new VoyageResultsVoyageLengthAgeKeyImportAction(importDataContext));
                }

                if (configuration.getLengthWeightKeyFile().hasFile()) {
                    importActions.add(new VoyageResultsVoyageLengthWeightKeyImportAction(importDataContext));
                }

                if (configuration.getEchotypeFile().hasFile()) {
                    importActions.add(new VoyageResultsVoyageEchotypeImportAction(importDataContext));
                }
                
                break;

            case RESULT_ESDU:
                if (configuration.getEsduByEchotypeFile().hasFile()) {
                    importActions.add(new VoyageResultsEsduByEchotypeCellImportAction(importDataContext));
                }

                if (configuration.getEsduByEchotypeAndSpeciesCategoryFile().hasFile()) {
                    importActions.add(new VoyageResultsEsduByEchotypeAndSpeciesCategoryCellImportAction(importDataContext));
                }

                if (configuration.getEsduByEchotypeAndSpeciesCategoryAndLengthFile().hasFile()) {
                    importActions.add(new VoyageResultsEsduByEchotypeAndSpeciesCategoryAndLengthCellImportAction(importDataContext));
                }

                if (configuration.getEsduBySpeciesAndAgeCategoryFile().hasFile()) {
                    importActions.add(new VoyageResultsEsduSpeciesAndAgeCategoryCellImportAction(importDataContext));
                }

                break;

            case RESULT_REGION:
                importActions.add(new VoyageResultsRegionImportAction(importDataContext));
                importActions.add(new VoyageResultsRegionAssociationImportAction(importDataContext));
                importActions.add(new VoyageResultsRegionResultsImportAction(importDataContext));

                break;

            case RESULT_MAP_FISH:
                importActions.add(new VoyageResultsMapFishCellImportAction(importDataContext));

                break;

            case RESULT_MAP_OTHER:
                importActions.add(new VoyageResultsMapOtherCellImportAction(importDataContext));
                break;

            default:
                throw new EchoBaseTechnicalException("Can not treate import result of type " + importMode);
        }

        return doImport(importDataContext, importActions);

    }

    private VoyageCommonsImportDataContext newVoyageCommonsImportContext(VoyageCommonsImportConfiguration configuration, EchoBaseUser user) {
        return new VoyageCommonsImportDataContext(persistenceService, getLocale(), getCsvSeparator(), configuration, user, newDate());
    }

    private VoyageCommonsAncillaryInstrumentationImportDataContext newVoyageCommonsAncillaryInstrumentationImportContext(VoyageCommonsAncillaryInstrumentationImportConfiguration configuration, EchoBaseUser user) {
        return new VoyageCommonsAncillaryInstrumentationImportDataContext(persistenceService, getLocale(), getCsvSeparator(), configuration, user, newDate());
    }

    private VoyageResultsImportDataContext newVoyageResultsImportContext(VoyageResultsImportConfiguration configuration, EchoBaseUser user) {
        return new VoyageResultsImportDataContext(persistenceService, getLocale(), getCsvSeparator(), configuration, user, newDate());
    }

    private VoyageOperationsImportDataContext newVoyageOperationsImportContext(VoyageOperationsImportConfiguration configuration, EchoBaseUser user) {
        return new VoyageOperationsImportDataContext(persistenceService, getLocale(), getCsvSeparator(), configuration, user, newDate());
    }

    private VoyageCatchesImportDataContext newVoyageCatchesImportContext(VoyageCatchesImportConfiguration configuration, EchoBaseUser user) {
        return new VoyageCatchesImportDataContext(persistenceService, getLocale(), getCsvSeparator(), configuration, user, newDate());
    }

    private VoyageAcousticsImportDataContext newVoyageAcousticsImportContext(VoyageAcousticsImportConfiguration configuration, EchoBaseUser user) {
        return new VoyageAcousticsImportDataContext(persistenceService, getLocale(), getCsvSeparator(), configuration, user, newDate());
    }

    private MooringCommonsMooringImportDataContext newMooringCommonsMooringImportContext(MooringCommonsMooringImportConfiguration configuration, EchoBaseUser user) {
        return new MooringCommonsMooringImportDataContext(persistenceService, getLocale(), getCsvSeparator(), configuration, user, newDate());
    }

    private MooringCommonsAncillaryInstrumentationImportDataContext newMooringCommonsAncillaryInstrumentationImportContext(MooringCommonsAncillaryInstrumentationImportConfiguration configuration, EchoBaseUser user) {
        return new MooringCommonsAncillaryInstrumentationImportDataContext(persistenceService, getLocale(), getCsvSeparator(), configuration, user, newDate());
    }

    private MooringAcousticsImportDataContext newMooringAcousticsImportContext(MooringAcousticsImportConfiguration configuration, EchoBaseUser user) {
        return new MooringAcousticsImportDataContext(persistenceService, getLocale(), getCsvSeparator(), configuration, user, newDate());
    }

    private MooringResultsImportDataContext newMooringResultsImportContext(MooringResultsImportConfiguration configuration, EchoBaseUser user) {
        return new MooringResultsImportDataContext(persistenceService, getLocale(), getCsvSeparator(), configuration, user, newDate());
    }

    protected <M extends ImportDataConfigurationSupport, C extends ImportDataContextSupport<M>, A extends ImportDataActionSupport> ImportDataResult<M> doImport(C importDataContext, Collection<A> importActions) throws ImportException {

        long s0 = TimeLog.getTime();

        M configuration = importDataContext.getConfiguration();
        EchoBaseUser user = importDataContext.getUser();

        long nbSteps = configuration.computeNbSteps();

        if (log.isInfoEnabled()) {
            log.info("Nb lines to import " + nbSteps);
        }

        s0 = TIME_LOG.log(s0, "computeNbSteps");

        try {

            for (A importAction : importActions) {
                importAction.run();
            }

            s0 = TIME_LOG.log(s0, "importDone");

            // add result in log book and compute resume to show in result
            ImportLog importLog = importDataContext.getImportLog();
            String importSummary = computeImportSummary(importLog);
            importLog.setImportText(importSummary);

            String importType = getImportLabel(configuration);

            persistenceService.createEntityModificationLog("Import",
                                                           importType,
                                                           importLog.getImportUser(),
                                                           importLog.getImportDate(),
                                                           importSummary);

            // do commit
            persistenceService.commit();

            TIME_LOG.log(s0, "importCommited");

            return new ImportDataResult<>(configuration, user, importDataContext.getResults(), importSummary);

        } catch (EchoBaseTechnicalException e) {
            throw new ImportException(e.getMessage(), e);
        }
    }

    protected String computeImportSummary(ImportLog importLog) {

        StringBuilder builder = new StringBuilder();

        String importText = importLog.getImportText();

        if (StringUtils.isNotEmpty(importText)) {
            builder.append(importText);
        }

        for (ImportFile importFile : importLog.getImportFile()) {
            String resultImportText = importFile.getImportText();
            builder.append("Depuis le fichier ").append(importFile.getName()).append("\n\t")
                   .append(resultImportText).append("\n");
        }
        return builder.toString();
    }

    protected <M extends ImportDataConfigurationSupport> String getImportLabel(M configuration) {
        return l(getLocale(), configuration.getImportType().getI18nKey());
    }

    public interface ImportDataAction<M extends ImportDataConfigurationSupport> {
        ImportDataResult<M> doImport(ImportDataService service, M configuration, EchoBaseUser user) throws ImportException;
    }

    public static class VoyageAcousticsImportDataAction implements ImportDataAction<VoyageAcousticsImportConfiguration> {
        @Override
        public ImportDataResult<VoyageAcousticsImportConfiguration> doImport(ImportDataService service, VoyageAcousticsImportConfiguration configuration, EchoBaseUser user) throws ImportException {
            return service.doImportVoyageAcoustics(configuration, user);
        }
    }

    public static class VoyageCommonsImportDataAction implements ImportDataAction<VoyageCommonsImportConfiguration> {
        @Override
        public ImportDataResult<VoyageCommonsImportConfiguration> doImport(ImportDataService service, VoyageCommonsImportConfiguration configuration, EchoBaseUser user) throws ImportException {
            return service.doImportVoyageCommons(configuration, user);
        }
    }

    public static class VoyageCommonsAncillaryInstrumentationImportDataAction implements ImportDataAction<VoyageCommonsAncillaryInstrumentationImportConfiguration> {
        @Override
        public ImportDataResult<VoyageCommonsAncillaryInstrumentationImportConfiguration> doImport(ImportDataService service, VoyageCommonsAncillaryInstrumentationImportConfiguration configuration, EchoBaseUser user) throws ImportException {
            return service.doImportVoyageCommonsAncillaryInstrumentation(configuration, user);
        }
    }

    public static class VoyageOperationsImportDataAction implements ImportDataAction<VoyageOperationsImportConfiguration> {
        @Override
        public ImportDataResult<VoyageOperationsImportConfiguration> doImport(ImportDataService service, VoyageOperationsImportConfiguration configuration, EchoBaseUser user) throws ImportException {
            return service.doImportVoyageOperations(configuration, user);
        }
    }

    public static class VoyageResultsImportDataAction implements ImportDataAction<VoyageResultsImportConfiguration> {
        @Override
        public ImportDataResult<VoyageResultsImportConfiguration> doImport(ImportDataService service, VoyageResultsImportConfiguration configuration, EchoBaseUser user) throws ImportException {
            return service.doImportVoyageResults(configuration, user);
        }
    }

    public static class VoyageCatchesImportDataAction implements ImportDataAction<VoyageCatchesImportConfiguration> {
        @Override
        public ImportDataResult<VoyageCatchesImportConfiguration> doImport(ImportDataService service, VoyageCatchesImportConfiguration configuration, EchoBaseUser user) throws ImportException {
            return service.doImportVoyageCatches(configuration, user);
        }
    }

    public static class MooringAcousticsImportDataAction implements ImportDataAction<MooringAcousticsImportConfiguration> {
        @Override
        public ImportDataResult<MooringAcousticsImportConfiguration> doImport(ImportDataService service, MooringAcousticsImportConfiguration configuration, EchoBaseUser user) throws ImportException {
            return service.doImportMooringAcoustics(configuration, user);
        }
    }

    public static class MooringCommonsMooringImportDataAction implements ImportDataAction<MooringCommonsMooringImportConfiguration> {
        @Override
        public ImportDataResult<MooringCommonsMooringImportConfiguration> doImport(ImportDataService service, MooringCommonsMooringImportConfiguration configuration, EchoBaseUser user) throws ImportException {
            return service.doImportMooringCommonsMooring(configuration, user);
        }
    }

    public static class MooringCommonsAncillaryInstrumentationImportDataAction implements ImportDataAction<MooringCommonsAncillaryInstrumentationImportConfiguration> {
        @Override
        public ImportDataResult<MooringCommonsAncillaryInstrumentationImportConfiguration> doImport(ImportDataService service, MooringCommonsAncillaryInstrumentationImportConfiguration configuration, EchoBaseUser user) throws ImportException {
            return service.doImportMooringCommonsAncillaryInstrumentation(configuration, user);
        }
    }

    public static class MooringResultsImportDataAction implements ImportDataAction<MooringResultsImportConfiguration> {
        @Override
        public ImportDataResult<MooringResultsImportConfiguration> doImport(ImportDataService service, MooringResultsImportConfiguration configuration, EchoBaseUser user) throws ImportException {
            return service.doImportMooringResults(configuration, user);
        }
    }

}
