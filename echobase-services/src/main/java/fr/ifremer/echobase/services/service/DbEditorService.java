/*
 * #%L
 * EchoBase :: Services
 * %%
 * Copyright (C) 2011 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ifremer.echobase.services.service;

import com.google.common.base.Joiner;
import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import fr.ifremer.echobase.EchoBaseTechnicalException;
import fr.ifremer.echobase.entities.AbstractEchoBaseDao;
import fr.ifremer.echobase.entities.EchoBaseUser;
import fr.ifremer.echobase.entities.EchoBaseUserEntityEnum;
import fr.ifremer.echobase.entities.EchoBaseUserPersistenceContext;
import fr.ifremer.echobase.services.EchoBaseServiceSupport;
import fr.ifremer.echobase.services.csv.EchoBaseCsvUtil;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.csv.ExportModel;
import org.nuiton.csv.ExportableColumn;
import org.nuiton.csv.ImportExportModel;
import org.nuiton.csv.ImportableColumn;
import org.nuiton.decorator.Decorator;
import org.nuiton.decorator.JXPathDecorator;
import org.nuiton.topia.persistence.TopiaDao;
import org.nuiton.topia.persistence.TopiaEntity;
import org.nuiton.topia.persistence.TopiaException;
import org.nuiton.topia.persistence.metadata.ColumnMeta;
import org.nuiton.topia.persistence.metadata.DbMeta;
import org.nuiton.topia.persistence.metadata.TableMeta;
import org.nuiton.topia.persistence.pager.FilterRule;
import org.nuiton.topia.persistence.pager.FilterRuleGroupOperator;
import org.nuiton.topia.persistence.pager.FilterRuleOperator;
import org.nuiton.topia.persistence.pager.TopiaPagerBean;
import org.nuiton.topia.persistence.util.TopiaUtil;
import org.nuiton.topia.service.csv.EntityCsvModel;
import org.nuiton.util.PagerBeanUtil;
import org.nuiton.util.beans.BeanMonitor;
import org.nuiton.util.beans.PropertyDiff;
import org.nuiton.util.pagination.PaginationParameter;

import javax.inject.Inject;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import static org.nuiton.i18n.I18n.l;

/**
 * Service to edit the database.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 0.1
 */
public class DbEditorService extends EchoBaseServiceSupport {

    /** Logger. */
    private static final Log log = LogFactory.getLog(DbEditorService.class);

    /**
     * To replace the eq (ne) to nu (nn) operand.
     *
     * @since 2.6
     */
    private static final String NULL_FILTER_VALUE = "$NULL";

    @Inject
    private UserDbPersistenceService persistenceService;

    @Inject
    private DecoratorService decoratorService;

    @Inject
    private EchoBaseUserPersistenceContext persistenceContext;

    public <E extends TopiaEntity> List<E> getForeignData(Class<E> entityType) {
        Preconditions.checkNotNull(entityType);
        try {
            TopiaDao<E> dao = persistenceContext.getDao(entityType);

            return dao.findAll();
        } catch (TopiaException eee) {
            throw new EchoBaseTechnicalException("Could not obtain data", eee);
        }
    }

    public TableMeta<EchoBaseUserEntityEnum> getTableMeta(EchoBaseUserEntityEnum tableName) {
        DbMeta<EchoBaseUserEntityEnum> dbMeta = getDbMeta();
        return dbMeta.getTable(tableName);
    }

    public Map<?, ?>[] getData(EchoBaseUserEntityEnum type, TopiaPagerBean pager) {

        TableMeta<EchoBaseUserEntityEnum> meta = getTableMeta(type);

        List<TopiaEntity> entities = getEntities(meta, pager);

        Map<?, ?>[] rows = new Map[entities.size()];

        ExportModel<TopiaEntity> model = buildForLoad(meta, "id", true);

        int i = 0;
        for (Object o : entities) {
            TopiaEntity entity = (TopiaEntity) o;
            Map<String, Object> row = loadRow(entity, model);
            rows[i++] = row;
        }
        return rows;
    }

    public <E extends TopiaEntity> List<E> getEntities(TableMeta<EchoBaseUserEntityEnum> tableMeta,
                                                       TopiaPagerBean pager) {

        Preconditions.checkNotNull(tableMeta);
        Preconditions.checkNotNull(pager);

        EchoBaseUserEntityEnum entityEnum = tableMeta.getSource();
        try {
            AbstractEchoBaseDao<E> dao = (AbstractEchoBaseDao<E>) persistenceContext.getDao(entityEnum.getContract());

            List<Object> paramsList = Lists.newArrayList();

            String hql = dao.newFromClause("e");

            if (pager.canFilter()) {
                String filterHql = pagerToHql(tableMeta, pager, paramsList);
                hql += " WHERE " + filterHql;
            }

            Object[] params = paramsList.toArray();

            if (log.isInfoEnabled()) {
                log.info("filterHql = " + hql + "\n" + Joiner.on('\n').join(params));
            }


            Map<String, Object> hqlParameters = TopiaUtil.convertPropertiesArrayToMap(params);
            Long count = dao.findAny("SELECT COUNT(*) " + hql, hqlParameters);

            pager.setRecords(count);
            PagerBeanUtil.computeRecordIndexesAndPagesNumber(pager);

            if (pager.getSortColumn() != null && !pager.getSortColumn().isEmpty()) {
                if (pager.isSortAscendant()) {
                    hql += " Order by e." + pager.getSortColumn() + " asc";
                } else {
                    hql += " Order by e." + pager.getSortColumn() + " desc";
                }
            }

            if (!hql.toLowerCase().contains("order by")) {
                hql+=" Order By e.id";
            }
            //FIXME echatellier 20160309 use proper paginationparameter everywhere
            PaginationParameter param = PaginationParameter.of(pager.getPageIndex() - 1, pager.getPageSize());
            return dao.find(hql, hqlParameters, param.getStartIndex(), param.getEndIndex());
        } catch (TopiaException eee) {
            throw new EchoBaseTechnicalException("Could not obtain data", eee);
        }
    }

    public <E extends TopiaEntity> Iterable<E> iterateOnEntities(TableMeta<EchoBaseUserEntityEnum> tableMeta,
                                                                 String extraWhereQuery) {

        try {
            AbstractEchoBaseDao<E> dao = (AbstractEchoBaseDao<E>) persistenceContext.getDao(tableMeta.getSource().getContract());

            String hql = dao.newFromClause("e");
            if (extraWhereQuery != null) {
                hql += " WHERE " + extraWhereQuery;
            }
            hql += " ORDER BY e.id";
            return dao.forHql(hql, Collections.<String, Object>emptyMap()).findAllLazy(5000);
        } catch (TopiaException eee) {
            throw new EchoBaseTechnicalException("Could not obtain data", eee);
        }
    }

    public Map<?, ?> getData(TableMeta<EchoBaseUserEntityEnum> tableMeta, String topiaId) {

        EchoBaseUserEntityEnum entityEnum = tableMeta.getSource();
        try {
            AbstractEchoBaseDao<?> dao = (AbstractEchoBaseDao<?>) persistenceContext.getDao(entityEnum.getContract());
            TopiaEntity entity = dao.forTopiaIdEquals(topiaId).findUnique();
            ExportModel<TopiaEntity> model = buildForLoad(tableMeta, TopiaEntity.PROPERTY_TOPIA_ID, true);
            return loadRow(entity, model);
        } catch (TopiaException eee) {
            throw new EchoBaseTechnicalException("Could not obtain data", eee);
        }
    }

    public <E extends TopiaEntity> void deleteEntity(EchoBaseUserEntityEnum entityEnum, String topiaId, EchoBaseUser user) {

        TopiaDao<E> dao = (TopiaDao<E>) persistenceContext.getDao(entityEnum.getContract());

        E entity = dao.forTopiaIdEquals(topiaId).findUnique();
        dao.delete(entity);

        persistenceContext.commit();

    }

    public void saveEntity(TableMeta<EchoBaseUserEntityEnum> meta,
                           Map<String, String> properties,
                           EchoBaseUser user) {

        // transform properties  entity
        ImportExportModel<TopiaEntity> model = buildForSave(meta);

        TopiaEntity entity = model.newEmptyInstance();

        for (ImportableColumn<TopiaEntity, Object> column : model.getColumnsForImport()) {
            String propertyName = column.getHeaderName();
            String stringValue = properties.get(propertyName);
            Object o;
            try {
                o = column.parseValue(stringValue);
            } catch (Exception e) {
                throw new EchoBaseTechnicalException("Could not parse property [" + propertyName + "] with value " + stringValue, e);
            }
            try {
                column.setValue(entity, o);
            } catch (Exception e) {
                throw new EchoBaseTechnicalException(
                        "Could not set property [" + propertyName + "] with value " + stringValue + " to entity " + entity, e);
            }
        }

        saveEntity(meta,
                   "Modification utilisateur",
                   entity,
                   user,
                   false
        );
        persistenceService.commit();
    }

    public boolean saveEntity(TableMeta<EchoBaseUserEntityEnum> tableMeta,
                              String messagePrefix,
                              TopiaEntity entity,
                              EchoBaseUser user,
                              boolean createIfNotFound) {
        String[] columnNames = tableMeta.getColumnNamesAsArray();
        BeanMonitor monitor = null;

        String id = entity.getTopiaId();

        try {
            TopiaDao<?> dao = persistenceContext.getDao(tableMeta.getSource().getContract());

            TopiaEntity entityToSave;

            // Find or create entity if no id
            boolean create = StringUtils.isEmpty(id);

            if (create) {

                // get naturalIds or not-null properties
                Map<String, Object> properties =
                        tableMeta.prepareCreate(entity, null);

                entityToSave = dao.create(properties);

                // push back topiaId (could be used by caller)
                entity.setTopiaId(entityToSave.getTopiaId());

            } else {

                entityToSave = dao.forTopiaIdEquals(id).findUniqueOrNull();
                if (entityToSave == null) {

                    if (!createIfNotFound) {

                        // this is an error
                        throw new EchoBaseTechnicalException(
                                "Could not found entity with id " + id);
                    } else {
                        entityToSave = dao.create(TopiaEntity.PROPERTY_TOPIA_ID, id);
                        create = true;
                    }
                }
            }

            if (user != null && !create) {

                // monitor the existing bean
                monitor = new BeanMonitor(columnNames);
                monitor.setBean(entityToSave);
            }

            tableMeta.copy(entity, entityToSave);

            if (user != null) {

                createEntityModificationLog(
                        tableMeta,
                        messagePrefix,
                        entityToSave,
                        user,
                        monitor
                );
            }
            return create;
        } catch (Exception eee) {
            throw new EchoBaseTechnicalException("Could not update entity", eee);
        } finally {
            if (monitor != null) {
                monitor.setBean(null);
            }
        }
    }

    protected <O> Decorator<O> getDecorator(Class<O> type) {
        return decoratorService.getDecorator(type, null);
    }


    protected String pagerToHql(TableMeta<EchoBaseUserEntityEnum> tableMeta,
                                TopiaPagerBean pager,
                                List<Object> params) {
        Preconditions.checkNotNull(pager);
        Preconditions.checkState(pager.canFilter());

        String filterHql = null;

        List<String> strFilterRules = Lists.newLinkedList();

        for (FilterRule rule : pager.getRules()) {

            Map<String, Object> filterParams = Maps.newHashMap();

            String ruleFilter = ruleFilterToHql(tableMeta, rule, filterParams);

            if (ruleFilter != null) {

                // register new rule
                strFilterRules.add("(" + ruleFilter + ")");

                // register params
                for (Map.Entry<String, Object> entry : filterParams.entrySet()) {
                    params.add(entry.getKey());
                    params.add(entry.getValue());
                }
            }
        }

        if (CollectionUtils.isNotEmpty(strFilterRules)) {

            // add a rule group operator
            FilterRuleGroupOperator groupOp =
                    pager.getGroupOp();
            filterHql = StringUtils.join(strFilterRules, groupOp.name());
        }

        return filterHql;
    }

    protected String ruleFilterToHql(TableMeta<EchoBaseUserEntityEnum> tableMeta,
                                     FilterRule rule,
                                     Map<String, Object> filterParams) {

        long timestamp = System.currentTimeMillis();
        int index = 0;

        String ruleFilter;
        String field = rule.getField();

        ColumnMeta columnMeta = tableMeta.getColumns(field);
        Preconditions.checkNotNull(columnMeta,
                                   "no property named " + columnMeta);

        FilterRuleOperator op = rule.getOp();
        String data = rule.getData();
        String paramName = field + "_" + timestamp + (index++);
        String propertyName = "e." + field;
        Object realData = data;

        if (!columnMeta.isNumber() && NULL_FILTER_VALUE.equals(data)) {
            if (op == FilterRuleOperator.eq) {

                // use null op
                op = FilterRuleOperator.nu;
                if (log.isInfoEnabled()) {
                    log.info("Use *is null* operand (column " + columnMeta.getName() + ")");
                }

            } else if (op == FilterRuleOperator.ne) {

                // use not null op
                op = FilterRuleOperator.nn;
                if (log.isInfoEnabled()) {
                    log.info("Use *is not null* operand (column " + columnMeta.getName() + ")");
                }
            }
        }

        if (columnMeta.isFK()) {

            if (op == FilterRuleOperator.nu || op == FilterRuleOperator.nn) {

                // is null or is not null is a simple case

                ruleFilter = op.toHql(propertyName,
                                      propertyName,
                                      data,
                                      filterParams);
            } else {
                // must do a select in using the decoration pattern
                JXPathDecorator<?> decorator =
                        (JXPathDecorator<?>) getDecorator(columnMeta.getType());

                if (decorator.getNbToken() == 1) {

                    String fName = "f_" + timestamp + (index);

                    // simple case one token
                    String token = fName + "." + decorator.getProperty(0);

                    String subQuery = op.toHql(paramName,
                                               token,
                                               data,
                                               filterParams);

                    ruleFilter = propertyName + " IN ( FROM " +
                                 columnMeta.getType().getName() + " " +
                                 fName + " WHERE " + subQuery + ")";
                } else {

                    // more than one property, headace time...
                    List<String> ruleFilters = Lists.newArrayList();
                    for (String decoratorToken : decorator.getTokens()) {
                        String fName = "f_" + timestamp + (index++);

                        // simple case one token
                        String token = fName + "." + decoratorToken;

                        String subQuery = op.toHql(paramName,
                                                   token,
                                                   data,
                                                   filterParams);

                        ruleFilters.add(propertyName + " IN ( FROM " +
                                        columnMeta.getType().getName() + " " +
                                        fName + " WHERE " + subQuery + ")");
                    }

                    String middleOp = "OR";
                    switch (op) {
                        // With negate operand, we must match all !(a or b) = !a and !b
                        case ne:
                        case nc:
                        case en:
                        case bn:
                            middleOp = "AND";
                            break;
                    }
                    ruleFilter = "( " + Joiner.on(" " + middleOp + " ").join(ruleFilters) + " )";
                }
            }

        } else {

            if (columnMeta.isNumber()) {

                if (op == FilterRuleOperator.cn ||
                    op == FilterRuleOperator.nc ||
                    op == FilterRuleOperator.bw ||
                    op == FilterRuleOperator.bn ||
                    op == FilterRuleOperator.ew ||
                    op == FilterRuleOperator.en) {

                    // string operation

                    propertyName = "str(" + propertyName + ")";
                } else {

                    // real number operation
                    try {
                        realData = convertNumber(columnMeta, data);
                    } catch (Exception e) {
                        if (log.isErrorEnabled()) {
                            log.error("Could not convert column (" + columnMeta.getName() + ") to number: " + data, e);
                        }
                        realData = null;
                    }
                }
            } else if (columnMeta.isDate()) {
                propertyName = "str(" + propertyName + ")";
            }

            if (realData == null) {

                // null rule filter
                ruleFilter = null;
            } else {
                ruleFilter = op.toHql(paramName,
                                      propertyName,
                                      realData,
                                      filterParams);
            }
        }

        return ruleFilter;
    }

    protected Number convertNumber(ColumnMeta columnMeta, String data) {
        Number number;
        if (columnMeta.getType() == byte.class ||
            columnMeta.getType() == Byte.class) {
            number = Byte.valueOf(data);
        } else if (columnMeta.getType() == short.class ||
                   columnMeta.getType() == Short.class) {
            number = Short.valueOf(data);
        } else if (columnMeta.getType() == int.class ||
                   columnMeta.getType() == Integer.class) {
            number = Integer.valueOf(data);
        } else if (columnMeta.getType() == long.class ||
                   columnMeta.getType() == Long.class) {
            number = Integer.valueOf(data);
        } else if (columnMeta.getType() == float.class ||
                   columnMeta.getType() == Float.class) {
            number = Float.valueOf(data);
        } else if (columnMeta.getType() == double.class ||
                   columnMeta.getType() == Double.class) {
            number = Double.valueOf(data);
        } else if (columnMeta.getType() == BigInteger.class) {
            number = new BigInteger(data);
        } else if (columnMeta.getType() == BigDecimal.class) {
            number = new BigDecimal(data);
        } else {
            throw new IllegalStateException(
                    "Can't convert column (" + columnMeta.getName() + ") of type: " + columnMeta.getType());
        }
        return number;
    }

    /**
     * @deprecated since 2.6, no more used
     */
    @Deprecated
    protected String getFilterHql(TableMeta<EchoBaseUserEntityEnum> tableMeta,
                                  TopiaPagerBean pager,
                                  List<Object> params) {
        Preconditions.checkNotNull(pager);
        Preconditions.checkState(pager.canFilter());

        String filterHql = null;

        Map<String, Object> filterParams = Maps.newHashMap();

        List<String> strFilterRules = Lists.newLinkedList();

        long timestamp = System.currentTimeMillis();
        int index = 0;
        for (FilterRule rule : pager.getRules()) {
            String ruleFilter;

            String field = rule.getField();

            ColumnMeta columnMeta = tableMeta.getColumns(field);
            Preconditions.checkNotNull(columnMeta,
                                       "no property named " + columnMeta);

            FilterRuleOperator op = rule.getOp();
            String data = rule.getData();
            String paramName = field + "_" + timestamp + (index++);
            String propertyName = "e." + field;
            Object realData = data;
            if (columnMeta.isFK()) {

                if (op == FilterRuleOperator.nu || op == FilterRuleOperator.nn) {

                    // is null or is not null is a simple case

                    ruleFilter = op.toHql(propertyName,
                                          propertyName,
                                          data,
                                          filterParams);
                } else {
                    // must do a select in using the decoration pattern
                    JXPathDecorator<?> decorator =
                            (JXPathDecorator<?>) getDecorator(columnMeta.getType());

                    if (decorator.getNbToken() == 1) {

                        String fName = "f_" + timestamp + (index++);

                        // simple case one token
                        String token = fName + "." + decorator.getProperty(0);

                        String subQuery = op.toHql(paramName,
                                                   token,
                                                   data,
                                                   filterParams);

                        ruleFilter = propertyName + " IN ( FROM " +
                                     columnMeta.getType().getName() + " " +
                                     fName + " WHERE " + subQuery + ")";
                    } else {

                        // more than one property, headace time...
                        List<String> ruleFilters = Lists.newArrayList();
                        for (String decoratorToken : decorator.getTokens()) {
                            String fName = "f_" + timestamp + (index++);

                            // simple case one token
                            String token = fName + "." + decoratorToken;

                            String subQuery = op.toHql(paramName,
                                                       token,
                                                       data,
                                                       filterParams);

                            ruleFilters.add(propertyName + " IN ( FROM " +
                                            columnMeta.getType().getName() + " " +
                                            fName + " WHERE " + subQuery + ")");
                        }

                        String middleOp = "OR";
                        switch (op) {
                            // With negate operand, we must match all !(a or b) = !a and !b
                            case ne:
                            case nc:
                            case en:
                            case bn:
                                middleOp = "AND";
                                break;
                        }
                        ruleFilter = "( " + Joiner.on(" " + middleOp + " ").join(ruleFilters) + " )";
                    }
                }

            } else {

                if (columnMeta.isNumber()) {

                    if (op == FilterRuleOperator.cn ||
                        op == FilterRuleOperator.nc ||
                        op == FilterRuleOperator.bw ||
                        op == FilterRuleOperator.bn ||
                        op == FilterRuleOperator.ew ||
                        op == FilterRuleOperator.en) {

                        // string operation

//                        realData = "'" + realData + "'";
                        propertyName = "str(" + propertyName + ")";
                    } else {

                        // real number operation

                        if (columnMeta.getType() == byte.class ||
                            columnMeta.getType() == Byte.class) {
                            realData = Byte.valueOf(data);
                        } else if (columnMeta.getType() == char.class ||
                                   columnMeta.getType() == Character.class) {
                            realData = StringUtils.isEmpty(data) ? ' ' : data.charAt(0);
                        } else if (columnMeta.getType() == short.class ||
                                   columnMeta.getType() == Short.class) {
                            realData = Short.valueOf(data);
                        } else if (columnMeta.getType() == int.class ||
                                   columnMeta.getType() == Integer.class) {
                            realData = Integer.valueOf(data);
                        } else if (columnMeta.getType() == long.class ||
                                   columnMeta.getType() == Long.class) {
                            realData = Integer.valueOf(data);
                        } else if (columnMeta.getType() == float.class ||
                                   columnMeta.getType() == Float.class) {
                            realData = Float.valueOf(data);
                        } else if (columnMeta.getType() == double.class ||
                                   columnMeta.getType() == Double.class) {
                            realData = Double.valueOf(data);
                        } else if (columnMeta.getType() == BigInteger.class) {
                            realData = new BigInteger(data);
                        } else if (columnMeta.getType() == BigDecimal.class) {
                            realData = new BigDecimal(data);
                        } else {
                            throw new IllegalStateException(
                                    "Can't convert column (" + columnMeta.getName() + ") of type: " + columnMeta.getType());
                        }
                    }
                } else if (columnMeta.isDate()) {
                    propertyName = "str(" + propertyName + ")";
                }
                ruleFilter = op.toHql(paramName,
                                      propertyName,
                                      realData,
                                      filterParams);
            }

            strFilterRules.add("(" + ruleFilter + ")");

            if (CollectionUtils.isNotEmpty(strFilterRules)) {
                FilterRuleGroupOperator groupOp =
                        pager.getGroupOp();
                filterHql = StringUtils.join(strFilterRules, groupOp.name());
            }

            for (Map.Entry<String, Object> entry : filterParams.entrySet()) {
                params.add(entry.getKey());
                params.add(entry.getValue());
            }
        }
        return filterHql;
    }

    public void createEntityModificationLog(TableMeta<EchoBaseUserEntityEnum> tableMeta,
                                            String messagePrefix,
                                            TopiaEntity entity,
                                            EchoBaseUser user,
                                            BeanMonitor monitor) throws TopiaException {

        StringBuilder buffer;

        String topiaId = entity.getTopiaId();

        buffer = new StringBuilder(messagePrefix).append('\n');
        Locale l = getLocale();
        if (monitor == null) {

            // no monitor, means this is a creation of an object
            buffer.append(l(l, "echobase.info.newEntityCreated", topiaId));
        } else {

            PropertyDiff[] propertyDiffs = monitor.getPropertyDiffs();

            int length = propertyDiffs.length;

            if (length == 0) {

                //  no property modified, do nothing
                return;
            }

            if (length == 1) {
                buffer.append(l(l, "echobase.info.onePropertyModified"));
            } else {
                buffer.append(
                        l(l, "echobase.info.somePropertiesModified", length));
            }
            for (PropertyDiff diff : propertyDiffs) {

                Object sourceValue = diff.getSourceValue();
                Object targetValue = diff.getTargetValue();

                String propertyName = diff.getSourceProperty();

                if (tableMeta.getColumns(propertyName).isFK()) {

                    // replace by the decorate value
                    if (sourceValue != null) {
                        sourceValue = decoratorService.decorate(
                                sourceValue, null);
                    }
                    if (targetValue != null) {
                        targetValue = decoratorService.decorate(
                                targetValue, null);
                    }
                }
                buffer.append("\n ");
                buffer.append(
                        l(l, "echobase.info.modifiedProperty", propertyName,
                          sourceValue,
                          targetValue)
                );
            }
        }

        persistenceService.createEntityModificationLog(
                tableMeta.getSource().name(),
                topiaId,
                user.getEmail(),
                newDate(),
                buffer.toString()
        );
    }

    protected <E extends TopiaEntity> Map<String, Object> loadRow(
            E entity,
            ExportModel<E> loadModel) {

        Map<String, Object> row = Maps.newLinkedHashMap();

        Iterable<ExportableColumn<E, Object>> columns =
                loadModel.getColumnsForExport();

        for (ExportableColumn<E, Object> column : columns) {
            String propertyName = column.getHeaderName();
            Object value;
            try {
                value = column.getValue(entity);
            } catch (Exception eee) {
                throw new EchoBaseTechnicalException(
                        "Could not obtain property [" + propertyName +
                        "] value from entity " + entity.getTopiaId(), eee
                );
            }
            String formatedValue;
            try {
                formatedValue = column.formatValue(value);
            } catch (Exception eee) {
                throw new EchoBaseTechnicalException(
                        "Could not format property [" + propertyName +
                        "] from value [" + value +
                        "] from entity " + entity.getTopiaId(), eee
                );
            }
            row.put(propertyName, formatedValue);

        }
        return row;
    }

    protected <E extends TopiaEntity> ImportExportModel<E> buildForSave(TableMeta<EchoBaseUserEntityEnum> meta) {

        EntityCsvModel<EchoBaseUserEntityEnum, E> model = EntityCsvModel.newModel(
                getConfiguration().getCsvSeparator(),
                meta,
                TopiaEntity.PROPERTY_TOPIA_ID
        );

        for (ColumnMeta columnMeta : meta) {
            String propertyName = columnMeta.getName();
            Class<?> type = columnMeta.getType();
            if (columnMeta.isFK()) {

                Class<TopiaEntity> entityType = (Class<TopiaEntity>) type;
                Collection<TopiaEntity> universe = getForeignData(entityType);

                // translate foreign key to his id
                model.addForeignKeyForImport(propertyName, entityType, universe);

            } else if (Date.class.equals(type)) {
                model.newMandatoryColumn(
                        propertyName,
                        EchoBaseCsvUtil.DATE_TIME_VALUE_PARSER);
            } else {
                model.addDefaultColumn(propertyName, type);
            }
        }
        return model;
    }

    protected <E extends TopiaEntity> ExportModel<E> buildForLoad(TableMeta<EchoBaseUserEntityEnum> meta,
                                                                  String idName,
                                                                  boolean addDecorated) {

        EntityCsvModel<EchoBaseUserEntityEnum, E> model = EntityCsvModel.newModel(
                getConfiguration().getCsvSeparator(),
                meta,
                idName
        );

        for (ColumnMeta columnMeta : meta) {
            String propertyName = columnMeta.getName();
            Class<?> type = columnMeta.getType();
            if (columnMeta.isFK()) {

                Class<TopiaEntity> entityType = (Class<TopiaEntity>) type;

                // translate foreign key to his id
                model.addForeignKeyForExport(propertyName, entityType);

                if (addDecorated) {

                    // translate foreign key to his decorated value
                    Decorator<TopiaEntity> decorator = getDecorator(entityType);
                    model.addDecoratedForeignKeyForExport(
                            propertyName + "_lib",
                            propertyName,
                            decorator
                    );
                }

            } else if ("id".equals(propertyName)) {

                if ("id".equals(idName)) {

                    // there will be a conflict with this meta, rename it to ID
                    model.addDefaultColumn("ID", propertyName, type);
                } else {

                    // no id conflict
                    model.addDefaultColumn(propertyName, type);

                }
            } else if ("importId".equals(propertyName) &&
                       Collection.class.equals(type)) {

                // special case we do not want to deal with {@link ImportLog#importId}
            } else {
                model.addDefaultColumn(propertyName, type);
            }
        }
        return model;
    }
}
