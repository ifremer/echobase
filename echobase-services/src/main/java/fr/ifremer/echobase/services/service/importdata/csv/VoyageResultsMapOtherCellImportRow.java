/*
 * #%L
 * EchoBase :: Services
 * %%
 * Copyright (C) 2011 - 2012 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ifremer.echobase.services.service.importdata.csv;

import fr.ifremer.echobase.entities.data.Cell;
import fr.ifremer.echobase.entities.data.CellImpl;
import fr.ifremer.echobase.entities.data.Result;
import fr.ifremer.echobase.entities.data.Voyage;
import fr.ifremer.echobase.entities.references.CellType;
import fr.ifremer.echobase.entities.references.DataQuality;
import fr.ifremer.echobase.services.csv.ResultAble;
import java.util.Collection;

import java.util.LinkedList;
import java.util.List;

/**
 * Bean used as a row for import of {@link VoyageResultsMapFishCellImportExportModel}.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 0.3
 */
public class VoyageResultsMapOtherCellImportRow implements ResultAble {

    public static final String PROPERTY_NAME = "name";

    public static final String PROPERTY_VOYAGE = "voyage";

    public static final String PROPERTY_CELL = "cell";

    public static final String PROPERTY_DATA_GRID_CELL_LONGITUDE = "gridCellLongitude";

    public static final String PROPERTY_DATA_GRID_CELL_LATITUDE = "gridCellLatitude";

    public static final String PROPERTY_DATA_GRID_CELL_DEPTH = "gridCellDepth";

    public static final String PROPERTY_DATA_GRID_LONGITUDE_LAG = "gridLongitudeLag";

    public static final String PROPERTY_DATA_GRID_LATITUDE_LAG = "gridLatitudeLag";

    public static final String PROPERTY_DATA_GRID_DEPTH_LAG = "gridDepthLag";

    public static final String PROPERTY_DATA_QUALITY = "dataQuality";

    protected final Cell cell;

    protected Voyage voyage;

    protected final List<Result> result = new LinkedList<>();

    protected DataQuality dataQuality;

    protected float gridCellLongitude;

    protected float gridCellLatitude;

    protected float gridCellDepth;

    protected float gridLongitudeLag;

    protected float gridLatitudeLag;

    protected float gridDepthLag;

    public static VoyageResultsMapOtherCellImportRow of(Voyage voyage, Cell cell) {
        VoyageResultsMapOtherCellImportRow row = new VoyageResultsMapOtherCellImportRow(cell);
        row.setVoyage(voyage);
        
        Collection<Result> results = cell.getResult();
        row.result.addAll(results);
        row.setDataQuality(results.iterator().next().getDataQuality());
        
        return row;
    }

    public VoyageResultsMapOtherCellImportRow(Cell cell) {
        this.cell = cell;
    }

    public VoyageResultsMapOtherCellImportRow(CellType cellType) {
        this(new CellImpl());
        this.cell.setCellType(cellType);
    }

    @Override
    public List<Result> getResult() {
        return result;
    }

    @Override
    public void addResult(Result result) {
        this.result.add(result);
    }

    @Override
    public DataQuality getDataQuality() {
        return dataQuality;
    }

    @Override
    public void setDataQuality(DataQuality dataQuality) {
        this.dataQuality = dataQuality;
    }


    public Cell getCell() {
        return cell;
    }

    public Voyage getVoyage() {
        return voyage;
    }

    public void setVoyage(Voyage voyage) {
        this.voyage = voyage;
    }

    public float getGridCellLongitude() {
        return gridCellLongitude;
    }

    public void setGridCellLongitude(float GridCellLongitude) {
        this.gridCellLongitude = GridCellLongitude;
    }

    public float getGridCellLatitude() {
        return gridCellLatitude;
    }

    public void setGridCellLatitude(float GridCellLatitude) {
        this.gridCellLatitude = GridCellLatitude;
    }

    public float getGridCellDepth() {
        return gridCellDepth;
    }

    public void setGridCellDepth(float GridCellDepth) {
        this.gridCellDepth = GridCellDepth;
    }

    public float getGridLongitudeLag() {
        return gridLongitudeLag;
    }

    public void setGridLongitudeLag(float GridLongitudeLag) {
        this.gridLongitudeLag = GridLongitudeLag;
    }

    public float getGridLatitudeLag() {
        return gridLatitudeLag;
    }

    public void setGridLatitudeLag(float GridLatitudeLag) {
        this.gridLatitudeLag = GridLatitudeLag;
    }

    public float getGridDepthLag() {
        return gridDepthLag;
    }

    public void setGridDepthLag(float GridDepthLag) {
        this.gridDepthLag = GridDepthLag;
    }

    public String getName() {
        return cell.getName();
    }

    public void setName(String name) {
        cell.setName(name);
    }

}
