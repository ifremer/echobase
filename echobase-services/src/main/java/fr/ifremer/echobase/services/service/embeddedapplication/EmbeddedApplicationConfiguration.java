/*
 * #%L
 * EchoBase :: Services
 * %%
 * Copyright (C) 2011 - 2012 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ifremer.echobase.services.service.embeddedapplication;

import fr.ifremer.echobase.services.AbstractEchobaseActionConfiguration;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;

/**
 * Configuration of a embedded application creation.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 0.2
 */
public class EmbeddedApplicationConfiguration extends AbstractEchobaseActionConfiguration {

    private static final long serialVersionUID = 1L;

    /** File name of the embedded application archive (without the zip extension). */
    private String fileName;

    /** Working directory where to generate the archive of embedded application. */
    private File workingDirectory;

    /** Embedded application archive file. */
    private File embeddedApplicationFile;

    /** Location of the war file to push in embedded application. */
    private File warLocation;

    /** Ids of voyages to push in embedded application (with all their datas). */
    private String[] voyageIds;

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public File getWorkingDirectory() {
        return workingDirectory;
    }

    public void setWorkingDirectory(File workingDirectory) {
        this.workingDirectory = workingDirectory;
    }

    public File getEmbeddedApplicationFile() {
        return embeddedApplicationFile;
    }

    public void setEmbeddedApplicationFile(File embeddedApplicationFile) {
        this.embeddedApplicationFile = embeddedApplicationFile;
    }

    public String[] getVoyageIds() {
        return voyageIds;
    }

    // Attention on ne peut pas utiliser un ... car sinon ça plante dans l'ui
    public void setVoyageIds(String[] voyageIds) {
        this.voyageIds = Arrays.copyOf(voyageIds, voyageIds.length);
    }

    public File getWarLocation() {
        return warLocation;
    }

    public void setWarLocation(File warLocation) {
        this.warLocation = warLocation;
    }

    @Override
    public void destroy() throws IOException {
        if (workingDirectory != null) {
            FileUtils.deleteDirectory(workingDirectory);
        }
    }
}
