package fr.ifremer.echobase.services.service;

/*
 * #%L
 * EchoBase :: Services
 * %%
 * Copyright (C) 2011 - 2013 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.base.Function;
import com.google.common.base.Optional;
import com.google.common.base.Preconditions;
import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;
import com.google.common.collect.LinkedListMultimap;
import com.google.common.collect.Maps;
import com.google.common.collect.Multimap;
import com.google.common.collect.Sets;
import fr.ifremer.echobase.EchoBaseTechnicalException;
import fr.ifremer.echobase.entities.EchoBaseUserPersistenceContext;
import fr.ifremer.echobase.entities.EntityModificationLog;
import fr.ifremer.echobase.entities.ImportFile;
import fr.ifremer.echobase.entities.ImportFileId;
import fr.ifremer.echobase.entities.ImportFileIdTopiaDao;
import fr.ifremer.echobase.entities.ImportLog;
import fr.ifremer.echobase.entities.ImportLogTopiaDao;
import fr.ifremer.echobase.entities.ImportType;
import fr.ifremer.echobase.entities.ImportedCell;
import fr.ifremer.echobase.entities.ImportedCellResult;
import fr.ifremer.echobase.entities.ImportedSampleDataResult;
import fr.ifremer.echobase.entities.data.Category;
import fr.ifremer.echobase.entities.data.Cell;
import fr.ifremer.echobase.entities.data.CellTopiaDao;
import fr.ifremer.echobase.entities.data.Data;
import fr.ifremer.echobase.entities.data.DataAcquisition;
import fr.ifremer.echobase.entities.data.DataProcessing;
import fr.ifremer.echobase.entities.data.Echotype;
import fr.ifremer.echobase.entities.data.GearMetadataValue;
import fr.ifremer.echobase.entities.data.LengthAgeKey;
import fr.ifremer.echobase.entities.data.LengthWeightKey;
import fr.ifremer.echobase.entities.data.Mooring;
import fr.ifremer.echobase.entities.data.Operation;
import fr.ifremer.echobase.entities.data.OperationMetadataValue;
import fr.ifremer.echobase.entities.data.Result;
import fr.ifremer.echobase.entities.data.Sample;
import fr.ifremer.echobase.entities.data.SampleData;
import fr.ifremer.echobase.entities.data.Transect;
import fr.ifremer.echobase.entities.data.Transit;
import fr.ifremer.echobase.entities.data.Voyage;
import fr.ifremer.echobase.entities.references.AcousticInstrument;
import fr.ifremer.echobase.entities.references.AgeCategory;
import fr.ifremer.echobase.entities.references.AncillaryInstrumentation;
import fr.ifremer.echobase.entities.references.AreaOfOperation;
import fr.ifremer.echobase.entities.references.Calibration;
import fr.ifremer.echobase.entities.references.CellType;
import fr.ifremer.echobase.entities.references.CellTypeImpl;
import fr.ifremer.echobase.entities.references.CellTypes;
import fr.ifremer.echobase.entities.references.DataMetadata;
import fr.ifremer.echobase.entities.references.DataQuality;
import fr.ifremer.echobase.entities.references.DepthStratum;
import fr.ifremer.echobase.entities.references.Gear;
import fr.ifremer.echobase.entities.references.GearMetadata;
import fr.ifremer.echobase.entities.references.Mission;
import fr.ifremer.echobase.entities.references.OperationMetadata;
import fr.ifremer.echobase.entities.references.Port;
import fr.ifremer.echobase.entities.references.SampleDataType;
import fr.ifremer.echobase.entities.references.SampleType;
import fr.ifremer.echobase.entities.references.SexCategory;
import fr.ifremer.echobase.entities.references.SizeCategory;
import fr.ifremer.echobase.entities.references.Species;
import fr.ifremer.echobase.entities.references.Species2;
import fr.ifremer.echobase.entities.references.SpeciesCategory;
import fr.ifremer.echobase.entities.references.Vessel;
import fr.ifremer.echobase.entities.references.VocabularyCIEM;
import fr.ifremer.echobase.entities.references.VocabularyCIEMTopiaDao;
import fr.ifremer.echobase.io.InputFile;
import fr.ifremer.echobase.services.EchoBaseServiceSupport;
import fr.ifremer.echobase.services.csv.CellValueFormatter;
import fr.ifremer.echobase.services.csv.CellValueParser;
import org.apache.commons.io.IOUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.csv.ValueFormatter;
import org.nuiton.csv.ValueParser;
import org.nuiton.decorator.Decorator;
import org.nuiton.topia.persistence.TopiaDao;
import org.nuiton.topia.persistence.TopiaEntity;
import org.nuiton.topia.persistence.TopiaException;
import org.nuiton.topia.persistence.TopiaIdFactory;
import org.nuiton.topia.persistence.TopiaNotFoundException;
import org.nuiton.topia.persistence.support.TopiaSqlQuery;

import javax.inject.Inject;
import javax.sql.rowset.serial.SerialBlob;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.zip.GZIPOutputStream;

/**
 * TODO
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.2
 */
public class UserDbPersistenceService extends EchoBaseServiceSupport {

    /** Logger. */
    private static final Log log = LogFactory.getLog(UserDbPersistenceService.class);

    @Inject
    private EchoBaseUserPersistenceContext persistenceContext;

    @Inject
    private DecoratorService decoratorService;

    public void setSpatialStructureFound() {

        persistenceContext.setSpatialStructureFound(true);
        serviceContext.getEchoBaseUserApplicationContext().setSpatialStructureFound(true);

    }

    //------------------------------------------------------------------------//
    //--- AgeCategory --------------------------------------------------------//
    //------------------------------------------------------------------------//

    public AgeCategory createAgeCategory(String ageCategoryName,
                                         String ageCategoryMeaning) {

        return persistenceContext.getAgeCategoryDao().create(
                AgeCategory.PROPERTY_NAME, ageCategoryName,
                AgeCategory.PROPERTY_MEANING, ageCategoryMeaning
        );
    }

    //------------------------------------------------------------------------//
    //--- AreaOfOperation ----------------------------------------------------//
    //------------------------------------------------------------------------//

    public AreaOfOperation getAreaOfOperation(String id) {
        return persistenceContext.getAreaOfOperationDao().forTopiaIdEquals(id).findUnique();
    }

    //------------------------------------------------------------------------//
    //--- AncillaryInstrumentation ----------------------------------------------------//
    //------------------------------------------------------------------------//

    public AncillaryInstrumentation getAncillaryInstrumentation(String id) {
        return persistenceContext.getAncillaryInstrumentationDao().forTopiaIdEquals(id).findUnique();
    }

    //------------------------------------------------------------------------//
    //--- Calibration --------------------------------------------------------//
    //------------------------------------------------------------------------//

    public Calibration getCalibration(String id) {
        return persistenceContext.getCalibrationDao().forTopiaIdEquals(id).findUnique();
    }

    public Calibration createCalibration(String accuracyEstimate,
                                         AcousticInstrument accousticInstrument,
                                         String acquisitionMethod,
                                         String comments,
                                         Date date,
                                         String processingMethod,
                                         String report) {
        return persistenceContext.getCalibrationDao().create(
                Calibration.PROPERTY_ACCURACY_ESTIMATE, accuracyEstimate,
                Calibration.PROPERTY_ACOUSTIC_INSTRUMENT, accousticInstrument,
                Calibration.PROPERTY_AQUISITION_METHOD, acquisitionMethod,
                Calibration.PROPERTY_COMMENTS, comments,
                Calibration.PROPERTY_DATE, date,
                Calibration.PROPERTY_PROCESSING_METHOD, processingMethod,
                Calibration.PROPERTY_REPORT, report
        );
    }

    public void deleteCalibration(Calibration calibration) {
        persistenceContext.getCalibrationDao().delete(calibration);
    }

    //------------------------------------------------------------------------//
    //--- Category -----------------------------------------------------------//
    //------------------------------------------------------------------------//

    public Category getCategory(String categoryId) {
        return persistenceContext.getCategoryDao().forTopiaIdEquals(categoryId).findUnique();
    }

    public Category getCategoryByEchotypeAndSpeciesCategory(Echotype echotype,
                                                            SpeciesCategory speciesCategory) {
        return persistenceContext.getCategoryDao().forProperties(
                Category.PROPERTY_ECHOTYPE, echotype,
                Category.PROPERTY_SPECIES_CATEGORY, speciesCategory
        ).findAnyOrNull();
    }

    public List<Category> getCategoryUsingEchotype(Voyage voyage) throws TopiaException {

        return persistenceContext.getCategoryDao().
                getCategoryUsingEchotype(voyage);
    }

    public List<Category> getCategorysByEchotype(Echotype echotype) {
        return persistenceContext.getCategoryDao().forEchotypeEquals(echotype).findAll();
    }

//    public Iterable<Category> getCategories(Predicate<Category> acceptPredicate) {
//        return Iterables.filter(persistenceContext.getCategoryDao(), acceptPredicate);
//    }

    public long countCategoryUsingEchotype(Voyage voyage) throws TopiaException {

        return persistenceContext.getCategoryDao().
                countCategoryUsingEchotype(voyage);
    }

    public Category createCategory(Echotype echotype,
                                   SpeciesCategory speciesCategory) {
        return persistenceContext.getCategoryDao().create(
                Category.PROPERTY_ECHOTYPE, echotype,
                Category.PROPERTY_SPECIES_CATEGORY, speciesCategory
        );
    }

    public void deleteCategory(Category category) {
        persistenceContext.getCategoryDao().delete(category);
    }

    public void deleteCategories(Collection<Category> categories) {
        persistenceContext.getCategoryDao().deleteAll(categories);
    }

    //------------------------------------------------------------------------//
    //--- Cell ---------------------------------------------------------------//
    //------------------------------------------------------------------------//

    public boolean containsCellByName(String cellName) {
        return persistenceContext.getCellDao().containsCellByName(cellName);
    }

    public boolean containsPostVoyageCellByName(Voyage voyage, String cellName) {
        return persistenceContext.getCellDao()
                                 .forNameEquals(cellName)
                                 .addEquals(Cell.PROPERTY_VOYAGE, voyage)
                                 .exists();
    }

    public boolean containsPostVoyageCellByNameAndType(Voyage voyage, String cellName, CellType cellType) {
        return persistenceContext.getCellDao()
                                 .forNameEquals(cellName)
                                 .addEquals(Cell.PROPERTY_VOYAGE, voyage)
                                 .addEquals(Cell.PROPERTY_CELL_TYPE, cellType)
                                 .exists();
    }

    public Cell getCell(String id) {
        return persistenceContext.getCellDao().forTopiaIdEquals(id).findUnique();
    }

    public Cell getCellByVoyageNameAndType(String cellName, CellType cellType, Voyage voyage) {
        return persistenceContext.getCellDao()
                .forNameEquals(cellName)
                .addEquals(Cell.PROPERTY_CELL_TYPE, cellType)
                .addEquals(Cell.PROPERTY_VOYAGE, voyage)
                .findUniqueOrNull();
    }

    public Optional<Cell> getOptionalCell(String id) {
        return persistenceContext.getCellDao().forTopiaIdEquals(id).tryFindUnique();
    }

    public Cell getCellContainsResult(Result result) {
        return persistenceContext.getCellDao().forResultContains(result).findAnyOrNull();
    }

    public long countVoyageOrphanCells(final Voyage voyage) {
        return persistenceContext.getCellDao().countVoyageOrphanCells(voyage);
    }

    public long countMooringOrphanCells(Mooring mooring) {
        return persistenceContext.getCellDao().countMooringOrphanCells(mooring);
    }

    public long countVoyageCellResults(final Voyage voyage) {
        return persistenceContext.getCellDao().countVoyageCellResults(voyage);
    }

    public long countMooringCellResults(Mooring mooring) {
        return persistenceContext.getCellDao().countMooringCellResults(mooring);
    }

    public List<String> getOrphanCellIds() throws TopiaException {
        return persistenceContext.getCellDao().getOrphanCellIds();
    }

    public List<String> getVoyageCellIds(Voyage voyage) throws TopiaException {
        return persistenceContext.getCellDao().getVoyageCellIds(voyage);
    }

    public List<String> getMooringCellIds(Mooring mooring) throws TopiaException {
        return persistenceContext.getCellDao().getMooringCellIds(mooring);
    }
    
    public ValueParser<Cell> newCellValueParser() {

        CellTopiaDao cellDao = persistenceContext.getCellDao();
        return new CellValueParser(cellDao);
    }

    public ValueFormatter<Cell> newCellValueFormatter() {
        CellTopiaDao cellDao = persistenceContext.getCellDao();
        CellType esduCellType = getEsduCellType();
        CellType elementaryCellType = getElementaryCellType();
        return new CellValueFormatter(cellDao, esduCellType, elementaryCellType);
    }

    public Cell createCell(CellType cellType, String cellName, DataQuality dataQuality) {
        return persistenceContext.getCellDao().create(
                Cell.PROPERTY_CELL_TYPE, cellType,
                Cell.PROPERTY_NAME, cellName,
                Cell.PROPERTY_DATA_QUALITY, dataQuality);
    }

    public Cell createCell(Cell cell) {
        return persistenceContext.getCellDao().create(cell);
    }

    public void deleteCell(Cell cell) {
        persistenceContext.getCellDao().delete(cell);
    }

    //------------------------------------------------------------------------//
    //--- CellType -----------------------------------------------------------//
    //------------------------------------------------------------------------//

    public Set<CellType> getRegionCellTypes() {
        List<CellType> cellTypes = persistenceContext.getCellTypeDao().findAll();
        return Sets.newHashSet(Iterables.filter(cellTypes, CellTypes.IS_REGION_CELL_TYPE));
    }

//    public CellType getCellType(String id) {
//        return persistenceContext.getCellTypeDao().findByTopiaId(id);
//    }

    public CellType getEsduCellType() {
        return getCellTypeById(CellTypeImpl.ESDU);
    }

    public CellType getElementaryCellType() {
        return getCellTypeById(CellTypeImpl.ELEMENTARY);
    }

    public CellType getMapCellType() {
        return getCellTypeById(CellTypeImpl.MAP);
    }

    public CellType getCellTypeById(String id) {
        return persistenceContext.getCellTypeDao().forIdEquals(id).findUnique();
    }

    //------------------------------------------------------------------------//
    //--- Data ---------------------------------------------------------------//
    //------------------------------------------------------------------------//

    public Data createData(DataMetadata dataMetaData, String dataValue) {
        return persistenceContext.getDataDao().create(
                Data.PROPERTY_DATA_METADATA, dataMetaData,
                Data.PROPERTY_DATA_VALUE, dataValue
        );
    }

    //------------------------------------------------------------------------//
    //--- DataAcquisition ----------------------------------------------------//
    //------------------------------------------------------------------------//

    public DataAcquisition getDataAcquisition(String id) {
        return persistenceContext.getDataAcquisitionDao().forTopiaIdEquals(id).findUnique();
    }

    public DataAcquisition createDataAcquisition(AcousticInstrument instrument) {
        return persistenceContext.getDataAcquisitionDao().create(
                DataAcquisition.PROPERTY_ACOUSTIC_INSTRUMENT, instrument
        );
    }

    public void deleteDataAcquisition(DataAcquisition dataAcquisition) {
        persistenceContext.getDataAcquisitionDao().delete(dataAcquisition);
    }

    //------------------------------------------------------------------------//
    //--- DataMetadata -------------------------------------------------------//
    //------------------------------------------------------------------------//

    public DataMetadata getDataMetadata(String id) {
        return persistenceContext.getDataMetadataDao().forTopiaIdEquals(id).findUnique();
    }

    public DataMetadata getDataMetadataByName(String name) {
        return persistenceContext.getDataMetadataDao().
                forNameEquals(name).findUnique();
    }

    public DataMetadata tryToGetDataMetadataByName(String name) {
        return persistenceContext.getDataMetadataDao().
                forNameEquals(name).findUniqueOrNull();
    }

    public List<DataMetadata> getDataMetadatasInName(Set<String> names) {
        return persistenceContext.getDataMetadataDao().forNameIn(names).findAll();
    }

    public TopiaEntity getEntity(String id) {
        return persistenceContext.findByTopiaId(id);
    }

    public CoserIndicators getRegionIndicators(String missionId, List<DataMetadata> dataMetadatas) {

        if (log.isInfoEnabled()) {
            for (DataMetadata dataMetadata : dataMetadatas) {
                log.info("Possible indicator: " + dataMetadata.getName());
            }
        }

        Predicate<Species> communityIndicatorSpeciesPredicate = Species2.newCommunityIndicatorSpeciesPredicate();
        Predicate<Species> populationIndicatorSpeciesPredicate = Species2.newPopulationIndicatorSpeciesPredicate();

        Set<DataMetadata> communityDataMetadatasSet = new HashSet<>();
        Set<DataMetadata> populationDataMetadatasSet = new HashSet<>();

        Mission mission = getMission(missionId);
        List<Voyage> voyagesForMission = getVoyagesForMission(mission);

        for (Voyage voyage : voyagesForMission) {

            if (log.isInfoEnabled()) {
                log.info("Scan voyage: " + voyage.getName());
            }

            for (Cell cell : voyage.getRegionCells()) {

                for (Result result : cell.getResult()) {

                    Species species = result.getCategory().getSpeciesCategory().getSpecies();

                    if (communityIndicatorSpeciesPredicate.apply(species)) {

                        communityDataMetadatasSet.add(result.getDataMetadata());

                    } else if (populationIndicatorSpeciesPredicate.apply(species)) {

                        populationDataMetadatasSet.add(result.getDataMetadata());

                    }

                }
            }
        }

        List<DataMetadata> communityDataMetadatasList = new ArrayList<>(communityDataMetadatasSet);
        if (log.isInfoEnabled()) {
            for (DataMetadata dataMetadata : communityDataMetadatasList) {
                boolean willUse = dataMetadatas.contains(dataMetadata);
                if (willUse) {
                    log.info("Community indicator accepted: " + dataMetadata.getName());
                } else {
                    log.info("Community indicator rejected: " + dataMetadata.getName());
                }
            }
        }
        communityDataMetadatasList.retainAll(dataMetadatas);

        List<DataMetadata> populationDataMetadatasList = new ArrayList<>(populationDataMetadatasSet);
        if (log.isInfoEnabled()) {
            for (DataMetadata dataMetadata : populationDataMetadatasList) {
                boolean willUse = dataMetadatas.contains(dataMetadata);
                if (willUse) {
                    log.info("Population indicator accepted: " + dataMetadata.getName());
                } else {
                    log.info("Population indicator rejected: " + dataMetadata.getName());
                }
            }
        }
        populationDataMetadatasList.retainAll(dataMetadatas);

        return new CoserIndicators(communityDataMetadatasList, populationDataMetadatasList);

    }

    //------------------------------------------------------------------------//
    //--- DataProcessing -----------------------------------------------------//
    //------------------------------------------------------------------------//

    public DataProcessing createDataProcessing(String id,
                                               String processingTemplate) {
        return persistenceContext.getDataProcessingDao().create(
                DataProcessing.PROPERTY_ID, id,
                DataProcessing.PROPERTY_PROCESSING_TEMPLATE, processingTemplate
        );
    }

    public DataProcessing getDataProcessing(String id) {
        return persistenceContext.getDataProcessingDao().forTopiaIdEquals(id).findUnique();
    }

    public DataProcessing getDataProcessingContainsCell(Cell cell) {
        return persistenceContext.getDataProcessingDao().forCellContains(cell).findAnyOrNull();
    }

    public Map<String, String> getDataProcessings(Voyage voyage) {
        Map<String, String> result = Maps.newLinkedHashMap();

        Decorator<Transit> transitDecorator = decoratorService.getDecorator(Transit.class, null);
        Decorator<Transect> transectDecorator = decoratorService.getDecorator(Transect.class, null);
        Decorator<DataAcquisition> dataAcquisitionDecorator = decoratorService.getDecorator(DataAcquisition.class, null);
        Decorator<DataProcessing> dataProcessingDecorator = decoratorService.getDecorator(DataProcessing.class, null);

            for (Transit transit : voyage.getTransit()) {
                String transitStr = transitDecorator.toString(transit);

                for (Transect transect : transit.getTransect()) {
                    String transectStr = transitStr + " / " + transectDecorator.toString(transect);

                    for (DataAcquisition dataAcquisition : transect.getDataAcquisition()) {
                        String dataAcquisitionStr = transectStr + " / " + dataAcquisitionDecorator.toString(dataAcquisition);

                        if (!dataAcquisition.isDataProcessingEmpty()) {
                            for (DataProcessing dataProcessing : dataAcquisition.getDataProcessing()) {

                                String value = dataAcquisitionStr + dataProcessingDecorator.toString(dataProcessing);
                                result.put(dataProcessing.getTopiaId(), value);
                            }
                        }
                    }
                }
            }
        return result;
    }

    public Map<String, String> getDataProcessings(Mooring mooring) {
        Map<String, String> result = Maps.newLinkedHashMap();

        Decorator<DataAcquisition> dataAcquisitionDecorator = decoratorService.getDecorator(DataAcquisition.class, null);
        Decorator<DataProcessing> dataProcessingDecorator = decoratorService.getDecorator(DataProcessing.class, null);

        for (DataAcquisition dataAcquisition : mooring.getDataAcquisition()) {
            String dataAcquisitionStr = dataAcquisitionDecorator.toString(dataAcquisition);

            if (!dataAcquisition.isDataProcessingEmpty()) {
                for (DataProcessing dataProcessing : dataAcquisition.getDataProcessing()) {

                    String value = dataAcquisitionStr + dataProcessingDecorator.toString(dataProcessing);
                    result.put(dataProcessing.getTopiaId(), value);
                }
            }
        }
        return result;
    }

    //------------------------------------------------------------------------//
    //--- Echotype -----------------------------------------------------------//
    //------------------------------------------------------------------------//

//    public Optional<Echotype> getOptionalEchotype(String id) {
//        return Optional.fromNullable(persistenceContext.getEchotypeDao().forTopiaIdEquals(id).findAnyOrNull());
//    }

    public Echotype getEchotype(String id) {
        return persistenceContext.getEchotypeDao().forTopiaIdEquals(id).findUnique();
    }

    public Echotype createEchotype(Echotype echotype) {
        return persistenceContext.getEchotypeDao().create(echotype);
    }

    public void deleteEchotype(Echotype echotype) {
        persistenceContext.getEchotypeDao().delete(echotype);
    }

    //------------------------------------------------------------------------//
    //--- EntityModificationLog ----------------------------------------------//
    //------------------------------------------------------------------------//

    public EntityModificationLog createEntityModificationLog(String entityType,
                                                             String entityId,
                                                             String userEmail,
                                                             Date date,
                                                             String comment) {
        return persistenceContext.getEntityModificationLogDao().create(
                EntityModificationLog.PROPERTY_ENTITY_TYPE, entityType,
                EntityModificationLog.PROPERTY_ENTITY_ID, entityId,
                EntityModificationLog.PROPERTY_MODIFICATION_USER, userEmail,
                EntityModificationLog.PROPERTY_MODIFICATION_DATE, date,
                EntityModificationLog.PROPERTY_MODIFICATION_TEXT, comment
        );
    }

    //------------------------------------------------------------------------//
    //--- GearMetadataValue --------------------------------------------------//
    //------------------------------------------------------------------------//

    public boolean containsGearMetadataValue(Voyage voyage, Vessel vessel, DepthStratum depthStratum, String operationId, Gear gear, GearMetadata gearMetadata) {
        return persistenceContext.getGearMetadataValueDao()
                                 .forGearMetadataEquals(gearMetadata)
                                 .addEquals(GearMetadataValue.PROPERTY_GEAR, gear)
                                 .addEquals(GearMetadataValue.PROPERTY_OPERATION + "." + Operation.PROPERTY_ID, operationId)
                                 .addEquals(GearMetadataValue.PROPERTY_OPERATION + "." + Operation.PROPERTY_DEPTH_STRATUM, depthStratum)
                                 .addEquals(GearMetadataValue.PROPERTY_OPERATION + "." + Operation.PROPERTY_TRANSECT + "." + Transect.PROPERTY_VESSEL, vessel)
                                 .addEquals(GearMetadataValue.PROPERTY_OPERATION + "." + Operation.PROPERTY_TRANSECT + "." + Transect.PROPERTY_TRANSIT + "." + Transit.PROPERTY_VOYAGE, voyage)
                                 .exists();
    }

    public GearMetadataValue createGearMetadataValue(GearMetadataValue gearMetadataValue) {
        return persistenceContext.getGearMetadataValueDao().create(gearMetadataValue);
    }

    public GearMetadataValue getGearMetadataValue(String gearMetadataValueId) {
        return persistenceContext.getGearMetadataValueDao().forTopiaIdEquals(gearMetadataValueId).findUnique();
    }

    //------------------------------------------------------------------------//
    //--- ImportLog ----------------------------------------------------------//
    //------------------------------------------------------------------------//

    public List<ImportLog> getImportLogs() {
        return persistenceContext.getImportLogDao().findAll();
    }

    public Optional<ImportLog> getOptionalImportLog(String id) {
        return persistenceContext.getImportLogDao().forTopiaIdEquals(id).tryFindUnique();
    }

    public ImportLog createImportLog(ImportType importType,
                                     String userEMail,
                                     Date date,
                                     String comment) {
        return persistenceContext.getImportLogDao().create(ImportLog.PROPERTY_IMPORT_TYPE, importType,
                                                           ImportLog.PROPERTY_IMPORT_USER, userEMail,
                                                           ImportLog.PROPERTY_IMPORT_DATE, date,
                                                           ImportLog.PROPERTY_IMPORT_TEXT, comment);
    }

    public void deleteImportLog(ImportLog importLog) {
        persistenceContext.getImportLogDao().delete(importLog);
    }

    public ImportFile createImportFile(InputFile inputFile) {

        Preconditions.checkNotNull(inputFile);
        Preconditions.checkArgument(inputFile.hasFile());

        SerialBlob importFileBLob = getSerialBlob(inputFile.getFile());

        return persistenceContext.getImportFileDao().create(ImportFile.PROPERTY_NAME, inputFile.getFileName(),
                                                            ImportFile.PROPERTY_FILE, importFileBLob);
    }

    public void addCheckedExportFile(String id, InputFile checkFile) {

        Preconditions.checkNotNull(id);
        Preconditions.checkNotNull(checkFile);
        Preconditions.checkArgument(checkFile.hasFile());

        SerialBlob checkFileBLob = getSerialBlob(checkFile.getFile());
        
        ImportFile importFile = persistenceContext.getImportFileDao().forTopiaIdEquals(id).findUnique();
        importFile.setCheckFile(checkFileBLob);
        flush();
    }

    private SerialBlob getSerialBlob(File file) {
        SerialBlob serialBlob;

        try {


            try (ByteArrayOutputStream out = new ByteArrayOutputStream()) {

                try (OutputStream outputStream = new GZIPOutputStream(out)) {

                    try (InputStream stream = new FileInputStream(file)) {

                        IOUtils.copy(stream, outputStream);

                    }
                }

                byte[] fileContent = out.toByteArray();
                serialBlob = new SerialBlob(fileContent);

            }

        } catch (Exception e) {
            throw new EchoBaseTechnicalException("Could not serialize file content " + file, e);
        }
        return serialBlob;
    }

    public Collection<ImportFile> getImportFiles(String importLogId) {

        ImportLog importLog = persistenceContext.getImportLogDao().forTopiaIdEquals(importLogId).findUnique();
        return importLog.getImportFile();

    }

    public ImportFile getImportFile(String importFileId) {

        return persistenceContext.getImportFileDao().forTopiaIdEquals(importFileId).findUnique();

    }

    public boolean isImportLogFor(String entityId, String importLogId, ImportType importType) {
        ImportLogTopiaDao importLogDao = persistenceContext.getImportLogDao();
        return importLogDao.forEntityIdEquals(entityId)
                           .addNotEquals(ImportLog.PROPERTY_TOPIA_ID, importLogId)
                           .addEquals(ImportLog.PROPERTY_IMPORT_TYPE, importType)
                           .exists();
    }

    public Multimap<String, ImportLog> indexImportLogByEntityId() {
        List<ImportLog> importLogs = getImportLogs();
        Multimap<String, ImportLog> indexes = LinkedListMultimap.create();

        for (ImportLog importLog : importLogs) {
            String entityId = importLog.getEntityId();
            if (entityId == null) {
                
                Collection<ImportFile> importFiles = importLog.getImportFile();
                for (ImportFile importFile : importFiles) {
                    Iterable<ImportFileId> importFileIds = getImportFileIdsForImportFile(importFile);
                    
                    for (ImportFileId importFileId : importFileIds) {
                        String id = importFileId.getEntityId();
                        indexes.put(id, importLog);
                    }
                }
            } else {
                indexes.put(entityId, importLog);
            }
        }
        
        return indexes;
    }
    
    //------------------------------------------------------------------------//
    //--- Port ---------------------------------------------------------------//
    //------------------------------------------------------------------------//

    public Port getPort(String id) {
        return persistenceContext.getPortDao().forTopiaIdEquals(id).findUnique();
    }
    
    //------------------------------------------------------------------------//
    //--- AcousticInstrument -------------------------------------------------//
    //------------------------------------------------------------------------//

    public AcousticInstrument getFirstAcousticInstrument() {
        return persistenceContext.getAcousticInstrumentDao().findAll().get(0);
    }

    //------------------------------------------------------------------------//
    //--- LengthAgeKey -------------------------------------------------------//
    //------------------------------------------------------------------------//

    public LengthAgeKey getLengthAgeKey(String id) {
        return persistenceContext.getLengthAgeKeyDao().forTopiaIdEquals(id).findUnique();
    }

    public LengthAgeKey createLengthAgeKey(LengthAgeKey lengthAgeKey) {
        return persistenceContext.getLengthAgeKeyDao().create(lengthAgeKey);
    }

    public void deleteLengthAgeKey(LengthAgeKey lengthAgeKey) {
        persistenceContext.getLengthAgeKeyDao().delete(lengthAgeKey);
    }

    //------------------------------------------------------------------------//
    //--- LengthWeightKey ----------------------------------------------------//
    //------------------------------------------------------------------------//

    public LengthWeightKey getLengthWeightKey(String id) {
        return persistenceContext.getLengthWeightKeyDao().forTopiaIdEquals(id).findUnique();
    }

    public LengthWeightKey createLengthWeightKey(LengthWeightKey lengthWeightKey) {
        return persistenceContext.getLengthWeightKeyDao().create(lengthWeightKey);
    }

    public void deleteLengthWeightKey(LengthWeightKey lengthAgeKey) {
        persistenceContext.getLengthWeightKeyDao().delete(lengthAgeKey);
    }

    //------------------------------------------------------------------------//
    //--- Mooring ------------------------------------------------------------//
    //------------------------------------------------------------------------//

    public Mooring getMooring(String id) {
        return persistenceContext.getMooringDao().forTopiaIdEquals(id).findUnique();
    }
    
    public boolean isMooringExistByCode(String code) {
        return persistenceContext.getMooringDao().forCodeEquals(code).exists();
    }

    public Mooring createMooring(Mooring mooring) {
        return persistenceContext.getMooringDao().create(mooring);
    }

    public void deleteMooring(Mooring mooring) {
        persistenceContext.getMooringDao().delete(mooring);
    }
    
    //------------------------------------------------------------------------//
    //--- Mission ------------------------------------------------------------//
    //------------------------------------------------------------------------//

    public Mission getMission(String id) {
        return persistenceContext.getMissionDao().forTopiaIdEquals(id).findUnique();
    }

    public boolean isMissionExistByName(String missionname) {
        return persistenceContext.getMissionDao().forNameEquals(missionname).exists();
    }

    public Mission createMission(Mission mission) {
        return persistenceContext.getMissionDao().create(mission);
    }

    //------------------------------------------------------------------------//
    //--- Operation ----------------------------------------------------------//
    //------------------------------------------------------------------------//

    public boolean containsOperation(Voyage voyage, Vessel vessel, DepthStratum depthStratum, String operationId) {
        return persistenceContext.getOperationDao()
                                 .forIdEquals(operationId)
                                 .addEquals(Operation.PROPERTY_DEPTH_STRATUM, depthStratum)
                                 .addEquals(Operation.PROPERTY_TRANSECT + "." + Transect.PROPERTY_VESSEL, vessel)
                                 .addEquals(Operation.PROPERTY_TRANSECT + "." + Transect.PROPERTY_TRANSIT + "." + Transit.PROPERTY_VOYAGE, voyage)
                                 .exists();
    }

    public Operation getOperation(String id) {
        return persistenceContext.getOperationDao().forTopiaIdEquals(id).findUnique();
    }

    public Operation getOperationContainsSample(Sample sample) {
        return persistenceContext.getOperationDao().forSampleContains(sample).findAnyOrNull();
    }

    public Operation createOperation(Operation operation) {
        return persistenceContext.getOperationDao().create(operation);
    }

    public void deleteOperation(Operation operation) {
        persistenceContext.getOperationDao().delete(operation);
    }

    //------------------------------------------------------------------------//
    //--- OperationMetadataValue ---------------------------------------------//
    //------------------------------------------------------------------------//

    public boolean containsOperationMetadataValue(Voyage voyage, Vessel vessel, DepthStratum depthStratum, String operationId, OperationMetadata operationMetadata) {
        return persistenceContext.getOperationMetadataValueDao()
                                 .forOperationMetadataEquals(operationMetadata)
                                 .addEquals(OperationMetadataValue.PROPERTY_OPERATION + "." + Operation.PROPERTY_ID, operationId)
                                 .addEquals(OperationMetadataValue.PROPERTY_OPERATION + "." + Operation.PROPERTY_DEPTH_STRATUM, depthStratum)
                                 .addEquals(OperationMetadataValue.PROPERTY_OPERATION + "." + Operation.PROPERTY_TRANSECT + "." + Transect.PROPERTY_VESSEL, vessel)
                                 .addEquals(OperationMetadataValue.PROPERTY_OPERATION + "." + Operation.PROPERTY_TRANSECT + "." + Transect.PROPERTY_TRANSIT + "." + Transit.PROPERTY_VOYAGE, voyage)
                                 .exists();
    }

    public OperationMetadataValue getOperationMetadataValue(String id) {
        return persistenceContext.getOperationMetadataValueDao().forTopiaIdEquals(id).findUnique();
    }

    public OperationMetadataValue createOperationMetadataValue(OperationMetadataValue operationMetadataValue) {
        return persistenceContext.getOperationMetadataValueDao().create(operationMetadataValue);
    }

    //------------------------------------------------------------------------//
    //--- Result -------------------------------------------------------------//
    //------------------------------------------------------------------------//

//    public List<Result> getAllWithCategoryCellAndDataMetadata(Set<String> cellIds,
//                                                              DataMetadata requiredDataMetadata,
//                                                              List<String> categoryIds) {
//        return persistenceContext.getResultDao().findAllWithCategoryCellAndDataMetadata(cellIds, requiredDataMetadata, categoryIds);
//    }
//
//    public List<Result> getAllWithNoCategoryCellAndDataMetadata(Set<String> cellIds,
//                                                                DataMetadata requiredDataMetadata) {
//        return persistenceContext.getResultDao().findAllWithNoCategoryCellAndDataMetadata(cellIds, requiredDataMetadata);
//    }

    public Result getResult(String id) {
        return persistenceContext.getResultDao().forTopiaIdEquals(id).findUnique();
    }

    public List<Result> getResultsForMissionAndDatametadata(Mission mission, DataMetadata dataMetadata) {
        return persistenceContext.getResultDao().findAllWithMissionAndDatametadata(mission, dataMetadata);
    }

    public Result createResult(Result result) {
        return persistenceContext.getResultDao().create(result);
    }

    public void deleteResult(Result result) {
        persistenceContext.getResultDao().delete(result);
    }

    public void deleteResults(Collection<Result> results) {
        persistenceContext.getResultDao().deleteAll(results);
    }

    //------------------------------------------------------------------------//
    //--- Sample -------------------------------------------------------------//
    //------------------------------------------------------------------------//

    public boolean containsSubSample(Operation operation, SpeciesCategory category) {
        return persistenceContext.getSampleDao()
                                 .forSpeciesCategoryEquals(category)
                                 .addEquals(Sample.PROPERTY_OPERATION, operation)
                                 .exists();
    }

    public Sample getSample(String id) {
        return persistenceContext.getSampleDao().forTopiaIdEquals(id).findUnique();
    }

    public Sample newSample() {
        return persistenceContext.getSampleDao().newInstance();
    }

    public Sample createSample(Sample sample) {
        return persistenceContext.getSampleDao().create(sample);
    }

    public void deleteSample(Sample sample) {
        persistenceContext.getSampleDao().delete(sample);
    }

    //------------------------------------------------------------------------//
    //--- SampleType ---------------------------------------------------------//
    //------------------------------------------------------------------------//

    public SampleType getSampleTypeByName(String name) {
        return persistenceContext.getSampleTypeDao().forNameEquals(name).findUnique();
    }

    //------------------------------------------------------------------------//
    //--- SampleData ---------------------------------------------------------//
    //------------------------------------------------------------------------//

    public SampleData getSampleData(String sampleDataId) {
        return persistenceContext.getSampleDataDao().forTopiaIdEquals(sampleDataId).findUnique();
    }

    public SampleData createSampleData(SampleDataType sampleDataType, String label, float value) {
        return persistenceContext.getSampleDataDao().create(
                SampleData.PROPERTY_SAMPLE_DATA_TYPE, sampleDataType,
                SampleData.PROPERTY_DATA_LABEL, label,
                SampleData.PROPERTY_DATA_VALUE, value);
    }

    public SampleData createSampleData(SampleData sample) {
        return persistenceContext.getSampleDataDao().create(sample);
    }

    //------------------------------------------------------------------------//
    //--- SampleDataType -----------------------------------------------------//
    //------------------------------------------------------------------------//

    public SampleDataType getSampleDataTypeByName(String name) {
        return persistenceContext.getSampleDataTypeDao().forNameEquals(name).findUnique();
    }

    public SampleDataType createSampleDataType(String sampleDataTypeName) {
        return persistenceContext.getSampleDataTypeDao().create(
                SampleDataType.PROPERTY_NAME, sampleDataTypeName);
    }


    //------------------------------------------------------------------------//
    //--- SizeCategory -------------------------------------------------------//
    //------------------------------------------------------------------------//

    public SizeCategory createSizeCategory(String sizeCategoryName,
                                           String sizeCategoryMeaning) {

        return persistenceContext.getSizeCategoryDao().create(
                SizeCategory.PROPERTY_NAME, sizeCategoryName,
                SizeCategory.PROPERTY_MEANING, sizeCategoryMeaning
        );
    }

    //------------------------------------------------------------------------//
    //--- Species ------------------------------------------------------------//
    //------------------------------------------------------------------------//

    public List<Species> getSpecies() {
        return persistenceContext.getSpeciesDao().findAll();
    }

//    public Optional<Species> getOptionalSpecies(String id) {
//        Species species = persistenceContext.getSpeciesDao().findByTopiaId(id);
//        return Optional.fromNullable(species);
//    }

    //------------------------------------------------------------------------//
    //--- SpeciesCategory ----------------------------------------------------//
    //------------------------------------------------------------------------//

    public SpeciesCategory getSpeciesCategory(Species species,
                                              Float lengthClass,
                                              SizeCategory sizeCategory,
                                              AgeCategory ageCategory,
                                              SexCategory sexCategory) {
        return persistenceContext.getSpeciesCategoryDao().forProperties(
                SpeciesCategory.PROPERTY_SPECIES, species,
                SpeciesCategory.PROPERTY_LENGTH_CLASS, lengthClass,
                SpeciesCategory.PROPERTY_SIZE_CATEGORY, sizeCategory,
                SpeciesCategory.PROPERTY_AGE_CATEGORY, ageCategory,
                SpeciesCategory.PROPERTY_SEX_CATEGORY, sexCategory
        ).findAnyOrNull();
    }

    public SpeciesCategory createSpeciesCategory(Species species,
                                                 Float lengthClass,
                                                 SizeCategory sizeCategory,
                                                 AgeCategory ageCategory,
                                                 SexCategory sexCategory) {
        return persistenceContext.getSpeciesCategoryDao().create(
                SpeciesCategory.PROPERTY_SPECIES, species,
                SpeciesCategory.PROPERTY_LENGTH_CLASS, lengthClass,
                SpeciesCategory.PROPERTY_SIZE_CATEGORY, sizeCategory,
                SpeciesCategory.PROPERTY_AGE_CATEGORY, ageCategory,
                SpeciesCategory.PROPERTY_SEX_CATEGORY, sexCategory
        );
    }

    public SizeCategory getSizeCategory(String name) {
        return persistenceContext.getSizeCategoryDao().forProperties(
                SizeCategory.PROPERTY_NAME, name
        ).findAnyOrNull();
    }

    //------------------------------------------------------------------------//
    //--- Transect -----------------------------------------------------------//
    //------------------------------------------------------------------------//

    public boolean containsTransect(Voyage voyage, String title) {
        return persistenceContext.getTransectDao()
                                 .forTitleEquals(title)
                                 .addEquals(Transect.PROPERTY_TRANSIT + "." + Transit.PROPERTY_VOYAGE, voyage)
                                 .exists();
    }

    public Transect getTransect(String id) {
        return persistenceContext.getTransectDao().forTopiaIdEquals(id).findUnique();
    }

    public Transect getTransectContainsOperation(Operation operation) {
        return persistenceContext.getTransectDao().forOperationContains(operation).findAnyOrNull();
    }

    public Transect getTransectContainsDataAcquisition(DataAcquisition dataAcquisition) {
        return persistenceContext.getTransectDao().forDataAcquisitionContains(dataAcquisition).findAnyOrNull();
    }

    public Transect createTransect(Transect transect) {
        return persistenceContext.getTransectDao().create(transect);
    }

    public void deleteTransect(Transect transect) {
        persistenceContext.getTransectDao().delete(transect);
    }

    public List<Transect> getTransects(Voyage voyage, Vessel vessel) {
        return persistenceContext.getTransectDao()
                .forVesselEquals(vessel)
                .addEquals(Transect.PROPERTY_TRANSIT + "." + Transit.PROPERTY_VOYAGE, voyage)
                .findAll();
    }
    
    public Transect getTransect(Vessel vessel, Transit transit) {
        return persistenceContext.getTransectDao()
                .forVesselEquals(vessel)
                .addEquals(Transect.PROPERTY_TRANSIT, transit)
                .setOrderByArguments(Transect.PROPERTY_TRANSIT, Transect.PROPERTY_VESSEL)
                .findFirstOrNull();
    }

    //------------------------------------------------------------------------//
    //--- Transit ------------------------------------------------------------//
    //------------------------------------------------------------------------//

    public Transit getTransit(String id) {
        return persistenceContext.getTransitDao().forTopiaIdEquals(id).findUnique();
    }

    public boolean containsTransit(Voyage voyage, Date startTime, Date endTime) {

        return persistenceContext.getTransitDao()
                                 .forVoyageEquals(voyage)
                                 .addEquals(Transit.PROPERTY_START_TIME, startTime)
                                 .addEquals(Transit.PROPERTY_END_TIME, endTime)
                                 .exists();

    }

    public Transit getTransitContainsTransect(Transect transect) {
        return persistenceContext.getTransitDao().forTransectContains(transect).findAnyOrNull();
    }

    public Transit createTransit(Transit transit) {
        return persistenceContext.getTransitDao().create(transit);
    }

    public void deleteTransit(Transit transit) {
        persistenceContext.getTransitDao().delete(transit);
    }

    //------------------------------------------------------------------------//
    //--- Vocabulary ---------------------------------------------------------//
    //------------------------------------------------------------------------//

    public Map<String, String> getVocabulary() {
        VocabularyCIEMTopiaDao vocabularyDao = persistenceContext.getVocabularyCIEMDao();
        List<VocabularyCIEM> all = vocabularyDao.findAll();
        
        Map<String, String> result = new HashMap<String, String>();
        for (VocabularyCIEM voca : all) {
            String key = voca.getLabel();
            String value = voca.getCode();
            result.put(key, value);
        }
        
        return result;
    }

    //------------------------------------------------------------------------//
    //--- Vessel -------------------------------------------------------------//
    //------------------------------------------------------------------------//

    public Vessel getVessel(String id) {
        return persistenceContext.getVesselDao().forTopiaIdEquals(id).findUnique();
    }

    //------------------------------------------------------------------------//
    //--- Voyage -------------------------------------------------------------//
    //------------------------------------------------------------------------//

    public Voyage getVoyage(String id) {
        return persistenceContext.getVoyageDao().forTopiaIdEquals(id).findUnique();
    }

    public List<Voyage> getVoyagesForMission(Mission mission) {
        return persistenceContext.getVoyageDao().forMissionEquals(mission).findAll();
    }

//    public List<Voyage> getAllVoyages() {
//        return persistenceContext.getVoyageDao().findAll();
//    }

    public long countVoyage() {
        return persistenceContext.getVoyageDao().count();
    }

    public Voyage createVoyage(Voyage voyage) {
        return persistenceContext.getVoyageDao().create(voyage);
    }

    public void deleteVoyage(Voyage voyage) {
        persistenceContext.getVoyageDao().delete(voyage);
    }

    public boolean containsVoyageByName(String voyageName) {
        return persistenceContext.getVoyageDao()
                                 .forNameEquals(voyageName)
                                 .exists();
    }

    public boolean containsMooringByCode(String mooringCode) {
        return persistenceContext.getMooringDao()
                                 .forCodeEquals(mooringCode)
                                 .exists();
    }

    //------------------------------------------------------------------------//
    //--- Transversal --------------------------------------------------------//
    //------------------------------------------------------------------------//

    public <E extends TopiaEntity> Map<String, String> loadSortAndDecorate(Class<E> beanType) {

        List<E> beans = persistenceContext.getDao(beanType).findAll();
        return decoratorService.sortAndDecorate(beans, null);
    }

    public <E extends TopiaEntity, K> Map<K, E> getEntitiesMap(Class<E> entityType, Function<E, K> function) {
        List<E> entities = persistenceContext.getDao(entityType).findAll();
        return Maps.uniqueIndex(entities, function);
    }

    public void executeSQL(String sqlScript) {
        persistenceContext.getSqlSupport().executeSql(sqlScript);
    }

    public void clear() {
        persistenceContext.getHibernateSupport().getHibernateSession().clear();
    }

    public void flush() {
        persistenceContext.getHibernateSupport().getHibernateSession().flush();
    }

    public void commit() {
        persistenceContext.commit();
    }

    public <R> R findSingleResult(TopiaSqlQuery<R> sqlQuery) {
        return persistenceContext.getSqlSupport().findSingleResult(sqlQuery);
    }

    public <R> List<R> findMultipleResult(TopiaSqlQuery<R> sqlQuery) {
        return persistenceContext.getSqlSupport().findMultipleResult(sqlQuery);
    }

    public boolean isIdExists(String id) {
        try {
            return getDAOFromId(id).forTopiaIdEquals(id).exists();
        } catch (TopiaException e) {
            throw new EchoBaseTechnicalException(
                    "Could not find out if id " + id + " exists.", e);
        }
    }

    protected <E extends TopiaEntity> TopiaDao<E> getDAOFromId(String id) {
        try {
            TopiaIdFactory topiaIdFactory = persistenceContext.getTopiaIdFactory();
            Class<E> className = topiaIdFactory.getClassName(id);
            return persistenceContext.getDao(className);
        } catch (TopiaNotFoundException e) {

            throw new EchoBaseTechnicalException(
                    "Could not find class from id: " + id, e);
        }
    }

    public long countTable(String tableName) {

        CountTableRows query = new CountTableRows(tableName);
        return findSingleResult(query);

    }

    public Species getSpecies(String speciesId) {
        return persistenceContext.getSpeciesDao().forTopiaIdEquals(speciesId).findUnique();
    }

    public void creteImportFileIds(Collection<ImportFileId> importedFileIdsToPersist) {

        ImportFileIdTopiaDao importFileIdDao = persistenceContext.getImportFileIdDao();
        for (ImportFileId importFileId : importedFileIdsToPersist) {
            importFileIdDao.create(importFileId);
        }

    }

    public Iterable<ImportFileId> getImportFileIdsForImportFile(ImportFile importFile) {

        ImportFileIdTopiaDao importFileIdDao = persistenceContext.getImportFileIdDao();
        return importFileIdDao.getImportFileIdsForImportFile(importFile);

    }

    public <E extends TopiaEntity> Iterable<ImportFileId> getImportFileIdsForImportFileAndType(ImportFile importFile, Class<E> entityType) {

        ImportFileIdTopiaDao importFileIdDao = persistenceContext.getImportFileIdDao();
        return importFileIdDao.getImportFileIdsForImportFileAndType(importFile, entityType);

    }

    public Iterable<ImportedCell> getImportedAcousticCells(ImportFile importFile) {
        ImportFileIdTopiaDao importFileIdDao = persistenceContext.getImportFileIdDao();
        return importFileIdDao.getImportedAcousticCells(importFile);
    }

    public Iterable<ImportedCellResult> getImportedCellResults(ImportFile importFile) {
        ImportFileIdTopiaDao importFileIdDao = persistenceContext.getImportFileIdDao();
        return importFileIdDao.getImportedCellResults(importFile);
    }

    public Iterable<ImportedSampleDataResult> getImportedSampleDataResults(ImportFile importFile) {
        ImportFileIdTopiaDao importFileIdDao = persistenceContext.getImportFileIdDao();
        return importFileIdDao.getImportedSampleDataResults(importFile);
    }

    public <E extends TopiaEntity> Iterable<E> getImportedEntities(ImportFile importFile, Class<E> entityType) {

        ImportFileIdTopiaDao importFileIdDao = persistenceContext.getImportFileIdDao();
        return importFileIdDao.getImportedEntities(importFile, entityType);

    }

    public long getImportFileIdsCount(ImportFile importFile) {

        ImportFileIdTopiaDao importFileIdDao = persistenceContext.getImportFileIdDao();
        return importFileIdDao.getImportFileIdsCountForImportFile(importFile);

    }

    public ImportFileId createImportFileId(ImportFile importFile, TopiaEntity importId, int lineNumber, int importOrder) {

        return persistenceContext.getImportFileIdDao().create(
                ImportFileId.PROPERTY_IMPORT_FILE, importFile,
                ImportFileId.PROPERTY_ENTITY_ID, importId.getTopiaId(),
                ImportFileId.PROPERTY_LINE_NUMBER, lineNumber,
                ImportFileId.PROPERTY_IMPORT_ORDER, importOrder);

    }

    public ImportFileId newImportFileId(ImportFile importFile, TopiaEntity importId, int lineNumber, int importOrder) {

        ImportFileId importFileId = persistenceContext.getImportFileIdDao().newInstance();
        importFileId.setImportFile(importFile);
        importFileId.setEntityId(importId.getTopiaId());
        importFileId.setLineNumber(lineNumber);
        importFileId.setImportOrder(importOrder);
        return importFileId;

    }

    private static class CountTableRows extends TopiaSqlQuery<Long> {

        public final String tableName;

        private CountTableRows(String tableName) {
            this.tableName = tableName;
        }

        @Override
        public PreparedStatement prepareQuery(Connection connection) throws SQLException {
            String hql = "SELECT count(*) FROM " + tableName;
            return connection.prepareStatement(hql);
        }

        @Override
        public Long prepareResult(ResultSet set) throws SQLException {
            return set.getLong(1);
        }
    }
}
