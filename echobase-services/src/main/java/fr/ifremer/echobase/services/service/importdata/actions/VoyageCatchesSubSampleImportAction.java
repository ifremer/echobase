package fr.ifremer.echobase.services.service.importdata.actions;

/*
 * #%L
 * EchoBase :: Services
 * %%
 * Copyright (C) 2011 - 2016 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.base.Preconditions;
import fr.ifremer.echobase.entities.ImportedSampleDataResult;
import fr.ifremer.echobase.entities.data.Operation;
import fr.ifremer.echobase.entities.data.Sample;
import fr.ifremer.echobase.entities.data.SampleData;
import fr.ifremer.echobase.entities.references.SampleDataType;
import fr.ifremer.echobase.entities.references.SampleType;
import fr.ifremer.echobase.entities.references.SexCategory;
import fr.ifremer.echobase.entities.references.SizeCategory;
import fr.ifremer.echobase.entities.references.Species;
import fr.ifremer.echobase.entities.references.SpeciesCategory;
import fr.ifremer.echobase.io.InputFile;
import fr.ifremer.echobase.services.service.importdata.DuplicatedSubSampleException;
import fr.ifremer.echobase.services.service.importdata.ImportDataFileResult;
import fr.ifremer.echobase.services.service.importdata.SpeciesCategoryCache;
import fr.ifremer.echobase.services.service.importdata.contexts.VoyageCatchesImportDataContext;
import fr.ifremer.echobase.services.service.importdata.csv.VoyageCatchesSubSampleImportExportModel;
import fr.ifremer.echobase.services.service.importdata.csv.VoyageCatchesSubSampleImportRow;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.csv.Import;

import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

/**
 * Created on 25/03/16.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 4.0
 */
public class VoyageCatchesSubSampleImportAction extends VoyageCatchesImportDataActionSupport<VoyageCatchesSubSampleImportRow> {

    /** Logger. */
    private static final Log log = LogFactory.getLog(VoyageCatchesSubSampleImportAction.class);

    public VoyageCatchesSubSampleImportAction(VoyageCatchesImportDataContext importDataContext) {
        super(importDataContext, importDataContext.getConfiguration().getSubSampleFile());
    }

    @Override
    protected VoyageCatchesSubSampleImportExportModel createCsvImportModel(VoyageCatchesImportDataContext importDataContext) {
        return VoyageCatchesSubSampleImportExportModel.forImport(importDataContext);
    }

    @Override
    protected VoyageCatchesSubSampleImportExportModel createCsvExportModel(VoyageCatchesImportDataContext importDataContext) {
        return VoyageCatchesSubSampleImportExportModel.forExport(importDataContext);
    }

    @Override
    protected void performImport(VoyageCatchesImportDataContext importDataContext, InputFile inputFile, ImportDataFileResult result) {

        if (log.isInfoEnabled()) {
            log.info("Starts import of subSample from file " + inputFile.getFileName());
        }

        SpeciesCategoryCache speciesCategoryCache = importDataContext.getSpeciesCategoryCache();
        Collection<Operation> operationWithTotalOrUnsortedSample = importDataContext.getVoyageOperationsWithTotalOrUnsortedSample();

        SampleType sampleTypeSubsample = importDataContext.getSampleTypeSubsample();
        SampleDataType sampleDataTypeNumberAtLength = importDataContext.getSampleDataTypeNumberAtLength();
        SampleDataType sampleDataTypeWeightAtLength = importDataContext.getSampleDataTypeWeightAtLength();

        try (Import<VoyageCatchesSubSampleImportRow> importer = open()) {

            incrementsProgress();
            int rowNumber = 0;
            for (VoyageCatchesSubSampleImportRow row : importer) {

                doFlushTransaction(++rowNumber);
                Operation operation = row.getOperation();

                checkOperationWithTotalOrUnsortedSample(rowNumber, operationWithTotalOrUnsortedSample, operation);

                Species species = row.getSpecies();
                SizeCategory sizeCategory = row.getSizeCategory();
                SexCategory sexCategory = row.getSexCategory();
                Float lengthClass = Float.parseFloat(row.getLengthClass());

                SpeciesCategory category = speciesCategoryCache.getSpeciesCategory(species, lengthClass, sizeCategory, null, sexCategory, result);

                // find the sample with this category
                Sample sample = operation.getSample(category, sampleTypeSubsample);

                boolean exists = sample != null && persistenceService.containsSubSample(operation, category);
                if (exists) {
                    throw new DuplicatedSubSampleException(getLocale(),
                                                           rowNumber,
                                                           operation.getId(),
                                                           category.toString(),
                                                           row.getLengthClass());
                }

                if (sample == null || (sample.getSampleWeight() != null && sample.getSampleWeight().equals(row.getSampleWeight()))) {

                    // must create it
                    sample = row.getSample();

                    sample.setSpeciesCategory(category);
                    sample.setSampleType(sampleTypeSubsample);

                    sample = addSample(operation, sample, result, rowNumber);

                }

                addProcessedRow(result, row);

                SampleDataType dataType;

                dataType = sampleDataTypeNumberAtLength;

                {

                    //create numberAtLength data
                    addSampleData(dataType, "" + row.getLengthClass(), row.getNumberAtLength(), sample, result, true, rowNumber);

                }
                if (row.getWeightAtLength() != null) {

                    //create weightAtLength data
                    addSampleData(sampleDataTypeWeightAtLength, "" + row.getLengthClass(), row.getWeightAtLength(), sample, result, true, rowNumber);

                }

            }

        }

    }

    @Override
    protected void computeImportedExport(VoyageCatchesImportDataContext importDataContext, ImportDataFileResult result) {

        List<SampleData> sampleDatas = new LinkedList<>();

        Sample sample = null;

        for (ImportedSampleDataResult sampleDataRow : persistenceService.getImportedSampleDataResults(result.getImportFile())) {

            SampleData sampleData = sampleDataRow.getSampleData();

            if (log.isInfoEnabled()) {
                log.info("Test imported id: " + sampleData.getTopiaId());
            }

            Sample currentSample = sampleDataRow.getSample();

            if (sample == null) {

                // first row
                sample = currentSample;

            } else {

                if (!sample.equals(currentSample)) {

                    // flush sample
                    flushSample(importDataContext, sample, result, sampleDatas);

                    sample = currentSample;
                    sampleDatas.clear();

                }

            }

            sampleDatas.add(sampleData);


        }

        if (!sampleDatas.isEmpty()) {

            // flush last sample
            flushSample(importDataContext, sample, result, sampleDatas);

        }

    }

    protected void flushSample(VoyageCatchesImportDataContext importDataContext, Sample sample, ImportDataFileResult result, List<SampleData> sampleDataIds) {

        Preconditions.checkNotNull(sample);

        if (log.isInfoEnabled()) {
            log.info("Adding sample: " + sample + " to imported export.");
        }

        Set<SampleDataType> lengthSampleDataTypes = new LinkedHashSet<>();
        lengthSampleDataTypes.add(importDataContext.getSampleDataTypeNumberAtLength());

        Preconditions.checkState(sample.isSampleDataNotEmpty());

        VoyageCatchesSubSampleImportRow importedRow = null;

        Operation operation = sample.getOperation();

        String lastLengthClass = null;
        boolean lengthDataTypeFound = false;
        for (SampleData aSampleData : sampleDataIds) {

            String lengthClass = aSampleData.getDataLabel();

            boolean isLengthDataType = lengthSampleDataTypes.contains(aSampleData.getSampleDataType());

            if (lastLengthClass == null) {

                // first row
                lastLengthClass = lengthClass;
                importedRow = VoyageCatchesSubSampleImportRow.of(operation, sample);
                lengthDataTypeFound = false;

            } else {

                if ((isLengthDataType && lengthDataTypeFound) || !lengthClass.equals(lastLengthClass)) {

                    // changing length class or length data type already found
                    addImportedRow(result, importedRow);

                    lastLengthClass = lengthClass;
                    importedRow = VoyageCatchesSubSampleImportRow.of(operation, sample);
                    lengthDataTypeFound = false;
                }
            }

            if (log.isInfoEnabled()) {
                log.info("Treated sample data: " + aSampleData);
            }

            addImportedSampleData(importDataContext, importedRow, aSampleData);

            if (isLengthDataType) {
                lengthDataTypeFound = true;
            }

        }

        addImportedRow(result, importedRow);

    }

    protected void addImportedSampleData(VoyageCatchesImportDataContext importDataContext, VoyageCatchesSubSampleImportRow importedRow, SampleData sampleData) {

        SampleDataType sampleDataTypeNumberAtLength = importDataContext.getSampleDataTypeNumberAtLength();
        SampleDataType sampleDataTypeWeightAtLength = importDataContext.getSampleDataTypeWeightAtLength();

        SampleDataType sampleDataType = sampleData.getSampleDataType();

        if (sampleDataTypeNumberAtLength.equals(sampleDataType)) {

            importedRow.setNumberAtLength(sampleData.getDataValue());

        } else if (sampleDataTypeWeightAtLength.equals(sampleDataType)) {

            importedRow.setWeightAtLength(sampleData.getDataValue());

        } else {
            throw new IllegalStateException("Can't deal with this sampleData: " + sampleData);
        }

        importedRow.setLengthClass(sampleData.getDataLabel());

    }

}
