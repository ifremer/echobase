/*
 * #%L
 * EchoBase :: Services
 * %%
 * Copyright (C) 2011 - 2012 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ifremer.echobase.services.service.importdata.csv;

import fr.ifremer.echobase.entities.data.Transect;
import fr.ifremer.echobase.entities.data.Voyage;
import fr.ifremer.echobase.entities.references.Vessel;
import fr.ifremer.echobase.services.csv.EchoBaseCsvUtil;
import fr.ifremer.echobase.services.service.importdata.contexts.VoyageCommonsImportDataContext;

/**
 * Model to import transects.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 0.3
 */
public class VoyageCommonsTransectImportExportModel extends EchoBaseImportExportModelSupport<VoyageCommonsTransectImportRow> {

    private VoyageCommonsTransectImportExportModel(char separator) {
        super(separator);
    }

    public static VoyageCommonsTransectImportExportModel forImport(VoyageCommonsImportDataContext importDataContext) {

        VoyageCommonsTransectImportExportModel model = new VoyageCommonsTransectImportExportModel(importDataContext.getCsvSeparator());
        model.newForeignKeyColumn(VoyageCommonsTransectImportRow.PROPERTY_VOYAGE, Voyage.class, Voyage.PROPERTY_NAME, importDataContext.getVoyagesByName());
        model.newForeignKeyColumn(EchoBaseCsvUtil.VESSEL_NAME, Transect.PROPERTY_VESSEL, Vessel.class, Vessel.PROPERTY_NAME, importDataContext.getVesselsByName());
        model.newMandatoryColumn(Transect.PROPERTY_TITLE);
        model.newMandatoryColumn(Transect.PROPERTY_TRANSECT_ABSTRACT);
        model.newMandatoryColumn(Transect.PROPERTY_STRATUM);
        model.newMandatoryColumn(Transect.PROPERTY_COMMENTS);
        model.newMandatoryColumn(Transect.PROPERTY_DATE_CREATED, EchoBaseCsvUtil.IMPORT_DAY_TIME_ECHOBASE);
        model.newMandatoryColumn(Transect.PROPERTY_TIME_COVERAGE_START, EchoBaseCsvUtil.IMPORT_DAY_TIME_ECHOBASE);
        model.newMandatoryColumn(Transect.PROPERTY_TIME_COVERAGE_END, EchoBaseCsvUtil.IMPORT_DAY_TIME_ECHOBASE);
        model.newMandatoryColumn(Transect.PROPERTY_GEOSPATIAL_LON_MIN, EchoBaseCsvUtil.PRIMITIVE_FLOAT);
        model.newMandatoryColumn(Transect.PROPERTY_GEOSPATIAL_LAT_MIN, EchoBaseCsvUtil.PRIMITIVE_FLOAT);
        model.newMandatoryColumn(Transect.PROPERTY_GEOSPATIAL_VERTICAL_MIN, EchoBaseCsvUtil.PRIMITIVE_FLOAT);
        model.newMandatoryColumn(Transect.PROPERTY_GEOSPATIAL_LON_MAX, EchoBaseCsvUtil.PRIMITIVE_FLOAT);
        model.newMandatoryColumn(Transect.PROPERTY_GEOSPATIAL_LAT_MAX, EchoBaseCsvUtil.PRIMITIVE_FLOAT);
        model.newMandatoryColumn(Transect.PROPERTY_GEOSPATIAL_VERTICAL_MAX, EchoBaseCsvUtil.PRIMITIVE_FLOAT);
        model.newMandatoryColumn(Transect.PROPERTY_LINESTRING);
        return model;

    }

    public static VoyageCommonsTransectImportExportModel forExport(VoyageCommonsImportDataContext importDataContext) {

        VoyageCommonsTransectImportExportModel model = new VoyageCommonsTransectImportExportModel(importDataContext.getCsvSeparator());
        model.newColumnForExport(VoyageCommonsTransectImportRow.PROPERTY_VOYAGE, EchoBaseCsvUtil.VOYAGE_FORMATTER);
        model.newColumnForExport(EchoBaseCsvUtil.VESSEL_NAME);
        model.newColumnForExport(Transect.PROPERTY_TITLE);
        model.newColumnForExport(Transect.PROPERTY_TRANSECT_ABSTRACT);
        model.newColumnForExport(Transect.PROPERTY_STRATUM);
        model.newColumnForExport(Transect.PROPERTY_COMMENTS);
        model.newColumnForExport(Transect.PROPERTY_DATE_CREATED, EchoBaseCsvUtil.IMPORT_DAY_TIME_ECHOBASE);
        model.newColumnForExport(Transect.PROPERTY_TIME_COVERAGE_START, EchoBaseCsvUtil.IMPORT_DAY_TIME_ECHOBASE);
        model.newColumnForExport(Transect.PROPERTY_TIME_COVERAGE_END, EchoBaseCsvUtil.IMPORT_DAY_TIME_ECHOBASE);
        model.newColumnForExport(Transect.PROPERTY_GEOSPATIAL_LON_MIN, EchoBaseCsvUtil.PRIMITIVE_FLOAT);
        model.newColumnForExport(Transect.PROPERTY_GEOSPATIAL_LAT_MIN, EchoBaseCsvUtil.PRIMITIVE_FLOAT);
        model.newColumnForExport(Transect.PROPERTY_GEOSPATIAL_VERTICAL_MIN, EchoBaseCsvUtil.PRIMITIVE_FLOAT);
        model.newColumnForExport(Transect.PROPERTY_GEOSPATIAL_LON_MAX, EchoBaseCsvUtil.PRIMITIVE_FLOAT);
        model.newColumnForExport(Transect.PROPERTY_GEOSPATIAL_LAT_MAX, EchoBaseCsvUtil.PRIMITIVE_FLOAT);
        model.newColumnForExport(Transect.PROPERTY_GEOSPATIAL_VERTICAL_MAX, EchoBaseCsvUtil.PRIMITIVE_FLOAT);
        model.newColumnForExport(Transect.PROPERTY_LINESTRING);
        return model;

    }

    @Override
    public VoyageCommonsTransectImportRow newEmptyInstance() {
        return new VoyageCommonsTransectImportRow();
    }

}
