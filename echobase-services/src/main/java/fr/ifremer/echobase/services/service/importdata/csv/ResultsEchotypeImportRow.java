/*
 * #%L
 * EchoBase :: Services
 * %%
 * Copyright (C) 2011 - 2012 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ifremer.echobase.services.service.importdata.csv;

import fr.ifremer.echobase.entities.data.Echotype;
import fr.ifremer.echobase.entities.data.EchotypeImpl;
import fr.ifremer.echobase.entities.references.DepthStratum;
import fr.ifremer.echobase.entities.references.Species;
import fr.ifremer.echobase.services.csv.ProviderAble;

import java.io.Serializable;
import org.nuiton.topia.persistence.TopiaEntity;

/**
 * @author Julien Ruchaud - ruchaud@codelutin.com
 * @since 4.0
 */
public abstract class ResultsEchotypeImportRow<E extends TopiaEntity> implements Serializable, ProviderAble<E> {

    private static final long serialVersionUID = 1L;

    public static final String PROPERTY_MEANING = Echotype.PROPERTY_MEANING;
    public static final String PROPERTY_ID = "sizeCategory";
    public static final String HEADER_ECHOTYPE_NAME = "echotypeName";
    protected static final String HEADER_SPECIES = "baracoudaCode";
    
    protected final Echotype echotype;
    protected Species species;
    
    public ResultsEchotypeImportRow() {
        this(new EchotypeImpl());
    }

    public ResultsEchotypeImportRow(Echotype echotype) {
        this.echotype = echotype;
    }

    public Echotype getEchotype() {
        return echotype;
    }

    public String getName() {
        return echotype.getName();
    }

    public void setName(String name) {
        echotype.setName(name);
    }

    public String getMeaning() {
        return echotype.getMeaning();
    }

    public void setMeaning(String meaning) {
        echotype.setMeaning(meaning);
    }

    public Species getSpecies() {
        return species;
    }

    public void setSpecies(Species species) {
        this.species = species;
    }

    public DepthStratum getDepthStratum() {
        return echotype.getDepthStratum();
    }

    public void setDepthStratum(DepthStratum depthStratum) {
        echotype.setDepthStratum(depthStratum);
    }

    public String getId() {
        return echotype.getId();
    }

    public void setId(String id) {
        echotype.setId(id);
    }
}
