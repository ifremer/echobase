package fr.ifremer.echobase.services.service.atlantos.xml;

import com.google.common.base.Function;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;
import fr.ifremer.echobase.entities.data.Operation;
import fr.ifremer.echobase.entities.data.OperationMetadataValue;
import fr.ifremer.echobase.entities.data.Sample;
import fr.ifremer.echobase.entities.data.SampleData;
import fr.ifremer.echobase.entities.data.Voyage;
import fr.ifremer.echobase.entities.references.SampleDataTypeImpl;
import fr.ifremer.echobase.entities.references.Species;
import fr.ifremer.echobase.entities.references.SpeciesCategory;
import fr.ifremer.echobase.entities.references.Vessel;
import fr.ifremer.echobase.services.EchoBaseService;
import fr.ifremer.echobase.services.csv.EchoBaseCsvUtil;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.inject.Inject;

/*
 * #%L
 * EchoBase :: Services
 * %%
 * Copyright (C) 2011 - 2014 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

/**
 * @author Julien Ruchaud - ruchaud@codelutin.com
 * @since 4
 */
public class XmlBioticExport implements EchoBaseService {
    
    @Inject
    protected VocabularyExport vocabulary;
    
    public void doExport(Voyage voyage, Vessel vessel, XmlWriter xmlVoca, XmlWriter xmlCruise) throws IOException {
        vocabulary.init(xmlVoca);
        
        // EXPORT BIOTIC
        xmlVoca.append("<?xml version=\"1.0\"?>\n");
        xmlVoca.open("Biotic", 
                 "xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance",
                 "xmlns:xsd", "http://www.w3.org/2001/XMLSchema");
        
        exportCruise(voyage, vessel, xmlCruise);

        int index = 1;
        Collection<Operation> operations = voyage.getOperations(vessel);
        for (Operation operation : operations) {
            Collection<Sample> samples = operation.getSample();

            boolean exportHaul=false;

            //Export Haul only if it contains samples that should be exported and gear should be exported
            for (Sample sample : samples) {
                SpeciesCategory category = sample.getSpeciesCategory();
                String name = sample.getSampleType().getName();
                if (    !name.equals("Sorted")
                        && category.getSpecies().getIcesExport() != null
                        && category.getSpecies().getIcesExport()
                        && operation.getGear().isIcesExport()) {

                    // EXPORT HAUL
                    exportHaul = true;
                    exportHaul(operation, index++, xmlCruise);
                    break;
                }
            }

            //Map with all the totals with no subsamples - key : code#cat
            Map<String,Sample> totals = new HashMap<>();

            //Map with all the subsamples - key : code#cat
            Map<String,List<Sample>> subsamples = new HashMap<>();

            //Map with all the individuals - key : code#cat#lengthClass
            Map<String,List<Sample>> individuals = new HashMap<>();

            //Map with all the speciesCategoryWeight - key : code#cat
            Map<String,Float> speciesCategoryWeights = new HashMap<>();

            //Map with all the speciesCategoryNumber - key : code#cat
            Map<String,Integer> speciesCategoryNumbers = new HashMap<>();

            //Map with all the subsampledNumber - key : code#cat
            Map<String,Integer> subsampledNumbers = new HashMap<>();

            //Map with all the subsampledWeight - key : code#cat
            Map<String,Float> subsampledWeights = new HashMap<>();


            for (Sample sample : samples) {

                String name = sample.getSampleType().getName();

                SpeciesCategory category = sample.getSpeciesCategory();

                if (!name.equals("Sorted") && category.getSpecies().getIcesExport() != null && category.getSpecies().getIcesExport()
                        && operation.getGear().isIcesExport()) {

                    //Compute code#cat key
                    Species species = category.getSpecies();
                    String code = species.getBaracoudaCode();
                    String categoryName = "";
                    if (category.getSizeCategory() != null) {
                        categoryName = category.getSizeCategory().getName();
                    }
                    String codeCatKey = code + "#" + categoryName;

                    switch (name) {
                        case "Total": {
                            //Add sample to totals list if no subsamples (yet)
                            if (subsamples.get(codeCatKey) == null) {
                                totals.put(codeCatKey, sample);
                            }

                            //Add sample weight to speciesCategoryWeight
                            Float weight = speciesCategoryWeights.get(codeCatKey);
                            if (weight == null) {
                                weight = 0f;
                            }

                            Float sampleWeight = sample.getSampleWeight();
                            if (sampleWeight != null) {
                                weight += sampleWeight;
                            }
                            speciesCategoryWeights.put(codeCatKey, weight);

                            //Add sample number to speciesCategoryNumber
                            Integer number = speciesCategoryNumbers.get(codeCatKey);
                            if (number == null) {
                                number = 0;
                            }

                            Integer numberSampled = sample.getNumberSampled();
                            if (numberSampled != null) {
                                number += numberSampled;
                            }
                            speciesCategoryNumbers.put(codeCatKey, number);

                            break;
                        }
                        case "Subsample": {

                            //Get back sample datas
                            Map<String, String> sampleDataValues = getSampleDataValues(sample);

                            // get back weight at length for this subsample
                            String weightAtLength = sampleDataValues.get(SampleDataTypeImpl.WEIGHT_AT_LENGTHKG);
                            Float sampleWeight = null;
                            if (weightAtLength != null) {
                                sampleWeight = Float.parseFloat(weightAtLength);
                            }

                            //add weightAtLength to the sum for this category
                            Float weightBySize = subsampledWeights.get(codeCatKey);
                            if (weightBySize == null) {
                                weightBySize = 0f;
                            }
                            if (sampleWeight != null) {
                                weightBySize += sampleWeight;
                            }
                            subsampledWeights.put(codeCatKey, weightBySize);

                            //get back number at length for this subsample
                            String numberAtLength = sampleDataValues.get(SampleDataTypeImpl.NUMBER_AT_LENGTH);
                            Float sampleNumber = null;
                            if (numberAtLength != null) {
                                sampleNumber = Float.parseFloat(numberAtLength);
                            }

                            //add numberAtLength to the sum for this category
                            Integer sumNumberAtLength = subsampledNumbers.get(codeCatKey);
                            if (sumNumberAtLength == null) {
                                sumNumberAtLength = 0;
                            }
                            if (sampleNumber != null) {
                                sumNumberAtLength += sampleNumber.intValue();
                            }
                            subsampledNumbers.put(codeCatKey, sumNumberAtLength);

                            //Add subsample to the list
                            List<Sample> speciesCategorySubsamples = subsamples.get(codeCatKey);
                            if (speciesCategorySubsamples == null) {
                                speciesCategorySubsamples = new ArrayList<>();
                            }
                            speciesCategorySubsamples.add(sample);
                            subsamples.put(codeCatKey, speciesCategorySubsamples);

                            //check if total present, if so, removes it
                            if (subsamples.get(codeCatKey) != null) {
                                totals.remove(codeCatKey);
                            }

                            break;
                        }
                        case "Individual": {

                            //Get back sample datas
                            Map<String, String> sampleDataValues = getSampleDataValues(sample);

                            //Get back lengthclass for individual, remove trailing .0
                            String lengthClass = sampleDataValues.get("LTmm1");
                            lengthClass = lengthClass.substring(0, lengthClass.indexOf("."));

                            String codeCatLCkey = codeCatKey + "#" + lengthClass;

                            //add individual to the list
                            List<Sample> lengthClassIndividuals = individuals.get(codeCatLCkey);
                            if (lengthClassIndividuals == null) {
                                lengthClassIndividuals = new ArrayList<>();
                                individuals.put(codeCatLCkey, lengthClassIndividuals);
                            }
                            lengthClassIndividuals.add(sample);

                            break;
                        }
                    }
                }
            }

            //Export totals
            for (Sample exportSample:totals.values()) {

                //Compute code#cat key
                SpeciesCategory category = exportSample.getSpeciesCategory();
                Species species = category.getSpecies();
                String code = species.getBaracoudaCode();
                String categoryName = "";
                if (category.getSizeCategory() != null) {
                    categoryName = category.getSizeCategory().getName();
                }
                String codeCatKey = code + "#" + categoryName;

                //get speciesCategoryWeight and speciesCategoryNumber
                Float speciesCategoryWeight = speciesCategoryWeights.get(codeCatKey);
                Integer speciesCategoryNumber = speciesCategoryNumbers.get(codeCatKey);

                //Export Catch - Total sample so no subsampledWeight or subsampledNumber
                exportCatch(exportSample,null,speciesCategoryWeight,null,speciesCategoryNumber,xmlCruise);

                xmlCruise.close("Catch");
            }

            //Export subsamples
            for (List<Sample> exportSubsamples:subsamples.values()){
                for (Sample exportSample : exportSubsamples){
                    //Compute code#cat key
                    SpeciesCategory category = exportSample.getSpeciesCategory();
                    Species species = category.getSpecies();
                    String code = species.getBaracoudaCode();
                    String categoryName = "";
                    if (category.getSizeCategory() != null) {
                        categoryName = category.getSizeCategory().getName();
                    }
                    String codeCatKey = code + "#" + categoryName;

                    //Compute code#cat#lengthClass key
                    Map<String, String> sampleDataValues = getSampleDataValues(exportSample);

                    //Get back lengthclass for individual
                    String lengthClass = sampleDataValues.get("LengthClass");
                    Float lengthClassValue = 0f;
                    if (lengthClass != null) {
                        lengthClassValue = Float.parseFloat(lengthClass) * 10;
                    }
                    String codeCatLCkey = codeCatKey + "#" + lengthClassValue.intValue();

                    //get speciesCategoryWeight and speciesCategoryNumber
                    Float speciesCategoryWeight = speciesCategoryWeights.get(codeCatKey);
                    Integer speciesCategoryNumber = speciesCategoryNumbers.get(codeCatKey);

                    //get subSampledWeight and subsampledNumbers
                    Float subsampledWeight = subsampledWeights.get(codeCatKey);
                    Integer subsampledNumber = subsampledNumbers.get(codeCatKey);

                    //Export Catch
                    exportCatch(exportSample,subsampledWeight,speciesCategoryWeight,subsampledNumber,speciesCategoryNumber,xmlCruise);

                    //Export individuals (only individuals for this subsample)
                    List<Sample> individualSamples = individuals.get(codeCatLCkey);
                    if (individualSamples != null) {
                        for (Sample individualSample : individualSamples) {
                            // EXPORT BIOLOGY
                            exportBiology(individualSample, xmlCruise);
                        }
                    }

                    xmlCruise.close("Catch");
                }
            }

            if (exportHaul) {
                xmlCruise.close("Haul");
            }
        }

        xmlCruise.close("Cruise");
        xmlCruise.close("Biotic");
        vocabulary.generate();
    }

    public void exportCruise(Voyage voyage, Vessel vessel, XmlWriter xml) throws IOException {
        xml.open("Cruise");

        xml.open("Survey");
        xml.create("Code",
                "IDREF", vocabulary.getVocabularyCode(voyage.getMission().getName(), "AC_Survey_PELGAS"));
        xml.close("Survey");
        xml.create("Country", 
                "IDREF", vocabulary.getVocabularyCode(voyage.getMission().getCountry(), "ISO_3166_FR"));
        xml.create("Platform", 
                "IDREF", vocabulary.getVocabularyCode(vessel.getCode(), "SHIPC_35HT"));
        xml.create("StartDate",
                EchoBaseCsvUtil.ISO8611_DATE_FORMATTER.format(voyage.getStartDate()));
        xml.create("EndDate",
                EchoBaseCsvUtil.ISO8611_DATE_FORMATTER.format(voyage.getEndDate()));
        xml.create("Organisation", 
                "IDREF", vocabulary.getVocabularyCode(voyage.getMission().getInstitution(), "EDMO_541"));
        xml.create("LocalID",
                voyage.getName());
    }

    public void exportHaul(Operation operation, int number, XmlWriter xml) throws IOException {
        xml.open("Haul");
        
        Collection<OperationMetadataValue> operationMetadataValues = operation.getOperationMetadataValue();
        ImmutableMap<String, OperationMetadataValue> operationMetadatas = Maps.uniqueIndex(operationMetadataValues, new Function<OperationMetadataValue, String>() {
            @Override
            public String apply(OperationMetadataValue value) {
                return value.getOperationMetadata().getName();
            }
        });
        
        String gearCode = operation.getGear().getGearCode();
        OperationMetadataValue meanWaterDepthMeta = operationMetadatas.get("MeanWaterDepth");
        Float meanWaterDepth = Float.parseFloat(meanWaterDepthMeta != null ? meanWaterDepthMeta.getDataValue() : "0.0");
        
        int minTrawlDepth;
        int maxTrawlDepth;
        
        String depthStratum = operation.getDepthStratum().getId();
        if (depthStratum.equals("SURF")) {
            minTrawlDepth = 0;
            maxTrawlDepth = 30;
            
        } else {
            minTrawlDepth = meanWaterDepth.intValue();
            maxTrawlDepth = minTrawlDepth;
            
            switch (gearCode) {
                case "76x70":
                    minTrawlDepth -= 20;
                    break;
                case "57x52":
                    minTrawlDepth -= 15;
                    break;
                case "942OBS":
                    minTrawlDepth -= 30;
                    break;
            }
        }

        //Normalize MaxTrawlDeh. If MaxTrawlDepth > BottomDepth (meanWaterDepth) -> MaxTrawlDepth = BottomDepth, cf. #11138
        if (maxTrawlDepth > meanWaterDepth.intValue()) {
            maxTrawlDepth = meanWaterDepth.intValue();
        }
        
        Float startLatitude = operation.getGearShootingStartLatitude();
        Float startLongitude = operation.getGearShootingStartLongitude();
        Float endLatitude = operation.getGearHaulingEndLatitude();
        Float endLongitude = operation.getGearHaulingEndLongitude();
        
        int distance = (int) (Math.sqrt(Math.pow(startLatitude - startLatitude, 2) + Math.pow(endLongitude - startLongitude, 2)) * 60);
        
        int netopening = 0;
        switch (gearCode) {
            case "76x70":
                netopening = 20;
                break;
            case "57x52":
                netopening = 15;
                break;
            case "942OBS":
                netopening = 30;
                break;
        }

        xml.create("Gear", 
            "IDREF", vocabulary.getVocabularyCode(gearCode, "Gear_PAR"));
        xml.create("Number",
            number);
        xml.create("StationName",
            operation.getId());
        xml.create("StartTime", 
            EchoBaseCsvUtil.ISO8611_DATETIME_FORMATTER.format(operation.getGearShootingStartTime()));
        xml.create("Duration", 
            (int)((operation.getGearShootingEndTime().getTime() - operation.getGearShootingStartTime().getTime()) / (1000 * 60)));
        xml.create("Validity", 
            "IDREF", vocabulary.getVocabularyCode("AC_HaulValidity_V"));
        xml.create("StartLatitude", startLatitude);
        xml.create("StartLongitude", startLongitude);
        xml.create("StopLatitude", endLatitude);
        xml.create("StopLongitude", endLongitude);
        xml.create("MinTrawlDepth",
            minTrawlDepth);
        xml.create("MaxTrawlDepth",
            maxTrawlDepth);
        xml.create("BottomDepth",
            meanWaterDepth.intValue());
        xml.create("Distance",
            distance);
        xml.create("Netopening",
            netopening);
        xml.create("Stratum",
                "IDREF", vocabulary.getVocabularyCode(depthStratum, "AC_Stratum_CLAS"));
    }
    
    public void exportCatch(Sample subsample, Float subsampledWeight, Float speciesCategoryWeight, Integer subsampledNumber, Integer speciesCategoryNumber, XmlWriter xml) throws IOException {
        xml.open("Catch");

        Map<String, String> sampleDataValues = getSampleDataValues(subsample);
        
        SpeciesCategory category = subsample.getSpeciesCategory();
        
        String lengthClass = sampleDataValues.get("LengthClass");
        float lengthClassValue = 0f;
        if (lengthClass != null) {
            lengthClassValue = Float.parseFloat(lengthClass) * 10;
        }

        String numberAtLength = sampleDataValues.get(SampleDataTypeImpl.NUMBER_AT_LENGTH);
        String weightAtLength = sampleDataValues.get(SampleDataTypeImpl.WEIGHT_AT_LENGTHKG);
        
        xml.create("DataType",
                "IDREF", vocabulary.getVocabularyCode("AC_CatchDataType_R"));
        xml.create("SpeciesCode", 
                "IDREF", vocabulary.getVocabularyCode("SpecWoRMS_" + category.getSpecies().getWormsCode()));
        xml.create("SpeciesValidity", 
                "IDREF", vocabulary.getVocabularyCode("AC_SpeciesValidity_1"));
        if (category.getSizeCategory().getName().equals("0")) {
            xml.create("SpeciesCategory", 1);
        } else if (category.getSizeCategory().getName().equals("G")){
            xml.create("SpeciesCategory", 2);
        }
        xml.create("SpeciesCategoryNumber", speciesCategoryNumber);
        xml.create("WeightUnit",
                "IDREF", vocabulary.getVocabularyCode("AC_WeightUnit_kg"));
        xml.create("SpeciesCategoryWeight", roundWith3Digits(speciesCategoryWeight));
        xml.create("SpeciesSex");
        
        if (subsampledNumber != null && subsampledNumber != 0) {
            //case catch of type subsample
            xml.create("SubsampledNumber", subsampledNumber);
            if (weightAtLength != null && subsampledWeight != null) {
                xml.create("SubsamplingFactor", Float.parseFloat(weightAtLength) / subsampledWeight);
            } else {
                xml.create("SubsamplingFactor", 0);
            }
            xml.create("SubsampleWeight", roundWith3Digits(subsampledWeight));
        } else {
            //Case catch of type total
            xml.create("SubsampledNumber", speciesCategoryNumber);
            if (weightAtLength != null && subsampledWeight != null) {
                xml.create("SubsamplingFactor", Float.parseFloat(weightAtLength) / speciesCategoryNumber);
            } else {
                xml.create("SubsamplingFactor", 0);
            }
            xml.create("SubsampleWeight", roundWith3Digits(speciesCategoryWeight));
        }
        
        if (lengthClassValue!= 0) {
            xml.create("LengthCode",
                    "IDREF", vocabulary.getVocabularyCode("AC_LengthCode_mm"));
            xml.create("LengthClass", (int) lengthClassValue);
            xml.create("LengthType",
                    "IDREF", vocabulary.getVocabularyCode("AC_LengthMeasurementType_1"));
            xml.create("NumberAtLength",  numberAtLength);
            xml.create("WeightAtLength", weightAtLength != null ? roundWith3Digits(Float.parseFloat(weightAtLength)) : 0);
        }
    }

    public String roundWith3Digits(String weightString) {
        //keep only 3 digits in decimal value for speciesCategoryWeight to prevent from rounds in calculus
        if (weightString.contains(".")){
            int dotIndex =  weightString.indexOf(".");
            if (weightString.length()>((dotIndex+4)+1)) {
                weightString = weightString.substring(0, dotIndex + 4);
            }
        }
        return weightString;
    }

    public String roundWith3Digits(Float weight) {
        if (weight != null && weight < 0.001) {
            return "0.000";
        } else if (weight != null) {
            return roundWith3Digits(weight.toString());
        } else {
            return "";
        }
    }

    public void exportBiology(Sample individualSample, XmlWriter xml) throws IOException {
        
        Map<String, Float> dataValues = new HashMap<>();
        Collection<SampleData> datas = individualSample.getSampleData();
        for (SampleData data : datas) {            
            String name = data.getSampleDataType().getName();
            Float value = data.getDataValue();
            dataValues.put(name, value);
        }
        
        Float age = dataValues.get("Age");
        if (age == null || age == -1) {
            return;
        }
                
        xml.open("Biology");
        
//        xml.create("StockCode", 
//            "IDREF", "");
        xml.create("FishID", 
            dataValues.get(SampleDataTypeImpl.SPECIMEN_INDEX).intValue());
        xml.create("LengthCode", 
            "IDREF", vocabulary.getVocabularyCode("AC_LengthCode_mm"));
        xml.create("LengthClass",
            dataValues.get("LTmm1") != null ? dataValues.get("LTmm1").intValue(): 0);
        xml.create("WeightUnit", 
            "IDREF", vocabulary.getVocabularyCode("AC_WeightUnit_gr"));
        xml.create("IndividualWeight",
            dataValues.get("Weightg") != null ? dataValues.get("Weightg").intValue() : 0);
        xml.create("IndividualSex", 
            "IDREF", dataValues.get("Sex") != null ? vocabulary.getVocabularyCode("Sex_" + dataValues.get("Sex"), "AC_Sex_U") : vocabulary.getVocabularyCode("AC_Sex_U"));
        xml.create("IndividualMaturity", 
            "IDREF", dataValues.get("Maturity") != null ? vocabulary.getVocabularyCode("Maturity_" + dataValues.get("Maturity"), "AC_MaturityCode_62") : vocabulary.getVocabularyCode("AC_MaturityCode_62"));
        xml.create("MaturityScale", 
            "IDREF", vocabulary.getVocabularyCode("AC_MaturityScale_M6"));
        xml.create("IndividualAge", 
            age.intValue());
//        xml.create("AgePlusGroup", 
//            "IDREF", "");
        xml.create("AgeSource", 
            "IDREF", vocabulary.getVocabularyCode("AC_AgeSource_Otolith"));
        xml.create("GeneticSamplingFlag", 
            "IDREF", vocabulary.getVocabularyCode("AC_SamplingFlag_no"));
        xml.create("StomachSamplingFlag", 
            "IDREF", vocabulary.getVocabularyCode("AC_SamplingFlag_no"));
        xml.create("ParasiteSamplingFlag", 
            "IDREF", vocabulary.getVocabularyCode("AC_SamplingFlag_no"));
//        xml.create("IndividualVertebraeCount");
        
        xml.close("Biology");
    }

    protected Map<String, String> getSampleDataValues(Sample sample) {
        Map<String, String> sampleDataValues = new HashMap<>();
        Collection<SampleData> datas = sample.getSampleData();
        for (SampleData data : datas) {

            String dataName = data.getSampleDataType().getName();
            Float value = data.getDataValue();
            String label = data.getDataLabel();

            if (SampleDataTypeImpl.NUMBER_AT_LENGTH.equals(dataName)) {
                //We always have floats instead of Atlantos expected ints (so rounding will strip the .0)
                sampleDataValues.put(SampleDataTypeImpl.NUMBER_AT_LENGTH, String.valueOf(Math.round(value)));
                sampleDataValues.put("LengthClass", label);
            } else {
                sampleDataValues.put(dataName, String.valueOf(value));
            }
        }
        return sampleDataValues;
    }
}
