package fr.ifremer.echobase.services.service.importdb.strategy;

/*
 * #%L
 * EchoBase :: Services
 * %%
 * Copyright (C) 2011 - 2012 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.Lists;
import fr.ifremer.echobase.entities.data.DataAcousticProvider;
import fr.ifremer.echobase.entities.EchoBaseUser;
import fr.ifremer.echobase.entities.EchoBaseUserEntityEnum;
import fr.ifremer.echobase.persistence.EchoBaseDbMeta;
import fr.ifremer.echobase.services.service.importdata.ImportException;
import org.nuiton.topia.persistence.TopiaException;
import org.nuiton.topia.persistence.metadata.AssociationMeta;
import org.nuiton.topia.persistence.metadata.TableMeta;

import java.io.File;
import java.util.List;
import java.util.Map;
import java.util.zip.ZipEntry;

/**
 * Referential only import db strategy.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.2
 */
public class ReferentialImportDbStrategy extends AbstractImportDbStrategy {

    @Override
    protected void validateTableEntries(EchoBaseDbMeta dbMeta,
                                        Map<TableMeta<EchoBaseUserEntityEnum>, ZipEntry> tables) throws ImportException {

        // check that contains only referential entries
        List<EchoBaseUserEntityEnum> badTableTypes = Lists.newArrayList();
        List<TableMeta<EchoBaseUserEntityEnum>> dataTables = dbMeta.getDataTables();
        for (TableMeta<EchoBaseUserEntityEnum> tableMetas : tables.keySet()) {
            if (dataTables.contains(tableMetas)) {
                badTableTypes.add(tableMetas.getSource());
            }
        }

        if (!badTableTypes.isEmpty()) {
            throw new ImportException(
                    "In referential import, can not accept " +
                    "data type, but found some data types: " + badTableTypes);
        }
    }

    @Override
    protected void validateAssociationEntries(EchoBaseDbMeta dbMeta,
                                              Map<AssociationMeta<EchoBaseUserEntityEnum>, ZipEntry> associations) throws ImportException {

        List<EchoBaseUserEntityEnum> badAssociationTypes = Lists.newArrayList();
        List<AssociationMeta<EchoBaseUserEntityEnum>> associationTables = dbMeta.getDataAssociations();
        for (AssociationMeta<EchoBaseUserEntityEnum> tableMetas : associations.keySet()) {
            if (associationTables.contains(tableMetas)) {
                badAssociationTypes.add(tableMetas.getSource());
            }
        }
        if (!badAssociationTypes.isEmpty()) {
            throw new ImportException(
                    "In referential import, can not accept " +
                    "data type, but found some data associations files: " +
                    badAssociationTypes);
        }
    }

    @Override
    protected void createImportLogEntry(EchoBaseUser user,
                                        File file,
                                        List<DataAcousticProvider> importedEntities) {
        // no importLog entry to add
    }

    @Override
    protected void createLogBookEntry(EchoBaseUser user,
                                      File file) throws TopiaException {

        persistenceService.createEntityModificationLog(
                "Import referential only db",
                "Referential EchoBase db",
                user.getEmail(),
                newDate(),
                "import db from file " + file.getName()
        );
    }

}
