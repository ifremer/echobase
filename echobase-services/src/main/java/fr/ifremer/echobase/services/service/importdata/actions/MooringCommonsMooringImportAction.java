package fr.ifremer.echobase.services.service.importdata.actions;

/*
 * #%L
 * EchoBase :: Services
 * %%
 * Copyright (C) 2011 - 2016 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.echobase.entities.EchoBaseUserEntityEnum;
import fr.ifremer.echobase.entities.data.Mooring;
import fr.ifremer.echobase.io.InputFile;
import fr.ifremer.echobase.services.service.importdata.DuplicatedMooringException;
import fr.ifremer.echobase.services.service.importdata.ImportDataFileResult;
import fr.ifremer.echobase.services.service.importdata.contexts.MooringCommonsMooringImportDataContext;
import fr.ifremer.echobase.services.service.importdata.csv.MooringCommonsMooringImportExportModel;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.csv.Import;

import java.util.Locale;

/**
 * Created on 25/03/16.
 *
 * @author Julien Ruchaud - ruchaud@codelutin.com
 * @since 4.0
 */
public class MooringCommonsMooringImportAction extends MooringCommonsImportDataActionSupport<Mooring> {

    /** Logger. */
    private static final Log log = LogFactory.getLog(MooringCommonsMooringImportAction.class);

    public MooringCommonsMooringImportAction(MooringCommonsMooringImportDataContext importDataContext) {
        super(importDataContext, importDataContext.getConfiguration().getMooringFile());
    }

    @Override
    protected MooringCommonsMooringImportExportModel createCsvImportModel(MooringCommonsMooringImportDataContext importDataContext) {
        return MooringCommonsMooringImportExportModel.forImport(importDataContext);
    }

    @Override
    protected MooringCommonsMooringImportExportModel createCsvExportModel(MooringCommonsMooringImportDataContext importDataContext) {
        return MooringCommonsMooringImportExportModel.forExport(importDataContext);
    }

    @Override
    public void performImport(MooringCommonsMooringImportDataContext importDataContext, InputFile inputFile, ImportDataFileResult result) {
        if (log.isInfoEnabled()) {
            log.info("Starts import of mooring from file " + inputFile.getFileName());
        }

        Locale locale = getLocale();

        try (Import<Mooring> importer = open()) {

            incrementsProgress();

            int rowNumber = 0;

            for (Mooring mooring : importer) {

                doFlushTransaction(++rowNumber);

                if (persistenceService.containsMooringByCode(mooring.getCode())) {
                    throw new DuplicatedMooringException(locale, rowNumber, mooring.getCode());
                }

                Mooring createdMooring = persistenceService.createMooring(mooring);
                addId(result, EchoBaseUserEntityEnum.Mooring, createdMooring, rowNumber);

                addProcessedRow(result, createdMooring);
            }
        }
    }

    @Override
    protected void computeImportedExport(MooringCommonsMooringImportDataContext importDataContext, ImportDataFileResult result) {

        for (Mooring mooring : getImportedEntities(Mooring.class, result)) {

            String mooringId = mooring.getTopiaId();
            if (log.isInfoEnabled()) {
                log.info("Adding mooring: " + mooringId + " to imported export.");
            }

            addImportedRow(result, mooring);
        }
    }

}
