/*
 * #%L
 * EchoBase :: Services
 * %%
 * Copyright (C) 2011 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ifremer.echobase.services.service.exportdb;

import fr.ifremer.echobase.entities.EchoBaseUserEntityEnum;
import fr.ifremer.echobase.services.EchoBaseServiceSupport;
import fr.ifremer.echobase.services.service.DbEditorService;
import fr.ifremer.echobase.services.service.DecoratorService;
import fr.ifremer.echobase.services.service.UserDbPersistenceService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.csv.ExportModel;
import org.nuiton.decorator.Decorator;
import org.nuiton.topia.persistence.TopiaEntity;
import org.nuiton.topia.persistence.metadata.AssociationMeta;
import org.nuiton.topia.persistence.metadata.ColumnMeta;
import org.nuiton.topia.persistence.metadata.TableMeta;
import org.nuiton.topia.service.csv.EntityCsvModel;
import org.nuiton.topia.service.csv.out.EntityAssociationExportModel;
import org.nuiton.topia.service.csv.out.ExportModelFactory;
import org.nuiton.topia.service.csv.out.PrepareDataForExport;
import org.nuiton.topia.service.csv.out.TopiaCsvExports;

import javax.inject.Inject;
import java.io.File;

/**
 * To export datas from db to csv files.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 0.2
 */
public class ExportService extends EchoBaseServiceSupport {

    /** Logger. */
    private static final Log log = LogFactory.getLog(ExportService.class);

    @Inject
    private UserDbPersistenceService persistenceService;

    @Inject
    private DbEditorService dbEditorService;

    @Inject
    private DecoratorService decoratorService;

    public String exportData(TableMeta<EchoBaseUserEntityEnum> meta, boolean asSeen) {

        if (log.isInfoEnabled()) {
            log.info("Export " + meta);
        }
        return TopiaCsvExports.exportData(meta,
                                          getModelFactory(asSeen),
                                          defaultPrepareDataForExport);
    }

    public void exportData(TableMeta<EchoBaseUserEntityEnum> meta, File file) {

        if (log.isInfoEnabled()) {
            log.info("Export " + meta + " to " + file);
        }

        TopiaCsvExports.exportData(meta,
                                   defaultExportFactory,
                                   defaultPrepareDataForExport,
                                   file);

        persistenceService.clear();
    }


    public void exportData(AssociationMeta<EchoBaseUserEntityEnum> meta, File file) {

        if (log.isInfoEnabled()) {
            log.info("Export " + meta + " to " + file);
        }
        TopiaCsvExports.exportData(meta,
                                   defaultExportFactory,
                                   defaultPrepareDataForExport,
                                   file);

        persistenceService.clear();
    }

    public ExportModelFactory<EchoBaseUserEntityEnum> getModelFactory(boolean asSeen) {
        ExportModelFactory<EchoBaseUserEntityEnum> result;
        if (asSeen) {
            result = asSeenExportFactory;
        } else {
            result = defaultExportFactory;
        }
        return result;
    }

    private ExportModelFactory<EchoBaseUserEntityEnum> defaultExportFactory = new ExportModelFactory<EchoBaseUserEntityEnum>() {

        @Override
        public <E extends TopiaEntity> ExportModel<E> buildForExport(TableMeta<EchoBaseUserEntityEnum> meta) {

            // normal export add topiaId column
            EntityCsvModel<EchoBaseUserEntityEnum, E> model = EntityCsvModel.newModel(
                    getConfiguration().getCsvSeparator(),
                    meta,
                    TopiaEntity.PROPERTY_TOPIA_ID
            );

            for (ColumnMeta columnMeta : meta) {
                String propertyName = columnMeta.getName();
                Class<?> type = columnMeta.getType();
                if (columnMeta.isFK()) {

                    Class<TopiaEntity> entityType = (Class<TopiaEntity>) type;


                    // export foreign key value as his topiaId
                    model.addForeignKeyForExport(propertyName, entityType);

                } else {
                    model.addDefaultColumn(propertyName, type);
                }
            }
            return model;
        }

        @Override
        public <E extends TopiaEntity> ExportModel<E> buildForExport(AssociationMeta<EchoBaseUserEntityEnum> associationMeta) {
            return EntityAssociationExportModel.newExportModel(
                    getConfiguration().getCsvSeparator(),
                    associationMeta
            );
        }
    };

    private ExportModelFactory<EchoBaseUserEntityEnum> asSeenExportFactory = new ExportModelFactory<EchoBaseUserEntityEnum>() {

        @Override
        public <E extends TopiaEntity> ExportModel<E> buildForExport(TableMeta<EchoBaseUserEntityEnum> meta) {


            // no need to have topiaId as first column
            EntityCsvModel<EchoBaseUserEntityEnum, E> model = EntityCsvModel.newModel(
                    getConfiguration().getCsvSeparator(),
                    meta
            );

            for (ColumnMeta columnMeta : meta) {
                String propertyName = columnMeta.getName();
                Class<?> type = columnMeta.getType();
                if (columnMeta.isFK()) {

                    Class<TopiaEntity> entityType = (Class<TopiaEntity>) type;

                    // export decorated foreign key value
                    Decorator<TopiaEntity> decorator =
                            decoratorService.getDecorator(entityType, null);
                    model.addDecoratedForeignKeyForExport(propertyName, propertyName, decorator);

                } else {
                    model.addDefaultColumn(propertyName, type);
                }
            }
            return model;
        }

        @Override
        public <E extends TopiaEntity> ExportModel<E> buildForExport(AssociationMeta<EchoBaseUserEntityEnum> associationMeta) {
            return EntityAssociationExportModel.newExportModel(
                    getConfiguration().getCsvSeparator(),
                    associationMeta
            );
        }
    };

    private PrepareDataForExport<EchoBaseUserEntityEnum> defaultPrepareDataForExport = new PrepareDataForExport<EchoBaseUserEntityEnum>() {

        @Override
        public <E extends TopiaEntity> Iterable<E> prepareData(TableMeta<EchoBaseUserEntityEnum> tableMeta) {

            return dbEditorService.iterateOnEntities(tableMeta, null);
        }

        @Override
        public <E extends TopiaEntity> Iterable<E> prepareData(AssociationMeta<EchoBaseUserEntityEnum> associationMeta) {

            TableMeta<EchoBaseUserEntityEnum> tableMeta = dbEditorService.getTableMeta(associationMeta.getSource());
            return dbEditorService.iterateOnEntities(tableMeta, "size(e." + associationMeta.getName() + ") > 0");
        }
    };
}
