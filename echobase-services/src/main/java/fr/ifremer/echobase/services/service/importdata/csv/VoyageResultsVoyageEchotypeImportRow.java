/*
 * #%L
 * EchoBase :: Services
 * %%
 * Copyright (C) 2011 - 2012 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ifremer.echobase.services.service.importdata.csv;

import fr.ifremer.echobase.entities.data.DataAcousticProvider;
import fr.ifremer.echobase.entities.data.Echotype;
import fr.ifremer.echobase.entities.data.Voyage;
import fr.ifremer.echobase.entities.references.Species;

/**
 * Bean used as a row for import of {@link VoyageResultsVoyageEchotypeImportExportModel}.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 0.3
 */
public class VoyageResultsVoyageEchotypeImportRow extends ResultsEchotypeImportRow<Voyage> {

    private static final long serialVersionUID = 1L;

    public static final String PROPERTY_VOYAGE = "voyage";
    protected Voyage voyage;

    public static VoyageResultsVoyageEchotypeImportRow of(DataAcousticProvider provider, Echotype echotype, Species species) {
        VoyageResultsVoyageEchotypeImportRow row = new VoyageResultsVoyageEchotypeImportRow(echotype);
        row.setProvider(provider);
        row.setSpecies(species);
        return row;
    }

    public VoyageResultsVoyageEchotypeImportRow() {
        super();
    }

    public VoyageResultsVoyageEchotypeImportRow(Echotype echotype) {
        super(echotype);
    }
    
    public Voyage getVoyage() {
        return voyage;
    }

    public void setVoyage(Voyage voyage) {
        this.voyage = voyage;
    }

    @Override
    public DataAcousticProvider<Voyage> getProvider() {
        return voyage;
    }

    @Override
    public void setProvider(DataAcousticProvider<Voyage> provider) {
        this.voyage = provider.getEntity();
    }
}
