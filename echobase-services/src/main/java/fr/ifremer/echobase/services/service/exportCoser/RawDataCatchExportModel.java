package fr.ifremer.echobase.services.service.exportCoser;

/*
 * #%L
 * EchoBase :: Services
 * %%
 * Copyright (C) 2011 - 2014 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import fr.ifremer.echobase.services.csv.EchoBaseCsvUtil;
import fr.ifremer.echobase.entities.data.Operation;
import fr.ifremer.echobase.entities.data.Sample;
import fr.ifremer.echobase.entities.data.Transect;
import fr.ifremer.echobase.entities.data.Transit;
import fr.ifremer.echobase.entities.data.Voyage;
import fr.ifremer.echobase.entities.references.Species;
import fr.ifremer.echobase.entities.references.SpeciesCategory;
import org.apache.commons.lang3.mutable.MutableFloat;
import org.apache.commons.lang3.mutable.MutableInt;
import org.apache.commons.lang3.tuple.Pair;
import org.nuiton.csv.ext.AbstractExportModel;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Created on 4/5/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.6
 */
public class RawDataCatchExportModel extends AbstractExportModel<RawDataCatchExportRow> {

    /*
    SELECT 
    voyage.name AS Campagne, 
    species.baracoudacode AS Espece, 
    operation.id AS Trait, 
    sample.numbersampled AS Nombre,
    sample.sampleweight AS Poids 
    FROM 
    transit AS transit,
    voyage AS voyage, 
    transect AS transect,
    operation AS operation, 
    sample AS sample,
    speciescategory AS speciescategory, 
    species AS species
    WHERE 
    transit.voyage = voyage.topiaid AND 
    transect.transit = transit.topiaid AND 
    operation.transect = transect.topiaid AND
    sample.operation = operation.topiaid AND
    sample.speciescategory = speciescategory.topiaid AND
    speciescategory.species = species.topiaid

dans cette requête, il faut en plus :
- enlever le '_' dans le baracoudacode
- créer un champ "Annee" avec l'année du voyage
- faire une somme des 'Nombre" et "Poids" en groupant sur les champs 
"Campagne","trait", "Espece" et "Annee", mais j'y arrive pas...
     */

    public RawDataCatchExportModel(char separator) {
        super(separator);
        newColumnForExport("Campagne", EchoBaseCsvUtil.<RawDataCatchExportRow, String>newBeanProperty(RawDataCatchExportRow.PROPERTY_VOYAGE_NAME));
        newColumnForExport("Annee", EchoBaseCsvUtil.<RawDataCatchExportRow, Date>newBeanProperty(RawDataCatchExportRow.PROPERTY_VOYAGE_YEAR), EchoBaseCsvUtil.YEAR);
        newColumnForExport("Trait", EchoBaseCsvUtil.<RawDataCatchExportRow, String>newBeanProperty(RawDataCatchExportRow.PROPERTY_OPERATION_ID));
        newColumnForExport("Espece", RawDataCatchExportRow.PROPERTY_SPECIES,EchoBaseCsvUtil.SPECIES_TO_COSER_CODE);
        newColumnForExport("Nombre", RawDataCatchExportRow.PROPERTY_OPERATION_NUMBER, EchoBaseCsvUtil.PRIMITIVE_INTEGER);
        newColumnForExport("Poids", RawDataCatchExportRow.PROPERTY_OPERATION_WEIGHT, EchoBaseCsvUtil.PRIMITIVE_FLOAT);
    }

    public List<RawDataCatchExportRow> prepareRows(List<Voyage> voyages) {
        List<RawDataCatchExportRow> rows = Lists.newArrayList();
        for (Voyage voyage : voyages) {
            prepareRows(voyage, rows);
        }
        return rows;
    }

    protected void prepareRows(Voyage voyage, List<RawDataCatchExportRow> rows) {
        if (voyage.isTransitNotEmpty()) {
            for (Transit transit : voyage.getTransit()) {

                if (transit.isTransectNotEmpty()) {
                    for (Transect transect : transit.getTransect()) {

                        if (transect.isOperationNotEmpty()) {
                            for (Operation operation : transect.getOperation()) {

                                if (operation.isSampleNotEmpty()) {

                                    // compute sum of number and weight per species
                                    Map<Species, Pair<MutableInt, MutableFloat>> speciesMap = Maps.newHashMap();
                                    for (Sample sample : operation.getSample()) {

                                        SpeciesCategory speciesCategory = sample.getSpeciesCategory();
                                        if (speciesCategory != null) {
                                            Species species = speciesCategory.getSpecies();
                                            Pair<MutableInt, MutableFloat> numberWeight = speciesMap.get(species);
                                            if (numberWeight == null) {
                                                numberWeight = Pair.of(new MutableInt(), new MutableFloat());
                                                speciesMap.put(species, numberWeight);
                                            }
                                            Integer numberSampled = sample.getNumberSampled();
                                            if (numberSampled != null) {
                                                numberWeight.getLeft().add(numberSampled);
                                            }
                                            Float sampleWeight = sample.getSampleWeight();
                                            if (sampleWeight != null) {
                                                numberWeight.getRight().add(sampleWeight);
                                            }
                                        }
                                    }

                                    // create rows
                                    for (Map.Entry<Species, Pair<MutableInt, MutableFloat>> entry : speciesMap.entrySet()) {
                                        Species species = entry.getKey();
                                        Pair<MutableInt, MutableFloat> numberWeight = entry.getValue();

                                        RawDataCatchExportRow row = new RawDataCatchExportRow();
                                        row.setVoyage(voyage);
                                        row.setOperation(operation);
                                        row.setSpecies(species);
                                        row.setNumber(numberWeight.getLeft().intValue());
                                        row.setWeight(numberWeight.getRight().floatValue());
                                        rows.add(row);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

}
