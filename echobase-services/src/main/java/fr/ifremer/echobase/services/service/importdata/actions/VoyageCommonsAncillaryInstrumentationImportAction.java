package fr.ifremer.echobase.services.service.importdata.actions;

/*
 * #%L
 * EchoBase :: Services
 * %%
 * Copyright (C) 2011 - 2016 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.echobase.entities.EchoBaseUserEntityEnum;
import fr.ifremer.echobase.entities.data.DataAcousticProvider;
import fr.ifremer.echobase.entities.data.Voyage;
import fr.ifremer.echobase.entities.references.AncillaryInstrumentation;
import fr.ifremer.echobase.entities.references.Vessel;
import fr.ifremer.echobase.services.service.importdata.ImportDataFileResult;
import fr.ifremer.echobase.services.service.importdata.MismatchProviderException;
import fr.ifremer.echobase.services.service.importdata.configurations.VoyageCommonsAncillaryInstrumentationImportConfiguration;
import fr.ifremer.echobase.services.service.importdata.contexts.VoyageCommonsAncillaryInstrumentationImportDataContext;
import fr.ifremer.echobase.services.service.importdata.csv.VoyageCommonsAncillaryInstrumentationImportExportModel;
import fr.ifremer.echobase.services.service.importdata.csv.VoyageCommonsAncillaryInstrumentationImportRow;
import java.util.List;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * @author Julien Ruchaud - ruchaud@codelutin.com
 * @since 4.0
 */
public class VoyageCommonsAncillaryInstrumentationImportAction extends ImportAncillaryInstrumentationActionSupport<VoyageCommonsAncillaryInstrumentationImportConfiguration, VoyageCommonsAncillaryInstrumentationImportDataContext, VoyageCommonsAncillaryInstrumentationImportRow> {

    /** Logger. */
    private static final Log log = LogFactory.getLog(VoyageCommonsAncillaryInstrumentationImportAction.class);

    public VoyageCommonsAncillaryInstrumentationImportAction(VoyageCommonsAncillaryInstrumentationImportDataContext importDataContext) {
        super(importDataContext, importDataContext.getConfiguration().getAncillaryInstrumentationFile());
    }

    @Override
    protected VoyageCommonsAncillaryInstrumentationImportRow newImportedRow(DataAcousticProvider provider, AncillaryInstrumentation ancillaryInstrumentation) {
        return VoyageCommonsAncillaryInstrumentationImportRow.of(provider, ancillaryInstrumentation);
    }

    @Override
    protected List<DataAcousticProvider> getDataProviders(VoyageCommonsAncillaryInstrumentationImportDataContext importDataContext, VoyageCommonsAncillaryInstrumentationImportRow row, int rowNumber) {
        Voyage expectedVoyage = importDataContext.getVoyage();
        Voyage voyage = row.getVoyage();

        if (!expectedVoyage.equals(voyage)) {
            throw new MismatchProviderException(getLocale(), rowNumber, voyage.getName());
        }
        
        Vessel vessel = row.getVessel();
        List transects = persistenceService.getTransects(voyage, vessel);
        return transects;
    }

    @Override
    protected VoyageCommonsAncillaryInstrumentationImportExportModel createCsvImportModel(VoyageCommonsAncillaryInstrumentationImportDataContext importDataContext) {
        return VoyageCommonsAncillaryInstrumentationImportExportModel.forImport(importDataContext);
    }

    @Override
    protected VoyageCommonsAncillaryInstrumentationImportExportModel createCsvExportModel(VoyageCommonsAncillaryInstrumentationImportDataContext importDataContext) {
        return VoyageCommonsAncillaryInstrumentationImportExportModel.forExport(importDataContext);
    }

}
