package fr.ifremer.echobase.services.service.importdata.actions;
/*
 * #%L
 * EchoBase :: Services
 * %%
 * Copyright (C) 2011 - 2019 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.echobase.services.service.importdata.configurations.ImportAcousticsConfiguration;
import fr.ifremer.echobase.services.service.importdata.contexts.ImportDataContextSupport;
import fr.ifremer.echobase.services.service.importdata.csv.CalibrationImportRow;

/**
 * Created on 05/12/19.
 *
 * @author Jean Couteau - couteau@codelutin.com
 * @since 4.2
 */
public abstract class ImportCalibrationActionSupport  <M extends ImportAcousticsConfiguration, C extends ImportDataContextSupport<M>, E extends CalibrationImportRow> extends ImportDataActionSupport<M, C, E> {

    public ImportCalibrationActionSupport(C importDataContext) {
        super(importDataContext, importDataContext.getConfiguration().getCalibrationsFile());
    }

}
