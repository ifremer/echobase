package fr.ifremer.echobase.services.service.removedata.strategy;

/*
 * #%L
 * EchoBase :: Services
 * %%
 * Copyright (C) 2011 - 2012 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.Sets;
import fr.ifremer.echobase.entities.ImportLog;
import fr.ifremer.echobase.entities.ImportType;
import fr.ifremer.echobase.entities.data.GearMetadataValue;
import fr.ifremer.echobase.entities.data.Operation;
import fr.ifremer.echobase.entities.data.OperationMetadataValue;
import fr.ifremer.echobase.entities.data.Transect;
import fr.ifremer.echobase.entities.data.Voyage;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.persistence.TopiaException;

import java.util.Set;
import fr.ifremer.echobase.entities.data.DataAcousticProvider;

/**
 * Remove a {@link ImportType#OPERATION} import.
 *
 * Can remove only {@link Operation}.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.2
 */
public class OperationRemoveDataStrategy extends AbstractRemoveDataStrategy<Voyage> {

    /** Logger. */
    private static final Log log =
            LogFactory.getLog(OperationRemoveDataStrategy.class);

    @Override
    public long computeNbSteps(DataAcousticProvider<Voyage> provider, ImportLog importLog) {
        return getImportFileIdsCount(importLog);
    }

    @Override
    protected void removeImportData(DataAcousticProvider<Voyage> provider, String id) throws TopiaException {
        if (id.startsWith(OperationMetadataValue.class.getName())) {

            OperationMetadataValue operationMetadataValue = persistenceService.getOperationMetadataValue(id);
            operationMetadataValue.getOperation().removeOperationMetadataValue(operationMetadataValue);

            if (log.isDebugEnabled()) {
                log.debug(operationMetadataValue + " was removed");
            }


        } else if (id.startsWith(GearMetadataValue.class.getName())) {

            GearMetadataValue gearMetadataValue = persistenceService.getGearMetadataValue(id);
            gearMetadataValue.getOperation().removeGearMetadataValue(gearMetadataValue);

            if (log.isDebugEnabled()) {
                log.debug(gearMetadataValue + " was removed");
            }

        } else if (id.startsWith(Operation.class.getName())) {

            // get operation
            Operation operation = persistenceService.getOperation(id);

            // remove it from transect
            Transect transect = persistenceService.getTransectContainsOperation(operation);
            transect.removeOperation(operation);

            // remove it
            persistenceService.deleteOperation(operation);
            if (log.isDebugEnabled()) {
                log.debug(operation + " was removed");
            }
        } else {
            canNotDealWithId(id);
        }
    }

    @Override
    public Set<ImportType> getPossibleSubImportType() {
        return Sets.newHashSet(ImportType.CATCHES);
    }
}
