/*
 * #%L
 * EchoBase :: Services
 * %%
 * Copyright (C) 2011 - 2012 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ifremer.echobase.services.service.importdata.csv;

import fr.ifremer.echobase.entities.data.Voyage;
import fr.ifremer.echobase.entities.references.CellType;
import fr.ifremer.echobase.entities.references.DataQuality;
import fr.ifremer.echobase.services.csv.EchoBaseCsvUtil;
import fr.ifremer.echobase.services.service.importdata.contexts.VoyageResultsImportDataContext;

/**
 * Model to import cells (with type "region").
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 0.3
 */
public class VoyageResultsRegionCellImportExportModel extends EchoBaseImportExportModelSupport<VoyageResultsRegionCellImportRow> {

    private VoyageResultsRegionCellImportExportModel(char separator) {
        super(separator);
    }

    public static VoyageResultsRegionCellImportExportModel forImport(VoyageResultsImportDataContext importDataContext) {

        VoyageResultsRegionCellImportExportModel model = new VoyageResultsRegionCellImportExportModel(importDataContext.getCsvSeparator());
        model.newForeignKeyColumn(VoyageResultsRegionCellImportRow.PROPERTY_VOYAGE, Voyage.class, Voyage.PROPERTY_NAME, importDataContext.getVoyagesByName());
        model.newForeignKeyColumn(VoyageResultsRegionCellImportRow.PROPERTY_CELL_TYPE, CellType.class, CellType.PROPERTY_ID, importDataContext.getRegionCellTypesById());
        model.newForeignKeyColumn(VoyageResultsRegionCellImportRow.PROPERTY_DATA_QUALITY, DataQuality.class, DataQuality.PROPERTY_QUALITY_DATA_FLAG_VALUES, importDataContext.getDataQualitiesByName());
        model.newMandatoryColumn(VoyageResultsRegionCellImportRow.PROPERTY_NAME);
        model.newMandatoryColumn("regionEnvCoordinates", VoyageResultsRegionCellImportRow.PROPERTY_DATA_COORDINATE);
        model.newMandatoryColumn("surface", VoyageResultsRegionCellImportRow.PROPERTY_DATA_SURFACE, EchoBaseCsvUtil.PRIMITIVE_FLOAT);
        return model;

    }

    public static VoyageResultsRegionCellImportExportModel forExport(VoyageResultsImportDataContext importDataContext) {

        VoyageResultsRegionCellImportExportModel model = new VoyageResultsRegionCellImportExportModel(importDataContext.getCsvSeparator());
        model.newColumnForExport(VoyageResultsRegionCellImportRow.PROPERTY_VOYAGE, EchoBaseCsvUtil.VOYAGE_FORMATTER);
        model.newColumnForExport(VoyageResultsRegionCellImportRow.PROPERTY_CELL_TYPE, EchoBaseCsvUtil.CELL_TYPE_FORMATTER);
        model.newColumnForExport(VoyageResultsRegionCellImportRow.PROPERTY_DATA_QUALITY, EchoBaseCsvUtil.DATA_QUALITY_FORMATTER);
        model.newColumnForExport(VoyageResultsRegionCellImportRow.PROPERTY_NAME);
        model.newColumnForExport("regionEnvCoordinates", VoyageResultsRegionCellImportRow.PROPERTY_DATA_COORDINATE);
        model.newColumnForExport("surface", VoyageResultsRegionCellImportRow.PROPERTY_DATA_SURFACE, EchoBaseCsvUtil.PRIMITIVE_FLOAT);
        return model;

    }

    @Override
    public VoyageResultsRegionCellImportRow newEmptyInstance() {
        return new VoyageResultsRegionCellImportRow();
    }
}
