package fr.ifremer.echobase.services.service.exportCoser;

/*
 * #%L
 * EchoBase :: Services
 * %%
 * Copyright (C) 2011 - 2014 Ifremer, Codelutin, Chemit Tony
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.echobase.services.csv.EchoBaseCsvUtil;
import org.nuiton.csv.ext.AbstractExportModel;

/**
 * Created on 3/20/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.6
 */
public class IndicatorExportModel extends AbstractExportModel<IndicatorExportRow> {

    public IndicatorExportModel(char separator) {
        super(separator);
        newColumnForExport("Campagne", EchoBaseCsvUtil.<IndicatorExportRow, String>newBeanProperty(IndicatorExportRow.PROPERTY_MISSION_NAME));
        newColumnForExport("Indicateur", EchoBaseCsvUtil.<IndicatorExportRow, String>newBeanProperty(IndicatorExportRow.PROPERTY_INDICATOR_NAME));
        newColumnForExport("Espece", IndicatorExportRow.PROPERTY_SPECIES, EchoBaseCsvUtil.SPECIES_TO_COSER_CODE);
        newColumnForExport("Strate", IndicatorExportRow.PROPERTY_STRATUM);
        newColumnForExport("Annee", IndicatorExportRow.PROPERTY_DATE, EchoBaseCsvUtil.YEAR);
        newColumnForExport("Estimation", IndicatorExportRow.PROPERTY_ESTIMATION, EchoBaseCsvUtil.FLOAT);
        newColumnForExport("EcartType", IndicatorExportRow.PROPERTY_STANDARD_DEVIATION, EchoBaseCsvUtil.FLOAT);
        newColumnForExport("CV", IndicatorExportRow.PROPERTY_CV, EchoBaseCsvUtil.FLOAT);
    }

}