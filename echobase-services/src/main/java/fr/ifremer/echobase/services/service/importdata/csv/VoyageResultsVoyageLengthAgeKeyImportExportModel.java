/*
 * #%L
 * EchoBase :: Services
 * %%
 * Copyright (C) 2011 - 2012 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ifremer.echobase.services.service.importdata.csv;

import fr.ifremer.echobase.entities.data.LengthAgeKey;
import fr.ifremer.echobase.entities.data.Voyage;
import fr.ifremer.echobase.entities.references.Species;
import fr.ifremer.echobase.entities.references.Strata;
import fr.ifremer.echobase.services.csv.EchoBaseCsvUtil;
import fr.ifremer.echobase.services.service.importdata.contexts.VoyageResultsImportDataContext;

/**
 * Model to import {@link  LengthAgeKey}.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 0.3
 */
public class VoyageResultsVoyageLengthAgeKeyImportExportModel extends EchoBaseImportExportModelSupport<VoyageResultsVoyageLengthAgeKeyImportRow> {

    protected static final String HEADER_SPECIES = "baracoudaCode";

    private VoyageResultsVoyageLengthAgeKeyImportExportModel(char separator) {
        super(separator);
    }

    public static VoyageResultsVoyageLengthAgeKeyImportExportModel forImport(VoyageResultsImportDataContext importDataContext) {

        VoyageResultsVoyageLengthAgeKeyImportExportModel model = new VoyageResultsVoyageLengthAgeKeyImportExportModel(importDataContext.getCsvSeparator());
        model.newForeignKeyColumn(VoyageResultsVoyageLengthAgeKeyImportRow.PROPERTY_VOYAGE, Voyage.class, Voyage.PROPERTY_NAME, importDataContext.getVoyagesByName());
        model.newMandatoryColumn(LengthAgeKey.PROPERTY_AGE, EchoBaseCsvUtil.PRIMITIVE_INTEGER);
        model.newMandatoryColumn(LengthAgeKey.PROPERTY_PERCENT_AT_AGE, EchoBaseCsvUtil.PRIMITIVE_FLOAT);
        model.newMandatoryColumn(LengthAgeKey.PROPERTY_LENGTH, EchoBaseCsvUtil.PRIMITIVE_FLOAT);
        model.newMandatoryColumn(LengthAgeKey.PROPERTY_METADATA);
        model.newForeignKeyColumn(LengthAgeKey.PROPERTY_STRATA, Strata.class, Strata.PROPERTY_NAME, importDataContext.getStratasByName());
        model.newForeignKeyColumn(HEADER_SPECIES, LengthAgeKey.PROPERTY_SPECIES, Species.class, Species.PROPERTY_BARACOUDA_CODE, importDataContext.getSpeciesByBaracoudaCode());
        return model;

    }

    public static VoyageResultsVoyageLengthAgeKeyImportExportModel forExport(VoyageResultsImportDataContext importDataContext) {

        VoyageResultsVoyageLengthAgeKeyImportExportModel model = new VoyageResultsVoyageLengthAgeKeyImportExportModel(importDataContext.getCsvSeparator());
        model.newColumnForExport(VoyageResultsVoyageLengthAgeKeyImportRow.PROPERTY_VOYAGE, EchoBaseCsvUtil.VOYAGE_FORMATTER);
        model.newColumnForExport(LengthAgeKey.PROPERTY_AGE, EchoBaseCsvUtil.PRIMITIVE_INTEGER);
        model.newColumnForExport(LengthAgeKey.PROPERTY_PERCENT_AT_AGE, EchoBaseCsvUtil.PRIMITIVE_FLOAT);
        model.newColumnForExport(LengthAgeKey.PROPERTY_LENGTH, EchoBaseCsvUtil.PRIMITIVE_FLOAT);
        model.newColumnForExport(LengthAgeKey.PROPERTY_METADATA);
        model.newColumnForExport(LengthAgeKey.PROPERTY_STRATA, LengthAgeKey.PROPERTY_STRATA, EchoBaseCsvUtil.STRATA_FORMATTER);
        model.newColumnForExport(HEADER_SPECIES, LengthAgeKey.PROPERTY_SPECIES, EchoBaseCsvUtil.SPECIES_FORMATTER);
        return model;

    }

    @Override
    public VoyageResultsVoyageLengthAgeKeyImportRow newEmptyInstance() {
        return new VoyageResultsVoyageLengthAgeKeyImportRow();
    }
}
