package fr.ifremer.echobase.services.service.workingDb;

/*
 * #%L
 * EchoBase :: Services
 * %%
 * Copyright (C) 2011 - 2012 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.echobase.EchoBaseTechnicalException;
import fr.ifremer.echobase.entities.DriverType;
import fr.ifremer.echobase.entities.EchoBaseInternalPersistenceContext;
import fr.ifremer.echobase.entities.WorkingDbConfiguration;
import fr.ifremer.echobase.entities.WorkingDbConfigurationTopiaDao;
import fr.ifremer.echobase.persistence.JdbcConfiguration;
import fr.ifremer.echobase.services.EchoBaseServiceSupport;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.persistence.TopiaException;
import org.nuiton.topia.persistence.internal.support.HibernateTopiaSqlSupport;

import javax.inject.Inject;
import java.io.File;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

/**
 * To manager {@link WorkingDbConfiguration}.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.1
 */
public class WorkingDbConfigurationService extends EchoBaseServiceSupport {

    /** Logger. */
    private static final Log log =
            LogFactory.getLog(WorkingDbConfigurationService.class);

    public static final String CREATE_DB_SQL_QUERY = "CREATE DATABASE \"%s\" WITH OWNER = %s ENCODING 'UTF8' TEMPLATE template0;";

    @Inject
    EchoBaseInternalPersistenceContext echoBaseInternalPersistenceContext;

    public List<WorkingDbConfiguration> getWorkingDbConfigurations() {
        try {
            return getDao().findAll();
        } catch (TopiaException eee) {
            throw new EchoBaseTechnicalException(eee);
        }
    }

    public WorkingDbConfiguration getEditableConf(String id) throws WorkingDbConfigurationNotFoundException {
        WorkingDbConfiguration entity = getExistingConf(id);
        WorkingDbConfiguration result = newConfiguration();
        result.setTopiaId(entity.getTopiaId());
        result.setDescription(entity.getDescription());
        result.setDriverType(entity.getDriverType());
        String url = entity.getUrl();

        if (url.contains("${echobase.data.directory}")) {
            File dataDirectory = getConfiguration().getDataDirectory();
            url = url.replace("${echobase.data.directory}",
                              dataDirectory.getAbsolutePath());
        }
        result.setUrl(url);
        return result;
    }

    public WorkingDbConfiguration create(WorkingDbConfiguration conf) throws WorkingDbConfigurationAlreadyExistException {
        try {

            // check not already exists ?
            boolean exists = isUrlAlreadyUsed(conf.getUrl());
            if (exists) {
                throw new WorkingDbConfigurationAlreadyExistException();
            }

            WorkingDbConfiguration result = getDao().create(conf);

            echoBaseInternalPersistenceContext.commit();
            return result;
        } catch (TopiaException eee) {
            throw new EchoBaseTechnicalException(eee);
        }
    }

    public void delete(String id) throws WorkingDbConfigurationNotFoundException {
        try {
            WorkingDbConfiguration entity = getExistingConf(id);
            getDao().delete(entity);
            echoBaseInternalPersistenceContext.commit();
        } catch (TopiaException eee) {
            throw new EchoBaseTechnicalException(eee);
        }
    }

    public WorkingDbConfiguration newConfiguration() {
        try {
            return getDao().newInstance();
        } catch (TopiaException e) {
            throw new EchoBaseTechnicalException(e);
        }
    }

    public boolean isUrlAlreadyUsed(String url) {
        try {
            return getDao().forUrlEquals(url).exists();
        } catch (TopiaException e) {
            throw new EchoBaseTechnicalException(e);
        }
    }

    public void createEmbeddedWorkingDbConfiguration() {

        String url = "jdbc:h2:file:${echobase.data.directory}/db/echobase;CACHE_SIZE=65536;AUTO_SERVER=TRUE";
        if (!isUrlAlreadyUsed(url)) {
            WorkingDbConfiguration conf = newConfiguration();
            conf.setDriverType(DriverType.H2);
            conf.setDescription("Embedded working db");
            conf.setUrl(url);
            try {
                create(conf);
            } catch (WorkingDbConfigurationAlreadyExistException e) {
                // can not happen
            }
        }
    }

    /**
     * Create the concrete working db from his working configuration.
     *
     * <strong>Note: </strong> Works only for pg db.
     *
     * @param jdbcConf id of the configuration to use.
     * @since 2.4
     */
    public void createDb(JdbcConfiguration jdbcConf) throws WorkingDbConfigurationNotFoundException {


        // get the dbName from his url
        String url = jdbcConf.getUrl();
        String dbName = StringUtils.substringAfterLast(url, "/");

        try {
            String sql = String.format(CREATE_DB_SQL_QUERY, dbName, jdbcConf.getLogin());
            if (log.isInfoEnabled()) {
                log.info("Create new postgres database: " + dbName +
                         " with sql statement: " + sql);
            }
            serviceContext.getEchoBaseUserPersistenceContext().getHibernateSupport().getHibernateSession().doWork(
                    new HibernateTopiaSqlSupport.HibernateSqlWork(sql) {
                        @Override
                        public void execute(Connection connection) throws SQLException {
                            //to create a database, need no transaction block
                            connection.setAutoCommit(true);
                            super.execute(connection);
                        }
                    }
            );
        } catch (Exception e) {
            throw new EchoBaseTechnicalException(
                    "Can't create a new database: " + dbName, e);
        }
    }

    public WorkingDbConfiguration getWorkingDbConfigurationByUrl(String url) {

        return getDao().forUrlEquals(url).findUnique();
    }

    protected WorkingDbConfiguration getExistingConf(String id) throws WorkingDbConfigurationNotFoundException {
        WorkingDbConfiguration entity = getDao().forTopiaIdEquals(id).findUnique();;
        if (entity == null) {
            throw new WorkingDbConfigurationNotFoundException();
        }
        return entity;
    }

    protected WorkingDbConfigurationTopiaDao getDao() {
        return echoBaseInternalPersistenceContext.getWorkingDbConfigurationDao();
    }
}
