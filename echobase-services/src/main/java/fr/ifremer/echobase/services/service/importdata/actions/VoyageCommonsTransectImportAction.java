package fr.ifremer.echobase.services.service.importdata.actions;

/*
 * #%L
 * EchoBase :: Services
 * %%
 * Copyright (C) 2011 - 2016 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.echobase.entities.EchoBaseUserEntityEnum;
import fr.ifremer.echobase.entities.data.Transect;
import fr.ifremer.echobase.entities.data.Transit;
import fr.ifremer.echobase.entities.data.Voyage;
import fr.ifremer.echobase.io.InputFile;
import fr.ifremer.echobase.services.service.importdata.DuplicatedTransectException;
import fr.ifremer.echobase.services.service.importdata.ImportDataFileResult;
import fr.ifremer.echobase.services.service.importdata.NoTransitFoundBetweenDateException;
import fr.ifremer.echobase.services.service.importdata.contexts.VoyageCommonsImportDataContext;
import fr.ifremer.echobase.services.service.importdata.csv.VoyageCommonsTransectImportExportModel;
import fr.ifremer.echobase.services.service.importdata.csv.VoyageCommonsTransectImportRow;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.csv.Import;

import java.util.Date;

/**
 * Created on 25/03/16.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 4.0
 */
public class VoyageCommonsTransectImportAction extends VoyageCommonsImportDataActionSupport<VoyageCommonsTransectImportRow> {

    /** Logger. */
    private static final Log log = LogFactory.getLog(VoyageCommonsTransectImportAction.class);

    public VoyageCommonsTransectImportAction(VoyageCommonsImportDataContext importDataContext) {
        super(importDataContext, importDataContext.getConfiguration().getTransectFile());
    }

    @Override
    protected VoyageCommonsTransectImportExportModel createCsvImportModel(VoyageCommonsImportDataContext importDataContext) {
        return VoyageCommonsTransectImportExportModel.forImport(importDataContext);

    }

    @Override
    protected VoyageCommonsTransectImportExportModel createCsvExportModel(VoyageCommonsImportDataContext importDataContext) {
        return VoyageCommonsTransectImportExportModel.forExport(importDataContext);
    }

    @Override
    protected void performImport(VoyageCommonsImportDataContext importDataContext, InputFile inputFile, ImportDataFileResult result) {

        if (log.isInfoEnabled()) {
            log.info("Starts import of transects from file " + inputFile.getFileName());
        }

        Voyage voyage = importDataContext.getVoyage();
        String datum = getConfiguration().getDatum();
        String license = getConfiguration().getTransectLicence();
        String geospatialVerticalPositive = getConfiguration().getTransectGeospatialVerticalPositive();
        String binUnitsPingAxis = getConfiguration().getTransectBinUnitsPingAxis();

        try (Import<VoyageCommonsTransectImportRow> importer = open()) {

            incrementsProgress();

            int rowNumber = 0;

            for (VoyageCommonsTransectImportRow row : importer) {

                doFlushTransaction(++rowNumber);

                Transect transect = row.getTransect();

                boolean exists = persistenceService.containsTransect(voyage, transect.getTitle());
                if (exists) {
                    throw new DuplicatedTransectException(getLocale(), rowNumber, voyage.getName(), transect.getTitle());
                }

                Date timeCoverageStart = transect.getTimeCoverageStart();
                Date timeCoverageEnd = transect.getTimeCoverageEnd();

                Transit transit = voyage.getTransit(timeCoverageStart, timeCoverageEnd);
                if (transit == null) {
                    throw new NoTransitFoundBetweenDateException(getLocale(), voyage, timeCoverageStart, timeCoverageEnd);
                }

                transect.setDatum(datum);
                transect.setLicence(license);
                transect.setGeospatialVerticalPositive(geospatialVerticalPositive);
                transect.setBinUnitsPingAxis(binUnitsPingAxis);

                Transect createdTransect = persistenceService.createTransect(transect);

                transit.addTransect(createdTransect);

                addProcessedRow(result, row);
                addId(result, EchoBaseUserEntityEnum.Transect, createdTransect, rowNumber);

            }

        }

    }

    @Override
    protected void computeImportedExport(VoyageCommonsImportDataContext importDataContext, ImportDataFileResult result) {

        String voyageId = importDataContext.getConfiguration().getVoyageId();
        Voyage voyage = persistenceService.getVoyage(voyageId);

        for (Transect transect : getImportedEntities(Transect.class, result)) {

            String transectId = transect.getTopiaId();
            if (log.isInfoEnabled()) {
                log.info("Adding transect: " + transectId + " to imported export.");
            }

            VoyageCommonsTransectImportRow importedRow = VoyageCommonsTransectImportRow.of(voyage, transect);
            addImportedRow(result, importedRow);

        }

    }

}
