package fr.ifremer.echobase.services.service.importdata.contexts;

/*
 * #%L
 * EchoBase :: Services
 * %%
 * Copyright (C) 2011 - 2016 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.echobase.entities.EchoBaseUser;
import fr.ifremer.echobase.entities.data.Transect;
import fr.ifremer.echobase.entities.data.Transit;
import fr.ifremer.echobase.entities.data.Voyage;
import fr.ifremer.echobase.entities.references.Vessel;
import fr.ifremer.echobase.services.service.UserDbPersistenceService;
import fr.ifremer.echobase.services.service.importdata.configurations.VoyageAcousticsImportConfiguration;
import java.util.Collection;

import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

/**
 * @author Julien Ruchaud - ruchaud@codelutin.com
 */
public class VoyageAcousticsImportDataContext extends ImportDataContextSupport<VoyageAcousticsImportConfiguration> {

    private Voyage voyage;
    private Vessel vessel;
    private Map<Transit, Transect> transects;

    public VoyageAcousticsImportDataContext(UserDbPersistenceService persistenceService, Locale locale, char csvSeparator, VoyageAcousticsImportConfiguration configuration, EchoBaseUser user, Date importDate) {
        super(persistenceService, locale, csvSeparator, configuration, user, importDate);
    }
    
    public final Voyage getVoyage() {
        if (voyage == null) {
            voyage = persistenceService.getVoyage(configuration.getVoyageId());
            
        }
        return voyage;
    }
    
    public final Map<Transit, Transect> getTransects() {
        if (transects == null) {
            transects = new HashMap<>();
            Vessel vessel = getVessel();
            
            Collection<Transit> transits = voyage.getTransit();
            for (Transit transit : transits) {
                Transect transect = persistenceService.getTransect(vessel, transit);
                transects.put(transit, transect);
            }
            
        }
        return transects;
    }
    
    public final Vessel getVessel() {
        if (vessel == null) {
            vessel = persistenceService.getVessel(configuration.getVesselId());
        }
        return vessel;
    }
    
    @Override
    public String getEntityId() {
        return configuration.getVoyageId();
    }

}
