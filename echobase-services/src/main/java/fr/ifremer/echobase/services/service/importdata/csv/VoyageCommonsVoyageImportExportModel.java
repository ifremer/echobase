/*
 * #%L
 * EchoBase :: Services
 * %%
 * Copyright (C) 2011 - 2012 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ifremer.echobase.services.service.importdata.csv;

import fr.ifremer.echobase.entities.data.Voyage;
import fr.ifremer.echobase.entities.data.VoyageImpl;
import fr.ifremer.echobase.entities.references.Port;
import fr.ifremer.echobase.services.csv.EchoBaseCsvUtil;
import fr.ifremer.echobase.services.service.importdata.contexts.VoyageCommonsImportDataContext;

/**
 * Model to import Voyages.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 0.3
 */
public class VoyageCommonsVoyageImportExportModel extends EchoBaseImportExportModelSupport<Voyage> {

    private VoyageCommonsVoyageImportExportModel(char separator) {
        super(separator);
    }

    public static VoyageCommonsVoyageImportExportModel forImport(VoyageCommonsImportDataContext importDataContext) {

        VoyageCommonsVoyageImportExportModel model = new VoyageCommonsVoyageImportExportModel(importDataContext.getCsvSeparator());
        model.newMandatoryColumn(Voyage.PROPERTY_NAME);
        model.newMandatoryColumn(Voyage.PROPERTY_START_DATE, EchoBaseCsvUtil.IMPORT_DAY_TIME_ECHOBASE);
        model.newMandatoryColumn(Voyage.PROPERTY_END_DATE, EchoBaseCsvUtil.IMPORT_DAY_TIME_ECHOBASE);
        model.newForeignKeyColumn(Voyage.PROPERTY_START_PORT, Voyage.PROPERTY_START_PORT, Port.class, Port.PROPERTY_CODE, importDataContext.getPortByCode());
        model.newForeignKeyColumn(Voyage.PROPERTY_END_PORT, Voyage.PROPERTY_END_PORT, Port.class, Port.PROPERTY_CODE, importDataContext.getPortByCode());
        return model;

    }

    public static VoyageCommonsVoyageImportExportModel forExport(VoyageCommonsImportDataContext importDataContext) {

        VoyageCommonsVoyageImportExportModel model = new VoyageCommonsVoyageImportExportModel(importDataContext.getCsvSeparator());
        model.newColumnForExport(Voyage.PROPERTY_NAME);
        model.newColumnForExport(Voyage.PROPERTY_START_DATE, EchoBaseCsvUtil.IMPORT_DAY_TIME_ECHOBASE);
        model.newColumnForExport(Voyage.PROPERTY_END_DATE, EchoBaseCsvUtil.IMPORT_DAY_TIME_ECHOBASE);
        model.newColumnForExport(Voyage.PROPERTY_START_PORT, Voyage.PROPERTY_START_PORT, EchoBaseCsvUtil.PORT_FORMATTER);
        model.newColumnForExport(Voyage.PROPERTY_END_PORT, Voyage.PROPERTY_END_PORT, EchoBaseCsvUtil.PORT_FORMATTER);
        return model;

    }

    @Override
    public Voyage newEmptyInstance() {
        return new VoyageImpl();
    }
}
