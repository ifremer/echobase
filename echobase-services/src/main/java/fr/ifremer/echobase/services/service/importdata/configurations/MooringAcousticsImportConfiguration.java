/*
 * #%L
 * EchoBase :: Services
 * %%
 * Copyright (C) 2011 - 2012 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ifremer.echobase.services.service.importdata.configurations;

import fr.ifremer.echobase.entities.ImportType;
import java.util.Locale;

/**
 * Configuration of a "accoustic datas" import for mooring.
 *
 * @author Julien Ruchaud - ruchaud@codelutin.com
 * @since 4.0
 */
public class MooringAcousticsImportConfiguration extends ImportAcousticsConfiguration {

    /** Selected mooring id where to import datas. */
    protected String mooringId;

    public MooringAcousticsImportConfiguration(Locale locale) {
        super(locale);
        importType = ImportType.MOORING_ACOUSTIC;
    }
    
    public String getMooringId() {
        return mooringId;
    }

    public void setMooringId(String mooringId) {
        this.mooringId = mooringId;
    }
}
