package fr.ifremer.echobase.services.service.importdata.actions;

/*
 * #%L
 * EchoBase :: Services
 * %%
 * Copyright (C) 2011 - 2016 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.echobase.entities.EchoBaseUserEntityEnum;
import fr.ifremer.echobase.entities.data.Category;
import fr.ifremer.echobase.entities.data.Cell;
import fr.ifremer.echobase.entities.data.Result;
import fr.ifremer.echobase.io.InputFile;
import fr.ifremer.echobase.services.csv.EchoBaseCsvUtil;
import fr.ifremer.echobase.services.csv.ResultAble;
import fr.ifremer.echobase.services.service.importdata.ImportDataFileResult;
import fr.ifremer.echobase.services.service.importdata.configurations.ImportDataConfigurationSupport;
import fr.ifremer.echobase.services.service.importdata.contexts.ImportDataContextSupport;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.LinkedList;
import java.util.List;

/**
 * @author Julien Ruchaud - ruchaud@codelutin.com
 * @since 4.0
 */
public abstract class ImportResultsDataActionSupport<M extends ImportDataConfigurationSupport, C extends ImportDataContextSupport<M>, E> extends ImportDataActionSupport<M, C, E> {

    /** Logger. */
    private static final Log log = LogFactory.getLog(ImportResultsDataActionSupport.class);

    public ImportResultsDataActionSupport(C importDataContext, InputFile inputFile) {
        super(importDataContext, inputFile);
    }

    protected List<Result> addResults(ResultAble row,
                                      Cell cell,
                                      Category category,
                                      String resultLabel,
                                      ImportDataFileResult importResult,
                                      boolean collectIds,
                                      boolean importNAResults, int rowNumber) {

        List<Result> results = new LinkedList<>();

        for (Result result : row.getResult()) {

            if (!importNAResults && EchoBaseCsvUtil.NA.equals(result.getResultValue())) {

                // Do not import NA results
                continue;
            }
            result.setDataQuality(row.getDataQuality());
            result.setCategory(category);
            result.setResultLabel(resultLabel);

            Result resultCreated = persistenceService.createResult(result);
            cell.addResult(resultCreated);

            if (collectIds) {

                addId(importResult,EchoBaseUserEntityEnum.Result, resultCreated, rowNumber);

            } else {

                importResult.incrementsNumberCreated(EchoBaseUserEntityEnum.Result);
            }

            results.add(resultCreated);

        }

        return results;
    }
}
