package fr.ifremer.echobase.services.service.spatial;

/*
 * #%L
 * EchoBase :: Services
 * %%
 * Copyright (C) 2011 - 2014 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.base.Charsets;
import com.google.common.base.Preconditions;
import com.google.common.io.Files;
import fr.ifremer.echobase.EchoBaseTechnicalException;
import fr.ifremer.echobase.config.EchoBaseConfiguration;
import fr.ifremer.echobase.entities.data.Voyage;
import fr.ifremer.echobase.io.EchoBaseIOUtil;
import fr.ifremer.echobase.persistence.JdbcConfiguration;
import fr.ifremer.echobase.services.EchoBaseServiceSupport;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.Scanner;

/**
 * Gis Service (to interact with a gis system).
 *
 * Created on 12/15/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.8
 */
public class GisService extends EchoBaseServiceSupport {

    /** Logger. */
    private static final Log log = LogFactory.getLog(GisService.class);

    private static final String[] TEMPLATE_MARKUP = {"{{dbname}}", "{{host}}", "{{port}}", "{{userName}}", "{{password}}", "{{voyageName}}", "{{voyageId}}", "{{resourcesPath}}"};

    private static final String GIS_INTERNAL_PATH = "/gis/templates/";

    private static final String QGIS_RESOURCES = "/gis/lizmap/resources/";

    public static void copyQgisDefaultTemplateFileIfNecessary(EchoBaseConfiguration configuration) throws IOException {

        File qgisDefaultTemplateFile = configuration.getQgisDefaultTemplateFile();

        if (!qgisDefaultTemplateFile.exists()) {

            String resourcePath = GisService.GIS_INTERNAL_PATH + qgisDefaultTemplateFile.getName();
            EchoBaseIOUtil.copyResource(resourcePath, qgisDefaultTemplateFile, false);

            if (log.isInfoEnabled()) {
                log.info("Copy qgis default template file to " + qgisDefaultTemplateFile);
            }

        }

    }

    public static void copyLizmapDefaultTemplateFileIfNecessary(EchoBaseConfiguration configuration) throws IOException {

        File lizmapTemplateFile = configuration.getLizmapDefaultTemplateFile();

        if (!lizmapTemplateFile.exists()) {

            String resourcePath = GisService.GIS_INTERNAL_PATH + lizmapTemplateFile.getName();
            EchoBaseIOUtil.copyResource(resourcePath, lizmapTemplateFile, false);

            if (log.isInfoEnabled()) {
                log.info("Copy lizmap default template file to " + lizmapTemplateFile);
            }

        }

    }

    public static void copyQgisResourcesIfNecessary(EchoBaseConfiguration configuration) throws IOException {

        File qgisResourcesDirectory = configuration.getQgisResourcesDirectory();

        if (!qgisResourcesDirectory.exists()) {

            URL resourcesUrl = GisService.class.getResource(QGIS_RESOURCES);
            //FIXME Oh my God! an url is not a file.
            File resources = new File(resourcesUrl.getFile());
            FileUtils.copyDirectory(resources, qgisResourcesDirectory);

            if (log.isInfoEnabled()) {
                log.info("Copy qgis resources to " + qgisResourcesDirectory);
            }
        }

    }

    /**
     * Add given working db to lizmap.
     *
     * @param conf data base connexion configuration
     */
    public void registerWorkingDb(JdbcConfiguration conf) {

        LizmapRepository lizmapRepository = newLizmapRepository(conf);
        lizmapRepository.register();

    }

    public String getVoyageMapUrl(JdbcConfiguration conf, Voyage voyage) {

        LizmapRepository lizmapRepository = newLizmapRepository(conf);
        return lizmapRepository.getVoyageMapUrl(voyage);

    }

    /**
     * Generate map files for this database and this voyage.
     *
     * @param conf   data base connexion configuration
     * @param voyage voyage for this map
     */
    public void generateMap(JdbcConfiguration conf, Voyage voyage) {

        Preconditions.checkNotNull(conf);
        Preconditions.checkNotNull(voyage);

        LizmapRepository lizmapRepository = newLizmapRepository(conf);

        File repositoryDirectory = lizmapRepository.getRepositoryDirectory();
        EchoBaseIOUtil.forceMkdir(repositoryDirectory);

        String[] templateValues = getTemplateValues(conf, voyage);

        generateFileFromTemplate(getConfiguration().getQgisTemplateFile(), lizmapRepository.getQGisFile(voyage), templateValues);

        generateFileFromTemplate(getConfiguration().getLizmapTemplateFile(), lizmapRepository.getLizmapFile(voyage), templateValues);

    }

    protected LizmapRepository newLizmapRepository(JdbcConfiguration conf) {
        return LizmapRepository.newLizmapRepository(getConfiguration(), conf);
    }

    /**
     * Generate file from template and values.
     *
     * @param templateFile   file template to used
     * @param target         file to generate
     * @param templateValues values to used in the template
     * @return file generated
     */
    protected File generateFileFromTemplate(File templateFile, File target, String... templateValues) {

        if (!target.exists()) {

            if (log.isInfoEnabled()) {
                log.info("Generate file: " + target);
            }

            try {

                Scanner scanner = new Scanner(templateFile, Charsets.UTF_8.name());

                try {

                    BufferedWriter bufferedWriter = Files.newWriter(target, Charsets.UTF_8);

                    try {

                        while (scanner.hasNextLine()) {

                            String line = scanner.nextLine();
                            line = StringUtils.replaceEach(line, TEMPLATE_MARKUP, templateValues);
                            bufferedWriter.write(line);
                            bufferedWriter.newLine();

                        }

                        bufferedWriter.close();

                    } finally {
                        IOUtils.closeQuietly(bufferedWriter);
                    }

                    scanner.close();

                } finally {
                    IOUtils.closeQuietly(scanner);
                }

            } catch (IOException e) {
                throw new EchoBaseTechnicalException(e);
            }
        }

        return target;

    }

    /**
     * Extract data value to used in template
     *
     * @param conf   data base connexion configuration
     * @param voyage voyage for this map
     * @return values table
     */
    public String[] getTemplateValues(JdbcConfiguration conf, Voyage voyage) {

        String[] templateValues = new String[TEMPLATE_MARKUP.length];

        String url = conf.getUrl();

        PgJdbcUrl jdbcUrl = new PgJdbcUrl(url);

        // dbname
        templateValues[0] = jdbcUrl.getDatabaseName();
        // host
        templateValues[1] = jdbcUrl.getHost();
        // port
        templateValues[2] = jdbcUrl.getPort();
        // userName
        templateValues[3] = conf.getLogin();
        // password
        templateValues[4] = conf.getPassword();
        // voyage name
        templateValues[5] = voyage.getName();
        // voyage id
        templateValues[6] = voyage.getTopiaId();
        // ressourcesPath
        templateValues[7] = getConfiguration().getQgisResourcesDirectory().getAbsolutePath();

        return templateValues;

    }

}
