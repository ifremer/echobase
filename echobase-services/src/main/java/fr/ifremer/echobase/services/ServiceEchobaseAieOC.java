package fr.ifremer.echobase.services;

/*
 * #%L
 * EchoBase :: Services
 * %%
 * Copyright (C) 2011 - 2014 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.echobase.entities.EchoBaseInternalPersistenceContext;
import fr.ifremer.echobase.entities.EchoBaseUserPersistenceContext;

import java.lang.reflect.Field;

/**
 * Created on 12/21/13.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.6
 */
public class ServiceEchobaseAieOC extends EchobaseAieOC {

    @Override
    protected Object toInject(EchoBaseServiceContext serviceContext,
                              Field field) throws ClassNotFoundException {

        Class<?> propertyType = field.getType();
        Object toInject = null;
        if (EchoBaseUserPersistenceContext.class.isAssignableFrom(propertyType)) {
            toInject = serviceContext.getEchoBaseUserPersistenceContext();

        }
        if (EchoBaseInternalPersistenceContext.class.isAssignableFrom(propertyType)) {
            toInject = serviceContext.getEchoBaseInternalPersistenceContext();

        } else if (EchoBaseServiceContext.class.isAssignableFrom(propertyType)) {
            toInject = serviceContext;

        } else if (EchoBaseService.class.isAssignableFrom(propertyType)) {

            Class<? extends EchoBaseService> serviceClass =
                    (Class<? extends EchoBaseService>) propertyType;
            toInject = serviceContext.newService(serviceClass);

        }
//        else if (TopiaDao.class.isAssignableFrom(propertyType)) {
//
//            Class<? extends TopiaDao> daoClass =
//                    (Class<? extends TopiaDao>) propertyType;
//
//            // need then also a @Named annotation with type of entity
//
//            Named named = field.getAnnotation(Named.class);
//            String entityClassFqn = named.value();
//            Class<TopiaEntity> entityClass = (Class<TopiaEntity>) Class.forName(entityClassFqn);
//            toInject = serviceContext.getEchoBaseUserPersistenceContext().getDao(entityClass, daoClass);
//        }

        return toInject;
    }
}
