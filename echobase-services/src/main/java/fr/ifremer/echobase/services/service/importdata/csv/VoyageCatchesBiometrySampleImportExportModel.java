/*
 * #%L
 * EchoBase :: Services
 * %%
 * Copyright (C) 2011 - 2012 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ifremer.echobase.services.service.importdata.csv;

import fr.ifremer.echobase.entities.data.Operation;
import fr.ifremer.echobase.entities.data.Sample;
import fr.ifremer.echobase.entities.data.SampleData;
import fr.ifremer.echobase.entities.references.SampleDataType;
import fr.ifremer.echobase.entities.references.SizeCategory;
import fr.ifremer.echobase.entities.references.Species;
import fr.ifremer.echobase.services.csv.EchoBaseCsvUtil;
import fr.ifremer.echobase.services.service.importdata.contexts.VoyageCatchesImportDataContext;

/**
 * Model to import {@link Sample} of total biometry.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 0.3
 */
public class VoyageCatchesBiometrySampleImportExportModel extends EchoBaseImportExportModelSupport<VoyageCatchesBiometrySampleImportRow> {

    private VoyageCatchesBiometrySampleImportExportModel(char separator) {
        super(separator);
    }

    public static VoyageCatchesBiometrySampleImportExportModel forImport(VoyageCatchesImportDataContext importDataContext) {
        VoyageCatchesBiometrySampleImportExportModel model = new VoyageCatchesBiometrySampleImportExportModel(importDataContext.getCsvSeparator());
        model.newForeignKeyColumn(EchoBaseCsvUtil.OPERATION_ID, VoyageCatchesBiometrySampleImportRow.PROPERTY_OPERATION, Operation.class, Operation.PROPERTY_ID, importDataContext.getVoyageOperationsById());
        model.newForeignKeyColumn(Species.PROPERTY_BARACOUDA_CODE, VoyageCatchesBiometrySampleImportRow.PROPERTY_SPECIES, Species.class, Species.PROPERTY_BARACOUDA_CODE, importDataContext.getSpeciesByBaracoudaCode());
        model.newForeignKeyColumn(VoyageCatchesBiometrySampleImportRow.PROPERTY_SIZE_CATEGORY, SizeCategory.class, SizeCategory.PROPERTY_NAME, importDataContext.getSizeCategoriesByName());

        model.newMandatoryColumn(VoyageCatchesBiometrySampleImportRow.PROPERTY_NUM_FISH, EchoBaseCsvUtil.PRIMITIVE_INTEGER);

        model.newForeignKeyColumn("name", SampleData.PROPERTY_SAMPLE_DATA_TYPE, SampleDataType.class, SampleDataType.PROPERTY_NAME, importDataContext.getSampleDataTypesByName());
        model.newMandatoryColumn(SampleData.PROPERTY_DATA_LABEL);
        model.newMandatoryColumn(SampleData.PROPERTY_DATA_VALUE, EchoBaseCsvUtil.NA_TO_FLOAT_PARSER_FORMATTER);
        return model;
    }

    public static VoyageCatchesBiometrySampleImportExportModel forExport(VoyageCatchesImportDataContext importDataContext) {
        VoyageCatchesBiometrySampleImportExportModel model = new VoyageCatchesBiometrySampleImportExportModel(importDataContext.getCsvSeparator());
        model.newColumnForExport(EchoBaseCsvUtil.OPERATION_ID, VoyageCatchesBiometrySampleImportRow.PROPERTY_OPERATION, EchoBaseCsvUtil.OPERATION_FORMATTER);
        model.newColumnForExport(Species.PROPERTY_BARACOUDA_CODE, VoyageCatchesBiometrySampleImportRow.PROPERTY_SPECIES, EchoBaseCsvUtil.SPECIES_FORMATTER);
        model.newColumnForExport(VoyageCatchesBiometrySampleImportRow.PROPERTY_SIZE_CATEGORY, EchoBaseCsvUtil.SIZE_CATEGORY_FORMATTER);
        model.newColumnForExport(VoyageCatchesBiometrySampleImportRow.PROPERTY_NUM_FISH, EchoBaseCsvUtil.PRIMITIVE_INTEGER);
        model.newColumnForExport("name", SampleData.PROPERTY_SAMPLE_DATA_TYPE, EchoBaseCsvUtil.SAMPLE_DATA_TYPE_FORMATTER);
        model.newColumnForExport(SampleData.PROPERTY_DATA_LABEL);
        model.newColumnForExport(SampleData.PROPERTY_DATA_VALUE, EchoBaseCsvUtil.NA_TO_FLOAT_PARSER_FORMATTER);
        return model;
    }

    @Override
    public VoyageCatchesBiometrySampleImportRow newEmptyInstance() {
        return new VoyageCatchesBiometrySampleImportRow();
    }
}
