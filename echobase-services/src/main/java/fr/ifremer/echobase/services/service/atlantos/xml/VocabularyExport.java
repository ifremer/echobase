package fr.ifremer.echobase.services.service.atlantos.xml;

/*
 * #%L
 * EchoBase :: Services
 * %%
 * Copyright (C) 2011 - 2016 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.echobase.entities.data.Category;
import fr.ifremer.echobase.entities.data.Echotype;
import fr.ifremer.echobase.entities.references.Species;
import fr.ifremer.echobase.services.EchoBaseService;
import fr.ifremer.echobase.services.service.UserDbPersistenceService;

import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.inject.Inject;

/**
 *
 * @author julien
 */
public class VocabularyExport implements EchoBaseService {

    public static final String VOCABULARY_PATTERN = "^(((AC_)|(ISO_)|(ICES_)|(Gear))?[^_]+)_(.+)";

    @Inject
    private UserDbPersistenceService persistenceService;
    
    protected Map<String, String> vocabularyTags;
    protected Map<String, String> vocabulary;
    protected Map<String, Set<String>> cache;

    //used to deal with echotypes in vocabulary export
    protected Map<String, Category> categories;

    protected boolean exportSpeciesCategory = true;
    
    protected XmlWriter writer;
    protected Pattern pattern;

    public void init(XmlWriter writer) {
        this.pattern = Pattern.compile(VOCABULARY_PATTERN);
        
        this.writer = writer;
        
        this.vocabularyTags = new HashMap<>();
        this.cache = new HashMap<>();
        //special mechanism for echotypes
        this.categories = new HashMap<>();
        
        this.vocabularyTags.put("AC_Survey", "Survey");
        this.vocabularyTags.put("ISO_3166", "Country");
        this.vocabularyTags.put("SHIPC", "Platform");
        this.vocabularyTags.put("EDMO", "Organisation");
        
        // For Accoustic
        this.vocabularyTags.put("AC_LogOrigin", "Origin");
        this.vocabularyTags.put("AC_LogValidity", "LogValidity");
        this.vocabularyTags.put("AC_AcquisitionMethod_SS", "AcquisitionMethod");
        this.vocabularyTags.put("AC_SaCategory", "SaCategory");
        this.vocabularyTags.put("AC_AcousticDataType", "Type");
        this.vocabularyTags.put("AC_DataUnit", "Unit");
        this.vocabularyTags.put("AC_PingAxisIntervalType", "PingAxisIntervalType");
        this.vocabularyTags.put("AC_PingAxisIntervalUnit", "PingAxisIntervalUnit");
        this.vocabularyTags.put("AC_PingAxisIntervalOrigin", "PingAxisIntervalOrigin");
        this.vocabularyTags.put("AC_TransducerLocation", "TransducerLocation");
        this.vocabularyTags.put("AC_TransducerBeamType", "TransducerBeamType");
        this.vocabularyTags.put("AC_AcquisitionMethod", "AcquisitionMethod");
        this.vocabularyTags.put("AC_ProcessingMethod", "ProcessingMethod");
        this.vocabularyTags.put("AC_DataAcquisitionSoftwareName", "DataAcquisitionSoftwareName");
        this.vocabularyTags.put("AC_StoredDataFormat", "StoredDataFormat");
        this.vocabularyTags.put("AC_DataProcessingSoftwareName", "DataProcessingSoftwareName");
        this.vocabularyTags.put("AC_TriwaveCorrection", "TriwaveCorrection");
        this.vocabularyTags.put("AC_OnAxisGainUnit", "OnAxisGainUnit");
        
        // For Biotic
        this.vocabularyTags.put("Gear", "Gear");
        this.vocabularyTags.put("AC_HaulValidity", "HaulValidity");
        this.vocabularyTags.put("AC_Stratum", "Stratum");
        this.vocabularyTags.put("AC_GearExceptions", "GearExceptions");
        this.vocabularyTags.put("AC_DoorType", "DoorType");
        this.vocabularyTags.put("AC_CatchDataType", "DataType");
        this.vocabularyTags.put("SpecWoRMS", "SpeciesCode");
        this.vocabularyTags.put("AC_SpeciesValidity", "SpeciesValidity");
        this.vocabularyTags.put("AC_CatchCategory", "SpeciesCategory");
        this.vocabularyTags.put("AC_Sex", "Sex");
        this.vocabularyTags.put("AC_LengthCode", "LengthCode");
        this.vocabularyTags.put("AC_LengthMeasurementType", "LengthType");
        this.vocabularyTags.put("ICES_StockCode", "StockCode");
        this.vocabularyTags.put("AC_WeightUnit", "WeightUnit");
        this.vocabularyTags.put("AC_MaturityCode", "Maturity");
        this.vocabularyTags.put("AC_MaturityScale", "MaturityScale");
        this.vocabularyTags.put("AC_AgePlusGroup", "AgePlusGroup");
        this.vocabularyTags.put("AC_AgeSource", "AgeSource");
        this.vocabularyTags.put("AC_SamplingFlag", "SamplingFlag");
        
        this.vocabulary = persistenceService.getVocabulary();
    }
    
    public void generate() throws IOException {

        //Special mechanism for echotypes
        for (Map.Entry<String, Category> entry: this.categories.entrySet()){
            exportEchotype(entry.getValue());
        }

        this.writer.open("Vocabulary");


        for (Map.Entry<String, Set<String>> entry : this.cache.entrySet()) {
            String tag = entry.getKey();
            Set<String> codes = entry.getValue();

            if (!tag.equals("SpeciesCategory") || exportSpeciesCategory) {

                this.writer.open(tag);

                for (String code : codes) {
                    Matcher matcher = this.pattern.matcher(code);
                    matcher.find();
                    String prefix = matcher.group(1);
                    String value = matcher.group(7);

                    this.writer.create("Code",
                            "ID", code,
                            "CodeType", "https://acoustic.ices.dk/Services/Schema/XML/" + prefix + ".xml",
                            value);
                }

                this.writer.close(tag);
            }
        }
        
        this.writer.close("Vocabulary");
    }

    /**
     * Method to get vocabulary code from VocabularyCIEM for a key (without prefix)
     * @param key key in VocabularyCIEM
     * @param defaultValue used if nothing in VocabularyCIEM
     * @return value from VocabularyCIEM (or default value if used)
     */
    public String getVocabularyCode(String key, String defaultValue) {
        if (key == null || key.trim().equals("")) {
            this.addVocabulary(defaultValue);
            return defaultValue;
        }
        
        String code = this.vocabulary.get(key.trim());
        if (code == null) {
            this.addVocabulary(defaultValue);
            return defaultValue;
        }
        
        this.addVocabulary(code);
        return code;
    }

    /**
     * Method to get vocabulary CIEM value for code. Should have prefix
     * @param code
     * @return
     */
    public String getVocabularyCode(String code) {
        this.addVocabulary(code);
        return code;
    }
    
    protected void addVocabulary(String code) {
        Matcher matcher = this.pattern.matcher(code);
        matcher.find();
        String prefix = matcher.group(1);
        
        String tag = this.vocabularyTags.get(prefix);
        
        Set<String> codes = this.cache.get(tag);
        if (codes == null) {
            codes = new LinkedHashSet<>();
            this.cache.put(tag, codes);
        }
        
        codes.add(code);
    }

    public void addCategory(Category category){
        this.categories.put(category.getEchotype().getName(), category);
    }

    public void exportEchotype(Category category) throws IOException {

        Echotype echotype = category.getEchotype();

        this.writer.open("EchoType",
                "ID", echotype.getName());
        this.writer.create("Stratum", "IDREF",
                getVocabularyCode("AC_Stratum_" + echotype.getDepthStratum().getId()));

        for (Species species : echotype.getSpecies()) {
            if (category.getSpeciesCategory() != null) {
                exportSpecies(species,
                        category.getSpeciesCategory().getSizeCategory().getName());
            } else {
                exportSpecies(species, null);
            }
        }

        this.writer.close("EchoType");
    }

    public void exportSpecies(Species species, String speciesCategoryName) throws IOException {
        this.writer.open("Species");
        this.writer.create("SpeciesCode", "IDREF",
                getVocabularyCode("SpecWoRMS_" + species.getWormsCode()));

        //We get speciesCategory vocabulary value to parse it, so no export for this one.
        String speciesCategoryCode = getVocabularyCode(speciesCategoryName,"AC_CatchCategory_1");
        setExportSpeciesCategory(false);
        this.writer.create("SpeciesCategory", speciesCategoryCode.charAt(speciesCategoryCode.length()-1));

        this.writer.close("Species");
    }

    public void setExportSpeciesCategory(boolean exportSpeciesCategory) {
        this.exportSpeciesCategory = exportSpeciesCategory;
    }
    
}
