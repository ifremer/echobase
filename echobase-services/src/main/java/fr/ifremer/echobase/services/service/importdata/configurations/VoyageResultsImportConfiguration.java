/*
 * #%L
 * EchoBase :: Services
 * %%
 * Copyright (C) 2011 - 2012 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ifremer.echobase.services.service.importdata.configurations;

import fr.ifremer.echobase.io.InputFile;

import java.util.Locale;

import static org.nuiton.i18n.I18n.l;

/**
 * Configuration of a "results" import.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 0.3
 */
public class VoyageResultsImportConfiguration extends ImportResultsConfigurationSupport {

    private static final long serialVersionUID = 1L;
    /** Acoustic result by echotype import. */
    protected final InputFile esduByEchotypeFile;
    /** Acoustic result by echotype and species category import. */
    protected final InputFile esduByEchotypeAndSpeciesCategoryFile;
    /** Acoustic result by species and size category import. */
    protected final InputFile esduByEchotypeandSpeciesCategoryAndLengthFile;
    /** Acoustic result by species and age category import. */
    protected final InputFile esduBySpeciesAndAgeCategoryFile;
    /** Region file to import. */
    protected final InputFile regionsFile;
    /** Region cell associations file to import. */
    protected final InputFile regionAssociationFile;
    /** Region cell results file to import. */
    protected final InputFile regionResultFile;
    /** Map cells file to import. */
    protected final InputFile mapsFile;
    /** Echotype  file to import. */
    protected final InputFile echotypeFile;
    /** LengthAgeKey file to import. */
    protected final InputFile lengthAgeKeyFile;
    /** LengthWeightKey file to import. */
    protected final InputFile lengthWeightKeyFile;
    /** Selected vessel id to find out transect where to import datas. */
    protected String vesselId;
    /** Selected voyage id where to import datas. */
    protected String voyageId;

    public VoyageResultsImportConfiguration(Locale locale) {
        regionsFile = InputFile.newFile(l(locale, "echobase.common.cellRegionsFile"));
        regionAssociationFile = InputFile.newFile(l(locale, "echobase.common.cellRegionAssociationFile"));
        regionResultFile = InputFile.newFile(l(locale, "echobase.common.cellRegionResultFile"));
        mapsFile = InputFile.newFile(l(locale, "echobase.common.cellMapsFile"));
        echotypeFile = InputFile.newFile(l(locale, "echobase.common.echotypeFile"));
        lengthAgeKeyFile = InputFile.newFile(l(locale, "echobase.common.lengthAgeKeyFile"));
        lengthWeightKeyFile = InputFile.newFile(l(locale, "echobase.common.lengthWeightKeyFile"));
        esduByEchotypeFile = InputFile.newFile(l(locale, "echobase.common.esduByEchotypeFile"));
        esduByEchotypeAndSpeciesCategoryFile = InputFile.newFile(l(locale, "echobase.common.esduByEchotypeAndSpeciesCategoryFile"));
        esduByEchotypeandSpeciesCategoryAndLengthFile = InputFile.newFile(l(locale, "echobase.common.esduByEchotypeAndSpeciesCategoryAndLengthFile"));
        esduBySpeciesAndAgeCategoryFile = InputFile.newFile(l(locale, "echobase.common.esduBySpeciesAndAgeCategoryFile"));
    }

    public final String getVoyageId() {
        return voyageId;
    }

    public final void setVoyageId(String voyageId) {
        this.voyageId = voyageId;
    }
    
    public String getVesselId() {
        return vesselId;
    }

    public void setVesselId(String vesselId) {
        this.vesselId = vesselId;
    }

    public InputFile getRegionsFile() {
        return regionsFile;
    }

    public InputFile getRegionAssociationFile() {
        return regionAssociationFile;
    }

    public InputFile getRegionResultFile() {
        return regionResultFile;
    }

    public InputFile getMapsFile() {
        return mapsFile;
    }

    public InputFile getEchotypeFile() {
        return echotypeFile;
    }

    public InputFile getLengthAgeKeyFile() {
        return lengthAgeKeyFile;
    }

    public InputFile getLengthWeightKeyFile() {
        return lengthWeightKeyFile;
    }

    public InputFile getEsduByEchotypeFile() {
        return esduByEchotypeFile;
    }

    public InputFile getEsduByEchotypeAndSpeciesCategoryFile() {
        return esduByEchotypeAndSpeciesCategoryFile;
    }

    public InputFile getEsduByEchotypeAndSpeciesCategoryAndLengthFile() {
        return esduByEchotypeandSpeciesCategoryAndLengthFile;
    }

    public InputFile getEsduBySpeciesAndAgeCategoryFile() {
        return esduBySpeciesAndAgeCategoryFile;
    }

    public boolean isOneEsduImportFile() {
        return esduByEchotypeFile.hasFile()
                || esduByEchotypeAndSpeciesCategoryFile.hasFile()
                || esduByEchotypeandSpeciesCategoryAndLengthFile.hasFile()
                || esduBySpeciesAndAgeCategoryFile.hasFile();

    }

    public boolean isOneVoyageImportFile() {
        return echotypeFile.hasFile() || lengthAgeKeyFile.hasFile() || lengthWeightKeyFile.hasFile();

    }

    @Override
    public InputFile[] getInputFiles() {
        return new InputFile[]{regionsFile,
                regionAssociationFile,
                regionResultFile,
                mapsFile, echotypeFile,
                lengthAgeKeyFile,
                lengthWeightKeyFile,
                esduByEchotypeFile,
                esduByEchotypeAndSpeciesCategoryFile,
                esduByEchotypeandSpeciesCategoryAndLengthFile,
                esduBySpeciesAndAgeCategoryFile};
    }
}
