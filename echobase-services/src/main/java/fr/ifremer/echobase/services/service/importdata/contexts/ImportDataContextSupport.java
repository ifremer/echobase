package fr.ifremer.echobase.services.service.importdata.contexts;

/*
 * #%L
 * EchoBase :: Services
 * %%
 * Copyright (C) 2011 - 2016 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.Maps;
import fr.ifremer.echobase.entities.EchoBaseUser;
import fr.ifremer.echobase.entities.ImportLog;
import fr.ifremer.echobase.entities.references.AcousticInstrument;
import fr.ifremer.echobase.entities.references.AcousticInstruments;
import fr.ifremer.echobase.entities.references.AgeCategories;
import fr.ifremer.echobase.entities.references.AgeCategory;
import fr.ifremer.echobase.entities.references.AncillaryInstrumentation;
import fr.ifremer.echobase.entities.references.AncillaryInstrumentations;
import fr.ifremer.echobase.entities.references.CellType;
import fr.ifremer.echobase.entities.references.CellTypes;
import fr.ifremer.echobase.entities.references.DataMetadata;
import fr.ifremer.echobase.entities.references.DataMetadatas;
import fr.ifremer.echobase.entities.references.DataQualities;
import fr.ifremer.echobase.entities.references.DataQuality;
import fr.ifremer.echobase.entities.references.DepthStratum;
import fr.ifremer.echobase.entities.references.DepthStratums;
import fr.ifremer.echobase.entities.references.Gear;
import fr.ifremer.echobase.entities.references.GearMetadata;
import fr.ifremer.echobase.entities.references.GearMetadatas;
import fr.ifremer.echobase.entities.references.Mission;
import fr.ifremer.echobase.entities.references.Missions;
import fr.ifremer.echobase.entities.references.OperationMetadata;
import fr.ifremer.echobase.entities.references.OperationMetadatas;
import fr.ifremer.echobase.entities.references.Port;
import fr.ifremer.echobase.entities.references.Ports;
import fr.ifremer.echobase.entities.references.SampleDataType;
import fr.ifremer.echobase.entities.references.SampleDataTypes;
import fr.ifremer.echobase.entities.references.SexCategories;
import fr.ifremer.echobase.entities.references.SexCategory;
import fr.ifremer.echobase.entities.references.SizeCategories;
import fr.ifremer.echobase.entities.references.SizeCategory;
import fr.ifremer.echobase.entities.references.Species;
import fr.ifremer.echobase.entities.references.Species2;
import fr.ifremer.echobase.entities.references.Strata;
import fr.ifremer.echobase.entities.references.Stratas;
import fr.ifremer.echobase.entities.references.Vessel;
import fr.ifremer.echobase.entities.references.Vessels;
import fr.ifremer.echobase.services.service.UserDbPersistenceService;
import fr.ifremer.echobase.services.service.importdata.ImportDataFileResult;
import fr.ifremer.echobase.services.service.importdata.configurations.ImportDataConfigurationSupport;
import org.apache.commons.lang3.StringUtils;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

/**
 * Created on 30/03/16.
 *
 * @author Tony Chemit - chemit@codelutin.com
 */
public abstract class ImportDataContextSupport<C extends ImportDataConfigurationSupport> {

    protected final UserDbPersistenceService persistenceService;
    protected final char csvSeparator;
    protected final C configuration;
    private final Locale locale;
    private final EchoBaseUser user;
    private final ImportLog importLog;

    // Referential
    private Map<String, Vessel> vesselsByName;
    private Map<String, Gear> gearsByCasinoGearName;
    private Map<String, DepthStratum> depthStratumsById;
    private Map<String, OperationMetadata> operationMetadatasByName;
    private Map<String, GearMetadata> gearMetadatasByName;
    private Map<String, DataMetadata> dataMetadatasByName;
    private Map<String, Species> speciesByBaracoudaCode;
    private Map<String, SampleDataType> sampleDataTypesByName;
    private Map<String, SizeCategory> sizeCategoriesByName;
    private Map<String, SexCategory> sexCategoriesByName;
    private Map<String, AcousticInstrument> instrumentsById;
    private Map<String, DataQuality> dataQualitiesByName;
    private Map<String, Strata> stratasByName;
    private Map<String, CellType> regionCellTypesById;
    private Map<String, AgeCategory> ageCategoriesByName;
    private Map<String, Port> portsByCode;
    private Map<String, Mission> missionByName;
    private Map<String, AncillaryInstrumentation> ancillaryInstrumentationsByName;

    private CellType esduCellType;
    private CellType elementaryCellType;
    private CellType mapCellType;
    private final LinkedList<ImportDataFileResult> results;

    ImportDataContextSupport(UserDbPersistenceService persistenceService, Locale locale, char csvSeparator, C configuration, EchoBaseUser user, Date importDate) {
        this.persistenceService = persistenceService;
        this.locale = locale;
        this.csvSeparator = csvSeparator;
        this.configuration = configuration;
        this.user = user;
        this.importLog = createImportLog(importDate);
        this.results = new LinkedList<>();
    }

    public final ImportLog getImportLog() {
        return importLog;
    }

    public final Map<String, AgeCategory> getAgeCategoriesByName() {
        if (ageCategoriesByName == null) {
            ageCategoriesByName = new TreeMap<>();
            ageCategoriesByName.putAll(persistenceService.getEntitiesMap(AgeCategory.class, AgeCategories.AGE_CATEGORY_NAME));
        }
        return ageCategoriesByName;
    }

    public final Map<String, CellType> getRegionCellTypesById() {
        if (regionCellTypesById == null) {
            Set<CellType> allCellTypes = persistenceService.getRegionCellTypes();

            // authorize only to use region* cell types
            regionCellTypesById = Maps.uniqueIndex(allCellTypes, CellTypes.CELL_TYPE_ID);
        }
        return regionCellTypesById;
    }

    public final CellType getEsduCellType() {
        if (esduCellType == null) {
            esduCellType = persistenceService.getEsduCellType();
        }
        return esduCellType;
    }

    public final CellType getElementaryCellType() {
        if (elementaryCellType == null) {
            elementaryCellType = persistenceService.getElementaryCellType();
        }
        return elementaryCellType;
    }

    public final CellType getMapCellType() {
        if (mapCellType == null) {
            mapCellType = persistenceService.getMapCellType();
        }
        return mapCellType;
    }

    public final Map<String, Strata> getStratasByName() {
        if (stratasByName == null) {
            stratasByName = persistenceService.getEntitiesMap(Strata.class, Stratas.STRATA_BY_NAME);
        }
        return stratasByName;
    }

    public final Map<String, AcousticInstrument> getInstrumentsById() {
        if (instrumentsById == null) {
            instrumentsById = persistenceService.getEntitiesMap(AcousticInstrument.class, AcousticInstruments.ACOUSTIC_INSTRUMENT_ID);
        }
        return instrumentsById;
    }

    public final Map<String, DataQuality> getDataQualitiesByName() {
        if (dataQualitiesByName == null) {
            dataQualitiesByName = persistenceService.getEntitiesMap(DataQuality.class, DataQualities.DATA_QUALITY_NAME);
        }
        return dataQualitiesByName;
    }

    public final Map<String, SexCategory> getSexCategoriesByName() {
        if (sexCategoriesByName == null) {
            sexCategoriesByName = persistenceService.getEntitiesMap(SexCategory.class, SexCategories.SEX_CATEGORY_NAME);
        }
        return sexCategoriesByName;
    }

    public final Map<String, SizeCategory> getSizeCategoriesByName() {
        if (sizeCategoriesByName == null) {
            sizeCategoriesByName = persistenceService.getEntitiesMap(SizeCategory.class, SizeCategories.SIZE_CATEGORY_NAME);
        }
        return sizeCategoriesByName;
    }

    public final Map<String, Species> getSpeciesByBaracoudaCode() {
        if (speciesByBaracoudaCode == null) {
            speciesByBaracoudaCode = persistenceService.getEntitiesMap(Species.class, Species2.SPECIES_BARACOUDA_CODE);
        }
        return speciesByBaracoudaCode;
    }

    public final Map<String, SampleDataType> getSampleDataTypesByName() {
        if (sampleDataTypesByName == null) {
            sampleDataTypesByName = persistenceService.getEntitiesMap(SampleDataType.class, SampleDataTypes.SAMPLE_DATA_TYPE_NAME);
        }
        return sampleDataTypesByName;
    }

    public final Map<String, GearMetadata> getGearMetadatasByName() {
        if (gearMetadatasByName == null) {
            gearMetadatasByName = persistenceService.getEntitiesMap(GearMetadata.class, GearMetadatas.GEAR_METADATA_NAME);
        }
        return gearMetadatasByName;
    }

    public final Map<String, OperationMetadata> getOperationMetadatasByName() {
        if (operationMetadatasByName == null) {
            operationMetadatasByName = persistenceService.getEntitiesMap(OperationMetadata.class, OperationMetadatas.OPERATION_METADATA_NAME);
        }
        return operationMetadatasByName;
    }

    public final Map<String, Gear> getGearsByCasinoGearName() {
        if (gearsByCasinoGearName == null) {
            gearsByCasinoGearName = persistenceService.getEntitiesMap(Gear.class, Gear::getCasinoGearName);
        }
        return gearsByCasinoGearName;
    }

    public final Map<String, DepthStratum> getDepthStratumsById() {
        if (depthStratumsById == null) {
            depthStratumsById = persistenceService.getEntitiesMap(DepthStratum.class, DepthStratums.DEPTH_STRATUM_ID);
        }
        return depthStratumsById;
    }

    public final Map<String, Vessel> getVesselsByName() {
        if (vesselsByName == null) {
            vesselsByName = persistenceService.getEntitiesMap(Vessel.class, Vessels.VESSEL_NAME);
        }
        return vesselsByName;
    }

    public final Map<String, Port> getPortByCode() {
        if (portsByCode == null) {
            portsByCode = persistenceService.getEntitiesMap(Port.class, Ports.PORT_CODE);
        }
        return portsByCode;
    }

    public Map<String, Mission> getMissionByName() {
        if (missionByName == null) {
            missionByName = persistenceService.getEntitiesMap(Mission.class, Missions.MISSION_NAME);
        }
        return missionByName;
    }
    
    public final Map<String, DataMetadata> getDataMetadatasByName() {
        if (dataMetadatasByName == null) {
            dataMetadatasByName = persistenceService.getEntitiesMap(DataMetadata.class, DataMetadatas.DATA_METADATA_NAME);
        }
        return dataMetadatasByName;
    }
    
    public final Map<String, AncillaryInstrumentation> getAncillaryInstrumentationsByName() {
        if (ancillaryInstrumentationsByName == null) {
            ancillaryInstrumentationsByName = persistenceService.getEntitiesMap(AncillaryInstrumentation.class, AncillaryInstrumentations.ANCILLARY_INSTRUMENTATION_NAME);
        }
        return ancillaryInstrumentationsByName;
    }

    public final Locale getLocale() {
        return locale;
    }

    public final char getCsvSeparator() {
        return csvSeparator;
    }

    public final C getConfiguration() {
        return configuration;
    }

    public final EchoBaseUser getUser() {
        return user;
    }

    public final UserDbPersistenceService getPersistenceService() {
        return persistenceService;
    }

    public void addResult(ImportDataFileResult result) {
        this.results.add(result);
    }

    public List<ImportDataFileResult> getResults() {
        return results;
    }

    protected ImportLog createImportLog(Date importDate) {

        StringBuilder importText = new StringBuilder();

        if (StringUtils.isNotEmpty(configuration.getImportNotes())) {
            importText.append(configuration.getImportNotes());
        }
        
        ImportLog importLog = persistenceService.createImportLog(configuration.getImportType(),
                user.getEmail(),
                importDate,
                importText.toString());

        importLog.setEntityId(getEntityId());
        return importLog;
    }
    
    public abstract String getEntityId();

}
