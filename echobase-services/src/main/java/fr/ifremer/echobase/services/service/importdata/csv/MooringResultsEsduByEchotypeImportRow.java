/*
 * #%L
 * EchoBase :: Services
 * %%
 * Copyright (C) 2011 - 2012 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ifremer.echobase.services.service.importdata.csv;

import fr.ifremer.echobase.entities.data.DataAcousticProvider;
import fr.ifremer.echobase.entities.data.Category;
import fr.ifremer.echobase.entities.data.Cell;
import fr.ifremer.echobase.entities.data.Echotype;
import fr.ifremer.echobase.entities.data.Mooring;
import fr.ifremer.echobase.entities.data.Result;
import fr.ifremer.echobase.entities.data.Voyage;
import fr.ifremer.echobase.entities.references.DataQuality;
import fr.ifremer.echobase.services.csv.CellAble;
import fr.ifremer.echobase.services.csv.ResultAble;

import java.util.LinkedList;
import java.util.List;

/**
 * Bean used as a row for import of {@link MooringResultsEsduByEchotypeImportExportModel}.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 0.3
 */
public class MooringResultsEsduByEchotypeImportRow implements ResultAble, CellAble<Mooring> {

    public static final String PROPERTY_MOORING = "mooring";
    public static final String PROPERTY_ECHOTYPE = "echotype";
    public static final String PROPERTY_CELL = "cell";
    public static final String PROPERTY_DATA_QUALITY = "dataQuality";

    protected Mooring mooring;
    protected Cell cell;
    protected Echotype echotype;
    protected final List<Result> result = new LinkedList<>();
    protected DataQuality dataQuality;

    public static MooringResultsEsduByEchotypeImportRow of(DataAcousticProvider provider, Cell cell, Category category, List<Result> cellResults) {
        MooringResultsEsduByEchotypeImportRow row = new MooringResultsEsduByEchotypeImportRow();
        row.setProvider(provider);
        row.setCell(cell);
        row.setEchotype(category.getEchotype());
        row.result.addAll(cellResults);
        row.setDataQuality(cellResults.get(0).getDataQuality());
        return row;
    }

    public Mooring getMooring() {
        return mooring;
    }

    public void setMooring(Mooring mooring) {
        this.mooring = mooring;
    }

    @Override
    public DataAcousticProvider<Mooring> getProvider() {
        return mooring;
    }

    @Override
    public void setProvider(DataAcousticProvider<Mooring> provider) {
        this.mooring = provider.getEntity();
    }

    @Override
    public Cell getCell() {
        return cell;
    }

    @Override
    public void setCell(Cell cell) {
        this.cell = cell;
    }

    @Override
    public List<Result> getResult() {
        return result;
    }

    @Override
    public void addResult(Result result) {
        this.result.add(result);
    }

    @Override
    public DataQuality getDataQuality() {
        return dataQuality;
    }

    @Override
    public void setDataQuality(DataQuality dataQuality) {
        this.dataQuality = dataQuality;
    }

    public Echotype getEchotype() {
        return echotype;
    }

    public void setEchotype(Echotype echotype) {
        this.echotype = echotype;
    }
}
