package fr.ifremer.echobase.services.service.exportCoser;

/*
 * #%L
 * EchoBase :: Services
 * %%
 * Copyright (C) 2011 - 2014 Ifremer, Codelutin, Chemit Tony
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.echobase.persistence.JdbcConfiguration;
import fr.ifremer.echobase.services.AbstractEchobaseActionConfiguration;
import fr.ifremer.echobase.services.ProgressModel;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.util.Set;

/**
 * Created on 3/1/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.6
 */
public class ExportCoserConfiguration extends AbstractEchobaseActionConfiguration {

    private static final long serialVersionUID = 1L;

    /**
     * Working directory.
     */
    private File workingDirectory;

    /**
     * Final export file.
     */
    private File exportFile;

    /**
     * Id of voyage to export.
     */
    private String missionId;

    /**
     * Name of the Coser facade.
     */
    private String facade;

    /**
     * Name of the coser zone.
     */
    private String zone;


    /**
     * Ids of community indicators to extract.
     */
    private Set<String> communityIndicator;

    /**
     * Ids of population indicators to extract.
     */
    private Set<String> populationIndicator;

    /**
     * Is the project is publishable by Coser?
     */
    private boolean publishable;

    /**
     * Should we produce raw data?
     */
    private boolean extractRawData;

    /**
     * Should we produce maps?
     */
    private boolean extractMap;

    /**
     * Should we produce community indicators?
     */
    private boolean extractCommunityIndicator;

    /**
     * Should we produce population indicators?
     */
    private boolean extractPopulationIndicator;

    /**
     * Name of user performing the action.
     */
    private String userName;

    /**
     * Comment.
     */
    private String comment;

    /**
     * Database configuration.
     */
    private JdbcConfiguration dbConfiguration;

    private String scriptErrorLog;

    public ExportCoserConfiguration() {
    }

    public ExportCoserConfiguration(ProgressModel progressModel) {
        super(progressModel);
    }

    public File getWorkingDirectory() {
        return workingDirectory;
    }

    public void setWorkingDirectory(File workingDirectory) {
        this.workingDirectory = workingDirectory;
    }

    public File getExportFile() {
        return exportFile;
    }

    public void setExportFile(File exportFile) {
        this.exportFile = exportFile;
    }

    public String getFacade() {
        return facade;
    }

    public void setFacade(String facade) {
        this.facade = facade;
    }

    public String getZone() {
        return zone;
    }

    public void setZone(String zone) {
        this.zone = zone;
    }

    public String getMissionId() {
        return missionId;
    }

    public void setMissionId(String missionId) {
        this.missionId = missionId;
    }

    public JdbcConfiguration getDbConfiguration() {
        return dbConfiguration;
    }

    public void setDbConfiguration(JdbcConfiguration dbConfiguration) {
        this.dbConfiguration = dbConfiguration;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public boolean isPublishable() {
        return publishable;
    }

    public void setPublishable(boolean publishable) {
        this.publishable = publishable;
    }

    public Set<String> getCommunityIndicator() {
        return communityIndicator;
    }

    public void setCommunityIndicator(Set<String> communityIndicator) {
        this.communityIndicator = communityIndicator;
    }

    public Set<String> getPopulationIndicator() {
        return populationIndicator;
    }

    public void setPopulationIndicator(Set<String> populationIndicator) {
        this.populationIndicator = populationIndicator;
    }

    public boolean isExtractRawData() {
        return extractRawData;
    }

    public void setExtractRawData(boolean extractRawData) {
        this.extractRawData = extractRawData;
    }

    public boolean isExtractMap() {
        return extractMap;
    }

    public void setExtractMap(boolean extractMap) {
        this.extractMap = extractMap;
    }

    public boolean isExtractCommunityIndicator() {
        return extractCommunityIndicator;
    }

    public void setExtractCommunityIndicator(boolean extractCommunityIndicator) {
        this.extractCommunityIndicator = extractCommunityIndicator;
    }

    public boolean isExtractPopulationIndicator() {
        return extractPopulationIndicator;
    }

    public void setExtractPopulationIndicator(boolean extractPopulationIndicator) {
        this.extractPopulationIndicator = extractPopulationIndicator;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getScriptErrorLog() {
        return scriptErrorLog;
    }

    public void setScriptErrorLog(String scriptErrorLog) {
        this.scriptErrorLog = scriptErrorLog;
    }

    @Override
    public void destroy() throws IOException {
        if (workingDirectory != null) {
            FileUtils.deleteDirectory(workingDirectory);
        }
    }
}
