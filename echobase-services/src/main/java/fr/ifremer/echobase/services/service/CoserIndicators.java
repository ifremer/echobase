package fr.ifremer.echobase.services.service;

/*
 * #%L
 * EchoBase :: Services
 * %%
 * Copyright (C) 2011 - 2015 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.echobase.entities.references.DataMetadata;

import java.util.List;

/**
* Created on 1/21/15.
*
* @author Tony Chemit - chemit@codelutin.com
* @since 2.9
*/
public class CoserIndicators {

    final List<DataMetadata> communityDataMetadatasList;

    final List<DataMetadata> populationDataMetadatasList;

    public CoserIndicators(List<DataMetadata> communityDataMetadatasList, List<DataMetadata> populationDataMetadatasList) {
        this.communityDataMetadatasList = communityDataMetadatasList;
        this.populationDataMetadatasList = populationDataMetadatasList;
    }

    public List<DataMetadata> getCommunityDataMetadatasList() {
        return communityDataMetadatasList;
    }

    public List<DataMetadata> getPopulationDataMetadatasList() {
        return populationDataMetadatasList;
    }

}
