package fr.ifremer.echobase.services.service.importdata.actions;

/*
 * #%L
 * EchoBase :: Services
 * %%
 * Copyright (C) 2011 - 2016 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.echobase.entities.EchoBaseUserEntityEnum;
import fr.ifremer.echobase.entities.data.Voyage;
import fr.ifremer.echobase.entities.references.AreaOfOperation;
import fr.ifremer.echobase.entities.references.Mission;
import fr.ifremer.echobase.io.InputFile;
import fr.ifremer.echobase.services.service.UserDbPersistenceService;
import fr.ifremer.echobase.services.service.importdata.DuplicatedVoyageException;
import fr.ifremer.echobase.services.service.importdata.ImportDataFileResult;
import fr.ifremer.echobase.services.service.importdata.MoreThanOnceVoyageToImportException;
import fr.ifremer.echobase.services.service.importdata.NoVoyageToImportException;
import fr.ifremer.echobase.services.service.importdata.contexts.VoyageCommonsImportDataContext;
import fr.ifremer.echobase.services.service.importdata.csv.VoyageCommonsVoyageImportExportModel;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.csv.Import;

import java.util.Locale;

/**
 * Created on 25/03/16.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 4.0
 */
public class VoyageCommonsVoyageImportAction extends VoyageCommonsImportDataActionSupport<Voyage> {

    /** Logger. */
    private static final Log log = LogFactory.getLog(VoyageCommonsVoyageImportAction.class);

    public VoyageCommonsVoyageImportAction(VoyageCommonsImportDataContext importDataContext) {
        super(importDataContext, importDataContext.getConfiguration().getVoyageFile());
    }

    @Override
    protected VoyageCommonsVoyageImportExportModel createCsvImportModel(VoyageCommonsImportDataContext importDataContext) {
        return VoyageCommonsVoyageImportExportModel.forImport(importDataContext);
    }

    @Override
    protected VoyageCommonsVoyageImportExportModel createCsvExportModel(VoyageCommonsImportDataContext importDataContext) {
        return VoyageCommonsVoyageImportExportModel.forExport(importDataContext);
    }

    @Override
    public void performImport(VoyageCommonsImportDataContext importDataContext, InputFile inputFile, ImportDataFileResult result) {


        if (log.isInfoEnabled()) {
            log.info("Starts import of voyages from file " + inputFile.getFileName());
        }

        UserDbPersistenceService persistenceService = getPersistenceService();
        Mission mission = persistenceService.getMission(importDataContext.getConfiguration().getMissionId());
        AreaOfOperation areaOfOperation = persistenceService.getAreaOfOperation(importDataContext.getConfiguration().getAreaOfOperationId());

        String voyageDescription = getConfiguration().getVoyageDescription();
        String datum = getConfiguration().getDatum();
        Locale locale = getLocale();

        Voyage newVoyage = null;

        try (Import<Voyage> importer = open()) {

            incrementsProgress();

            int rowNumber = 0;

            for (Voyage voyage : importer) {

                doFlushTransaction(++rowNumber);

                if (persistenceService.containsVoyageByName(voyage.getName())) {
                    throw new DuplicatedVoyageException(locale, rowNumber, voyage.getName());
                }

                voyage.setMission(mission);
                voyage.setAreaOfOperation(areaOfOperation);
                voyage.setDescription(voyageDescription);
                voyage.setDatum(datum);

                Voyage createdVoyage = persistenceService.createVoyage(voyage);
                if (newVoyage == null) {
                    newVoyage = createdVoyage;
                } else {
                    // this means a voyage file with more than one row not possible...

                    throw new MoreThanOnceVoyageToImportException(locale);
                }

                // collect id of the import
                addId(result, EchoBaseUserEntityEnum.Voyage, voyage, rowNumber);

                addProcessedRow(result, createdVoyage);

            }

        }

        if (newVoyage == null) {
            throw new NoVoyageToImportException(locale);
        }

        // push back to id of the voyage in configuration for next imports
        getConfiguration().setVoyageId(newVoyage.getTopiaId());
        importDataContext.getImportLog().setEntityId(newVoyage.getTopiaId());

    }

    @Override
    protected void computeImportedExport(VoyageCommonsImportDataContext importDataContext, ImportDataFileResult result) {

        for (Voyage voyage : getImportedEntities(Voyage.class, result)) {

            String voyageId = voyage.getTopiaId();
            if (log.isInfoEnabled()) {
                log.info("Adding voyage: " + voyageId + " to imported export.");
            }

            addImportedRow(result, voyage);

        }

    }

}
