package fr.ifremer.echobase.services.service.importdata.actions;

/*
 * #%L
 * EchoBase :: Services
 * %%
 * Copyright (C) 2011 - 2016 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.echobase.EchoBaseTechnicalException;
import fr.ifremer.echobase.entities.EchoBaseUser;
import fr.ifremer.echobase.entities.EchoBaseUserEntityEnum;
import fr.ifremer.echobase.entities.ImportFile;
import fr.ifremer.echobase.entities.ImportFileId;
import fr.ifremer.echobase.entities.data.Cell;
import fr.ifremer.echobase.entities.data.Data;
import fr.ifremer.echobase.entities.references.DataMetadata;
import fr.ifremer.echobase.entities.references.DataQuality;
import fr.ifremer.echobase.io.EchoBaseIOUtil;
import fr.ifremer.echobase.io.InputFile;
import fr.ifremer.echobase.services.csv.BatchCsvExport;
import fr.ifremer.echobase.services.service.UserDbPersistenceService;
import fr.ifremer.echobase.services.service.importdata.CheckFileException;
import fr.ifremer.echobase.services.service.importdata.ImportDataFileResult;
import fr.ifremer.echobase.services.service.importdata.ImportException;
import fr.ifremer.echobase.services.service.importdata.configurations.ImportDataConfigurationSupport;
import fr.ifremer.echobase.services.service.importdata.contexts.ImportDataContextSupport;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.csv.ExportModel;
import org.nuiton.csv.Import;
import org.nuiton.csv.ImportModel;
import org.nuiton.topia.persistence.TopiaEntity;

import java.io.BufferedWriter;
import java.io.Closeable;
import java.io.File;
import java.io.IOException;
import java.io.Reader;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.Locale;

import static org.nuiton.i18n.I18n.l;

/**
 * Created on 25/03/16.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @param <M> Configuration from client
 * @param <C> Database context
 * @param <E> Model of the lines in cvs
 */
public abstract class ImportDataActionSupport<M extends ImportDataConfigurationSupport, C extends ImportDataContextSupport<M>, E> implements Closeable {

    /** Logger. */
    private static final Log log = LogFactory.getLog(ImportDataActionSupport.class);

    /**
     * Max number of rows before flushing transaction.
     */
    protected static final int TRANSACTION_FLUSH_MAX = 1000;
    /**
     * Max number of rows before flushing export writer.
     */
    protected static final int EXPORT_FLUSH_MAX = 1000;
    /**
     * Persistence service used for any db actions.
     */
    protected final UserDbPersistenceService persistenceService;
    protected final ImportFile importFile;
    /**
     * Import data context.
     */
    private final C importDataContext;
    /**
     * Input file to import.
     */
    private final InputFile inputFile;
    /**
     * Processed rows to export.
     */
    private final LinkedList<E> processedRowsToExport;
    /**
     * Imported rows to export.
     */
    private final LinkedList<E> importedRowsToExport;
    /**
     * Imported entities id to persis.
     */
    private final LinkedList<ImportFileId> importedFileIdsToPersist;
    /**
     * The import model.
     */
    private ImportModel<E> importModel;
    /**
     * Reader of data to import.
     */
    private Reader reader;
    /**
     * Import of data.
     */
    private Import<E> csvImport;
    /**
     * Export of processed data.
     */
    private BatchCsvExport<E> csvProcessedExport;
    /**
     * Export of imported data.
     */
    private BatchCsvExport<E> csvImportedExport;

    protected ImportDataActionSupport(C importDataContext, InputFile inputFile) {
        this.importDataContext = importDataContext;
        this.inputFile = inputFile;
        this.persistenceService = importDataContext.getPersistenceService();
        this.processedRowsToExport = new LinkedList<>();
        this.importedRowsToExport = new LinkedList<>();
        this.importedFileIdsToPersist = new LinkedList<>();
        this.importFile = persistenceService.createImportFile(inputFile);
        if (log.isInfoEnabled()) {
            try {
                log.info("Add ImportFile: " + importFile.getName() + " - size: " + importFile.getFile().length());
            } catch (SQLException e) {
                throw new EchoBaseTechnicalException("Can't get length of importFile " + importFile.getName());
            }
        }
        importDataContext.getImportLog().addImportFile(importFile);

        // flush after adding incoming file to database
        getPersistenceService().flush();

    }

    /**
     * Creates a csv import model.
     *
     * @param importDataContext import data context
     * @return the csv import model
     */
    protected abstract ImportModel<E> createCsvImportModel(C importDataContext);

    /**
     * Creates a csv export model.
     *
     * @param importDataContext import data context used to get some stuff from database.
     * @return the csv export model
     */
    protected abstract ExportModel<E> createCsvExportModel(C importDataContext);

    /**
     * Logical code of import action.
     *
     * @param importDataContext import data context (everything coming from database should be in there)
     * @param inputFile         the imput file to import
     * @param result            where to store import result
     */
    protected abstract void performImport(C importDataContext, InputFile inputFile, ImportDataFileResult result);

    /**
     * Compute export file from imported data.
     *
     * @param importDataContext import data context (everything coming from database should be in there)
     * @param result            where to store import result
     */
    protected abstract void computeImportedExport(C importDataContext, ImportDataFileResult result);

    public final void run() throws ImportException {

        this.importModel = createCsvImportModel(importDataContext);

        ImportDataFileResult result = new ImportDataFileResult(importFile);
        importDataContext.addResult(result);
        this.csvProcessedExport = createBatchCsvExport(result.getProcessedImportFile());
        this.csvImportedExport = createBatchCsvExport(result.getImportedExportFile());

        try {
            performImport(importDataContext, inputFile, result);
        } catch (Exception e) {
            throw new ImportException(importDataContext.getLocale(), inputFile, e);
        } finally {
            flushProcessedExport(result);
            flushImportFileIds(result);
        }

        // flush after import (to get all imported ids in database)
        getPersistenceService().flush();

        try {
            computeImportedExport(importDataContext, result);
        } catch (EchoBaseTechnicalException e) {
            throw new ImportException(importDataContext.getLocale(), inputFile, e);
        } finally {
            flushImportedExport(result);
        }

        try {
            checkImport(result, result.getProcessedImportFile(), result.getImportedExportFile());
        } catch (Exception e) {
            throw new ImportException(importDataContext.getLocale(), inputFile, e);
        }

        flushResult(result);

    }

    private void flushResult(ImportDataFileResult result) {

        StringBuilder importText = new StringBuilder();

        for (EchoBaseUserEntityEnum entityType : result.getEntityTypes()) {
            int numberCreated = result.getNumberCreated(entityType);
            int numberUpdated = result.getNumberUpdated(entityType);
            int numberNotImported = result.getNumberNotImported(entityType);
            boolean withCreated = numberCreated > 0;
            boolean withUpdated = numberUpdated > 0;
            boolean withNotImported = numberNotImported > 0;
            if (!withCreated && !withUpdated && !withNotImported) {
                continue;
            }
            importText.append("\n\tEntité ").append(entityType).append(" (");
            if (withCreated) {
                importText.append(numberCreated);
                importText.append(" création(s)");
            }
            if (withUpdated) {
                importText.append(numberUpdated);
                importText.append(" mise(s) à jour");
            }
            if (withNotImported) {
                importText.append(numberNotImported);
                importText.append(" non importée(s)");
            }
            importText.append(")");
        }
        
        importFile.setImportText(importText.toString().trim());
        persistenceService.addCheckedExportFile(importFile.getTopiaId(), result.getImportedExportFile());
    }

    @Override
    public final void close() throws IOException {
        if (reader != null) {
            IOUtils.closeQuietly(reader);
        }
        if (csvImport != null) {
            IOUtils.closeQuietly(csvImport);
        }
        if (csvProcessedExport != null) {
            IOUtils.closeQuietly(csvProcessedExport);
        }
        if (csvImportedExport != null) {
            IOUtils.closeQuietly(csvImportedExport);
        }
    }

    protected final BatchCsvExport<E> createBatchCsvExport(InputFile exportInputFile) {

        ExportModel<E> exportModel = createCsvExportModel(importDataContext);

        try {

            Path workingDirectory = importDataContext.getConfiguration().getWorkingDirectory().toPath();
            Path exportFile = Files.createTempFile(workingDirectory, exportInputFile.getFileName(), "");
            if (log.isInfoEnabled()) {
                log.info("Create temporary export file: " + exportFile);
            }
            exportInputFile.setFile(exportFile.toFile());

            BufferedWriter writer = Files.newBufferedWriter(exportFile, StandardCharsets.UTF_8);
            return new BatchCsvExport<>(exportModel, writer);

        } catch (IOException e) {
            throw new EchoBaseTechnicalException("Could not create export file", e);
        }

    }

    protected final void addProcessedRow(ImportDataFileResult result, E processedRow) {

        processedRowsToExport.add(processedRow);

        if (processedRowsToExport.size() % EXPORT_FLUSH_MAX == 0) {

            flushProcessedExport(result);

        }
    }

    protected final void addImportedRow(ImportDataFileResult result, E importedRow) {

        importedRowsToExport.add(importedRow);

        if (importedRowsToExport.size() % EXPORT_FLUSH_MAX == 0) {

            flushImportedExport(result);

        }
    }

    protected final Import<E> open() {
        if (reader != null) {
            throw new IllegalStateException("Reader was already opened");
        }
        if (csvImport != null) {
            throw new IllegalStateException("csvImport was already opened");
        }
        try {
            reader = Files.newBufferedReader(getInputFile().getFile().toPath(), StandardCharsets.UTF_8);
        } catch (IOException e) {
            throw new EchoBaseTechnicalException("Could not find import file " + getInputFile().getFile(), e);
        }
        return csvImport = Import.newImport(importModel, reader);
    }

    protected void checkImport(ImportDataFileResult result, InputFile processedExportFile, InputFile importedExportFile) {
        if (log.isInfoEnabled()) {
            log.info("Check processed export file: " + processedExportFile + " vs imported export file: " + importedExportFile);
        }
        try {

            //TODO Faire cela de manière moins brute : comparaison des fichiers ligne à ligne (n plus cela nous donnerait aussi le numéro de première ligne défaillante)
            File dataDirectory = persistenceService.getConfiguration().getDataDirectory();
            String expectedContent = new String(Files.readAllBytes(processedExportFile.getFile().toPath()), StandardCharsets.UTF_8);
            String actualContent = new String(Files.readAllBytes(importedExportFile.getFile().toPath()), StandardCharsets.UTF_8);
            if (!expectedContent.equals(actualContent)) {

                // On recopie les fichiers pour que l'utilisateur puisse les télécharger...
                File workingDirectory = new File(dataDirectory, "import-check-" + System.nanoTime());
                FileUtils.forceMkdir(workingDirectory);

                File targetProcessedExportFile = EchoBaseIOUtil.copyAndGzipFileToDirectory(processedExportFile.getFile(), workingDirectory);
                File targetImportedExportFile = EchoBaseIOUtil.copyAndGzipFileToDirectory(importedExportFile.getFile(), workingDirectory);

                int directoryPrefix = dataDirectory.getAbsolutePath().length() + 1;
                String targetProcessedExportFileSuffix = targetProcessedExportFile.getAbsolutePath().substring(directoryPrefix);
                String targetImportedExportFileSuffix = targetImportedExportFile.getAbsolutePath().substring(directoryPrefix);

                throw new CheckFileException("Check file for file: " + importedExportFile.getFileName() + " failed, contents are not the same.", result.getImportFile().getName(), targetProcessedExportFileSuffix, targetImportedExportFileSuffix);

            }
        } catch (IOException e) {
            throw new EchoBaseTechnicalException("Could not read files", e);
        }

    }

    protected final Locale getLocale() {
        return importDataContext.getLocale();
    }

    protected final M getConfiguration() {
        return importDataContext.getConfiguration();
    }

    protected final EchoBaseUser getUser() {
        return importDataContext.getUser();
    }

    protected final InputFile getInputFile() {
        return inputFile;
    }

    protected final String getImportLabel() {
        return l(getLocale(), getConfiguration().getImportType().getI18nKey());
    }

    protected final String getImportMessage() {
        return l(getLocale(), "echobase.importLabel.withFile", getImportLabel(), getInputFile().getFileName());
    }

    protected void incrementsProgress() {
        getConfiguration().incrementsProgress();
    }

    protected void doFlushTransaction(int rowNumber) {
        getConfiguration().incrementsProgress();
        if (rowNumber % TRANSACTION_FLUSH_MAX == 0) {
            // flush each 1000 imported rows
            String message = getImportMessage();
            if (log.isDebugEnabled()) {
                log.debug(message);
            }
            getPersistenceService().flush();
        }
    }

    protected final UserDbPersistenceService getPersistenceService() {
        return importDataContext.getPersistenceService();
    }

    protected final Data createCellData(Cell cell,
                                        DataMetadata dataMetaData,
                                        String dataValue,
                                        DataQuality dataQuality,
                                        ImportDataFileResult importResult,
                                        boolean collectId,
                                        int rowNumber) {

        Data data = getPersistenceService().createData(dataMetaData, dataValue);
        cell.addData(data);
        cell.setDataQuality(dataQuality);

        if (collectId) {
            addId(importResult, EchoBaseUserEntityEnum.Data, data, rowNumber);
        } else {
            importResult.incrementsNumberCreated(EchoBaseUserEntityEnum.Data);
        }

        return data;

    }

    public <EE extends TopiaEntity> void addId(ImportDataFileResult importResult, EchoBaseUserEntityEnum entityEnum, EE entity, int lineNumber) {
        int entityCount = importResult.addId(entityEnum);

        ImportFileId importFileId = persistenceService.newImportFileId(importResult.getImportFile(), entity, lineNumber, entityCount);
        importedFileIdsToPersist.add(importFileId);

        if (importedFileIdsToPersist.size() % EXPORT_FLUSH_MAX == 0) {
            flushImportFileIds(importResult);
        }
    }

    public <EE extends TopiaEntity> void updateId(ImportDataFileResult importResult, EchoBaseUserEntityEnum entityEnum, EE entity, int lineNumber) {
        int entityCount = importResult.updateId(entityEnum);
        
        ImportFileId importFileId = persistenceService.newImportFileId(importResult.getImportFile(), entity, lineNumber, entityCount);
        importedFileIdsToPersist.add(importFileId);

        if (importedFileIdsToPersist.size() % EXPORT_FLUSH_MAX == 0) {
            flushImportFileIds(importResult);
        }
    }

    protected <EE extends TopiaEntity> Iterable<EE> getImportedEntities(Class<EE> type, ImportDataFileResult result) {
        return persistenceService.getImportedEntities(result.getImportFile(), type);
    }

    protected Iterable<ImportFileId> getImportFileIds(ImportDataFileResult result) {
        return persistenceService.getImportFileIdsForImportFile(result.getImportFile());
    }

    private void flushProcessedExport(ImportDataFileResult result) {
        try {
            csvProcessedExport.export(processedRowsToExport);
        } catch (Exception e) {
            throw new EchoBaseTechnicalException(l(getLocale(), "echobase.exportError.toFile", result.getProcessedImportFile().getFileName(), e.getMessage()), e);
        } finally {
            processedRowsToExport.clear();
        }
    }

    private void flushImportedExport(ImportDataFileResult result) {

        try {
            csvImportedExport.export(importedRowsToExport);
        } catch (Exception e) {
            throw new EchoBaseTechnicalException(l(getLocale(), "echobase.exportError.toFile", result.getImportedExportFile().getFileName(), e.getMessage()), e);
        } finally {
            importedRowsToExport.clear();
        }

    }

    private void flushImportFileIds(ImportDataFileResult result) {

        try {
            persistenceService.creteImportFileIds(importedFileIdsToPersist);

        } catch (Exception e) {
            throw new EchoBaseTechnicalException(l(getLocale(), "echobase.exportError.toFile", result.getProcessedImportFile().getFileName(), e.getMessage()), e);
        } finally {
            importedFileIdsToPersist.clear();
        }

    }

}
