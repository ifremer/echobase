package fr.ifremer.echobase.services.csv;

/*
 * #%L
 * EchoBase :: Services
 * %%
 * Copyright (C) 2011 - 2016 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.base.Supplier;
import org.nuiton.csv.Export;
import org.nuiton.csv.ExportModel;
import org.nuiton.csv.ExportableColumn;

import java.io.Closeable;
import java.io.IOException;
import java.io.Writer;

/**
 * To export
 * Created on 01/04/16.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 4.0
 */
public class BatchCsvExport<E> implements Closeable {

    /**
     * Where to export.
     */
    protected final Writer writer;
    /**
     * Underligned exporter object.
     */
    protected final EchoBaseExport<E> export;
    /**
     * What to export.
     */
    protected final MySupplier<Iterable<E>> dataSupplier;
    /**
     * Is header was already written?
     */
    protected boolean headerDone;

    public BatchCsvExport(ExportModel<E> model, Writer writer) {
        this.writer = writer;
        this.dataSupplier = new MySupplier<>();
        this.export = new EchoBaseExport<>(model, dataSupplier);
    }

    public synchronized void export(Iterable<E> data) throws Exception {

        dataSupplier.setData(data);
        export.write(writer, !headerDone);
        writer.flush();
        headerDone = true;

    }

    @Override
    public void close() throws IOException {
        writer.close();
    }

    protected static class MySupplier<E> implements Supplier<E> {

        private E data;

        @Override
        public E get() {
            return data;
        }

        public void setData(E data) {
            this.data = data;
        }

    }

    protected static class EchoBaseExport<E> extends Export<E> {

        private final Supplier<Iterable<E>> dataSupplier;

        protected EchoBaseExport(ExportModel<E> model, Supplier<Iterable<E>> dataSupplier) {
            super(model, null);
            this.dataSupplier = dataSupplier;
        }

        @Override
        public void write(Writer writer, boolean writeHeader) throws Exception {
            // write csv header
            if (writeHeader) {
                writeHeader(writer);
            }

            // obtain export columns
            Iterable<ExportableColumn<E, Object>> columns = model.getColumnsForExport();

            for (E row : dataSupplier.get()) {

                // write the row for this data
                writeRow(writer, columns, row);
            }

        }
    }
}
