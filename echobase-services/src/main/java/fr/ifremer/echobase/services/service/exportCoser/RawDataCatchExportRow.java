package fr.ifremer.echobase.services.service.exportCoser;

/*
 * #%L
 * EchoBase :: Services
 * %%
 * Copyright (C) 2011 - 2014 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.echobase.entities.data.Operation;
import fr.ifremer.echobase.entities.data.Voyage;
import fr.ifremer.echobase.entities.references.Species;

/**
 * Created on 4/5/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.6
 */
public class RawDataCatchExportRow {

    public static final String PROPERTY_SPECIES = "species";

    public static final String PROPERTY_OPERATION_ID = "operation." + Operation.PROPERTY_ID;

    public static final String PROPERTY_VOYAGE_NAME = "voyage." + Voyage.PROPERTY_NAME;

    public static final String PROPERTY_VOYAGE_YEAR = "voyage." + Voyage.PROPERTY_START_DATE;

    public static final String PROPERTY_OPERATION_NUMBER = "number";

    public static final String PROPERTY_OPERATION_WEIGHT = "weight";

    protected Voyage voyage;

    protected Species species;

    protected Operation operation;

    protected int number;

    protected float weight;

    public Voyage getVoyage() {
        return voyage;
    }

    public void setVoyage(Voyage voyage) {
        this.voyage = voyage;
    }

    public Species getSpecies() {
        return species;
    }

    public void setSpecies(Species species) {
        this.species = species;
    }

    public Operation getOperation() {
        return operation;
    }

    public void setOperation(Operation operation) {
        this.operation = operation;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public float getWeight() {
        return weight;
    }

    public void setWeight(float weight) {
        this.weight = weight;
    }
}
