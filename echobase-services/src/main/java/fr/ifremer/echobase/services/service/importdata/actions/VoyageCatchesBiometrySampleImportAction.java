package fr.ifremer.echobase.services.service.importdata.actions;

/*
 * #%L
 * EchoBase :: Services
 * %%
 * Copyright (C) 2011 - 2016 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.base.Preconditions;
import fr.ifremer.echobase.entities.EchoBaseUserEntityEnum;
import fr.ifremer.echobase.entities.ImportedSampleDataResult;
import fr.ifremer.echobase.entities.data.Operation;
import fr.ifremer.echobase.entities.data.Sample;
import fr.ifremer.echobase.entities.data.SampleData;
import fr.ifremer.echobase.entities.references.SampleDataType;
import fr.ifremer.echobase.entities.references.SampleType;
import fr.ifremer.echobase.entities.references.SizeCategory;
import fr.ifremer.echobase.entities.references.Species;
import fr.ifremer.echobase.entities.references.SpeciesCategory;
import fr.ifremer.echobase.io.InputFile;
import fr.ifremer.echobase.services.service.importdata.ImportDataFileResult;
import fr.ifremer.echobase.services.service.importdata.SpeciesCategoryCache;
import fr.ifremer.echobase.services.service.importdata.contexts.VoyageCatchesImportDataContext;
import fr.ifremer.echobase.services.service.importdata.csv.VoyageCatchesBiometrySampleImportExportModel;
import fr.ifremer.echobase.services.service.importdata.csv.VoyageCatchesBiometrySampleImportRow;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.csv.Import;

import java.util.Collection;
import java.util.Map;
import java.util.TreeMap;
import org.nuiton.csv.ImportRuntimeException;

/**
 * Created on 25/03/16.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 4.0
 */
public class VoyageCatchesBiometrySampleImportAction extends VoyageCatchesImportDataActionSupport<VoyageCatchesBiometrySampleImportRow> {

    /** Logger. */
    private static final Log log = LogFactory.getLog(VoyageCatchesBiometrySampleImportAction.class);

    public VoyageCatchesBiometrySampleImportAction(VoyageCatchesImportDataContext importDataContext) {
        super(importDataContext, importDataContext.getConfiguration().getBiometrySampleFile());
    }

    @Override
    protected VoyageCatchesBiometrySampleImportExportModel createCsvImportModel(VoyageCatchesImportDataContext importDataContext) {
        return VoyageCatchesBiometrySampleImportExportModel.forImport(importDataContext);
    }

    @Override
    protected VoyageCatchesBiometrySampleImportExportModel createCsvExportModel(VoyageCatchesImportDataContext importDataContext) {
        return VoyageCatchesBiometrySampleImportExportModel.forExport(importDataContext);
    }

    @Override
    protected void performImport(VoyageCatchesImportDataContext importDataContext, InputFile inputFile, ImportDataFileResult result) {

        if (log.isInfoEnabled()) {
            log.info("Starts import of biometry from file " + inputFile.getFileName());
        }

        SampleDataType sampleDataTypeFishIndex = importDataContext.getSampleDataTypeSpecimenIndex();
        SampleType sampleTypeIndividual = importDataContext.getSampleTypeIndividualType();
        Collection<Operation> operationWithTotalOrUnsortedSample = importDataContext.getVoyageOperationsWithTotalOrUnsortedSample();
        SpeciesCategoryCache speciesCategoryCache = importDataContext.getSpeciesCategoryCache();
        Map<String, Sample> samples = new TreeMap<>();

        try (Import<VoyageCatchesBiometrySampleImportRow> importer = open()) {

            incrementsProgress();
            int rowNumber = 0;
            for (VoyageCatchesBiometrySampleImportRow row : importer) {

                doFlushTransaction(++rowNumber);

                Operation operation = row.getOperation();
                if (operation != null) {
                 
                    checkOperationWithTotalOrUnsortedSample(rowNumber, operationWithTotalOrUnsortedSample, operation);

                    Species species = row.getSpecies();

                    int numFish = row.getNumFish();

                    SizeCategory sizeCategory = row.getSizeCategory();

                    String sampleKey = operation.getId() + "_" + species.getBaracoudaCode() + "_" + numFish;

                    Sample sample = samples.get(sampleKey);

                    if (sample == null) {

                        // create a new sample
                        sample = persistenceService.newSample();

                        sample.setSampleType(sampleTypeIndividual);

                        SpeciesCategory category = speciesCategoryCache.getSpeciesCategory(species, null, sizeCategory, null, null, result);

                        sample.setSpeciesCategory(category);

                        sample = addSample(operation, sample, result, rowNumber);

                        samples.put(sampleKey, sample);
                    }

                    addProcessedRow(result, row);

                    SampleData sampleDataFishIndex = persistenceService.createSampleData(sampleDataTypeFishIndex, null, (float) numFish);
                    sample.addSampleData(sampleDataFishIndex);
                    addId(result, EchoBaseUserEntityEnum.SampleData, sampleDataFishIndex, rowNumber);

                    SampleData sampleDataCreated = persistenceService.createSampleData(row.getSampleData());
                    sample.addSampleData(sampleDataCreated);
                    addId(result, EchoBaseUserEntityEnum.SampleData, sampleDataCreated, rowNumber);
                } else {
                    throw new ImportRuntimeException("At line " + rowNumber + ", no operation found");
                }
            }

        }

    }

    @Override
    protected void computeImportedExport(VoyageCatchesImportDataContext importDataContext, ImportDataFileResult result) {

        SampleDataType sampleDataTypeFishIndex = importDataContext.getSampleDataTypeSpecimenIndex();

        SampleData sampleDataFishIndex = null;

        for (ImportedSampleDataResult sampleDataRow : persistenceService.getImportedSampleDataResults(result.getImportFile())) {

            SampleData sampleData = sampleDataRow.getSampleData();
            if (sampleDataTypeFishIndex.equals(sampleData.getSampleDataType())) {
                sampleDataFishIndex = sampleData;
                continue;
            }

            if (log.isInfoEnabled()) {
                log.info("Adding sampleData: " + sampleData.getTopiaId() + " to imported export.");
            }

            Preconditions.checkNotNull(sampleDataFishIndex);

            Float numFishSampleDataValue = sampleDataFishIndex.getDataValue();
            Preconditions.checkNotNull(numFishSampleDataValue);

            VoyageCatchesBiometrySampleImportRow importedRow = VoyageCatchesBiometrySampleImportRow.of(sampleDataRow.getOperation(), sampleDataRow.getSpeciesCategory().getSpecies(), sampleDataRow.getSpeciesCategory().getSizeCategory(), sampleData, numFishSampleDataValue.intValue());

            addImportedRow(result, importedRow);

        }

    }

}
