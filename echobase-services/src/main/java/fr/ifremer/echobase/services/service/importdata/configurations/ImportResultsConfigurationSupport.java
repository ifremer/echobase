/*
 * #%L
 * EchoBase :: Services
 * %%
 * Copyright (C) 2011 - 2012 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ifremer.echobase.services.service.importdata.configurations;

/**
 * Configuration of a "results" import.
 *
 * @author Julien Ruchaud - ruchaud@codelutin.com
 * @since 4.0
 */
public abstract class ImportResultsConfigurationSupport extends ImportDataConfigurationSupport {

    private static final long serialVersionUID = 1L;

    /** Selected dataProcessing id where to search esdu or elementary cells. */
    protected String dataProcessingId;
    
    /** resultLabel to store while importing acoustic result. */
    protected String resultLabel;

    public String getDataProcessingId() {
        return dataProcessingId;
    }

    public void setDataProcessingId(String dataProcessingId) {
        this.dataProcessingId = dataProcessingId;
    }

    public String getResultLabel() {
        return resultLabel;
    }

    public void setResultLabel(String resultLabel) {
        this.resultLabel = resultLabel;
    }
}
