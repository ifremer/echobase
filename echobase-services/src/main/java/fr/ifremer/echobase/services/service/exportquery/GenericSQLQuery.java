package fr.ifremer.echobase.services.service.exportquery;

/*
 * #%L
 * EchoBase :: Services
 * %%
 * Copyright (C) 2011 - 2012 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.persistence.TopiaException;
import org.nuiton.topia.persistence.support.TopiaSqlQuery;
import org.nuiton.topia.persistence.support.TopiaSqlSupport;
import org.nuiton.util.PagerBean;
import org.nuiton.util.PagerBeanUtil;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

/**
 * TODO
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.0
 */
public class GenericSQLQuery extends TopiaSqlQuery<Map<String, Object>> {

    /** Logger. */
    private static final Log log = LogFactory.getLog(GenericSQLQuery.class);

    protected String[] columnNames;

    private final String sql;

    private final PagerBean pager;

    public GenericSQLQuery(String sql, PagerBean pager) {
        this.sql = sql;
        this.pager = pager;
    }

    public String[] getColumnNames() {
        return columnNames;
    }

    public ExportQueryCsvModel generateCsvModel(char charSeparator) {
        return new ExportQueryCsvModel(
                charSeparator,
                columnNames
        );
    }

    public List<Map<String, Object>> getResult(TopiaSqlSupport tx) throws TopiaException {
        return tx.findMultipleResult(this);
    }

    public void testQuery(TopiaSqlSupport tx) throws TopiaException {
        tx.findSingleResult(this);
    }

    public String[] getColumnNames(TopiaSqlSupport tx) throws TopiaException {
        tx.findSingleResult(this);
        return columnNames;
    }

    @Override
    public PreparedStatement prepareQuery(Connection connection) throws SQLException {
        return connection.prepareStatement(
                sql,
                ResultSet.TYPE_SCROLL_INSENSITIVE,
                ResultSet.CONCUR_READ_ONLY
        );
    }

    @Override
    public void afterExecuteQuery(ResultSet set) throws SQLException {
        super.afterExecuteQuery(set);

        // obtain columnNames
        columnNames = getColumnNames(set);

        if (pager != null) {

            // must count rows
            long nbRows = getNbRows(set);
            if (log.isInfoEnabled()) {
                log.info("For request " + sql + ", nb rows = " + nbRows);
            }
            pager.setRecords(nbRows);
            PagerBeanUtil.computeRecordIndexesAndPagesNumber(pager);
        }
    }

    /**
     * From a given result set, let's count his number of row.
     *
     * <strong>Note:</strong> the result set must be scrollable to go back to
     * before first row.
     *
     * @param set the result set to inspect
     * @return the number of row of the given result set
     * @throws SQLException if any pb
     * @since 2.6.4
     */
    @Override
    protected long getNbRows(ResultSet set) throws SQLException {

        //TODO Bring this back to Topia
        long nbRows = 0;
        if (set.last()) {
            nbRows = set.getRow();
        }
        // go back before first row (be ware the resultset must be scrollable)
        set.beforeFirst();
        return nbRows;
    }

    @Override
    public Map<String, Object> prepareResult(ResultSet set) throws SQLException {

        if (pager != null) {

            // get row number (getRow() begins at 1)
            int rowNumber = set.getRow() - 1;
            if (rowNumber < pager.getRecordStartIndex() ||
                rowNumber >= pager.getRecordEndIndex()) {

                // out of pager bound, by returning null
                // result will not be take in account
                return null;
            }
        }

        return getRowAsMap(columnNames, set);
    }

}
