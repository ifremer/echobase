/*
 * #%L
 * EchoBase :: Services
 * %%
 * Copyright (C) 2011 - 2012 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ifremer.echobase.services.service.importdata.csv;

import fr.ifremer.echobase.entities.data.Operation;
import fr.ifremer.echobase.entities.data.OperationMetadataValue;
import fr.ifremer.echobase.entities.data.OperationMetadataValueImpl;
import fr.ifremer.echobase.entities.data.Transect;
import fr.ifremer.echobase.entities.references.OperationMetadata;
import fr.ifremer.echobase.entities.references.Vessel;

/**
 * Bean used as a row for import of {@link VoyageOperationsOperationMetadataValueImportExportModel}.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 0.3
 */
public class VoyageOperationsOperationMetadataValueImportRow {

    public static final String PROPERTY_VESSEL = Transect.PROPERTY_VESSEL;
    public static final String PROPERTY_OPERATION = Transect.PROPERTY_OPERATION;
    public static final String PROPERTY_METADATA_TYPE = "metadataType";
    public static final String PROPERTY_OPERATION_METADATA_VALUE = Operation.PROPERTY_OPERATION_METADATA_VALUE;

    protected final OperationMetadataValue operationMetadataValue;
    protected Operation operation;
    protected Vessel vessel;

    public static VoyageOperationsOperationMetadataValueImportRow of(OperationMetadataValue operationMetadataValue) {
        VoyageOperationsOperationMetadataValueImportRow row = new VoyageOperationsOperationMetadataValueImportRow(operationMetadataValue);
        Operation operation = operationMetadataValue.getOperation();
        row.setOperation(operation);
        row.setVessel(operation.getTransect().getVessel());
        return row;
    }

    public VoyageOperationsOperationMetadataValueImportRow(OperationMetadataValue operationMetadataValue) {
        this.operationMetadataValue = operationMetadataValue;
    }

    public VoyageOperationsOperationMetadataValueImportRow() {
        this(new OperationMetadataValueImpl());
    }

    public String getDataValue() {
        return operationMetadataValue.getDataValue();
    }

    public void setDataValue(String dataValue) {
        operationMetadataValue.setDataValue(dataValue);
    }

    public OperationMetadataValue getOperationMetadataValue() {
        return operationMetadataValue;
    }

    public OperationMetadata getOperationMetadata() {
        return operationMetadataValue.getOperationMetadata();
    }

    public void setOperationMetadata(OperationMetadata operationMetadata) {
        operationMetadataValue.setOperationMetadata(operationMetadata);
    }

    public Operation getOperation() {
        return operation;
    }

    public void setOperation(Operation operation) {
        this.operation = operation;
    }

    public Vessel getVessel() {
        return vessel;
    }

    public void setVessel(Vessel vessel) {
        this.vessel = vessel;
    }
}
