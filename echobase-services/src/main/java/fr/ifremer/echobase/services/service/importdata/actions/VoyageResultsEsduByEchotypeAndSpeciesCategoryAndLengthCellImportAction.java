package fr.ifremer.echobase.services.service.importdata.actions;

/*
 * #%L
 * EchoBase :: Services
 * %%
 * Copyright (C) 2011 - 2016 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.base.Preconditions;
import com.google.common.base.Strings;
import fr.ifremer.echobase.entities.data.DataAcousticProvider;
import fr.ifremer.echobase.entities.data.Category;
import fr.ifremer.echobase.entities.data.Cell;
import fr.ifremer.echobase.entities.data.Result;
import fr.ifremer.echobase.entities.references.SizeCategory;
import fr.ifremer.echobase.services.service.importdata.ImportDataFileResult;
import fr.ifremer.echobase.services.service.importdata.MismatchLengthCategoryMeaningException;
import fr.ifremer.echobase.services.service.importdata.ResultCategoryCache;
import fr.ifremer.echobase.services.service.importdata.SizeCategoryCache;
import fr.ifremer.echobase.services.service.importdata.contexts.VoyageResultsImportDataContext;
import fr.ifremer.echobase.services.service.importdata.csv.VoyageResultsEsduByEchotypeAndSpeciesCategoryAndLengthImportExportModel;
import fr.ifremer.echobase.services.service.importdata.csv.VoyageResultsEsduByEchotypeAndSpeciesCategoryAndLengthImportRow;

import java.util.List;

/**
 * Created on 25/03/16.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 4.0
 */
public class VoyageResultsEsduByEchotypeAndSpeciesCategoryAndLengthCellImportAction extends VoyageResultsCellImportDataActionSupport<VoyageResultsEsduByEchotypeAndSpeciesCategoryAndLengthImportRow> {

    protected SizeCategoryCache sizeCategoryCache;

    public VoyageResultsEsduByEchotypeAndSpeciesCategoryAndLengthCellImportAction(VoyageResultsImportDataContext importDataContext) {
        super(importDataContext, importDataContext.getConfiguration().getEsduByEchotypeAndSpeciesCategoryAndLengthFile(), VoyageResultsEsduByEchotypeAndSpeciesCategoryAndLengthImportExportModel.COLUMN_NAMES_TO_EXCLUDE);
        this.sizeCategoryCache = importDataContext.getSizeCategoryCache();
    }

    @Override
    protected VoyageResultsEsduByEchotypeAndSpeciesCategoryAndLengthImportExportModel createCsvImportModel(VoyageResultsImportDataContext importDataContext) {
        return VoyageResultsEsduByEchotypeAndSpeciesCategoryAndLengthImportExportModel.forImport(importDataContext, metas);
    }

    @Override
    protected VoyageResultsEsduByEchotypeAndSpeciesCategoryAndLengthImportExportModel createCsvExportModel(VoyageResultsImportDataContext importDataContext) {
        return VoyageResultsEsduByEchotypeAndSpeciesCategoryAndLengthImportExportModel.forExport(importDataContext, metas);
    }

    @Override
    protected Category getResultCategory(ImportDataFileResult result, ResultCategoryCache resultCategoryCache, VoyageResultsEsduByEchotypeAndSpeciesCategoryAndLengthImportRow row) {
        SizeCategory lengthCategory = null;
        SizeCategory sizeCategory = null;

        if (!Strings.isNullOrEmpty(row.getSizeCategory())) {
            sizeCategory = sizeCategoryCache.getSizeCategory(row.getSizeCategory());
        }
        
        if (row.getLengthClass() != null) {
            lengthCategory = sizeCategoryCache.getSizeCategory(String.valueOf(row.getLengthClass()), row.getLengthClassMeaning(), result);
            Preconditions.checkNotNull(lengthCategory);
            if (!lengthCategory.getMeaning().equals(row.getLengthClassMeaning())) {
                throw new MismatchLengthCategoryMeaningException(getLocale(), row.getLengthClassMeaning(), lengthCategory.getMeaning());
            }

        }

        return resultCategoryCache.getResultCategory(row.getEchotype(),
                                                     row.getSpecies(),
                                                     row.getLengthClass(),
                                                     sizeCategory,
                                                     null,
                                                     result);
    }

    @Override
    protected VoyageResultsEsduByEchotypeAndSpeciesCategoryAndLengthImportRow newImportedRow(DataAcousticProvider voyage, Cell cell, Category category, List<Result> cellResults) {
        
        Float lengthClass = category.getSpeciesCategory().getLengthClass();
        SizeCategory lengthCategory = sizeCategoryCache.getSizeCategory(String.valueOf(lengthClass));
        
        return VoyageResultsEsduByEchotypeAndSpeciesCategoryAndLengthImportRow.of(voyage, cell, category, lengthCategory, cellResults);
    }

}
