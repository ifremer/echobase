package fr.ifremer.echobase.services.service.exportquery;

/*
 * #%L
 * EchoBase :: Services
 * %%
 * Copyright (C) 2011 - 2012 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.echobase.services.csv.EchoBaseCsvUtil;
import org.nuiton.csv.ext.AbstractExportModel;

import java.util.Map;

/**
 * Csv model to export sql in {@link ExportQueryService}.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 0.2
 */
public class ExportQueryCsvModel extends AbstractExportModel<Map<String, Object>> {

    public ExportQueryCsvModel(char separator, String[] columnHeaders) {
        super(separator);
        for (String columnHeader : columnHeaders) {
            newColumnForExport(
                    columnHeader,
                    EchoBaseCsvUtil.TO_STRING_FORMATTER
            );
        }
    }

}
