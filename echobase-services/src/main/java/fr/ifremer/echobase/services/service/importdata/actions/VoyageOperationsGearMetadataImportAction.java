package fr.ifremer.echobase.services.service.importdata.actions;

/*
 * #%L
 * EchoBase :: Services
 * %%
 * Copyright (C) 2011 - 2016 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.echobase.entities.EchoBaseUserEntityEnum;
import fr.ifremer.echobase.entities.data.GearMetadataValue;
import fr.ifremer.echobase.entities.data.Operation;
import fr.ifremer.echobase.entities.data.Voyage;
import fr.ifremer.echobase.entities.references.Vessel;
import fr.ifremer.echobase.io.InputFile;
import fr.ifremer.echobase.services.service.importdata.DuplicatedGearMetadataValueException;
import fr.ifremer.echobase.services.service.importdata.ImportDataFileResult;
import fr.ifremer.echobase.services.service.importdata.contexts.VoyageOperationsImportDataContext;
import fr.ifremer.echobase.services.service.importdata.csv.VoyageOperationsGearMetadataValueImportExportModel;
import fr.ifremer.echobase.services.service.importdata.csv.VoyageOperationsGearMetadataValueImportRow;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.csv.Import;

/**
 * Created on 25/03/16.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 4.0
 */
public class VoyageOperationsGearMetadataImportAction extends VoyageOperationsImportDataActionSupport<VoyageOperationsGearMetadataValueImportRow> {

    /** Logger. */
    private static final Log log = LogFactory.getLog(VoyageOperationsGearMetadataImportAction.class);

    public VoyageOperationsGearMetadataImportAction(VoyageOperationsImportDataContext importDataContext) {
        super(importDataContext, importDataContext.getConfiguration().getGearMetadataFile());
    }

    @Override
    protected VoyageOperationsGearMetadataValueImportExportModel createCsvImportModel(VoyageOperationsImportDataContext importDataContext) {
        return VoyageOperationsGearMetadataValueImportExportModel.forImport(importDataContext);
    }

    @Override
    protected VoyageOperationsGearMetadataValueImportExportModel createCsvExportModel(VoyageOperationsImportDataContext importDataContext) {
        return VoyageOperationsGearMetadataValueImportExportModel.forExport(importDataContext);
    }

    @Override
    protected void performImport(VoyageOperationsImportDataContext importDataContext, InputFile inputFile, ImportDataFileResult result) {

        if (log.isInfoEnabled()) {
            log.info("Starts import of gear metadata values from file " + inputFile.getFileName());
        }

        Voyage voyage = importDataContext.getVoyage();

        try (Import<VoyageOperationsGearMetadataValueImportRow> importer = open()) {

            int rowNumber = 0;
            incrementsProgress();
            for (VoyageOperationsGearMetadataValueImportRow row : importer) {

                doFlushTransaction(++rowNumber);
                Operation operation = row.getOperation();

                Vessel vessel = operation.getTransect().getVessel();
                GearMetadataValue gearMetadataValuetoCreate = row.getGearMetadataValue();

                boolean exists = persistenceService.containsGearMetadataValue(voyage,
                                                                              vessel,
                                                                              operation.getDepthStratum(),
                                                                              operation.getId(),
                                                                              operation.getGear(),
                                                                              gearMetadataValuetoCreate.getGearMetadata());
                if (exists) {
                    throw new DuplicatedGearMetadataValueException(getLocale(),
                                                                   rowNumber,
                                                                   voyage.getName(),
                                                                   vessel.getName(),
                                                                   operation.getDepthStratum().getId(),
                                                                   operation.getId(),
                                                                   operation.getGear().getCasinoGearName(),
                                                                   gearMetadataValuetoCreate.getGearMetadata().getName());
                }

                GearMetadataValue gearMetadataValue = persistenceService.createGearMetadataValue(gearMetadataValuetoCreate);

                operation.addGearMetadataValue(gearMetadataValue);
                addId(result, EchoBaseUserEntityEnum.GearMetadataValue, gearMetadataValue, rowNumber);

                addProcessedRow(result, row);

            }

        }

    }

    @Override
    protected void computeImportedExport(VoyageOperationsImportDataContext importDataContext, ImportDataFileResult result) {

        for (GearMetadataValue gearMetadataValue : getImportedEntities(GearMetadataValue.class, result)) {

            if (log.isInfoEnabled()) {
                log.info("Adding gear metadata value : " + gearMetadataValue.getTopiaId() + " to imported export.");
            }

            VoyageOperationsGearMetadataValueImportRow importedRow = VoyageOperationsGearMetadataValueImportRow.of(gearMetadataValue);
            addImportedRow(result, importedRow);

        }
    }

}
