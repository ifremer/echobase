package fr.ifremer.echobase.services.service.importdata.actions;

/*
 * #%L
 * EchoBase :: Services
 * %%
 * Copyright (C) 2011 - 2016 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.echobase.entities.data.DataAcousticProvider;
import fr.ifremer.echobase.entities.data.Echotype;
import fr.ifremer.echobase.entities.references.Species;
import fr.ifremer.echobase.services.service.importdata.configurations.VoyageResultsImportConfiguration;
import fr.ifremer.echobase.services.service.importdata.contexts.VoyageResultsImportDataContext;
import fr.ifremer.echobase.services.service.importdata.csv.VoyageResultsVoyageEchotypeImportExportModel;
import fr.ifremer.echobase.services.service.importdata.csv.VoyageResultsVoyageEchotypeImportRow;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Created on 25/03/16.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 4.0
 */
public class VoyageResultsVoyageEchotypeImportAction extends ImportResultsEchotypeActionSupport<VoyageResultsImportConfiguration, VoyageResultsImportDataContext, VoyageResultsVoyageEchotypeImportRow> {

    /** Logger. */
    private static final Log log = LogFactory.getLog(VoyageResultsVoyageEchotypeImportAction.class);

    public VoyageResultsVoyageEchotypeImportAction(VoyageResultsImportDataContext importDataContext) {
        super(importDataContext, importDataContext.getConfiguration().getEchotypeFile());
    }

    @Override
    protected VoyageResultsVoyageEchotypeImportExportModel createCsvImportModel(VoyageResultsImportDataContext importDataContext) {
        return VoyageResultsVoyageEchotypeImportExportModel.forImport(importDataContext);
    }

    @Override
    protected VoyageResultsVoyageEchotypeImportExportModel createCsvExportModel(VoyageResultsImportDataContext importDataContext) {
        return VoyageResultsVoyageEchotypeImportExportModel.forExport(importDataContext);
    }

    @Override
    protected DataAcousticProvider getDataProvider(VoyageResultsImportDataContext importDataContext) {
        return importDataContext.getVoyage();
    }

    @Override
    protected VoyageResultsVoyageEchotypeImportRow newImportedRow(DataAcousticProvider voyage, Echotype echotype, Species species) {
        return VoyageResultsVoyageEchotypeImportRow.of(voyage, echotype, species);
    }
    
}
