/*
 * #%L
 * EchoBase :: Domain
 * %%
 * Copyright (C) 2011 - 2012 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ifremer.echobase.services.csv;

import com.google.common.collect.Maps;
import fr.ifremer.echobase.entities.data.Cell;
import fr.ifremer.echobase.entities.data.CellTopiaDao;
import org.apache.commons.collections4.CollectionUtils;
import org.nuiton.csv.ImportRuntimeException;
import org.nuiton.csv.ValueParser;
import org.nuiton.topia.persistence.TopiaException;

import java.util.List;
import java.util.Map;

public class CellValueParser implements ValueParser<Cell> {

    private final Map<String, Cell> esduCellMap;

    private CellTopiaDao cellDAO;

    public CellValueParser(CellTopiaDao cellDAO) {
        this.esduCellMap = Maps.newTreeMap();
        this.cellDAO = cellDAO;
    }

    protected CellValueParser(Map<String, Cell> esduCellMap) {
        this.esduCellMap = esduCellMap;
    }

    @Override
    public Cell parse(String cellId) {
        // get esdu cell
        Cell result = getCell(cellId);

        if (result == null) {
            //FIXME USe a real exception
            throw new ImportRuntimeException("Can not found esdu cell [" + cellId + "]");
        }
        return result;
    }

    protected Cell getCell(String cellId) {

        Cell result = esduCellMap.get(cellId);

        if (result == null && cellDAO != null) {
            try {
                List<Cell> cells = cellDAO.forNameEquals(cellId).findAll();

                if (CollectionUtils.isEmpty(cells)) {
                    //FIXME USe a real exception
                    throw new ImportRuntimeException("Can not find cell with name " + cellId);
                }

                //TODO Should check this cell is in voyage ?
                result = cells.get(0);

                // store it in cache
                esduCellMap.put(cellId, result);

            } catch (TopiaException e) {
                //FIXME USe a real exception
                throw new ImportRuntimeException("Can not find cell with name " + cellId);
            }
        }
        return result;
    }
}
