/*
 * #%L
 * EchoBase :: Services
 * %%
 * Copyright (C) 2011 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ifremer.echobase.services.service.importdb;

import fr.ifremer.echobase.io.InputFile;
import fr.ifremer.echobase.services.AbstractEchobaseActionConfiguration;
import fr.ifremer.echobase.services.ProgressModel;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.util.Locale;

import static org.nuiton.i18n.I18n.l;

/**
 * Configuration of a import db operation.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 0.2
 */
public class ImportDbConfiguration extends AbstractEchobaseActionConfiguration {

    private static final long serialVersionUID = 1L;

    protected File workingDirectory;

    protected final InputFile input;

    protected ImportDbMode importDbMode;

    protected boolean computeSteps = true;

    protected boolean commitAfterEachFile;

    public ImportDbConfiguration(ProgressModel progressModel,
                                 Locale locale) {
        super(progressModel);
        input = InputFile.newFile(l(locale, "echobase.common.importDbFile"));
    }

    public ImportDbConfiguration(Locale locale) {
        input = InputFile.newFile(l(locale, "echobase.common.importDbFile"));
    }

    public File getWorkingDirectory() {
        return workingDirectory;
    }

    public InputFile getInput() {
        return input;
    }

    public ImportDbMode getImportDbMode() {
        return importDbMode;
    }

    public void setWorkingDirectory(File workingDirectory) {
        this.workingDirectory = workingDirectory;
    }

    public void setImportDbMode(ImportDbMode importDbMode) {
        this.importDbMode = importDbMode;
    }

    public boolean isComputeSteps() {
        return computeSteps;
    }

    public void setComputeSteps(boolean computeSteps) {
        this.computeSteps = computeSteps;
    }

    public boolean isCommitAfterEachFile() {
        return commitAfterEachFile;
    }

    public void setCommitAfterEachFile(boolean commitAfterEachFile) {
        this.commitAfterEachFile = commitAfterEachFile;
    }

    @Override
    public void destroy() throws IOException {
        if (workingDirectory != null) {
            FileUtils.deleteDirectory(workingDirectory);
        }
    }

}
