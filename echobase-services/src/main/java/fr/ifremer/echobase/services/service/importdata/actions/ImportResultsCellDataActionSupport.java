package fr.ifremer.echobase.services.service.importdata.actions;

/*
 * #%L
 * EchoBase :: Services
 * %%
 * Copyright (C) 2011 - 2016 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.base.Preconditions;
import fr.ifremer.echobase.entities.data.DataAcousticProvider;
import fr.ifremer.echobase.entities.ImportedCellResult;
import fr.ifremer.echobase.entities.data.Category;
import fr.ifremer.echobase.entities.data.Cell;
import fr.ifremer.echobase.entities.data.Result;
import fr.ifremer.echobase.entities.references.DataMetadata;
import fr.ifremer.echobase.io.InputFile;
import fr.ifremer.echobase.services.csv.CellAble;
import fr.ifremer.echobase.services.csv.ResultAble;
import fr.ifremer.echobase.services.service.importdata.ImportDataFileResult;
import fr.ifremer.echobase.services.service.importdata.ResultCategoryCache;
import fr.ifremer.echobase.services.service.importdata.configurations.ImportResultsConfigurationSupport;
import fr.ifremer.echobase.services.service.importdata.contexts.ImportResultsDataContext;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.csv.Import;

import java.util.LinkedList;
import java.util.List;

/**
 * @author Julien Ruchaud - ruchaud@codelutin.com
 * @since 4.0
 */
public abstract class ImportResultsCellDataActionSupport<M extends ImportResultsConfigurationSupport, C extends ImportResultsDataContext<M>, E extends CellAble & ResultAble> extends ImportResultsDataActionSupport<M, C, E> {

    /** Logger. */
    private static final Log log = LogFactory.getLog(ImportResultsCellDataActionSupport.class);

    protected final List<DataMetadata> metas;

    protected ImportResultsCellDataActionSupport(C importDataContext, InputFile inputFile, String... columnNamesToExclude) {
        super(importDataContext, inputFile);
        this.metas = importDataContext.getMetas(getInputFile(), columnNamesToExclude);
    }

    protected abstract E newImportedRow(DataAcousticProvider provider, Cell cell, Category category, List<Result> cellResults);

    protected abstract Category getResultCategory(ImportDataFileResult result, ResultCategoryCache resultCategoryCache, E row);

    protected abstract DataAcousticProvider getDataProvider(C importDataContext);
    
    @Override
    protected final void performImport(C importDataContext, InputFile inputFile, ImportDataFileResult result) {

        if (log.isInfoEnabled()) {
            log.info("Starts import of acoustic result by species and size category from file " + inputFile.getFileName());
        }

        ResultCategoryCache resultCategoryCache = importDataContext.getResultCategoryCache();

        String resultLabel = getConfiguration().getResultLabel();

        try (Import<E> importer = open()) {

            incrementsProgress();
            int rowNumber = 0;
            for (E row : importer) {

                doFlushTransaction(++rowNumber);

                Category category = getResultCategory(result, resultCategoryCache, row);
                Cell cell = row.getCell();

                addResults(row, cell, category, resultLabel, result, true, true, rowNumber);

                addProcessedRow(result, row);
            }
        }
    }
    
    @Override
    protected final void computeImportedExport(C importDataContext, ImportDataFileResult result) {
        DataAcousticProvider provider = getDataProvider(importDataContext);

        List<Result> cellResults = new LinkedList<>();
        Cell cell = null;
        Category category = null;
        int currentLineNumber = -1;
        for (ImportedCellResult resultRow : persistenceService.getImportedCellResults(result.getImportFile())) {

            int lineNumber = resultRow.getLineNumber();

            if (currentLineNumber == -1) {

                currentLineNumber = lineNumber;
                cell = resultRow.getCell();
                category = resultRow.getCategory();

            } else if (currentLineNumber != lineNumber) {

                flushCell(currentLineNumber, result, provider, cell, category, cellResults);

                currentLineNumber = lineNumber;
                cell = resultRow.getCell();
                category = resultRow.getCategory();
                cellResults.clear();

            }

            cellResults.add(resultRow.getResult());

        }

        flushCell(currentLineNumber, result, provider, cell, category, cellResults);

    }

    protected void flushCell(int lineNumber, ImportDataFileResult result, DataAcousticProvider provider, Cell cell, Category category, List<Result> cellResults) {
        Preconditions.checkState(!cellResults.isEmpty());

        if (log.isInfoEnabled()) {
            log.info("Flush " + cell + " (" + lineNumber + ") with " + cellResults.size() + " result(s) with category: " + category + ".");
        }

        E row = newImportedRow(provider, cell, category, cellResults);
        addImportedRow(result, row);
    }
}
