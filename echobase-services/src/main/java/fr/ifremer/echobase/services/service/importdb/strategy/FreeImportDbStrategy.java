package fr.ifremer.echobase.services.service.importdb.strategy;

/*
 * #%L
 * EchoBase :: Services
 * %%
 * Copyright (C) 2011 - 2012 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.echobase.entities.data.DataAcousticProvider;
import fr.ifremer.echobase.entities.EchoBaseUser;
import fr.ifremer.echobase.entities.EchoBaseUserEntityEnum;
import fr.ifremer.echobase.entities.ImportLog;
import fr.ifremer.echobase.entities.ImportType;
import fr.ifremer.echobase.entities.data.Voyage;
import fr.ifremer.echobase.persistence.EchoBaseDbMeta;
import fr.ifremer.echobase.services.service.DecoratorService;
import fr.ifremer.echobase.services.service.importdata.ImportException;
import org.nuiton.decorator.Decorator;
import org.nuiton.topia.persistence.TopiaException;
import org.nuiton.topia.persistence.metadata.AssociationMeta;
import org.nuiton.topia.persistence.metadata.TableMeta;

import javax.inject.Inject;
import java.io.File;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.zip.ZipEntry;
import org.nuiton.topia.persistence.TopiaEntity;

/**
 * Free import db strategy (can import either referential or/and data).
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.2
 */
public class FreeImportDbStrategy extends AbstractImportDbStrategy {

    @Inject
    private DecoratorService decoratorService;

    @Override
    protected void validateTableEntries(EchoBaseDbMeta dbMeta,
                                        Map<TableMeta<EchoBaseUserEntityEnum>, ZipEntry> tables) throws ImportException {
        // nothing to validate
    }

    @Override
    protected void validateAssociationEntries(EchoBaseDbMeta dbMeta,
                                              Map<AssociationMeta<EchoBaseUserEntityEnum>, ZipEntry> associations) throws ImportException {
        // nothing to validate
    }

    @Override
    protected void createImportLogEntry(EchoBaseUser user,
                                        File file,
                                        List<DataAcousticProvider> importedEntities) throws TopiaException {
        Date date = newDate();

        Decorator<Voyage> decorator = decoratorService.getDecorator(Voyage.class, null);
        for (DataAcousticProvider provider : importedEntities) {
            TopiaEntity entity = provider.getEntity();
            String topiaId = entity.getTopiaId();

            ImportType type;
            if (topiaId.startsWith(Voyage.class.getName())) {
                type = ImportType.VOYAGE;
            } else {
                type = ImportType.MOORING_COMMONS;
            }
            
            // create a importLog entry
            ImportLog importLog = persistenceService.createImportLog(
                    type,
                    user.getEmail(),
                    date,
                    "Import voyage " + decorator.toString(entity) + " from file " + file.getName()
            );
            importLog.setEntityId(topiaId);
        }
    }

    @Override
    protected void createLogBookEntry(EchoBaseUser user,
                                      File file) throws TopiaException {

        persistenceService.createEntityModificationLog(
                "Import db",
                "EchoBase (referential + data) db",
                user.getEmail(),
                newDate(),
                "import db from file " + file.getName()
        );
    }

}
