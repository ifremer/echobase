package fr.ifremer.echobase.services.service.importdata.contexts;

/*
 * #%L
 * EchoBase :: Services
 * %%
 * Copyright (C) 2011 - 2016 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.Maps;
import fr.ifremer.echobase.entities.EchoBaseUser;
import fr.ifremer.echobase.entities.data.Cell;
import fr.ifremer.echobase.entities.data.Cells;
import fr.ifremer.echobase.entities.data.Echotype;
import fr.ifremer.echobase.entities.data.Echotypes;
import fr.ifremer.echobase.entities.data.Mooring;
import fr.ifremer.echobase.entities.data.Moorings;
import fr.ifremer.echobase.entities.data.Voyage;
import fr.ifremer.echobase.entities.data.Voyages;
import fr.ifremer.echobase.services.service.UserDbPersistenceService;
import fr.ifremer.echobase.services.service.importdata.configurations.MooringResultsImportConfiguration;
import java.util.Collections;

import java.util.Date;
import java.util.Locale;
import java.util.Map;

/**
 * @author Julien Ruchaud - ruchaud@codelutin.com
 */
public class MooringResultsImportDataContext extends ImportResultsDataContext<MooringResultsImportConfiguration> {

    private Mooring mooring;
    private Map<String, Mooring> mooringsByCode;
    private Map<String, Echotype> mooringEchotypesByName;

    public MooringResultsImportDataContext(UserDbPersistenceService persistenceService, Locale locale, char csvSeparator, MooringResultsImportConfiguration configuration, EchoBaseUser user, Date importDate) {
        super(persistenceService, locale, csvSeparator, configuration, user, importDate);
    }
    
    public final Mooring getMooring() {
        if (mooring == null) {
            mooring = persistenceService.getMooring(configuration.getMooringId());
        }
        return mooring;
    }

    @Override
    public String getEntityId() {
        return configuration.getMooringId();
    }

    public final Map<String, Echotype> getMooringEchotypesByName() {
        if (mooringEchotypesByName == null) {
            mooringEchotypesByName = Maps.uniqueIndex(getMooring().getEchotype(), Echotypes.ECHOTYPE_NAME);
        }
        return mooringEchotypesByName;
    }

    public final Map<String, Mooring> getMooringsByCode() {
        if (mooringsByCode == null) {
            mooringsByCode = Maps.uniqueIndex(Collections.singletonList(getMooring()), Moorings.MOORING_CODE);
        }
        return mooringsByCode;
    }

}
