/*
 * #%L
 * EchoBase :: Services
 * %%
 * Copyright (C) 2011 - 2012 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ifremer.echobase.services.service.importdata.csv;

import fr.ifremer.echobase.entities.data.Echotype;
import fr.ifremer.echobase.entities.data.Voyage;
import fr.ifremer.echobase.entities.references.DepthStratum;
import fr.ifremer.echobase.entities.references.Species;
import fr.ifremer.echobase.services.csv.EchoBaseCsvUtil;
import fr.ifremer.echobase.services.service.importdata.contexts.VoyageResultsImportDataContext;
import static fr.ifremer.echobase.services.service.importdata.csv.ResultsEchotypeImportRow.HEADER_ECHOTYPE_NAME;
import static fr.ifremer.echobase.services.service.importdata.csv.ResultsEchotypeImportRow.HEADER_SPECIES;

/**
 * Model to import echotypes.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 0.3
 */
public class VoyageResultsVoyageEchotypeImportExportModel extends EchoBaseImportExportModelSupport<VoyageResultsVoyageEchotypeImportRow> {

    private VoyageResultsVoyageEchotypeImportExportModel(char separator) {
        super(separator);
    }

    public static VoyageResultsVoyageEchotypeImportExportModel forImport(VoyageResultsImportDataContext importDataContext) {

        VoyageResultsVoyageEchotypeImportExportModel model = new VoyageResultsVoyageEchotypeImportExportModel(importDataContext.getCsvSeparator());
        model.newMandatoryColumn(HEADER_ECHOTYPE_NAME, Echotype.PROPERTY_NAME);
        model.newMandatoryColumn(VoyageResultsVoyageEchotypeImportRow.PROPERTY_MEANING, Echotype.PROPERTY_MEANING);
        model.newMandatoryColumn(VoyageResultsVoyageEchotypeImportRow.PROPERTY_ID, Echotype.PROPERTY_ID);
        model.newForeignKeyColumn(VoyageResultsVoyageEchotypeImportRow.PROPERTY_VOYAGE, Voyage.class, Voyage.PROPERTY_NAME, importDataContext.getVoyagesByName());
        model.newForeignKeyColumn(EchoBaseCsvUtil.DEPTH_STRATUM_ID, Echotype.PROPERTY_DEPTH_STRATUM, DepthStratum.class, DepthStratum.PROPERTY_ID, importDataContext.getDepthStratumsById());
        model.newForeignKeyColumn(HEADER_SPECIES, Echotype.PROPERTY_SPECIES, Species.class, Species.PROPERTY_BARACOUDA_CODE, importDataContext.getSpeciesByBaracoudaCode());
        return model;

    }

    public static VoyageResultsVoyageEchotypeImportExportModel forExport(VoyageResultsImportDataContext importDataContext) {

        VoyageResultsVoyageEchotypeImportExportModel model = new VoyageResultsVoyageEchotypeImportExportModel(importDataContext.getCsvSeparator());
        model.newColumnForExport(HEADER_ECHOTYPE_NAME, Echotype.PROPERTY_NAME);
        model.newColumnForExport(VoyageResultsVoyageEchotypeImportRow.PROPERTY_MEANING, Echotype.PROPERTY_MEANING);
        model.newColumnForExport(VoyageResultsVoyageEchotypeImportRow.PROPERTY_ID, Echotype.PROPERTY_ID);
        model.newColumnForExport(VoyageResultsVoyageEchotypeImportRow.PROPERTY_VOYAGE, EchoBaseCsvUtil.VOYAGE_FORMATTER);
        model.newColumnForExport(EchoBaseCsvUtil.DEPTH_STRATUM_ID, Echotype.PROPERTY_DEPTH_STRATUM, EchoBaseCsvUtil.DEPTH_STRATUM_FORMATTER);
        model.newColumnForExport(HEADER_SPECIES, Echotype.PROPERTY_SPECIES, EchoBaseCsvUtil.SPECIES_FORMATTER);
        return model;

    }

    @Override
    public VoyageResultsVoyageEchotypeImportRow newEmptyInstance() {
        return new VoyageResultsVoyageEchotypeImportRow();
    }
}
