package fr.ifremer.echobase.services.service.importdata;

/*
 * #%L
 * EchoBase :: Services
 * %%
 * Copyright (C) 2011 - 2016 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.echobase.entities.EchoBaseUserEntityEnum;
import fr.ifremer.echobase.entities.references.AgeCategory;
import fr.ifremer.echobase.services.service.UserDbPersistenceService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.Map;
import java.util.TreeMap;

/**
 * Created on 05/04/16.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 4.0
 */
public class AgeCategoryCache {

    /** Logger. */
    private static final Log log = LogFactory.getLog(AgeCategoryCache.class);

    private final Map<String, AgeCategory> cache;
    private final UserDbPersistenceService persistenceService;

    public AgeCategoryCache(UserDbPersistenceService persistenceService, Map<String, AgeCategory> sizeCategoriesByName) {
        this.persistenceService = persistenceService;
        this.cache = new TreeMap<>(sizeCategoriesByName);
    }

    public AgeCategory getAgeCategory(String ageCategoryName, String ageCategoryMeaning, ImportDataFileResult importResult) {

        AgeCategory category = cache.get(ageCategoryName);

        if (category == null) {

            // try to find it in db
            if (log.isInfoEnabled()) {
                log.info("Age category (" + ageCategoryName + ") not found in cache (nor in database), create it.");
            }

            category = persistenceService.createAgeCategory(ageCategoryName, ageCategoryMeaning);
            importResult.incrementsNumberCreated(EchoBaseUserEntityEnum.AgeCategory);

            cache.put(ageCategoryName, category);

        }

        return category;

    }

}
