package fr.ifremer.echobase.services.service.importdata.actions;

/*
 * #%L
 * EchoBase :: Services
 * %%
 * Copyright (C) 2011 - 2019 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.echobase.entities.EchoBaseUserEntityEnum;
import fr.ifremer.echobase.entities.references.Calibration;
import fr.ifremer.echobase.io.InputFile;
import fr.ifremer.echobase.services.service.importdata.ImportDataFileResult;
import fr.ifremer.echobase.services.service.importdata.configurations.VoyageAcousticsImportConfiguration;
import fr.ifremer.echobase.services.service.importdata.contexts.VoyageAcousticsImportDataContext;
import fr.ifremer.echobase.services.service.importdata.csv.CalibrationImportExportModel;
import fr.ifremer.echobase.services.service.importdata.csv.CalibrationImportRow;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.csv.Import;


/**
 * Created on 05/12/19.
 *
 * @author Jean Couteau - couteau@codelutin.com
 * @since 4.2
 */
public class VoyageAcousticsCalibrationImportAction extends ImportCalibrationActionSupport<VoyageAcousticsImportConfiguration, VoyageAcousticsImportDataContext, CalibrationImportRow> {

    /** Logger. */
    private static final Log log = LogFactory.getLog(VoyageAcousticsCalibrationImportAction.class);

    public VoyageAcousticsCalibrationImportAction(VoyageAcousticsImportDataContext importDataContext) {
        super(importDataContext);
    }

    @Override
    protected CalibrationImportExportModel createCsvImportModel(VoyageAcousticsImportDataContext importDataContext) {
        return CalibrationImportExportModel.forImport(importDataContext);
    }

    @Override
    protected CalibrationImportExportModel createCsvExportModel(VoyageAcousticsImportDataContext importDataContext) {
        return CalibrationImportExportModel.forExport(importDataContext);
    }

    @Override
    protected void performImport(VoyageAcousticsImportDataContext importDataContext, InputFile inputFile, ImportDataFileResult result) {

        if (log.isInfoEnabled()) {
            log.info("Starts import of calibrations from file " + inputFile.getFileName());
        }

        try (Import<CalibrationImportRow> importer = open()) {

            incrementsProgress();

            int rowNumber = 0;

            for (CalibrationImportRow row : importer) {

                doFlushTransaction(++rowNumber);

                Calibration createdCalibration = persistenceService.createCalibration(row.getAccuracyEstimate(),
                        row.getAcousticInstrument(),
                        row.getAcquisitionMethod(),
                        row.getComments(),
                        row.getDate(),
                        row.getProcessingMethod(),
                        row.getReport());

                addProcessedRow(result, row);
                addId(result, EchoBaseUserEntityEnum.Calibration, createdCalibration, rowNumber);

            }

        }

    }

    @Override
    protected void computeImportedExport(VoyageAcousticsImportDataContext importDataContext, ImportDataFileResult result) {

        for (Calibration calibration : getImportedEntities(Calibration.class, result)) {

            String calibrationId = calibration.getTopiaId();
            if (log.isInfoEnabled()) {
                log.info("Adding calibration: " + calibrationId + " to imported export.");
            }

            CalibrationImportRow importedRow = CalibrationImportRow.of(calibration);
            addImportedRow(result, importedRow);

        }

    }

}
