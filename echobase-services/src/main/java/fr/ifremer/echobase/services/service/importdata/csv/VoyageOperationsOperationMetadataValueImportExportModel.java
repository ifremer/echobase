/*
 * #%L
 * EchoBase :: Services
 * %%
 * Copyright (C) 2011 - 2012 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ifremer.echobase.services.service.importdata.csv;

import fr.ifremer.echobase.entities.data.Operation;
import fr.ifremer.echobase.entities.data.OperationMetadataValue;
import fr.ifremer.echobase.entities.references.OperationMetadata;
import fr.ifremer.echobase.entities.references.Vessel;
import fr.ifremer.echobase.services.csv.EchoBaseCsvUtil;
import fr.ifremer.echobase.services.service.importdata.contexts.VoyageOperationsImportDataContext;

/**
 * Model to import {@link OperationMetadataValue}.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 0.3
 */
public class VoyageOperationsOperationMetadataValueImportExportModel extends EchoBaseImportExportModelSupport<VoyageOperationsOperationMetadataValueImportRow> {

    private VoyageOperationsOperationMetadataValueImportExportModel(char separator) {
        super(separator);
    }

    public static VoyageOperationsOperationMetadataValueImportExportModel forImport(VoyageOperationsImportDataContext importDataContext) {

        VoyageOperationsOperationMetadataValueImportExportModel model = new VoyageOperationsOperationMetadataValueImportExportModel(importDataContext.getCsvSeparator());
        model.newForeignKeyColumn(EchoBaseCsvUtil.VESSEL_NAME, VoyageOperationsOperationMetadataValueImportRow.PROPERTY_VESSEL, Vessel.class, Vessel.PROPERTY_NAME, importDataContext.getVesselsByName());
        model.newForeignKeyColumn(EchoBaseCsvUtil.OPERATION_ID, VoyageOperationsOperationMetadataValueImportRow.PROPERTY_OPERATION, Operation.class, Operation.PROPERTY_ID, importDataContext.getVoyageOperationsById());
        model.newForeignKeyColumn(VoyageOperationsOperationMetadataValueImportRow.PROPERTY_METADATA_TYPE, OperationMetadataValue.PROPERTY_OPERATION_METADATA, OperationMetadata.class, OperationMetadata.PROPERTY_NAME, importDataContext.getOperationMetadatasByName());
        model.newMandatoryColumn(VoyageOperationsOperationMetadataValueImportRow.PROPERTY_OPERATION_METADATA_VALUE, OperationMetadataValue.PROPERTY_DATA_VALUE);
        return model;

    }

    public static VoyageOperationsOperationMetadataValueImportExportModel forExport(VoyageOperationsImportDataContext importDataContext) {

        VoyageOperationsOperationMetadataValueImportExportModel model = new VoyageOperationsOperationMetadataValueImportExportModel(importDataContext.getCsvSeparator());
        model.newColumnForExport(EchoBaseCsvUtil.VESSEL_NAME, VoyageOperationsOperationMetadataValueImportRow.PROPERTY_VESSEL, EchoBaseCsvUtil.VESSEL_FORMATTER);
        model.newColumnForExport(EchoBaseCsvUtil.OPERATION_ID, VoyageOperationsOperationMetadataValueImportRow.PROPERTY_OPERATION, EchoBaseCsvUtil.OPERATION_FORMATTER);
        model.newColumnForExport(VoyageOperationsOperationMetadataValueImportRow.PROPERTY_METADATA_TYPE, OperationMetadataValue.PROPERTY_OPERATION_METADATA, EchoBaseCsvUtil.OPERATION_FMETADATA_ORMATTER);
        model.newColumnForExport(VoyageOperationsOperationMetadataValueImportRow.PROPERTY_OPERATION_METADATA_VALUE, OperationMetadataValue.PROPERTY_DATA_VALUE);
        return model;

    }

    @Override
    public VoyageOperationsOperationMetadataValueImportRow newEmptyInstance() {
        return new VoyageOperationsOperationMetadataValueImportRow();
    }
}
