package fr.ifremer.echobase.services.service.importdata;

/*
 * #%L
 * EchoBase :: Services
 * %%
 * Copyright (C) 2011 - 2016 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.echobase.entities.EchoBaseUser;
import fr.ifremer.echobase.services.service.importdata.configurations.ImportDataConfigurationSupport;

import java.util.Collections;
import java.util.List;

/**
 * Created on 26/03/16.
 *
 * @author Tony Chemit - chemit@codelutin.com
 */
public class ImportDataResult<C extends ImportDataConfigurationSupport> {

    private final C configuration;

    private final EchoBaseUser user;
    private final String importSummary;

    private final List<ImportDataFileResult> importResults;

    public ImportDataResult(C configuration, EchoBaseUser user, List<ImportDataFileResult> importResults, String importSummary) {
        this.configuration = configuration;
        this.user = user;
        this.importSummary = importSummary;
        this.importResults = Collections.unmodifiableList(importResults);
    }

    public C getConfiguration() {
        return configuration;
    }

    public EchoBaseUser getUser() {
        return user;
    }

    public List<ImportDataFileResult> getImportResults() {
        return importResults;
    }

    public String getImportSummary() {
        return importSummary;
    }
}
