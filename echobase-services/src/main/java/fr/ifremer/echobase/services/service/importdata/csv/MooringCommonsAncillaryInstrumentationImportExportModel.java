/*
 * #%L
 * EchoBase :: Services
 * %%
 * Copyright (C) 2011 - 2012 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ifremer.echobase.services.service.importdata.csv;

import fr.ifremer.echobase.entities.data.Mooring;
import fr.ifremer.echobase.entities.references.AncillaryInstrumentation;
import fr.ifremer.echobase.services.csv.EchoBaseCsvUtil;
import fr.ifremer.echobase.services.service.importdata.contexts.MooringCommonsAncillaryInstrumentationImportDataContext;

/**
 * @author Julien Ruchaud - ruchaud@codelutin.com
 * @since 4.0
 */
public class MooringCommonsAncillaryInstrumentationImportExportModel extends EchoBaseImportExportModelSupport<MooringCommonsAncillaryInstrumentationImportRow> {

    private MooringCommonsAncillaryInstrumentationImportExportModel(char separator) {
        super(separator);
    }

    public static MooringCommonsAncillaryInstrumentationImportExportModel forImport(MooringCommonsAncillaryInstrumentationImportDataContext importDataContext) {
        MooringCommonsAncillaryInstrumentationImportExportModel model = new MooringCommonsAncillaryInstrumentationImportExportModel(importDataContext.getCsvSeparator());

        model.newForeignKeyColumn(MooringCommonsAncillaryInstrumentationImportRow.PROPERTY_MOORING, Mooring.class, Mooring.PROPERTY_CODE, importDataContext.getMooringsByCode());
        model.newForeignKeyColumn(MooringCommonsAncillaryInstrumentationImportRow.PROPERTY_ANCILLARY_INSTRUMENTATION, AncillaryInstrumentation.class, AncillaryInstrumentation.PROPERTY_NAME, importDataContext.getAncillaryInstrumentationsByName());

        return model;
    }

    public static MooringCommonsAncillaryInstrumentationImportExportModel forExport(MooringCommonsAncillaryInstrumentationImportDataContext importDataContext) {
        MooringCommonsAncillaryInstrumentationImportExportModel model = new MooringCommonsAncillaryInstrumentationImportExportModel(importDataContext.getCsvSeparator());

        model.newColumnForExport(MooringCommonsAncillaryInstrumentationImportRow.PROPERTY_MOORING, EchoBaseCsvUtil.MOORING_FORMATTER);
        model.newColumnForExport(MooringCommonsAncillaryInstrumentationImportRow.PROPERTY_ANCILLARY_INSTRUMENTATION, EchoBaseCsvUtil.ANCILLARY_INSTRUMENTATION_FORMATTER);

        return model;
    }

    @Override
    public MooringCommonsAncillaryInstrumentationImportRow newEmptyInstance() {
        return new MooringCommonsAncillaryInstrumentationImportRow();
    }
}
