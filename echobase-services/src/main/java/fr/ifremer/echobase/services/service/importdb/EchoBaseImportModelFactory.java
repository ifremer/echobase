package fr.ifremer.echobase.services.service.importdb;

/*
 * #%L
 * EchoBase :: Services
 * %%
 * Copyright (C) 2011 - 2012 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.echobase.entities.EchoBaseUserEntityEnum;
import fr.ifremer.echobase.services.EchoBaseServiceSupport;
import fr.ifremer.echobase.services.service.DbEditorService;
import org.nuiton.csv.ImportModel;
import org.nuiton.topia.persistence.TopiaEntity;
import org.nuiton.topia.persistence.metadata.AssociationMeta;
import org.nuiton.topia.persistence.metadata.ColumnMeta;
import org.nuiton.topia.persistence.metadata.TableMeta;
import org.nuiton.topia.service.csv.EntityCsvModel;
import org.nuiton.topia.service.csv.in.EntityAssociationImportModel;
import org.nuiton.topia.service.csv.in.ImportModelFactory;

import java.util.Collection;
import java.util.Map;

/**
 * Import model factory.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.2
 */
public class EchoBaseImportModelFactory implements ImportModelFactory<EchoBaseUserEntityEnum> {

    public static ImportModelFactory<EchoBaseUserEntityEnum> newFactory(DbEditorService service) {
        return new EchoBaseImportModelFactory(
                service,
                service.getConfiguration().getCsvSeparator()
        );
    }

    protected final DbEditorService service;

    protected final char csvSeparator;

    public EchoBaseImportModelFactory(DbEditorService service,
                                      char csvSeparator) {
        this.service = service;
        this.csvSeparator = csvSeparator;
    }


    @Override
    public <E extends TopiaEntity> ImportModel<E> buildForImport(TableMeta<EchoBaseUserEntityEnum> meta) {

        EntityCsvModel<EchoBaseUserEntityEnum, E> model = EntityCsvModel.newModel(
                csvSeparator,
                meta,
                TopiaEntity.PROPERTY_TOPIA_ID
        );

        for (ColumnMeta columnMeta : meta) {
            String propertyName = columnMeta.getName();
            Class<?> type = columnMeta.getType();
            if (columnMeta.isFK()) {

                Class<TopiaEntity> entityType = (Class<TopiaEntity>) type;
                Collection<TopiaEntity> universe = service.getForeignData(entityType);
                model.addForeignKeyForImport(propertyName, entityType, universe);
            } else {
                model.addDefaultColumn(propertyName, type);
            }
        }
        return model;
    }

    @Override
    public ImportModel<Map<String, Object>> buildForImport(AssociationMeta<EchoBaseUserEntityEnum> meta) {

        return EntityAssociationImportModel.newImportModel(
                csvSeparator,
                meta
        );
    }

    @Override
    public boolean isNMAssociationMeta(AssociationMeta<EchoBaseUserEntityEnum> meta) {
        EchoBaseUserEntityEnum source = meta.getSource();
        EchoBaseUserEntityEnum target = meta.getTarget();
        boolean result = false;
        if (source == EchoBaseUserEntityEnum.Echotype && target == EchoBaseUserEntityEnum.Species ||
            source == EchoBaseUserEntityEnum.Voyage && target == EchoBaseUserEntityEnum.Strata || 
            source == EchoBaseUserEntityEnum.Transect && target == EchoBaseUserEntityEnum.AncillaryInstrumentation) {
            result = true;
        }
        return result;
    }
}
