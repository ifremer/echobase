package fr.ifremer.echobase.services.service.spatial;

/*
 * #%L
 * EchoBase :: Services
 * %%
 * Copyright (C) 2011 - 2015 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created on 1/22/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.9
 */
public class PgJdbcUrl {

    protected final String url;

    private static final Pattern URL_PATTERN = Pattern.compile("jdbc:postgresql://([^:]+)(:(.+)){0,1}/(.+)");

    private final Matcher matcher;

    public PgJdbcUrl(String url) {
        this.url = url;
        matcher = URL_PATTERN.matcher(url);
        if (!matcher.matches()) {
            throw new IllegalArgumentException(url + " is not a valid pg jdbc url");

        }
    }

    public String getHost() {
        return matcher.group(1);
    }

    public String getPort() {
        String port = matcher.group(3);
        return port == null ? "5432" : port;
    }

    public String getDatabaseName() {
        return matcher.group(4);
    }


}
