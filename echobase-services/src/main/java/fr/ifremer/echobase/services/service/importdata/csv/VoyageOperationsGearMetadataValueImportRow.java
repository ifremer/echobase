/*
 * #%L
 * EchoBase :: Services
 * %%
 * Copyright (C) 2011 - 2012 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ifremer.echobase.services.service.importdata.csv;

import fr.ifremer.echobase.entities.data.GearMetadataValue;
import fr.ifremer.echobase.entities.data.GearMetadataValueImpl;
import fr.ifremer.echobase.entities.data.Operation;
import fr.ifremer.echobase.entities.data.Transect;
import fr.ifremer.echobase.entities.references.Gear;
import fr.ifremer.echobase.entities.references.GearMetadata;
import fr.ifremer.echobase.entities.references.Vessel;

/**
 * Bean used as a row for import of {@link VoyageOperationsGearMetadataValueImportExportModel}.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 0.3
 */
public class VoyageOperationsGearMetadataValueImportRow {

    public static final String PROPERTY_VESSEL = Transect.PROPERTY_VESSEL;
    public static final String PROPERTY_OPERATION = Transect.PROPERTY_OPERATION;
    public static final String PROPERTY_GEAR_METADATA_VALUE = "gearMetadataValue";
    public static final String PROPERTY_METADATA_TYPE = "metadataType";

    protected final GearMetadataValue gearMetadataValue;
    protected Vessel vessel;
    protected Operation operation;

    public static VoyageOperationsGearMetadataValueImportRow of(GearMetadataValue gearMetadataValue) {
        VoyageOperationsGearMetadataValueImportRow row = new VoyageOperationsGearMetadataValueImportRow(gearMetadataValue);
        Operation operation = gearMetadataValue.getOperation();
        row.setOperation(operation);
        row.setVessel(operation.getTransect().getVessel());
        return row;
    }

    public VoyageOperationsGearMetadataValueImportRow(GearMetadataValue gearMetadataValue) {
        this.gearMetadataValue = gearMetadataValue;
    }

    public VoyageOperationsGearMetadataValueImportRow() {
        this(new GearMetadataValueImpl());
    }

    public GearMetadataValue getGearMetadataValue() {
        return gearMetadataValue;
    }

    public GearMetadata getGearMetadata() {
        return getGearMetadataValue().getGearMetadata();
    }

    public void setGearMetadata(GearMetadata gearMetadata) {
        gearMetadataValue.setGearMetadata(gearMetadata);
    }

    public String getDataValue() {
        return getGearMetadataValue().getDataValue();
    }

    public void setDataValue(String dataValue) {
        gearMetadataValue.setDataValue(dataValue);
    }

    public Gear getGear() {
        return getGearMetadataValue().getGear();
    }

    public void setGear(Gear gear) {
        gearMetadataValue.setGear(gear);
    }

    public Operation getOperation() {
        return operation;
    }

    public void setOperation(Operation operation) {
        this.operation = operation;
    }

    public Vessel getVessel() {
        return vessel;
    }

    public void setVessel(Vessel vessel) {
        this.vessel = vessel;
    }
}
