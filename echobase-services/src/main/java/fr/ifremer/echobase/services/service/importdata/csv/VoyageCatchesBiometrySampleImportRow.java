/*
 * #%L
 * EchoBase :: Services
 * %%
 * Copyright (C) 2011 - 2012 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ifremer.echobase.services.service.importdata.csv;

import fr.ifremer.echobase.entities.data.Operation;
import fr.ifremer.echobase.entities.data.SampleData;
import fr.ifremer.echobase.entities.data.SampleDataImpl;
import fr.ifremer.echobase.entities.references.SampleDataType;
import fr.ifremer.echobase.entities.references.SizeCategory;
import fr.ifremer.echobase.entities.references.Species;

/**
 * Bean used as a row for import of {@link VoyageCatchesBiometrySampleImportExportModel}.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 0.3
 */
public class VoyageCatchesBiometrySampleImportRow {

    public static final String PROPERTY_NUM_FISH = "numFish";

    public static final String PROPERTY_OPERATION = "operation";

    public static final String PROPERTY_SPECIES = "species";

    public static final String PROPERTY_SIZE_CATEGORY = "sizeCategory";

    protected final SampleData sampleData;

    protected SizeCategory sizeCategory;

    protected Operation operation;

    protected Species species;

    protected int numFish;

    public static VoyageCatchesBiometrySampleImportRow of(Operation operation, Species species, SizeCategory sizeCategory, SampleData sampleData, int numFish) {

        VoyageCatchesBiometrySampleImportRow row = new VoyageCatchesBiometrySampleImportRow(sampleData);
        row.setOperation(operation);
        row.setSpecies(species);
        row.setNumFish(numFish);
        row.setSizeCategory(sizeCategory);

        return row;
    }

    public VoyageCatchesBiometrySampleImportRow(SampleData sampleData) {
        this.sampleData = sampleData;
    }

    public VoyageCatchesBiometrySampleImportRow() {
        this(new SampleDataImpl());
    }

    public Operation getOperation() {
        return operation;
    }

    public void setOperation(Operation operation) {
        this.operation = operation;
    }

    public Species getSpecies() {
        return species;
    }

    public void setSpecies(Species species) {
        this.species = species;
    }

    public int getNumFish() {
        return numFish;
    }

    public void setNumFish(int numfish) {
        this.numFish = numfish;
    }

    public String getDataLabel() {
        return sampleData.getDataLabel();
    }

    public void setDataLabel(String dataLabel) {
        sampleData.setDataLabel(dataLabel);
    }

    public Float getDataValue() {
        return sampleData.getDataValue();
    }

    public void setDataValue(Float dataValue) {
        sampleData.setDataValue(dataValue);
    }

    public SampleDataType getSampleDataType() {
        return sampleData.getSampleDataType();
    }

    public void setSampleDataType(SampleDataType sampleDataType) {
        sampleData.setSampleDataType(sampleDataType);
    }

    public SampleData getSampleData() {
        return sampleData;
    }

    public SizeCategory getSizeCategory() {
        return sizeCategory;
    }

    public void setSizeCategory(SizeCategory sizeCategory) {
        this.sizeCategory = sizeCategory;
    }
}
