/*
 * #%L
 * EchoBase :: Services
 * %%
 * Copyright (C) 2011 - 2012 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ifremer.echobase.services.service.importdata.csv;

import fr.ifremer.echobase.entities.data.Operation;
import fr.ifremer.echobase.entities.data.OperationImpl;
import fr.ifremer.echobase.entities.data.Transect;
import fr.ifremer.echobase.entities.references.DepthStratum;
import fr.ifremer.echobase.entities.references.Gear;
import fr.ifremer.echobase.entities.references.Vessel;

import java.util.Date;

/**
 * Bean used as a row for import of {@link VoyageOperationsOperationImportExportModel}.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 0.3
 */
public class VoyageOperationsOperationImportRow {

    public static final String PROPERTY_VESSEL = Transect.PROPERTY_VESSEL;

    protected final Operation operation;
    protected Vessel vessel;

    public static VoyageOperationsOperationImportRow of(Operation operation) {
        VoyageOperationsOperationImportRow row = new VoyageOperationsOperationImportRow(operation);
        Transect transect = operation.getTransect();
        row.setVessel(transect.getVessel());
        return row;
    }

    public VoyageOperationsOperationImportRow(Operation operation) {
        this.operation = operation;
    }

    public VoyageOperationsOperationImportRow() {
        this(new OperationImpl());
    }

    public Operation getOperation() {
        return operation;
    }

    public Vessel getVessel() {
        return vessel;
    }

    public void setVessel(Vessel vessel) {
        this.vessel = vessel;
    }

    public String getId() {
        return operation.getId();
    }

    public void setId(String id) {
        operation.setId(id);
    }

    public Date getGearShootingStartTime() {
        return operation.getGearShootingStartTime();
    }

    public void setGearShootingStartTime(Date gearShootingStartTime) {
        operation.setGearShootingStartTime(gearShootingStartTime);
    }

    public Date getGearShootingEndTime() {
        return operation.getGearShootingEndTime();
    }

    public void setGearShootingEndTime(Date gearShootingEndTime) {
        operation.setGearShootingEndTime(gearShootingEndTime);
    }

    public Float getGearShootingStartLatitude() {
        return operation.getGearShootingStartLatitude();
    }

    public void setGearShootingStartLatitude(Float gearShootingStartLatitude) {
        operation.setGearShootingStartLatitude(gearShootingStartLatitude);
    }

    public Float getGearHaulingEndLatitude() {
        return operation.getGearHaulingEndLatitude();
    }

    public void setGearHaulingEndLatitude(Float gearHaulingEndLatitude) {
        operation.setGearHaulingEndLatitude(gearHaulingEndLatitude);
    }

    public Float getGearShootingStartLongitude() {
        return operation.getGearShootingStartLongitude();
    }

    public void setGearShootingStartLongitude(Float gearShootingStartLongitude) {
        operation.setGearShootingStartLongitude(gearShootingStartLongitude);
    }

    public Float getGearHaulingEndLongitude() {
        return operation.getGearHaulingEndLongitude();
    }

    public void setGearHaulingEndLongitude(Float gearHaulingEndLongitude) {
        operation.setGearHaulingEndLongitude(gearHaulingEndLongitude);
    }

    public float getMidHaulLatitude() {
        return operation.getMidHaulLatitude();
    }

    public void setMidHaulLatitude(float midHauleLatitude) {
        operation.setMidHaulLatitude(midHauleLatitude);
    }

    public float getMidHaulLongitude() {
        return operation.getMidHaulLongitude();
    }

    public void setMidHaulLongitude(float midHauleLongitude) {
        operation.setMidHaulLongitude(midHauleLongitude);
    }

    public Gear getGear() {
        return operation.getGear();
    }

    public void setGear(Gear gear) {
        operation.setGear(gear);
    }

    public DepthStratum getDepthStratum() {
        return operation.getDepthStratum();
    }

    public void setDepthStratum(DepthStratum depthStratum) {
        operation.setDepthStratum(depthStratum);
    }
}
