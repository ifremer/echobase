package fr.ifremer.echobase.services.service.importdata;

/*
 * #%L
 * EchoBase :: Services
 * %%
 * Copyright (C) 2011 - 2016 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.echobase.entities.EchoBaseUserEntityEnum;
import fr.ifremer.echobase.entities.references.AgeCategory;
import fr.ifremer.echobase.entities.references.SexCategory;
import fr.ifremer.echobase.entities.references.SizeCategory;
import fr.ifremer.echobase.entities.references.Species;
import fr.ifremer.echobase.entities.references.SpeciesCategory;
import fr.ifremer.echobase.services.service.UserDbPersistenceService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.Map;
import java.util.TreeMap;

/**
 * Created on 29/03/16.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 4.0
 */
public class SpeciesCategoryCache {

    /** Logger. */
    private static final Log log = LogFactory.getLog(SpeciesCategoryCache.class);

    private final Map<String, SpeciesCategory> cache;
    private final UserDbPersistenceService persistenceService;

    public SpeciesCategoryCache(UserDbPersistenceService persistenceService) {
        this.persistenceService = persistenceService;
        this.cache = new TreeMap<>();
    }

    public SpeciesCategory getSpeciesCategory(Species species,
                                              Float lengthClass,
                                              SizeCategory sizeCategory,
                                              AgeCategory ageCategory,
                                              SexCategory sexCategory,
                                              ImportDataFileResult importResult) {

        String key
                = species == null ? "" : species.getBaracoudaCode()
                + "#" + (lengthClass == null ? "" : lengthClass)
                + "#" + (ageCategory == null ? "" : ageCategory.getName())
                + "#" + (sizeCategory == null ? "" : sizeCategory.getName())
                + "#" + (sexCategory == null ? "" : sexCategory.getName());

        SpeciesCategory category = cache.get(key);

        if (category == null) {

            // try to find it in db
            if (log.isInfoEnabled()) {
                log.info("Species category (" + key + ") not found in cache.");
            }

            category = persistenceService.getSpeciesCategory(species, lengthClass, sizeCategory, ageCategory, sexCategory);

            if (category == null) {

                // not found in db, create it
                if (log.isInfoEnabled()) {
                    log.info("Species category (" + key + ") not found in database, create it.");
                }
                category = persistenceService.createSpeciesCategory(species, lengthClass, sizeCategory, ageCategory, sexCategory);
                importResult.incrementsNumberCreated(EchoBaseUserEntityEnum.SpeciesCategory);
            } else {

                if (log.isInfoEnabled()) {
                    log.info("Species category (" + key + ") found in database, add it to cache.");
                }

            }

            cache.put(key, category);

        }

        return category;

    }

}
