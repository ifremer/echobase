/*
 * #%L
 * EchoBase :: Services
 * %%
 * Copyright (C) 2011 - 2012 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ifremer.echobase.services.service.importdata.csv;

import fr.ifremer.echobase.entities.data.Operation;
import fr.ifremer.echobase.entities.data.Sample;
import fr.ifremer.echobase.entities.data.SampleImpl;
import fr.ifremer.echobase.entities.references.SizeCategory;
import fr.ifremer.echobase.entities.references.Species;
import fr.ifremer.echobase.entities.references.SpeciesCategory;

/**
 * Bean used as a row for import of {@link VoyageCatchesTotalSampleImportExportModel}.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 0.3
 */
public class VoyageCatchesTotalSampleImportRow {

    public static final String PROPERTY_OPERATION = "operation";

    public static final String PROPERTY_SPECIES = "species";

    public static final String PROPERTY_SIZE_CATEGORY = "sizeCategory";

    public static final String PROPERTY_MEAN_LENGTH = "meanLength";

    public static final String PROPERTY_MEAN_WEIGHT = "meanWeight";

    public static final String PROPERTY_NO_PER_KG = "noPerKg";

    public static final String PROPERTY_SORTED_WEIGHT = "sortedWeight";
    protected final Sample sample;
    protected Float meanLength;
    protected Float meanWeight;
    protected Float noPerKg;
    protected float sortedWeight;
    protected Operation operation;
    protected Species species;
    protected SizeCategory sizeCategory;

    public VoyageCatchesTotalSampleImportRow() {
        this(new SampleImpl());
    }

    public VoyageCatchesTotalSampleImportRow(Sample sample) {
        this.sample = sample;
    }

    public Sample getSample() {
        return sample;
    }

    public Operation getOperation() {
        return operation;
    }

    public void setOperation(Operation operation) {
        this.operation = operation;
    }

    public Species getSpecies() {
        return species;
    }

    public void setSpecies(Species species) {
        this.species = species;
    }

    public SizeCategory getSizeCategory() {
        return sizeCategory;
    }

    public void setSizeCategory(SizeCategory sizeCategory) {
        this.sizeCategory = sizeCategory;
    }

    public Integer getNumberSampled() {
        return sample.getNumberSampled();
    }

    public void setNumberSampled(Integer numberSampled) {
        sample.setNumberSampled(numberSampled);
    }

    public Float getSampleWeight() {
        return sample.getSampleWeight();
    }

    public void setSampleWeight(Float sampleWeight) {
        sample.setSampleWeight(sampleWeight);
    }

    public Float getMeanLength() {
        return meanLength;
    }

    public void setMeanLength(Float meanLength) {
        this.meanLength = meanLength;
    }

    public Float getMeanWeight() {
        return meanWeight;
    }

    public void setMeanWeight(Float meanWeight) {
        this.meanWeight = meanWeight;
    }

    public Float getNoPerKg() {
        return noPerKg;
    }

    public void setNoPerKg(Float noPerKg) {
        this.noPerKg = noPerKg;
    }

    public float getSortedWeight() {
        return sortedWeight;
    }

    public void setSortedWeight(float sortedWeight) {
        this.sortedWeight = sortedWeight;
    }

    public static VoyageCatchesTotalSampleImportRow of(Operation operation, SpeciesCategory speciesCategory, Sample sample, Float sampleSortedWeight) {
        VoyageCatchesTotalSampleImportRow row = new VoyageCatchesTotalSampleImportRow(sample);
        row.setOperation(operation);
        row.setSpecies(speciesCategory.getSpecies());
        row.setSizeCategory(speciesCategory.getSizeCategory());
        row.setSortedWeight(sampleSortedWeight);
        return row;
    }
}
