package fr.ifremer.echobase.services;

/*
 * #%L
 * EchoBase :: Services
 * %%
 * Copyright (C) 2011 - 2014 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.util.ReflectUtil;

import javax.inject.Inject;
import java.lang.reflect.Field;

/**
 * To inject some stuff.
 *
 * Created on 12/21/13.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.6
 */
public abstract class EchobaseAieOC {

    /** Logger. */
    private static final Log log = LogFactory.getLog(EchobaseAieOC.class);

    protected abstract Object toInject(EchoBaseServiceContext serviceContext, Field field) throws ClassNotFoundException;

    public void inject(EchoBaseServiceContext serviceContext, Object o) throws IllegalAccessException, ClassNotFoundException {

        Iterable<Field> declaredFields = Iterables.filter(
                ReflectUtil.getAllDeclaredFields(o.getClass()),
                new Predicate<Field>() {
                    @Override
                    public boolean apply(Field input) {
                        return input.isAnnotationPresent(Inject.class);
                    }
                });

        for (Field field : declaredFields) {

            Object toInject = toInject(serviceContext, field);

            if (toInject != null) {

                if (log.isDebugEnabled()) {
                    log.debug(toInject + " in action " + o + " in " +
                              field.getName());
                }
                if (!field.isAccessible()) {
                    field.setAccessible(true);
                }
                field.set(o, toInject);
            }
        }
    }
}
