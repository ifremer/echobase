/*
 * #%L
 * EchoBase :: Services
 * %%
 * Copyright (C) 2011 - 2012 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ifremer.echobase.services.service.importdata.csv;

import fr.ifremer.echobase.entities.data.Echotype;
import fr.ifremer.echobase.entities.data.Result;
import fr.ifremer.echobase.entities.data.Voyage;
import fr.ifremer.echobase.entities.references.DataMetadata;
import fr.ifremer.echobase.entities.references.DataQuality;
import fr.ifremer.echobase.entities.references.Species;
import fr.ifremer.echobase.services.csv.EchoBaseCsvUtil;
import fr.ifremer.echobase.services.service.importdata.contexts.VoyageResultsImportDataContext;

import java.util.List;

/**
 * Model to import {@link Result} for esdu cell and species and size category.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 0.3
 */
public class VoyageResultsEsduByEchotypeAndSpeciesCategoryAndLengthImportExportModel extends EchoBaseImportExportModelSupport<VoyageResultsEsduByEchotypeAndSpeciesCategoryAndLengthImportRow> {

    protected static final String HEADER_SPECIES = "baracoudaCode";

    public static final String[] COLUMN_NAMES_TO_EXCLUDE = {
            EchoBaseCsvUtil.CELL_NAME,
            HEADER_SPECIES,
            VoyageResultsEsduByEchotypeAndSpeciesCategoryAndLengthImportRow.PROPERTY_LENGTH_CLASS,
            VoyageResultsEsduByEchotypeAndSpeciesCategoryAndLengthImportRow.PROPERTY_ECHOTYPE,
            VoyageResultsEsduByEchotypeAndSpeciesCategoryAndLengthImportRow.PROPERTY_SIZE_CATEGORY,
            VoyageResultsEsduByEchotypeAndSpeciesCategoryAndLengthImportRow.PROPERTY_LENGTH_CLASS_MEANING,
            VoyageResultsEsduByEchotypeAndSpeciesCategoryAndLengthImportRow.PROPERTY_DATA_QUALITY,
            VoyageResultsEsduByEchotypeAndSpeciesCategoryAndLengthImportRow.PROPERTY_VOYAGE
    };

    private VoyageResultsEsduByEchotypeAndSpeciesCategoryAndLengthImportExportModel(char separator) {
        super(separator);
    }

    public static VoyageResultsEsduByEchotypeAndSpeciesCategoryAndLengthImportExportModel forImport(VoyageResultsImportDataContext importDataContext,
                                                                                                    List<DataMetadata> dataMetadatas) {

        VoyageResultsEsduByEchotypeAndSpeciesCategoryAndLengthImportExportModel model = new VoyageResultsEsduByEchotypeAndSpeciesCategoryAndLengthImportExportModel(importDataContext.getCsvSeparator());
        model.newForeignKeyColumn(VoyageResultsEsduByEchotypeAndSpeciesCategoryAndLengthImportRow.PROPERTY_VOYAGE, Voyage.class, Voyage.PROPERTY_NAME, importDataContext.getVoyagesByName());
        model.newMandatoryColumn(EchoBaseCsvUtil.CELL_NAME, VoyageResultsEsduByEchotypeAndSpeciesCategoryAndLengthImportRow.PROPERTY_CELL, importDataContext.getCellValueParser());
        model.newForeignKeyColumn(VoyageResultsEsduByEchotypeAndSpeciesCategoryAndLengthImportRow.PROPERTY_ECHOTYPE, Echotype.class, Echotype.PROPERTY_NAME, importDataContext.getVoyageEchotypesByName());
        model.newMandatoryColumn(VoyageResultsEsduByEchotypeAndSpeciesCategoryAndLengthImportRow.PROPERTY_LENGTH_CLASS, EchoBaseCsvUtil.NA_TO_FLOAT_PARSER_FORMATTER);
        model.newMandatoryColumn(VoyageResultsEsduByEchotypeAndSpeciesCategoryAndLengthImportRow.PROPERTY_SIZE_CATEGORY);
        model.newMandatoryColumn(VoyageResultsEsduByEchotypeAndSpeciesCategoryAndLengthImportRow.PROPERTY_LENGTH_CLASS_MEANING);
        model.newForeignKeyColumn(HEADER_SPECIES, VoyageResultsEsduByEchotypeAndSpeciesCategoryAndLengthImportRow.PROPERTY_SPECIES, Species.class, Species.PROPERTY_BARACOUDA_CODE, importDataContext.getSpeciesByBaracoudaCode());
        model.newForeignKeyColumn(VoyageResultsEsduByEchotypeAndSpeciesCategoryAndLengthImportRow.PROPERTY_DATA_QUALITY, DataQuality.class, DataQuality.PROPERTY_QUALITY_DATA_FLAG_VALUES, importDataContext.getDataQualitiesByName());

        addResultsColumnsForImport(model, dataMetadatas);
        return model;

    }

    public static VoyageResultsEsduByEchotypeAndSpeciesCategoryAndLengthImportExportModel forExport(VoyageResultsImportDataContext importDataContext,
                                                                                                    List<DataMetadata> dataMetadatas) {

        VoyageResultsEsduByEchotypeAndSpeciesCategoryAndLengthImportExportModel model = new VoyageResultsEsduByEchotypeAndSpeciesCategoryAndLengthImportExportModel(importDataContext.getCsvSeparator());
        model.newColumnForExport(VoyageResultsEsduByEchotypeAndSpeciesCategoryAndLengthImportRow.PROPERTY_VOYAGE, EchoBaseCsvUtil.VOYAGE_FORMATTER);
        model.newColumnForExport(EchoBaseCsvUtil.CELL_NAME, VoyageResultsEsduByEchotypeAndSpeciesCategoryAndLengthImportRow.PROPERTY_CELL, importDataContext.getCellValueFormatter());
        model.newColumnForExport(VoyageResultsEsduByEchotypeAndSpeciesCategoryAndLengthImportRow.PROPERTY_ECHOTYPE, EchoBaseCsvUtil.ECHOTYPE_FORMATTER);
        model.newColumnForExport(VoyageResultsEsduByEchotypeAndSpeciesCategoryAndLengthImportRow.PROPERTY_LENGTH_CLASS, EchoBaseCsvUtil.NA_TO_FLOAT_PARSER_FORMATTER);
        model.newColumnForExport(VoyageResultsEsduByEchotypeAndSpeciesCategoryAndLengthImportRow.PROPERTY_SIZE_CATEGORY);
        model.newColumnForExport(VoyageResultsEsduByEchotypeAndSpeciesCategoryAndLengthImportRow.PROPERTY_LENGTH_CLASS_MEANING);
        model.newColumnForExport(HEADER_SPECIES, VoyageResultsEsduByEchotypeAndSpeciesCategoryAndLengthImportRow.PROPERTY_SPECIES, EchoBaseCsvUtil.SPECIES_FORMATTER);
        model.newColumnForExport(VoyageResultsEsduByEchotypeAndSpeciesCategoryAndLengthImportRow.PROPERTY_DATA_QUALITY, EchoBaseCsvUtil.DATA_QUALITY_FORMATTER);

        addResultsColumns(model, dataMetadatas);
        return model;

    }

    @Override
    public VoyageResultsEsduByEchotypeAndSpeciesCategoryAndLengthImportRow newEmptyInstance() {
        return new VoyageResultsEsduByEchotypeAndSpeciesCategoryAndLengthImportRow();
    }

}
