package fr.ifremer.echobase.services.service.importdata.actions;

/*
 * #%L
 * EchoBase :: Services
 * %%
 * Copyright (C) 2011 - 2016 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.echobase.entities.data.DataAcousticProvider;
import fr.ifremer.echobase.entities.data.Category;
import fr.ifremer.echobase.entities.data.Cell;
import fr.ifremer.echobase.entities.data.Result;
import fr.ifremer.echobase.entities.data.Voyage;
import fr.ifremer.echobase.services.service.importdata.ImportDataFileResult;
import fr.ifremer.echobase.services.service.importdata.ResultCategoryCache;
import fr.ifremer.echobase.services.service.importdata.contexts.VoyageResultsImportDataContext;
import fr.ifremer.echobase.services.service.importdata.csv.VoyageResultsRegionCellResultImportExportModel;
import fr.ifremer.echobase.services.service.importdata.csv.VoyageResultsRegionCellResultImportRow;

import java.util.List;

/**
 * Created on 25/03/16.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 4.0
 */
public class VoyageResultsRegionResultsImportAction extends VoyageResultsCellImportDataActionSupport<VoyageResultsRegionCellResultImportRow> {

    public VoyageResultsRegionResultsImportAction(VoyageResultsImportDataContext importDataContext) {
        super(importDataContext, importDataContext.getConfiguration().getRegionResultFile(), VoyageResultsRegionCellResultImportExportModel.COLUMN_NAMES_TO_EXCLUDE);
    }

    @Override
    protected VoyageResultsRegionCellResultImportExportModel createCsvImportModel(VoyageResultsImportDataContext importDataContext) {
        return VoyageResultsRegionCellResultImportExportModel.forImport(importDataContext, metas);
    }

    @Override
    protected VoyageResultsRegionCellResultImportExportModel createCsvExportModel(VoyageResultsImportDataContext importDataContext) {
        return VoyageResultsRegionCellResultImportExportModel.forExport(importDataContext, metas);

    }

    @Override
    protected Category getResultCategory(ImportDataFileResult result, ResultCategoryCache resultCategoryCache, VoyageResultsRegionCellResultImportRow row) {
        return resultCategoryCache.getResultCategory(row.getEchotype(),
                                                     row.getSpecies(),
                                                     null,
                                                     row.getSizeCategory(),
                                                     null,
                                                     result);
    }

    @Override
    protected VoyageResultsRegionCellResultImportRow newImportedRow(DataAcousticProvider voyage, Cell cell, Category category, List<Result> cellResults) {
        return VoyageResultsRegionCellResultImportRow.of(voyage, cell, category, cellResults);
    }
}
