package fr.ifremer.echobase.services.service.importdata.contexts;

/*
 * #%L
 * EchoBase :: Services
 * %%
 * Copyright (C) 2011 - 2016 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import fr.ifremer.echobase.entities.EchoBaseUser;
import fr.ifremer.echobase.entities.data.Cell;
import fr.ifremer.echobase.entities.data.Cells;
import fr.ifremer.echobase.entities.data.DataProcessing;
import fr.ifremer.echobase.entities.references.DataMetadata;
import fr.ifremer.echobase.entities.references.DataMetadataImpl;
import fr.ifremer.echobase.io.InputFile;
import fr.ifremer.echobase.services.csv.EchoBaseCsvUtil;
import fr.ifremer.echobase.services.service.UserDbPersistenceService;
import fr.ifremer.echobase.services.service.importdata.AgeCategoryCache;
import fr.ifremer.echobase.services.service.importdata.DataMetadataNotFoundException;
import fr.ifremer.echobase.services.service.importdata.SizeCategoryCache;
import org.nuiton.csv.ValueFormatter;
import org.nuiton.csv.ValueParser;
import org.nuiton.csv.ext.CsvReaders;

import java.util.Date;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;

import static fr.ifremer.echobase.services.service.importdata.ImportDataService.REMOVE_DOUBLE_QUOTES_PATTERN;
import fr.ifremer.echobase.services.service.importdata.ResultCategoryCache;
import fr.ifremer.echobase.services.service.importdata.SpeciesCategoryCache;
import fr.ifremer.echobase.services.service.importdata.configurations.ImportResultsConfigurationSupport;

/**
 * @author Julien Ruchaud - ruchaud@codelutin.com
 */
public abstract class ImportResultsDataContext<C extends ImportResultsConfigurationSupport> extends ImportDataContextSupport<C> {

    private SpeciesCategoryCache speciesCategoryCache;
    private ResultCategoryCache resultCategoryCache;

    private DataMetadata gridCellLongitudeMeta;
    private DataMetadata gridCellLatitudeMeta;
    private DataMetadata gridCellDepthMeta;
    private DataMetadata gridLongitudeLagMeta;
    private DataMetadata gridLatitudeLagMeta;
    private DataMetadata gridDepthLagMeta;
    private DataMetadata regionEnvCoordinateMeta;
    private DataMetadata surfaceMeta;

    private Set<Cell> esduCells;

    private Map<String, Cell> esduCellsByName;

    private SizeCategoryCache sizeCategoryCache;
    private AgeCategoryCache ageCategoryCache;

    public ImportResultsDataContext(UserDbPersistenceService persistenceService, Locale locale, char csvSeparator, C configuration, EchoBaseUser user, Date importDate) {
        super(persistenceService, locale, csvSeparator, configuration, user, importDate);
    }

    public SizeCategoryCache getSizeCategoryCache() {
        if (sizeCategoryCache == null) {
            sizeCategoryCache = new SizeCategoryCache(persistenceService, getSizeCategoriesByName());
        }
        return sizeCategoryCache;
    }

    public AgeCategoryCache getAgeCategoryCache() {
        if (ageCategoryCache == null) {
            ageCategoryCache = new AgeCategoryCache(persistenceService, getAgeCategoriesByName());
        }
        return ageCategoryCache;
    }

    public ValueParser<Cell> getCellValueParser() {
        return EchoBaseCsvUtil.newCellValueParser(getEsduCellsByName());
    }

    public ValueFormatter<Cell> getCellValueFormatter() {
        return EchoBaseCsvUtil.newCellValueFormatter(getEsduCells(), getEsduCellType(), getElementaryCellType());
    }

    public ValueParser<Cell> newCellValueParser() {
        return persistenceService.newCellValueParser();
    }

    public ValueFormatter<Cell> newCellValueFormatter() {
        return persistenceService.newCellValueFormatter();
    }

    public Map<String, Cell> getEsduCellsByName() {
        if (esduCellsByName == null) {
            esduCellsByName = Maps.uniqueIndex(getEsduCells(), Cells.CELL_BY_NAME);
        }
        return esduCellsByName;
    }

    public Set<Cell> getEsduCells() {
        if (esduCells == null) {

            // get selected dataProcessing
            DataProcessing dataProcessing = persistenceService.getDataProcessing(configuration.getDataProcessingId());

            // get esdu cells usables
            esduCells = new LinkedHashSet<>(dataProcessing.getCell());

        }
        return esduCells;
    }

    public List<DataMetadata> getMetas(InputFile inputFile, String... columnNamesToExclude) {

        Map<String, DataMetadata> dataMetadatasByName = getDataMetadatasByName();

        String[] headers = CsvReaders.getHeader(inputFile.getFile(), csvSeparator);

        List<String> metadataNames = Lists.newArrayList(headers);
        for (String columnToExclude : columnNamesToExclude) {
            metadataNames.remove(columnToExclude);
            metadataNames.remove("\"" + columnToExclude + "\"");
        }

        List<DataMetadata> result = Lists.newArrayList();

        for (String metadataName : metadataNames) {

            Matcher matcher = REMOVE_DOUBLE_QUOTES_PATTERN.matcher(metadataName);
            if (matcher.matches()) {
                metadataName = matcher.group(1);
            }
            DataMetadata dataMetadata = dataMetadatasByName.get(metadataName);
            if (dataMetadata == null) {
                throw new DataMetadataNotFoundException(getLocale(), metadataName, dataMetadatasByName.keySet());
            }
            result.add(dataMetadata);

        }

        return result;

    }

    public DataMetadata getRegionEnvCoordinateMeta() {
        if (regionEnvCoordinateMeta == null) {
            regionEnvCoordinateMeta = persistenceService.getDataMetadataByName(DataMetadataImpl.REGION_ENV_COORDINATES);
        }
        return regionEnvCoordinateMeta;
    }

    public DataMetadata getSurfaceMeta() {
        if (surfaceMeta == null) {
            surfaceMeta = persistenceService.getDataMetadataByName(DataMetadataImpl.SURFACE);
        }
        return surfaceMeta;
    }

    public final DataMetadata getGridCellLongitudeMeta() {
        if (gridCellLongitudeMeta == null) {
            gridCellLongitudeMeta = persistenceService.getDataMetadataByName(DataMetadataImpl.GRID_CELL_LONGITUDE);
        }
        return gridCellLongitudeMeta;
    }

    public final DataMetadata getGridCellLatitudeMeta() {
        if (gridCellLatitudeMeta == null) {
            gridCellLatitudeMeta = persistenceService.getDataMetadataByName(DataMetadataImpl.GRID_CELL_LATITUDE);
        }
        return gridCellLatitudeMeta;
    }

    public final DataMetadata getGridCellDepthMeta() {
        if (gridCellDepthMeta == null) {
            gridCellDepthMeta = persistenceService.getDataMetadataByName(DataMetadataImpl.GRID_CELL_DEPTH);
        }
        return gridCellDepthMeta;
    }

    public final DataMetadata getGridLongitudeLagMeta() {
        if (gridLongitudeLagMeta == null) {
            gridLongitudeLagMeta = persistenceService.getDataMetadataByName(DataMetadataImpl.GRID_LONGITUDE_LAG);
        }
        return gridLongitudeLagMeta;
    }

    public final DataMetadata getGridLatitudeLagMeta() {
        if (gridLatitudeLagMeta == null) {
            gridLatitudeLagMeta = persistenceService.getDataMetadataByName(DataMetadataImpl.GRID_LATITUDE_LAG);
        }
        return gridLatitudeLagMeta;
    }

    public final DataMetadata getGridDepthLagMeta() {
        if (gridDepthLagMeta == null) {
            gridDepthLagMeta = persistenceService.getDataMetadataByName(DataMetadataImpl.GRID_DEPTH_LAG);
        }
        return gridDepthLagMeta;
    }

    public SpeciesCategoryCache getSpeciesCategoryCache() {
        if (speciesCategoryCache == null) {
            speciesCategoryCache = new SpeciesCategoryCache(persistenceService);
        }
        return speciesCategoryCache;
    }

    public ResultCategoryCache getResultCategoryCache() {
        if (resultCategoryCache == null) {
            resultCategoryCache = new ResultCategoryCache(persistenceService, getSpeciesCategoryCache());
        }
        return resultCategoryCache;
    }

}
