package fr.ifremer.echobase.services.service.exportCoser;

/*
 * #%L
 * EchoBase :: Services
 * %%
 * Copyright (C) 2011 - 2014 Ifremer, Codelutin, Chemit Tony
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.echobase.entities.references.DataMetadata;
import fr.ifremer.echobase.entities.references.Mission;
import fr.ifremer.echobase.entities.references.Species;

import java.util.Date;

/**
 * Created on 3/20/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.6
 */
public class IndicatorExportRow {

    public static final String PROPERTY_MISSION_NAME = "mission." + Mission.PROPERTY_NAME;

    public static final String PROPERTY_INDICATOR_NAME = "indicator." + DataMetadata.PROPERTY_NAME;

    public static final String PROPERTY_SPECIES = "species";

    public static final String PROPERTY_STRATUM = "stratum";

    public static final String PROPERTY_DATE = "date";

    public static final String PROPERTY_ESTIMATION = "estimation";

    public static final String PROPERTY_STANDARD_DEVIATION = "standardDeviation";

    public static final String PROPERTY_CV = "cv";

    protected Mission mission;

    protected DataMetadata indicator;

    protected Species species;

    protected String stratum;

    protected Date date;

    protected float estimation;

    protected float standardDeviation;

    public Mission getMission() {
        return mission;
    }

    public DataMetadata getIndicator() {
        return indicator;
    }

    public Species getSpecies() {
        return species;
    }

    public String getStratum() {
        return stratum;
    }

    public Date getDate() {
        return date;
    }

    public float getEstimation() {
        return estimation;
    }

    public float getStandardDeviation() {
        return standardDeviation;
    }

    public float getCv() {
        return standardDeviation / estimation;
    }

    public void setMission(Mission mission) {
        this.mission = mission;
    }

    public void setIndicator(DataMetadata indicator) {
        this.indicator = indicator;
    }

    public void setSpecies(Species species) {
        this.species = species;
    }

    public void setStratum(String stratum) {
        this.stratum = stratum;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public void setEstimation(float estimation) {
        this.estimation = estimation;
    }

    public void setStandardDeviation(float standardDeviation) {
        this.standardDeviation = standardDeviation;
    }
}
