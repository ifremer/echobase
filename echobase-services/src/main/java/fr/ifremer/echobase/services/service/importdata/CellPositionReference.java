/*
 * #%L
 * EchoBase :: Services
 * %%
 * Copyright (C) 2011 - 2012 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ifremer.echobase.services.service.importdata;

import fr.ifremer.echobase.I18nAble;

import static org.nuiton.i18n.I18n.n;

/**
 * To define cell position reference in acoustic import.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 0.3
 */
public enum CellPositionReference implements I18nAble {

    /** Begin. */
    START(n("echobase.common.cellPositionReference.start")) {
        @Override
        public String getMetadataNameSuffix() {
            return "Start";
        }
    },

    /** Mid. */
    MID(n("echobase.common.cellPositionReference.mid")) {
        @Override
        public String getMetadataNameSuffix() {
            return "Bary";
        }
    },

    /** End. */
    END(n("echobase.common.cellPositionReference.end")) {
        @Override
        public String getMetadataNameSuffix() {
            return "End";
        }
    };

    private final String i18nKey;

    CellPositionReference(String i18nKey) {
        this.i18nKey = i18nKey;
    }

    @Override
    public String getI18nKey() {
        return i18nKey;
    }

    public abstract String getMetadataNameSuffix();
}
