package fr.ifremer.echobase.services.service.importdata.actions;

/*
 * #%L
 * EchoBase :: Services
 * %%
 * Copyright (C) 2011 - 2016 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.base.Preconditions;
import fr.ifremer.echobase.entities.EchoBaseUserEntityEnum;
import fr.ifremer.echobase.entities.ImportFileId;
import fr.ifremer.echobase.entities.data.Category;
import fr.ifremer.echobase.entities.data.Cell;
import fr.ifremer.echobase.entities.data.Data;
import fr.ifremer.echobase.entities.data.Result;
import fr.ifremer.echobase.entities.data.Voyage;
import fr.ifremer.echobase.entities.references.CellType;
import fr.ifremer.echobase.entities.references.DataMetadata;
import fr.ifremer.echobase.entities.references.DataQuality;
import fr.ifremer.echobase.io.InputFile;
import fr.ifremer.echobase.services.service.importdata.ImportDataFileResult;
import fr.ifremer.echobase.services.service.importdata.MismatchProviderException;
import fr.ifremer.echobase.services.service.importdata.ResultCategoryCache;
import fr.ifremer.echobase.services.service.importdata.contexts.VoyageResultsImportDataContext;
import fr.ifremer.echobase.services.service.importdata.csv.VoyageResultsMapFishCellImportExportModel;
import fr.ifremer.echobase.services.service.importdata.csv.VoyageResultsMapFishCellImportRow;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.csv.Import;

import java.util.List;
import java.util.Map;

/**
 * Created on 25/03/16.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 4.0
 */
public class VoyageResultsMapFishCellImportAction extends VoyageResultsImportDataActionSupport<VoyageResultsMapFishCellImportRow> {

    /** Logger. */
    private static final Log log = LogFactory.getLog(VoyageResultsMapFishCellImportAction.class);

    private final List<DataMetadata> metas;
    private final DataMetadata dataLongitudeMeta;
    private final DataMetadata dataLatitudeMeta;
    private final DataMetadata dataDepthMeta;
    private final DataMetadata dataLongitudeLagMeta;
    private final DataMetadata dataLatitudeLagMeta;
    private final DataMetadata dataDepthLagMeta;
    private final Map<String, List<String>> cellIdToCategoryId = new LinkedHashMap<>();

    public VoyageResultsMapFishCellImportAction(VoyageResultsImportDataContext importDataContext) {
        super(importDataContext, importDataContext.getConfiguration().getMapsFile());
        metas = importDataContext.getMetas(getInputFile(), VoyageResultsMapFishCellImportExportModel.COLUMN_NAMES_TO_EXCLUDE);
        dataLongitudeMeta = importDataContext.getGridCellLongitudeMeta();
        dataLatitudeMeta = importDataContext.getGridCellLatitudeMeta();
        dataDepthMeta = importDataContext.getGridCellDepthMeta();
        dataLongitudeLagMeta = importDataContext.getGridLongitudeLagMeta();
        dataLatitudeLagMeta = importDataContext.getGridLatitudeLagMeta();
        dataDepthLagMeta = importDataContext.getGridDepthLagMeta();
    }

    @Override
    protected VoyageResultsMapFishCellImportExportModel createCsvImportModel(VoyageResultsImportDataContext importDataContext) {
        return VoyageResultsMapFishCellImportExportModel.forImport(importDataContext, metas);
    }

    @Override
    protected VoyageResultsMapFishCellImportExportModel createCsvExportModel(VoyageResultsImportDataContext importDataContext) {
        return VoyageResultsMapFishCellImportExportModel.forExport(importDataContext, metas);
    }

    @Override
    protected void performImport(VoyageResultsImportDataContext importDataContext, InputFile inputFile, ImportDataFileResult result) {

        if (log.isInfoEnabled()) {
            log.info("Starts import of Map cells from file " + inputFile.getFileName());
        }

        Voyage expectedVoyage = importDataContext.getVoyage();
        ResultCategoryCache resultCategoryCache = importDataContext.getResultCategoryCache();

        String resultLabel = getConfiguration().getResultLabel();

        try (Import<VoyageResultsMapFishCellImportRow> importer = open()) {

            incrementsProgress();

            int rowNumber = 0;
            for (VoyageResultsMapFishCellImportRow row : importer) {

                doFlushTransaction(++rowNumber);

                Voyage voyage = row.getVoyage();
                if (!expectedVoyage.equals(voyage)) {
                    throw new MismatchProviderException(getLocale(), rowNumber, voyage.getName());
                }

                Cell rowCell = row.getCell();
                String cellName = rowCell.getName();
                CellType cellType = rowCell.getCellType();
                
                Cell cell = persistenceService.getCellByVoyageNameAndType(cellName, cellType, voyage);
                if (cell == null) {
                    cell = persistenceService.createCell(rowCell);
                    voyage.addPostCell(cell);
                }

                // collect ids
                addId(result, EchoBaseUserEntityEnum.Cell, cell, rowNumber);

                addProcessedRow(result, row);

                DataQuality dataQuality = row.getDataQuality();

                // add gridCellLongitude data
                createCellData(cell,
                               dataLongitudeMeta,
                               String.valueOf(row.getGridCellLongitude()),
                               dataQuality,
                               result,
                               false, rowNumber);

                // add gridCellLatitude data
                createCellData(cell,
                               dataLatitudeMeta,
                               String.valueOf(row.getGridCellLatitude()),
                               dataQuality,
                               result,
                               false, rowNumber);

                // add gridCellDepth data
                createCellData(cell,
                               dataDepthMeta,
                               String.valueOf(row.getGridCellDepth()),
                               dataQuality,
                               result,
                               false, rowNumber);

                // add gridLongitudeLag data
                createCellData(cell,
                               dataLongitudeLagMeta,
                               String.valueOf(row.getGridLongitudeLag()),
                               dataQuality,
                               result,
                               false, rowNumber);

                // add gridLatitudeLag data
                createCellData(cell,
                               dataLatitudeLagMeta,
                               String.valueOf(row.getGridLatitudeLag()),
                               dataQuality,
                               result,
                               false, rowNumber);

                // add gridDepthLag data
                createCellData(cell,
                               dataDepthLagMeta,
                               String.valueOf(row.getGridDepthLag()),
                               dataQuality,
                               result,
                               false, rowNumber);

                Category category = resultCategoryCache.getResultCategory(null,
                                                                          row.getSpecies(),
                                                                          null,
                                                                          row.getSizeCategory(),
                                                                          row.getAgeCategory(),
                                                                          result);

                String topiaId = cell.getTopiaId();
                List<String> categories = cellIdToCategoryId.get(topiaId);
                if (categories == null) {
                    categories = new ArrayList<>();
                    cellIdToCategoryId.put(topiaId, categories);
                }
                categories.add(category.getTopiaId());

                addResults(row, cell, category, resultLabel, result, false, true, rowNumber);

            }

        }

    }

    @Override
    protected void computeImportedExport(VoyageResultsImportDataContext importDataContext, ImportDataFileResult result) {

        String voyageId = importDataContext.getConfiguration().getVoyageId();
        Voyage voyage = persistenceService.getVoyage(voyageId);

        for (ImportFileId importFileId : getImportFileIds(result)) {
            String cellId = importFileId.getEntityId();
            Cell cell = persistenceService.getCell(cellId);
            Preconditions.checkNotNull(cell);

            if (log.isInfoEnabled()) {
                log.info("Cell has changed, flushing cell: " + cell);
            }
            
            List<String> categories = cellIdToCategoryId.remove(cellId);
            if (categories != null) {
                
                for (String categoryId : categories) {

                    Preconditions.checkNotNull(categoryId);
                    Category category = persistenceService.getCategory(categoryId);
                    Preconditions.checkNotNull(category);


                    List<Result> results = new ArrayList<Result>();
                    if (cell.isResultNotEmpty()) {

                        // check category is matching
                        for (Result cellResult : cell.getResult()) {
                            if (category.equals(cellResult.getCategory())) {
                                results.add(cellResult);
                            }
                        }

                    }

                    VoyageResultsMapFishCellImportRow row = VoyageResultsMapFishCellImportRow.of(voyage, cell, category, results);

                    for (Data data : cell.getData()) {

                        Float dataValue = Float.valueOf(data.getDataValue());
                        DataMetadata dataMetadata = data.getDataMetadata();

                        if (dataLatitudeMeta.equals(dataMetadata)) {
                            row.setGridCellLatitude(dataValue);
                        } else if (dataLongitudeMeta.equals(dataMetadata)) {
                            row.setGridCellLongitude(dataValue);
                        } else if (dataDepthMeta.equals(dataMetadata)) {
                            row.setGridCellDepth(dataValue);
                        } else if (dataLatitudeLagMeta.equals(dataMetadata)) {
                            row.setGridLatitudeLag(dataValue);
                        } else if (dataLongitudeLagMeta.equals(dataMetadata)) {
                            row.setGridLongitudeLag(dataValue);
                        } else if (dataDepthLagMeta.equals(dataMetadata)) {
                            row.setGridDepthLag(dataValue);
                        } else {
                            throw new IllegalStateException("Cant deal with data of metadata: " + dataMetadata);
                        }

                    }

                    addImportedRow(result, row);
                }
            }
        }
    }
}
