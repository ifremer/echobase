package fr.ifremer.echobase.services.service.removedata.strategy;

/*
 * #%L
 * EchoBase :: Services
 * %%
 * Copyright (C) 2011 - 2012 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.echobase.entities.ImportLog;
import fr.ifremer.echobase.entities.ImportType;
import fr.ifremer.echobase.entities.data.Operation;
import fr.ifremer.echobase.entities.data.Sample;
import fr.ifremer.echobase.entities.data.Voyage;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.persistence.TopiaException;

import java.util.Collections;
import java.util.Set;
import fr.ifremer.echobase.entities.data.DataAcousticProvider;

/**
 * Remove a {@link ImportType#CATCHES} import.
 *
 * Can remove only  {@link Sample}.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.2
 */
public class CatchesRemoveDataStrategy extends AbstractRemoveDataStrategy<Voyage> {

    /** Logger. */
    private static final Log log =
            LogFactory.getLog(CatchesRemoveDataStrategy.class);

    @Override
    public long computeNbSteps(DataAcousticProvider<Voyage> provider, ImportLog importLog) {
        return getImportFileIdsCount(importLog);
    }

    @Override
    protected void removeImportData(DataAcousticProvider<Voyage> provider, String id) throws TopiaException {
        if (id.startsWith(Sample.class.getName())) {

            // get sample
            Sample sample = persistenceService.getSample(id);

            // remove it from operation
            Operation operation = persistenceService.getOperationContainsSample(sample);
            operation.removeSample(sample);

            // remove sample
            persistenceService.deleteSample(sample);
            if (log.isDebugEnabled()) {
                log.debug(sample.getTopiaId() + " was removed");
            }
        } else {
            canNotDealWithId(id);
        }
    }

    @Override
    public Set<ImportType> getPossibleSubImportType() {
        return Collections.emptySet();
    }
}
