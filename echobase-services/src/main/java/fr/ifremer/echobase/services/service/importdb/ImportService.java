/*
 * #%L
 * EchoBase :: Services
 * %%
 * Copyright (C) 2011 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ifremer.echobase.services.service.importdb;

import com.google.common.base.Charsets;
import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import com.google.common.io.Files;
import fr.ifremer.echobase.EchoBaseTechnicalException;
import fr.ifremer.echobase.entities.EchoBaseUser;
import fr.ifremer.echobase.entities.EchoBaseUserEntityEnum;
import fr.ifremer.echobase.entities.references.Gear;
import fr.ifremer.echobase.entities.references.GearCharacteristic;
import fr.ifremer.echobase.entities.references.GearCharacteristicValue;
import fr.ifremer.echobase.services.EchoBaseServiceSupport;
import fr.ifremer.echobase.services.service.DbEditorService;
import fr.ifremer.echobase.services.service.UserDbPersistenceService;
import org.apache.commons.io.IOUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.csv.Import;
import org.nuiton.csv.ImportModel;
import org.nuiton.topia.persistence.TopiaEntity;
import org.nuiton.topia.persistence.metadata.TableMeta;
import org.nuiton.topia.service.csv.in.CsvImportResult;
import org.nuiton.topia.service.csv.in.ImportModelFactory;
import org.nuiton.util.beans.BeanMonitor;

import javax.inject.Inject;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.Map;

/**
 * To import datas from a csv file into the db.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 0.2
 */
public class ImportService extends EchoBaseServiceSupport {

    /** Logger. */
    private static final Log log = LogFactory.getLog(ImportService.class);

    @Inject
    private UserDbPersistenceService persistenceService;

    @Inject
    private DbEditorService dbEditorService;

    public CsvImportResult<EchoBaseUserEntityEnum> importDatas(EchoBaseUserEntityEnum entityType,
                                                               String importFileName,
                                                               File importFile,
                                                               boolean createIfNotFound,
                                                               EchoBaseUser user) throws IOException {

        ImportModelFactory<EchoBaseUserEntityEnum> importModelFactory =
                EchoBaseImportModelFactory.newFactory(dbEditorService);

        TableMeta<EchoBaseUserEntityEnum> meta = dbEditorService.getTableMeta(entityType);
        ImportModel<TopiaEntity> csvModel = importModelFactory.buildForImport(meta);
        String messagePrefix = "Import du fichier " + importFileName + " le " + newDate();

        CsvImportResult<EchoBaseUserEntityEnum> result = CsvImportResult.newResult(
                entityType, importFileName, createIfNotFound);

        BufferedReader bf = Files.newReader(importFile, Charsets.UTF_8);
        try {

            Import<TopiaEntity> importer = Import.newImport(csvModel, bf);

            try {
                for (TopiaEntity entity : importer) {
                    boolean create = dbEditorService.saveEntity(meta,
                                                                messagePrefix,
                                                                entity,
                                                                user,
                                                                createIfNotFound
                    );

                    if (create) {

                        result.incrementsNumberCreated();
                    } else {

                        result.incrementsNumberUpdated();
                    }
                }
                importer.close();

            } finally {

                IOUtils.closeQuietly(importer);
            }

            bf.close();

            persistenceService.commit();
        } catch (EchoBaseTechnicalException eee) {
            throw eee;
        } catch (Exception eee) {
            log.error("Failed to read import file " + importFile.getName(), eee);
            throw new EchoBaseTechnicalException(eee);
        } finally {
            IOUtils.closeQuietly(bf);
        }
        return result;
    }

    public CsvImportResult<EchoBaseUserEntityEnum> importGearCharacteristicValues(String importFileName,
                                                                                  File importFile,
                                                                                  EchoBaseUser user) throws IOException {

        // On importe et regroupe les caractéristiques par engin
        Multimap<Gear, GearCharacteristicValue> gearCharacteristicValuesByGear = ArrayListMultimap.create();

        try (BufferedReader bf = Files.newReader(importFile, Charsets.UTF_8)) {

            Map<String, Gear> gearsByCasinoGearName = persistenceService.getEntitiesMap(Gear.class, Gear::getCasinoGearName);
            Map<String, GearCharacteristic> gearCharacteristicsByName = persistenceService.getEntitiesMap(GearCharacteristic.class, GearCharacteristic::getName);
            GearCharacteristicValuesImportModel csvModel = GearCharacteristicValuesImportModel.forImport(getCsvSeparator(), gearsByCasinoGearName, gearCharacteristicsByName);

            try (Import<GearCharacteristicValuesImportRow> importer = Import.newImport(csvModel, bf)) {

                for (GearCharacteristicValuesImportRow gearCharacteristicValuesImportRow : importer) {

                    Gear gear = gearCharacteristicValuesImportRow.getGear();
                    GearCharacteristicValue gearCharacteristicValue = gearCharacteristicValuesImportRow.getGearCharacteristicValue();
                    gearCharacteristicValuesByGear.put(gear, gearCharacteristicValue);

                }

            }

        } catch (EchoBaseTechnicalException eee) {
            throw eee;
        } catch (Exception eee) {
            log.error("Failed to read import file " + importFile.getName(), eee);
            throw new EchoBaseTechnicalException(eee);
        }

        String messagePrefix = "Import des caractéristiques d'engin à partir du fichier " + importFileName + " le " + newDate();

        CsvImportResult<EchoBaseUserEntityEnum> result = CsvImportResult.newResult(EchoBaseUserEntityEnum.Gear, importFileName, false);

        TableMeta<EchoBaseUserEntityEnum> meta = dbEditorService.getTableMeta(EchoBaseUserEntityEnum.Gear);
        BeanMonitor beanMonitor = new BeanMonitor();

        for (Map.Entry<Gear, Collection<GearCharacteristicValue>> gearCollectionEntry : gearCharacteristicValuesByGear.asMap().entrySet()) {

            Gear gear = gearCollectionEntry.getKey();
            beanMonitor.setBean(gear);

            Collection<GearCharacteristicValue> gearCharacteristicValues = gearCollectionEntry.getValue();
            gear.addAllGearCharacteristicValue(gearCharacteristicValues);

            result.incrementsNumberUpdated();

            if (user != null) {

                dbEditorService.createEntityModificationLog(
                        meta,
                        messagePrefix,
                        gear,
                        user,
                        beanMonitor);

            }

        }

        persistenceService.commit();

        return result;

    }

}
