package fr.ifremer.echobase.services.service.importdata.actions;

/*
 * #%L
 * EchoBase :: Services
 * %%
 * Copyright (C) 2011 - 2016 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.echobase.entities.EchoBaseUserEntityEnum;
import fr.ifremer.echobase.entities.data.Transit;
import fr.ifremer.echobase.entities.data.Voyage;
import fr.ifremer.echobase.io.InputFile;
import fr.ifremer.echobase.services.service.importdata.DuplicatedTransitException;
import fr.ifremer.echobase.services.service.importdata.ImportDataFileResult;
import fr.ifremer.echobase.services.service.importdata.contexts.VoyageCommonsImportDataContext;
import fr.ifremer.echobase.services.service.importdata.csv.VoyageCommonsTransitImportExportModel;
import fr.ifremer.echobase.services.service.importdata.csv.VoyageCommonsTransitImportRow;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.csv.Import;

/**
 * Created on 25/03/16.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 4.0
 */
public class VoyageCommonsTransitImportAction extends VoyageCommonsImportDataActionSupport<VoyageCommonsTransitImportRow> {

    /** Logger. */
    private static final Log log = LogFactory.getLog(VoyageCommonsTransitImportAction.class);

    public VoyageCommonsTransitImportAction(VoyageCommonsImportDataContext importDataContext) {
        super(importDataContext, importDataContext.getConfiguration().getTransitFile());
    }

    @Override
    protected VoyageCommonsTransitImportExportModel createCsvImportModel(VoyageCommonsImportDataContext importDataContext) {
        return VoyageCommonsTransitImportExportModel.forImport(importDataContext);
    }

    @Override
    protected VoyageCommonsTransitImportExportModel createCsvExportModel(VoyageCommonsImportDataContext importDataContext) {
        return VoyageCommonsTransitImportExportModel.forExport(importDataContext);
    }

    @Override
    protected void performImport(VoyageCommonsImportDataContext importDataContext, InputFile inputFile, ImportDataFileResult result) {

        if (log.isInfoEnabled()) {
            log.info("Starts import of transits from file " + inputFile.getFileName());
        }

        Voyage voyage = importDataContext.getVoyage();

        String relatedActivity = getConfiguration().getTransitRelatedActivity();

        try (Import<VoyageCommonsTransitImportRow> importer = open()) {

            incrementsProgress();

            int rowNumber = 0;

            for (VoyageCommonsTransitImportRow row : importer) {

                doFlushTransaction(++rowNumber);

                Transit transit = row.getTransit();
                transit.setVoyage(voyage);
                transit.setRelatedActivity(relatedActivity);

                boolean exists = persistenceService.containsTransit(voyage, transit.getStartTime(), transit.getEndTime());
                if (exists) {
                    throw new DuplicatedTransitException(getLocale(), rowNumber, voyage.getName(), transit.getStartTime(), transit.getEndTime());
                }

                Transit createdTransit = persistenceService.createTransit(transit);

                voyage.addTransit(createdTransit);

                addId(result, EchoBaseUserEntityEnum.Transit, createdTransit, rowNumber);
                addProcessedRow(result, row);

            }

        }

    }

    @Override
    protected void computeImportedExport(VoyageCommonsImportDataContext importDataContext, ImportDataFileResult result) {

        String voyageId = importDataContext.getConfiguration().getVoyageId();
        Voyage voyage = persistenceService.getVoyage(voyageId);

        for (Transit transit : getImportedEntities(Transit.class, result)) {
            String transitId = transit.getTopiaId();
            if (log.isInfoEnabled()) {
                log.info("Adding transit: " + transitId + " to imported export.");
            }

            VoyageCommonsTransitImportRow importedRow = VoyageCommonsTransitImportRow.of(voyage, transit);
            addImportedRow(result, importedRow);

        }

    }

}
