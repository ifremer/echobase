/*
 * #%L
 * EchoBase :: Services
 * %%
 * Copyright (C) 2011 - 2012 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ifremer.echobase.services.service.importdata.csv;

import fr.ifremer.echobase.entities.data.Voyage;
import fr.ifremer.echobase.entities.references.AgeCategory;
import fr.ifremer.echobase.entities.references.CellType;
import fr.ifremer.echobase.entities.references.DataMetadata;
import fr.ifremer.echobase.entities.references.DataQuality;
import fr.ifremer.echobase.entities.references.SizeCategory;
import fr.ifremer.echobase.entities.references.Species;
import fr.ifremer.echobase.services.csv.EchoBaseCsvUtil;
import fr.ifremer.echobase.services.service.importdata.contexts.VoyageResultsImportDataContext;

import java.util.List;

/**
 * Model to import cells of type 'Map'.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 0.3
 */
public class VoyageResultsMapFishCellImportExportModel extends EchoBaseImportExportModelSupport<VoyageResultsMapFishCellImportRow> {

    protected static final String HEADER_SPECIES = "baracoudaCode";

    public static final String[] COLUMN_NAMES_TO_EXCLUDE = {
            VoyageResultsMapFishCellImportRow.PROPERTY_NAME,
            HEADER_SPECIES,
            VoyageResultsMapFishCellImportRow.PROPERTY_DATA_GRID_CELL_LONGITUDE,
            VoyageResultsMapFishCellImportRow.PROPERTY_DATA_GRID_CELL_LATITUDE,
            VoyageResultsMapFishCellImportRow.PROPERTY_DATA_GRID_CELL_DEPTH,
            VoyageResultsMapFishCellImportRow.PROPERTY_DATA_GRID_LONGITUDE_LAG,
            VoyageResultsMapFishCellImportRow.PROPERTY_DATA_GRID_LATITUDE_LAG,
            VoyageResultsMapFishCellImportRow.PROPERTY_DATA_GRID_DEPTH_LAG,
            VoyageResultsMapFishCellImportRow.PROPERTY_VOYAGE,
            VoyageResultsMapFishCellImportRow.PROPERTY_SIZE_CATEGORY,
            VoyageResultsMapFishCellImportRow.PROPERTY_AGE_CATEGORY,
            VoyageResultsMapFishCellImportRow.PROPERTY_DATA_QUALITY
    };

    protected final CellType cellType;

    private VoyageResultsMapFishCellImportExportModel(char separator, CellType cellType) {
        super(separator);
        this.cellType = cellType;

    }

    public static VoyageResultsMapFishCellImportExportModel forImport(VoyageResultsImportDataContext importDataContext, List<DataMetadata> dataMetadatas) {

        VoyageResultsMapFishCellImportExportModel model = new VoyageResultsMapFishCellImportExportModel(importDataContext.getCsvSeparator(), importDataContext.getMapCellType());
        model.newForeignKeyColumn(VoyageResultsMapFishCellImportRow.PROPERTY_VOYAGE, Voyage.class, Voyage.PROPERTY_NAME, importDataContext.getVoyagesByName());
        model.newMandatoryColumn(VoyageResultsMapFishCellImportRow.PROPERTY_NAME);
        model.newForeignKeyColumn(HEADER_SPECIES, VoyageResultsMapFishCellImportRow.PROPERTY_SPECIES, Species.class, Species.PROPERTY_BARACOUDA_CODE, importDataContext.getSpeciesByBaracoudaCode());
        model.newForeignKeyColumn(VoyageResultsMapFishCellImportRow.PROPERTY_SIZE_CATEGORY, SizeCategory.class, SizeCategory.PROPERTY_NAME, importDataContext.getSizeCategoriesByName());
        model.newForeignKeyColumn(VoyageResultsMapFishCellImportRow.PROPERTY_AGE_CATEGORY, AgeCategory.class, AgeCategory.PROPERTY_NAME, importDataContext.getAgeCategoriesByName());
        model.newForeignKeyColumn(VoyageResultsMapFishCellImportRow.PROPERTY_DATA_QUALITY, DataQuality.class, DataQuality.PROPERTY_QUALITY_DATA_FLAG_VALUES, importDataContext.getDataQualitiesByName());
        model.newMandatoryColumn(VoyageResultsMapFishCellImportRow.PROPERTY_DATA_GRID_CELL_LONGITUDE, EchoBaseCsvUtil.PRIMITIVE_FLOAT);
        model.newMandatoryColumn(VoyageResultsMapFishCellImportRow.PROPERTY_DATA_GRID_CELL_LATITUDE, EchoBaseCsvUtil.PRIMITIVE_FLOAT);
        model.newMandatoryColumn(VoyageResultsMapFishCellImportRow.PROPERTY_DATA_GRID_CELL_DEPTH, EchoBaseCsvUtil.PRIMITIVE_FLOAT);
        model.newMandatoryColumn(VoyageResultsMapFishCellImportRow.PROPERTY_DATA_GRID_LONGITUDE_LAG, EchoBaseCsvUtil.PRIMITIVE_FLOAT);
        model.newMandatoryColumn(VoyageResultsMapFishCellImportRow.PROPERTY_DATA_GRID_LATITUDE_LAG, EchoBaseCsvUtil.PRIMITIVE_FLOAT);
        model.newMandatoryColumn(VoyageResultsMapFishCellImportRow.PROPERTY_DATA_GRID_DEPTH_LAG, EchoBaseCsvUtil.PRIMITIVE_FLOAT);

        addResultsColumnsForImport(model, dataMetadatas);
        return model;

    }

    public static VoyageResultsMapFishCellImportExportModel forExport(VoyageResultsImportDataContext importDataContext, List<DataMetadata> dataMetadatas) {

        VoyageResultsMapFishCellImportExportModel model = new VoyageResultsMapFishCellImportExportModel(importDataContext.getCsvSeparator(), importDataContext.getMapCellType());
        model.newColumnForExport(VoyageResultsMapFishCellImportRow.PROPERTY_VOYAGE, EchoBaseCsvUtil.VOYAGE_FORMATTER);
        model.newColumnForExport(VoyageResultsMapFishCellImportRow.PROPERTY_NAME);
        model.newColumnForExport(HEADER_SPECIES, VoyageResultsMapFishCellImportRow.PROPERTY_SPECIES, EchoBaseCsvUtil.SPECIES_FORMATTER);
        model.newColumnForExport(VoyageResultsMapFishCellImportRow.PROPERTY_SIZE_CATEGORY, EchoBaseCsvUtil.SIZE_CATEGORY_FORMATTER);
        model.newColumnForExport(VoyageResultsMapFishCellImportRow.PROPERTY_AGE_CATEGORY, EchoBaseCsvUtil.AGE_CATEGORY_FORMATTER);
        model.newColumnForExport(VoyageResultsMapFishCellImportRow.PROPERTY_DATA_QUALITY, EchoBaseCsvUtil.DATA_QUALITY_FORMATTER);
        model.newColumnForExport(VoyageResultsMapFishCellImportRow.PROPERTY_DATA_GRID_CELL_LONGITUDE, EchoBaseCsvUtil.PRIMITIVE_FLOAT);
        model.newColumnForExport(VoyageResultsMapFishCellImportRow.PROPERTY_DATA_GRID_CELL_LATITUDE, EchoBaseCsvUtil.PRIMITIVE_FLOAT);
        model.newColumnForExport(VoyageResultsMapFishCellImportRow.PROPERTY_DATA_GRID_CELL_DEPTH, EchoBaseCsvUtil.PRIMITIVE_FLOAT);
        model.newColumnForExport(VoyageResultsMapFishCellImportRow.PROPERTY_DATA_GRID_LONGITUDE_LAG, EchoBaseCsvUtil.PRIMITIVE_FLOAT);
        model.newColumnForExport(VoyageResultsMapFishCellImportRow.PROPERTY_DATA_GRID_LATITUDE_LAG, EchoBaseCsvUtil.PRIMITIVE_FLOAT);
        model.newColumnForExport(VoyageResultsMapFishCellImportRow.PROPERTY_DATA_GRID_DEPTH_LAG, EchoBaseCsvUtil.PRIMITIVE_FLOAT);

        addResultsColumns(model, dataMetadatas);
        return model;

    }

    @Override
    public VoyageResultsMapFishCellImportRow newEmptyInstance() {
        return new VoyageResultsMapFishCellImportRow(cellType);
    }
}
