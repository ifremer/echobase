package fr.ifremer.echobase.services.service.importdb.strategy;

/*
 * #%L
 * EchoBase :: Services
 * %%
 * Copyright (C) 2011 - 2012 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.echobase.entities.EchoBaseUserEntityEnum;
import fr.ifremer.echobase.entities.EchoBaseUserPersistenceContext;
import org.nuiton.csv.Import;
import org.nuiton.csv.ImportToMap;
import org.nuiton.topia.persistence.TopiaDao;
import org.nuiton.topia.persistence.TopiaEntity;
import org.nuiton.topia.persistence.TopiaEntityEnum;
import org.nuiton.topia.persistence.TopiaException;
import org.nuiton.topia.persistence.metadata.AssociationMeta;
import org.nuiton.topia.persistence.metadata.TableMeta;
import org.nuiton.topia.service.csv.CsvProgressModel;
import org.nuiton.topia.service.csv.in.CsvImportResult;
import org.nuiton.topia.service.csv.in.ImportModelFactory;
import org.nuiton.topia.service.csv.in.ImportStrategy;
import org.nuiton.topia.service.csv.in.TopiaCsvImports;

import java.util.Map;

/**
 * How to import csv files.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.1
 */
public class EchoBaseImportStrategy implements ImportStrategy<EchoBaseUserEntityEnum> {

    private final EchoBaseUserPersistenceContext persistenceContext;

    private final int nbRowBuffer;

    private final ImportModelFactory<EchoBaseUserEntityEnum> modelFactory;

    public EchoBaseImportStrategy(ImportModelFactory<EchoBaseUserEntityEnum> modelFactory,
                                  EchoBaseUserPersistenceContext persistenceContext,
                                  int nbRowBuffer) {
        this.persistenceContext = persistenceContext;
        this.nbRowBuffer = nbRowBuffer;
        this.modelFactory = modelFactory;
    }

    public ImportModelFactory<EchoBaseUserEntityEnum> getModelFactory() {
        return modelFactory;
    }

    @Override
    public <E extends TopiaEntity> void importTable(TableMeta<EchoBaseUserEntityEnum> meta,
                                                    Import<E> importer,
                                                    CsvImportResult<EchoBaseUserEntityEnum> csvResult) throws TopiaException {
        TopiaDao<E> dao = (TopiaDao<E>) persistenceContext.getDao(meta.getSource().getContract());

        if (meta.getSource() == EchoBaseUserEntityEnum.Category) {

            // special case, test if exists before to add it, otherwise skip it
            importAllEntitiesIfNotExisting(dao, meta, importer, csvResult, nbRowBuffer);

        } else {
            TopiaCsvImports.importAllEntities(persistenceContext.getHibernateSupport(), dao, meta, importer, csvResult, nbRowBuffer);
        }
    }

    @Override
    public <E extends TopiaEntity> Iterable<E> importTableAndReturnThem(TableMeta<EchoBaseUserEntityEnum> meta,
                                                                        Import<E> importer,
                                                                        CsvImportResult<EchoBaseUserEntityEnum> csvResult) throws TopiaException {
        TopiaDao<E> dao = (TopiaDao<E>) persistenceContext.getDao(meta.getSource().getContract());

        return TopiaCsvImports.importAllEntitiesAndReturnThem(
                dao, meta, importer, csvResult);

    }

    @Override
    public void importAssociation(AssociationMeta<EchoBaseUserEntityEnum> meta,
                                  ImportToMap importer,
                                  CsvImportResult<EchoBaseUserEntityEnum> csvResult) throws TopiaException {

        if (getModelFactory().isNMAssociationMeta(meta)) {
            TopiaCsvImports.importNMAssociation(persistenceContext.getSqlSupport(),
                                                meta,
                                                importer,
                                                csvResult,
                                                nbRowBuffer);
        } else {
            TopiaCsvImports.importAssociation(persistenceContext.getSqlSupport(),
                                              meta,
                                              importer,
                                              csvResult,
                                              nbRowBuffer);
        }
    }

    public <T extends TopiaEntityEnum, E extends TopiaEntity> void importAllEntitiesIfNotExisting(TopiaDao<E> dao,
                                                                                                  TableMeta<T> meta,
                                                                                                  Import<E> importer,
                                                                                                  CsvImportResult<T> csvResult,
                                                                                                  int nbRowBuffer) throws TopiaException {

//        TopiaContext context = dao.getTopiaContext();

        CsvProgressModel progressModel = csvResult == null ? null :
                                         csvResult.getProgressModel();

        int compt = 0;
        for (E entity : importer) {

            if (!dao.forTopiaIdEquals(entity.getTopiaId()).exists()) {

                Map<String, Object> properties = meta.prepareCreate(
                        entity, entity.getTopiaId());
                E entityToSave = dao.create(properties);

                meta.copy(entity, entityToSave);
                compt++;
                if (compt % nbRowBuffer == 0) {
                    // flush it
                    persistenceContext.getHibernateSupport().getHibernateSession().flush();
                }
            }

            if (csvResult != null) {
                csvResult.incrementsNumberUpdated();
                if (progressModel != null) {
                    progressModel.incrementsProgress();
                }
            }

        }
    }
}
