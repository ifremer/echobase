package fr.ifremer.echobase.services.service.importdata.contexts;

/*
 * #%L
 * EchoBase :: Services
 * %%
 * Copyright (C) 2011 - 2016 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.Collections2;
import com.google.common.collect.Maps;
import fr.ifremer.echobase.entities.EchoBaseUser;
import fr.ifremer.echobase.entities.data.Operation;
import fr.ifremer.echobase.entities.data.Operations;
import fr.ifremer.echobase.entities.data.Voyage;
import fr.ifremer.echobase.entities.data.Voyages;
import fr.ifremer.echobase.services.service.UserDbPersistenceService;
import fr.ifremer.echobase.services.service.importdata.ResultCategoryCache;
import fr.ifremer.echobase.services.service.importdata.SpeciesCategoryCache;
import fr.ifremer.echobase.services.service.importdata.configurations.VoyageImportDataConfigurationSupport;

import java.util.Date;
import java.util.Collection;
import java.util.Collections;
import java.util.Locale;
import java.util.Map;

/**
 * Created on 30/03/16.
 *
 * @author Tony Chemit - chemit@codelutin.com
 */
class VoyageImportDataContextSupport<C extends VoyageImportDataConfigurationSupport> extends ImportDataContextSupport<C> {

    private SpeciesCategoryCache speciesCategoryCache;
    private ResultCategoryCache resultCategoryCache;

    private Voyage voyage;
    private Collection<Operation> voyageOperations;
    private Collection<Operation> voyageOperationsWithTotalOrUnsortedSample;

    private Map<String, Voyage> voyagesByName;
    private Map<String, Operation> voyageOperationsById;

    public VoyageImportDataContextSupport(UserDbPersistenceService persistenceService, Locale locale, char csvSeparator, C configuration, EchoBaseUser user, Date importDate) {
        super(persistenceService, locale, csvSeparator, configuration, user, importDate);
    }

    @Override
    public String getEntityId() {
        return configuration.getVoyageId();
    }

    public final Collection<Operation> getVoyageOperationsWithTotalOrUnsortedSample() {
        if (voyageOperationsWithTotalOrUnsortedSample == null) {
            voyageOperationsWithTotalOrUnsortedSample = Collections2.filter(getVoyageOperations(), Operations.OPERATION_WITH_TOTAL_OR_UNSORTED_SAMPLE);
        }
        return voyageOperationsWithTotalOrUnsortedSample;
    }

    public final Collection<Operation> getVoyageOperations() {
        if (voyageOperations == null) {
            voyageOperations = getVoyage().getAllOperations();
        }
        return voyageOperations;
    }

    public final Map<String, Operation> getVoyageOperationsById() {
        if (voyageOperationsById == null) {
            voyageOperationsById = Maps.uniqueIndex(getVoyageOperations(), Operations.OPERATION_ID);
        }
        return voyageOperationsById;
    }

    public final Map<String, Voyage> getVoyagesByName() {
        if (voyagesByName == null) {
            voyagesByName = Maps.uniqueIndex(Collections.singletonList(getVoyage()), Voyages.VOYAGE_NAME);
        }
        return voyagesByName;
    }

    public final Voyage getVoyage() {
        if (voyage == null) {
            voyage = persistenceService.getVoyage(configuration.getVoyageId());
        }
        return voyage;
    }

    public final SpeciesCategoryCache getSpeciesCategoryCache() {
        if (speciesCategoryCache == null) {
            speciesCategoryCache = new SpeciesCategoryCache(persistenceService);
        }
        return speciesCategoryCache;
    }

    public final ResultCategoryCache getResultCategoryCache() {
        if (resultCategoryCache == null) {
            resultCategoryCache = new ResultCategoryCache(persistenceService, getSpeciesCategoryCache());
        }
        return resultCategoryCache;
    }

}
