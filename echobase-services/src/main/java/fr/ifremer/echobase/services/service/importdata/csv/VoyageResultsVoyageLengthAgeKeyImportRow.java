/*
 * #%L
 * EchoBase :: Services
 * %%
 * Copyright (C) 2011 - 2012 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ifremer.echobase.services.service.importdata.csv;

import fr.ifremer.echobase.entities.data.LengthAgeKey;
import fr.ifremer.echobase.entities.data.LengthAgeKeyImpl;
import fr.ifremer.echobase.entities.data.Voyage;
import fr.ifremer.echobase.entities.references.Species;
import fr.ifremer.echobase.entities.references.Strata;

import java.io.Serializable;

/**
 * Bean used as a row for import of {@link VoyageResultsVoyageLengthAgeKeyImportExportModel}.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 0.3
 */
public class VoyageResultsVoyageLengthAgeKeyImportRow implements Serializable {

    public static final String PROPERTY_VOYAGE = "voyage";
    private static final long serialVersionUID = 1L;

    protected final LengthAgeKey lengthAgeKey;
    protected Voyage voyage;

    public static VoyageResultsVoyageLengthAgeKeyImportRow of(Voyage voyage, LengthAgeKey lengthAgeKey) {
        VoyageResultsVoyageLengthAgeKeyImportRow row = new VoyageResultsVoyageLengthAgeKeyImportRow(lengthAgeKey);
        row.setVoyage(voyage);
        return row;
    }

    public VoyageResultsVoyageLengthAgeKeyImportRow() {
        this(new LengthAgeKeyImpl());
    }

    public VoyageResultsVoyageLengthAgeKeyImportRow(LengthAgeKey lengthAgeKey) {
        this.lengthAgeKey = lengthAgeKey;
    }

    public LengthAgeKey getLengthAgeKey() {
        return lengthAgeKey;
    }

    public Voyage getVoyage() {
        return voyage;
    }

    public void setVoyage(Voyage voyage) {
        this.voyage = voyage;
    }

    public int getAge() {
        return lengthAgeKey.getAge();
    }

    public void setAge(int age) {
        lengthAgeKey.setAge(age);
    }

    public float getLength() {
        return lengthAgeKey.getLength();
    }

    public void setLength(float length) {
        lengthAgeKey.setLength(length);
    }

    public float getPercentAtAge() {
        return lengthAgeKey.getPercentAtAge();
    }

    public void setPercentAtAge(float percentAtAge) {
        lengthAgeKey.setPercentAtAge(percentAtAge);
    }

    public String getMetadata() {
        return lengthAgeKey.getMetadata();
    }

    public void setMetadata(String metadata) {
        lengthAgeKey.setMetadata(metadata);
    }

    public Strata getStrata() {
        return lengthAgeKey.getStrata();
    }

    public void setStrata(Strata strata) {
        lengthAgeKey.setStrata(strata);
    }

    public Species getSpecies() {
        return lengthAgeKey.getSpecies();
    }

    public void setSpecies(Species species) {
        lengthAgeKey.setSpecies(species);
    }
}
