package fr.ifremer.echobase.services.service.importdata;

/*
 * #%L
 * EchoBase :: Services
 * %%
 * Copyright (C) 2011 - 2015 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import org.nuiton.csv.ImportRuntimeException;

import java.util.Locale;

import static org.nuiton.i18n.I18n.l;

/**
 * Created on 5/09/16.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 4.0
 */
public class DuplicatedGearMetadataValueException extends ImportRuntimeException {

    private static final long serialVersionUID = 1L;

    public DuplicatedGearMetadataValueException(Locale locale,
                                                int rowNumber,
                                                String voyageName,
                                                String vesselName,
                                                String depthStratumName,
                                                String operationId,
                                                String gearName,
                                                String gearMetatadataName) {
        super(l(locale, "echobase.importError.duplicate.gearMetataValue", rowNumber, gearMetatadataName, gearName, operationId, depthStratumName, vesselName, voyageName));
    }

}
