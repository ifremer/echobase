/*
 * #%L
 * EchoBase :: Services
 * %%
 * Copyright (C) 2011 - 2012 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ifremer.echobase.services.service.importdata.csv;

import fr.ifremer.echobase.entities.data.Cell;
import fr.ifremer.echobase.entities.data.Echotype;
import fr.ifremer.echobase.entities.data.Voyage;
import fr.ifremer.echobase.entities.references.DataMetadata;
import fr.ifremer.echobase.entities.references.DataQuality;
import fr.ifremer.echobase.entities.references.SizeCategory;
import fr.ifremer.echobase.entities.references.Species;
import fr.ifremer.echobase.services.csv.EchoBaseCsvUtil;
import fr.ifremer.echobase.services.service.importdata.contexts.VoyageResultsImportDataContext;

import java.util.List;

/**
 * Model to import results of cell of type 'Region'.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 0.3
 */
public class VoyageResultsRegionCellResultImportExportModel extends EchoBaseImportExportModelSupport<VoyageResultsRegionCellResultImportRow> {

    protected static final String HEADER_SPECIES = "baracoudaCode";

    public static final String[] COLUMN_NAMES_TO_EXCLUDE = {
            EchoBaseCsvUtil.CELL_NAME,
            HEADER_SPECIES,
            VoyageResultsRegionCellResultImportRow.PROPERTY_VOYAGE,
            VoyageResultsRegionCellResultImportRow.PROPERTY_SIZE_CATEGORY,
            VoyageResultsRegionCellResultImportRow.PROPERTY_ECHOTYPE,
            VoyageResultsRegionCellResultImportRow.PROPERTY_DATA_QUALITY
    };

    private VoyageResultsRegionCellResultImportExportModel(char separator) {
        super(separator);
    }

    public static VoyageResultsRegionCellResultImportExportModel forImport(VoyageResultsImportDataContext importDataContext, List<DataMetadata> dataMetadatas) {

        VoyageResultsRegionCellResultImportExportModel model = new VoyageResultsRegionCellResultImportExportModel(importDataContext.getCsvSeparator());
        model.newForeignKeyColumn(VoyageResultsRegionCellResultImportRow.PROPERTY_VOYAGE, Voyage.class, Voyage.PROPERTY_NAME, importDataContext.getVoyagesByName());
        model.newForeignKeyColumn(EchoBaseCsvUtil.CELL_NAME, VoyageResultsRegionCellResultImportRow.PROPERTY_CELL, Cell.class, Cell.PROPERTY_NAME, importDataContext.getVoyageRegionsByName());
        model.newForeignKeyColumn(VoyageResultsRegionCellResultImportRow.PROPERTY_ECHOTYPE, Echotype.class, Echotype.PROPERTY_NAME, importDataContext.getVoyageEchotypesByName());
        model.newForeignKeyColumn(HEADER_SPECIES, VoyageResultsRegionCellResultImportRow.PROPERTY_SPECIES, Species.class, Species.PROPERTY_BARACOUDA_CODE, importDataContext.getSpeciesByBaracoudaCode());
        model.newForeignKeyColumn(VoyageResultsRegionCellResultImportRow.PROPERTY_SIZE_CATEGORY, SizeCategory.class, SizeCategory.PROPERTY_NAME, importDataContext.getSizeCategoriesByName());
        model.newForeignKeyColumn(VoyageResultsRegionCellResultImportRow.PROPERTY_DATA_QUALITY, DataQuality.class, DataQuality.PROPERTY_QUALITY_DATA_FLAG_VALUES, importDataContext.getDataQualitiesByName());

        addResultsColumnsForImport(model, dataMetadatas);
        return model;

    }

    public static VoyageResultsRegionCellResultImportExportModel forExport(VoyageResultsImportDataContext importDataContext, List<DataMetadata> dataMetadatas) {

        VoyageResultsRegionCellResultImportExportModel model = new VoyageResultsRegionCellResultImportExportModel(importDataContext.getCsvSeparator());
        model.newColumnForExport(VoyageResultsRegionCellResultImportRow.PROPERTY_VOYAGE, EchoBaseCsvUtil.VOYAGE_FORMATTER);
        model.newColumnForExport(EchoBaseCsvUtil.CELL_NAME, VoyageResultsRegionCellResultImportRow.PROPERTY_CELL, EchoBaseCsvUtil.CELL_FORMATTER);
        model.newColumnForExport(VoyageResultsRegionCellResultImportRow.PROPERTY_ECHOTYPE, EchoBaseCsvUtil.ECHOTYPE_FORMATTER);
        model.newColumnForExport(HEADER_SPECIES, VoyageResultsRegionCellResultImportRow.PROPERTY_SPECIES, EchoBaseCsvUtil.SPECIES_FORMATTER);
        model.newColumnForExport(VoyageResultsRegionCellResultImportRow.PROPERTY_SIZE_CATEGORY, EchoBaseCsvUtil.SIZE_CATEGORY_FORMATTER);
        model.newColumnForExport(VoyageResultsRegionCellResultImportRow.PROPERTY_DATA_QUALITY, EchoBaseCsvUtil.DATA_QUALITY_FORMATTER);

        addResultsColumns(model, dataMetadatas);

        return model;

    }

    @Override
    public VoyageResultsRegionCellResultImportRow newEmptyInstance() {
        return new VoyageResultsRegionCellResultImportRow();
    }
}
