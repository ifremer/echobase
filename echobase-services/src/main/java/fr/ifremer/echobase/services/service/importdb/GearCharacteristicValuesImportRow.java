package fr.ifremer.echobase.services.service.importdb;

/*
 * #%L
 * EchoBase :: Services
 * %%
 * Copyright (C) 2011 - 2016 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.echobase.entities.references.Gear;
import fr.ifremer.echobase.entities.references.GearCharacteristic;
import fr.ifremer.echobase.entities.references.GearCharacteristicValue;
import fr.ifremer.echobase.entities.references.GearCharacteristicValueImpl;

/**
 * Created on 14/05/16.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 4.0
 */
public class GearCharacteristicValuesImportRow {

    public static final String PROPERTY_GEAR = "gear";
    public static final String PROPERTY_CHARACTERISTIC = "characteristic";
    public static final String PROPERTY_DATA_VALUE = "dataValue";

    private Gear gear;
    private final GearCharacteristicValue gearCharacteristicValue;

    public GearCharacteristicValuesImportRow() {
        gearCharacteristicValue = new GearCharacteristicValueImpl();
    }

    public void setGear(Gear gear) {
        this.gear = gear;
    }

    public void setDataValue(String dataValue) {
        gearCharacteristicValue.setDataValue(dataValue);
    }

    public void setCharacteristic(GearCharacteristic characteristic) {
        gearCharacteristicValue.setGearCharacteristic(characteristic);
    }

    public Gear getGear() {
        return gear;
    }

    public GearCharacteristicValue getGearCharacteristicValue() {
        return gearCharacteristicValue;
    }

}
