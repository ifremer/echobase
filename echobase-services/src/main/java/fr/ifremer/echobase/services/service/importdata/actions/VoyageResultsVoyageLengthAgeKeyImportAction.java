package fr.ifremer.echobase.services.service.importdata.actions;

/*
 * #%L
 * EchoBase :: Services
 * %%
 * Copyright (C) 2011 - 2016 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.echobase.entities.EchoBaseUserEntityEnum;
import fr.ifremer.echobase.entities.data.LengthAgeKey;
import fr.ifremer.echobase.entities.data.Voyage;
import fr.ifremer.echobase.io.InputFile;
import fr.ifremer.echobase.services.service.importdata.DuplicatedResultsVoyageLengthAgeKeyException;
import fr.ifremer.echobase.services.service.importdata.ImportDataFileResult;
import fr.ifremer.echobase.services.service.importdata.MismatchProviderException;
import fr.ifremer.echobase.services.service.importdata.contexts.VoyageResultsImportDataContext;
import fr.ifremer.echobase.services.service.importdata.csv.VoyageResultsVoyageLengthAgeKeyImportExportModel;
import fr.ifremer.echobase.services.service.importdata.csv.VoyageResultsVoyageLengthAgeKeyImportRow;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.csv.Import;

import java.util.Set;
import java.util.TreeSet;

/**
 * Created on 25/03/16.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 4.0
 */
public class VoyageResultsVoyageLengthAgeKeyImportAction extends VoyageResultsImportDataActionSupport<VoyageResultsVoyageLengthAgeKeyImportRow> {

    /** Logger. */
    private static final Log log = LogFactory.getLog(VoyageResultsVoyageLengthAgeKeyImportAction.class);

    public VoyageResultsVoyageLengthAgeKeyImportAction(VoyageResultsImportDataContext importDataContext) {
        super(importDataContext, importDataContext.getConfiguration().getLengthAgeKeyFile());
    }

    @Override
    protected VoyageResultsVoyageLengthAgeKeyImportExportModel createCsvImportModel(VoyageResultsImportDataContext importDataContext) {
        return VoyageResultsVoyageLengthAgeKeyImportExportModel.forImport(importDataContext);
    }

    @Override
    protected VoyageResultsVoyageLengthAgeKeyImportExportModel createCsvExportModel(VoyageResultsImportDataContext importDataContext) {
        return VoyageResultsVoyageLengthAgeKeyImportExportModel.forExport(importDataContext);
    }

    @Override
    protected void performImport(VoyageResultsImportDataContext importDataContext, InputFile inputFile, ImportDataFileResult result) {

        if (log.isInfoEnabled()) {
            log.info("Starts import of lenthAgeKey from file " + inputFile.getFileName());
        }

        Voyage expectedVoyage = importDataContext.getVoyage();

        Set<String> cacheKeys = new TreeSet<>();

        for (LengthAgeKey lengthAgeKey : expectedVoyage.getLengthAgeKey()) {
            cacheKeys.add(cacheKey(lengthAgeKey));
        }

        try (Import<VoyageResultsVoyageLengthAgeKeyImportRow> importer = open()) {

            incrementsProgress();

            int rowNumber = 0;
            for (VoyageResultsVoyageLengthAgeKeyImportRow row : importer) {

                doFlushTransaction(++rowNumber);

                Voyage voyage = row.getVoyage();

                if (!expectedVoyage.equals(voyage)) {
                    throw new MismatchProviderException(getLocale(), rowNumber, voyage.getName());
                }

                LengthAgeKey lengthAgeKeyToCreate = row.getLengthAgeKey();
                String cacheKey = cacheKey(lengthAgeKeyToCreate);
                if (cacheKeys.contains(cacheKey)) {
                    throw new DuplicatedResultsVoyageLengthAgeKeyException(getLocale(),
                                                                           rowNumber,
                                                                           voyage.getName(),
                                                                           lengthAgeKeyToCreate.getSpecies().getBaracoudaCode(),
                                                                           lengthAgeKeyToCreate.getLength(),
                                                                           lengthAgeKeyToCreate.getAge());
                }

                LengthAgeKey lengthAgeKey = persistenceService.createLengthAgeKey(lengthAgeKeyToCreate);

                // attach it to voyage
                voyage.addLengthAgeKey(lengthAgeKey);

                // collect ids
                addId(result, EchoBaseUserEntityEnum.LengthAgeKey, lengthAgeKey, rowNumber);

                addProcessedRow(result, row);

            }

        }

    }

    @Override
    protected void computeImportedExport(VoyageResultsImportDataContext importDataContext, ImportDataFileResult result) {

        String voyageId = importDataContext.getConfiguration().getVoyageId();
        Voyage voyage = persistenceService.getVoyage(voyageId);

        for (LengthAgeKey lengthAgeKey : getImportedEntities(LengthAgeKey.class, result)) {

            VoyageResultsVoyageLengthAgeKeyImportRow row = VoyageResultsVoyageLengthAgeKeyImportRow.of(voyage, lengthAgeKey);
            addImportedRow(result, row);

        }

    }

    private String cacheKey(LengthAgeKey lengthAgeKey) {
        return lengthAgeKey.getSpecies().getTopiaId() + "~" + lengthAgeKey.getLength() + "~" + lengthAgeKey.getAge();
    }

}
