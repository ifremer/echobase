/*
 * #%L
 * EchoBase :: Services
 * %%
 * Copyright (C) 2011 - 2012 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ifremer.echobase.services.service.importdata.csv;

import fr.ifremer.echobase.entities.data.Cell;
import fr.ifremer.echobase.entities.data.Data;
import fr.ifremer.echobase.entities.data.DataAcquisition;
import fr.ifremer.echobase.entities.data.DataProcessing;
import fr.ifremer.echobase.entities.references.AcousticInstrument;
import fr.ifremer.echobase.entities.references.DataQuality;
import fr.ifremer.echobase.services.csv.EchoBaseCsvUtil;
import fr.ifremer.echobase.services.service.importdata.contexts.ImportDataContextSupport;

/**
 * To import acoustic datas (says {@link DataAcquisition},
 * {@link DataProcessing}, {@link Cell} and {@link Data}.
 *
 * @author Julien Ruchaud - ruchaud@codelutin.com
 * @since 4.0
 */
public class AcousticImportExportModel extends EchoBaseImportExportModelSupport<AcousticImportRow> {

    private AcousticImportExportModel(char separator) {
        super(separator);
    }

    public static AcousticImportExportModel forImport(ImportDataContextSupport importDataContext) {

        AcousticImportExportModel model = new AcousticImportExportModel(importDataContext.getCsvSeparator());

        model.newMandatoryColumn("MOVIES_EILayer", AcousticImportRow.PROPERTY_EI_LAYER);//A
        model.newIgnoredColumn("MOVIES_EILayer\\sndset");//B
        model.newIgnoredColumn("MOVIES_EILayer\\sndset\\sndname");//C
        model.newIgnoredColumn("MOVIES_EILayer\\sndset\\sndident");//D
        model.newForeignKeyColumn("MOVIES_EILayer\\sndset\\softChannelId", AcousticImportRow.PROPERTY_ACOUSTIC_INSTRUMENT, AcousticInstrument.class, AcousticInstrument.PROPERTY_ID, importDataContext.getInstrumentsById()); //E
        model.newIgnoredColumn("MOVIES_EILayer\\sndset\\channelName");//F
        model.newIgnoredColumn("MOVIES_EILayer\\sndset\\dataType");//G
        model.newIgnoredColumn("MOVIES_EILayer\\sndset\\beamType");//H
        model.newMandatoryColumn("MOVIES_EILayer\\sndset\\acousticFrequency", AcousticImportRow.PROPERTY_FREQUENCY, EchoBaseCsvUtil.PRIMITIVE_FLOAT);//I
        model.newIgnoredColumn("MOVIES_EILayer\\sndset\\startSample");//J
        model.newIgnoredColumn("MOVIES_EILayer\\sndset\\mainBeamAlongSteeringAngle");//K
        model.newIgnoredColumn("MOVIES_EILayer\\sndset\\mainBeamAthwartSteeringAngle");//L
        model.newMandatoryColumn("MOVIES_EILayer\\sndset\\absorptionCoef", AcousticImportRow.PROPERTY_TRANSCEIVER_ACQUISITION_ABSORPTION, EchoBaseCsvUtil.PRIMITIVE_FLOAT);//M
        model.newMandatoryColumn("MOVIES_EILayer\\sndset\\transmissionPower", AcousticImportRow.PROPERTY_TRANSCEIVER_ACQUISITION_POWER, EchoBaseCsvUtil.PRIMITIVE_FLOAT);//N
        model.newMandatoryColumn("MOVIES_EILayer\\sndset\\beamAlongAngleSensitivity", AcousticImportRow.PROPERTY_TRANSDUCER_ACQUISITION_BEAM_ANGLE_ALONGSHIP, EchoBaseCsvUtil.PRIMITIVE_FLOAT);//O
        model.newMandatoryColumn("MOVIES_EILayer\\sndset\\beamAthwartAngleSensitivity", AcousticImportRow.PROPERTY_TRANSDUCER_ACQUISITION_BEAM_ANGLE_ATHWARTSHIP, EchoBaseCsvUtil.PRIMITIVE_FLOAT);//P
        model.newIgnoredColumn("MOVIES_EILayer\\sndset\\beam3dBWidthAlong");//Q
        model.newIgnoredColumn("MOVIES_EILayer\\sndset\\beam3dBWidthAthwart");//R
        model.newMandatoryColumn("MOVIES_EILayer\\sndset\\beamEquTwoWayAngle", AcousticImportRow.PROPERTY_TRANSDUCER_ACQUISITION_PSI, EchoBaseCsvUtil.PRIMITIVE_FLOAT);//S
        model.newMandatoryColumn("MOVIES_EILayer\\sndset\\beamGain", AcousticImportRow.PROPERTY_TRANSCEIVER_ACQUISITION_GAIN, EchoBaseCsvUtil.PRIMITIVE_FLOAT);//T
        model.newMandatoryColumn("MOVIES_EILayer\\sndset\\beamSACorrection", AcousticImportRow.PROPERTY_TRANSCEIVER_ACQUISITION_SACORRECTION, EchoBaseCsvUtil.PRIMITIVE_FLOAT);//U
        model.newIgnoredColumn("MOVIES_EILayer\\sndset\\bottomDetectionMinDepth");//V
        model.newIgnoredColumn("MOVIES_EILayer\\sndset\\bottomDetectionMaxDepth");//W
        model.newIgnoredColumn("MOVIES_EILayer\\sndset\\bottomDetectionMinLevel");//X
        model.newIgnoredColumn("MOVIES_EILayer\\sndset\\AlongTXRXWeightId");//Y
        model.newIgnoredColumn("MOVIES_EILayer\\sndset\\AthwartTXRXWeightId");//Z
        model.newIgnoredColumn("MOVIES_EILayer\\sndset\\SplitBeamAlongTXRXWeightId");//AA
        model.newIgnoredColumn("MOVIES_EILayer\\sndset\\SplitBeamAthwartTXRXWeightId");//AB
        model.newMandatoryColumn("MOVIES_EILayer\\sndset\\bandWidth", AcousticImportRow.PROPERTY_BANDWIDTH, EchoBaseCsvUtil.PRIMITIVE_FLOAT);//AC
        model.newIgnoredColumn("MOVIES_EILayer\\sndset\\tvgminrange");//AD
        model.newIgnoredColumn("MOVIES_EILayer\\sndset\\tvgmaxrange");//AE
        model.newMandatoryColumn("MOVIES_EILayer\\sndset\\pulseduration", AcousticImportRow.PROPERTY_TRANSCEIVER_ACQUISITION_PULSE_LENGTH, EchoBaseCsvUtil.PRIMITIVE_FLOAT);//AF
        model.newIgnoredColumn("MOVIES_EILayer\\shipnav");//AG
        model.newIgnoredColumn("MOVIES_EILayer\\shipnav\\lat");//AH
        model.newIgnoredColumn("MOVIES_EILayer\\shipnav\\long");//AI
        model.newIgnoredColumn("MOVIES_EILayer\\shipnav\\alt");//AJ
        model.newIgnoredColumn("MOVIES_EILayer\\shipnav\\gndspeed");//AK
        model.newIgnoredColumn("MOVIES_EILayer\\shipnav\\gndcourse");//AL
        model.newIgnoredColumn("MOVIES_EILayer\\shipnav\\surfspeed");//AM
        model.newIgnoredColumn("MOVIES_EILayer\\shipnav\\surfcourse");//AN
        model.newIgnoredColumn("MOVIES_EILayer\\shipnav\\driftspeed");//AO
        model.newIgnoredColumn("MOVIES_EILayer\\shipnav\\driftcourse");//AP
        model.newIgnoredColumn("MOVIES_EILayer\\shipnav\\heading");//AQ
        model.newIgnoredColumn("MOVIES_EILayer\\shipnav\\roll");//AR
        model.newIgnoredColumn("MOVIES_EILayer\\shipnav\\pitch");//AS
        model.newIgnoredColumn("MOVIES_EILayer\\shipnav\\heave");//AT
        model.newMandatoryColumn("MOVIES_EILayer\\shipnav\\depth", AcousticImportRow.PROPERTY_ESDU_CELL_DATA_DEPTH);//AU
        model.newIgnoredColumn("MOVIES_EILayer\\shipnav\\draught");//AV

        // Cell elementary Datas
        model.newIgnoredColumn("MOVIES_EILayer\\cellset");//AW
        model.newMandatoryColumn("MOVIES_EILayer\\cellset\\cellnum", AcousticImportRow.PROPERTY_CELL_NUM, EchoBaseCsvUtil.PRIMITIVE_INTEGER);//AX
        model.newMandatoryColumn("MOVIES_EILayer\\cellset\\celltype", AcousticImportRow.PROPERTY_CELL_TYPE, EchoBaseCsvUtil.PRIMITIVE_INTEGER);//AY
        model.newMandatoryColumn("MOVIES_EILayer\\cellset\\depthstart", AcousticImportRow.PROPERTY_CELL_DEPTH_START, EchoBaseCsvUtil.NA_TO_FLOAT_PARSER_FORMATTER);//AZ
        model.newMandatoryColumn("MOVIES_EILayer\\cellset\\depthend", AcousticImportRow.PROPERTY_CELL_DEPTH_END, EchoBaseCsvUtil.NA_TO_FLOAT_PARSER_FORMATTER);//BA
        model.newIgnoredColumn("MOVIES_EILayer\\cellset\\indexstart");//BB
        model.newIgnoredColumn("MOVIES_EILayer\\cellset\\indexend");//BC
        model.newMandatoryColumn("MOVIES_EILayer\\cellset\\datestart", AcousticImportRow.PROPERTY_CELL_DATE_START, EchoBaseCsvUtil.IMPORT_DAY_TIME_ECHOBASE);//BD
        model.newMandatoryColumn("MOVIES_EILayer\\cellset\\dateend", AcousticImportRow.PROPERTY_CELL_DATE_END, EchoBaseCsvUtil.IMPORT_DAY_TIME_ECHOBASE);//BE

        model.newMandatoryColumn("MOVIES_EILayer\\cellset\\lat", AcousticImportRow.PROPERTY_CELL_LATITUDE, EchoBaseCsvUtil.PRIMITIVE_FLOAT);//BF
        model.newMandatoryColumn("MOVIES_EILayer\\cellset\\long", AcousticImportRow.PROPERTY_CELL_LONGITUDE, EchoBaseCsvUtil.PRIMITIVE_FLOAT);//BG
        model.newMandatoryColumn("MOVIES_EILayer\\cellset\\volume", AcousticImportRow.PROPERTY_CELL_VOLUME, EchoBaseCsvUtil.NA_TO_FLOAT_PARSER_FORMATTER);//BH
        model.newMandatoryColumn("MOVIES_EILayer\\cellset\\area", AcousticImportRow.PROPERTY_CELL_SURFACE, EchoBaseCsvUtil.PRIMITIVE_INTEGER);//BI
        model.newIgnoredColumn("MOVIES_EILayer\\cellset\\diststart");//BJ
        model.newIgnoredColumn("MOVIES_EILayer\\cellset\\distend");//BK
        model.newMandatoryColumn("MOVIES_EILayer\\cellset\\thresholdup", AcousticImportRow.PROPERTY_E_ITHRESHOLD_HIGH, EchoBaseCsvUtil.PRIMITIVE_INTEGER);//BL
        model.newMandatoryColumn("MOVIES_EILayer\\cellset\\thresholdlow", AcousticImportRow.PROPERTY_E_ITHRESHOLD_LOW, EchoBaseCsvUtil.PRIMITIVE_INTEGER);//BM
        model.newIgnoredColumn("MOVIES_EILayer\\eilayer");//BN
        model.newMandatoryColumn("MOVIES_EILayer\\eilayer\\sa", AcousticImportRow.PROPERTY_CELL_NASC, EchoBaseCsvUtil.NA_TO_FLOAT_PARSER_FORMATTER);//BO
        model.newIgnoredColumn("MOVIES_EILayer\\eilayer\\sv");//BP
        model.newMandatoryColumn("MOVIES_EILayer\\eilayer\\ni", AcousticImportRow.PROPERTY_CELL_NUMBER_OF_SAMPLES_ECHO_INTEGRATED, EchoBaseCsvUtil.NA_TO_INTEGER_PARSER_FORMATTER);//BQ
        model.newMandatoryColumn("MOVIES_EILayer\\eilayer\\nt", AcousticImportRow.PROPERTY_CELL_NUMBER_OF_SAMPLES_RECORDED, EchoBaseCsvUtil.NA_TO_INTEGER_PARSER_FORMATTER);//BR
        model.newIgnoredColumn("MOVIES_EILayer\\boterr");//BR
        model.newIgnoredColumn("MOVIES_EILayer\\boterr\\sa");//BT
        model.newIgnoredColumn("MOVIES_EILayer\\boterr\\ni");//BU
        model.newMandatoryColumn("MOVIES_EILayer\\sndset\\soundcelerity", AcousticImportRow.PROPERTY_SOUND_CELERITY);//BV

        model.newForeignKeyColumn(AcousticImportRow.PROPERTY_DATA_QUALITY, DataQuality.class, DataQuality.PROPERTY_QUALITY_DATA_FLAG_VALUES, importDataContext.getDataQualitiesByName()); //BW
        model.newMandatoryColumn(AcousticImportRow.PROPERTY_LABEL, EchoBaseCsvUtil.NA_TO_STRING_PARSER_FORMATTER); //BX

        return model;
    }

    public static AcousticImportExportModel forExport(ImportDataContextSupport importDataContext) {

        AcousticImportExportModel model = new AcousticImportExportModel(importDataContext.getCsvSeparator());

        model.newColumnForExport("MOVIES_EILayer", AcousticImportRow.PROPERTY_EI_LAYER);//A
//        model.newIgnoredColumn("MOVIES_EILayer\\sndset");//B
//        model.newIgnoredColumn("MOVIES_EILayer\\sndset\\sndname");//C
//        model.newIgnoredColumn("MOVIES_EILayer\\sndset\\sndident");//D
        model.newColumnForExport("MOVIES_EILayer\\sndset\\softChannelId", AcousticImportRow.PROPERTY_ACOUSTIC_INSTRUMENT, EchoBaseCsvUtil.ACOUSTIC_INSTRUMENT_FORMATTER); //E
//        model.newIgnoredColumn("MOVIES_EILayer\\sndset\\channelName");//F
//        model.newIgnoredColumn("MOVIES_EILayer\\sndset\\dataType");//G
//        model.newIgnoredColumn("MOVIES_EILayer\\sndset\\beamType");//H
        model.newColumnForExport("MOVIES_EILayer\\sndset\\acousticFrequency", AcousticImportRow.PROPERTY_FREQUENCY, EchoBaseCsvUtil.PRIMITIVE_FLOAT);//I
//        model.newIgnoredColumn("MOVIES_EILayer\\sndset\\startSample");//J
//        model.newIgnoredColumn("MOVIES_EILayer\\sndset\\mainBeamAlongSteeringAngle");//K
//        model.newIgnoredColumn("MOVIES_EILayer\\sndset\\mainBeamAthwartSteeringAngle");//L
        model.newColumnForExport("MOVIES_EILayer\\sndset\\absorptionCoef", AcousticImportRow.PROPERTY_TRANSCEIVER_ACQUISITION_ABSORPTION, EchoBaseCsvUtil.PRIMITIVE_FLOAT);//M
        model.newColumnForExport("MOVIES_EILayer\\sndset\\transmissionPower", AcousticImportRow.PROPERTY_TRANSCEIVER_ACQUISITION_POWER, EchoBaseCsvUtil.PRIMITIVE_FLOAT);//N
        model.newColumnForExport("MOVIES_EILayer\\sndset\\beamAlongAngleSensitivity", AcousticImportRow.PROPERTY_TRANSDUCER_ACQUISITION_BEAM_ANGLE_ALONGSHIP, EchoBaseCsvUtil.PRIMITIVE_FLOAT);//O
        model.newColumnForExport("MOVIES_EILayer\\sndset\\beamAthwartAngleSensitivity", AcousticImportRow.PROPERTY_TRANSDUCER_ACQUISITION_BEAM_ANGLE_ATHWARTSHIP, EchoBaseCsvUtil.PRIMITIVE_FLOAT);//P
//        model.newIgnoredColumn("MOVIES_EILayer\\sndset\\beam3dBWidthAlong");//Q
//        model.newIgnoredColumn("MOVIES_EILayer\\sndset\\beam3dBWidthAthwart");//R
        model.newColumnForExport("MOVIES_EILayer\\sndset\\beamEquTwoWayAngle", AcousticImportRow.PROPERTY_TRANSDUCER_ACQUISITION_PSI, EchoBaseCsvUtil.PRIMITIVE_FLOAT);//S
        model.newColumnForExport("MOVIES_EILayer\\sndset\\beamGain", AcousticImportRow.PROPERTY_TRANSCEIVER_ACQUISITION_GAIN, EchoBaseCsvUtil.PRIMITIVE_FLOAT);//T
        model.newColumnForExport("MOVIES_EILayer\\sndset\\beamSACorrection", AcousticImportRow.PROPERTY_TRANSCEIVER_ACQUISITION_SACORRECTION, EchoBaseCsvUtil.PRIMITIVE_FLOAT);//U
//        model.newIgnoredColumn("MOVIES_EILayer\\sndset\\bottomDetectionMinDepth");//V
//        model.newIgnoredColumn("MOVIES_EILayer\\sndset\\bottomDetectionMaxDepth");//W
//        model.newIgnoredColumn("MOVIES_EILayer\\sndset\\bottomDetectionMinLevel");//X
//        model.newIgnoredColumn("MOVIES_EILayer\\sndset\\AlongTXRXWeightId");//Y
//        model.newIgnoredColumn("MOVIES_EILayer\\sndset\\AthwartTXRXWeightId");//Z
//        model.newIgnoredColumn("MOVIES_EILayer\\sndset\\SplitBeamAlongTXRXWeightId");//AA
//        model.newIgnoredColumn("MOVIES_EILayer\\sndset\\SplitBeamAthwartTXRXWeightId");//AB
        model.newColumnForExport("MOVIES_EILayer\\sndset\\bandWidth", AcousticImportRow.PROPERTY_BANDWIDTH, EchoBaseCsvUtil.PRIMITIVE_FLOAT);//AC
//        model.newIgnoredColumn("MOVIES_EILayer\\sndset\\tvgminrange");//AD
//        model.newIgnoredColumn("MOVIES_EILayer\\sndset\\tvgmaxrange");//AE
        model.newColumnForExport("MOVIES_EILayer\\sndset\\pulseduration", AcousticImportRow.PROPERTY_TRANSCEIVER_ACQUISITION_PULSE_LENGTH, EchoBaseCsvUtil.PRIMITIVE_FLOAT);//AF
//        model.newIgnoredColumn("MOVIES_EILayer\\shipnav");//AG
//        model.newIgnoredColumn("MOVIES_EILayer\\shipnav\\lat");//AH
//        model.newIgnoredColumn("MOVIES_EILayer\\shipnav\\long");//AI
//        model.newIgnoredColumn("MOVIES_EILayer\\shipnav\\alt");//AJ
//        model.newIgnoredColumn("MOVIES_EILayer\\shipnav\\gndspeed");//AK
//        model.newIgnoredColumn("MOVIES_EILayer\\shipnav\\gndcourse");//AL
//        model.newIgnoredColumn("MOVIES_EILayer\\shipnav\\surfspeed");//AM
//        model.newIgnoredColumn("MOVIES_EILayer\\shipnav\\surfcourse");//AN
//        model.newIgnoredColumn("MOVIES_EILayer\\shipnav\\driftspeed");//AO
//        model.newIgnoredColumn("MOVIES_EILayer\\shipnav\\driftcourse");//AP
//        model.newIgnoredColumn("MOVIES_EILayer\\shipnav\\heading");//AQ
//        model.newIgnoredColumn("MOVIES_EILayer\\shipnav\\roll");//AR
//        model.newIgnoredColumn("MOVIES_EILayer\\shipnav\\pitch");//AS
//        model.newIgnoredColumn("MOVIES_EILayer\\shipnav\\heave");//AT
        model.newColumnForExport("MOVIES_EILayer\\shipnav\\depth", AcousticImportRow.PROPERTY_ESDU_CELL_DATA_DEPTH);//AU
//        model.newIgnoredColumn("MOVIES_EILayer\\shipnav\\draught");//AV

        // Cell elementary Datas
//        model.newIgnoredColumn("MOVIES_EILayer\\cellset");//AW
        model.newColumnForExport("MOVIES_EILayer\\cellset\\cellnum", AcousticImportRow.PROPERTY_CELL_NUM, EchoBaseCsvUtil.PRIMITIVE_INTEGER);//AX
        model.newColumnForExport("MOVIES_EILayer\\cellset\\celltype", AcousticImportRow.PROPERTY_CELL_TYPE, EchoBaseCsvUtil.PRIMITIVE_INTEGER);//AY
        model.newColumnForExport("MOVIES_EILayer\\cellset\\depthstart", AcousticImportRow.PROPERTY_CELL_DEPTH_START, EchoBaseCsvUtil.NA_TO_FLOAT_PARSER_FORMATTER);//AZ
        model.newColumnForExport("MOVIES_EILayer\\cellset\\depthend", AcousticImportRow.PROPERTY_CELL_DEPTH_END, EchoBaseCsvUtil.NA_TO_FLOAT_PARSER_FORMATTER);//BA
//        model.newIgnoredColumn("MOVIES_EILayer\\cellset\\indexstart");//BB
//        model.newIgnoredColumn("MOVIES_EILayer\\cellset\\indexend");//BC
        model.newColumnForExport("MOVIES_EILayer\\cellset\\datestart", AcousticImportRow.PROPERTY_CELL_DATE_START, EchoBaseCsvUtil.IMPORT_DAY_TIME_ECHOBASE);//BD
        model.newColumnForExport("MOVIES_EILayer\\cellset\\dateend", AcousticImportRow.PROPERTY_CELL_DATE_END, EchoBaseCsvUtil.IMPORT_DAY_TIME_ECHOBASE);//BE

        model.newColumnForExport("MOVIES_EILayer\\cellset\\lat", AcousticImportRow.PROPERTY_CELL_LATITUDE, EchoBaseCsvUtil.PRIMITIVE_FLOAT);//BF
        model.newColumnForExport("MOVIES_EILayer\\cellset\\long", AcousticImportRow.PROPERTY_CELL_LONGITUDE, EchoBaseCsvUtil.PRIMITIVE_FLOAT);//BG
        model.newColumnForExport("MOVIES_EILayer\\cellset\\volume", AcousticImportRow.PROPERTY_CELL_VOLUME, EchoBaseCsvUtil.NA_TO_FLOAT_PARSER_FORMATTER);//BH
        model.newColumnForExport("MOVIES_EILayer\\cellset\\area", AcousticImportRow.PROPERTY_CELL_SURFACE, EchoBaseCsvUtil.PRIMITIVE_INTEGER);//BI
//        model.newIgnoredColumn("MOVIES_EILayer\\cellset\\diststart");//BJ
//        model.newIgnoredColumn("MOVIES_EILayer\\cellset\\distend");//BK
        model.newColumnForExport("MOVIES_EILayer\\cellset\\thresholdup", AcousticImportRow.PROPERTY_E_ITHRESHOLD_HIGH, EchoBaseCsvUtil.PRIMITIVE_INTEGER);//BL
        model.newColumnForExport("MOVIES_EILayer\\cellset\\thresholdlow", AcousticImportRow.PROPERTY_E_ITHRESHOLD_LOW, EchoBaseCsvUtil.PRIMITIVE_INTEGER);//BM
//        model.newIgnoredColumn("MOVIES_EILayer\\eilayer");//BN
        model.newColumnForExport("MOVIES_EILayer\\eilayer\\sa", AcousticImportRow.PROPERTY_CELL_NASC, EchoBaseCsvUtil.NA_TO_FLOAT_PARSER_FORMATTER);//BO
//        model.newIgnoredColumn("MOVIES_EILayer\\eilayer\\sv");//BP
        model.newColumnForExport("MOVIES_EILayer\\eilayer\\ni", AcousticImportRow.PROPERTY_CELL_NUMBER_OF_SAMPLES_ECHO_INTEGRATED, EchoBaseCsvUtil.NA_TO_INTEGER_PARSER_FORMATTER);//BQ
        model.newColumnForExport("MOVIES_EILayer\\eilayer\\nt", AcousticImportRow.PROPERTY_CELL_NUMBER_OF_SAMPLES_RECORDED, EchoBaseCsvUtil.NA_TO_INTEGER_PARSER_FORMATTER);//BR
//        model.newIgnoredColumn("MOVIES_EILayer\\boterr");//BR
//        model.newIgnoredColumn("MOVIES_EILayer\\boterr\\sa");//BT
//        model.newIgnoredColumn("MOVIES_EILayer\\boterr\\ni");//BU
        model.newColumnForExport("MOVIES_EILayer\\sndset\\soundcelerity", AcousticImportRow.PROPERTY_SOUND_CELERITY);//BV

        model.newColumnForExport(AcousticImportRow.PROPERTY_DATA_QUALITY, EchoBaseCsvUtil.DATA_QUALITY_FORMATTER); //BW
        model.newColumnForExport(AcousticImportRow.PROPERTY_LABEL, EchoBaseCsvUtil.NA_TO_STRING_PARSER_FORMATTER); //BX

        return model;
    }

    @Override
    public AcousticImportRow newEmptyInstance() {
        return new AcousticImportRow();
    }
}
