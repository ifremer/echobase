package fr.ifremer.echobase.services.service.removedata.strategy;

/*
 * #%L
 * EchoBase :: Services
 * %%
 * Copyright (C) 2011 - 2012 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.Sets;
import fr.ifremer.echobase.entities.ImportLog;
import fr.ifremer.echobase.entities.ImportType;
import fr.ifremer.echobase.entities.data.Cell;
import fr.ifremer.echobase.entities.data.DataAcquisition;
import fr.ifremer.echobase.entities.data.DataProcessing;
import fr.ifremer.echobase.entities.data.Transect;
import fr.ifremer.echobase.entities.data.Voyage;
import fr.ifremer.echobase.entities.references.Calibration;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.persistence.TopiaException;

import java.util.Set;
import fr.ifremer.echobase.entities.data.DataAcousticProvider;

/**
 * Remove a {@link ImportType#ACOUSTIC} import.
 *
 * Can remove only {@link DataAcquisition} or {@link Cell}.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.2
 */
public class AcousticRemoveDataStrategy extends AbstractRemoveDataStrategy<Voyage> {

    /** Logger. */
    private static final Log log =
            LogFactory.getLog(AcousticRemoveDataStrategy.class);

    @Override
    public long computeNbSteps(DataAcousticProvider<Voyage> provider, ImportLog importLog) {
        Voyage voyage = provider.getEntity();

        long result = getImportFileIdsCount(importLog);

        // add all cell results
        result += persistenceService.countVoyageCellResults(voyage);

        // add all postCell
        result += voyage.sizePostCell();

        // add all orphan cells
        result += persistenceService.countVoyageOrphanCells(voyage);
        return result;
    }

    @Override
    protected void removePreData(DataAcousticProvider<Voyage> provider) throws TopiaException {
        Voyage voyage = provider.getEntity();

        // remove all cell results
        removeVoyageCellResults(voyage);

        // remove post cell from top voyage
        removeVoyagePostCell(voyage);
    }

    @Override
    protected void removePostData(DataAcousticProvider<Voyage> provider) throws TopiaException {
        // remove orphans cells
        removeOrphanCells();
    }

    @Override
    protected void removeImportData(DataAcousticProvider<Voyage> provider, String id) throws TopiaException {

        if (id.startsWith(DataAcquisition.class.getName())) {

            // get dataAcquisition
            DataAcquisition dataAcquisition = persistenceService.getDataAcquisition(id);

            // remove it from cell
            Transect transect = persistenceService.getTransectContainsDataAcquisition(dataAcquisition);
            transect.removeDataAcquisition(dataAcquisition);

            // remove dataAcquisition
            persistenceService.deleteDataAcquisition(dataAcquisition);
            if (log.isDebugEnabled()) {
                log.debug(dataAcquisition.getTopiaId() + " was removed");
            }
        } else if (id.startsWith(Cell.class.getName())) {

            // get cell
            Cell cell = persistenceService.getCell(id);

            if (cell == null) {
                throw new IllegalStateException("Could not find cell " + id);
            }
            // remove it from the dataAcquisition
            DataProcessing dataProcessing = persistenceService.getDataProcessingContainsCell(cell);
            if (dataProcessing != null) {
                dataProcessing.removeCell(cell);
            }

            // remove cell
            persistenceService.deleteCell(cell);
            if (log.isDebugEnabled()) {
                log.debug(cell.getTopiaId() + " was removed");
            }
        } else if (id.startsWith(Calibration.class.getName())) {
            Calibration calibration = persistenceService.getCalibration(id);

            persistenceService.deleteCalibration(calibration);

            if (log.isDebugEnabled()) {
                log.debug(calibration.getTopiaId() + " was removed");
            }

        } else {
            canNotDealWithId(id);
        }
    }

    @Override
    public Set<ImportType> getPossibleSubImportType() {
        return Sets.newHashSet(ImportType.RESULT_VOYAGE,
                               ImportType.RESULT_ESDU,
                               ImportType.RESULT_MAP_FISH,
                               ImportType.RESULT_MAP_OTHER,
                               ImportType.RESULT_REGION);
    }
}
