/*
 * #%L
 * EchoBase :: Services
 * %%
 * Copyright (C) 2011 - 2012 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ifremer.echobase.services.service.importdata.csv;

import fr.ifremer.echobase.entities.data.Cell;
import fr.ifremer.echobase.entities.data.Data;
import fr.ifremer.echobase.entities.data.Voyage;
import fr.ifremer.echobase.entities.references.CellType;
import fr.ifremer.echobase.entities.references.DataQuality;

/**
 * Bean used as a row for import of {@link VoyageResultsRegionCellImportExportModel}.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 0.3
 */
public class VoyageResultsRegionCellImportRow {

    public static final String PROPERTY_NAME = Cell.PROPERTY_NAME;

    public static final String PROPERTY_VOYAGE = "voyage";

    public static final String PROPERTY_CELL_TYPE = Cell.PROPERTY_CELL_TYPE;

    public static final String PROPERTY_DATA_QUALITY = Data.PROPERTY_DATA_QUALITY;

    public static final String PROPERTY_DATA_COORDINATE = "dataCoordinate";

    public static final String PROPERTY_DATA_SURFACE = "dataSurface";

    protected Voyage voyage;

    protected CellType cellType;

    protected DataQuality dataQuality;

    protected String name;

    protected String dataCoordinate;

    protected float dataSurface;

    public static VoyageResultsRegionCellImportRow of(Voyage voyage, Data coordinateData, float surfaceDataValue) {
        VoyageResultsRegionCellImportRow row = new VoyageResultsRegionCellImportRow();
        row.setVoyage(voyage);
        Cell cell = coordinateData.getCell();
        row.setName(cell.getName());
        row.setCellType(cell.getCellType());
        row.setDataSurface(surfaceDataValue);
        row.setDataCoordinate(coordinateData.getDataValue());
        row.setDataQuality(cell.getDataQuality());
        return row;
    }

    public VoyageResultsRegionCellImportRow() {
    }

    public Voyage getVoyage() {
        return voyage;
    }

    public void setVoyage(Voyage voyage) {
        this.voyage = voyage;
    }

    public CellType getCellType() {
        return cellType;
    }

    public void setCellType(CellType cellType) {
        this.cellType = cellType;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDataCoordinate() {
        return dataCoordinate;
    }

    public void setDataCoordinate(String dataCoordinate) {
        this.dataCoordinate = dataCoordinate;
    }

    public float getDataSurface() {
        return dataSurface;
    }

    public void setDataSurface(float dataSurface) {
        this.dataSurface = dataSurface;
    }

    public DataQuality getDataQuality() {
        return dataQuality;
    }

    public void setDataQuality(DataQuality dataQuality) {
        this.dataQuality = dataQuality;
    }
}
