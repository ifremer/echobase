/*
 * #%L
 * EchoBase :: Services
 * %%
 * Copyright (C) 2011 - 2012 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ifremer.echobase.services.service.importdata.csv;

import fr.ifremer.echobase.entities.data.Operation;
import fr.ifremer.echobase.entities.data.Sample;
import fr.ifremer.echobase.entities.data.SampleImpl;
import fr.ifremer.echobase.entities.references.SexCategory;
import fr.ifremer.echobase.entities.references.SizeCategory;
import fr.ifremer.echobase.entities.references.Species;

/**
 * Bean used as a row for import of {@link VoyageCatchesSubSampleImportExportModel}.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 0.3
 */
public class VoyageCatchesSubSampleImportRow {

    public static final String PROPERTY_OPERATION = "operation";

    public static final String PROPERTY_SPECIES = "species";

    public static final String PROPERTY_SIZE_CATEGORY = "sizeCategory";

    public static final String PROPERTY_SEX_CATEGORY = "sexCategory";

    public static final String PROPERTY_NUMBER_AT_LENGTH = "numberAtLength";

    public static final String PROPERTY_WEIGHT_AT_LENGTH = "weightAtLength";

    public static final String PROPERTY_LENGTH_CLASS = "lengthClass";

    protected final Sample sample;

    protected Operation operation;
    protected Species species;
    protected SizeCategory sizeCategory;
    protected SexCategory sexCategory;
    protected float numberAtLength;
    protected Float weightAtLength;
    protected String lengthClass;

    public static VoyageCatchesSubSampleImportRow of(Operation operation, Sample sample) {
        VoyageCatchesSubSampleImportRow row = new VoyageCatchesSubSampleImportRow(sample);
        row.setOperation(operation);
        row.setSpecies(sample.getSpeciesCategory().getSpecies());
        row.setSizeCategory(sample.getSpeciesCategory().getSizeCategory());
        row.setSexCategory(sample.getSpeciesCategory().getSexCategory());
        return row;
    }

    public VoyageCatchesSubSampleImportRow(Sample sample) {
        this.sample = sample;
    }

    public VoyageCatchesSubSampleImportRow() {
        this(new SampleImpl());
    }

    public Sample getSample() {
        return sample;
    }

    public Operation getOperation() {
        return operation;
    }

    public void setOperation(Operation operation) {
        this.operation = operation;
    }

    public Species getSpecies() {
        return species;
    }

    public void setSpecies(Species species) {
        this.species = species;
    }

    public SizeCategory getSizeCategory() {
        return sizeCategory;
    }

    public void setSizeCategory(SizeCategory sizeCategory) {
        this.sizeCategory = sizeCategory;
    }

    public Float getSampleWeight() {
        return sample.getSampleWeight();
    }

    public void setSampleWeight(Float sampleWeight) {
        sample.setSampleWeight(sampleWeight);
    }

    public Integer getNumberSampled() {
        return sample.getNumberSampled();
    }

    public void setNumberSampled(Integer numberSampled) {
        sample.setNumberSampled(numberSampled);
    }

    public SexCategory getSexCategory() {
        return sexCategory;
    }

    public void setSexCategory(SexCategory sexCategory) {
        this.sexCategory = sexCategory;
    }

    public float getNumberAtLength() {
        return numberAtLength;
    }

    public void setNumberAtLength(float numberAtLength) {
        this.numberAtLength = numberAtLength;
    }

    public Float getWeightAtLength() {
        return weightAtLength;
    }

    public void setWeightAtLength(Float weightAtLength) {
        this.weightAtLength = weightAtLength;
    }

    public String getLengthClass() {
        return lengthClass;
    }

    public void setLengthClass(String lengthClass) {
        this.lengthClass = lengthClass;
    }
}
