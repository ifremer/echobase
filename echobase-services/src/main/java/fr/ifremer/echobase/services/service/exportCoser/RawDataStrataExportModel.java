package fr.ifremer.echobase.services.service.exportCoser;

/*
 * #%L
 * EchoBase :: Services
 * %%
 * Copyright (C) 2011 - 2014 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.Lists;
import fr.ifremer.echobase.services.csv.EchoBaseCsvUtil;
import fr.ifremer.echobase.entities.data.Cell;
import fr.ifremer.echobase.entities.data.Data;
import fr.ifremer.echobase.entities.data.Voyage;
import fr.ifremer.echobase.entities.references.DataMetadata;
import org.nuiton.csv.ext.AbstractExportModel;

import java.util.List;

/**
 * Created on 4/5/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.6
 */
public class RawDataStrataExportModel extends AbstractExportModel<RawDataStrataExportRow> {

    /*
    SELECT
    voyage.name AS Campagne,
    cell.name AS Strate,
    datametadata.name,
    data.datavalue AS Surface
    FROM
    voyage AS voyage,
    mission AS mission,
    cell AS cell,
    celltype AS celltype,
    data AS data,
    datametadata AS datametadata
    WHERE
    voyage.mission = mission.topiaid AND
    cell.voyage = voyage.topiaid AND
    cell.celltype = celltype.topiaid AND
    data.cell = cell.topiaid AND
    data.datametadata = datametadata.topiaid AND
    celltype.id = 'Region' AND
    datametadata.name = 'Surface'
     */

    public RawDataStrataExportModel(char separator) {
        super(separator);
        newColumnForExport("Campagne", EchoBaseCsvUtil.<RawDataStrataExportRow, String>newBeanProperty(RawDataStrataExportRow.PROPERTY_VOYAGE_NAME));
        newColumnForExport("Strate", EchoBaseCsvUtil.<RawDataStrataExportRow, String>newBeanProperty(RawDataStrataExportRow.PROPERTY_CELL_NAME));
        newColumnForExport("Surface", EchoBaseCsvUtil.<RawDataStrataExportRow, String>newBeanProperty(RawDataStrataExportRow.PROPERTY_DATA_VALUE));
    }

    public List<RawDataStrataExportRow> prepareRows(List<Voyage> voyages, DataMetadata surfaceMetadata) {
        List<RawDataStrataExportRow> rows = Lists.newArrayList();
        for (Voyage voyage : voyages) {
            prepareRows(voyage, surfaceMetadata, rows);
        }
        return rows;
    }

    protected void prepareRows(Voyage voyage, DataMetadata surfaceMetadata, List<RawDataStrataExportRow> rows) {

        for (Cell cell : voyage.getRegionCells()) {

            if (cell.isDataNotEmpty()) {

                for (Data data : cell.getData()) {

                    if (surfaceMetadata.equals(data.getDataMetadata())) {
                        RawDataStrataExportRow row = new RawDataStrataExportRow();
                        row.setVoyage(voyage);
                        row.setCell(cell);
                        row.setData(data);
                        rows.add(row);
                    }
                }
            }
        }

    }

}