package fr.ifremer.echobase.services.service.importdata.actions;

/*
 * #%L
 * EchoBase :: Services
 * %%
 * Copyright (C) 2011 - 2016 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import fr.ifremer.echobase.entities.EchoBaseUserEntityEnum;
import fr.ifremer.echobase.entities.ImportedCell;
import fr.ifremer.echobase.entities.data.Cell;
import fr.ifremer.echobase.entities.data.Data;
import fr.ifremer.echobase.entities.data.DataAcquisition;
import fr.ifremer.echobase.entities.data.DataProcessing;
import fr.ifremer.echobase.entities.data.Datas;
import fr.ifremer.echobase.entities.references.AcousticInstrument;
import fr.ifremer.echobase.entities.references.AcousticInstruments;
import fr.ifremer.echobase.entities.references.CellType;
import fr.ifremer.echobase.entities.references.DataMetadata;
import fr.ifremer.echobase.entities.references.DataMetadataImpl;
import fr.ifremer.echobase.entities.references.DataQuality;
import fr.ifremer.echobase.io.InputFile;
import fr.ifremer.echobase.services.csv.EchoBaseCsvUtil;
import fr.ifremer.echobase.services.service.importdata.DuplicatedEsduCellException;
import fr.ifremer.echobase.services.service.importdata.ElementaryCellWithoutDepthEndException;
import fr.ifremer.echobase.services.service.importdata.EsduCellNotFoundException;
import fr.ifremer.echobase.services.service.importdata.EsduCellWithDepthEndFilledException;
import fr.ifremer.echobase.services.service.importdata.ImportDataFileResult;
import fr.ifremer.echobase.services.service.importdata.configurations.ImportAcousticsConfiguration;
import fr.ifremer.echobase.services.service.importdata.contexts.ImportDataContextSupport;
import fr.ifremer.echobase.services.service.importdata.csv.AcousticImportRow;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.csv.Import;
import org.nuiton.csv.ImportRuntimeException;
import fr.ifremer.echobase.entities.data.DataAcousticProvider;
import fr.ifremer.echobase.services.service.importdata.DuplicatedElementaryCellException;
import java.util.Collection;

/**
 * @author Julien Ruchaud - ruchaud@codelutin.com
 * @since 4.0
 */
public abstract class ImportAcousticsActionSupport<M extends ImportAcousticsConfiguration, C extends ImportDataContextSupport<M>, E extends AcousticImportRow> extends ImportDataActionSupport<M, C, E> {

    /** Logger. */
    private static final Log log = LogFactory.getLog(ImportAcousticsActionSupport.class);

    private final DateFormat cellDateFormat;
    private final DataMetadataProvider dataMetadataProvider;

    public ImportAcousticsActionSupport(C importDataContext) {
        super(importDataContext, importDataContext.getConfiguration().getMoviesFile());
        
        this.cellDateFormat = new SimpleDateFormat(EchoBaseCsvUtil.CELLULE_DATE_FORMAT);
        this.dataMetadataProvider = new DataMetadataProvider(importDataContext);
    }

    @Override
    protected void doFlushTransaction(int rowNumber) {
        super.doFlushTransaction(rowNumber);
        
        if (rowNumber % TRANSACTION_FLUSH_MAX == 0) {
            getPersistenceService().clear();
        }
    }

    @Override
    protected void performImport(C importDataContext, InputFile inputFile, ImportDataFileResult result) {

        if (log.isInfoEnabled()) {
            log.info("Starts " + getImportMessage());
        }

        CellType esduCellType = importDataContext.getEsduCellType();
        CellType elementaryCellType = importDataContext.getElementaryCellType();

        boolean addDataAcquisition = getConfiguration().isAddDataAcquisition();

        List<Cell> elementaryCells = Lists.newArrayList();

        Locale locale = getLocale();
        String dataProcessingId = null;

        String processingTemplate = getConfiguration().getProcessingTemplate();

        try (Import<E> importer = open()) {
            DataAcquisition dataAcquisition = null;
            DataProcessing dataProcessing = null;

            Cell elementaryCell;
            int rowNumber = 0;

            incrementsProgress();

            Set<String> createdDataAcquisitions = Sets.newHashSet();

            for (E row : importer) {
                
                doFlushTransaction(++rowNumber);

                if (dataProcessingId == null) {

                    // compute once for all the common dataprocessing id used
                    // for all the movies file
                    dataProcessingId = row.getEiLayer() + processingTemplate;
                }

                Date endDate = row.getCellDateEnd();
                
                DataAcousticProvider dataProvider = getDataProvider(importDataContext, row, rowNumber);

                AcousticInstrument instrument = row.getAcousticInstrument();
                boolean isME70 = AcousticInstruments.IS_ACOUSTIC_INSTRUMENT_ME70.apply(instrument);

                float frequency = row.getFrequency();

                if (dataProcessing == null || !instrument.equals(dataAcquisition.getAcousticInstrument()) || frequency != dataProcessing.getFrequency()) {

                    // need to use another data acquisition

                    if (log.isDebugEnabled()) {
                        log.debug("[row " + rowNumber + "] New instrument to use (" + instrument.getId() + ")");
                    }

                    if (CollectionUtils.isNotEmpty(elementaryCells)) {

                        // means miss a esdu cell
                        // can not find correct transect
                        throw new EsduCellNotFoundException(locale, dataProvider, instrument, rowNumber);
                    }

                    String softwareVersion = getSoftwareVersion(isME70);

                    String soundSpeedCalculations = getSoundSpeedCalculations(isME70);

                    // try to get existing data acquisition
                    dataAcquisition = getDataAcquisition(dataProvider, instrument, frequency, addDataAcquisition);

                    if (dataAcquisition == null) {

                        // need to create the data acquisition
                        dataAcquisition = createDataAcquisition(instrument, softwareVersion, soundSpeedCalculations, row);

                        // collect id of the import
                        addId(result, EchoBaseUserEntityEnum.DataAcquisition, dataAcquisition, rowNumber);
                        createdDataAcquisitions.add(dataAcquisition.getTopiaId());

                        // add dataAcquisition to mooring
                        dataProvider.addDataAcquisition(dataAcquisition);

                        if (log.isDebugEnabled()) {
                            log.debug("[row " + rowNumber + "] New dataAquisition to use (number: " + result.getNumberCreated(EchoBaseUserEntityEnum.DataAcquisition) + ")");
                        }

                        // create data processing
                        dataProcessing = createDataProcessing(dataProcessingId, soundSpeedCalculations, row);

                        if (log.isDebugEnabled()) {
                            log.debug("[row " + rowNumber + "] New dataProcessing to use (" + dataProcessing.getId() + ")");
                        }

                        addId(result, EchoBaseUserEntityEnum.DataProcessing, dataProcessing, rowNumber);

                        // add it to data acquisition
                        dataAcquisition.addDataProcessing(dataProcessing);
                    }
                }

                int cellType = row.getCellType();

                int cellNum = row.getCellNum();
                String cellDate = cellDateFormat.format(endDate);
                String esduCellId = cellDate + "_" + cellNum;

                if (row.getCellNasc() == null) {

                    // dead cell not to be imported

                    String id = rowNumber + " - " + cellType + " : " + esduCellId;
                    if (log.isWarnEnabled()) {
                        log.warn("Will not import cell of row " + id);
                    }
                    result.addNotImportedId(EchoBaseUserEntityEnum.Cell, id);

                    continue;
                }

                addProcessedRow(result, row);

                DataQuality dataQuality = row.getDataQuality();

                boolean isEsduCell = cellType == 4;
                if (isEsduCell) {

                    if (log.isDebugEnabled()) {
                        log.debug("[row " + rowNumber + "] Esdu cell (" + esduCellId + ")");
                    }

                    // check name does not exist
                    boolean exists = persistenceService.containsCellByName(esduCellId);
                    if (exists) {
                        throw new DuplicatedEsduCellException(getLocale(), rowNumber, esduCellId, dataProvider);
                    }

                    // this is a esdu cell row

                    // create esdu cell
                    Cell esduCell = persistenceService.createCell(esduCellType, esduCellId, dataQuality);

                    if (row.getCellDepthEnd() != null) {
                        throw new EsduCellWithDepthEndFilledException(getLocale(), esduCell, rowNumber);
                    }

                    // add all found elementary cells
                    esduCell.addAllChilds(elementaryCells);

                    // clear elementary cells
                    elementaryCells.clear();

                    addId(result, EchoBaseUserEntityEnum.Cell, esduCell, rowNumber);

                    Preconditions.checkNotNull(dataProcessing);

                    // add it to data processing
                    dataProcessing.addCell(esduCell);

                    // create esdu cell data
                    createEsduCellData(esduCell, row, dataQuality, result, rowNumber);

                } else {
                    // this is a elementary cell row
                    boolean surface = cellType == 0;

                    String elementaryCellId = cellDate + "_" + cellNum + "_" + (surface ? "S" : "B");

                    // check name does not exist
                    boolean exists = persistenceService.containsCellByName(elementaryCellId);
                    if (exists) {
                        throw new DuplicatedElementaryCellException(getLocale(), rowNumber, elementaryCellId, dataProvider);
                    }
                    if (log.isTraceEnabled()) {
                        log.trace("[row " + rowNumber + "] elementary cell (" + elementaryCellId + ")");
                    }

                    // create the elementary cell
                    elementaryCell = persistenceService.createCell(elementaryCellType, elementaryCellId, dataQuality);

                    if (row.getCellDepthEnd() == null) {
                        throw new ElementaryCellWithoutDepthEndException(getLocale(), elementaryCell, rowNumber);
                    }

                    // keep (to attach them to esdu cell)
                    elementaryCells.add(elementaryCell);

                    addId(result, EchoBaseUserEntityEnum.Cell, elementaryCell, rowNumber);

                    // create datas of the elementary cell
                    createElementaryCellData(elementaryCell, surface, row, dataQuality, result, rowNumber);
                }
            }
        }
    }

    protected abstract DataAcousticProvider getDataProvider(C importDataContext, E row, int rowNumber);
    
    private DataAcquisition getDataAcquisition(DataAcousticProvider provider, AcousticInstrument instrument, float frequency, boolean addDataAcquisition) {

        DataAcquisition result = null;
        if (!addDataAcquisition && !provider.isDataAcquisitionEmpty()) {

            // try to obtain an existing data acquisition
            Collection<DataAcquisition> dataAcquisition = provider.getDataAcquisition();
            for (DataAcquisition acquisition : dataAcquisition) {

                if (instrument.equals(acquisition.getAcousticInstrument()) ) {

                    for (DataProcessing processing : acquisition.getDataProcessing()) {

                        if (frequency == processing.getFrequency()) {

                            // found a matching dataAcquisition from this acoustic instrument and frequency
                            result = acquisition;
                            break;
                        }
                    }
                }
            }
        }

        return result;

    }
        
    @Override
    protected void computeImportedExport(C importDataContext, ImportDataFileResult result) {

        String processingTemplate = getConfiguration().getProcessingTemplate();

        CellType esduCellType = importDataContext.getEsduCellType();

        List<Cell> elementaryCells = new LinkedList<>();

        for (ImportedCell importedCell : persistenceService.getImportedAcousticCells(result.getImportFile())) {

            if (log.isInfoEnabled()) {
                log.info("(Line :" + importedCell.getLineNumber() + " - Imported id: " + importedCell.getCell());
            }

            Cell cell = importedCell.getCell();

            if (esduCellType.equals(cell.getCellType())) {

                if (log.isDebugEnabled()) {
                    log.debug("Esdu Cell created: " + cell);
                }

                // flush previous esdu cell
                flushImportedEsduCell(result, processingTemplate, importedCell.getDataAcquisition(), importedCell.getDataProcessing(), cell, elementaryCells);
                elementaryCells.clear();

            } else {

                if (log.isDebugEnabled()) {
                    log.debug("Elementary Cell created: " + cell);
                }

                elementaryCells.add(cell);

            }

        }

        // Should not have any remaining elementary cells
        Preconditions.checkState(elementaryCells.isEmpty());

    }
    
    private Date formatDate(String date) {
        try {
            return cellDateFormat.parse(date);
        } catch (ParseException e) {
            throw new ImportRuntimeException("Can't parse cell date: " + date);
        }
    }

    private void flushImportedEsduCell(ImportDataFileResult result, String processingTemplate, DataAcquisition dataAcquisition, DataProcessing dataProcessing, Cell esduCell, List<Cell> elementaryCells) {

        Preconditions.checkArgument(!elementaryCells.isEmpty(), "cant have esdu with no elementary cells");

        Preconditions.checkNotNull(dataAcquisition);
        Preconditions.checkNotNull(dataProcessing);

        DataMetadata radialNumberDataMetadata = dataMetadataProvider.getRadialNumberDataMetadata();
        Preconditions.checkNotNull(radialNumberDataMetadata);

        E esduRow = (E) AcousticImportRow.ofEsduCell(processingTemplate, dataAcquisition, dataProcessing, esduCell, radialNumberDataMetadata);

        ImmutableMap<DataMetadata, Data> esduCellDataByMeta = Maps.uniqueIndex(esduCell.getData(), Datas.TO_DATA_METADATA);

        {
            Data data = esduCellDataByMeta.get(dataMetadataProvider.getLatitudeDataMetadata());
            Preconditions.checkNotNull(data);
            esduRow.setCellLatitude(Float.valueOf(data.getDataValue()));
        }
        {
            Data data = esduCellDataByMeta.get(dataMetadataProvider.getLongitudeDataMetadata());
            Preconditions.checkNotNull(data);
            esduRow.setCellLongitude(Float.valueOf(data.getDataValue()));
        }
        {
            Data data = esduCellDataByMeta.get(dataMetadataProvider.getEsduStartDepthDataMetadata());
            Preconditions.checkNotNull(data);
            esduRow.setEsduCellDataDepth(data.getDataValue());
        }
        {
            Data data = esduCellDataByMeta.get(dataMetadataProvider.getDepthRefStartDataMetadata(false));
            Preconditions.checkNotNull(data);
            esduRow.setCellDepthStart(Float.valueOf(data.getDataValue()));
        }
        {
            Data data = esduCellDataByMeta.get(dataMetadataProvider.getTimeStartDataMetadata());
            Preconditions.checkNotNull(data);
            esduRow.setCellDateStart(formatDate(data.getDataValue()));
        }
        {
            Data data = esduCellDataByMeta.get(dataMetadataProvider.getTimeEndDataMetadata());
            Preconditions.checkNotNull(data);
            esduRow.setCellDateEnd(formatDate(data.getDataValue()));
        }
        {
            Data data = esduCellDataByMeta.get(dataMetadataProvider.getSurfaceDataMetadata());
            Preconditions.checkNotNull(data);
            esduRow.setCellSurface(Integer.valueOf(data.getDataValue()));
        }
        {
            Data data = esduCellDataByMeta.get(dataMetadataProvider.getNascDataMetadata());
            Preconditions.checkNotNull(data);
            esduRow.setCellNasc(Float.valueOf(data.getDataValue()));
        }
        {
            Data data = esduCellDataByMeta.get(dataMetadataProvider.getVolumeDataMetadata());
            if (data != null) {
                esduRow.setCellVolume(Float.valueOf(data.getDataValue()));
            }
        }
        {
            Data data = esduCellDataByMeta.get(dataMetadataProvider.getNumberOfSamplesRecordedDataMetadata());
            if (data != null) {
                esduRow.setCellNumberOfSamplesRecorded(Integer.valueOf(data.getDataValue()));
            }
        }
        {
            Data data = esduCellDataByMeta.get(dataMetadataProvider.getNumberOfSamplesEchoIntegratedDataMetadata());
            if (data != null) {
                esduRow.setCellNumberOfSamplesEchoIntegrated(Integer.valueOf(data.getDataValue()));
            }
        }

        for (Cell elementaryCell : elementaryCells) {
            E elementaryRow = (E) AcousticImportRow.ofElementaryCell(processingTemplate, dataAcquisition, dataProcessing, elementaryCell, radialNumberDataMetadata);

            ImmutableMap<DataMetadata, Data> elementaryCellDataByMeta = Maps.uniqueIndex(elementaryCell.getData(), Datas.TO_DATA_METADATA);

            {
                Data data = elementaryCellDataByMeta.get(dataMetadataProvider.getLatitudeDataMetadata());
                Preconditions.checkNotNull(data);
                elementaryRow.setCellLatitude(Float.valueOf(data.getDataValue()));
            }
            {
                Data data = elementaryCellDataByMeta.get(dataMetadataProvider.getLongitudeDataMetadata());
                Preconditions.checkNotNull(data);
                elementaryRow.setCellLongitude(Float.valueOf(data.getDataValue()));
            }
            {
                Data data = elementaryCellDataByMeta.get(dataMetadataProvider.getDepthRefStartDataMetadata(true));
                if (data == null) {
                    data = elementaryCellDataByMeta.get(dataMetadataProvider.getDepthRefStartDataMetadata(false));
                }
                Preconditions.checkNotNull(data);
                elementaryRow.setCellDepthStart(Float.valueOf(data.getDataValue()));
            }
            {
                Data data = elementaryCellDataByMeta.get(dataMetadataProvider.getDepthRefEndDataMetadata(true));
                if (data == null) {
                    data = elementaryCellDataByMeta.get(dataMetadataProvider.getDepthRefEndDataMetadata(false));
                }
                Preconditions.checkNotNull(data);
                elementaryRow.setCellDepthEnd(Float.valueOf(data.getDataValue()));
            }
            {
                Data data = elementaryCellDataByMeta.get(dataMetadataProvider.getTimeStartDataMetadata());
                Preconditions.checkNotNull(data);
                elementaryRow.setCellDateStart(formatDate(data.getDataValue()));
            }
            {
                Data data = elementaryCellDataByMeta.get(dataMetadataProvider.getTimeEndDataMetadata());
                Preconditions.checkNotNull(data);
                elementaryRow.setCellDateEnd(formatDate(data.getDataValue()));
            }
            {
                Data data = elementaryCellDataByMeta.get(dataMetadataProvider.getSurfaceDataMetadata());
                Preconditions.checkNotNull(data);
                elementaryRow.setCellSurface(Integer.valueOf(data.getDataValue()));
            }
            {
                Data data = elementaryCellDataByMeta.get(dataMetadataProvider.getNascDataMetadata());
                Preconditions.checkNotNull(data);
                elementaryRow.setCellNasc(Float.valueOf(data.getDataValue()));
            }
            {
                Data data = elementaryCellDataByMeta.get(dataMetadataProvider.getVolumeDataMetadata());
                if (data != null) {
                    elementaryRow.setCellVolume(Float.valueOf(data.getDataValue()));
                }
            }
            {
                Data data = elementaryCellDataByMeta.get(dataMetadataProvider.getNumberOfSamplesRecordedDataMetadata());
                if (data != null) {
                    elementaryRow.setCellNumberOfSamplesRecorded(Integer.valueOf(data.getDataValue()));
                }
            }
            {
                Data data = elementaryCellDataByMeta.get(dataMetadataProvider.getNumberOfSamplesEchoIntegratedDataMetadata());
                if (data != null) {
                    elementaryRow.setCellNumberOfSamplesEchoIntegrated(Integer.valueOf(data.getDataValue()));
                }
            }

            elementaryRow.setEsduCellDataDepth(esduRow.getEsduCellDataDepth());

            addImportedRow(result, elementaryRow);
        }

        addImportedRow(result, esduRow);
    }

    private void createEsduCellData(Cell cell,
                                    E row,
                                    DataQuality dataQuality,
                                    ImportDataFileResult importResult,
                                    int rowNumber) {

        String dataValue;

        // create Latitude data
        dataValue = String.valueOf(row.getCellLatitude());
        createCellData(cell, dataMetadataProvider.getLatitudeDataMetadata(), dataValue, dataQuality, importResult, false, rowNumber);

        // create Longitude data
        dataValue = String.valueOf(row.getCellLongitude());
        createCellData(cell, dataMetadataProvider.getLongitudeDataMetadata(), dataValue, dataQuality, importResult, false, rowNumber);

        // create Depth data
        dataValue = row.getEsduCellDataDepth();
        createCellData(cell, dataMetadataProvider.getEsduStartDepthDataMetadata(), dataValue, dataQuality, importResult, false, rowNumber);

        // create depth start data
        dataValue = String.valueOf(row.getCellDepthStart());
        createCellData(cell, dataMetadataProvider.getDepthRefStartDataMetadata(false), dataValue, dataQuality, importResult, false, rowNumber);

        // create Time Start data
        dataValue = cellDateFormat.format(row.getCellDateStart());
        createCellData(cell, dataMetadataProvider.getTimeStartDataMetadata(), dataValue, dataQuality, importResult, false, rowNumber);

        // create Time end data
        dataValue = cellDateFormat.format(row.getCellDateEnd());
        createCellData(cell, dataMetadataProvider.getTimeEndDataMetadata(), dataValue, dataQuality, importResult, false, rowNumber);

        // create Surface data
        dataValue = String.valueOf(row.getCellSurface());
        createCellData(cell, dataMetadataProvider.getSurfaceDataMetadata(), dataValue, dataQuality, importResult, false, rowNumber);

        // create NASC data
        dataValue = String.valueOf(row.getCellNasc());
        createCellData(cell, dataMetadataProvider.getNascDataMetadata(), dataValue, dataQuality, importResult, false, rowNumber);

        // create Volume data
        if (row.getCellVolume() != null) {
            dataValue = String.valueOf(row.getCellVolume());
            createCellData(cell, dataMetadataProvider.getVolumeDataMetadata(), dataValue, dataQuality, importResult, false, rowNumber);
        }

        // create NumberOfSamplesRecorded data
        if (row.getCellNumberOfSamplesRecorded() != null) {
            dataValue = String.valueOf(row.getCellNumberOfSamplesRecorded());
            createCellData(cell, dataMetadataProvider.getNumberOfSamplesRecordedDataMetadata(), dataValue, dataQuality, importResult, false, rowNumber);
        }

        // create NumberOfSamplesEchoIntegrated data
        if (row.getCellNumberOfSamplesEchoIntegrated() != null) {
            dataValue = String.valueOf(row.getCellNumberOfSamplesEchoIntegrated());
            createCellData(cell, dataMetadataProvider.getNumberOfSamplesEchoIntegratedDataMetadata(), dataValue, dataQuality, importResult, false, rowNumber);
        }

        // create RadialNumber data
        if (row.getLabel() != null) {
            dataValue = row.getLabel();
            createCellData(cell, dataMetadataProvider.getRadialNumberDataMetadata(), dataValue, dataQuality, importResult, false, rowNumber);
        }

    }

    private void createElementaryCellData(Cell cell,
                                          boolean surface,
                                          E row,
                                          DataQuality dataQuality,
                                          ImportDataFileResult importResult,
                                          int rowNumber) {

        String dataValue;

        // create Latitude data
        dataValue = String.valueOf(row.getCellLatitude());
        createCellData(cell, dataMetadataProvider.getLatitudeDataMetadata(), dataValue, dataQuality, importResult, false, rowNumber);

        // create Longitude data
        dataValue = String.valueOf(row.getCellLongitude());
        createCellData(cell, dataMetadataProvider.getLongitudeDataMetadata(), dataValue, dataQuality, importResult, false, rowNumber);

        // create depth start data
        dataValue = String.valueOf(row.getCellDepthStart());
        createCellData(cell, dataMetadataProvider.getDepthRefStartDataMetadata(surface), dataValue, dataQuality, importResult, false, rowNumber);
        
        // create depth end data
        dataValue = String.valueOf(row.getCellDepthEnd());
        createCellData(cell, dataMetadataProvider.getDepthRefEndDataMetadata(surface), dataValue, dataQuality, importResult, false, rowNumber);

        // create Time Start data
        dataValue = cellDateFormat.format(row.getCellDateStart());
        createCellData(cell, dataMetadataProvider.getTimeStartDataMetadata(), dataValue, dataQuality, importResult, false, rowNumber);

        // create Time end data
        dataValue = cellDateFormat.format(row.getCellDateEnd());
        createCellData(cell, dataMetadataProvider.getTimeEndDataMetadata(), dataValue, dataQuality, importResult, false, rowNumber);

        // create Surface data
        dataValue = String.valueOf(row.getCellSurface());
        createCellData(cell, dataMetadataProvider.getSurfaceDataMetadata(), dataValue, dataQuality, importResult, false, rowNumber);

        // create acoustic density data
        dataValue = String.valueOf(row.getCellNasc());
        createCellData(cell, dataMetadataProvider.getNascDataMetadata(), dataValue, dataQuality, importResult, false, rowNumber);

        // create Volume data
        if (row.getCellVolume() != null) {
            dataValue = String.valueOf(row.getCellVolume());
            createCellData(cell, dataMetadataProvider.getVolumeDataMetadata(), dataValue, dataQuality, importResult, false, rowNumber);
        }

        // create NumberOfSamplesRecorded data
        if (row.getCellNumberOfSamplesRecorded() != null) {
            dataValue = String.valueOf(row.getCellNumberOfSamplesRecorded());
            createCellData(cell, dataMetadataProvider.getNumberOfSamplesRecordedDataMetadata(), dataValue, dataQuality, importResult, false, rowNumber);
        }
        // create NumberOfSamplesEchoIntegrated data
        if (row.getCellNumberOfSamplesEchoIntegrated() != null) {
            dataValue = String.valueOf(row.getCellNumberOfSamplesEchoIntegrated());
            createCellData(cell, dataMetadataProvider.getNumberOfSamplesEchoIntegratedDataMetadata(), dataValue, dataQuality, importResult, false, rowNumber);
        }

        // create RadialNumber data
        if (row.getLabel() != null) {
            dataValue = row.getLabel();
            createCellData(cell, dataMetadataProvider.getRadialNumberDataMetadata(), dataValue, dataQuality, importResult, false, rowNumber);
        }

    }

    private DataAcquisition createDataAcquisition(AcousticInstrument instrument,
                                                  String softwareVersion,
                                                  String soundSpeedCalculations,
                                                  E row) {

        M configuration = getConfiguration();

        String transceiverAcquisitionAbsorptionDescription = configuration.getTransceiverAcquisitionAbsorptionDescription();
        String loggedDataFormat = configuration.getLoggedDataFormat();
        String loggedDataDatatype = configuration.getLoggedDataDatatype();
        String pingDutyCycle = configuration.getPingDutyCycle();
        String softwareName = configuration.getAcquisitionSoftwareName();

        DataAcquisition dataAcquisition = getPersistenceService().createDataAcquisition(instrument);

        // fill from manual configuration
        dataAcquisition.setTransceiverAcquisitionAbsorptionDescription(transceiverAcquisitionAbsorptionDescription);
        dataAcquisition.setAcquisitionSoftwareVersion(softwareVersion);
        dataAcquisition.setLoggedDataFormat(loggedDataFormat);
        dataAcquisition.setLoggedDataDatatype(loggedDataDatatype);
        dataAcquisition.setPingDutyCycle(pingDutyCycle);
        dataAcquisition.setEchosounderSoundSpeed(row.getSoundCelerity());
        dataAcquisition.setSoundSpeedCalculations(soundSpeedCalculations);
        dataAcquisition.setSoftwareName(softwareName);

        // fill from csv file
        float transceiverAcquisitionAbsorption = row.getTransceiverAcquisitionAbsorption();
        float transducerAcquisitionBeamAngleAthwartship = row.getTransducerAcquisitionBeamAngleAthwartship();
        float transducerAcquisitionBeamAngleAlongship = row.getTransducerAcquisitionBeamAngleAlongship();
        float transducerAcquisitionPsi = row.getTransducerAcquisitionPsi();
        float transceiverAcquisitionPower = row.getTransceiverAcquisitionPower();
        float transceiverAcquisitionPulseLength = row.getTransceiverAcquisitionPulseLength();
        float transceiverAcquisitionGain = row.getTransceiverAcquisitionGain();
        float transceiverAcquisitionSacorrection = row.getTransceiverAcquisitionSacorrection();

        dataAcquisition.setTransceiverAcquisitionAbsorption(transceiverAcquisitionAbsorption);
        dataAcquisition.setTransducerAcquisitionBeamAngleAthwartship(transducerAcquisitionBeamAngleAthwartship);
        dataAcquisition.setTransducerAcquisitionBeamAngleAlongship(transducerAcquisitionBeamAngleAlongship);
        dataAcquisition.setTransducerAcquisitionPsi(transducerAcquisitionPsi);
        dataAcquisition.setTransceiverAcquisitionPower(transceiverAcquisitionPower);
        dataAcquisition.setTransceiverAcquisitionPulseLength(transceiverAcquisitionPulseLength);
        dataAcquisition.setTransceiverAcquisitionGain(transceiverAcquisitionGain);
        dataAcquisition.setTransceiverAcquisitionSacorrection(transceiverAcquisitionSacorrection);

        return dataAcquisition;
    }

    private DataProcessing createDataProcessing(String id,
                                                String soundSpeedCalculations,
                                                E row) {

        M configuration = getConfiguration();

        String transceiverAcquisitionAbsorptionDescription = configuration.getTransceiverAcquisitionAbsorptionDescription();
        String processingTemplate = configuration.getProcessingTemplate();
        String processingDescription = configuration.getProcessingDescription();
        String sounderConstant = configuration.getSounderConstant();
        float digitThreshold = configuration.getDigitThreshold();
        String acousticDensityUnit = configuration.getAcousticDensityUnit();
        String notes = configuration.getNotes();
        String softwareName = configuration.getProcessingSoftwareName();
        String softwareVersion = configuration.getProcessingSoftwareVersion();

        DataProcessing dataProcessing = getPersistenceService().createDataProcessing(id, processingTemplate);

        // fill from manual configuration
        dataProcessing.setProcessingDescription(processingDescription);
        dataProcessing.setSounderConstant(sounderConstant);
        dataProcessing.setDigitThreshold(digitThreshold);
        dataProcessing.setAcousticDensityUnit(acousticDensityUnit);
        dataProcessing.setNotes(notes);
        dataProcessing.setSoftwareName(softwareName);
        dataProcessing.setProcessingSoftwareVersion(softwareVersion);
        dataProcessing.setTransceiverProcessingAbsorptionDescription(transceiverAcquisitionAbsorptionDescription);
        dataProcessing.setEchosounderSoundSpeed(row.getSoundCelerity());
        dataProcessing.setSoundSpeedCalculations(soundSpeedCalculations);

        // fill from csv file
        float transceiverAcquisitionAbsorption = row.getTransceiverAcquisitionAbsorption();
        float transducerAcquisitionBeamAngleAthwartship = row.getTransducerAcquisitionBeamAngleAthwartship();
        float transducerAcquisitionBeamAngleAlongship = row.getTransducerAcquisitionBeamAngleAlongship();
        float transducerAcquisitionPsi = row.getTransducerAcquisitionPsi();
        float transceiverAcquisitionGain = row.getTransceiverAcquisitionGain();
        float transceiverAcquisitionSacorrection = row.getTransceiverAcquisitionSacorrection();
        int eIThresholdLow = row.geteIThresholdLow();
        int eIThresholdHigh = row.geteIThresholdHigh();
        String channelId = row.getAcousticInstrument().getId();
        float bandwidth = row.getBandwidth();
        float frequency = row.getFrequency();

        dataProcessing.seteIThresholdLow(eIThresholdLow);
        dataProcessing.seteIThresholdHigh(eIThresholdHigh);
        dataProcessing.setTransceiverProcessingSacorrection(transceiverAcquisitionSacorrection);
        dataProcessing.setTransceiverProcessingAbsorption(transceiverAcquisitionAbsorption);
        dataProcessing.setTransceiverProcessingGain(transceiverAcquisitionGain);
        dataProcessing.setTransducerProcessingPsi(transducerAcquisitionPsi);
        dataProcessing.setTransducerProcessingBeamAngleAthwartship(transducerAcquisitionBeamAngleAthwartship);
        dataProcessing.setTransducerProcessingBeamAngleAlongship(transducerAcquisitionBeamAngleAlongship);
        dataProcessing.setChannelId(channelId);
        dataProcessing.setBandwidth(bandwidth);
        dataProcessing.setFrequency(frequency);

        return dataProcessing;
    }

    private String getSoftwareVersion(boolean isME70) {
        String result;
        if (isME70) {
            result = getConfiguration().getAcquisitionSoftwareVersionME70();
        } else {
            result = getConfiguration().getAcquisitionSoftwareVersionER60();
        }
        return result;
    }

    private String getSoundSpeedCalculations(boolean isME70) {
        String result;
        if (isME70) {
            result = getConfiguration().getSoundSpeedCalculationsME70();
        } else {
            result = getConfiguration().getSoundSpeedCalculationsER60();
        }
        return result;
    }
    public static class DataMetadataProvider<M extends ImportAcousticsConfiguration, C extends ImportDataContextSupport<M>> {

        private final DataMetadata radialNumberDataMetadata;
        private final DataMetadata latitudeDataMetadata;
        private final DataMetadata longitudeDataMetadata;
        private final DataMetadata esduStartDepthDataMetadata;
        private final DataMetadata timeStartDataMetadata;
        private final DataMetadata timeEndDataMetadata;
        private final DataMetadata nascDataMetadata;
        private final DataMetadata volumeDataMetadata;
        private final DataMetadata surfaceDataMetadata;
        private final DataMetadata numberOfSamplesRecordedDataMetadata;
        private final DataMetadata numberOfSamplesEchoIntegratedDataMetadata;
        private final DataMetadata depthRefSurfaceStartDataMetadata;
        private final DataMetadata depthRefSurfaceEndDataMetadata;
        private final DataMetadata depthRefBottomStartDataMetadata;
        private final DataMetadata depthRefBottomEndDataMetadata;

        public DataMetadataProvider(C importDataContext) {
            String suffix = importDataContext.getConfiguration().getCellPositionReference().getMetadataNameSuffix();
            Map<String, DataMetadata> dataMetadatasByName = importDataContext.getDataMetadatasByName();
            radialNumberDataMetadata= dataMetadatasByName.get(DataMetadataImpl.RADIAL_NUMBER);
            latitudeDataMetadata = dataMetadatasByName.get("Latitude" + suffix);
            longitudeDataMetadata = dataMetadatasByName.get("Longitude" + suffix);
            esduStartDepthDataMetadata = dataMetadatasByName.get("ESDUstartDepth");
            timeStartDataMetadata = dataMetadatasByName.get("TimeStart");
            timeEndDataMetadata = dataMetadatasByName.get("TimeEnd");
            nascDataMetadata = dataMetadatasByName.get("NASC");
            volumeDataMetadata = dataMetadatasByName.get("Volume");
            surfaceDataMetadata = dataMetadatasByName.get("Surface");
            numberOfSamplesRecordedDataMetadata = dataMetadatasByName.get("NumberOfSamplesRecorded");
            numberOfSamplesEchoIntegratedDataMetadata = dataMetadatasByName.get("NumberOfSamplesEchoIntegrated");
            depthRefSurfaceStartDataMetadata = dataMetadatasByName.get("DepthRefSurfaceStart");
            depthRefSurfaceEndDataMetadata = dataMetadatasByName.get("DepthRefSurfaceEnd");
            depthRefBottomStartDataMetadata = dataMetadatasByName.get("DepthRefBottomStart");
            depthRefBottomEndDataMetadata = dataMetadatasByName.get("DepthRefBottomEnd");
        }

        public DataMetadata getRadialNumberDataMetadata() {
            return radialNumberDataMetadata;
        }

        public DataMetadata getLatitudeDataMetadata() {
            return latitudeDataMetadata;
        }

        public DataMetadata getLongitudeDataMetadata() {
            return longitudeDataMetadata;
        }

        public DataMetadata getEsduStartDepthDataMetadata() {
            return esduStartDepthDataMetadata;
        }

        public DataMetadata getTimeStartDataMetadata() {
            return timeStartDataMetadata;
        }

        public DataMetadata getTimeEndDataMetadata() {
            return timeEndDataMetadata;
        }

        public DataMetadata getNascDataMetadata() {
            return nascDataMetadata;
        }

        public DataMetadata getVolumeDataMetadata() {
            return volumeDataMetadata;
        }

        public DataMetadata getSurfaceDataMetadata() {
            return surfaceDataMetadata;
        }

        public DataMetadata getNumberOfSamplesRecordedDataMetadata() {
            return numberOfSamplesRecordedDataMetadata;
        }

        public DataMetadata getNumberOfSamplesEchoIntegratedDataMetadata() {
            return numberOfSamplesEchoIntegratedDataMetadata;
        }

        public DataMetadata getDepthRefStartDataMetadata(boolean surface) {
            return surface ? depthRefSurfaceStartDataMetadata : depthRefBottomStartDataMetadata;
        }

        public DataMetadata getDepthRefEndDataMetadata(boolean surface) {
            return surface ? depthRefSurfaceEndDataMetadata : depthRefBottomEndDataMetadata;
        }

    }

}
