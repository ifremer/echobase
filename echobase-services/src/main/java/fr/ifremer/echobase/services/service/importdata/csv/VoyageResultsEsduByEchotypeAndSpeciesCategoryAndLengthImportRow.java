/*
 * #%L
 * EchoBase :: Services
 * %%
 * Copyright (C) 2011 - 2012 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ifremer.echobase.services.service.importdata.csv;

import fr.ifremer.echobase.entities.data.DataAcousticProvider;
import fr.ifremer.echobase.entities.data.Category;
import fr.ifremer.echobase.entities.data.Cell;
import fr.ifremer.echobase.entities.data.Echotype;
import fr.ifremer.echobase.entities.data.Result;
import fr.ifremer.echobase.entities.data.Voyage;
import fr.ifremer.echobase.entities.references.DataQuality;
import fr.ifremer.echobase.entities.references.SizeCategory;
import fr.ifremer.echobase.entities.references.Species;
import fr.ifremer.echobase.services.csv.CellAble;
import fr.ifremer.echobase.services.csv.ResultAble;

import java.util.LinkedList;
import java.util.List;

/**
 * Bean used as a row for import of
 * {@link VoyageResultsEsduByEchotypeAndSpeciesCategoryAndLengthImportExportModel}.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 0.3
 */
public class VoyageResultsEsduByEchotypeAndSpeciesCategoryAndLengthImportRow implements ResultAble, CellAble<Voyage> {

    public static final String PROPERTY_VOYAGE = "voyage";
    public static final String PROPERTY_CELL = "cell";
    public static final String PROPERTY_SPECIES = "species";
    public static final String PROPERTY_ECHOTYPE = "echotype";
    public static final String PROPERTY_LENGTH_CLASS = "lengthClass";
    public static final String PROPERTY_SIZE_CATEGORY = "sizeCategory";
    public static final String PROPERTY_LENGTH_CLASS_MEANING = "lengthClassMeaning";
    public static final String PROPERTY_DATA_QUALITY = "dataQuality";

    protected Voyage voyage;
    protected Cell cell;
    protected final List<Result> result = new LinkedList<>();
    protected Species species;
    protected DataQuality dataQuality;
    protected Float lengthClass;
    protected String sizeCategory;
    protected String lengthClassMeaning;
    protected Echotype echotype;

    public static VoyageResultsEsduByEchotypeAndSpeciesCategoryAndLengthImportRow of(DataAcousticProvider provider, Cell cell, Category category, SizeCategory lengthCategory, List<Result> results) {
        VoyageResultsEsduByEchotypeAndSpeciesCategoryAndLengthImportRow row = new VoyageResultsEsduByEchotypeAndSpeciesCategoryAndLengthImportRow();
        row.setProvider(provider);
        row.setCell(cell);
        row.setSpecies(category.getSpeciesCategory().getSpecies());
        
        row.setLengthClass(category.getSpeciesCategory().getLengthClass());
        row.setLengthClassMeaning(lengthCategory.getMeaning());
        
        SizeCategory sizeCategory = category.getSpeciesCategory().getSizeCategory();
        if (sizeCategory != null) {
            row.setSizeCategory(sizeCategory.getName());
        }
        row.setEchotype(category.getEchotype());
        row.setDataQuality(results.get(0).getDataQuality());
        row.result.addAll(results);
        return row;
    }

    public Voyage getVoyage() {
        return voyage;
    }

    public void setVoyage(Voyage voyage) {
        this.voyage = voyage;
    }

    @Override
    public DataAcousticProvider<Voyage> getProvider() {
        return voyage;
    }

    @Override
    public void setProvider(DataAcousticProvider<Voyage> provider) {
        this.voyage = provider.getEntity();
    }

    public Species getSpecies() {
        return species;
    }

    public void setSpecies(Species species) {
        this.species = species;
    }

    @Override
    public Cell getCell() {
        return cell;
    }

    @Override
    public void setCell(Cell cell) {
        this.cell = cell;
    }

    @Override
    public List<Result> getResult() {
        return result;
    }

    @Override
    public void addResult(Result result) {
        this.result.add(result);
    }

    @Override
    public DataQuality getDataQuality() {
        return dataQuality;
    }

    @Override
    public void setDataQuality(DataQuality dataQuality) {
        this.dataQuality = dataQuality;
    }

    public String getSizeCategory() {
        return sizeCategory;
    }

    public void setSizeCategory(String sizeCategory) {
        this.sizeCategory = sizeCategory;
    }

    public String getLengthClassMeaning() {
        return lengthClassMeaning;
    }

    public void setLengthClassMeaning(String lengthClassMeaning) {
        this.lengthClassMeaning = lengthClassMeaning;
    }

    public Float getLengthClass() {
        return lengthClass;
    }

    public void setLengthClass(Float lengthClass) {
        this.lengthClass = lengthClass;
    }

    public Echotype getEchotype() {
        return echotype;
    }

    public void setEchotype(Echotype echotype) {
        this.echotype = echotype;
    }
}
