package fr.ifremer.echobase.services.service.importdata.csv;

/*
 * #%L
 * EchoBase :: Services
 * %%
 * Copyright (C) 2011 - 2016 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.echobase.entities.references.DataMetadata;
import fr.ifremer.echobase.services.csv.EchoBaseCsvUtil;
import fr.ifremer.echobase.services.csv.ResultAble;
import org.nuiton.csv.ext.AbstractImportExportModel;
import org.nuiton.topia.persistence.TopiaEntity;
import org.nuiton.topia.service.csv.TopiaCsvCommons;

import java.util.List;
import java.util.Map;

/**
 * Created on 27/03/16.
 *
 * @author Tony Chemit - chemit@codelutin.com
 */
public abstract class EchoBaseImportExportModelSupport<E> extends AbstractImportExportModel<E> {

    protected EchoBaseImportExportModelSupport(char separator) {
        super(separator);
    }

    protected static <R extends ResultAble> void addResultsColumnsForImport(EchoBaseImportExportModelSupport<R> model, List<DataMetadata> dataMetadatas) {
        for (DataMetadata metadata : dataMetadatas) {
            String name = metadata.getName();
            model.newMandatoryColumn(
                    name,
                    EchoBaseCsvUtil.newResultValueParser(metadata, false),
                    EchoBaseCsvUtil.<R>newResultValueSetter());
        }
    }

    protected static <R extends ResultAble> void addResultsColumns(EchoBaseImportExportModelSupport<R> model, List<DataMetadata> dataMetadatas) {
        for (DataMetadata metadata : dataMetadatas) {
            model.newColumnForExport(
                    metadata.getName(),
                    EchoBaseCsvUtil.<R>newResultValueGetter(metadata),
                    EchoBaseCsvUtil.newResultValueFormatter(metadata, true));
        }
    }

    public <E extends TopiaEntity> void newForeignKeyColumn(String headerName, String propertyName, Class<E> entityType, String foreignKeyName, Map<String, E> universe) {
        newMandatoryColumn(headerName, propertyName, TopiaCsvCommons.newForeignKeyValue(entityType, foreignKeyName, universe));
    }

    public <E extends TopiaEntity> void newForeignKeyColumn(String propertyName, Class<E> entityType, String foreignKeyName, Map<String, E> universe) {
        newMandatoryColumn(propertyName, propertyName, TopiaCsvCommons.newForeignKeyValue(entityType, foreignKeyName, universe));
    }

}
