package fr.ifremer.echobase.services.service.exportCoser;

/*
 * #%L
 * EchoBase :: Services
 * %%
 * Copyright (C) 2011 - 2014 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.Lists;
import fr.ifremer.echobase.services.csv.EchoBaseCsvUtil;
import fr.ifremer.echobase.entities.data.Operation;
import fr.ifremer.echobase.entities.data.Sample;
import fr.ifremer.echobase.entities.data.SampleData;
import fr.ifremer.echobase.entities.data.Transect;
import fr.ifremer.echobase.entities.data.Transit;
import fr.ifremer.echobase.entities.data.Voyage;
import fr.ifremer.echobase.entities.references.SampleDataType;
import fr.ifremer.echobase.entities.references.SexCategory;
import fr.ifremer.echobase.entities.references.Species;
import fr.ifremer.echobase.entities.references.SpeciesCategory;
import org.apache.commons.collections4.keyvalue.MultiKey;
import org.apache.commons.collections4.map.MultiKeyMap;
import org.nuiton.csv.ValueFormatter;
import org.nuiton.csv.ext.AbstractExportModel;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Created on 4/5/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.6
 */
public class RawDataSizeExportModel extends AbstractExportModel<RawDataSizeExportRow> {

    /*
    SELECT
    voyage.name AS Campagne,
    operation.id AS Trait,
    species.baracoudacode AS Espece,
    sexcategory.name AS Sexe,
    sampledata.datalabel,
    sampledata.datavalue,
    sampledatatype.name
    FROM
    voyage AS voyage,
    mission AS mission,
    transit AS transit,
    transect AS transect,
    operation AS operation,
    sample AS sample,
    sampledata AS sampledata,
    sampledatatype AS sampledatatype,
    sampletype AS sampletype,
    speciescategory AS speciescategory,
    species AS species,
    sizecategory AS sizecategory,
    sexcategory AS sexcategory
    WHERE
    voyage.mission = mission.topiaid AND
    transit.voyage = voyage.topiaid AND
    transect.transit = transit.topiaid AND
    operation.transect = transect.topiaid AND
    sample.operation = operation.topiaid AND
    sampledata.sample = sample.topiaid AND
    sampledata.sampledatatype = sampledatatype.topiaid AND
    sample.sampletype = sampletype.topiaid AND
    sample.speciescategory = speciescategory.topiaid AND
    speciescategory.species = species.topiaid AND
    speciescategory.sizecategory = sizecategory.topiaid AND
    speciescategory.sexcategory = sexcategory.topiaid AND
    sampletype.name = 'Subsample'

dans cette requête, il faut en plus :
- enlever le '_' dans le baracoudacode
- créer un champ Annee avec l'année du voyage
- mettre en colonne les champs LTcm1 et WeightAtLengthkg
- faire une somme des datavalues des 'LTcm1 et WeightAtLengthkg 
respectivement en groupant sur les champs Campagne,trait, Espece 
et "Annee", mais j'y arrive pas...
- ajouter une colonne "maturite" avec des NA
- convertir les "N" de la colonne "sexe" en NA
     */

    public RawDataSizeExportModel(char separator) {
        super(separator);
        newColumnForExport("Campagne", EchoBaseCsvUtil.<RawDataSizeExportRow, String>newBeanProperty(RawDataSizeExportRow.PROPERTY_VOYAGE_NAME));
        newColumnForExport("Annee", EchoBaseCsvUtil.<RawDataSizeExportRow, Date>newBeanProperty(RawDataSizeExportRow.PROPERTY_VOYAGE_YEAR), EchoBaseCsvUtil.YEAR);
        newColumnForExport("Trait", EchoBaseCsvUtil.<RawDataSizeExportRow, String>newBeanProperty(RawDataSizeExportRow.PROPERTY_OPERATION_ID));
        newColumnForExport("Espece", RawDataSizeExportRow.PROPERTY_SPECIES, EchoBaseCsvUtil.SPECIES_TO_COSER_CODE);
        newColumnForExport("Sexe", RawDataSizeExportRow.PROPERTY_SEX_CATEGORY, new ValueFormatter<SexCategory>() {
            @Override
            public String format(SexCategory value) {
                String result;
                if (value == null || "N".equals(value.getName())) {
                    result = EchoBaseCsvUtil.NA;
                } else {
                    result = value.getName();
                }
                return result;
            }
        });
        newColumnForExport("Maturite", RawDataSizeExportRow.PROPERTY_MATURITE);
        newColumnForExport("Longueur", RawDataSizeExportRow.PROPERTY_LENGTH_STEP);
        newColumnForExport("Nombre", RawDataSizeExportRow.PROPERTY_NUMBER, EchoBaseCsvUtil.NA_TO_INTEGER_PARSER_FORMATTER);
        newColumnForExport("Poids", RawDataSizeExportRow.PROPERTY_WEIGHT, EchoBaseCsvUtil.NA_TO_FLOAT_PARSER_FORMATTER);
        newColumnForExport("Age", RawDataSizeExportRow.PROPERTY_AGE, EchoBaseCsvUtil.NA_TO_INTEGER_PARSER_FORMATTER);
    }

    public List<RawDataSizeExportRow> prepareRows(List<Voyage> voyages,
                                                  SampleDataType weightSampleDataType,
                                                  SampleDataType lenghtSampleDataType) {
        List<RawDataSizeExportRow> rows = Lists.newArrayList();
        for (Voyage voyage : voyages) {
            prepareRows(voyage, rows, weightSampleDataType, lenghtSampleDataType);
        }
        return rows;
    }

    protected void prepareRows(Voyage voyage,
                               List<RawDataSizeExportRow> rows,
                               SampleDataType weightSampleDataType,
                               SampleDataType lenghtSampleDataType) {
        if (voyage.isTransitNotEmpty()) {
            for (Transit transit : voyage.getTransit()) {

                if (transit.isTransectNotEmpty()) {
                    for (Transect transect : transit.getTransect()) {

                        if (transect.isOperationNotEmpty()) {
                            for (Operation operation : transect.getOperation()) {

                                if (operation.isSampleNotEmpty()) {

                                    // compute sum of number and weight per species and lengthStep
                                    MultiKeyMap<Object, LengthStepContext> map = new MultiKeyMap<>();

                                    for (Sample sample : operation.getSample()) {

                                        SpeciesCategory speciesCategory = sample.getSpeciesCategory();
                                        if (speciesCategory != null) {
                                            Species species = speciesCategory.getSpecies();
                                            SexCategory sexCategory = speciesCategory.getSexCategory();

                                            if (sample.isSampleDataNotEmpty()) {
                                                for (SampleData sampleData : sample.getSampleData()) {
                                                    if (weightSampleDataType.equals(sampleData.getSampleDataType())) {

                                                        // register a weight
                                                        addWeight(species, sexCategory, map, sampleData);
                                                    } else if (lenghtSampleDataType.equals(sampleData.getSampleDataType())) {
                                                        // register a number
                                                        addNumber(species, sexCategory, map, sampleData);
                                                    }
                                                }
                                            }
                                        }
                                    }

                                    // create rows
                                    for (Map.Entry<MultiKey<?>, LengthStepContext> entry : map.entrySet()) {
                                        MultiKey<?> key = entry.getKey();
                                        Species species = (Species) key.getKey(0);
                                        String lengthStep = (String) key.getKey(1);

                                        LengthStepContext numberWeight = entry.getValue();

                                        RawDataSizeExportRow row = new RawDataSizeExportRow();
                                        row.setVoyage(voyage);
                                        row.setOperation(operation);
                                        row.setSpecies(species);
                                        row.setSexCategory(numberWeight.sexCategory);
                                        row.setMaturite(EchoBaseCsvUtil.NA);
                                        row.setLengthStep(lengthStep);
                                        row.setNumber((int) numberWeight.number);
                                        row.setWeight(numberWeight.weight);
                                        rows.add(row);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    protected void addWeight(Species species, SexCategory sexCategory, MultiKeyMap<Object, LengthStepContext> map, SampleData sampleData) {
        String lengthStep = sampleData.getDataLabel();
        LengthStepContext lengthStepContext = map.get(species, lengthStep);
        if (lengthStepContext == null) {
            lengthStepContext = new LengthStepContext(sexCategory);
            map.put(species, lengthStep, lengthStepContext);
        }
        Float weight = sampleData.getDataValue();
        lengthStepContext.addWeight(weight);
    }

    protected void addNumber(Species species, SexCategory sexCategory, MultiKeyMap<Object, LengthStepContext> map, SampleData sampleData) {
        String lengthStep = sampleData.getDataLabel();
        LengthStepContext lengthStepContext = map.get(species, lengthStep);
        if (lengthStepContext == null) {
            lengthStepContext = new LengthStepContext(sexCategory);
            map.put(species, lengthStep, lengthStepContext);
        }
        Float number = sampleData.getDataValue();
        lengthStepContext.addNumber(number);
    }

    static class LengthStepContext {

        protected final SexCategory sexCategory;

        protected float number;

        protected float weight;

        LengthStepContext(SexCategory sexCategory) {
            this.sexCategory = sexCategory;
        }

        void addNumber(float number) {
            this.number += number;
        }

        void addWeight(float weight) {
            this.weight += weight;
        }
    }
}