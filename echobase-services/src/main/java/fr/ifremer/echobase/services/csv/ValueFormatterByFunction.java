package fr.ifremer.echobase.services.csv;

/*
 * #%L
 * EchoBase :: Services
 * %%
 * Copyright (C) 2011 - 2016 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.base.Function;
import org.nuiton.csv.ValueFormatter;

/**
 * Created on 30/03/16.
 *
 * @author Tony Chemit - chemit@codelutin.com
 */
class ValueFormatterByFunction<E> implements ValueFormatter<E> {

    private final Function<E, String> function;

    protected ValueFormatterByFunction(Function<E, String> function) {
        this.function = function;
    }

    @Override
    public String format(E value) {
        return function.apply(value);
    }

}
