/*
 * #%L
 * EchoBase :: Services
 * %%
 * Copyright (C) 2011 - 2012 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ifremer.echobase.services.service.importdata.configurations;

import fr.ifremer.echobase.io.InputFile;

import java.util.Locale;

import static org.nuiton.i18n.I18n.l;

/**
 * Configuration of a "results" import for mooring.
 *
 * @author Julien Ruchaud - ruchaud@codelutin.com
 * @since 4.0
 */
public class MooringResultsImportConfiguration extends ImportResultsConfigurationSupport {

    private static final long serialVersionUID = 1L;
    /** Acoustic result by echotype import. */
    protected final InputFile esduByEchotypeFile;
    /** Acoustic result by echotype and species category import. */
    protected final InputFile esduByEchotypeAndSpeciesCategoryFile;
    /** Echotype  file to import. */
    protected final InputFile echotypeFile;
    /** Selected mooring id where to import datas. */
    protected String mooringId;

    public MooringResultsImportConfiguration(Locale locale) {
        echotypeFile = InputFile.newFile(l(locale, "echobase.common.echotypeFile"));
        esduByEchotypeFile = InputFile.newFile(l(locale, "echobase.common.esduByEchotypeFile"));
        esduByEchotypeAndSpeciesCategoryFile = InputFile.newFile(l(locale, "echobase.common.esduByEchotypeAndSpeciesCategoryFile"));
    }

    public String getMooringId() {
        return mooringId;
    }

    public void setMooringId(String mooringId) {
        this.mooringId = mooringId;
    }
    public InputFile getEchotypeFile() {
        return echotypeFile;
    }

    public InputFile getEsduByEchotypeFile() {
        return esduByEchotypeFile;
    }

    public InputFile getEsduByEchotypeAndSpeciesCategoryFile() {
        return esduByEchotypeAndSpeciesCategoryFile;
    }

    public boolean isOneEsduImportFile() {
        return esduByEchotypeFile.hasFile()
                || esduByEchotypeAndSpeciesCategoryFile.hasFile();

    }

    public boolean isOneMooringImportFile() {
        return echotypeFile.hasFile();

    }

    @Override
    public InputFile[] getInputFiles() {
        return new InputFile[]{
            echotypeFile,
            esduByEchotypeFile,
            esduByEchotypeAndSpeciesCategoryFile,
        };
    }
}
