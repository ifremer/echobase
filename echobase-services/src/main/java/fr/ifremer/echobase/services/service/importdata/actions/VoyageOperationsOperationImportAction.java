package fr.ifremer.echobase.services.service.importdata.actions;

/*
 * #%L
 * EchoBase :: Services
 * %%
 * Copyright (C) 2011 - 2016 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.echobase.entities.EchoBaseUserEntityEnum;
import fr.ifremer.echobase.entities.data.Operation;
import fr.ifremer.echobase.entities.data.Transect;
import fr.ifremer.echobase.entities.data.Transit;
import fr.ifremer.echobase.entities.data.Voyage;
import fr.ifremer.echobase.entities.references.Vessel;
import fr.ifremer.echobase.io.InputFile;
import fr.ifremer.echobase.services.service.importdata.DuplicatedOperationException;
import fr.ifremer.echobase.services.service.importdata.ImportDataFileResult;
import fr.ifremer.echobase.services.service.importdata.contexts.VoyageOperationsImportDataContext;
import fr.ifremer.echobase.services.service.importdata.csv.VoyageOperationsOperationImportExportModel;
import fr.ifremer.echobase.services.service.importdata.csv.VoyageOperationsOperationImportRow;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.csv.Import;

import java.util.Date;

/**
 * Created on 25/03/16.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 4.0
 */
public class VoyageOperationsOperationImportAction extends VoyageOperationsImportDataActionSupport<VoyageOperationsOperationImportRow> {

    /** Logger. */
    private static final Log log = LogFactory.getLog(VoyageOperationsOperationImportAction.class);

    public VoyageOperationsOperationImportAction(VoyageOperationsImportDataContext importDataContext) {
        super(importDataContext, importDataContext.getConfiguration().getOperationFile());
    }

    @Override
    protected VoyageOperationsOperationImportExportModel createCsvImportModel(VoyageOperationsImportDataContext importDataContext) {
        return VoyageOperationsOperationImportExportModel.forImport(importDataContext);
    }

    @Override
    protected VoyageOperationsOperationImportExportModel createCsvExportModel(VoyageOperationsImportDataContext importDataContext) {
        return VoyageOperationsOperationImportExportModel.forExport(importDataContext);
    }

    @Override
    protected void performImport(VoyageOperationsImportDataContext importDataContext, InputFile inputFile, ImportDataFileResult result) {

        if (log.isInfoEnabled()) {
            log.info("Starts import of operation from file " + inputFile.getFileName());
        }

        Voyage voyage = importDataContext.getVoyage();

        try (Import<VoyageOperationsOperationImportRow> importer = open()) {

            incrementsProgress();
            int rowNumber = 0;
            for (VoyageOperationsOperationImportRow row : importer) {

                doFlushTransaction(++rowNumber);

                Vessel vessel = row.getVessel();

                Operation operation = row.getOperation();

                boolean exists = persistenceService.containsOperation(voyage, vessel, operation.getDepthStratum(), operation.getId());
                if (exists) {
                    throw new DuplicatedOperationException(getLocale(), rowNumber, voyage.getName(), vessel.getName(), operation.getDepthStratum().getId(), operation.getId());
                }

                Date startTime = operation.getGearShootingStartTime();
                Date endTime = operation.getGearShootingEndTime();

                Transit transit = voyage.getTransit(startTime, endTime);

                Transect transect = transit.getTransect(vessel, operation.getGear().getGearCode());

                Operation createdOperation = persistenceService.createOperation(operation);

                // collect ids
                addId(result, EchoBaseUserEntityEnum.Operation, createdOperation, rowNumber);

                transect.addOperation(createdOperation);

                addProcessedRow(result, row);

            }

        }

    }

    @Override
    protected void computeImportedExport(VoyageOperationsImportDataContext importDataContext, ImportDataFileResult result) {

        for (Operation operation : getImportedEntities(Operation.class, result)) {

            if (log.isInfoEnabled()) {
                log.info("Adding operation: " + operation.getTopiaId() + " to imported export.");
            }

            VoyageOperationsOperationImportRow importedRow = VoyageOperationsOperationImportRow.of(operation);
            addImportedRow(result, importedRow);

        }

    }

}
