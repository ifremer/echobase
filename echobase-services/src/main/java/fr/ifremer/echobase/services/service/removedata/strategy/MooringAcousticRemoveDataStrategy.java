package fr.ifremer.echobase.services.service.removedata.strategy;

/*
 * #%L
 * EchoBase :: Services
 * %%
 * Copyright (C) 2011 - 2012 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.Sets;
import fr.ifremer.echobase.entities.ImportLog;
import fr.ifremer.echobase.entities.ImportType;
import fr.ifremer.echobase.entities.data.Cell;
import fr.ifremer.echobase.entities.data.DataAcquisition;
import fr.ifremer.echobase.entities.data.DataProcessing;
import fr.ifremer.echobase.entities.data.Mooring;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.persistence.TopiaException;

import java.util.Set;
import fr.ifremer.echobase.entities.data.DataAcousticProvider;

/**
 * Remove a {@link ImportType#MOORING_ACOUSTIC} import.
 *
 * Can remove only {@link DataAcquisition} or {@link Cell}.
 *
 * @author Julien Ruchaud - ruchaud@codelutin.com
 * @since 4.0
 */
public class MooringAcousticRemoveDataStrategy extends AbstractRemoveDataStrategy<Mooring> {

    /** Logger. */
    private static final Log log = LogFactory.getLog(MooringAcousticRemoveDataStrategy.class);

    @Override
    public long computeNbSteps(DataAcousticProvider<Mooring> provider, ImportLog importLog) {
        Mooring mooring = provider.getEntity();

        long result = getImportFileIdsCount(importLog);

        // add all cell results
        result += persistenceService.countMooringCellResults(mooring);

        // add all orphan cells
        result += persistenceService.countMooringOrphanCells(mooring);
        
        return result;
    }

    @Override
    protected void removePreData(DataAcousticProvider<Mooring> provider) throws TopiaException {
        Mooring mooring = provider.getEntity();

        // remove all cell results
        removeMooringCellResults(mooring);
    }

    @Override
    protected void removePostData(DataAcousticProvider<Mooring> provider) throws TopiaException {
        // remove orphans cells
        removeOrphanCells();
    }

    @Override
    protected void removeImportData(DataAcousticProvider<Mooring> provider, String id) throws TopiaException {

        if (id.startsWith(DataAcquisition.class.getName())) {

            // get dataAcquisition
            DataAcquisition dataAcquisition = persistenceService.getDataAcquisition(id);

            // remove it from cell
            Mooring mooring = provider.getEntity();
            mooring.removeDataAcquisition(dataAcquisition);

            // remove dataAcquisition
            persistenceService.deleteDataAcquisition(dataAcquisition);
            if (log.isDebugEnabled()) {
                log.debug(dataAcquisition.getTopiaId() + " was removed");
            }
        } else if (id.startsWith(Cell.class.getName())) {

            // get cell
            Cell cell = persistenceService.getCell(id);

            if (cell == null) {
                throw new IllegalStateException("Could not find cell " + id);
            }
            // remove it from the dataAcquisition
            DataProcessing dataProcessing = persistenceService.getDataProcessingContainsCell(cell);
            if (dataProcessing != null) {
                dataProcessing.removeCell(cell);
            }

            // remove cell
            persistenceService.deleteCell(cell);
            if (log.isDebugEnabled()) {
                log.debug(cell.getTopiaId() + " was removed");
            }
        } else {
            canNotDealWithId(id);
        }
    }

    @Override
    public Set<ImportType> getPossibleSubImportType() {
        return Sets.newHashSet(ImportType.RESULT_MOORING,
                               ImportType.RESULT_MOORING_ESDU);
    }
}
