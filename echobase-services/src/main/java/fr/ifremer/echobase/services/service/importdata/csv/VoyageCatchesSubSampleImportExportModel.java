/*
 * #%L
 * EchoBase :: Services
 * %%
 * Copyright (C) 2011 - 2012 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ifremer.echobase.services.service.importdata.csv;

import fr.ifremer.echobase.entities.data.Operation;
import fr.ifremer.echobase.entities.data.Sample;
import fr.ifremer.echobase.entities.data.SampleData;
import fr.ifremer.echobase.entities.references.SexCategory;
import fr.ifremer.echobase.entities.references.SizeCategory;
import fr.ifremer.echobase.entities.references.Species;
import fr.ifremer.echobase.services.csv.EchoBaseCsvUtil;
import fr.ifremer.echobase.services.service.importdata.contexts.VoyageCatchesImportDataContext;

/**
 * Model to import {@link SampleData}.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 0.3
 */
public class VoyageCatchesSubSampleImportExportModel extends EchoBaseImportExportModelSupport<VoyageCatchesSubSampleImportRow> {

    private VoyageCatchesSubSampleImportExportModel(char separator) {
        super(separator);
    }

    public static VoyageCatchesSubSampleImportExportModel forImport(VoyageCatchesImportDataContext importDataContext) {

        VoyageCatchesSubSampleImportExportModel model = new VoyageCatchesSubSampleImportExportModel(importDataContext.getCsvSeparator());
        model.newIgnoredColumn("subHaul");
        model.newForeignKeyColumn(EchoBaseCsvUtil.OPERATION_ID, VoyageCatchesSubSampleImportRow.PROPERTY_OPERATION, Operation.class, Operation.PROPERTY_ID, importDataContext.getVoyageOperationsById());
        model.newForeignKeyColumn(Species.PROPERTY_BARACOUDA_CODE, VoyageCatchesSubSampleImportRow.PROPERTY_SPECIES, Species.class, Species.PROPERTY_BARACOUDA_CODE, importDataContext.getSpeciesByBaracoudaCode());
        model.newForeignKeyColumn(VoyageCatchesSubSampleImportRow.PROPERTY_SIZE_CATEGORY, SizeCategory.class, SizeCategory.PROPERTY_NAME, importDataContext.getSizeCategoriesByName());
        model.newForeignKeyColumn(VoyageCatchesSubSampleImportRow.PROPERTY_SEX_CATEGORY, SexCategory.class, SexCategory.PROPERTY_NAME, importDataContext.getSexCategoriesByName());
        model.newMandatoryColumn(VoyageCatchesSubSampleImportRow.PROPERTY_LENGTH_CLASS);
        model.newMandatoryColumn(Sample.PROPERTY_SAMPLE_WEIGHT, EchoBaseCsvUtil.PRIMITIVE_FLOAT);
        model.newMandatoryColumn(Sample.PROPERTY_NUMBER_SAMPLED, EchoBaseCsvUtil.PRIMITIVE_INTEGER);
        model.newMandatoryColumn(VoyageCatchesSubSampleImportRow.PROPERTY_NUMBER_AT_LENGTH, EchoBaseCsvUtil.PRIMITIVE_FLOAT);
        model.newMandatoryColumn(VoyageCatchesSubSampleImportRow.PROPERTY_WEIGHT_AT_LENGTH, EchoBaseCsvUtil.NA_TO_FLOAT_PARSER_FORMATTER);
        model.newIgnoredColumn("round");
        return model;

    }

    public static VoyageCatchesSubSampleImportExportModel forExport(VoyageCatchesImportDataContext importDataContext) {

        VoyageCatchesSubSampleImportExportModel model = new VoyageCatchesSubSampleImportExportModel(importDataContext.getCsvSeparator());
        model.newColumnForExport(EchoBaseCsvUtil.OPERATION_ID, VoyageCatchesTotalSampleImportRow.PROPERTY_OPERATION, EchoBaseCsvUtil.OPERATION_FORMATTER);
        model.newColumnForExport(Species.PROPERTY_BARACOUDA_CODE, VoyageCatchesTotalSampleImportRow.PROPERTY_SPECIES, EchoBaseCsvUtil.SPECIES_FORMATTER);
        model.newColumnForExport(VoyageCatchesSubSampleImportRow.PROPERTY_SIZE_CATEGORY, EchoBaseCsvUtil.SIZE_CATEGORY_FORMATTER);
        model.newColumnForExport(VoyageCatchesSubSampleImportRow.PROPERTY_SEX_CATEGORY, EchoBaseCsvUtil.SEX_CATEGORY_FORMATTER);
        model.newColumnForExport(VoyageCatchesSubSampleImportRow.PROPERTY_LENGTH_CLASS);
        model.newColumnForExport(Sample.PROPERTY_SAMPLE_WEIGHT, EchoBaseCsvUtil.PRIMITIVE_FLOAT);
        model.newColumnForExport(Sample.PROPERTY_NUMBER_SAMPLED, EchoBaseCsvUtil.PRIMITIVE_INTEGER);
        model.newColumnForExport(VoyageCatchesSubSampleImportRow.PROPERTY_NUMBER_AT_LENGTH, EchoBaseCsvUtil.PRIMITIVE_FLOAT);
        model.newColumnForExport(VoyageCatchesSubSampleImportRow.PROPERTY_WEIGHT_AT_LENGTH, EchoBaseCsvUtil.NA_TO_FLOAT_PARSER_FORMATTER);
        //model.newColumnForExport(VoyageCatchesSubSampleImportRow.PROPERTY_ROUND, EchoBaseCsvUtil.NA_TO_INTEGER_PARSER_FORMATTER);
        return model;

    }

    @Override
    public VoyageCatchesSubSampleImportRow newEmptyInstance() {
        return new VoyageCatchesSubSampleImportRow();
    }
}
