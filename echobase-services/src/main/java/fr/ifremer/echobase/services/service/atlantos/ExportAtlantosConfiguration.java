package fr.ifremer.echobase.services.service.atlantos;

/*
 * #%L
 * EchoBase :: Services
 * %%
 * Copyright (C) 2011 - 2014 Ifremer, Codelutin, Chemit Tony
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.echobase.services.AbstractEchobaseActionConfiguration;
import fr.ifremer.echobase.services.ProgressModel;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;

/**
 * @author Julien Ruchaud - ruchaud@codelutin.com
 * @since 4
 */
public class ExportAtlantosConfiguration extends AbstractEchobaseActionConfiguration {

    private static final long serialVersionUID = 1L;

    /**
     * Working directory.
     */
    private File workingDirectory;

    /**
     * Final export file.
     */
    private File exportFile;

    /**
     * Id of voyage to export.
     */
    private String voyageId;

    /**
     * Id of vessel to export.
     */
    private String vesselId;

    public ExportAtlantosConfiguration() {
    }

    public ExportAtlantosConfiguration(ProgressModel progressModel) {
        super(progressModel);
    }

    public File getWorkingDirectory() {
        return workingDirectory;
    }

    public void setWorkingDirectory(File workingDirectory) {
        this.workingDirectory = workingDirectory;
    }

    public File getExportFile() {
        return exportFile;
    }

    public void setExportFile(File exportFile) {
        this.exportFile = exportFile;
    }

    public String getVoyageId() {
        return voyageId;
    }

    public void setVoyageId(String voyageId) {
        this.voyageId = voyageId;
    }

    public String getVesselId() {
        return vesselId;
    }

    public void setVesselId(String vesselId) {
        this.vesselId = vesselId;
    }

    @Override
    public void destroy() throws IOException {
        if (workingDirectory != null) {
            FileUtils.deleteDirectory(workingDirectory);
        }
    }
}
