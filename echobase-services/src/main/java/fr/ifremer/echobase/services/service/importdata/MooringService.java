/*
 * #%L
 * EchoBase :: Services
 * %%
 * Copyright (C) 2011 - 2012 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ifremer.echobase.services.service.importdata;

import com.google.common.base.Preconditions;
import fr.ifremer.echobase.EchoBaseTechnicalException;
import fr.ifremer.echobase.entities.data.Mooring;
import fr.ifremer.echobase.services.EchoBaseServiceSupport;
import fr.ifremer.echobase.services.service.UserDbPersistenceService;
import org.nuiton.topia.persistence.TopiaException;

import javax.inject.Inject;

/**
 * Service to create mooring.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 0.5
 */
public class MooringService extends EchoBaseServiceSupport {

    @Inject
    protected UserDbPersistenceService persistenceService;

    public Mooring createMooring(Mooring mooring) throws MooringCodeAlreadyExistException {

        Preconditions.checkNotNull(mooring);

        try {
            // check mission name is unique
            boolean exists = persistenceService.isMissionExistByName(mooring.getCode());

            if (exists) {
                throw new MooringCodeAlreadyExistException();
            }
            Mooring result = persistenceService.createMooring(mooring);
            persistenceService.commit();
            return result;
        } catch (TopiaException eee) {
            throw new EchoBaseTechnicalException(eee);
        }
    }
}
