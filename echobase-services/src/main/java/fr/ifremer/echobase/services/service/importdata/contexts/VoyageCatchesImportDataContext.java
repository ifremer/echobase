package fr.ifremer.echobase.services.service.importdata.contexts;

/*
 * #%L
 * EchoBase :: Services
 * %%
 * Copyright (C) 2011 - 2016 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.echobase.entities.EchoBaseUser;
import fr.ifremer.echobase.entities.references.SampleDataType;
import fr.ifremer.echobase.entities.references.SampleDataTypeImpl;
import fr.ifremer.echobase.entities.references.SampleType;
import fr.ifremer.echobase.entities.references.SampleTypeImpl;
import fr.ifremer.echobase.services.service.UserDbPersistenceService;
import fr.ifremer.echobase.services.service.importdata.configurations.VoyageCatchesImportConfiguration;

import java.util.Date;
import java.util.Locale;

/**
 * Created on 30/03/16.
 *
 * @author Tony Chemit - chemit@codelutin.com
 */
public class VoyageCatchesImportDataContext extends VoyageImportDataContextSupport<VoyageCatchesImportConfiguration> {

    private SampleType sampleTypeIndividualType;
    private SampleType sampleTypeTotal;
    private SampleType sampleTypeUnsorted;
    private SampleType sampleTypeSorted;
    private SampleType sampleTypeSubsample;
    private SampleDataType sampleDataTypeMeanLength;
    private SampleDataType sampleDataTypeMeanWeight;
    private SampleDataType sampleDataTypeNoPerKg;
    private SampleDataType sampleDataTypeFishIndex;
    private SampleDataType sampleDataTypeNumberAtLength;
    private SampleDataType sampleDataTypeWeightAtLength;

    public VoyageCatchesImportDataContext(UserDbPersistenceService persistenceService, Locale locale, char csvSeparator, VoyageCatchesImportConfiguration configuration, EchoBaseUser user, Date importDate) {
        super(persistenceService, locale, csvSeparator, configuration, user, importDate);
    }

    public final SampleType getSampleTypeSubsample() {
        if (sampleTypeSubsample == null) {
            sampleTypeSubsample = persistenceService.getSampleTypeByName(SampleTypeImpl.SUB_SAMPLE_TYPE);
        }
        return sampleTypeSubsample;
    }

    public final SampleDataType getSampleDataTypeNumberAtLength() {
        if (sampleDataTypeNumberAtLength == null) {
            sampleDataTypeNumberAtLength = persistenceService.getSampleDataTypeByName(SampleDataTypeImpl.NUMBER_AT_LENGTH);
        }
        return sampleDataTypeNumberAtLength;
    }

    public final SampleDataType getSampleDataTypeWeightAtLength() {
        if (sampleDataTypeWeightAtLength == null) {
            sampleDataTypeWeightAtLength = persistenceService.getSampleDataTypeByName(SampleDataTypeImpl.WEIGHT_AT_LENGTHKG);
        }
        return sampleDataTypeWeightAtLength;
    }

    public final SampleType getSampleTypeTotal() {
        if (sampleTypeTotal == null) {
            sampleTypeTotal = persistenceService.getSampleTypeByName(SampleTypeImpl.TOTAL_SAMPLE_TYPE);
        }
        return sampleTypeTotal;
    }

    public final SampleType getSampleTypeUnsorted() {
        if (sampleTypeUnsorted == null) {
            sampleTypeUnsorted = persistenceService.getSampleTypeByName(SampleTypeImpl.UNSORTED_SAMPLE_TYPE);
        }
        return sampleTypeUnsorted;
    }

    public final SampleType getSampleTypeSorted() {
        if (sampleTypeSorted == null) {
            sampleTypeSorted = persistenceService.getSampleTypeByName(SampleTypeImpl.SORTED_SAMPLE_TYPE);
        }
        return sampleTypeSorted;
    }

    public final SampleDataType getSampleDataTypeMeanLength() {
        if (sampleDataTypeMeanLength == null) {
            sampleDataTypeMeanLength = persistenceService.getSampleDataTypeByName(SampleDataTypeImpl.MEAN_LENGTHCM);
        }
        return sampleDataTypeMeanLength;
    }

    public final SampleDataType getSampleDataTypeMeanWeight() {
        if (sampleDataTypeMeanWeight == null) {
            sampleDataTypeMeanWeight = persistenceService.getSampleDataTypeByName(SampleDataTypeImpl.MEAN_WEIGHTG);
        }
        return sampleDataTypeMeanWeight;
    }

    public final SampleDataType getSampleDataTypeNoPerKg() {
        if (sampleDataTypeNoPerKg == null) {
            sampleDataTypeNoPerKg = persistenceService.getSampleDataTypeByName(SampleDataTypeImpl.NO_PER_KG);
        }
        return sampleDataTypeNoPerKg;
    }

    public final SampleDataType getSampleDataTypeSpecimenIndex() {
        if (sampleDataTypeFishIndex == null) {
            sampleDataTypeFishIndex = persistenceService.getSampleDataTypeByName(SampleDataTypeImpl.SPECIMEN_INDEX);
        }
        return sampleDataTypeFishIndex;
    }

    public final SampleType getSampleTypeIndividualType() {
        if (sampleTypeIndividualType == null) {
            sampleTypeIndividualType = persistenceService.getSampleTypeByName(SampleTypeImpl.INDIVIDUAL_SAMPLE_TYPE);
        }
        return sampleTypeIndividualType;
    }

}
