package fr.ifremer.echobase.services.service.spatial;

/*
 * #%L
 * EchoBase :: Services
 * %%
 * Copyright (C) 2011 - 2013 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.echobase.EchoBaseTechnicalException;
import fr.ifremer.echobase.entities.EchoBaseUserPersistenceContext;
import fr.ifremer.echobase.io.EchoBaseIOUtil;
import fr.ifremer.echobase.services.EchoBaseServiceSupport;
import fr.ifremer.echobase.services.service.UserDbPersistenceService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.util.TimeLog;

import javax.inject.Inject;

/**
 * Spatial data service : to manage spatial data of a user database.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.2
 */
public class SpatialDataService extends EchoBaseServiceSupport {

    /** Logger. */
    private static final Log log = LogFactory.getLog(SpatialDataService.class);

    public static final TimeLog TILE_LOG = new TimeLog(SpatialDataService.class);

    public static final String POSTGIS_STRUCTURE_SQL = "/postgis-structure.sql";

    public static final String POSTGIS_VIEW_SQL = "/postgis-view.sql";

    @Inject
    private UserDbPersistenceService persistenceService;

    @Inject
    private EchoBaseUserPersistenceContext persistenceContext;

    public void addSpatialSupport() {

        // add spatial structure
        executeSqlScript(POSTGIS_STRUCTURE_SQL);

        // add spatial views
        executeSqlScript(POSTGIS_VIEW_SQL);

        // do commit
        persistenceService.commit();

        persistenceService.setSpatialStructureFound();

    }

    /**
     * @return {@code true} if there is some postgis data to compute.
     * @since 2.8
     */
    public boolean isSpatialDataToComputeExist() {

        boolean result = false;
        if (persistenceContext.isSpatialStructureFound()) {

            long nbCellTemp = persistenceService.countTable("echobase_cell_spatial_temp");
            long nbOperationTemp = persistenceService.countTable("echobase_operation_spatial_temp");
            result = nbCellTemp + nbOperationTemp > 0;
        }

        return result;

    }

    /**
     * To update the {@code echobase_cell_spatial} table from the filled
     * table {@code echobase_cell_spatial_temp}.
     *
     * To update the {@code echobase_operation_spatial} table from the filled
     * table {@code echobase_operation_spatial_temp}.
     *
     * To rebuild the gis views.
     *
     * @since 2.8
     */
    public void computespatialData() {

        if (persistenceContext.isSpatialStructureFound()) {

            // try the update only for postgresql
            try {
                long time = TimeLog.getTime();
                if (log.isInfoEnabled()) {
                    log.info("Will try to compute operation spatial data from temp table...");
                }
                persistenceService.executeSQL("SELECT echobase_fill_operation_spatial_table();");
                TILE_LOG.log(time, "computespatialData::echobase_fill_operation_spatial_table");

                time = TimeLog.getTime();
                if (log.isInfoEnabled()) {
                    log.info("Will try to compute cell spatial data from temp table...");
                }
                persistenceService.executeSQL("SELECT echobase_fill_cell_spatial_table();");
                TILE_LOG.log(time, "computespatialData::echobase_fill_cell_spatial_table");

                time = TimeLog.getTime();
                if (log.isInfoEnabled()) {
                    log.info("Will try to refresh all spatial views...");
                }
                persistenceService.executeSQL("SELECT echobase_refresh_views();");
                TILE_LOG.log(time, "computespatialData::echobase_refresh_views");
                persistenceService.commit();
            } catch (Exception e) {
                throw new EchoBaseTechnicalException("Could not compute spatial data", e);
            }
        }

    }

    protected void executeSqlScript(String scriptPath) {

        // get sql script
        String sql = EchoBaseIOUtil.loadScript(scriptPath);

        if (log.isInfoEnabled()) {
            log.info("Will execute sql file " + scriptPath);
        }

        try {
            persistenceService.executeSQL(sql);
        } catch (Exception e) {
            throw new EchoBaseTechnicalException(
                    "Could not execute sql file " + scriptPath, e);
        }
    }


}
