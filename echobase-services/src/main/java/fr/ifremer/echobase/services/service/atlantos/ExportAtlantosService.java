package fr.ifremer.echobase.services.service.atlantos;

/*
 * #%L
 * EchoBase :: Services
 * %%
 * Copyright (C) 2011 - 2014 Ifremer, Codelutin, Chemit Tony
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.base.Preconditions;
import fr.ifremer.echobase.entities.data.Voyage;
import fr.ifremer.echobase.entities.references.Vessel;
import fr.ifremer.echobase.io.EchoBaseIOUtil;
import fr.ifremer.echobase.services.EchoBaseServiceSupport;
import fr.ifremer.echobase.services.service.UserDbPersistenceService;
import fr.ifremer.echobase.services.service.atlantos.xml.XmlAccousticExport;
import fr.ifremer.echobase.services.service.atlantos.xml.XmlBioticExport;
import fr.ifremer.echobase.services.service.atlantos.xml.XmlWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.inject.Inject;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;

/**
 * @author Julien Ruchaud - ruchaud@codelutin.com
 * @since 4
 */
public class ExportAtlantosService extends EchoBaseServiceSupport {

    /** Logger. */
    private static final Log log = LogFactory.getLog(ExportAtlantosService.class);

    @Inject
    private UserDbPersistenceService persistenceService;

    @Inject
    private XmlAccousticExport xmlAccousticExport;
    
    @Inject
    private XmlBioticExport xmlBioticExport;

    public void doXmlExport(ExportAtlantosConfiguration model) throws IOException {

        Preconditions.checkNotNull(model);
        Preconditions.checkNotNull(model.getVoyageId());

        int nbSteps = 3;
        model.setNbSteps(nbSteps);

        Voyage voyage = persistenceService.getVoyage(model.getVoyageId());
        Preconditions.checkNotNull(voyage);
        
        Vessel vessel = persistenceService.getVessel(model.getVesselId());
        Preconditions.checkNotNull(vessel);
        
        File tempDirectory = model.getWorkingDirectory();
        String basePath = tempDirectory.getAbsolutePath();

        String name = voyage.getName();
        String vesselCode = vessel.getCode();
        SimpleDateFormat formatter = new SimpleDateFormat("YYYY");
        String year = formatter.format(voyage.getStartDate());
        
        // Export Accoustic
        Path outputAccousticHead = Paths.get(basePath, "Acoustic_" + year + name + ".xml");
        FileWriter fileAccousticHead = new FileWriter(outputAccousticHead.toFile());
        XmlWriter xmlAccousticHead = new XmlWriter(fileAccousticHead);
        
        Path outputAccousticVoca = Paths.get(basePath, "Acoustic_" + year + name + "-voca.xml");
        FileWriter fileAccousticVoca = new FileWriter(outputAccousticVoca.toFile());
        XmlWriter xmlAccousticVoca = new XmlWriter(fileAccousticVoca);
        
        Path outputAccousticCruise = Paths.get(basePath, "Acoustic_" + year + name + "-cruise.xml");
        FileWriter fileAccousticCruise = new FileWriter(outputAccousticCruise.toFile());
        XmlWriter xmlAccousticCruise = new XmlWriter(fileAccousticCruise);
        
        xmlAccousticExport.doExport(voyage, vessel, xmlAccousticHead, xmlAccousticVoca, xmlAccousticCruise);
        fileAccousticHead.close();
        fileAccousticVoca.close();
        fileAccousticCruise.close();
        
        try (FileChannel outAccoustic = new FileOutputStream(outputAccousticHead.toFile(), true).getChannel();
                FileChannel inAccousticVoca = new FileInputStream(outputAccousticVoca.toFile()).getChannel();
                FileChannel inAccousticCruise = new FileInputStream(outputAccousticCruise.toFile()).getChannel()) {
            outAccoustic.transferFrom(inAccousticVoca, 0, inAccousticVoca.size());
            outAccoustic.transferFrom(inAccousticCruise, 0, inAccousticCruise.size());
        }
        
        outputAccousticHead.toFile().deleteOnExit();
        outputAccousticVoca.toFile().delete();
        outputAccousticCruise.toFile().delete();
        
        model.incrementsProgress();
        
        // Export Biotic
        Path outputBioticVoca = Paths.get(basePath, "Biotic_" + year + name + vesselCode + ".xml");
        FileWriter fileBioticVoca = new FileWriter(outputBioticVoca.toFile());
        XmlWriter xmlBioticVoca = new XmlWriter(fileBioticVoca);
        
        Path outputBioticCruise = Paths.get(basePath, "Biotic_" + year + name + vesselCode + "-cruise.xml");
        FileWriter fileBioticCruise = new FileWriter(outputBioticCruise.toFile());
        XmlWriter xmlBioticCruise = new XmlWriter(fileBioticCruise);
        
        xmlBioticExport.doExport(voyage, vessel, xmlBioticVoca, xmlBioticCruise);
        fileBioticVoca.close();
        fileBioticCruise.close();
        
        try (FileChannel outBiotic = new FileOutputStream(outputBioticVoca.toFile(), true).getChannel();
                FileChannel inBioticCruise = new FileInputStream(outputBioticCruise.toFile()).getChannel()) {
            outBiotic.transferFrom(inBioticCruise, 0, inBioticCruise.size());
        }
        
        outputBioticVoca.toFile().deleteOnExit();
        outputBioticCruise.toFile().delete();
        
        model.incrementsProgress();
        
        // Create Zip file
        Path zipPath = Paths.get(basePath, "ICES_" + year + name + vesselCode + ".zip");
        File zipFile = zipPath.toFile();

        EchoBaseIOUtil.compressZipFile(zipFile, tempDirectory, false);
//        zipFile.deleteOnExit();
        
        model.setExportFile(zipFile);
        model.incrementsProgress();
    }

}
