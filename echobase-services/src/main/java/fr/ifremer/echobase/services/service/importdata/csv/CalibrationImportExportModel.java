/*
 * #%L
 * EchoBase :: Services
 * %%
 * Copyright (C) 2011 - 2012 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ifremer.echobase.services.service.importdata.csv;

import fr.ifremer.echobase.entities.references.AcousticInstrument;
import fr.ifremer.echobase.services.csv.EchoBaseCsvUtil;
import fr.ifremer.echobase.services.service.importdata.contexts.ImportDataContextSupport;

/**
 * To import calibration datas ({@link fr.ifremer.echobase.entities.references.Calibration}).
 *
 * @author Jean Couteau - couteau@codelutin.com
 * @since 4.2
 */
public class CalibrationImportExportModel extends EchoBaseImportExportModelSupport<CalibrationImportRow> {

    private CalibrationImportExportModel(char separator) {
        super(separator);
    }

    public static CalibrationImportExportModel forImport(ImportDataContextSupport importDataContext) {

        CalibrationImportExportModel model = new CalibrationImportExportModel(importDataContext.getCsvSeparator());

        model.newIgnoredColumn("topiaId");//A
        model.newMandatoryColumn("accuracyEstimate", CalibrationImportRow.PROPERTY_ACCURACY_ESTIMATE, EchoBaseCsvUtil.STRING);//B
        model.newMandatoryColumn("aquisitionMethod", CalibrationImportRow.PROPERTY_ACQUISITION_METHOD, EchoBaseCsvUtil.STRING);//C
        model.newMandatoryColumn("comments", CalibrationImportRow.PROPERTY_COMMENTS, EchoBaseCsvUtil.STRING); //D
        model.newMandatoryColumn("date", CalibrationImportRow.PROPERTY_DATE, EchoBaseCsvUtil.DATE_TIME_VALUE_PARSER);//E
        model.newMandatoryColumn("processingMethod", CalibrationImportRow.PROPERTY_PROCESSING_METHOD, EchoBaseCsvUtil.STRING);//F
        model.newMandatoryColumn("report", CalibrationImportRow.PROPERTY_REPORT, EchoBaseCsvUtil.STRING);//G
        model.newForeignKeyColumn("acousticinstrument", CalibrationImportRow.PROPERTY_ACOUSTIC_INSTRUMENT, AcousticInstrument.class, AcousticInstrument.PROPERTY_ID, importDataContext.getInstrumentsById());//H

        return model;
    }

    public static CalibrationImportExportModel forExport(ImportDataContextSupport importDataContext) {

        CalibrationImportExportModel model = new CalibrationImportExportModel(importDataContext.getCsvSeparator());

        //model.newIgnoredColumn("topiaId");//A
        model.newColumnForExport("accuracyEstimate", CalibrationImportRow.PROPERTY_ACCURACY_ESTIMATE, EchoBaseCsvUtil.STRING);//B
        model.newColumnForExport("aquisitionMethod", CalibrationImportRow.PROPERTY_ACQUISITION_METHOD, EchoBaseCsvUtil.STRING);//C
        model.newColumnForExport("comments", CalibrationImportRow.PROPERTY_COMMENTS, EchoBaseCsvUtil.STRING); //D
        model.newColumnForExport("date", CalibrationImportRow.PROPERTY_DATE, EchoBaseCsvUtil.ISO8611_DATETIME_FORMATTER);//E
        model.newColumnForExport("processingMethod", CalibrationImportRow.PROPERTY_PROCESSING_METHOD, EchoBaseCsvUtil.STRING);//F
        model.newColumnForExport("report", CalibrationImportRow.PROPERTY_REPORT, EchoBaseCsvUtil.STRING);//G
        model.newForeignKeyColumn("acousticinstrument", CalibrationImportRow.PROPERTY_ACOUSTIC_INSTRUMENT, AcousticInstrument.class, AcousticInstrument.PROPERTY_ID, importDataContext.getInstrumentsById());//H

        return model;
    }

    @Override
    public CalibrationImportRow newEmptyInstance() {
        return new CalibrationImportRow();
    }
}
