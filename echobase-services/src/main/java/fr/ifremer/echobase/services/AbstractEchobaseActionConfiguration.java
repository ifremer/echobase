/*
 * #%L
 * EchoBase :: Services
 * %%
 * Copyright (C) 2011 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ifremer.echobase.services;

import org.nuiton.util.StringUtil;

import java.io.IOException;
import java.io.Serializable;

/**
 * Abstract long action configuration.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 0.2
 */
public abstract class AbstractEchobaseActionConfiguration implements Serializable, ProgressModel {

    private static final long serialVersionUID = 1L;

    private final ProgressModel progressModel;

    private Exception error;

    private String resultMessage;

    protected AbstractEchobaseActionConfiguration() {
        progressModel = new DefaultProgressModel();
    }

    protected AbstractEchobaseActionConfiguration(ProgressModel progressModel) {
        this.progressModel = progressModel;
    }

    @Override
    public final long getNbSteps() {
        return progressModel.getNbSteps();
    }

    @Override
    public final float getProgress() {
        return progressModel.getProgress();
    }

    @Override
    public final void setProgress(float progress) {
        progressModel.setProgress(progress);
    }

    @Override
    public final void incrementsProgress() {
        progressModel.incrementsProgress();
    }

    @Override
    public final void setNbSteps(long nbSteps) {
        progressModel.setNbSteps(nbSteps);
    }

    @Override
    public final long getStartTime() {
        return progressModel.getStartTime();
    }

    @Override
    public final void setStartTime(long startTime) {
        progressModel.setStartTime(startTime);
    }

    @Override
    public final long getEndTime() {
        return progressModel.getEndTime();
    }

    @Override
    public final void setEndTime(long endTime) {
        progressModel.setEndTime(endTime);
    }

    public Exception getError() {
        return error;
    }

    public void setError(Exception error) {
        this.error = error;
    }

    public boolean hasError() {
        return error != null;
    }

    public String getResultMessage() {
        return resultMessage;
    }

    public void setResultMessage(String resultMessage) {
        this.resultMessage = resultMessage;
    }

    @Override
    public String getActionTime() {
        return progressModel.getActionTime();
    }

    public void beginAction() {
        setStartTime(System.nanoTime());
    }

    public void endAction() {
        setEndTime(System.nanoTime());
    }

    public void destroy() throws IOException {
    }

    /**
     * Default progression model implementation.
     *
     * @author Tony Chemit - chemit@codelutin.com
     * @since 0.3
     */
    public static class DefaultProgressModel implements ProgressModel {

        private static final long serialVersionUID = 1L;

        private long nbSteps;

        private float stepIncrement;

        private float progression;

        private long startTime;

        private long endTime;

        @Override
        public long getNbSteps() {
            return nbSteps;
        }

        @Override
        public float getProgress() {
            return progression;
        }

        @Override
        public void setProgress(float progress) {
            this.progression = progress;
        }

        @Override
        public long getStartTime() {
            return startTime;
        }

        @Override
        public void setStartTime(long startTime) {
            this.startTime = startTime;
        }

        @Override
        public long getEndTime() {
            return endTime;
        }

        @Override
        public void setEndTime(long endTime) {
            this.endTime = endTime;
        }

        @Override
        public final void incrementsProgress() {
            setProgress(progression + stepIncrement);
        }

        @Override
        public final void setNbSteps(long nbSteps) {

            this.nbSteps = nbSteps;
            this.stepIncrement = 100f / nbSteps;
        }

        @Override
        public String getActionTime() {
            long sTime = getStartTime();
            long eTime = getEndTime();
            return StringUtil.convertTime(eTime - sTime);
        }
    }
}
