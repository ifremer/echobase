package fr.ifremer.echobase.services.service.importdata;

/*
 * #%L
 * EchoBase :: Services
 * %%
 * Copyright (C) 2011 - 2016 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Sets;
import com.google.common.io.Files;
import fr.ifremer.echobase.entities.EchoBaseUserEntityEnum;
import fr.ifremer.echobase.entities.ImportFile;
import fr.ifremer.echobase.entities.ImportLog;
import fr.ifremer.echobase.io.InputFile;
import org.apache.commons.lang3.mutable.MutableInt;
import org.nuiton.topia.persistence.TopiaEntity;

import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

/**
 * Created on 26/03/16.
 *
 * @author Tony Chemit - chemit@codelutin.com
 */
public class ImportDataFileResult {

    /** Entity types to import csv datas. */
    protected final Set<EchoBaseUserEntityEnum> entityTypes;
    /** Count of created entities. */
    protected final Map<EchoBaseUserEntityEnum, MutableInt> numberCreated;
    /** Count of updated entities. */
    protected final Map<EchoBaseUserEntityEnum, MutableInt> numberUpdated;
    /** Count of not imported entities. */
    protected final Map<EchoBaseUserEntityEnum, MutableInt> numberNotImported;
    /** Ids of not imported entities. */
    protected final LinkedHashSet<String> notImportedIds;
    /**
     * Processed Import file (used to compare with result file).
     *
     * This file may not be exactly the same than the one given in the import configuration, we for example may
     * remove ignored columns.
     */
    private final InputFile processedImportFile;
    /**
     * Export file.
     *
     * Used to check that import is really ok and stored then in the {@link ImportLog}.
     */
    private final InputFile importedExportFile;

    /**
     * The persisted import file.
     */
    private final ImportFile importFile;

    /**
     * Count of imported entities
     */
    private int importedEntityCount;

    public ImportDataFileResult(ImportFile importFile) {
        this.importFile = importFile;
        String filenameWithoutExtension = Files.getNameWithoutExtension(importFile.getName());
        {
            String filename = filenameWithoutExtension + ".processed.csv";
            this.processedImportFile = InputFile.newFile(filename);
            this.processedImportFile.setFileName(filename);
        }
        {
            String filename = filenameWithoutExtension + ".checked.csv";
            this.importedExportFile = InputFile.newFile(filename);
            this.importedExportFile.setFileName(filename);
        }
        this.entityTypes = Collections.unmodifiableSet(Sets.newHashSet(EchoBaseUserEntityEnum.values()));
        this.numberCreated = new TreeMap<>();
        this.numberUpdated = new TreeMap<>();
        this.numberNotImported = new TreeMap<>();
        for (EchoBaseUserEntityEnum entityType : entityTypes) {
            numberCreated.put(entityType, new MutableInt());
            numberUpdated.put(entityType, new MutableInt());
            numberNotImported.put(entityType, new MutableInt());
        }
        this.notImportedIds = new LinkedHashSet<>();
    }

    public ImportFile getImportFile() {
        return importFile;
    }

    public InputFile getProcessedImportFile() {
        return processedImportFile;
    }

    public InputFile getImportedExportFile() {
        return importedExportFile;
    }

    public <E extends TopiaEntity> int addId(EchoBaseUserEntityEnum entityEnum) {
        importedEntityCount++;
        incrementsNumberCreated(entityEnum);
        return importedEntityCount;
    }

    public <E extends TopiaEntity> int updateId(EchoBaseUserEntityEnum entityEnum) {
        importedEntityCount++;
        incrementsNumberUpdated(entityEnum);
        return importedEntityCount;
    }
    
    public <E extends TopiaEntity> void addNotImportedId(EchoBaseUserEntityEnum entityEnum, E entity) {
        addNotImportedId(entityEnum, entity.getTopiaId());
    }

    public void addNotImportedId(EchoBaseUserEntityEnum entityEnum, String id) {
        this.notImportedIds.add(id);
        incrementsNumberNotImported(entityEnum);
    }

    public int sizeImportFileIds() {
        return importedEntityCount;
    }

//    public List<ImportFileId> getImportFileIdsForImportFile() {
//        return importFile.getImportFileId();
//    }

    public LinkedHashSet<String> getNotImportedIds() {
        return notImportedIds;
    }

    public Set<EchoBaseUserEntityEnum> getEntityTypes() {
        return ImmutableSet.copyOf(entityTypes);
    }

    public int getNumberCreated(EchoBaseUserEntityEnum entityType) {
        return getInteger(numberCreated, entityType);
    }

    public int getNumberUpdated(EchoBaseUserEntityEnum entityType) {
        return getInteger(numberUpdated, entityType);
    }

    public int getNumberNotImported(EchoBaseUserEntityEnum entityType) {
        return getInteger(numberNotImported, entityType);
    }

    public void incrementsNumberCreated(EchoBaseUserEntityEnum entityType) {
        increments(numberCreated, entityType);
    }

    public void incrementsNumberNotImported(EchoBaseUserEntityEnum entityType) {
        increments(numberNotImported, entityType);
    }

    public void incrementsNumberUpdated(EchoBaseUserEntityEnum entityType) {
        increments(numberUpdated, entityType);
    }

    protected int getInteger(Map<EchoBaseUserEntityEnum, MutableInt> map, EchoBaseUserEntityEnum entityType) {
        return map.get(entityType).intValue();
    }

    protected void increments(Map<EchoBaseUserEntityEnum, MutableInt> map, EchoBaseUserEntityEnum entityType) {
        MutableInt result = map.get(entityType);
        result.increment();
    }
}
