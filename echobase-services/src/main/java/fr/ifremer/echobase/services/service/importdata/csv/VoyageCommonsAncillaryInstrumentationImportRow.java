/*
 * #%L
 * EchoBase :: Services
 * %%
 * Copyright (C) 2011 - 2012 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ifremer.echobase.services.service.importdata.csv;

import fr.ifremer.echobase.entities.data.DataAcousticProvider;
import fr.ifremer.echobase.entities.data.Transect;
import fr.ifremer.echobase.entities.data.Voyage;
import fr.ifremer.echobase.entities.references.AncillaryInstrumentation;
import fr.ifremer.echobase.entities.references.Vessel;

/**
 * @author Julien Ruchaud - ruchaud@codelutin.com
 * @since 4.0
 */
public class VoyageCommonsAncillaryInstrumentationImportRow extends AncillaryInstrumentationImportRow {

    private static final long serialVersionUID = 1L;

    public static final String PROPERTY_VOYAGE = "voyage";
    public static final String PROPERTY_VESSEL = "vessel";

    protected Voyage voyage;
    
    protected Vessel vessel;

    public static VoyageCommonsAncillaryInstrumentationImportRow of(DataAcousticProvider<Transect> provider, AncillaryInstrumentation ancillaryInstrumentation) {
        VoyageCommonsAncillaryInstrumentationImportRow row = new VoyageCommonsAncillaryInstrumentationImportRow(ancillaryInstrumentation);
        Transect transect = provider.getEntity();
        row.voyage = transect.getTransit().getVoyage();
        row.vessel = transect.getVessel();
        return row;
    }

    public VoyageCommonsAncillaryInstrumentationImportRow() {
        super();
    }

    public VoyageCommonsAncillaryInstrumentationImportRow(AncillaryInstrumentation ancillaryInstrumentation) {
        super(ancillaryInstrumentation);
    }
    
    public Voyage getVoyage() {
        return voyage;
    }

    public void setVoyage(Voyage voyage) {
        this.voyage = voyage;
    }

    public Vessel getVessel() {
        return vessel;
    }

    public void setVessel(Vessel vessel) {
        this.vessel = vessel;
    }
}
