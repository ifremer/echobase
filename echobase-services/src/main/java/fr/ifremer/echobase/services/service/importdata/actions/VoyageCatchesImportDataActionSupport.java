package fr.ifremer.echobase.services.service.importdata.actions;

/*
 * #%L
 * EchoBase :: Services
 * %%
 * Copyright (C) 2011 - 2016 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.base.Preconditions;
import fr.ifremer.echobase.entities.EchoBaseUserEntityEnum;
import fr.ifremer.echobase.entities.data.Operation;
import fr.ifremer.echobase.entities.data.Sample;
import fr.ifremer.echobase.entities.data.SampleData;
import fr.ifremer.echobase.entities.references.SampleDataType;
import fr.ifremer.echobase.io.InputFile;
import fr.ifremer.echobase.services.service.importdata.ImportDataFileResult;
import fr.ifremer.echobase.services.service.importdata.configurations.VoyageCatchesImportConfiguration;
import fr.ifremer.echobase.services.service.importdata.contexts.VoyageCatchesImportDataContext;
import org.nuiton.csv.ImportRuntimeException;

import java.util.Collection;

/**
 * Created on 30/03/16.
 *
 * @author Tony Chemit - chemit@codelutin.com
 */
public abstract class VoyageCatchesImportDataActionSupport<E> extends ImportDataActionSupport<VoyageCatchesImportConfiguration, VoyageCatchesImportDataContext, E> {

    protected VoyageCatchesImportDataActionSupport(VoyageCatchesImportDataContext importDataContext, InputFile inputFile) {
        super(importDataContext, inputFile);
    }

    protected Sample addSample(Operation operation, Sample sample, ImportDataFileResult importResult, int rowNumber) {

        Preconditions.checkNotNull(operation);
        Preconditions.checkNotNull(sample);
        Sample result = getPersistenceService().createSample(sample);
        operation.addSample(result);
        addId(importResult, EchoBaseUserEntityEnum.Sample, sample, rowNumber);
        return result;

    }

    protected SampleData addSampleData(SampleDataType sampleDataType, String label, float value, Sample sample, ImportDataFileResult importResult, boolean collectId, int rowNumber) {

        SampleData sampleData = getPersistenceService().createSampleData(sampleDataType, label, value);
        sample.addSampleData(sampleData);
        if (collectId) {
            addId(importResult, EchoBaseUserEntityEnum.SampleData, sampleData, rowNumber);
        } else {
            importResult.incrementsNumberCreated(EchoBaseUserEntityEnum.SampleData);
        }
        return sampleData;

    }

    //FIXME Define a real exception
    protected void checkOperationWithTotalOrUnsortedSample(int rowNumber, Collection<Operation> operationWithTotalOrUnsortedSample, Operation operation) {
        if (!operationWithTotalOrUnsortedSample.contains(operation)) {

            // can not accept this import
            throw new ImportRuntimeException("At line " + rowNumber + ", wants to add a subsample sample but operation " + operation.getId() + " has no sample of type *Total*, nor *Unsorted*");
        }
    }

}
