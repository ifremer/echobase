/*
 * #%L
 * EchoBase :: Services
 * %%
 * Copyright (C) 2011 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ifremer.echobase.services;

import fr.ifremer.echobase.EchoBaseTechnicalException;
import fr.ifremer.echobase.config.EchoBaseConfiguration;
import fr.ifremer.echobase.entities.EchoBaseInternalPersistenceContext;
import fr.ifremer.echobase.entities.EchoBaseUserPersistenceContext;
import fr.ifremer.echobase.entities.EchoBaseUserTopiaApplicationContext;
import fr.ifremer.echobase.persistence.EchoBaseDbMeta;
import org.apache.commons.lang3.time.DateUtils;

import java.lang.reflect.InvocationTargetException;
import java.net.URL;
import java.util.Date;
import java.util.Locale;

/**
 * Instances of this class will be given to service factory.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 0.1
 */
public class DefaultEchoBaseServiceContext implements EchoBaseServiceContext {

    protected final Locale locale;

    protected EchoBaseUserTopiaApplicationContext echoBaseApplicationContext;

    protected EchoBaseUserPersistenceContext echoBaseUserPersistenceContext;

    protected EchoBaseInternalPersistenceContext echoBaseInternalPersistenceContext;

    protected EchoBaseConfiguration configuration;

    protected EchoBaseDbMeta dbMeta;

    protected EchobaseAieOC injector;

    public static EchoBaseServiceContext newContext(
            Locale locale,
            EchoBaseConfiguration configuration,
            EchoBaseDbMeta dbMeta) {
        return new DefaultEchoBaseServiceContext(locale,
                                                 configuration,
                                                 dbMeta);
    }

    protected DefaultEchoBaseServiceContext(Locale locale,
                                            EchoBaseConfiguration configuration,
                                            EchoBaseDbMeta dbMeta) {
        this.locale = locale;
        this.configuration = configuration;
        this.dbMeta = dbMeta;
        this.injector = new ServiceEchobaseAieOC();
    }

    @Override
    public EchoBaseUserTopiaApplicationContext getEchoBaseUserApplicationContext() {
        return echoBaseApplicationContext;
    }

    @Override
    public void setEchoBaseUserApplicationContext(EchoBaseUserTopiaApplicationContext echoBaseApplicationContext) {
        this.echoBaseApplicationContext = echoBaseApplicationContext;
    }

    @Override
    public EchoBaseUserPersistenceContext getEchoBaseUserPersistenceContext() {
        return echoBaseUserPersistenceContext;
    }

    @Override
    public void setEchoBaseUserPersistenceContext(EchoBaseUserPersistenceContext userTopiaPersistenceContext) {
        this.echoBaseUserPersistenceContext = userTopiaPersistenceContext;
    }

    @Override
    public EchoBaseInternalPersistenceContext getEchoBaseInternalPersistenceContext() {
        return echoBaseInternalPersistenceContext;
    }

    @Override
    public void setEchoBaseInternalPersistenceContext(EchoBaseInternalPersistenceContext internalTopiaPersistenceContext) {
        this.echoBaseInternalPersistenceContext = internalTopiaPersistenceContext;
    }

    @Override
    public Locale getLocale() {
        return locale;
    }

    @Override
    public Date newDate() {
        Date result = new Date();
        DateUtils.setMilliseconds(result, 0);
        return result;
    }

    @Override
    public EchoBaseConfiguration getConfiguration() {
        return configuration;
    }

    @Override
    public EchoBaseDbMeta getDbMeta() {
        return dbMeta;
    }

    @Override
    public String getUserDbUrl() {
        return getEchoBaseUserPersistenceContext().getUrl();
    }

    @Override
    public URL getCoserApiURL() {
        return getConfiguration().getCoserApiURL();
    }

    @Override
    public <S extends EchoBaseService> S newService(Class<S> serviceClass) {
        // instantiate service using empty constructor
        S service;
        try {
            service = serviceClass.getConstructor().newInstance();
        } catch (InstantiationException e) {
            throw new EchoBaseTechnicalException(e);
        } catch (IllegalAccessException e) {
            throw new EchoBaseTechnicalException(e);
        } catch (InvocationTargetException e) {
            throw new EchoBaseTechnicalException(e);
        } catch (NoSuchMethodException e) {
            throw new EchoBaseTechnicalException(e);
        }

        // inject
        try {
            injector.inject(this, service);
        } catch (IllegalAccessException e) {
            throw new EchoBaseTechnicalException(e);
        } catch (ClassNotFoundException e) {
            throw new EchoBaseTechnicalException(e);
        }

        return service;
    }

}
