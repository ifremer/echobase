/*
 * #%L
 * EchoBase :: Services
 * %%
 * Copyright (C) 2011 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ifremer.echobase.services.service;

import com.google.common.base.Preconditions;
import fr.ifremer.echobase.EchoBaseTechnicalException;
import fr.ifremer.echobase.entities.EchoBaseInternalPersistenceContext;
import fr.ifremer.echobase.entities.EchoBaseUser;
import fr.ifremer.echobase.entities.EchoBaseUserTopiaDao;
import fr.ifremer.echobase.services.EchoBaseServiceSupport;
import org.apache.commons.lang3.StringUtils;
import org.nuiton.topia.persistence.TopiaException;
import org.nuiton.util.PagerBean;
import org.nuiton.util.StringUtil;
import org.nuiton.util.pagination.PaginationParameter;

import javax.inject.Inject;
import java.util.List;

/**
 * @author Sylvain Letellier
 * @since 0.1
 */
public class UserService extends EchoBaseServiceSupport {

    public static final String DEFAULT_ADMIN_EMAIL = "admin";

    public static final String DEFAULT_ADMIN_PASSWORD = "admin";

    public static final String DEFAULT_USER_EMAIL = "user";

    public static final String DEFAULT_USER_PASSWORD = "user";

    @Inject
    EchoBaseInternalPersistenceContext echoBaseInternalPersistenceContext;

    public List<EchoBaseUser> getUsers() {
        return getUsers((PaginationParameter)null);
    }

    /**
     * @since 3.0, prefer using PaginationParameter param
     */
    @Deprecated
    public List<EchoBaseUser> getUsers(PagerBean pager) {
        PaginationParameter param = PaginationParameter.of(pager.getPageIndex() - 1, pager.getPageSize(), EchoBaseUser.PROPERTY_EMAIL, true);
        return getUsers(param);
    }

    public List<EchoBaseUser> getUsers(PaginationParameter pager) {
        try {
            EchoBaseUserTopiaDao dao = getDao();
            return dao.findAll(pager);
        } catch (TopiaException eee) {
            throw new EchoBaseTechnicalException(eee);
        }
    }

    public EchoBaseUser getUserByEmail(String email) {
        Preconditions.checkNotNull(email);
        try {
            EchoBaseUserTopiaDao dao = getDao();
            return dao.forEmailEquals(email).findAnyOrNull();
        } catch (TopiaException eee) {
            throw new EchoBaseTechnicalException(eee);
        }
    }

    public EchoBaseUser getUserById(String userId) {
        Preconditions.checkNotNull(userId);
        try {
            EchoBaseUser user = getDao().forTopiaIdEquals(userId).findUnique();;
            EchoBaseUser result = newUser();
            result.setEmail(user.getEmail());
            result.setPassword(user.getPassword());
            result.setAdmin(user.isAdmin());
            return result;
        } catch (TopiaException eee) {
            throw new EchoBaseTechnicalException(eee);
        }
    }

    public void createOrUpdate(EchoBaseUser user) {
        try {
            EchoBaseUser userToCreateOrUpdate;

            // No id, creating new one entity
            String id = user.getTopiaId();
            String password = user.getPassword();
            if (StringUtils.isEmpty(id)) {
                userToCreateOrUpdate = getDao().create(
                        EchoBaseUser.PROPERTY_EMAIL, user.getEmail(),
                        EchoBaseUser.PROPERTY_PASSWORD, encodePassword(password)
                );
            } else {
                userToCreateOrUpdate = getDao().forTopiaIdEquals(id).findUnique();;

                userToCreateOrUpdate.setEmail(user.getEmail());
                if (StringUtils.isNotEmpty(password)) {
                    userToCreateOrUpdate.setPassword(encodePassword(password));
                }
            }
            userToCreateOrUpdate.setAdmin(user.isAdmin());
            echoBaseInternalPersistenceContext.commit();
        } catch (TopiaException eee) {
            throw new EchoBaseTechnicalException(eee);
        }
    }

    public void delete(EchoBaseUser user) {
        try {
            EchoBaseUser userToDelete = getDao().forTopiaIdEquals(user.getTopiaId()).findUnique();;
            getDao().delete(userToDelete);
            echoBaseInternalPersistenceContext.commit();
        } catch (TopiaException eee) {
            throw new EchoBaseTechnicalException(eee);
        }
    }

    public boolean checkPassword(EchoBaseUser user,
                                 String password) throws Exception {
        String s = encodePassword(password);
        return s.equals(user.getPassword());
    }

    public void createDefaultUsers() {
        EchoBaseUser user;

        user = newUser();
        user.setEmail(DEFAULT_ADMIN_EMAIL);
        user.setPassword(DEFAULT_ADMIN_PASSWORD);
        user.setAdmin(true);
        createOrUpdate(user);

        user = newUser();
        user.setEmail(DEFAULT_USER_EMAIL);
        user.setPassword(DEFAULT_USER_PASSWORD);
        user.setAdmin(false);
        createOrUpdate(user);
    }

    public static String encodePassword(String password) {
        return StringUtil.encodeMD5(password);
    }

    public EchoBaseUser newUser() {
        try {
            return getDao().newInstance();
        } catch (TopiaException e) {
            throw new EchoBaseTechnicalException(e);
        }
    }

    protected EchoBaseUserTopiaDao getDao() {
        return echoBaseInternalPersistenceContext.getEchoBaseUserDao();
    }
}
