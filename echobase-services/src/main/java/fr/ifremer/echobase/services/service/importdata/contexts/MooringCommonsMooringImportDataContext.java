package fr.ifremer.echobase.services.service.importdata.contexts;

/*
 * #%L
 * EchoBase :: Services
 * %%
 * Copyright (C) 2011 - 2016 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.echobase.entities.EchoBaseUser;
import fr.ifremer.echobase.services.service.UserDbPersistenceService;
import fr.ifremer.echobase.services.service.importdata.configurations.MooringCommonsMooringImportConfiguration;

import java.util.Date;
import java.util.Locale;

/**
 * @author Julien Ruchaud - ruchaud@codelutin.com
 * @since 4.0
 */
public class MooringCommonsMooringImportDataContext extends ImportDataContextSupport<MooringCommonsMooringImportConfiguration> {

    public MooringCommonsMooringImportDataContext(UserDbPersistenceService persistenceService, Locale locale, char csvSeparator, MooringCommonsMooringImportConfiguration configuration, EchoBaseUser user, Date importDate) {
        super(persistenceService, locale, csvSeparator, configuration, user, importDate);
    }

    @Override
    public String getEntityId() {
        return null;
    }

}
