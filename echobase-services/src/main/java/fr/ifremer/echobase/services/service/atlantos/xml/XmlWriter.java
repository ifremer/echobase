package fr.ifremer.echobase.services.service.atlantos.xml;

import java.io.IOException;
import java.io.Writer;
import java.util.Arrays;
import org.apache.commons.lang3.StringUtils;

/*
 * #%L
 * EchoBase :: Services
 * %%
 * Copyright (C) 2011 - 2014 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

/**
 * @author Julien Ruchaud - ruchaud@codelutin.com
 * @since 4
 */
public class XmlWriter {

    protected Writer writer;
    protected String tab;
    
    public XmlWriter(Writer writer) throws IOException {
        this.writer = writer;
        this.tab = "";
    }
    
    public Writer append(String content) throws IOException {
        this.writer.append(content);
        return this.writer;
    }
    
    public Writer open(String tag, String ... attributes) throws IOException {
        this.writer.append(this.tab + "<" + tag);
        this.addAttributes(attributes);
        this.writer.append(">\n");

        this.tab += "\t";
        return this.writer;
    }
    
    public Writer close(String tag) throws IOException {
        this.tab = StringUtils.substring(this.tab, 0, -1);
        this.writer.append(this.tab + "</" + tag + ">\n");        
        return this.writer;
    }

    public Writer create(String tag, Object ... attributes) throws IOException {
        this.writer.append(this.tab + "<" + tag);
        this.addAttributes(attributes);
        
        if (attributes.length % 2 != 0) {
            Object innerValue = attributes[attributes.length - 1];
            if (innerValue != null) {
                this.writer.append(">" + innerValue);
            } else {
                this.writer.append(">");
            }
            this.writer.append("</" + tag + ">\n");
        } else {
            this.writer.append("/>\n");
        }
        
        return this.writer;
    }

    protected void addAttributes(Object[] attributes) throws IOException {
        if (attributes.length % 2 != 0) {
            attributes = Arrays.copyOf(attributes, attributes.length - 1);
        }
        
        for (int index = 0; index < attributes.length; index+=2) {
            Object key = attributes[index];
            Object value = attributes[index + 1];
            
            if (value != null) {
                this.writer.append(" " + key + "=\"" + value + "\"");
            } else {
                this.writer.append(" " + key + "=\"\"");
            }
        }
    }
}
