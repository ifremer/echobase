/*
 * #%L
 * EchoBase :: Services
 * %%
 * Copyright (C) 2011 - 2012 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ifremer.echobase.services.service.importdata.csv;

import fr.ifremer.echobase.entities.data.Mooring;
import fr.ifremer.echobase.entities.data.MooringImpl;
import fr.ifremer.echobase.entities.references.Mission;
import fr.ifremer.echobase.services.csv.EchoBaseCsvUtil;
import fr.ifremer.echobase.services.service.importdata.contexts.MooringCommonsMooringImportDataContext;

/**
 * Model to import Mooring.
 *
 * @author Julien Ruchaud - ruchaud@codelutin.com
 * @since 4.0
 */
public class MooringCommonsMooringImportExportModel extends EchoBaseImportExportModelSupport<Mooring> {

    private MooringCommonsMooringImportExportModel(char separator) {
        super(separator);
    }

    public static MooringCommonsMooringImportExportModel forImport(MooringCommonsMooringImportDataContext importDataContext) {
        MooringCommonsMooringImportExportModel model = new MooringCommonsMooringImportExportModel(importDataContext.getCsvSeparator());
        
        model.newForeignKeyColumn(Mooring.PROPERTY_MISSION, Mooring.PROPERTY_MISSION, Mission.class, Mission.PROPERTY_NAME, importDataContext.getMissionByName());
        model.newMandatoryColumn(Mooring.PROPERTY_CODE);
        model.newMandatoryColumn(Mooring.PROPERTY_DESCRIPTION);
        model.newMandatoryColumn(Mooring.PROPERTY_DEPTH, EchoBaseCsvUtil.PRIMITIVE_FLOAT);
        model.newMandatoryColumn(Mooring.PROPERTY_NORTH_LIMIT, EchoBaseCsvUtil.PRIMITIVE_FLOAT);
        model.newMandatoryColumn(Mooring.PROPERTY_EAST_LIMIT, EchoBaseCsvUtil.PRIMITIVE_FLOAT);
        model.newMandatoryColumn(Mooring.PROPERTY_SOUTH_LIMIT, EchoBaseCsvUtil.PRIMITIVE_FLOAT);
        model.newMandatoryColumn(Mooring.PROPERTY_UP_LIMIT, EchoBaseCsvUtil.PRIMITIVE_FLOAT);
        model.newMandatoryColumn(Mooring.PROPERTY_DOWN_LIMIT, EchoBaseCsvUtil.PRIMITIVE_FLOAT);
        model.newMandatoryColumn(Mooring.PROPERTY_UNITS);
        model.newMandatoryColumn(Mooring.PROPERTY_ZUNITS);
        model.newMandatoryColumn(Mooring.PROPERTY_PROJECTION);
        model.newMandatoryColumn(Mooring.PROPERTY_DEPLOYMENT_DATE, EchoBaseCsvUtil.IMPORT_DAY_TIME_ECHOBASE);
        model.newMandatoryColumn(Mooring.PROPERTY_RETRIEVAL_DATE, EchoBaseCsvUtil.IMPORT_DAY_TIME_ECHOBASE);
        model.newMandatoryColumn(Mooring.PROPERTY_SITE_NAME);
        model.newMandatoryColumn(Mooring.PROPERTY_OPERATOR);
        model.newMandatoryColumn(Mooring.PROPERTY_COMMENTS);

        return model;
    }

    public static MooringCommonsMooringImportExportModel forExport(MooringCommonsMooringImportDataContext importDataContext) {
        MooringCommonsMooringImportExportModel model = new MooringCommonsMooringImportExportModel(importDataContext.getCsvSeparator());

        model.newColumnForExport(Mooring.PROPERTY_MISSION, Mooring.PROPERTY_MISSION, EchoBaseCsvUtil.MISSION_FORMATTER);
        model.newColumnForExport(Mooring.PROPERTY_CODE);
        model.newColumnForExport(Mooring.PROPERTY_DESCRIPTION);
        model.newColumnForExport(Mooring.PROPERTY_DEPTH, EchoBaseCsvUtil.PRIMITIVE_FLOAT);
        model.newColumnForExport(Mooring.PROPERTY_NORTH_LIMIT, EchoBaseCsvUtil.PRIMITIVE_FLOAT);
        model.newColumnForExport(Mooring.PROPERTY_EAST_LIMIT, EchoBaseCsvUtil.PRIMITIVE_FLOAT);
        model.newColumnForExport(Mooring.PROPERTY_SOUTH_LIMIT, EchoBaseCsvUtil.PRIMITIVE_FLOAT);
        model.newColumnForExport(Mooring.PROPERTY_UP_LIMIT, EchoBaseCsvUtil.PRIMITIVE_FLOAT);
        model.newColumnForExport(Mooring.PROPERTY_DOWN_LIMIT, EchoBaseCsvUtil.PRIMITIVE_FLOAT);
        model.newColumnForExport(Mooring.PROPERTY_UNITS);
        model.newColumnForExport(Mooring.PROPERTY_ZUNITS);
        model.newColumnForExport(Mooring.PROPERTY_PROJECTION);
        model.newColumnForExport(Mooring.PROPERTY_DEPLOYMENT_DATE, EchoBaseCsvUtil.IMPORT_DAY_TIME_ECHOBASE);
        model.newColumnForExport(Mooring.PROPERTY_RETRIEVAL_DATE, EchoBaseCsvUtil.IMPORT_DAY_TIME_ECHOBASE);
        model.newColumnForExport(Mooring.PROPERTY_SITE_NAME);
        model.newColumnForExport(Mooring.PROPERTY_OPERATOR);
        model.newColumnForExport(Mooring.PROPERTY_COMMENTS);

        return model;
    }

    @Override
    public Mooring newEmptyInstance() {
        return new MooringImpl();
    }
}
