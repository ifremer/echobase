/*
 * #%L
 * EchoBase :: Services
 * %%
 * Copyright (C) 2011 - 2012 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ifremer.echobase.services.service.importdata.configurations;

import fr.ifremer.echobase.entities.ImportType;
import fr.ifremer.echobase.io.InputFile;

import java.util.Locale;

import static org.nuiton.i18n.I18n.l;

/**
 * Configuration of a "catches data import".
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 0.3
 */
public class VoyageCatchesImportConfiguration extends VoyageImportDataConfigurationSupport {

    private static final long serialVersionUID = 1L;

    /** Sample file to import. */
    protected final InputFile totalSampleFile;

    /** Sub sample file to import. */
    protected final InputFile subSampleFile;

    /** Biometry sample file to import. */
    protected final InputFile biometrySampleFile;

    public VoyageCatchesImportConfiguration(Locale locale) {
        totalSampleFile = InputFile.newFile(l(locale, "echobase.common.totalSampleFile"));
        subSampleFile = InputFile.newFile(l(locale, "echobase.common.subSampleFile"));
        biometrySampleFile = InputFile.newFile(l(locale, "echobase.common.biometrySampleFile"));
        importType = ImportType.CATCHES;
    }

    public InputFile getTotalSampleFile() {
        return totalSampleFile;
    }

    public InputFile getSubSampleFile() {
        return subSampleFile;
    }

    public InputFile getBiometrySampleFile() {
        return biometrySampleFile;
    }

    public boolean isOneImportFile() {
        return totalSampleFile.hasFile()
                || subSampleFile.hasFile()
                || biometrySampleFile.hasFile();
    }

    @Override
    public InputFile[] getInputFiles() {
        return new InputFile[]{totalSampleFile, subSampleFile, biometrySampleFile};
    }
}
