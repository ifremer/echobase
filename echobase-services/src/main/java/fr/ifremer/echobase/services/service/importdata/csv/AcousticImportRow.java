/*
 * #%L
 * EchoBase :: Services
 * %%
 * Copyright (C) 2011 - 2012 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ifremer.echobase.services.service.importdata.csv;

import fr.ifremer.echobase.entities.data.Cell;
import fr.ifremer.echobase.entities.data.Data;
import fr.ifremer.echobase.entities.data.DataAcquisition;
import fr.ifremer.echobase.entities.data.DataProcessing;
import fr.ifremer.echobase.entities.references.AcousticInstrument;
import fr.ifremer.echobase.entities.references.DataMetadata;
import fr.ifremer.echobase.entities.references.DataQuality;
import org.apache.commons.lang3.StringUtils;

import java.util.Collection;
import java.util.Date;
import java.util.Optional;

/**
 * Bean of a row for {@link AcousticImportExportModel} import.
 *
 * @author Julien Ruchaud - ruchaud@codelutin.com
 * @since 4.0
 */
public class AcousticImportRow {

    public static final String PROPERTY_ACOUSTIC_INSTRUMENT = "acousticInstrument";

    public static final String PROPERTY_EI_LAYER = "eiLayer";

    public static final String PROPERTY_CELL_TYPE = "cellType";

    public static final String PROPERTY_CELL_NUM = "cellNum";

    public static final String PROPERTY_ESDU_CELL_DATA_DEPTH = "esduCellDataDepth";

    public static final String PROPERTY_CELL_DATE_START = "cellDateStart";

    public static final String PROPERTY_CELL_DATE_END = "cellDateEnd";

    public static final String PROPERTY_CELL_DEPTH_START = "cellDepthStart";

    public static final String PROPERTY_CELL_DEPTH_END = "cellDepthEnd";

    public static final String PROPERTY_CELL_NASC = "cellNasc";

    public static final String PROPERTY_TRANSCEIVER_ACQUISITION_PULSE_LENGTH = "transceiverAcquisitionPulseLength";

    public static final String PROPERTY_TRANSCEIVER_ACQUISITION_GAIN = "transceiverAcquisitionGain";

    public static final String PROPERTY_TRANSCEIVER_ACQUISITION_ABSORPTION = "transceiverAcquisitionAbsorption";

    public static final String PROPERTY_TRANSDUCER_ACQUISITION_BEAM_ANGLE_ATHWARTSHIP = "transducerAcquisitionBeamAngleAthwartship";

    public static final String PROPERTY_TRANSDUCER_ACQUISITION_BEAM_ANGLE_ALONGSHIP = "transducerAcquisitionBeamAngleAlongship";

    public static final String PROPERTY_TRANSDUCER_ACQUISITION_PSI = "transducerAcquisitionPsi";

    public static final String PROPERTY_TRANSCEIVER_ACQUISITION_POWER = "transceiverAcquisitionPower";

    public static final String PROPERTY_TRANSCEIVER_ACQUISITION_SACORRECTION = "transceiverAcquisitionSacorrection";

    public static final String PROPERTY_E_ITHRESHOLD_LOW = "eIThresholdLow";

    public static final String PROPERTY_E_ITHRESHOLD_HIGH = "eIThresholdHigh";

    public static final String PROPERTY_CELL_VOLUME = "cellVolume";

    public static final String PROPERTY_CELL_SURFACE = "cellSurface";

    public static final String PROPERTY_CELL_NUMBER_OF_SAMPLES_RECORDED = "cellNumberOfSamplesRecorded";

    public static final String PROPERTY_CELL_NUMBER_OF_SAMPLES_ECHO_INTEGRATED = "cellNumberOfSamplesEchoIntegrated";

    public static final String PROPERTY_SOUND_CELERITY = "soundCelerity";

    public static final String PROPERTY_CELL_LATITUDE = "cellLatitude";

    public static final String PROPERTY_CELL_LONGITUDE = "cellLongitude";

    public static final String PROPERTY_DATA_QUALITY = "dataQuality";

    public static final String PROPERTY_LABEL = "label";

    public static final String PROPERTY_BANDWIDTH = "bandwidth";

    public static final String PROPERTY_FREQUENCY = "frequency";

    protected String eiLayer;

    protected String esduCellDataDepth;

    protected Date cellDateStart;

    protected Date cellDateEnd;

    protected float cellLatitude;

    protected float cellLongitude;

    protected Float cellVolume;

    protected int cellSurface;

    protected Integer cellNumberOfSamplesRecorded;

    protected Integer cellNumberOfSamplesEchoIntegrated;

    protected int cellType;

    protected int cellNum;

    protected Float cellDepthStart;

    protected Float cellDepthEnd;

    protected Float cellNasc;

    protected float transceiverAcquisitionPulseLength;

    protected float transceiverAcquisitionGain;

    protected float transceiverAcquisitionAbsorption;

    protected float transceiverAcquisitionPower;

    protected float transceiverAcquisitionSacorrection;

    protected float transducerAcquisitionBeamAngleAthwartship;

    protected float transducerAcquisitionBeamAngleAlongship;

    protected float transducerAcquisitionPsi;

    protected float bandwidth;

    protected float frequency;

    protected int eIThresholdLow;

    protected int eIThresholdHigh;

    protected String soundCelerity;

    protected DataQuality dataQuality;

    protected AcousticInstrument acousticInstrument;

    /**
     * Radial number data (See https://forge.codelutin.com/issues/8222)
     *
     * @since 4.0
     */
    protected String label;

    // Format: <date> <integer>( [S|B])?
    public static Integer getCellNum(String cellName) {
        String[] splited = cellName.split("_");
        return Integer.valueOf(splited[1]);
    }
    
    public static AcousticImportRow ofEsduCell(String processingTemplate,
                                                     DataAcquisition dataAcquisition,
                                                     DataProcessing dataProcessing,
                                                     Cell esduCell,
                                                     DataMetadata radialNumberDataMetadata) {

        AcousticImportRow row = of(processingTemplate, dataAcquisition, dataProcessing, esduCell, radialNumberDataMetadata);

        // esdu cell
        String cellName = esduCell.getName();
        Integer cellNum = getCellNum(cellName);
        row.setCellNum(cellNum);
        
        row.setCellType(4);
        row.setDataQuality(esduCell.getDataQuality());

        return row;
    }

    public static AcousticImportRow ofElementaryCell(String processingTemplate,
                                                           DataAcquisition dataAcquisition,
                                                           DataProcessing dataProcessing,
                                                           Cell elementaryCell,
                                                           DataMetadata radialNumberDataMetadata) {
        AcousticImportRow row = of(processingTemplate, dataAcquisition, dataProcessing, elementaryCell, radialNumberDataMetadata);

        // elementary cell
        String cellName = elementaryCell.getName();
        Integer cellNum = getCellNum(cellName);
        row.setCellNum(cellNum);

        int cellType;
        if (cellName.endsWith("S")) {
            // surface
            cellType = 0;
        } else {
            // bottom
            cellType = 1;
        }

        row.setCellType(cellType);
        row.setCellNum(cellNum);
        row.setDataQuality(elementaryCell.getDataQuality());
        return row;
    }

    private static AcousticImportRow of(String processingTemplate,
                                              DataAcquisition dataAcquisition,
                                              DataProcessing dataProcessing,
                                              Cell cell,
                                              DataMetadata radialNumberDataMetadata) {
        AcousticImportRow row = new AcousticImportRow();
        row.setAcousticInstrument(dataAcquisition.getAcousticInstrument());
        row.setDataQuality(cell.getDataQuality());

        // dataAcquisition
        row.setTransceiverAcquisitionAbsorption(dataAcquisition.getTransceiverAcquisitionAbsorption());
        row.setTransceiverAcquisitionPower(dataAcquisition.getTransceiverAcquisitionPower());
        row.setTransceiverAcquisitionPulseLength(dataAcquisition.getTransceiverAcquisitionPulseLength());
        row.setTransceiverAcquisitionGain(dataAcquisition.getTransceiverAcquisitionGain());
        row.setTransceiverAcquisitionSacorrection(dataAcquisition.getTransceiverAcquisitionSacorrection());

        row.setTransducerAcquisitionBeamAngleAthwartship(dataAcquisition.getTransducerAcquisitionBeamAngleAthwartship());
        row.setTransducerAcquisitionBeamAngleAlongship(dataAcquisition.getTransducerAcquisitionBeamAngleAlongship());
        row.setTransducerAcquisitionPsi(dataAcquisition.getTransducerAcquisitionPsi());

        // dataProcessing
        row.seteIThresholdLow(dataProcessing.geteIThresholdLow());
        row.seteIThresholdHigh(dataProcessing.geteIThresholdHigh());
        row.setBandwidth(dataProcessing.getBandwidth());
        row.setFrequency(dataProcessing.getFrequency());

        row.setTransceiverAcquisitionSacorrection(dataProcessing.getTransceiverProcessingSacorrection());
        row.setTransceiverAcquisitionAbsorption(dataProcessing.getTransceiverProcessingAbsorption());
        row.setTransceiverAcquisitionGain(dataProcessing.getTransceiverProcessingGain());

        row.setTransducerAcquisitionPsi(dataProcessing.getTransducerProcessingPsi());
        row.setTransducerAcquisitionBeamAngleAthwartship(dataProcessing.getTransducerProcessingBeamAngleAthwartship());
        row.setTransducerAcquisitionBeamAngleAlongship(dataProcessing.getTransducerProcessingBeamAngleAlongship());

        String dataProcessingId = dataProcessing.getId();
        row.setEiLayer(StringUtils.substring(dataProcessingId, 0, -processingTemplate.length()));
        row.setSoundCelerity(dataProcessing.getEchosounderSoundSpeed());

        Collection<Data> cellData = cell.getData();
        Optional<Data> optionalRadialNumberData = cellData.stream().filter(data -> data.getDataMetadata().equals(radialNumberDataMetadata)).findFirst();
        if (optionalRadialNumberData.isPresent()) {
            Data data = optionalRadialNumberData.get();
            String dataValue = data.getDataValue();
            row.setLabel(dataValue);
        }
        return row;
    }

    public String getEiLayer() {
        return eiLayer;
    }

    public void setEiLayer(String eiLayer) {
        this.eiLayer = eiLayer;
    }

    public AcousticInstrument getAcousticInstrument() {
        return acousticInstrument;
    }

    public void setAcousticInstrument(AcousticInstrument acousticInstrument) {
        this.acousticInstrument = acousticInstrument;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public Date getCellDateStart() {
        return cellDateStart;
    }

    public void setCellDateStart(Date cellDateStart) {
        this.cellDateStart = cellDateStart;
    }

    public int getCellType() {
        return cellType;
    }

    public void setCellType(int cellType) {
        this.cellType = cellType;
    }

    public int getCellNum() {
        return cellNum;
    }

    public void setCellNum(int cellNum) {
        this.cellNum = cellNum;
    }

    public String getEsduCellDataDepth() {
        return esduCellDataDepth;
    }

    public void setEsduCellDataDepth(String esduCellDataDepth) {
        this.esduCellDataDepth = esduCellDataDepth;
    }

    public Float getCellDepthStart() {
        return cellDepthStart;
    }

    public void setCellDepthStart(Float cellDepthStart) {
        this.cellDepthStart = cellDepthStart;
    }

    public Float getCellDepthEnd() {
        return cellDepthEnd;
    }

    public void setCellDepthEnd(Float cellDepthEnd) {
        this.cellDepthEnd = cellDepthEnd;
    }

    public Float getCellNasc() {
        return cellNasc;
    }

    public void setCellNasc(Float cellNasc) {
        this.cellNasc = cellNasc;
    }

    public float getTransceiverAcquisitionPulseLength() {
        return transceiverAcquisitionPulseLength;
    }

    public void setTransceiverAcquisitionPulseLength(float transceiverAcquisitionPulseLength) {
        this.transceiverAcquisitionPulseLength = transceiverAcquisitionPulseLength;
    }

    public float getTransceiverAcquisitionGain() {
        return transceiverAcquisitionGain;
    }

    public void setTransceiverAcquisitionGain(float transceiverAcquisitionGain) {
        this.transceiverAcquisitionGain = transceiverAcquisitionGain;
    }

    public float getTransceiverAcquisitionAbsorption() {
        return transceiverAcquisitionAbsorption;
    }

    public void setTransceiverAcquisitionAbsorption(float transceiverAcquisitionAbsorption) {
        this.transceiverAcquisitionAbsorption = transceiverAcquisitionAbsorption;
    }

    public float getTransceiverAcquisitionPower() {
        return transceiverAcquisitionPower;
    }

    public void setTransceiverAcquisitionPower(float transceiverAcquisitionPower) {
        this.transceiverAcquisitionPower = transceiverAcquisitionPower;
    }

    public float getTransceiverAcquisitionSacorrection() {
        return transceiverAcquisitionSacorrection;
    }

    public void setTransceiverAcquisitionSacorrection(float transceiverAcquisitionSacorrection) {
        this.transceiverAcquisitionSacorrection = transceiverAcquisitionSacorrection;
    }

    public float getTransducerAcquisitionBeamAngleAthwartship() {
        return transducerAcquisitionBeamAngleAthwartship;
    }

    public void setTransducerAcquisitionBeamAngleAthwartship(float transducerAcquisitionBeamAngleAthwartship) {
        this.transducerAcquisitionBeamAngleAthwartship = transducerAcquisitionBeamAngleAthwartship;
    }

    public float getTransducerAcquisitionBeamAngleAlongship() {
        return transducerAcquisitionBeamAngleAlongship;
    }

    public void setTransducerAcquisitionBeamAngleAlongship(float transducerAcquisitionBeamAngleAlongship) {
        this.transducerAcquisitionBeamAngleAlongship = transducerAcquisitionBeamAngleAlongship;
    }

    public float getTransducerAcquisitionPsi() {
        return transducerAcquisitionPsi;
    }

    public void setTransducerAcquisitionPsi(float transducerAcquisitionPsi) {
        this.transducerAcquisitionPsi = transducerAcquisitionPsi;
    }

    public float getCellLatitude() {
        return cellLatitude;
    }

    public void setCellLatitude(float cellLatitude) {
        this.cellLatitude = cellLatitude;
    }

    public float getCellLongitude() {
        return cellLongitude;
    }

    public void setCellLongitude(float cellLongitude) {
        this.cellLongitude = cellLongitude;
    }

    public int geteIThresholdLow() {
        return eIThresholdLow;
    }

    public void seteIThresholdLow(int eIThresholdLow) {
        this.eIThresholdLow = eIThresholdLow;
    }

    public int geteIThresholdHigh() {
        return eIThresholdHigh;
    }

    public void seteIThresholdHigh(int eIThresholdHigh) {
        this.eIThresholdHigh = eIThresholdHigh;
    }

    public Date getCellDateEnd() {
        return cellDateEnd;
    }

    public void setCellDateEnd(Date cellDateEnd) {
        this.cellDateEnd = cellDateEnd;
    }

    public Float getCellVolume() {
        return cellVolume;
    }

    public void setCellVolume(Float cellVolume) {
        this.cellVolume = cellVolume;
    }

    public int getCellSurface() {
        return cellSurface;
    }

    public void setCellSurface(int cellSurface) {
        this.cellSurface = cellSurface;
    }

    public Integer getCellNumberOfSamplesRecorded() {
        return cellNumberOfSamplesRecorded;
    }

    public void setCellNumberOfSamplesRecorded(Integer cellNumberOfSamplesRecorded) {
        this.cellNumberOfSamplesRecorded = cellNumberOfSamplesRecorded;
    }

    public Integer getCellNumberOfSamplesEchoIntegrated() {
        return cellNumberOfSamplesEchoIntegrated;
    }

    public void setCellNumberOfSamplesEchoIntegrated(Integer cellNumberOfSamplesEchoIntegrated) {
        this.cellNumberOfSamplesEchoIntegrated = cellNumberOfSamplesEchoIntegrated;
    }

    public String getSoundCelerity() {
        return soundCelerity;
    }

    public void setSoundCelerity(String soundCelerity) {
        this.soundCelerity = soundCelerity;
    }

    public DataQuality getDataQuality() {
        return dataQuality;
    }

    public void setDataQuality(DataQuality dataQuality) {
        this.dataQuality = dataQuality;
    }

    public float getBandwidth() {
        return bandwidth;
    }

    public void setBandwidth(float bandwidth) {
        this.bandwidth = bandwidth;
    }

    public float getFrequency() {
        return frequency;
    }

    public void setFrequency(float frequency) {
        this.frequency = frequency;
    }
}
