package fr.ifremer.echobase.services.service.exportCoser;

/*
 * #%L
 * EchoBase :: Services
 * %%
 * Copyright (C) 2011 - 2014 Ifremer, Codelutin, Chemit Tony
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.Lists;
import fr.ifremer.echobase.services.csv.EchoBaseCsvUtil;
import fr.ifremer.echobase.entities.references.Species;
import org.nuiton.csv.ext.AbstractExportModel;

import java.util.List;

/**
 * Created on 3/5/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.6
 */
public class SpeciesExportModel extends AbstractExportModel<SpeciesExportRow> {

    public SpeciesExportModel(char separator) {
        super(separator);
        newColumnForExport("C_Perm", Species.PROPERTY_TAXON_CODE);
        newColumnForExport("NumSys", Species.PROPERTY_TAXON_SYSTEMATIC_ORDER);
        newColumnForExport("NivSys", Species.PROPERTY_TAXON_SYSTEMATIC_LEVEL);
        newColumnForExport("C_VALIDE", EchoBaseCsvUtil.<SpeciesExportRow, Species>newBeanProperty(SpeciesExportRow.PROPERTY_SPECIES), EchoBaseCsvUtil.SPECIES_TO_COSER_CODE);
        newColumnForExport("L_VALIDE", Species.PROPERTY_GENUS_SPECIES);
        newColumnForExport("AA_VALIDE", Species.PROPERTY_AUTHOR_REFERENCE);
        newColumnForExport("C_TxPère", Species.PROPERTY_TAXON_FATHER_MEMOCODE);
        newColumnForExport("Taxa", "taxaCode");
    }

    public List<SpeciesExportRow> toRows(List<Species> speciesList) {
        List<SpeciesExportRow> rows = Lists.newArrayList();
        for (Species species : speciesList) {
            SpeciesExportRow row = new SpeciesExportRow();
            row.setSpecies(species);
            rows.add(row);
        }
        return rows;
    }
}
