/*
 * #%L
 * EchoBase :: Services
 * %%
 * Copyright (C) 2011 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ifremer.echobase.services.service.exportdb;

import fr.ifremer.echobase.services.AbstractEchobaseActionConfiguration;
import fr.ifremer.echobase.services.ProgressModel;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;

/**
 * Configuration of a complete db export.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 0.2
 */
public class ExportDbConfiguration extends AbstractEchobaseActionConfiguration {

    private static final long serialVersionUID = 1L;

    private String fileName;

    private File workingDirectory;

    private File exportFile;

    private ExportDbMode exportDbMode;

    /** Ids of voyages to push in embedded application (with all their datas). */
    private String[] voyageIds;

    private boolean computeSteps;

    public ExportDbConfiguration() {
    }

    public ExportDbConfiguration(ProgressModel progressModel) {
        super(progressModel);
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public File getWorkingDirectory() {
        return workingDirectory;
    }

    public void setWorkingDirectory(File workingDirectory) {
        this.workingDirectory = workingDirectory;
    }

    public File getExportFile() {
        return exportFile;
    }

    public void setExportFile(File exportFile) {
        this.exportFile = exportFile;
    }

    public String[] getVoyageIds() {
        return voyageIds;
    }

    // Attention on ne peut pas utiliser un ... car sinon ça plante dans l'ui
    public void setVoyageIds(String[] voyageIds) {
        this.voyageIds = voyageIds;
    }

    public ExportDbMode getExportDbMode() {
        return exportDbMode;
    }

    public void setExportDbMode(ExportDbMode exportDbMode) {
        this.exportDbMode = exportDbMode;
    }

    public boolean isComputeSteps() {
        return computeSteps;
    }

    public void setComputeSteps(boolean computeSteps) {
        this.computeSteps = computeSteps;
    }

    @Override
    public void destroy() throws IOException {
        if (workingDirectory != null) {
            FileUtils.deleteDirectory(workingDirectory);
        }
    }

}
