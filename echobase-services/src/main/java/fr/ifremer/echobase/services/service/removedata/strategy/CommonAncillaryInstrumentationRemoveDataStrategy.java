package fr.ifremer.echobase.services.service.removedata.strategy;

/*
 * #%L
 * EchoBase :: Services
 * %%
 * Copyright (C) 2011 - 2013 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.Sets;
import fr.ifremer.echobase.entities.ImportLog;
import fr.ifremer.echobase.entities.ImportType;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.persistence.TopiaException;

import java.util.Set;
import fr.ifremer.echobase.entities.data.DataAcousticProvider;
import fr.ifremer.echobase.entities.data.Transect;
import fr.ifremer.echobase.entities.data.Voyage;
import fr.ifremer.echobase.entities.references.AncillaryInstrumentation;

/**
 * Remove a {@link ImportType#COMMON_ANCILLARY_INSTRUMENTATION} import.
 *
 * Can remove only {@link AncillaryInstrumentation}.
 *
 * @author Julien Ruchaud - ruchaud@codelutin.com
 * @since 4.0
 */
public class CommonAncillaryInstrumentationRemoveDataStrategy extends AbstractRemoveDataStrategy<Voyage> {

    /** Logger. */
    private static final Log log =
            LogFactory.getLog(CommonTransectRemoveDataStrategy.class);

    @Override
    public long computeNbSteps(DataAcousticProvider<Voyage> provider, ImportLog importLog) {
        long result = getImportFileIdsCount(importLog);

        return result;
    }
    
    @Override
    protected void removeImportData(DataAcousticProvider<Voyage> provider, String id) throws TopiaException {

        if (id.startsWith(Transect.class.getName())) {
            Transect transect = persistenceService.getTransect(id);
            transect.clearAncillaryInstrumentation();
            
            if (log.isDebugEnabled()) {
                log.debug(id + " was removed");
            }
        } else {
            canNotDealWithId(id);
        }
    }

    @Override
    public Set<ImportType> getPossibleSubImportType() {
        return Sets.newHashSet();
    }
}
