/*
 * #%L
 * EchoBase :: Services
 * %%
 * Copyright (C) 2011 - 2012 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ifremer.echobase.services.service.importdata.csv;

import fr.ifremer.echobase.entities.data.Voyage;
import fr.ifremer.echobase.entities.references.CellType;
import fr.ifremer.echobase.entities.references.DataMetadata;
import fr.ifremer.echobase.entities.references.DataQuality;
import fr.ifremer.echobase.services.csv.EchoBaseCsvUtil;
import fr.ifremer.echobase.services.service.importdata.contexts.VoyageResultsImportDataContext;

import java.util.List;

/**
 * Model to import cells of type 'Map'.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 0.3
 */
public class VoyageResultsMapOtherCellImportExportModel extends EchoBaseImportExportModelSupport<VoyageResultsMapOtherCellImportRow> {

    public static final String[] COLUMN_NAMES_TO_EXCLUDE = {
            VoyageResultsMapOtherCellImportRow.PROPERTY_NAME,
            VoyageResultsMapOtherCellImportRow.PROPERTY_DATA_GRID_CELL_LONGITUDE,
            VoyageResultsMapOtherCellImportRow.PROPERTY_DATA_GRID_CELL_LATITUDE,
            VoyageResultsMapOtherCellImportRow.PROPERTY_DATA_GRID_CELL_DEPTH,
            VoyageResultsMapOtherCellImportRow.PROPERTY_DATA_GRID_LONGITUDE_LAG,
            VoyageResultsMapOtherCellImportRow.PROPERTY_DATA_GRID_LATITUDE_LAG,
            VoyageResultsMapOtherCellImportRow.PROPERTY_DATA_GRID_DEPTH_LAG,
            VoyageResultsMapOtherCellImportRow.PROPERTY_VOYAGE,
            VoyageResultsMapOtherCellImportRow.PROPERTY_DATA_QUALITY
    };
    protected final CellType cellType;

    private VoyageResultsMapOtherCellImportExportModel(char separator, CellType cellType) {
        super(separator);
        this.cellType = cellType;

    }

    public static VoyageResultsMapOtherCellImportExportModel forImport(VoyageResultsImportDataContext importDataContext, List<DataMetadata> dataMetadatas) {

        VoyageResultsMapOtherCellImportExportModel model = new VoyageResultsMapOtherCellImportExportModel(importDataContext.getCsvSeparator(), importDataContext.getMapCellType());
        model.newForeignKeyColumn(VoyageResultsMapOtherCellImportRow.PROPERTY_VOYAGE, Voyage.class, Voyage.PROPERTY_NAME, importDataContext.getVoyagesByName());
        model.newMandatoryColumn(VoyageResultsMapOtherCellImportRow.PROPERTY_NAME);
        model.newForeignKeyColumn(VoyageResultsMapOtherCellImportRow.PROPERTY_DATA_QUALITY, DataQuality.class, DataQuality.PROPERTY_QUALITY_DATA_FLAG_VALUES, importDataContext.getDataQualitiesByName());
        model.newMandatoryColumn(VoyageResultsMapOtherCellImportRow.PROPERTY_DATA_GRID_CELL_LONGITUDE, EchoBaseCsvUtil.PRIMITIVE_FLOAT);
        model.newMandatoryColumn(VoyageResultsMapOtherCellImportRow.PROPERTY_DATA_GRID_CELL_LATITUDE, EchoBaseCsvUtil.PRIMITIVE_FLOAT);
        model.newMandatoryColumn(VoyageResultsMapOtherCellImportRow.PROPERTY_DATA_GRID_CELL_DEPTH, EchoBaseCsvUtil.PRIMITIVE_FLOAT);
        model.newMandatoryColumn(VoyageResultsMapOtherCellImportRow.PROPERTY_DATA_GRID_LONGITUDE_LAG, EchoBaseCsvUtil.PRIMITIVE_FLOAT);
        model.newMandatoryColumn(VoyageResultsMapOtherCellImportRow.PROPERTY_DATA_GRID_LATITUDE_LAG, EchoBaseCsvUtil.PRIMITIVE_FLOAT);
        model.newMandatoryColumn(VoyageResultsMapOtherCellImportRow.PROPERTY_DATA_GRID_DEPTH_LAG, EchoBaseCsvUtil.PRIMITIVE_FLOAT);

        addResultsColumnsForImport(model, dataMetadatas);
        return model;

    }

    public static VoyageResultsMapOtherCellImportExportModel forExport(VoyageResultsImportDataContext importDataContext, List<DataMetadata> dataMetadatas) {

        VoyageResultsMapOtherCellImportExportModel model = new VoyageResultsMapOtherCellImportExportModel(importDataContext.getCsvSeparator(), importDataContext.getMapCellType());
        model.newColumnForExport(VoyageResultsMapOtherCellImportRow.PROPERTY_VOYAGE, EchoBaseCsvUtil.VOYAGE_FORMATTER);
        model.newColumnForExport(VoyageResultsMapOtherCellImportRow.PROPERTY_NAME);
        model.newColumnForExport(VoyageResultsMapOtherCellImportRow.PROPERTY_DATA_QUALITY, EchoBaseCsvUtil.DATA_QUALITY_FORMATTER);
        model.newColumnForExport(VoyageResultsMapOtherCellImportRow.PROPERTY_DATA_GRID_CELL_LONGITUDE, EchoBaseCsvUtil.PRIMITIVE_FLOAT);
        model.newColumnForExport(VoyageResultsMapOtherCellImportRow.PROPERTY_DATA_GRID_CELL_LATITUDE, EchoBaseCsvUtil.PRIMITIVE_FLOAT);
        model.newColumnForExport(VoyageResultsMapOtherCellImportRow.PROPERTY_DATA_GRID_CELL_DEPTH, EchoBaseCsvUtil.PRIMITIVE_FLOAT);
        model.newColumnForExport(VoyageResultsMapOtherCellImportRow.PROPERTY_DATA_GRID_LONGITUDE_LAG, EchoBaseCsvUtil.PRIMITIVE_FLOAT);
        model.newColumnForExport(VoyageResultsMapOtherCellImportRow.PROPERTY_DATA_GRID_LATITUDE_LAG, EchoBaseCsvUtil.PRIMITIVE_FLOAT);
        model.newColumnForExport(VoyageResultsMapOtherCellImportRow.PROPERTY_DATA_GRID_DEPTH_LAG, EchoBaseCsvUtil.PRIMITIVE_FLOAT);

        addResultsColumns(model, dataMetadatas);
        return model;

    }

    @Override
    public VoyageResultsMapOtherCellImportRow newEmptyInstance() {
        return new VoyageResultsMapOtherCellImportRow(cellType);
    }
}
