package fr.ifremer.echobase.ui.actions.removedata;

/*
 * #%L
 * EchoBase :: Services
 * %%
 * Copyright (C) 2011 - 2012 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.echobase.services.service.removedata.RemoveDataService;
import fr.ifremer.echobase.services.service.removedata.strategy.MooringResultEsduRemoveDataStrategy;
import org.junit.Test;
import org.nuiton.topia.persistence.TopiaException;

/**
 * Test{@link RemoveDataService} with {@link MooringResultEsduRemoveDataStrategy}.
 *
 * @author Julien Ruchaud - ruchaud@codelutin.com
 * @since 4.0
 */
public class MooringResultEsduRemoveDataServiceTest extends AbstractRemoveDataServiceTest {

    @Test
    public void removeImport() throws TopiaException {

        removeImport(// import to remove
                     importMooringResultEsduId,
                     // data that should be removed
                     resultEsdu2Id,
                     // importLog that should be removed
                     importMooringResultEsduId);
    }

}
