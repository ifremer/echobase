package fr.ifremer.echobase.ui.actions.removedata;

/*
 * #%L
 * EchoBase :: Services
 * %%
 * Copyright (C) 2011 - 2013 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.echobase.services.service.removedata.RemoveDataService;
import fr.ifremer.echobase.services.service.removedata.strategy.CommonTransitRemoveDataStrategy;
import org.junit.Test;
import org.nuiton.topia.persistence.TopiaException;

/**
 * Test{@link RemoveDataService} with {@link CommonTransitRemoveDataStrategy}.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.2
 */
public class CommonTransitRemoveDataServiceTest

        extends AbstractRemoveDataServiceTest {

    @Test
    public void removeImport1() throws TopiaException {

        removeImport(// import to remove
                     importCommonTransit1Id,
                     // data that should be removed
                     transit1Id , transect1Id, operation1Id, transect1_2Id,
                     totalSample1Id, subSample1Id, biometrySample1Id,
                     cellEsdu1Id, cellElementary1Id, resultEsdu1Id,
                     cellRegion1Id, resultRegion1Id,
                     cellMapFish1Id, resultMapFish1Id,
                     cellMapOther1Id, resultMapOther1Id,
                     // importLog that should be removed
                     importCommonTransit1Id,
                     importCommonTransect1Id,
                     importOperation1Id,
                     importCatches1Id,
                     importAcoustic1Id,
                     importResultEsdu1Id,
                     importResultRegion1Id,
                     importResultMapFish1Id,
                     importResultMapOther1Id
                     );
    }

    @Test
    public void removeImport2() throws TopiaException {

        removeImport(// import to remove
                     importCommonTransit2Id,
                     // data that should be removed
                     transit2Id , transect2Id, transect2_2Id,
                     // importLog that should be removed
                     importCommonTransit2Id,
                     importCommonTransect2Id);
    }
}



