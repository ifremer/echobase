package fr.ifremer.echobase.ui.actions.removedata;

/*
 * #%L
 * EchoBase :: Services
 * %%
 * Copyright (C) 2011 - 2012 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.echobase.services.service.removedata.RemoveDataService;
import fr.ifremer.echobase.services.service.removedata.strategy.ResultVoyageRemoveDataStrategy;
import org.junit.Test;
import org.nuiton.topia.persistence.TopiaException;

/**
 * Test{@link RemoveDataService} with {@link ResultVoyageRemoveDataStrategy}.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.3
 */
public class ResultVoyageRemoveDataServiceTest extends AbstractRemoveDataServiceTest {

    @Test
    public void removeImport() throws TopiaException {

        removeImport(// import to remove
                     importResultVoyage1Id,
                     // data that should be removed
                     echotype1Id, lengthAgeKey1Id, lengthWeightKey1Id,
                     cellMapFish1Id, resultMapFish1Id,
                     cellMapOther1Id, resultMapOther1Id,
                     cellRegion1Id, resultEsdu1Id, resultRegion1Id,
                     // importLog that should be removed
                     importResultVoyage1Id,
                     importResultEsdu1Id,
                     importResultRegion1Id,
                     importResultMapFish1Id,
                     importResultMapOther1Id);
    }
}
