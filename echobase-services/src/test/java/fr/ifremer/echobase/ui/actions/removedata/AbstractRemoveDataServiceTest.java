package fr.ifremer.echobase.ui.actions.removedata;

/*
 * #%L
 * EchoBase :: Services
 * %%
 * Copyright (C) 2011 - 2012 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.Lists;
import fr.ifremer.echobase.EchoBaseTechnicalException;
import fr.ifremer.echobase.entities.EchoBaseUser;
import fr.ifremer.echobase.entities.EchoBaseUserPersistenceContext;
import fr.ifremer.echobase.entities.ImportFile;
import fr.ifremer.echobase.entities.ImportLog;
import fr.ifremer.echobase.entities.ImportType;
import fr.ifremer.echobase.entities.data.Category;
import fr.ifremer.echobase.entities.data.CategoryTopiaDao;
import fr.ifremer.echobase.entities.data.Cell;
import fr.ifremer.echobase.entities.data.CellTopiaDao;
import fr.ifremer.echobase.entities.data.Data;
import fr.ifremer.echobase.entities.data.DataAcquisition;
import fr.ifremer.echobase.entities.data.DataAcquisitionTopiaDao;
import fr.ifremer.echobase.entities.data.DataProcessing;
import fr.ifremer.echobase.entities.data.DataProcessingTopiaDao;
import fr.ifremer.echobase.entities.data.DataTopiaDao;
import fr.ifremer.echobase.entities.data.Echotype;
import fr.ifremer.echobase.entities.data.EchotypeTopiaDao;
import fr.ifremer.echobase.entities.data.GearMetadataValue;
import fr.ifremer.echobase.entities.data.GearMetadataValueTopiaDao;
import fr.ifremer.echobase.entities.data.LengthAgeKey;
import fr.ifremer.echobase.entities.data.LengthAgeKeyTopiaDao;
import fr.ifremer.echobase.entities.data.LengthWeightKey;
import fr.ifremer.echobase.entities.data.LengthWeightKeyTopiaDao;
import fr.ifremer.echobase.entities.data.Mooring;
import fr.ifremer.echobase.entities.data.MooringTopiaDao;
import fr.ifremer.echobase.entities.data.Operation;
import fr.ifremer.echobase.entities.data.OperationMetadataValue;
import fr.ifremer.echobase.entities.data.OperationMetadataValueTopiaDao;
import fr.ifremer.echobase.entities.data.OperationTopiaDao;
import fr.ifremer.echobase.entities.data.Result;
import fr.ifremer.echobase.entities.data.ResultTopiaDao;
import fr.ifremer.echobase.entities.data.Sample;
import fr.ifremer.echobase.entities.data.SampleData;
import fr.ifremer.echobase.entities.data.SampleDataTopiaDao;
import fr.ifremer.echobase.entities.data.SampleTopiaDao;
import fr.ifremer.echobase.entities.data.Transect;
import fr.ifremer.echobase.entities.data.TransectTopiaDao;
import fr.ifremer.echobase.entities.data.Transit;
import fr.ifremer.echobase.entities.data.TransitTopiaDao;
import fr.ifremer.echobase.entities.data.Voyage;
import fr.ifremer.echobase.entities.data.VoyageTopiaDao;
import fr.ifremer.echobase.entities.references.AcousticInstrument;
import fr.ifremer.echobase.entities.references.AncillaryInstrumentation;
import fr.ifremer.echobase.entities.references.AncillaryInstrumentationTopiaDao;
import fr.ifremer.echobase.entities.references.CellType;
import fr.ifremer.echobase.entities.references.DataMetadata;
import fr.ifremer.echobase.entities.references.Gear;
import fr.ifremer.echobase.entities.references.GearMetadata;
import fr.ifremer.echobase.entities.references.Mission;
import fr.ifremer.echobase.entities.references.OperationMetadata;
import fr.ifremer.echobase.entities.references.Port;
import fr.ifremer.echobase.entities.references.SampleDataType;
import fr.ifremer.echobase.entities.references.SampleType;
import fr.ifremer.echobase.entities.references.Species;
import fr.ifremer.echobase.entities.references.SpeciesCategory;
import fr.ifremer.echobase.entities.references.Strata;
import fr.ifremer.echobase.entities.references.Vessel;
import fr.ifremer.echobase.io.InputFile;
import fr.ifremer.echobase.services.EchoBaseTestServiceSupport;
import fr.ifremer.echobase.services.FakeEchoBaseServiceContext;
import fr.ifremer.echobase.services.ImportDataFixtures;
import fr.ifremer.echobase.services.service.UserDbPersistenceService;
import fr.ifremer.echobase.services.service.importdata.configurations.ImportDataConfigurationSupport;
import fr.ifremer.echobase.services.service.importdata.configurations.MooringAcousticsImportConfiguration;
import fr.ifremer.echobase.services.service.importdata.configurations.MooringCommonsAncillaryInstrumentationImportConfiguration;
import fr.ifremer.echobase.services.service.importdata.configurations.MooringCommonsMooringImportConfiguration;
import fr.ifremer.echobase.services.service.importdata.configurations.MooringResultsImportConfiguration;
import fr.ifremer.echobase.services.service.importdata.configurations.VoyageAcousticsImportConfiguration;
import fr.ifremer.echobase.services.service.importdata.configurations.VoyageCatchesImportConfiguration;
import fr.ifremer.echobase.services.service.importdata.configurations.VoyageCommonsAncillaryInstrumentationImportConfiguration;
import fr.ifremer.echobase.services.service.importdata.configurations.VoyageCommonsImportConfiguration;
import fr.ifremer.echobase.services.service.importdata.configurations.VoyageOperationsImportConfiguration;
import fr.ifremer.echobase.services.service.importdata.configurations.VoyageResultsImportConfiguration;
import fr.ifremer.echobase.services.service.removedata.RemoveDataConfiguration;
import fr.ifremer.echobase.services.service.removedata.RemoveDataService;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Assert;
import org.junit.Before;
import org.nuiton.topia.persistence.TopiaDao;
import org.nuiton.topia.persistence.TopiaEntity;
import org.nuiton.topia.persistence.TopiaException;
import org.nuiton.topia.persistence.TopiaIdFactory;

import javax.inject.Inject;
import java.io.File;
import java.io.IOException;
import java.util.List;

/**
 * Abstract test for remove data feature.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.2
 */
public abstract class AbstractRemoveDataServiceTest extends EchoBaseTestServiceSupport {

    /** Logger. */
    private static final Log log = LogFactory.getLog(AbstractRemoveDataServiceTest.class);

    protected String importCommonVoyage1Id;

    protected String importCommonTransit1Id;

    protected String importCommonTransect1Id;

    protected String importOperation1Id;

    protected String importCatches1Id;

    protected String importAcoustic1Id;

    protected String importResultVoyage1Id;

    protected String importResultEsdu1Id;
    
    protected String importMooringResultEsduId;

    protected String importResultRegion1Id;

    protected String importResultMapFish1Id;

    protected String importResultMapOther1Id;
    
    protected String importMooringId;
    
    protected String importMooringAcousticId;
    
    protected String importVoyageAncillaryInstrumentationId;
    
    protected String importMooringAncillaryInstrumentationId;

    protected String importMooringResultsId;
    
    protected static final String voyage1Id = "fr.ifremer.echobase.entities.data.Voyage#1#1";

    protected static final String transit1Id = "fr.ifremer.echobase.entities.data.Transit#1#1";

    protected static final String transect1Id = "fr.ifremer.echobase.entities.data.Transect#1#1";

    protected static final String transect1_2Id = "fr.ifremer.echobase.entities.data.Transect#1#2";

    protected static final String operation1Id = "fr.ifremer.echobase.entities.data.Operation#1#1";

    protected static final String totalSample1Id = "fr.ifremer.echobase.entities.data.Sample#1#total1";

    protected static final String subSample1Id = "fr.ifremer.echobase.entities.data.Sample#1#sub1";

    protected static final String biometrySample1Id = "fr.ifremer.echobase.entities.data.Sample#1#biometry1";

    protected static final String dataAcquisition1Id = "fr.ifremer.echobase.entities.data.DataAcquisition#1";

    protected static final String cellEsdu1Id = "fr.ifremer.echobase.entities.data.Cell#1#esdu1";

    protected static final String cellMapFish1Id = "fr.ifremer.echobase.entities.data.Cell#1#mapfish1";

    protected static final String cellMapOther1Id = "fr.ifremer.echobase.entities.data.Cell#1#mapOther1";

    protected static final String cellRegion1Id = "fr.ifremer.echobase.entities.data.Cell#1#region1";

    protected static final String cellElementary1Id = "fr.ifremer.echobase.entities.data.Cell#1#elementary1";

    protected static final String cellEsdu1_2Id = "fr.ifremer.echobase.entities.data.Cell#1#esdu2";

    protected static final String resultEsdu1Id = "fr.ifremer.echobase.entities.data.Result#1#esdu1";
    
    protected static final String resultEsdu2Id = "fr.ifremer.echobase.entities.data.Result#1#esdu2";

    protected static final String resultMapFish1Id = "fr.ifremer.echobase.entities.data.Result#1#mapFish1";

    protected static final String resultMapOther1Id = "fr.ifremer.echobase.entities.data.Result#1#mapOther1";

    protected static final String resultRegion1Id = "fr.ifremer.echobase.entities.data.Result#1#region1";

    protected static final String resultEsdu1_2Id = "fr.ifremer.echobase.entities.data.Result#1#esdu2";

    protected static final String echotype1Id = "fr.ifremer.echobase.entities.data.Echotype#1#1";
    
    protected static final String echotype2Id = "fr.ifremer.echobase.entities.data.Echotype#1#2";

    protected static final String lengthAgeKey1Id = "fr.ifremer.echobase.entities.data.LengthAgeKey#1#1";

    protected static final String lengthWeightKey1Id = "fr.ifremer.echobase.entities.data.LengthWeightKey#1#1";
    
    protected static final String mooringId = "fr.ifremer.echobase.entities.data.Mooring#1#1";
    
    protected static final String dataAcquisition2Id = "fr.ifremer.echobase.entities.data.DataAcquisition#2";

    protected static final String cellEsdu2Id = "fr.ifremer.echobase.entities.data.Cell#1#esdu2";
    
    protected static final String cellElementary2Id = "fr.ifremer.echobase.entities.data.Cell#1#elementary2";
            
    protected String importCommonVoyage2Id;

    protected String importCommonTransect2Id;

    protected String importCommonTransit2Id;

    protected static final String voyage2Id = "fr.ifremer.echobase.entities.data.Voyage#2#1";

    protected static final String transit2Id = "fr.ifremer.echobase.entities.data.Transit#2#1";

    protected static final String transect2Id = "fr.ifremer.echobase.entities.data.Transect#2#1";

    protected static final String transect2_2Id = "fr.ifremer.echobase.entities.data.Transect#2#2";

    protected AncillaryInstrumentationTopiaDao ancillaryInstrumentationDAO;
    
    protected VoyageTopiaDao voyageDAO;

    protected TransectTopiaDao transectDAO;

    protected TransitTopiaDao transitDAO;

    protected DataAcquisitionTopiaDao dataAcquisitionDAO;

    protected DataProcessingTopiaDao dataProcessingDAO;

    protected CellTopiaDao cellDAO;

    protected DataTopiaDao dataDAO;

    protected OperationTopiaDao operationDAO;

    protected OperationMetadataValueTopiaDao operationMetadataValueDAO;

    protected GearMetadataValueTopiaDao gearMetadataValueDAO;

    protected SampleTopiaDao sampleDAO;

    protected SampleDataTopiaDao sampleDataDAO;

    protected CategoryTopiaDao categoryDAO;

    protected EchotypeTopiaDao echotypeDAO;

    protected LengthAgeKeyTopiaDao lengthAgeKeyDAO;

    protected LengthWeightKeyTopiaDao lengthWeightKeyDAO;

    protected ResultTopiaDao resultDAO;
    
    protected MooringTopiaDao mooringDAO;

    @Inject
    private RemoveDataService removeDataService;

    @Override
    protected FakeEchoBaseServiceContext initContext() {
        return new FakeEchoBaseServiceContext(ImportDataFixtures.IMPORT_DATA_ECHOBASE_NO_DATA.getDbPath());
    }


    EchoBaseUserPersistenceContext persistenceContext;

    @Override
    @Before
    public void setUp() throws Exception {
        super.setUp();

        persistenceContext = serviceContext.getEchoBaseUserPersistenceContext();

        ancillaryInstrumentationDAO = persistenceContext.getAncillaryInstrumentationDao();
        
        voyageDAO = persistenceContext.getVoyageDao();
        transectDAO = persistenceContext.getTransectDao();
        transitDAO = persistenceContext.getTransitDao();
        dataAcquisitionDAO = persistenceContext.getDataAcquisitionDao();
        dataProcessingDAO = persistenceContext.getDataProcessingDao();

        cellDAO = persistenceContext.getCellDao();
        dataDAO = persistenceContext.getDataDao();
        operationDAO = persistenceContext.getOperationDao();
        operationMetadataValueDAO = persistenceContext.getOperationMetadataValueDao();
        gearMetadataValueDAO = persistenceContext.getGearMetadataValueDao();
        sampleDAO = persistenceContext.getSampleDao();
        sampleDataDAO = persistenceContext.getSampleDataDao();

        echotypeDAO = persistenceContext.getEchotypeDao();
        lengthAgeKeyDAO = persistenceContext.getLengthAgeKeyDao();
        lengthWeightKeyDAO = persistenceContext.getLengthWeightKeyDao();
        categoryDAO = persistenceContext.getCategoryDao();
        resultDAO = persistenceContext.getResultDao();

        mooringDAO = persistenceContext.getMooringDao();

        //create imports for voyage 1
        createCommonVoyageImportV1();
        createCommonTransitImportV1();
        createCommonTransectImportV1();
        createOperationImportV1();
        createCatchesImportV1();
        createAcousticImportV1();
        createResultVoyageImportV1();
        createResultEsduImportV1();
        createResultRegionImportV1();
        createResultMapFishImportV1();
        createResultMapOtherImportV1();
        createVoyageAncillaryInstrumentationImport();

        // create imports for voyage 2
        createCommonVoyageImportV2();
        createCommonTransitImportV2();
        createCommonTransectImportV2();
//        createOperationImportV2();
//        createCatchesImportV2();
//        createAcousticImportV2();
//        createResultVoyageImportV2();
//        createResultEsduImportV2();
//        createResultRegionImportV2();
//        createResultMapImportv2();

        // Create imports for mooring
        createMooringImport();
        createMooringAcousticImport();
        createMooringAncillaryInstrumentationImport();
        createMooringResultsImport();
        createMooringResultEsduImport();
    }

    protected void removeImport(String importId, String... shoudDeleteIds) throws TopiaException {

        List<String> existingId = Lists.newArrayList(
                importCommonVoyage1Id,
                importCommonTransit1Id,
                importCommonTransect1Id,
                importOperation1Id,
                importCatches1Id,
                importAcoustic1Id,
                importResultVoyage1Id,
                importResultEsdu1Id,
                importResultRegion1Id,
                importResultMapFish1Id,
                importResultMapOther1Id,
                importCommonVoyage2Id,
                importCommonTransit2Id,
                importCommonTransect2Id,
                importMooringId,
                importMooringAcousticId,
                importVoyageAncillaryInstrumentationId,
                importMooringAncillaryInstrumentationId,
                importMooringResultsId,
                importMooringResultEsduId,

                voyage1Id, transit1Id, transect1Id, transect1_2Id, operation1Id,
                totalSample1Id, subSample1Id, biometrySample1Id,
                cellEsdu1Id, cellElementary1Id, resultEsdu1Id,
                cellRegion1Id, resultRegion1Id,
                cellMapFish1Id, resultMapFish1Id,
                cellMapOther1Id, resultMapOther1Id,
                echotype1Id, lengthAgeKey1Id, lengthWeightKey1Id,
                voyage2Id, transit2Id, transect2Id, transect2_2Id,
                mooringId,
                dataAcquisition2Id, cellEsdu2Id, cellElementary2Id, resultEsdu2Id,
                echotype2Id
        );
        List<String> deletedId = Lists.newArrayList();

        RemoveDataConfiguration conf = new RemoveDataConfiguration();
        conf.setImportLogIds(new String[]{importId});

        EchoBaseUser fakeUser = createFakeUser();

        removeDataService.removeImport(conf, fakeUser);

        for (String id : shoudDeleteIds) {
            existingId.remove(id);
            deletedId.add(id);
        }

        for (String entityId : deletedId) {
            if (entityId == null) continue;
            TopiaEntity entity = getEntity(entityId);
            if (entity instanceof ImportLog) {

                Assert.assertNull("ImporTLog with id " + entityId + " (" + ((ImportLog) entity).getImportType() + ") should have been deleted, but is still in db :(", entity);
            } else {
                Assert.assertNull("Entity with id " + entityId + " should have been deleted, but is still in db :(", entity);
            }
        }

        for (String entityId : existingId) {
            if (entityId == null) continue;
            TopiaEntity entity = getEntity(entityId);
            Assert.assertNotNull("Entity with id " + entityId + " should NOT have been deleted, but is deleted in db :(", entity);

        }
    }

    private <E extends TopiaEntity> E getFirstEntity(Class<E> entityType) {
        List<E> entities = persistenceContext.getDao(entityType).findAll();
        Assert.assertTrue(CollectionUtils.isNotEmpty(entities));
        return entities.get(0);
    }

    private <E extends TopiaEntity> E getEntity(String id) throws TopiaException {

        TopiaIdFactory topiaIdFactory = persistenceContext.getTopiaIdFactory();
        Class<E> entityType = topiaIdFactory.getClassName(id);
        TopiaDao<E> dao = persistenceContext.getDao(entityType);
        return dao.forTopiaIdEquals(id).findUniqueOrNull();
    }
    
    private void createCommonVoyageImportV1() {

        // create voyage
        Voyage voyage = voyageDAO.create(
                TopiaEntity.PROPERTY_TOPIA_ID, voyage1Id,
                Voyage.PROPERTY_MISSION, getFirstEntity(Mission.class),
                Voyage.PROPERTY_NAME, "voyage1",
                Voyage.PROPERTY_START_DATE, newDate(),
                Voyage.PROPERTY_END_DATE, newDate(),
                Voyage.PROPERTY_START_PORT, getFirstEntity(Port.class),
                Voyage.PROPERTY_END_PORT, getFirstEntity(Port.class),
                Voyage.PROPERTY_DESCRIPTION, "voyage1Description",
                Voyage.PROPERTY_DATUM, "voyage1Datum"
        );

        persistenceContext.commit();

        importCommonVoyage1Id = createImport(
                new VoyageCommonsImportConfiguration(getLocale()),
                ImportType.COMMON_VOYAGE,
                "Common/Voyage1",
                voyage1Id,
                voyage);
    }
    
    private void createCommonVoyageImportV2() {

        // create voyage
        Voyage voyage = voyageDAO.create(
                TopiaEntity.PROPERTY_TOPIA_ID, voyage2Id,
                Voyage.PROPERTY_MISSION, getFirstEntity(Mission.class),
                Voyage.PROPERTY_NAME, "voyage2",
                Voyage.PROPERTY_START_DATE, newDate(),
                Voyage.PROPERTY_END_DATE, newDate(),
                Voyage.PROPERTY_START_PORT, getFirstEntity(Port.class),
                Voyage.PROPERTY_END_PORT, getFirstEntity(Port.class),
                Voyage.PROPERTY_DESCRIPTION, "voyage2Description",
                Voyage.PROPERTY_DATUM, "voyage2Datum"
        );

        persistenceContext.commit();

        importCommonVoyage2Id = createImport(
                new VoyageCommonsImportConfiguration(getLocale()),
                ImportType.COMMON_VOYAGE,
                "Common/Voyage2",
                voyage2Id,
                voyage);
    }

    private void createCommonTransitImportV1() {

        // get voyage
        Voyage voyage = getEntity(voyage1Id);

        // create transit
        Transit transit = transitDAO.create(
                TopiaEntity.PROPERTY_TOPIA_ID, transit1Id,
                Transit.PROPERTY_VOYAGE, voyage,
                Transit.PROPERTY_START_TIME, newDate(),
                Transit.PROPERTY_END_TIME, newDate(),
                Transit.PROPERTY_START_LOCALITY, "transit1tartLocality",
                Transit.PROPERTY_END_LOCALITY, "transit1EndLocality",
                Transit.PROPERTY_DESCRIPTION, "transit1Description",
                Transit.PROPERTY_RELATED_ACTIVITY, "transit1RelatedActivity"
        );
        voyage.addTransit(transit);

        persistenceContext.commit();

        importCommonTransit1Id = createImport(
                new VoyageCommonsImportConfiguration(getLocale()),
                ImportType.COMMON_TRANSIT,
                "Common/Transit1",
                voyage1Id,
                transit);
    }

    private void createCommonTransitImportV2() {

        // get voyage
        Voyage voyage = getEntity(voyage2Id);

        // create transit
        Transit transit = transitDAO.create(
                TopiaEntity.PROPERTY_TOPIA_ID, transit2Id,
                Transit.PROPERTY_VOYAGE, voyage,
                Transit.PROPERTY_START_TIME, newDate(),
                Transit.PROPERTY_END_TIME, newDate(),
                Transit.PROPERTY_START_LOCALITY, "transit2StartLocality",
                Transit.PROPERTY_END_LOCALITY, "transit2EndLocality",
                Transit.PROPERTY_DESCRIPTION, "transit2Description",
                Transit.PROPERTY_RELATED_ACTIVITY, "transit2RelatedActivity"
        );
        voyage.addTransit(transit);

        persistenceContext.commit();

        importCommonTransit2Id = createImport(
                new VoyageCommonsImportConfiguration(getLocale()),
                ImportType.COMMON_TRANSIT,
                "Common/Transit2",
                voyage2Id,
                transit);
    }


    private void createCommonTransectImportV1() throws TopiaException {

        Voyage voyage = getEntity(voyage1Id);

        Transit transit = voyage.getTransitByTopiaId(transit1Id);

        // create transect
        Transect transect = transectDAO.create(
                TopiaEntity.PROPERTY_TOPIA_ID, transect1Id,
                Transect.PROPERTY_TRANSIT, transit,
                Transect.PROPERTY_TITLE, "transect1Title",
                Transect.PROPERTY_VESSEL, getFirstEntity(Vessel.class),
                Transect.PROPERTY_STRATUM, "transect1Stratum"
        );
        transit.addTransect(transect);

        // create transect2
        Transect transect2 = transectDAO.create(
                TopiaEntity.PROPERTY_TOPIA_ID, transect1_2Id,
                Transect.PROPERTY_TRANSIT, transit,
                Transect.PROPERTY_TITLE, "transect12Title",
                Transect.PROPERTY_VESSEL, getFirstEntity(Vessel.class),
                Transect.PROPERTY_STRATUM, "transect12Stratum"
        );
        transit.addTransect(transect2);

        persistenceContext.commit();

        importCommonTransect1Id = createImport(
                new VoyageCommonsImportConfiguration(getLocale()),
                ImportType.COMMON_TRANSECT,
                "Common/Transect1",
                voyage1Id,
                transect, transect2);
    }

    private void createCommonTransectImportV2() throws TopiaException {

        Voyage voyage = getEntity(voyage2Id);

        Transit transit = voyage.getTransitByTopiaId(transit2Id);

        // create transect
        Transect transect = transectDAO.create(
                TopiaEntity.PROPERTY_TOPIA_ID, transect2Id,
                Transect.PROPERTY_TRANSIT, transit,
                Transect.PROPERTY_TITLE, "transect2Title",
                Transect.PROPERTY_VESSEL, getFirstEntity(Vessel.class),
                Transect.PROPERTY_STRATUM, "transect22tratum"
        );
        transit.addTransect(transect);

        // create transect2
        Transect transect2 = transectDAO.create(
                TopiaEntity.PROPERTY_TOPIA_ID, transect2_2Id,
                Transect.PROPERTY_TRANSIT, transit,
                Transect.PROPERTY_TITLE, "transect22Title",
                Transect.PROPERTY_VESSEL, getFirstEntity(Vessel.class),
                Transect.PROPERTY_STRATUM, "transect22Stratum"
        );
        transit.addTransect(transect2);

        persistenceContext.commit();

        importCommonTransect2Id = createImport(
                new VoyageCommonsImportConfiguration(getLocale()),
                ImportType.COMMON_TRANSECT,
                "Common/Transect2",
                voyage2Id,
                transect, transect2);
    }

    private void createOperationImportV1() throws TopiaException {

        Transect transect = getEntity(transect1Id);

        // create operation
        Operation operation = operationDAO.create(
                TopiaEntity.PROPERTY_TOPIA_ID, operation1Id,
                Operation.PROPERTY_ID, "operationId",
                Operation.PROPERTY_GEAR, getFirstEntity(Gear.class)
        );

        transect.addOperation(operation);

        // create operationMetadata
        OperationMetadataValue operationMetadataValue = operationMetadataValueDAO.create(
                OperationMetadataValue.PROPERTY_OPERATION_METADATA, getFirstEntity(OperationMetadata.class),
                OperationMetadataValue.PROPERTY_DATA_VALUE, "dataValue"
        );

        operation.addOperationMetadataValue(operationMetadataValue);

        // create geartMetadata
        GearMetadataValue gearMetadataValue = gearMetadataValueDAO.create(
                GearMetadataValue.PROPERTY_GEAR, operation.getGear(),
                GearMetadataValue.PROPERTY_GEAR_METADATA, getFirstEntity(GearMetadata.class),
                GearMetadataValue.PROPERTY_DATA_VALUE, "gearDataValue"
        );

        operation.addGearMetadataValue(gearMetadataValue);

        importOperation1Id = createImport(
                new VoyageOperationsImportConfiguration(getLocale()),
                ImportType.OPERATION,
                "Operation1",
                voyage1Id,
                operation);

    }

    private void createCatchesImportV1() throws TopiaException {

        Operation operation = getEntity(operation1Id);

        // create totalSample
        Sample totalSample = sampleDAO.create(TopiaEntity.PROPERTY_TOPIA_ID, totalSample1Id,
                                              Sample.PROPERTY_SAMPLE_TYPE, getFirstEntity(SampleType.class),
                                              Sample.PROPERTY_SPECIES_CATEGORY, getFirstEntity(SpeciesCategory.class),
                                              Sample.PROPERTY_NUMBER_SAMPLED, 10,
                                              Sample.PROPERTY_SAMPLE_WEIGHT, 2.4f
        );
        operation.addSample(totalSample);

        // add a data
        SampleData totalSampleData = sampleDataDAO.create(SampleData.PROPERTY_DATA_VALUE, 24.5f,
                                                          SampleData.PROPERTY_SAMPLE_DATA_TYPE, getFirstEntity(SampleDataType.class)
        );
        totalSample.addSampleData(totalSampleData);

        // create subSample
        Sample subSample = sampleDAO.create(TopiaEntity.PROPERTY_TOPIA_ID, subSample1Id,
                                            Sample.PROPERTY_SAMPLE_TYPE, getFirstEntity(SampleType.class),
                                            Sample.PROPERTY_SPECIES_CATEGORY, getFirstEntity(SpeciesCategory.class),
                                            Sample.PROPERTY_NUMBER_SAMPLED, 10,
                                            Sample.PROPERTY_SAMPLE_WEIGHT, 2.5f
        );
        operation.addSample(subSample);
        // add a data
        SampleData subSampleData = sampleDataDAO.create(
                SampleData.PROPERTY_DATA_VALUE, 25.4f,
                SampleData.PROPERTY_SAMPLE_DATA_TYPE, getFirstEntity(SampleDataType.class)
        );
        subSample.addSampleData(subSampleData);

        // create biometrySample
        Sample biometrySample = sampleDAO.create(
                TopiaEntity.PROPERTY_TOPIA_ID, biometrySample1Id,
                Sample.PROPERTY_SAMPLE_TYPE, getFirstEntity(SampleType.class),
                Sample.PROPERTY_SPECIES_CATEGORY, getFirstEntity(SpeciesCategory.class),
                Sample.PROPERTY_NUMBER_SAMPLED, 10,
                Sample.PROPERTY_SAMPLE_WEIGHT, 2.6f
        );
        operation.addSample(biometrySample);

        // add a data
        SampleData biometrySampleData = sampleDataDAO.create(
                SampleData.PROPERTY_DATA_VALUE, 26.4f,
                SampleData.PROPERTY_SAMPLE_DATA_TYPE, getFirstEntity(SampleDataType.class)
        );
        biometrySample.addSampleData(biometrySampleData);

        importCatches1Id = createImport(
                new VoyageCatchesImportConfiguration(getLocale()),
                ImportType.CATCHES,
                "Catches1",
                voyage1Id,
                totalSample, subSample, biometrySample);
    }

    private void createAcousticImportV1() throws TopiaException {

        Transect transect = getEntity(transect1Id);

        // create data acquisition
        DataAcquisition dataAcquisition = dataAcquisitionDAO.create(TopiaEntity.PROPERTY_TOPIA_ID, dataAcquisition1Id,
                                                                    DataAcquisition.PROPERTY_ACOUSTIC_INSTRUMENT, getFirstEntity(AcousticInstrument.class));
        transect.addDataAcquisition(dataAcquisition);

        // create dataProcessing
        DataProcessing dataProcessing = dataProcessingDAO.create(DataProcessing.PROPERTY_ID, "id1",
                                                                 DataProcessing.PROPERTY_PROCESSING_TEMPLATE, "processingTemplate");
        dataAcquisition.addDataProcessing(dataProcessing);

        // create esdu cell
        Cell esduCell = cellDAO.create(TopiaEntity.PROPERTY_TOPIA_ID, cellEsdu1Id,
                                       Cell.PROPERTY_CELL_TYPE, getFirstEntity(CellType.class),
                                       Cell.PROPERTY_NAME, "cellEsdu1");
        dataProcessing.addCell(esduCell);

        // create cell data
        Data esduData = dataDAO.create(Data.PROPERTY_DATA_METADATA, getFirstEntity(DataMetadata.class),
                                       Data.PROPERTY_DATA_VALUE, "esdu1Data");
        esduCell.addData(esduData);

        // create elementary cell
        Cell elementaryCell = cellDAO.create(TopiaEntity.PROPERTY_TOPIA_ID, cellElementary1Id,
                                             Cell.PROPERTY_CELL_TYPE, getFirstEntity(CellType.class),
                                             Cell.PROPERTY_NAME, "cellElementary1");
        esduCell.addChilds(elementaryCell);

        // create cell data
        Data elementaryData = dataDAO.create(Data.PROPERTY_DATA_METADATA, getFirstEntity(DataMetadata.class),
                                             Data.PROPERTY_DATA_VALUE, "elementary1Data");
        elementaryCell.addData(elementaryData);

        importAcoustic1Id = createImport(
                new VoyageAcousticsImportConfiguration(getLocale()),
                ImportType.ACOUSTIC,
                "Acoustic1",
                voyage1Id,
                dataAcquisition);
    }

    private void createResultVoyageImportV1() throws TopiaException {

        Voyage voyage = getEntity(voyage1Id);

        // create echotype
        Echotype echotype = echotypeDAO.create(TopiaEntity.PROPERTY_TOPIA_ID, echotype1Id,
                                               Echotype.PROPERTY_NAME, "echotype1Name");
        voyage.addEchotype(echotype);

        // create lengthAgeKey
        LengthAgeKey lengthAgeKey = lengthAgeKeyDAO.create(TopiaEntity.PROPERTY_TOPIA_ID, lengthAgeKey1Id,
                                                           LengthAgeKey.PROPERTY_AGE, 12,
                                                           LengthAgeKey.PROPERTY_LENGTH, 12.5f,
                                                           LengthAgeKey.PROPERTY_METADATA, "lengthAgeKeyMetadata1",
                                                           LengthAgeKey.PROPERTY_SPECIES, getFirstEntity(Species.class),
                                                           LengthAgeKey.PROPERTY_STRATA, getFirstEntity(Strata.class)
        );
        voyage.addLengthAgeKey(lengthAgeKey);

        // create lengthWeightKey

        LengthWeightKey lengthWeightKey = lengthWeightKeyDAO.create(TopiaEntity.PROPERTY_TOPIA_ID, lengthWeightKey1Id,
                                                                    LengthWeightKey.PROPERTY_APARAMETER, 12.5f,
                                                                    LengthWeightKey.PROPERTY_BPARAMETER, 0.2f,
                                                                    LengthWeightKey.PROPERTY_METADATA, "lengthWeightMetadata1",
                                                                    LengthWeightKey.PROPERTY_SPECIES_CATEGORY, getFirstEntity(SpeciesCategory.class),
                                                                    LengthWeightKey.PROPERTY_STRATA, getFirstEntity(Strata.class)
        );
        voyage.addLengthWeightKey(lengthWeightKey);

        importResultVoyage1Id = createImport(
                new VoyageResultsImportConfiguration(getLocale()),
                ImportType.RESULT_VOYAGE,
                "ResultsVoyage1",
                voyage1Id,
                echotype, lengthAgeKey, lengthWeightKey);
    }

    private void createResultEsduImportV1() throws TopiaException {

        // create result (on ESDU)

        Category category = categoryDAO.create();

        Result result = resultDAO.create(TopiaEntity.PROPERTY_TOPIA_ID, resultEsdu1Id,
                                         Result.PROPERTY_CATEGORY, category,
                                         Result.PROPERTY_DATA_METADATA, getFirstEntity(DataMetadata.class),
                                         Result.PROPERTY_RESULT_VALUE, "resultValueEdsu1",
                                         Result.PROPERTY_RESULT_LABEL, "resultLabelEdsu1"
        );

        Cell cell = getEntity(cellEsdu1Id);
        cell.addResult(result);

        importResultEsdu1Id = createImport(
                new VoyageResultsImportConfiguration(getLocale()),
                ImportType.RESULT_ESDU,
                "ResultsEsdu1",
                voyage1Id,
                result);
    }

    private void createResultRegionImportV1() throws TopiaException {

        // create region
        Cell region = cellDAO.create(TopiaEntity.PROPERTY_TOPIA_ID, cellRegion1Id,
                                     Cell.PROPERTY_CELL_TYPE, getFirstEntity(CellType.class),
                                     Cell.PROPERTY_NAME, "cellRegion1");

        Voyage voyage = getEntity(voyage1Id);
        voyage.addPostCell(region);

        // add esdu cell on it
        Cell esduCell = getEntity(cellEsdu1Id);
        region.addChilds(esduCell);

        // create result (on region)
        Category category = categoryDAO.create();
        Result result = resultDAO.create(TopiaEntity.PROPERTY_TOPIA_ID, resultRegion1Id,
                                         Result.PROPERTY_CATEGORY, category,
                                         Result.PROPERTY_DATA_METADATA, getFirstEntity(DataMetadata.class),
                                         Result.PROPERTY_RESULT_VALUE, "resultValueRegion1",
                                         Result.PROPERTY_RESULT_LABEL, "resultLabelRegion1"
        );
        region.addResult(result);

        importResultRegion1Id = createImport(
                new VoyageResultsImportConfiguration(getLocale()),
                ImportType.RESULT_REGION,
                "ResultsRegion1",
                voyage1Id,
                region);
    }

    private void createResultMapFishImportV1() throws TopiaException {

        // create map
        Cell map = cellDAO.create(TopiaEntity.PROPERTY_TOPIA_ID, cellMapFish1Id,
                                  Cell.PROPERTY_CELL_TYPE, getFirstEntity(CellType.class),
                                  Cell.PROPERTY_NAME, "cellMap1");

        Voyage voyage = getEntity(voyage1Id);
        voyage.addPostCell(map);

        // create result (on map)
        Category category = categoryDAO.create();
        Result result = resultDAO.create(TopiaEntity.PROPERTY_TOPIA_ID, resultMapFish1Id,
                                         Result.PROPERTY_CATEGORY, category,
                                         Result.PROPERTY_DATA_METADATA, getFirstEntity(DataMetadata.class),
                                         Result.PROPERTY_RESULT_VALUE, "resultValueMapFish1",
                                         Result.PROPERTY_RESULT_LABEL, "resultLabelMapFish1"
        );
        map.addResult(result);

        importResultMapFish1Id = createImport(
                new VoyageResultsImportConfiguration(getLocale()),
                ImportType.RESULT_MAP_FISH,
                "ResultsMapFish1",
                voyage1Id,
                map);
    }

    private void createResultMapOtherImportV1() throws TopiaException {

        // create map
        Cell map = cellDAO.create(TopiaEntity.PROPERTY_TOPIA_ID, cellMapOther1Id,
                                  Cell.PROPERTY_CELL_TYPE, getFirstEntity(CellType.class),
                                  Cell.PROPERTY_NAME, "cellMapOther1");

        Voyage voyage = getEntity(voyage1Id);
        voyage.addPostCell(map);

        // create result (on map)
        Result result = resultDAO.create(TopiaEntity.PROPERTY_TOPIA_ID, resultMapOther1Id,
                                         Result.PROPERTY_CATEGORY, null,
                                         Result.PROPERTY_DATA_METADATA, getFirstEntity(DataMetadata.class),
                                         Result.PROPERTY_RESULT_VALUE, "resultValueMapOther1",
                                         Result.PROPERTY_RESULT_LABEL, "resultLabelMapOther1"
        );
        map.addResult(result);

        importResultMapOther1Id = createImport(
                new VoyageResultsImportConfiguration(getLocale()),
                ImportType.RESULT_MAP_OTHER,
                "ResultsMapOther1",
                voyage1Id,
                map);
    }
    
    private void createMooringImport() {

        // create mooring
        Mooring mooring = mooringDAO.create(
                TopiaEntity.PROPERTY_TOPIA_ID, mooringId,
                Mooring.PROPERTY_CODE, "MOOMO",
                Mooring.PROPERTY_DESCRIPTION, "MOOMO",
                Mooring.PROPERTY_MISSION, getFirstEntity(Mission.class)
        );

        persistenceContext.commit();

        importMooringId = createImport(new MooringCommonsMooringImportConfiguration(getLocale()),
                ImportType.MOORING_COMMONS,
                "Common/Mooring",
                null,
                mooring);
    }
    
    private void createMooringAcousticImport() {
        Mooring mooring = getEntity(mooringId);

        // create data acquisition
        DataAcquisition dataAcquisition = dataAcquisitionDAO.create(TopiaEntity.PROPERTY_TOPIA_ID, dataAcquisition2Id,
                                                                    DataAcquisition.PROPERTY_ACOUSTIC_INSTRUMENT, getFirstEntity(AcousticInstrument.class));
        mooring.addDataAcquisition(dataAcquisition);

        // create dataProcessing
        DataProcessing dataProcessing = dataProcessingDAO.create(DataProcessing.PROPERTY_ID, "id2",
                                                                 DataProcessing.PROPERTY_PROCESSING_TEMPLATE, "processingTemplate");
        dataAcquisition.addDataProcessing(dataProcessing);

        // create esdu cell
        Cell esduCell = cellDAO.create(TopiaEntity.PROPERTY_TOPIA_ID, cellEsdu2Id,
                                       Cell.PROPERTY_CELL_TYPE, getFirstEntity(CellType.class),
                                       Cell.PROPERTY_NAME, "cellEsdu2");
        dataProcessing.addCell(esduCell);

        // create cell data
        Data esduData = dataDAO.create(Data.PROPERTY_DATA_METADATA, getFirstEntity(DataMetadata.class),
                                       Data.PROPERTY_DATA_VALUE, "esdu2Data");
        esduCell.addData(esduData);

        // create elementary cell
        Cell elementaryCell = cellDAO.create(TopiaEntity.PROPERTY_TOPIA_ID, cellElementary2Id,
                                             Cell.PROPERTY_CELL_TYPE, getFirstEntity(CellType.class),
                                             Cell.PROPERTY_NAME, "cellElementary2");
        esduCell.addChilds(elementaryCell);

        // create cell data
        Data elementaryData = dataDAO.create(Data.PROPERTY_DATA_METADATA, getFirstEntity(DataMetadata.class),
                                             Data.PROPERTY_DATA_VALUE, "elementary1Data");
        elementaryCell.addData(elementaryData);

        importMooringAcousticId = createImport(
                new MooringAcousticsImportConfiguration(getLocale()),
                ImportType.MOORING_ACOUSTIC,
                "Acoustic2",
                mooringId,
                dataAcquisition);
    }
    
    private void createVoyageAncillaryInstrumentationImport() {
        List<AncillaryInstrumentation> instrumentations = ancillaryInstrumentationDAO.findAll();
        Transect transect = getEntity(transect1Id);
        transect.addAllAncillaryInstrumentation(instrumentations);
        
        Voyage voyage = transect.getTransit().getVoyage();
        
        importVoyageAncillaryInstrumentationId  = createImport(
                new VoyageCommonsAncillaryInstrumentationImportConfiguration(getLocale()),
                ImportType.COMMON_ANCILLARY_INSTRUMENTATION,
                "AncillaryInstrumentationVoyage",
                voyage.getTopiaId(),
                transect);
    }
    
    private void createMooringAncillaryInstrumentationImport() {
        List<AncillaryInstrumentation> instrumentations = ancillaryInstrumentationDAO.findAll();
        Mooring mooring = getEntity(mooringId);
        mooring.addAllAncillaryInstrumentation(instrumentations);
        
        importMooringAncillaryInstrumentationId  = createImport(
                new MooringCommonsAncillaryInstrumentationImportConfiguration(getLocale()),
                ImportType.MOORING_ANCILLARY_INSTRUMENTATION,
                "AncillaryInstrumentationMooring",
                mooringId,
                mooring);
    }
    
    private void createMooringResultsImport() throws TopiaException {

        Mooring mooring = getEntity(mooringId);

        // create echotype
        Echotype echotype = echotypeDAO.create(TopiaEntity.PROPERTY_TOPIA_ID, echotype2Id,
                                               Echotype.PROPERTY_NAME, "echotype2Name");
        mooring.addEchotype(echotype);

        importMooringResultsId = createImport(
                new MooringResultsImportConfiguration(getLocale()),
                ImportType.RESULT_MOORING,
                "MooringResults",
                mooringId,
                echotype);
    }

    private void createMooringResultEsduImport() throws TopiaException {

        // create result (on ESDU)

        Category category = categoryDAO.create();

        Result result = resultDAO.create(TopiaEntity.PROPERTY_TOPIA_ID, resultEsdu2Id,
                                         Result.PROPERTY_CATEGORY, category,
                                         Result.PROPERTY_DATA_METADATA, getFirstEntity(DataMetadata.class),
                                         Result.PROPERTY_RESULT_VALUE, "resultValueEdsu2",
                                         Result.PROPERTY_RESULT_LABEL, "resultLabelEdsu2"
        );

        Cell cell = getEntity(cellEsdu2Id);
        cell.addResult(result);

        importMooringResultEsduId = createImport(
                new MooringResultsImportConfiguration(getLocale()),
                ImportType.RESULT_MOORING_ESDU,
                "ResultsEsdu2",
                mooringId,
                result);
    }


    private <C extends ImportDataConfigurationSupport> String createImport(C importConf,
                                                                                 ImportType importType,
                                                                                 String importNote,
                                                                                 String providerId,
                                                                                 TopiaEntity... importIds) {
        importConf.setImportType(importType);
        importConf.setImportNotes(importNote);
//        importConf.setVoyageId(voyageId);

        UserDbPersistenceService persistenceService = newService(UserDbPersistenceService.class);

        ImportLog importLog = persistenceService.createImportLog(importConf.getImportType(),
                                                                 createFakeUser().getEmail(),
                                                                 newDate(),
                                                                 importNote);
        importLog.setEntityId(providerId);

        InputFile fakeInputFile = InputFile.newFile("Fake import: " + importConf);
        try {
            fakeInputFile.setFile(File.createTempFile("prefix", "suffix", getConfiguration().getTemporaryDirectory()));
        } catch (IOException e) {
            throw new EchoBaseTechnicalException("Can't create temporary file", e);
        }
        ImportFile importFile = persistenceService.createImportFile(fakeInputFile);
        importLog.addImportFile(importFile);
        for (TopiaEntity importId : importIds) {
            persistenceService.createImportFileId(importFile, importId, 1, 1);
        }

        persistenceContext.commit();

        String importId = importLog.getTopiaId();

        if (log.isInfoEnabled()) {
            log.info("Created importdata " + importType + ": " + importId);
        }
        return importId;
    }
}
