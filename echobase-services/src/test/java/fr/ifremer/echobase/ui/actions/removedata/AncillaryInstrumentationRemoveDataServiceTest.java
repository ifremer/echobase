package fr.ifremer.echobase.ui.actions.removedata;

/*
 * #%L
 * EchoBase :: Services
 * %%
 * Copyright (C) 2011 - 2012 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.echobase.services.service.removedata.RemoveDataService;
import fr.ifremer.echobase.services.service.removedata.strategy.MooringAncillaryInstrumentationRemoveDataStrategy;
import org.junit.Test;

/**
 * Test{@link RemoveDataService} with {@link MooringAncillaryInstrumentationRemoveDataStrategy} or 
 * {@link CommonsAncillaryInstrumentationRemoveDataStrategy}.
 *
 * @author Julien Ruchaud - ruchaud@codelutin.com
 * @since 4.0
 */
public class AncillaryInstrumentationRemoveDataServiceTest extends AbstractRemoveDataServiceTest {

    @Test
    public void removeVoyageImport() throws Exception {

        removeImport(// import to remove
                     importVoyageAncillaryInstrumentationId,
                     importVoyageAncillaryInstrumentationId
        );

    }
    
    @Test
    public void removeMooringImport() throws Exception {

        removeImport(// import to remove
                     importMooringAncillaryInstrumentationId,
                     importMooringAncillaryInstrumentationId
        );

    }
}
