package fr.ifremer.echobase.services.service.importdata;

/*
 * #%L
 * EchoBase :: Services
 * %%
 * Copyright (C) 2011 - 2013 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.base.Optional;
import fr.ifremer.echobase.entities.EchoBaseUserPersistenceContext;
import fr.ifremer.echobase.entities.references.SampleDataType;
import fr.ifremer.echobase.services.service.importdata.configurations.VoyageCatchesImportConfiguration;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.IOException;

/**
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.2
 */
public abstract class VoyageCatchesImportServiceITSupport extends AbstractImportDataServiceIT<VoyageCatchesImportConfiguration> {

    /** Logger. */
    private static final Log log = LogFactory.getLog(VoyageCatchesImportServiceITSupport.class);

    protected VoyageCatchesImportServiceITSupport(int nbFiles) {
        super(nbFiles);
    }

    @Override
    protected final String[] getImportPath(String filename) {
        return new String[]{"/import-data", "catches", filename};
    }

    @Override
    protected ImportDataService.VoyageCatchesImportDataAction newAction() throws IOException {
        return new ImportDataService.VoyageCatchesImportDataAction();
    }

    @Override
    protected VoyageCatchesImportConfiguration createConfiguration() throws IOException {
        VoyageCatchesImportConfiguration configuration = new VoyageCatchesImportConfiguration(getLocale());
        configuration.setVoyageId(getVoyageId());
        return configuration;
    }

    protected void addMissingSampleDataType(String sampleDataTypeName) {

        EchoBaseUserPersistenceContext persistenceContext = this.serviceContext.getEchoBaseUserPersistenceContext();

        Optional<SampleDataType> optionalSampleDataType = persistenceContext.getSampleDataTypeDao().forNameEquals(sampleDataTypeName).tryFindUnique();
        if (!optionalSampleDataType.isPresent()) {

            if (log.isInfoEnabled()) {
                log.info("Add sampleDataType " +sampleDataTypeName);
            }
            persistenceContext.getSampleDataTypeDao().create(SampleDataType.PROPERTY_NAME,sampleDataTypeName);

            persistenceContext.commit();
        }

    }
}
