/*
 * #%L
 * EchoBase :: Services
 * %%
 * Copyright (C) 2011 - 2012 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ifremer.echobase.services.service.importdb;

import fr.ifremer.echobase.entities.EchoBaseUser;
import fr.ifremer.echobase.entities.EchoBaseUserImpl;
import fr.ifremer.echobase.services.EchoBaseTestServiceSupport;
import fr.ifremer.echobase.services.FakeEchoBaseServiceContext;
import fr.ifremer.echobase.services.service.importdata.ImportException;
import org.junit.Assert;
import org.junit.Test;
import org.nuiton.util.FileUtil;

import javax.inject.Inject;
import java.io.File;
import java.io.IOException;

/**
 * Tests the {@link ImportDbService}.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 0.3
 */
public class ImportDbServiceTest extends EchoBaseTestServiceSupport {

    public static final String DB_VERSION = "1.2";

    @Inject
    private ImportDbService importDbService;

    @Override
    protected FakeEchoBaseServiceContext initContext() {
        return new FakeEchoBaseServiceContext(null);
    }

    @Test
    public void importDb() throws IOException, ImportException {

        ImportDbConfiguration conf = new ImportDbConfiguration(getLocale());

        File workingDirectory = new File(getTestDir(), "work-dir");
        FileUtil.createDirectoryIfNecessary(workingDirectory);
        conf.setWorkingDirectory(workingDirectory);
        conf.setComputeSteps(true);
        conf.setCommitAfterEachFile(false);
        conf.setImportDbMode(ImportDbMode.REFERENTIAL);
        prepareInputFile(conf.getInput(), fixtures.importDbReferentialPath());

        EchoBaseUser user = new EchoBaseUserImpl();
        user.setEmail("testUser@fake.fr");

        importDbService.doImport(conf, user);

        Assert.assertTrue(conf.getProgress() > 94);
    }

}
