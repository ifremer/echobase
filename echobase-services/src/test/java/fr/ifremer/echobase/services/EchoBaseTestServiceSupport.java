/*
 * #%L
 * EchoBase :: Services
 * %%
 * Copyright (C) 2011 - 2012 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ifremer.echobase.services;

import com.google.common.base.Preconditions;
import fr.ifremer.echobase.entities.EchoBaseUser;
import fr.ifremer.echobase.entities.EchoBaseUserImpl;
import fr.ifremer.echobase.entities.data.Mooring;
import fr.ifremer.echobase.entities.data.Voyage;
import fr.ifremer.echobase.io.InputFile;
import fr.ifremer.echobase.services.service.exportdb.ExportDbConfiguration;
import fr.ifremer.echobase.services.service.exportdb.ExportDbMode;
import fr.ifremer.echobase.services.service.exportdb.ExportDbService;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.nuiton.util.FileUtil;
import org.nuiton.util.StringUtil;

import javax.inject.Inject;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.zip.GZIPInputStream;

/**
 * Nice test service support.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 0.3
 */
public abstract class EchoBaseTestServiceSupport extends EchoBaseServiceSupport {

    /** Logger. */
    private static final Log log = LogFactory.getLog(EchoBaseTestServiceSupport.class);

    protected abstract FakeEchoBaseServiceContext initContext();

    protected final EchoBaseServiceFixtures fixtures = new EchoBaseServiceFixtures();

    @Rule
    public FakeEchoBaseServiceContext fakeServiceContext = initContext();

    @Inject
    private ExportDbService exportDbService;

//    @Inject
//    private ImportDbService importDbService;

    @Before
    public void setUp() throws Exception {
        this.serviceContext = fakeServiceContext;
        if (fakeServiceContext != null) {
            fakeServiceContext.inject(this);
        }
    }

    protected EchoBaseUser createFakeUser() {
        EchoBaseUser echoBaseUser = new EchoBaseUserImpl();
        echoBaseUser.setEmail("echobase-test@ifremer.fr");
        return echoBaseUser;
    }

    protected File getTestDir() {
        return fakeServiceContext.getTestDir();
    }

    public void assertConfProgressionToEnd(ProgressModel conf) {
        Assert.assertEquals(100f, conf.getProgress(), 1);
    }

    protected void prepareInputFile(InputFile inputFile, String... path) throws IOException {
        String filename = path[path.length - 1];
        String resourcePath = StringUtil.join(Arrays.asList(path), "/", true);
        try (InputStream inputStream = getClass().getResourceAsStream(resourcePath)) {

            Preconditions.checkNotNull(inputStream, "Could not find resource at " + resourcePath);
            File testDir = getTestDir();

            File newFile;

            if (filename.endsWith(".gz")) {

                // gzipped file

                filename = filename.substring(0, filename.length() - 3);
                newFile = new File(testDir, filename);

                try (FileOutputStream outputStream = FileUtils.openOutputStream(newFile)) {
                    IOUtils.copy(new GZIPInputStream(inputStream), outputStream);
                }

            } else {

                // non gzipped file

                newFile = new File(testDir, filename);

                FileUtils.copyInputStreamToFile(inputStream, newFile);
            }

            inputFile.setFileName(filename);
            inputFile.setFile(newFile);

            if (log.isDebugEnabled()) {
                log.debug("Copy file to " + newFile);
            }

        }
    }

    protected String getVoyageId() {
        Voyage voyage = serviceContext.getEchoBaseUserPersistenceContext().getVoyageDao().findAll().get(0);
        Preconditions.checkNotNull(voyage);
        return voyage.getTopiaId();
    }

    protected String getMooringId() {
        Mooring mooring = serviceContext.getEchoBaseUserPersistenceContext().getMooringDao().findAll().get(0);
        Preconditions.checkNotNull(mooring);
        return mooring.getTopiaId();
    }

//    protected void importdb(ImportDbMode mode, String path) throws IOException, ImportException {
//
//        ImportDbConfiguration conf = new ImportDbConfiguration(getLocale());
//
//        File workingDirectory = new File(getTestDir(), "work-dir");
//        FileUtil.createDirectoryIfNecessary(workingDirectory);
//        conf.setWorkingDirectory(workingDirectory);
//        conf.setComputeSteps(true);
//        conf.setCommitAfterEachFile(false);
//        conf.setImportDbMode(mode);
//        prepareInputFile(conf.getInput(), path);
//
//        EchoBaseUser user = new EchoBaseUserImpl();
//        user.setEmail("testUser@fake.fr");
//
//        importDbService.doImport(conf, user);
//    }

    protected void exportDb(Voyage voyage, String exportFilename) throws IOException {

        ExportDbConfiguration conf = new ExportDbConfiguration();
        conf.setVoyageIds(new String[]{voyage.getTopiaId()});

        File workingDirectory = new File(getTestDir(), "work-dir");
        FileUtil.createDirectoryIfNecessary(workingDirectory);
        conf.setWorkingDirectory(workingDirectory);
        conf.setFileName(exportFilename);
        conf.setComputeSteps(true);
        conf.setExportDbMode(ExportDbMode.ALL);
        exportDbService.doExport(conf);
    }

}
