/*
 * #%L
 * EchoBase :: Services
 * %%
 * Copyright (C) 2011 - 2012 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ifremer.echobase.services.service.importdata;

import com.google.common.collect.Iterables;
import fr.ifremer.echobase.entities.ImportType;
import fr.ifremer.echobase.entities.data.Echotype;
import fr.ifremer.echobase.services.ImportDataFixtures;
import fr.ifremer.echobase.services.service.importdata.configurations.MooringResultsImportConfiguration;

import java.io.IOException;

/**
 * @author Julien Ruchaud - ruchaud@codelutin.com
 * @since 4.0
 */
public class MooringResultsImportServiceIT extends AbstractImportDataServiceIT<MooringResultsImportConfiguration> {

    public MooringResultsImportServiceIT() {
        super(1);
    }

    @Override
    public ImportDataFixtures getImportDataFixture() {
        return ImportDataFixtures.IMPORT_DATA_ECHOBASE_MOORING;
    }

    protected String[] getImportPath(String filename) {
        return new String[]{"/import-data", "result", "mooring", filename};
    }

    @Override
    protected MooringResultsImportConfiguration createConfiguration() throws IOException {
        MooringResultsImportConfiguration configuration = new MooringResultsImportConfiguration(getLocale());
        configuration.setImportType(ImportType.RESULT_MOORING);
        configuration.setMooringId(getMooringId());
        prepareInputFile(configuration.getEchotypeFile(), getImportPath("echotype.csv.gz"));
        return configuration;
    }

    @Override
    protected ImportDataService.MooringResultsImportDataAction newAction() throws IOException {
        return new ImportDataService.MooringResultsImportDataAction();
    }

    @Override
    protected void assertAfertImport(ImportDataResult<MooringResultsImportConfiguration> result) throws IOException {
        int nbEchotype = fixtures.NB_ECHOTYPE();
        ImportDataFileResult importDataFileResult = Iterables.get(result.getImportResults(), 0);
        assertCsvImportResultPerEntity(importDataFileResult, Echotype.class, nbEchotype, 0, nbEchotype);
        assertCsvImportResult0(importDataFileResult, nbEchotype);
    }

}
