/*
 * #%L
 * EchoBase :: Services
 * %%
 * Copyright (C) 2011 - 2012 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ifremer.echobase.services.service.importdata;

import com.google.common.collect.Iterables;
import fr.ifremer.echobase.entities.ImportType;
import fr.ifremer.echobase.entities.data.Category;
import fr.ifremer.echobase.entities.data.Cell;
import fr.ifremer.echobase.entities.data.Data;
import fr.ifremer.echobase.entities.data.Result;
import fr.ifremer.echobase.services.ImportDataFixtures;
import fr.ifremer.echobase.services.service.importdata.configurations.VoyageResultsImportConfiguration;

import java.io.IOException;

/**
 * @author Tony Chemit - chemit@codelutin.com
 * @since 0.3
 */
public class VoyageResultsMapFishCellImportServiceIT extends VoyageResultsImportServiceITSupport {

    public VoyageResultsMapFishCellImportServiceIT() {
        super(1);
    }

    @Override
    public ImportDataFixtures getImportDataFixture() {
        return ImportDataFixtures.IMPORT_DATA_ECHOBASE_CATCHES_AND_VOYAGE_RESULT;
    }

    protected String[] getImportPath(String filename) {
        return new String[]{"/import-data", "result", "map", filename};
    }

    @Override
    protected VoyageResultsImportConfiguration createConfiguration() throws IOException {
        VoyageResultsImportConfiguration configuration = super.createConfiguration();
        configuration.setImportType(ImportType.RESULT_MAP_FISH);
        prepareInputFile(configuration.getMapsFile(), getImportPath("mapsFish.csv.gz"));
        return configuration;
    }

    @Override
    protected void assertAfertImport(ImportDataResult<VoyageResultsImportConfiguration> result) throws IOException {

        int nbCategory = 2;
        int nbCell = 381;
        int nbCellData = 2286;

        //381 rows imported
        //380 cells only (one cell with 2 categories -> 2 rows)
        //XXX bug du calcul de nombre de résultats ?
        ImportDataFileResult importDataFileResult = Iterables.get(result.getImportResults(), 0);
        assertCsvImportResultPerEntity(importDataFileResult, Cell.class, nbCell, 0, 380);
        assertCsvImportResultPerEntity(importDataFileResult, Data.class, nbCellData, 0, nbCellData);
        assertCsvImportResultPerEntity(importDataFileResult, Result.class, nbCell, 0, nbCell);
        assertCsvImportResultPerEntity(importDataFileResult, Category.class, nbCategory, 0, nbCategory);
        assertCsvImportResult0(importDataFileResult, nbCell);

    }

}
