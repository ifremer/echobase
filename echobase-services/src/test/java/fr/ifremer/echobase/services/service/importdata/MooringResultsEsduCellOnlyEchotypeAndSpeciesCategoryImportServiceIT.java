/*
 * #%L
 * EchoBase :: Services
 * %%
 * Copyright (C) 2011 - 2012 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ifremer.echobase.services.service.importdata;

import com.google.common.collect.Iterables;
import fr.ifremer.echobase.entities.ImportType;
import fr.ifremer.echobase.entities.data.Category;
import fr.ifremer.echobase.entities.data.Result;
import fr.ifremer.echobase.entities.references.SpeciesCategory;
import fr.ifremer.echobase.io.InputFile;
import fr.ifremer.echobase.services.ImportDataFixtures;
import fr.ifremer.echobase.services.csv.EchoBaseCsvUtil;
import fr.ifremer.echobase.services.service.importdata.configurations.MooringResultsImportConfiguration;

import java.io.IOException;

/**
 * @author Julien Ruchaud - ruchaud@codelutin.com
 * @since 4.0
 */
public class MooringResultsEsduCellOnlyEchotypeAndSpeciesCategoryImportServiceIT extends AbstractImportDataServiceIT<MooringResultsImportConfiguration> {

    public MooringResultsEsduCellOnlyEchotypeAndSpeciesCategoryImportServiceIT() {
        super(1);
    }

    @Override
    public ImportDataFixtures getImportDataFixture() {
        return ImportDataFixtures.IMPORT_DATA_ECHOBASE_MOORING_RESULTS;
    }

    protected String[] getImportPath(String filename) {
        return new String[]{"/import-data", "result", "mooring-esdu", filename};
    }

    @Override
    protected MooringResultsImportConfiguration createConfiguration() throws IOException {
        MooringResultsImportConfiguration configuration = new MooringResultsImportConfiguration(getLocale());
        configuration.setImportType(ImportType.RESULT_MOORING_ESDU);
        configuration.setMooringId(getMooringId());
        configuration.setResultLabel("resultLabel");

        InputFile inputFile = configuration.getEsduByEchotypeAndSpeciesCategoryFile();
        prepareInputFile(inputFile, getImportPath("byEchotypeAndSpeciesCategory_small.csv.gz"));

        addMooringMissingEsduCells(EchoBaseCsvUtil.CELL_NAME, configuration.getMooringId(), inputFile);
        configuration.setDataProcessingId(getDataProcessingId());
        
        return configuration;
    }

    @Override
    protected ImportDataService.MooringResultsImportDataAction newAction() throws IOException {
        return new ImportDataService.MooringResultsImportDataAction();
    }

    @Override
    protected void assertAfertImport(ImportDataResult<MooringResultsImportConfiguration> result) throws IOException {

        int nbCategory = 20;
        int nbSpeciesCategories = 1;
        int nbResult = 1584;

        ImportDataFileResult importDataFileResult = Iterables.get(result.getImportResults(), 0);
        assertCsvImportResultPerEntity(importDataFileResult, Result.class, nbResult, 0, nbResult);
        assertCsvImportResultPerEntity(importDataFileResult, Category.class, nbCategory, 0, nbCategory);
        assertCsvImportResultPerEntity(importDataFileResult, SpeciesCategory.class, nbSpeciesCategories, 0, 457);
        assertCsvImportResult0(importDataFileResult, nbResult);
    }

}
