/*
 * #%L
 * EchoBase :: Services
 * %%
 * Copyright (C) 2011 - 2012 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ifremer.echobase.services.service.importdata;

import com.google.common.collect.Iterables;
import fr.ifremer.echobase.entities.data.GearMetadataValue;
import fr.ifremer.echobase.entities.data.Operation;
import fr.ifremer.echobase.entities.data.OperationMetadataValue;
import fr.ifremer.echobase.services.ImportDataFixtures;
import fr.ifremer.echobase.services.service.importdata.configurations.VoyageOperationsImportConfiguration;

import java.io.IOException;

/**
 * @author Tony Chemit - chemit@codelutin.com
 * @since 0.3
 */
public class VoyageOperationImportServiceIT extends AbstractImportDataServiceIT<VoyageOperationsImportConfiguration> {

    public VoyageOperationImportServiceIT() {
        super(3);
    }

    @Override
    public ImportDataFixtures getImportDataFixture() {
        return ImportDataFixtures.IMPORT_DATA_ECHOBASE_COMMON_DATA;
    }

    @Override
    protected String[] getImportPath(String filename) {
        return new String[]{"/import-data", "operation", filename};
    }

    @Override
    protected VoyageOperationsImportConfiguration createConfiguration() throws IOException {
        VoyageOperationsImportConfiguration configuration = new VoyageOperationsImportConfiguration(getLocale());
        configuration.setVoyageId(getVoyageId());
        prepareInputFile(configuration.getOperationFile(), getImportPath("operation.csv.gz"));
        prepareInputFile(configuration.getOperationMetadataFile(), getImportPath("operationmetadatavalue.csv.gz"));
        prepareInputFile(configuration.getGearMetadataFile(), getImportPath("gearmetadatavalue.csv.gz"));
        return configuration;
    }

    @Override
    protected ImportDataService.VoyageOperationsImportDataAction newAction() throws IOException {
        return new ImportDataService.VoyageOperationsImportDataAction();
    }

    @Override
    protected void assertAfertImport(ImportDataResult<VoyageOperationsImportConfiguration> result) throws IOException {

        {
            ImportDataFileResult importDataFileResult = Iterables.get(result.getImportResults(), 0);
            int nbOperation = 68;
            assertCsvImportResultPerEntity(importDataFileResult, Operation.class, nbOperation, 0, nbOperation);
            assertCsvImportResult0(importDataFileResult, nbOperation);
        }
        {
            ImportDataFileResult importDataFileResult = Iterables.get(result.getImportResults(), 1);
            int nbOperationMetadatavalue = 204;
            assertCsvImportResultPerEntity(importDataFileResult, OperationMetadataValue.class, nbOperationMetadatavalue, 0, nbOperationMetadatavalue);
            assertCsvImportResult0(importDataFileResult, nbOperationMetadatavalue);
        }

        {
            ImportDataFileResult importDataFileResult = Iterables.get(result.getImportResults(), 2);
            int nbGearMetadatavalue = 476;
            assertCsvImportResultPerEntity(importDataFileResult, GearMetadataValue.class, nbGearMetadatavalue, 0, nbGearMetadatavalue);
            assertCsvImportResult0(importDataFileResult, nbGearMetadatavalue);
        }

    }

}
