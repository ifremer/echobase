package fr.ifremer.echobase.services.service.atlantos;

/*
 * #%L
 * EchoBase :: Services
 * %%
 * Copyright (C) 2011 - 2014 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.echobase.services.EchoBaseTestServiceSupport;
import fr.ifremer.echobase.services.FakeEchoBaseServiceContext;
import fr.ifremer.echobase.services.ImportDataFixtures;
import fr.ifremer.echobase.services.service.atlantos.xml.VocabularyExport;
import org.junit.Assert;
import org.junit.Test;
import org.nuiton.util.FileUtil;

import javax.inject.Inject;
import java.io.File;
import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Julien Ruchaud - ruchaud@codelutin.com
 * @since 4
 */
public class ExportAtlantosServiceTest extends EchoBaseTestServiceSupport {

    @Override
    protected FakeEchoBaseServiceContext initContext() {
        return new FakeEchoBaseServiceContext(
                ImportDataFixtures.EXPORT_DATA_ECHOBASE_ATLANTOS.getDbPath());
    }

    @Inject
    private ExportAtlantosService exportService;

    @Test
    public void testXmlExport() throws Exception {

        File workingDirectory = new File(getTestDir(), "testAtlantos");
        FileUtil.createDirectoryIfNecessary(workingDirectory);

        ExportAtlantosConfiguration model = new ExportAtlantosConfiguration();
        model.setVoyageId(getVoyageId());
        model.setVesselId(fixtures.VESSEL_ID());
        model.setWorkingDirectory(workingDirectory);

        exportService.doXmlExport(model);

        Assert.assertEquals(3, workingDirectory.list().length);

        boolean oneZipFile = Arrays.stream(workingDirectory.list())
                      .anyMatch(filename -> new File(workingDirectory, filename).isFile() &&
                                            filename.endsWith(".zip") && filename.startsWith("ICES_"));

        Assert.assertTrue(oneZipFile);

        boolean bioticFile = Arrays.stream(workingDirectory.list())
                      .anyMatch(filename -> new File(workingDirectory, filename).isFile() &&
                                            filename.endsWith(".xml") && filename.startsWith("Biotic_") &&
                                            !filename.endsWith("-voca.xml") && !filename.endsWith("-cruise.xml"));

        Assert.assertTrue(bioticFile);

        boolean acousticFile = Arrays.stream(workingDirectory.list())
                      .anyMatch(filename -> new File(workingDirectory, filename).isFile() &&
                                            filename.endsWith(".xml") && filename.startsWith("Acoustic_") &&
                                            !filename.endsWith("-voca.xml") && !filename.endsWith("-cruise.xml"));

        Assert.assertTrue(acousticFile);
    }
    
    @Test
    public void testPattern() {
        Pattern pattern = Pattern.compile(VocabularyExport.VOCABULARY_PATTERN);

        Matcher matcher = pattern.matcher("Gear_PMT_57x52");
        Assert.assertTrue(matcher.find());
        
        matcher = pattern.matcher("SHIPC_35HT");
        Assert.assertTrue(matcher.find());
        
        matcher = pattern.matcher("AC_SaCategory_D1-1");
        Assert.assertTrue(matcher.find());
    }
}
