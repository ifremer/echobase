/*
 * #%L
 * EchoBase :: Services
 * %%
 * Copyright (C) 2011 - 2012 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ifremer.echobase.services.service.importdata;

import com.google.common.collect.Iterables;
import fr.ifremer.echobase.entities.ImportType;
import fr.ifremer.echobase.entities.data.Transect;
import fr.ifremer.echobase.entities.data.Transit;
import fr.ifremer.echobase.entities.data.Voyage;
import fr.ifremer.echobase.services.ImportDataFixtures;
import fr.ifremer.echobase.services.service.importdata.configurations.VoyageCommonsImportConfiguration;

import java.io.IOException;

/**
 * @author Tony Chemit - chemit@codelutin.com
 * @since 0.3
 */
public class VoyageCommonsAllImportServiceIT extends VoyageCommonsImportServiceITSupport {

    public VoyageCommonsAllImportServiceIT() {
        super(3);
    }

    @Override
    public ImportDataFixtures getImportDataFixture() {
        return ImportDataFixtures.IMPORT_DATA_ECHOBASE_NO_DATA;
    }

    @Override
    protected VoyageCommonsImportConfiguration createConfiguration() throws IOException {

        VoyageCommonsImportConfiguration configuration = super.createConfiguration();
        configuration.setAreaOfOperationId(fixtures.AREA_OF_OPERATION_ID());
        configuration.setDatum(fixtures.DATUM());
        configuration.setMissionId(fixtures.MISSION_ID());
        configuration.setTransectBinUnitsPingAxis(fixtures.TRANSECT_BIN_UNITS_PING_AXIS());
        configuration.setTransectGeospatialVerticalPositive(fixtures.TRANSECT_GEOSPATIAL_VERTICLA_POSITIVE());
        configuration.setTransectLicence(fixtures.TRANSECT_LICENSE());
        configuration.setTransitRelatedActivity(fixtures.TRANSIT_RELATED_ACTIVITY());
        configuration.setVoyageDescription(fixtures.VOYAGE_DESCRIPTION());

        prepareInputFile(configuration.getVoyageFile(), getImportPath("voyage.csv.gz"));
        prepareInputFile(configuration.getTransitFile(), getImportPath("transit.csv.gz"));
        prepareInputFile(configuration.getTransectFile(), getImportPath("transect.csv.gz"));

        configuration.setImportType(ImportType.COMMON_ALL);
        return configuration;

    }

    @Override
    protected void assertAfertImport(ImportDataResult<VoyageCommonsImportConfiguration> result) throws IOException {

        int nbVoyage = fixtures.NB_VOYAGE();
        int nbTransit = fixtures.NB_TRANSIT();
        int nbTransect = fixtures.NB_TRANSECT();

        {

            ImportDataFileResult importDataFileResult = Iterables.get(result.getImportResults(), 0);
            assertCsvImportResultPerEntity(importDataFileResult, Voyage.class, nbVoyage, 0, nbVoyage);
            assertCsvImportResult0(importDataFileResult, nbVoyage);
        }
        {

            ImportDataFileResult importDataFileResult = Iterables.get(result.getImportResults(), 1);
            assertCsvImportResultPerEntity(importDataFileResult, Transit.class, nbTransit, 0, nbTransit);
            assertCsvImportResult0(importDataFileResult, nbTransit);
        }
        {

            ImportDataFileResult importDataFileResult = Iterables.get(result.getImportResults(), 2);
            assertCsvImportResultPerEntity(importDataFileResult, Transect.class, nbTransect, 0, nbTransect);
            assertCsvImportResult0(importDataFileResult, nbTransect);
        }

    }

}
