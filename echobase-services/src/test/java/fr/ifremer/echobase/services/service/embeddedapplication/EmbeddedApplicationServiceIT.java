/*
 * #%L
 * EchoBase :: Services
 * %%
 * Copyright (C) 2011 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ifremer.echobase.services.service.embeddedapplication;

import fr.ifremer.echobase.services.EchoBaseTestServiceSupport;
import fr.ifremer.echobase.services.FakeEchoBaseServiceContext;
import fr.ifremer.echobase.services.ImportDataFixtures;
import org.apache.commons.io.FileUtils;
import org.junit.Assert;
import org.junit.Test;
import org.nuiton.util.FileUtil;

import java.io.File;

/**
 * Test {@link EmbeddedApplicationService}.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 0.2
 */
public class EmbeddedApplicationServiceIT extends EchoBaseTestServiceSupport {

    protected FakeEchoBaseServiceContext initContext() {
        return new FakeEchoBaseServiceContext(ImportDataFixtures.IMPORT_DATA_ECHOBASE_COMMON_DATA.getDbPath());
    }

    @Test
    public void testCreateEmbeddedApplication() throws Exception {
        EmbeddedApplicationConfiguration conf = new EmbeddedApplicationConfiguration();
        conf.setVoyageIds(new String[]{getVoyageId()});

        File workingDirectory = new File(getTestDir(), "work-dir");
        FileUtil.createDirectoryIfNecessary(workingDirectory);
        conf.setWorkingDirectory(workingDirectory);
        File warLocation = new File(workingDirectory, "echobase.war");
        FileUtils.write(warLocation, "dummy!");
        conf.setWarLocation(warLocation);
        conf.setFileName("echobase");
        EmbeddedApplicationService service =
                newService(EmbeddedApplicationService.class);

        File zipFile = service.createEmbeddedApplication(conf);

        Assert.assertTrue(zipFile.exists());
        Assert.assertEquals(100f, conf.getProgress(), 1);

        // TODO Test that embedded db has exactly same number of data than in incoming db
    }
}
