package fr.ifremer.echobase.services.service.spatial;

/*
 * #%L
 * EchoBase :: Services
 * %%
 * Copyright (C) 2011 - 2014 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.echobase.entities.DriverType;
import fr.ifremer.echobase.entities.data.Voyage;
import fr.ifremer.echobase.entities.data.VoyageImpl;
import fr.ifremer.echobase.io.EchoBaseIOUtil;
import fr.ifremer.echobase.persistence.JdbcConfiguration;
import fr.ifremer.echobase.services.EchoBaseTestServiceSupport;
import fr.ifremer.echobase.services.FakeEchoBaseServiceContext;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;

import javax.inject.Inject;
import java.io.File;

/**
 * ADd an assume
 */
@Ignore
public class GisServiceTest extends EchoBaseTestServiceSupport {

    /** Logger. */
    private static final Log log = LogFactory.getLog(GisServiceTest.class);

    @Inject
    private GisService service;

    @Override
    protected FakeEchoBaseServiceContext initContext() {
        return new FakeEchoBaseServiceContext(null);
    }

    @Test
    public void testGenerateMapFile() throws Exception {

        JdbcConfiguration conf = JdbcConfiguration.newConfig(
                DriverType.POSTGRESQL,
                "jdbc:postgresql://localhost:5432/echobase-PELGAS-2011",
                "echobase",
                "Secret!");

        File qgisTemplate = getConfiguration().getQgisTemplateFile();

        Assert.assertFalse(qgisTemplate.exists());

        GisService.copyQgisDefaultTemplateFileIfNecessary(getConfiguration());
        GisService.copyQgisResourcesIfNecessary(getConfiguration());

        Assert.assertTrue(qgisTemplate.exists());

        Voyage voyage = new VoyageImpl();
        voyage.setName("PELGAS-2011");
        voyage.setTopiaId("fr.ifremer.echobase.entities.data.Voyage#1418197326119#0.08901075127744962");

        File repository = getConfiguration().getLizmapProjectsDirectory();

        EchoBaseIOUtil.forceMkdir(repository);

        String[] templateValues = service.getTemplateValues(conf, voyage);

        File theTaisteProject = service.generateFileFromTemplate(qgisTemplate, new File(repository, "TheTaisteProject.qgs"), templateValues);

        if (log.isInfoEnabled()) {
            log.info("Generated qgis projet file: " + theTaisteProject);
        }

        Assert.assertTrue(theTaisteProject.exists());

    }

}