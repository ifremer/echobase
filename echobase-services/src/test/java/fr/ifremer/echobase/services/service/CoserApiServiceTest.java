package fr.ifremer.echobase.services.service;

/*
 * #%L
 * EchoBase :: Services
 * %%
 * Copyright (C) 2011 - 2014 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.echobase.services.EchoBaseTestServiceSupport;
import fr.ifremer.echobase.services.FakeEchoBaseServiceContext;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.MapUtils;
import org.junit.Assert;
import org.junit.Assume;
import org.junit.Test;

import javax.inject.Inject;
import java.io.IOException;
import java.net.URL;
import java.util.Map;
import java.util.Set;

/**
 * Created on 3/23/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.5
 */
public class CoserApiServiceTest extends EchoBaseTestServiceSupport {

    @Inject
    private CoserApiService service;

    @Test
    public void testGetFacades() throws Exception {

        URL coserApiURL = serviceContext.getCoserApiURL();
        try {
            coserApiURL.openStream();
        } catch (IOException e) {
            Assume.assumeTrue("Could not find coser server: " + coserApiURL, false);
        }

        Map<String, String> facades = service.getFacades();
        Assert.assertTrue(MapUtils.isNotEmpty(facades));
    }

    @Test
    public void testGetZonesForFacade() throws Exception {
        URL coserApiURL = serviceContext.getCoserApiURL();
        try {
            coserApiURL.openStream();
        } catch (IOException e) {
            Assume.assumeTrue("Could not find coser server: " + coserApiURL, false);
        }

        Map<String, String> zones = service.getZonesForFacade("atlantique");
        Assert.assertTrue(MapUtils.isNotEmpty(zones));

        zones = service.getZonesForFacade("atlantique" + System.nanoTime());
        Assert.assertTrue(MapUtils.isEmpty(zones));
    }

    @Test
    public void testGetIndicators() throws Exception {

        URL coserApiURL = serviceContext.getCoserApiURL();
        try {
            coserApiURL.openStream();
        } catch (IOException e) {
            Assume.assumeTrue("Could not find coser server: " + coserApiURL, false);
        }

        Set<String> indicators = service.getIndicators();
        Assert.assertTrue(CollectionUtils.isNotEmpty(indicators));
    }

    @Override
    protected FakeEchoBaseServiceContext initContext() {
        return new FakeEchoBaseServiceContext(null);
    }
}
