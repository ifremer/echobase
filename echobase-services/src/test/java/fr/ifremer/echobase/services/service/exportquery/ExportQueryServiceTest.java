/*
 * #%L
 * EchoBase :: Services
 * %%
 * Copyright (C) 2011 - 2012 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ifremer.echobase.services.service.exportquery;

import fr.ifremer.echobase.services.EchoBaseTestServiceSupport;
import fr.ifremer.echobase.services.FakeEchoBaseServiceContext;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;

import javax.inject.Inject;

/**
 * To test {@link ExportQueryService}.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 0.3
 */
public class ExportQueryServiceTest extends EchoBaseTestServiceSupport {
    @Override
    protected FakeEchoBaseServiceContext initContext() {
        return new FakeEchoBaseServiceContext(null);
    }

    @Inject
    ExportQueryService exportQueryService;

    @Test
    public void processLibreOfficeSqlQuery() throws Exception {

        processLibreOfficeSqlQuery("select \"t\".name FROM \"t\" WHERE rien;",
                                   "select t.name FROM t WHERE rien;");

        processLibreOfficeSqlQuery("select \"t\".name, \"s\".name FROM \"t\", \"s\" WHERE rien;",
                                   "select t.name, s.name FROM t, s WHERE rien;");

        processLibreOfficeSqlQuery("select \"t\".name FROM \"t\" WHERE (select \"s\".name FROM \"s\" WHERE rien2);",
                                   "select t.name FROM t WHERE (select s.name FROM s WHERE rien2);");
    }

    protected void processLibreOfficeSqlQuery(String libreOfficeQuery,
                                              String expected) {

        String actual = exportQueryService.processLibreOfficeSqlQuery(libreOfficeQuery);
        Assert.assertEquals(expected, actual);
    }
}
