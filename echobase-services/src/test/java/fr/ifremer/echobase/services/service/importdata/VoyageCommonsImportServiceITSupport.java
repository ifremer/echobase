package fr.ifremer.echobase.services.service.importdata;

/*
 * #%L
 * EchoBase :: Services
 * %%
 * Copyright (C) 2011 - 2013 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.echobase.services.service.importdata.configurations.VoyageCommonsImportConfiguration;

import java.io.IOException;

/**
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.2
 */
public abstract class VoyageCommonsImportServiceITSupport extends AbstractImportDataServiceIT<VoyageCommonsImportConfiguration> {

    protected VoyageCommonsImportServiceITSupport(int nbFiles) {
        super(nbFiles);
    }

    @Override
    protected final String[] getImportPath(String filename) {
        return new String[]{"/import-data", "common", filename};
    }

    @Override
    protected final ImportDataService.VoyageCommonsImportDataAction newAction() throws IOException {
        return new ImportDataService.VoyageCommonsImportDataAction();
    }

    @Override
    protected VoyageCommonsImportConfiguration createConfiguration() throws IOException {
        return new VoyageCommonsImportConfiguration(getLocale());
    }

}
