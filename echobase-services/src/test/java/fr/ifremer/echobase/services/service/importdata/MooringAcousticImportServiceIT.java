/*
 * #%L
 * EchoBase :: Services
 * %%
 * Copyright (C) 2011 - 2012 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ifremer.echobase.services.service.importdata;

import com.google.common.collect.Iterables;
import fr.ifremer.echobase.entities.data.Cell;
import fr.ifremer.echobase.entities.data.Data;
import fr.ifremer.echobase.entities.data.DataAcquisition;
import fr.ifremer.echobase.entities.data.DataProcessing;
import fr.ifremer.echobase.entities.references.DataMetadataImpl;
import fr.ifremer.echobase.services.ImportDataFixtures;
import fr.ifremer.echobase.services.service.importdata.configurations.MooringAcousticsImportConfiguration;

import java.io.IOException;

/**
 * @author Julien Ruchaud - ruchaud@codelutin.com
 * @since 4.0
 */
public class MooringAcousticImportServiceIT extends AbstractImportDataServiceIT<MooringAcousticsImportConfiguration> {

    public MooringAcousticImportServiceIT() {
        super(1);
    }

    @Override
    public ImportDataFixtures getImportDataFixture() {
        return ImportDataFixtures.IMPORT_DATA_ECHOBASE_MOORING;
    }

    @Override
    protected String[] getImportPath(String filename) {
        return new String[]{"/import-data", "acoustic", filename};
    }

    @Override
    protected MooringAcousticsImportConfiguration createConfiguration() throws IOException {
        MooringAcousticsImportConfiguration conf = new MooringAcousticsImportConfiguration(getLocale());
        conf.setMooringId(getMooringId());
        conf.setAcousticDensityUnit("acousticDensityUnit");
        conf.setAcquisitionSoftwareVersionER60("acquisitionSoftwareVersionER60");
        conf.setAcquisitionSoftwareVersionME70("acquisitionSoftwareVersionME70");
        conf.setAddDataAcquisition(false);
        conf.setTransceiverAcquisitionAbsorptionDescription("transceiverAcquisitionAbsorptionDescription");
        conf.setCellPositionReference(CellPositionReference.START);
        conf.setDigitThreshold(1.5f);
        conf.setLoggedDataDatatype("loggedDataDatatype");
        conf.setLoggedDataFormat("loggedDataFormat");
        conf.setNotes("notes");
        conf.setPingDutyCycle("pingDutyCycle");
        conf.setProcessingDescription("processingDescription");
        conf.setProcessingTemplate("processingTemplate");
        conf.setSoundSpeedCalculationsER60("soundSpeedCalculationsER60");
        conf.setSoundSpeedCalculationsME70("soundSpeedCalculationsME70");
        conf.setTransceiverAcquisitionAbsorptionDescription("transceiverAcquisitionAbsorptionDescription");
        prepareInputFile(conf.getMoviesFile(), getImportPath("movies_small.csv.gz"));
        return conf;
    }

    @Override
    protected ImportDataService.MooringAcousticsImportDataAction newAction() throws IOException {
        return new ImportDataService.MooringAcousticsImportDataAction();
    }

    @Override
    protected void assertBeforeImport() {
        super.assertBeforeImport();

        addMissingDataMetadata(DataMetadataImpl.RADIAL_NUMBER);
    }

    @Override
    protected void assertAfertImport(ImportDataResult<MooringAcousticsImportConfiguration> result) throws IOException {

        int nbDataAcquisition = fixtures.NB_DATA_ACQUISITION();
        int nbDataProcessing = fixtures.NB_DATA_PROCESSING();
//        int nbCell = fixtures.NB_CELL();
//        int nbData = fixtures.NB_DATA();

        int nbCell = 289;
        int nbData = 3154;

        ImportDataFileResult importDataFileResult = Iterables.get(result.getImportResults(), 0);

        assertCsvImportResult0(importDataFileResult, nbDataAcquisition + nbDataProcessing + nbCell);

        assertCsvImportResultPerEntity(importDataFileResult, DataAcquisition.class, nbDataAcquisition, 0, nbDataAcquisition);
        assertCsvImportResultPerEntity(importDataFileResult, DataProcessing.class, nbDataProcessing, 0, nbDataProcessing);
        assertCsvImportResultPerEntity(importDataFileResult, Cell.class, nbCell, 0, nbCell);
        assertCsvImportResultPerEntity(importDataFileResult, Data.class, nbData, 0, nbData);

    }

}
