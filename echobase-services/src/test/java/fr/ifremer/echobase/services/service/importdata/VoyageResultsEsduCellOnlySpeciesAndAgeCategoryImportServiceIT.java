/*
 * #%L
 * EchoBase :: Services
 * %%
 * Copyright (C) 2011 - 2012 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ifremer.echobase.services.service.importdata;

import com.google.common.collect.Iterables;
import fr.ifremer.echobase.entities.ImportType;
import fr.ifremer.echobase.entities.data.Category;
import fr.ifremer.echobase.entities.data.Result;
import fr.ifremer.echobase.entities.references.AgeCategory;
import fr.ifremer.echobase.entities.references.SpeciesCategory;
import fr.ifremer.echobase.io.InputFile;
import fr.ifremer.echobase.services.ImportDataFixtures;
import fr.ifremer.echobase.services.csv.EchoBaseCsvUtil;
import fr.ifremer.echobase.services.service.importdata.configurations.VoyageResultsImportConfiguration;

import java.io.IOException;

/**
 * @author Tony Chemit - chemit@codelutin.com
 * @since 0.3
 */
public class VoyageResultsEsduCellOnlySpeciesAndAgeCategoryImportServiceIT extends VoyageResultsImportServiceITSupport {

    public VoyageResultsEsduCellOnlySpeciesAndAgeCategoryImportServiceIT() {
        super(1);
    }

    @Override
    public ImportDataFixtures getImportDataFixture() {
        return ImportDataFixtures.IMPORT_DATA_ECHOBASE_CATCHES_AND_VOYAGE_RESULT;
    }

    protected String[] getImportPath(String filename) {
        return new String[]{"/import-data", "result", "esdu", filename};
    }

    @Override
    protected VoyageResultsImportConfiguration createConfiguration() throws IOException {
        VoyageResultsImportConfiguration configuration = super.createConfiguration();
        configuration.setImportType(ImportType.RESULT_ESDU);

        InputFile inputFile = configuration.getEsduBySpeciesAndAgeCategoryFile();
        prepareInputFile(inputFile, getImportPath("bySpeciesAndAgeCategory_small.csv.gz"));

        addVoyageMissingEsduCells(EchoBaseCsvUtil.CELL_NAME, configuration.getVoyageId(), inputFile);
        configuration.setDataProcessingId(getDataProcessingId());
        return configuration;
    }

    @Override
    protected void assertAfertImport(ImportDataResult<VoyageResultsImportConfiguration> result) throws IOException {

        int nbResult = 149;
//        int nbResult = 10021;
        int nbCategory = 11;
//        int nbCategory = 14;
        int nbSpeciesCategories = 11;
//        int nbSpeciesCategories = 129;
        int nbAgeCategories = 4;
//        int nbAgeCategories = 6;

        ImportDataFileResult importDataFileResult = Iterables.get(result.getImportResults(), 0);
        assertCsvImportResultPerEntity(importDataFileResult, Result.class, nbResult, 0, nbResult);
        assertCsvImportResultPerEntity(importDataFileResult, Category.class, nbCategory, 0, nbCategory);
        assertCsvImportResultPerEntity(importDataFileResult, SpeciesCategory.class, nbSpeciesCategories, 0, 467);
//        assertCsvImportResultPerEntity(importDataFileResult, SpeciesCategory.class, nbSpeciesCategories, 0, 470);
        assertCsvImportResultPerEntity(importDataFileResult, AgeCategory.class, nbAgeCategories, 0, 8);
//        assertCsvImportResultPerEntity(importDataFileResult, AgeCategory.class, nbAgeCategories, 0, 10);
        assertCsvImportResult0(importDataFileResult, nbResult);

    }

}
