package fr.ifremer.echobase.services;

/*
 * #%L
 * EchoBase :: Services
 * %%
 * Copyright (C) 2011 - 2012 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.Sets;

import java.util.Set;

/**
 * Obtain some fixtures for service tests.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.2
 */
public class EchoBaseServiceFixtures {

    public String VESSEL_ID() {
        return "fr.ifremer.echobase.entities.references.Vessel#1323196672049#0.9790502711645855";
    }

    public String MISSION_ID() {
        return "fr.ifremer.echobase.entities.references.Mission#1323127544274#0.7939481378378231";
    }

    public String AREA_OF_OPERATION_ID() {
        return "fr.ifremer.echobase.entities.references.AreaOfOperation#1323128474277#0.011341599655098622";
    }

    public String importDbReferentialPath() {
        return "/echobase-importDb-referentiel.zip";
    }

    public String DATUM() {
        return "datum";
    }

    public int NB_VOYAGE() {
        return 1;
    }

    public int NB_MOORING() {
        return 7;
    }

    public int NB_MOORING_TOTAL() {
        return 1;
    }

    public int NB_ANCILLAY_INSTRUMENTATION() {
        return 3;
    }

    public int NB_MOORING_ANCILLAY_INSTRUMENTATION() {
        return 2;
    }

    public int NB_VOYAGE_ANCILLAY_INSTRUMENTATION() {
        return 66;
    }

    public int NB_TRANSIT() {
        return 3;
    }

    public int NB_TRANSECT() {
        return 251;
    }

    public int NB_OPERATION() {
        return 131;
    }

    public int NB_OPERATION_METADATAVALUE() {
        return 393;
    }

    public int NB_GEAR_METADATAVALUE() {
        return 917;
    }

    public int NB_SAMPLE_TOTAL() {
        return 1232;
    }

    public int NB_SAMPLE_DATA_TOTAL() {
        return 1834;
    }

    public int NB_SAMPLE_UNSORTED() {
        return 100;
    }

    public int NB_SAMPLE_DATA_UNSORTED() {
        return 200;
    }

    public int NB_SAMPLE_UNSORTED_ALL() {
        return 3952;
    }

    public int NB_SAMPLE_DATA_UNSORTED_ALL() { return 7839; }

    public int NB_SAMPLE_BIOMETRY() {
        return 15;
    }

    public int NB_SAMPLE_DATA_BIOMETRY() {
        return 86;
    }

    public int NB_SAMPLE() {
        return 1261;
    }

    public int NB_SAMPLE_ALL() {
        return NB_SAMPLE_TOTAL() + NB_SAMPLE_UNSORTED_ALL() + NB_SAMPLE_BIOMETRY();
    }

    public int NB_SAMPLE_DATA() {
        return NB_SAMPLE_DATA_TOTAL() + NB_SAMPLE_DATA_UNSORTED() + NB_SAMPLE_DATA_BIOMETRY();
    }

    public int NB_SAMPLE_DATA_ALL() {
        return NB_SAMPLE_DATA_TOTAL() + NB_SAMPLE_DATA_UNSORTED_ALL() + NB_SAMPLE_DATA_BIOMETRY();
    }

    public int NB_DATA_ACQUISITION() {
        return 1;
    }

    public int NB_DATA_PROCESSING() {
        return 1;
    }

    public int NB_CELL() {
        return 26668;
    }

    public int NB_DATA() {
        return 264478;
    }

    public int NB_LENGTH_AGE_KEY() {
        return 404;
    }

    public int NB_LENGTH_WEIGHT_KEY() {
        return 7;
    }

    public int NB_ECHOTYPE() {
        return 8;
    }

    public String TRANSECT_BIN_UNITS_PING_AXIS() {
        return "transectBinUnitsPingAxis";
    }

    public String TRANSIT_RELATED_ACTIVITY() {
        return "transitRelatedActivity";
    }

    public String VOYAGE_DESCRIPTION() {
        return "voyageDescription";
    }

    public String TRANSECT_GEOSPATIAL_VERTICLA_POSITIVE() {
        return "transectGeospatialVerticalPositive";
    }

    public String TRANSECT_LICENSE() {
        return "transectLicence";
    }

    public Set<String> COSER_INDICATORS() {
        return Sets.newHashSet(
                "EA",
                "Abundance",
                "Shannonmod",
                "propLW20",
                "L50",
                "ycg",
                "propLW25",
                "propL30",
                "Imin",
                "Imax",
                "Conserv",
                "meanWbar",
                "Btot",
                "l0.25",
                "Biomass",
                "microS",
                "Lvar",
                "Iso",
                "Lbar",
                "propLW15",
                "xcg",
                "propL20",
                "Ntot",
                "propL25",
                "biomSmall",
                "Gtot",
                "l0.75",
                "PropMat",
                "xaxe1.2",
                "Delta",
                "xaxe1.1",
                "yaxe1.2",
                "I",
                "yaxe2.2",
                "l0.95",
                "yaxe2.1",
                "Wtot",
                "xaxe2.1",
                "biomBig",
                "Wbcomm",
                "propL15",
                "lnN",
                "Npatch",
                "xaxe2.2",
                "sexRatio",
                "meanQuant0.95",
                "Dbar",
                "Lbcomm",
                "l0.05",
                "propLW30",
                "yaxe1.1",
                "Wbar",
                "PA",
                "SA");
    }
}
