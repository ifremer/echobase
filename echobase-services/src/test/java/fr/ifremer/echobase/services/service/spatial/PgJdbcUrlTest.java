package fr.ifremer.echobase.services.service.spatial;

/*
 * #%L
 * EchoBase :: Services
 * %%
 * Copyright (C) 2011 - 2015 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import org.junit.Assert;
import org.junit.Test;

public class PgJdbcUrlTest {

    @Test
    public void testGetPort() {

        {
            PgJdbcUrl jdbcUrl = new PgJdbcUrl("jdbc:postgresql://localhost/Echobase-test");
            Assert.assertEquals("5432", jdbcUrl.getPort());
        }
        {
            PgJdbcUrl jdbcUrl = new PgJdbcUrl("jdbc:postgresql://localhost:5433/Echobase-test");
            Assert.assertEquals("5433", jdbcUrl.getPort());

        }

    }

    @Test
    public void testGetHost() {

        {
            PgJdbcUrl jdbcUrl = new PgJdbcUrl("jdbc:postgresql://localhost/Echobase-test");
            Assert.assertEquals("localhost", jdbcUrl.getHost());
        }
        {
            PgJdbcUrl jdbcUrl = new PgJdbcUrl("jdbc:postgresql://localhost:5433/Echobase-test");
            Assert.assertEquals("localhost", jdbcUrl.getHost());

        }

    }

    @Test
    public void testGetDatabaseName() {

        {
            PgJdbcUrl jdbcUrl = new PgJdbcUrl("jdbc:postgresql://localhost/Echobase-test");
            Assert.assertEquals("Echobase-test", jdbcUrl.getDatabaseName());
        }
        {
            PgJdbcUrl jdbcUrl = new PgJdbcUrl("jdbc:postgresql://localhost:5433/Echobase-test");
            Assert.assertEquals("Echobase-test", jdbcUrl.getDatabaseName());

        }

    }
}