/*
 * #%L
 * EchoBase :: Services
 * %%
 * Copyright (C) 2011 - 2012 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ifremer.echobase.services.service.exportdb;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import fr.ifremer.echobase.entities.EchoBaseUserEntityEnum;
import fr.ifremer.echobase.entities.data.Category;
import fr.ifremer.echobase.entities.data.Cell;
import fr.ifremer.echobase.entities.data.Data;
import fr.ifremer.echobase.entities.data.DataAcquisition;
import fr.ifremer.echobase.entities.data.DataProcessing;
import fr.ifremer.echobase.entities.data.Result;
import fr.ifremer.echobase.entities.data.Transect;
import fr.ifremer.echobase.entities.data.Voyage;
import fr.ifremer.echobase.entities.references.AcousticInstrument;
import fr.ifremer.echobase.entities.references.CellType;
import fr.ifremer.echobase.entities.references.DataMetadata;
import fr.ifremer.echobase.entities.references.Species;
import fr.ifremer.echobase.entities.references.SpeciesCategory;
import fr.ifremer.echobase.io.EchoBaseIOUtil;
import fr.ifremer.echobase.services.EchoBaseTestServiceSupport;
import fr.ifremer.echobase.services.FakeEchoBaseServiceContext;
import fr.ifremer.echobase.services.ImportDataFixtures;
import fr.ifremer.echobase.services.service.UserDbPersistenceService;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Assert;
import org.junit.Test;
import org.nuiton.topia.persistence.TopiaDao;
import org.nuiton.topia.persistence.metadata.TableMeta;
import org.nuiton.topia.service.csv.in.TopiaCsvImports;
import org.nuiton.util.FileUtil;

import javax.inject.Inject;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

/**
 * Test {@link ExportDbService}.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 0.3
 */
public class ExportDbServiceTest extends EchoBaseTestServiceSupport {

    protected FakeEchoBaseServiceContext initContext() {
        return new FakeEchoBaseServiceContext(ImportDataFixtures.IMPORT_DATA_ECHOBASE_COMMON_DATA.getDbPath());
    }

    /** Logger. */
    private static final Log log = LogFactory.getLog(ExportDbServiceTest.class);

    @Inject
    private ExportDbService exportDbService;

    @Override
    public void setUp() throws Exception {
        super.setUp();

        TopiaDao<DataProcessing> dataProcessingDAO = serviceContext.getEchoBaseUserPersistenceContext().getDataProcessingDao();
        TopiaDao<DataAcquisition> dataAcquisitionDAO = serviceContext.getEchoBaseUserPersistenceContext().getDataAcquisitionDao();
        TopiaDao<Cell> cellDAO = serviceContext.getEchoBaseUserPersistenceContext().getCellDao();
        TopiaDao<Category> categoryDAO = serviceContext.getEchoBaseUserPersistenceContext().getCategoryDao();
        TopiaDao<Result> resultDAO = serviceContext.getEchoBaseUserPersistenceContext().getResultDao();
        TopiaDao<SpeciesCategory> speciesCategoryDAO = serviceContext.getEchoBaseUserPersistenceContext().getSpeciesCategoryDao();
        TopiaDao<Data> dataDAO = serviceContext.getEchoBaseUserPersistenceContext().getDataDao();

        UserDbPersistenceService persistenceService = serviceContext.newService(UserDbPersistenceService.class);

        Voyage voyage = persistenceService.getVoyage(getVoyageId());
        Assert.assertNotNull(voyage);

        Transect transect = voyage.getTransit().iterator().next().getTransect().iterator().next();
        Assert.assertNotNull(transect);

        List<AcousticInstrument> acousticInstruments = serviceContext.getEchoBaseUserPersistenceContext().getAcousticInstrumentDao().findAll();
        Assert.assertTrue(CollectionUtils.isNotEmpty(acousticInstruments));
        AcousticInstrument acousticInstrument = acousticInstruments.get(0);
        DataAcquisition dataAcquisition = dataAcquisitionDAO.create(
                DataAcquisition.PROPERTY_ACOUSTIC_INSTRUMENT, acousticInstrument
        );
        transect.addDataAcquisition(dataAcquisition);
        DataProcessing dataProcessing = dataProcessingDAO.create(
                DataProcessing.PROPERTY_ID, "id",
                DataProcessing.PROPERTY_PROCESSING_TEMPLATE, "processingtemplate"
        );
        dataAcquisition.addDataProcessing(dataProcessing);

        List<Species> speciesList = serviceContext.getEchoBaseUserPersistenceContext().getSpeciesDao().findAll();
        Assert.assertTrue(CollectionUtils.isNotEmpty(speciesList));
        Species species = speciesList.get(0);

        List<DataMetadata> dataMetadatas = serviceContext.getEchoBaseUserPersistenceContext().getDataMetadataDao().findAll();
        Assert.assertTrue(CollectionUtils.isNotEmpty(dataMetadatas));
        DataMetadata dataMetadata = dataMetadatas.get(0);

        SpeciesCategory speciesCategory = speciesCategoryDAO.create(
                SpeciesCategory.PROPERTY_SPECIES, species
        );

        Category category = categoryDAO.create(
                Category.PROPERTY_SPECIES_CATEGORY, speciesCategory
        );

        List<CellType> cellTypes = serviceContext.getEchoBaseUserPersistenceContext().getCellTypeDao().findAll();
        Assert.assertTrue(CollectionUtils.isNotEmpty(cellTypes));
        CellType cellType = cellTypes.get(0);

        Cell cell = cellDAO.create(
                Cell.PROPERTY_NAME, "cellName",
                Cell.PROPERTY_CELL_TYPE, cellType
        );
        if (log.isInfoEnabled()) {
            log.info("cell:" + cell.getTopiaId());
        }
        dataProcessing.addCell(cell);

        Data cellData = dataDAO.create(
                Data.PROPERTY_DATA_METADATA, dataMetadata,
                Data.PROPERTY_DATA_VALUE, "cellDataValue"
        );
        cell.addData(cellData);
        Result cellResult = resultDAO.create(
                Result.PROPERTY_CATEGORY, category,
                Result.PROPERTY_DATA_METADATA, dataMetadata,
                Result.PROPERTY_RESULT_LABEL, "cellResultLabel",
                Result.PROPERTY_RESULT_VALUE, "cellResultValue"
        );
        cell.addResult(cellResult);

        Cell childCell = cellDAO.create(
                Cell.PROPERTY_NAME, "childCell",
                Cell.PROPERTY_CELL_TYPE, cellType
        );
        cell.addChilds(childCell);

        Data childCellData = dataDAO.create(
                Data.PROPERTY_DATA_METADATA, dataMetadata,
                Data.PROPERTY_DATA_VALUE, "childCellDataValue"
        );
        childCell.addData(childCellData);

        Result childCellResult = resultDAO.create(
                Result.PROPERTY_CATEGORY, category,
                Result.PROPERTY_DATA_METADATA, dataMetadata,
                Result.PROPERTY_RESULT_LABEL, "childCellResultLabel",
                Result.PROPERTY_RESULT_VALUE, "childCellResultValue"
        );
        childCell.addResult(childCellResult);

        Cell postCell = cellDAO.create(
                Cell.PROPERTY_NAME, "postCellcellName",
                Cell.PROPERTY_CELL_TYPE, cellType
        );

        postCell.addChilds(cell);
        voyage.addPostCell(postCell);


        Data postCellData = dataDAO.create(
                Data.PROPERTY_DATA_METADATA, dataMetadata,
                Data.PROPERTY_DATA_VALUE, "postCellDataValue"
        );
        postCell.addData(postCellData);

        Result postCellResult = resultDAO.create(
                Result.PROPERTY_CATEGORY, category,
                Result.PROPERTY_DATA_METADATA, dataMetadata,
                Result.PROPERTY_RESULT_LABEL, "postCellResultLabel",
                Result.PROPERTY_RESULT_VALUE, "postCellResultValue"
        );
        postCell.addResult(postCellResult);

        serviceContext.getEchoBaseUserPersistenceContext().commit();
    }

    @Test
    public void exportReferential() throws IOException {

        ExportDbConfiguration conf = new ExportDbConfiguration();

        File workingDirectory = new File(getTestDir(), "work-dir");
        FileUtil.createDirectoryIfNecessary(workingDirectory);
        conf.setWorkingDirectory(workingDirectory);
        conf.setFileName("echobase-referential");
        conf.setComputeSteps(true);
        conf.setExportDbMode(ExportDbMode.REFERENTIAL);
        exportDbService.doExport(conf);
        File exportFile = conf.getExportFile();
        Assert.assertNotNull(exportFile);
        Assert.assertTrue(exportFile.exists());

        // check all tables (referential) where exported
        ZipFile zipFile = new ZipFile(exportFile);

        Map<TableMeta<EchoBaseUserEntityEnum>, ZipEntry> tables =
                TopiaCsvImports.discoverEntries(
                        "echobase/", getDbMeta().getReferenceTables(),
                        zipFile, Lists.<String>newArrayList());

        checkAllTablesExported(zipFile, tables);
    }

    @Test
    public void exportReferentialAndData() throws IOException {

        ExportDbConfiguration conf = new ExportDbConfiguration();

        conf.setVoyageIds(new String[]{getVoyageId()});

        File workingDirectory = new File(getTestDir(), "work-dir");
        FileUtil.createDirectoryIfNecessary(workingDirectory);
        conf.setWorkingDirectory(workingDirectory);
        conf.setFileName("echobase-referentialAndData");
        conf.setComputeSteps(true);
        conf.setExportDbMode(ExportDbMode.REFERENTIAL_AND_DATA);
        exportDbService.doExport(conf);
        File exportFile = conf.getExportFile();
        Assert.assertNotNull(exportFile);
        Assert.assertTrue(exportFile.exists());


        // check all tables (referential + data) where exported
        ZipFile zipFile = new ZipFile(exportFile);

        Map<TableMeta<EchoBaseUserEntityEnum>, ZipEntry> tables =
                TopiaCsvImports.discoverEntries(
                        "echobase/", getDbMeta().getAllTables(),
                        zipFile, Lists.<String>newArrayList());

        checkAllTablesExported(zipFile, tables);
    }

    @Test
    public void exportAll() throws IOException {

        ExportDbConfiguration conf = new ExportDbConfiguration();
        conf.setVoyageIds(new String[]{getVoyageId()});

        File workingDirectory = new File(getTestDir(), "work-dir");
        FileUtil.createDirectoryIfNecessary(workingDirectory);
        conf.setWorkingDirectory(workingDirectory);
        conf.setFileName("echobase-all");
        conf.setComputeSteps(true);
        conf.setExportDbMode(ExportDbMode.ALL);
        exportDbService.doExport(conf);
        File exportFile = conf.getExportFile();
        Assert.assertNotNull(exportFile);
        Assert.assertTrue(exportFile.exists());


        // check all tables (referential + data) where exported
        ZipFile zipFile = new ZipFile(exportFile);

        Map<TableMeta<EchoBaseUserEntityEnum>, ZipEntry> tables =
                TopiaCsvImports.discoverEntries(
                        "echobase/", getDbMeta().getAllTables(),
                        zipFile, Lists.<String>newArrayList());

        checkAllTablesExported(zipFile, tables);
    }

    private void checkAllTablesExported(ZipFile zipFile, Map<TableMeta<EchoBaseUserEntityEnum>, ZipEntry> tables) throws IOException {
        for (Map.Entry<TableMeta<EchoBaseUserEntityEnum>, ZipEntry> entry : tables.entrySet()) {
            TableMeta<EchoBaseUserEntityEnum> type = entry.getKey();
            if (type.getSource() == EchoBaseUserEntityEnum.Gear) {
                // there is some extra lines so can not count from here...
                //FIXME should use a csv import to read entries nstead
                continue;
            }
            ZipEntry zipEntry = entry.getValue();
            long nbCells = EchoBaseIOUtil.countLines(zipFile, Sets.newHashSet(zipEntry));
            Assert.assertEquals(1 + serviceContext.getEchoBaseUserPersistenceContext().getDao(type.getEntityType()).count(), nbCells);
        }
    }

}
