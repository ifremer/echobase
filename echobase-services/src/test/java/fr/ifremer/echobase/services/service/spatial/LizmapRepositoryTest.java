package fr.ifremer.echobase.services.service.spatial;

/*
 * #%L
 * EchoBase :: Services
 * %%
 * Copyright (C) 2011 - 2015 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.echobase.config.EchoBaseConfiguration;
import fr.ifremer.echobase.entities.DriverType;
import fr.ifremer.echobase.io.EchoBaseIOUtil;
import fr.ifremer.echobase.persistence.JdbcConfiguration;
import fr.ifremer.echobase.services.EchoBaseTestServiceSupport;
import fr.ifremer.echobase.services.FakeEchoBaseServiceContext;
import org.junit.Assert;
import org.junit.Test;

public class LizmapRepositoryTest extends EchoBaseTestServiceSupport {

    @Override
    protected FakeEchoBaseServiceContext initContext() {
        return new FakeEchoBaseServiceContext(null);
    }

    @Test
    public void testGetRepositoryName() throws Exception {

        EchoBaseConfiguration configuration = getConfiguration();
        EchoBaseIOUtil.forceMkdir(configuration.getLizmapProjectsDirectory());

        {

            JdbcConfiguration jdbcConfiguration = JdbcConfiguration.newConfig(DriverType.POSTGRESQL, "jdbc:postgresql://localhost/Echobase-test", "login", "password");
            LizmapRepository lizmapRepository = LizmapRepository.newLizmapRepository(configuration, jdbcConfiguration);
            String repositoryName = lizmapRepository.getRepositoryName();
            Assert.assertNotNull(repositoryName);
            Assert.assertEquals("echobaselocalhost5432Echobasetest", repositoryName);

        }

        {

            JdbcConfiguration jdbcConfiguration = JdbcConfiguration.newConfig(DriverType.POSTGRESQL, "jdbc:postgresql://localhost:5433/Echobase-test", "login", "password");
            LizmapRepository lizmapRepository = LizmapRepository.newLizmapRepository(configuration, jdbcConfiguration);
            String repositoryName = lizmapRepository.getRepositoryName();
            Assert.assertNotNull(repositoryName);
            Assert.assertEquals("echobaselocalhost5433Echobasetest", repositoryName);

        }

    }

}