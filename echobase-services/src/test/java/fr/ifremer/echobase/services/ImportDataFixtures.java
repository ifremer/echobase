package fr.ifremer.echobase.services;

/*
 * #%L
 * EchoBase :: Services
 * %%
 * Copyright (C) 2011 - 2016 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.echobase.entities.data.Cell;
import fr.ifremer.echobase.entities.data.Data;
import fr.ifremer.echobase.entities.data.DataAcquisition;
import fr.ifremer.echobase.entities.data.DataProcessing;
import fr.ifremer.echobase.entities.data.Echotype;
import fr.ifremer.echobase.entities.data.GearMetadataValue;
import fr.ifremer.echobase.entities.data.LengthAgeKey;
import fr.ifremer.echobase.entities.data.LengthWeightKey;
import fr.ifremer.echobase.entities.data.Mooring;
import fr.ifremer.echobase.entities.data.Operation;
import fr.ifremer.echobase.entities.data.OperationMetadataValue;
import fr.ifremer.echobase.entities.data.Result;
import fr.ifremer.echobase.entities.data.Sample;
import fr.ifremer.echobase.entities.data.SampleData;
import fr.ifremer.echobase.entities.data.Transect;
import fr.ifremer.echobase.entities.data.Transit;
import fr.ifremer.echobase.entities.data.Voyage;
import fr.ifremer.echobase.entities.references.AncillaryInstrumentation;

/**
 * Created on 07/04/16.
 *
 * @author Tony Chemit - chemit@codelutin.com
 */
public enum ImportDataFixtures {

    IMPORT_DATA_ECHOBASE_NO_DATA("/import-data/echobase-nodata.h2.db.gz") {
        @Override
        public void assertBeforeImportNotExisting(FakeEchoBaseServiceContext serviceContext, EchoBaseServiceFixtures fixtures) {
            serviceContext.assertNoEntities(Voyage.class, Transit.class, Transect.class);
            serviceContext.assertNoEntities(Operation.class, OperationMetadataValue.class, GearMetadataValue.class);
            serviceContext.assertNoEntities(Sample.class, SampleData.class);
            serviceContext.assertNoEntities(DataAcquisition.class, DataProcessing.class, Cell.class, Data.class, Result.class);
            serviceContext.assertNoEntities(LengthAgeKey.class, LengthWeightKey.class, Echotype.class);
            serviceContext.assertNoEntities(Mooring.class);
        }

        @Override
        public void assertBeforeImportExisting(FakeEchoBaseServiceContext serviceContext, EchoBaseServiceFixtures fixtures) {
            //no data
        }
    },
    IMPORT_DATA_ECHOBASE_COMMON_DATA("/import-data/echobase-commonData.h2.db.gz") {
        @Override
        public void assertBeforeImportNotExisting(FakeEchoBaseServiceContext serviceContext, EchoBaseServiceFixtures fixtures) {
            serviceContext.assertNoEntities(Operation.class, OperationMetadataValue.class, GearMetadataValue.class);
            serviceContext.assertNoEntities(Sample.class, SampleData.class);
            serviceContext.assertNoEntities(DataAcquisition.class, DataProcessing.class, Cell.class, Data.class, Result.class);
            serviceContext.assertNoEntities(LengthAgeKey.class, LengthWeightKey.class, Echotype.class);
        }

        @Override
        public void assertBeforeImportExisting(FakeEchoBaseServiceContext serviceContext, EchoBaseServiceFixtures fixtures) {
            serviceContext.assertNbEntities(Voyage.class, fixtures.NB_VOYAGE());
            serviceContext.assertNbEntities(Transit.class, fixtures.NB_TRANSIT());
            serviceContext.assertNbEntities(Transect.class, fixtures.NB_TRANSECT());
            serviceContext.assertNbEntities(AncillaryInstrumentation.class, fixtures.NB_ANCILLAY_INSTRUMENTATION());
        }
    },
    IMPORT_DATA_ECHOBASE_OPERATION("/import-data/echobase-operation.h2.db.gz") {
        @Override
        public void assertBeforeImportNotExisting(FakeEchoBaseServiceContext serviceContext, EchoBaseServiceFixtures fixtures) {
            serviceContext.assertNoEntities(Sample.class, SampleData.class);
            serviceContext.assertNoEntities(DataAcquisition.class, DataProcessing.class, Cell.class, Data.class, Result.class);
            serviceContext.assertNoEntities(LengthAgeKey.class, LengthWeightKey.class, Echotype.class);
        }

        @Override
        public void assertBeforeImportExisting(FakeEchoBaseServiceContext serviceContext, EchoBaseServiceFixtures fixtures) {
            IMPORT_DATA_ECHOBASE_COMMON_DATA.assertBeforeImportExisting(serviceContext, fixtures);
            serviceContext.assertNbEntities(Operation.class, fixtures.NB_OPERATION());
            serviceContext.assertNbEntities(OperationMetadataValue.class, fixtures.NB_OPERATION_METADATAVALUE());
            serviceContext.assertNbEntities(GearMetadataValue.class, fixtures.NB_GEAR_METADATAVALUE());
        }
    },
    IMPORT_DATA_ECHOBASE_OPERATION_TOTAL_SAMPLES("/import-data/echobase-operation-total-samples.h2.db.gz") {
        @Override
        public void assertBeforeImportNotExisting(FakeEchoBaseServiceContext serviceContext, EchoBaseServiceFixtures fixtures) {
            serviceContext.assertNoEntities(DataAcquisition.class, DataProcessing.class, Cell.class, Data.class, Result.class);
            serviceContext.assertNoEntities(LengthAgeKey.class, LengthWeightKey.class, Echotype.class);
        }

        @Override
        public void assertBeforeImportExisting(FakeEchoBaseServiceContext serviceContext, EchoBaseServiceFixtures fixtures) {
            IMPORT_DATA_ECHOBASE_OPERATION.assertBeforeImportExisting(serviceContext, fixtures);
            serviceContext.assertNbEntities(Sample.class, fixtures.NB_SAMPLE_TOTAL());
            serviceContext.assertNbEntities(SampleData.class, fixtures.NB_SAMPLE_DATA_TOTAL());
        }
    },
    IMPORT_DATA_ECHOBASE_CATCHES("/import-data/echobase-catches.h2.db.gz") {
        @Override
        public void assertBeforeImportNotExisting(FakeEchoBaseServiceContext serviceContext, EchoBaseServiceFixtures fixtures) {
            serviceContext.assertNoEntities(LengthAgeKey.class, LengthWeightKey.class, Echotype.class);
            serviceContext.assertNoEntities(DataAcquisition.class, DataProcessing.class, Data.class, Result.class, Cell.class);
        }

        @Override
        public void assertBeforeImportExisting(FakeEchoBaseServiceContext serviceContext, EchoBaseServiceFixtures fixtures) {
            IMPORT_DATA_ECHOBASE_OPERATION.assertBeforeImportExisting(serviceContext, fixtures);
            serviceContext.assertNbEntities(Sample.class, fixtures.NB_SAMPLE());
            serviceContext.assertNbEntities(SampleData.class, fixtures.NB_SAMPLE_DATA());
        }
    },
    IMPORT_DATA_ECHOBASE_CATCHES_AND_VOYAGE_RESULT("/import-data/echobase-catches-and-voyage-result.h2.db.gz") {
        @Override
        public void assertBeforeImportNotExisting(FakeEchoBaseServiceContext serviceContext, EchoBaseServiceFixtures fixtures) {
            serviceContext.assertNoEntities(DataAcquisition.class, DataProcessing.class, Data.class, Result.class, Cell.class);
            serviceContext.assertNoEntities(DataAcquisition.class, DataProcessing.class, Data.class, Result.class, Cell.class);
        }

        @Override
        public void assertBeforeImportExisting(FakeEchoBaseServiceContext serviceContext, EchoBaseServiceFixtures fixtures) {
            IMPORT_DATA_ECHOBASE_CATCHES.assertBeforeImportExisting(serviceContext, fixtures);
            serviceContext.assertNbEntities(LengthAgeKey.class, fixtures.NB_LENGTH_AGE_KEY());
            serviceContext.assertNbEntities(LengthWeightKey.class, fixtures.NB_LENGTH_WEIGHT_KEY());
            serviceContext.assertNbEntities(Echotype.class, fixtures.NB_ECHOTYPE());
        }
    },
    IMPORT_DATA_ECHOBASE_MOORING("/import-data/echobase-mooring.h2.db.gz") {
        @Override
        public void assertBeforeImportNotExisting(FakeEchoBaseServiceContext serviceContext, EchoBaseServiceFixtures fixtures) {
            serviceContext.assertNoEntities(LengthAgeKey.class, LengthWeightKey.class, Echotype.class);
            serviceContext.assertNoEntities(DataAcquisition.class, DataProcessing.class, Data.class, Result.class, Cell.class);
        }

        @Override
        public void assertBeforeImportExisting(FakeEchoBaseServiceContext serviceContext, EchoBaseServiceFixtures fixtures) {
            serviceContext.assertNbEntities(Mooring.class, fixtures.NB_MOORING_TOTAL());
            serviceContext.assertNbEntities(AncillaryInstrumentation.class, fixtures.NB_ANCILLAY_INSTRUMENTATION());
        }
    },
    IMPORT_DATA_ECHOBASE_MOORING_RESULTS("/import-data/echobase-mooring-results.h2.db.gz") {
        @Override
        public void assertBeforeImportNotExisting(FakeEchoBaseServiceContext serviceContext, EchoBaseServiceFixtures fixtures) {
            serviceContext.assertNoEntities(DataAcquisition.class, DataProcessing.class, Data.class, Result.class, Cell.class);
        }

        @Override
        public void assertBeforeImportExisting(FakeEchoBaseServiceContext serviceContext, EchoBaseServiceFixtures fixtures) {
            IMPORT_DATA_ECHOBASE_MOORING.assertBeforeImportExisting(serviceContext, fixtures);
            serviceContext.assertNbEntities(Echotype.class, fixtures.NB_ECHOTYPE());
        }
    },
    EXPORT_DATA_ECHOBASE_ATLANTOS("/import-data/echobase-atlantos.h2.db.gz") {
        @Override
        public void assertBeforeImportNotExisting(FakeEchoBaseServiceContext serviceContext, EchoBaseServiceFixtures fixtures) {
        }

        @Override
        public void assertBeforeImportExisting(FakeEchoBaseServiceContext serviceContext, EchoBaseServiceFixtures fixtures) {
            serviceContext.assertNbEntities(DataAcquisition.class, 1);
            serviceContext.assertNbEntities(DataProcessing.class, 1);
        }
    };


    private final String dbPath;

    ImportDataFixtures(String dbPath) {
        this.dbPath = dbPath;
    }

    public String getDbPath() {
        return dbPath;
    }

    public abstract void assertBeforeImportNotExisting(FakeEchoBaseServiceContext serviceContext, EchoBaseServiceFixtures fixtures);

    public abstract void assertBeforeImportExisting(FakeEchoBaseServiceContext serviceContext, EchoBaseServiceFixtures fixtures);
}
