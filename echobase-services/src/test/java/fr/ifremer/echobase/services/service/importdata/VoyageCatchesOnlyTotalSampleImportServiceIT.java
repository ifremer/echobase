/*
 * #%L
 * EchoBase :: Services
 * %%
 * Copyright (C) 2011 - 2012 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ifremer.echobase.services.service.importdata;

import com.google.common.collect.Iterables;
import fr.ifremer.echobase.entities.data.Sample;
import fr.ifremer.echobase.entities.data.SampleData;
import fr.ifremer.echobase.services.ImportDataFixtures;
import fr.ifremer.echobase.services.service.importdata.configurations.VoyageCatchesImportConfiguration;

import java.io.IOException;

/**
 * @author Tony Chemit - chemit@codelutin.com
 * @since 0.3
 */
public class VoyageCatchesOnlyTotalSampleImportServiceIT extends VoyageCatchesImportServiceITSupport {

    public VoyageCatchesOnlyTotalSampleImportServiceIT() {
        super(1);
    }

    @Override
    public ImportDataFixtures getImportDataFixture() {
        return ImportDataFixtures.IMPORT_DATA_ECHOBASE_OPERATION;
    }

    @Override
    protected VoyageCatchesImportConfiguration createConfiguration() throws IOException {
        VoyageCatchesImportConfiguration configuration = super.createConfiguration();
        prepareInputFile(configuration.getTotalSampleFile(), getImportPath("totalsample.csv.gz"));
        return configuration;
    }

    @Override
    protected void assertAfertImport(ImportDataResult<VoyageCatchesImportConfiguration> result) throws IOException {
        
        ImportDataFileResult importDataFileResult = Iterables.get(result.getImportResults(), 0);

        int nbSamples = fixtures.NB_SAMPLE_TOTAL();
        int nbSampleData = fixtures.NB_SAMPLE_DATA_TOTAL();

        assertCsvImportResultPerEntity(importDataFileResult, Sample.class, nbSamples, 0, nbSamples);
        assertCsvImportResultPerEntity(importDataFileResult, SampleData.class, nbSampleData, 0, nbSampleData);
        assertCsvImportResult0(importDataFileResult, nbSamples);

    }

}
