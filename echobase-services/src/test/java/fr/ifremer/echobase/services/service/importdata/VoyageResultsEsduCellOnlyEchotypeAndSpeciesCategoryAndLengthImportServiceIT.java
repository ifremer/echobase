/*
 * #%L
 * EchoBase :: Services
 * %%
 * Copyright (C) 2011 - 2012 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ifremer.echobase.services.service.importdata;

import com.google.common.collect.Iterables;
import fr.ifremer.echobase.entities.ImportType;
import fr.ifremer.echobase.entities.data.Category;
import fr.ifremer.echobase.entities.data.Result;
import fr.ifremer.echobase.entities.references.SizeCategory;
import fr.ifremer.echobase.entities.references.SpeciesCategory;
import fr.ifremer.echobase.io.InputFile;
import fr.ifremer.echobase.services.ImportDataFixtures;
import fr.ifremer.echobase.services.csv.EchoBaseCsvUtil;
import fr.ifremer.echobase.services.service.importdata.configurations.VoyageResultsImportConfiguration;
import org.junit.Ignore;

import java.io.IOException;

/**
 * @author Tony Chemit - chemit@codelutin.com
 * @since 0.3
 */
@Ignore
public class VoyageResultsEsduCellOnlyEchotypeAndSpeciesCategoryAndLengthImportServiceIT extends VoyageResultsImportServiceITSupport {

    public VoyageResultsEsduCellOnlyEchotypeAndSpeciesCategoryAndLengthImportServiceIT() {
        super(1);
    }

    @Override
    public ImportDataFixtures getImportDataFixture() {
        return ImportDataFixtures.IMPORT_DATA_ECHOBASE_CATCHES_AND_VOYAGE_RESULT;
    }

    protected String[] getImportPath(String filename) {
        return new String[]{"/import-data", "result", "esdu", filename};
    }

    @Override
    protected VoyageResultsImportConfiguration createConfiguration() throws IOException {
        VoyageResultsImportConfiguration configuration = super.createConfiguration();
        configuration.setImportType(ImportType.RESULT_ESDU);

        InputFile inputFile = configuration.getEsduByEchotypeAndSpeciesCategoryAndLengthFile();
        prepareInputFile(inputFile, getImportPath("byEchotypeAndSpeciesCategoryAndLength.csv.gz"));

        addVoyageMissingEsduCells(EchoBaseCsvUtil.CELL_NAME, configuration.getVoyageId(), inputFile);
        configuration.setDataProcessingId(getDataProcessingId());
        return configuration;
    }

    @Override
    protected void assertAfertImport(ImportDataResult<VoyageResultsImportConfiguration> result) throws IOException {

        int nbCategory = 129;
        int nbSpeciesCategories= 129;
        int nbSizeCategories= 62;
        int nbResult = 68108;

        ImportDataFileResult importDataFileResult = Iterables.get(result.getImportResults(), 0);
        assertCsvImportResult0(importDataFileResult, nbResult);
        assertCsvImportResultPerEntity(importDataFileResult, Result.class, nbResult, 0, nbResult);
        assertCsvImportResultPerEntity(importDataFileResult, Category.class, nbCategory, 0, nbCategory);
        assertCsvImportResultPerEntity(importDataFileResult, SpeciesCategory.class, nbSpeciesCategories, 0, 585);
        assertCsvImportResultPerEntity(importDataFileResult, SizeCategory.class, nbSizeCategories, 0, 69);

    }

}
