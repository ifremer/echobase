/*
 * #%L
 * EchoBase :: Services
 * %%
 * Copyright (C) 2011 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ifremer.echobase.services.service;

import fr.ifremer.echobase.entities.EchoBaseUserEntityEnum;
import fr.ifremer.echobase.services.EchoBaseTestServiceSupport;
import fr.ifremer.echobase.services.FakeEchoBaseServiceContext;
import org.junit.Assert;
import org.junit.Test;
import org.nuiton.decorator.Decorator;

import javax.inject.Inject;
import java.util.List;

/**
 * To test the {@link DecoratorService}.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 0.2
 */
public class DecoratorServiceTest extends EchoBaseTestServiceSupport {

    @Inject
    private DecoratorService decoratorService;

    @Override
    protected FakeEchoBaseServiceContext initContext() {
        return new FakeEchoBaseServiceContext(null);
    }

    @Test
    public void getDecoratorForReferences() throws Exception {
        List<EchoBaseUserEntityEnum> expectedTypes = serviceContext.getDbMeta().getReferenceTypes();

        for (EchoBaseUserEntityEnum type : expectedTypes) {
            Decorator<?> decorator =
                    decoratorService.getDecorator(type.getContract(), null);
            Assert.assertNotNull("Missing decorator for type : " + type, decorator);
        }
    }

    @Test
    public void getDecoratorForData() throws Exception {
        List<EchoBaseUserEntityEnum> expectedTypes =
                serviceContext.getDbMeta().getDataTypes();

        for (EchoBaseUserEntityEnum type : expectedTypes) {
            Decorator<?> decorator =
                    decoratorService.getDecorator(type.getContract(), null);
            Assert.assertNotNull("Missing decorator for type : " + type, decorator);
        }
    }

}
