/*
 * #%L
 * EchoBase :: Services
 * %%
 * Copyright (C) 2011 - 2012 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ifremer.echobase.services.service.importdata;

import com.google.common.collect.Iterables;
import fr.ifremer.echobase.entities.ImportType;
import fr.ifremer.echobase.entities.data.Category;
import fr.ifremer.echobase.entities.data.Cell;
import fr.ifremer.echobase.entities.data.Data;
import fr.ifremer.echobase.entities.data.Result;
import fr.ifremer.echobase.entities.references.SpeciesCategory;
import fr.ifremer.echobase.services.ImportDataFixtures;
import fr.ifremer.echobase.services.service.importdata.configurations.VoyageResultsImportConfiguration;

import java.io.IOException;

/**
 * @author Tony Chemit - chemit@codelutin.com
 * @since 0.3
 */
public class VoyageResultsRegionCell_AllImportServiceIT extends VoyageResultsImportServiceITSupport {

    public VoyageResultsRegionCell_AllImportServiceIT() {
        super(3);
    }

    @Override
    public ImportDataFixtures getImportDataFixture() {
        return ImportDataFixtures.IMPORT_DATA_ECHOBASE_CATCHES_AND_VOYAGE_RESULT;
    }

    protected String[] getImportPath(String filename) {
        return new String[]{"/import-data", "result", "region", filename};
    }

    @Override
    protected VoyageResultsImportConfiguration createConfiguration() throws IOException {
        VoyageResultsImportConfiguration configuration = super.createConfiguration();
        configuration.setImportType(ImportType.RESULT_REGION);
        prepareInputFile(configuration.getRegionsFile(), getImportPath("regions.csv.gz"));
        prepareInputFile(configuration.getRegionAssociationFile(), getImportPath("regionAssociations.csv.gz"));
        prepareInputFile(configuration.getRegionResultFile(), getImportPath("regionResults.csv.gz"));

        addVoyageMissingEsduCells("esduName", configuration.getVoyageId(), configuration.getRegionAssociationFile());

        fakeServiceContext.assertNbEntities(Cell.class, NB_ESDU_CELLS);

        return configuration;
    }

        public static final int NB_ESDU_CELLS = 2073;
//    public static final int NB_ESDU_CELLS = 22;

    @Override
    protected void assertAfertImport(ImportDataResult<VoyageResultsImportConfiguration> result) throws IOException {

        int nbRegionCell = 10;
        int nbRegionCellData = 238;

//        //FIXME Comprendre pourquoi toutes les cellules esdu sont dans 2 régions différentes ?
        int nbUpdatedEsduCell = 2 * NB_ESDU_CELLS;
//        int nbUpdatedEsduCell = NB_ESDU_CELLS;
        int nbRegionCellResult = 2128/*1873*/;

        int nbCell = NB_ESDU_CELLS + nbRegionCell;

        {
            ImportDataFileResult importDataFileResult = Iterables.get(result.getImportResults(), 0);
            assertCsvImportResultPerEntity(importDataFileResult, Cell.class, nbRegionCell, 0, nbCell);
            assertCsvImportResultPerEntity(importDataFileResult, Data.class, nbRegionCellData, 0, nbRegionCellData);
            assertCsvImportResult0(importDataFileResult, nbRegionCell + nbRegionCellData);
        }

        {
            ImportDataFileResult importDataFileResult = Iterables.get(result.getImportResults(), 1);
            assertCsvImportResult0(importDataFileResult, 0);
            assertCsvImportResultPerEntity(importDataFileResult, Cell.class, 0, nbUpdatedEsduCell, nbCell);
        }

        {
            ImportDataFileResult importDataFileResult = Iterables.get(result.getImportResults(), 2);
            assertCsvImportResult0(importDataFileResult, nbRegionCellResult);
            assertCsvImportResultPerEntity(importDataFileResult, Result.class, nbRegionCellResult, 0, nbRegionCellResult);
            assertCsvImportResultPerEntity(importDataFileResult, Category.class, 27, 0, 27);
            assertCsvImportResultPerEntity(importDataFileResult, SpeciesCategory.class, 1, 0, 457);
        }

    }

}
