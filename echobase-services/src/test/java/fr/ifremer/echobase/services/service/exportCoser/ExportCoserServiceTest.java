package fr.ifremer.echobase.services.service.exportCoser;

/*
 * #%L
 * EchoBase :: Services
 * %%
 * Copyright (C) 2011 - 2014 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.Iterables;
import com.google.common.collect.Sets;
import fr.ifremer.coser.bean.EchoBaseProject;
import fr.ifremer.echobase.entities.references.DataMetadata;
import fr.ifremer.echobase.entities.references.Mission;
import fr.ifremer.echobase.services.EchoBaseTestServiceSupport;
import fr.ifremer.echobase.services.FakeEchoBaseServiceContext;
import fr.ifremer.echobase.services.ImportDataFixtures;
import fr.ifremer.echobase.services.service.UserDbPersistenceService;
import org.junit.Assert;
import org.junit.Test;
import org.nuiton.topia.persistence.TopiaEntities;
import org.nuiton.util.FileUtil;

import javax.inject.Inject;
import java.io.File;
import java.util.List;
import java.util.Set;

/**
 * Created on 4/5/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.6
 */
public class ExportCoserServiceTest extends EchoBaseTestServiceSupport {

    protected FakeEchoBaseServiceContext initContext() {
        return new FakeEchoBaseServiceContext(
                ImportDataFixtures.IMPORT_DATA_ECHOBASE_CATCHES_AND_VOYAGE_RESULT.getDbPath());
    }

    @Inject
    private ExportCoserService exportService;

    @Inject
    private UserDbPersistenceService persistenceService;

    @Test
    public void testExtractRawData() throws Exception {

        File workingDirectory = new File(getTestDir(), "testExtractRawData");
        FileUtil.createDirectoryIfNecessary(workingDirectory);

        EchoBaseProject echobaseProject = new EchoBaseProject(workingDirectory);

        ExportCoserConfiguration model = new ExportCoserConfiguration();
        model.setMissionId(fixtures.MISSION_ID());

        Mission mission = persistenceService.getMission(model.getMissionId());

        Assert.assertFalse(echobaseProject.getRawDataDirectory().exists());
        Assert.assertFalse(new File(echobaseProject.getRawDataDirectory(), "Captures.csv").exists());
        Assert.assertFalse(new File(echobaseProject.getRawDataDirectory(), "Tailles.csv").exists());
        Assert.assertFalse(new File(echobaseProject.getRawDataDirectory(), "Strates.csv").exists());

        exportService.extractRawData(echobaseProject, mission, model);

        Assert.assertTrue(echobaseProject.getRawDataDirectory().exists());
        Assert.assertTrue(new File(echobaseProject.getRawDataDirectory(), "Captures.csv").exists());
        Assert.assertTrue(new File(echobaseProject.getRawDataDirectory(), "Tailles.csv").exists());
        Assert.assertTrue(new File(echobaseProject.getRawDataDirectory(), "Strates.csv").exists());
    }

    @Test
    public void testExtractSpeciesFile() throws Exception {

        File workingDirectory = new File(getTestDir(), "testExtractSpeciesFile");
        FileUtil.createDirectoryIfNecessary(workingDirectory);

        EchoBaseProject echobaseProject = new EchoBaseProject(workingDirectory);

        ExportCoserConfiguration model = new ExportCoserConfiguration();
        model.setMissionId(fixtures.MISSION_ID());

        Assert.assertFalse(echobaseProject.getSpeciesDefinitionFile().exists());

        exportService.extractSpeciesFile(echobaseProject);

        Assert.assertTrue(echobaseProject.getSpeciesDefinitionFile().exists());
    }

    @Test
    public void testExtractPopulationIndicators() throws Exception {

        Set<String> indicatorNames = fixtures.COSER_INDICATORS();
        List<DataMetadata> dataMetadatas = persistenceService.getDataMetadatasInName(indicatorNames);
        Set<String> indicatorIds = Sets.newHashSet(Iterables.transform(dataMetadatas, TopiaEntities.getTopiaIdFunction()));

        File workingDirectory = new File(getTestDir(), "testExtractCommunityIndicators");
        FileUtil.createDirectoryIfNecessary(workingDirectory);

        ExportCoserConfiguration model = new ExportCoserConfiguration();
        model.setMissionId(fixtures.MISSION_ID());
        model.setPopulationIndicator(indicatorIds);

        EchoBaseProject echobaseProject = new EchoBaseProject(workingDirectory);

        Mission mission = persistenceService.getMission(model.getMissionId());

        Assert.assertFalse(echobaseProject.getPopulationIndicatorsFile().exists());

        exportService.extractPopulationIndicators(model, echobaseProject, mission);

        Assert.assertTrue(echobaseProject.getPopulationIndicatorsFile().exists());
    }

    @Test
    public void testExtractCommunityIndicators() throws Exception {

        Set<String> indicatorNames = fixtures.COSER_INDICATORS();
        List<DataMetadata> dataMetadatas = persistenceService.getDataMetadatasInName(indicatorNames);
        Set<String> indicatorIds = Sets.newHashSet(Iterables.transform(dataMetadatas, TopiaEntities.getTopiaIdFunction()));

        File workingDirectory = new File(getTestDir(), "testExtractCommunityIndicators");
        FileUtil.createDirectoryIfNecessary(workingDirectory);

        ExportCoserConfiguration model = new ExportCoserConfiguration();
        model.setMissionId(fixtures.MISSION_ID());
        model.setCommunityIndicator(indicatorIds);

        EchoBaseProject echobaseProject = new EchoBaseProject(workingDirectory);

        Mission mission = persistenceService.getMission(model.getMissionId());

        Assert.assertFalse(echobaseProject.getCommunityIndicatorsFile().exists());

        exportService.extractCommunityIndicators(model, echobaseProject, mission);

        Assert.assertTrue(echobaseProject.getCommunityIndicatorsFile().exists());
    }
}
