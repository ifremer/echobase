/*
 * #%L
 * EchoBase :: Services
 * %%
 * Copyright (C) 2011 - 2012 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ifremer.echobase.services.service.importdata;

import com.google.common.collect.Iterables;
import fr.ifremer.echobase.entities.data.Sample;
import fr.ifremer.echobase.entities.data.SampleData;
import fr.ifremer.echobase.entities.references.SampleDataTypeImpl;
import fr.ifremer.echobase.services.ImportDataFixtures;
import fr.ifremer.echobase.services.service.importdata.configurations.VoyageCatchesImportConfiguration;

import java.io.IOException;

/**
 * @author Tony Chemit - chemit@codelutin.com
 * @since 0.3
 */
public class VoyageCatchesAllImportServiceIT extends VoyageCatchesImportServiceITSupport {

    public VoyageCatchesAllImportServiceIT() {
        super(3);
    }

    @Override
    public ImportDataFixtures getImportDataFixture() {
        return ImportDataFixtures.IMPORT_DATA_ECHOBASE_OPERATION;
    }

    @Override
    protected VoyageCatchesImportConfiguration createConfiguration() throws IOException {
        VoyageCatchesImportConfiguration configuration = super.createConfiguration();
        prepareInputFile(configuration.getTotalSampleFile(), getImportPath("totalsample.csv.gz"));
        prepareInputFile(configuration.getSubSampleFile(), getImportPath("subsample_all.csv.gz"));
        prepareInputFile(configuration.getBiometrySampleFile(), getImportPath("biometrysample.csv.gz"));
        return configuration;
    }

    @Override
    protected void assertBeforeImport() {
        super.assertBeforeImport();

        addMissingSampleDataType(SampleDataTypeImpl.SPECIMEN_INDEX);
    }

    @Override
    protected void assertAfertImport(ImportDataResult<VoyageCatchesImportConfiguration> result) throws IOException {

        int nbSample = fixtures.NB_SAMPLE_ALL();
        int nbSampleData = fixtures.NB_SAMPLE_DATA_ALL();

        int nbSampleTotal = fixtures.NB_SAMPLE_TOTAL();
        int nbSampleDataTotal = fixtures.NB_SAMPLE_DATA_TOTAL();

        int nbSampleUnsorted = fixtures.NB_SAMPLE_UNSORTED_ALL();
        int nbSampleDataUnsorted = fixtures.NB_SAMPLE_DATA_UNSORTED_ALL();

        int nbSampleBiometry = fixtures.NB_SAMPLE_BIOMETRY();
        int nbSampleDataBiometry = fixtures.NB_SAMPLE_DATA_BIOMETRY();

        {
            ImportDataFileResult importDataFileResult = Iterables.get(result.getImportResults(), 0);
            assertCsvImportResult0(importDataFileResult, nbSampleTotal);

            assertCsvImportResultPerEntity(importDataFileResult, Sample.class, nbSampleTotal, 0, nbSample);
            assertCsvImportResultPerEntity(importDataFileResult, SampleData.class, nbSampleDataTotal, 0, nbSampleData);
        }
        {
            ImportDataFileResult importDataFileResult = Iterables.get(result.getImportResults(), 1);
            assertCsvImportResult0(importDataFileResult, nbSampleUnsorted + nbSampleDataUnsorted);
            assertCsvImportResultPerEntity(importDataFileResult, Sample.class, nbSampleUnsorted, 0, nbSample);
            assertCsvImportResultPerEntity(importDataFileResult, SampleData.class, nbSampleDataUnsorted, 0, nbSampleData);
        }

        {
            ImportDataFileResult importDataFileResult = Iterables.get(result.getImportResults(), 2);
            assertCsvImportResult0(importDataFileResult, nbSampleBiometry + nbSampleDataBiometry);
            assertCsvImportResultPerEntity(importDataFileResult, Sample.class, nbSampleBiometry, 0, nbSample);
            assertCsvImportResultPerEntity(importDataFileResult, SampleData.class, nbSampleDataBiometry, 0, nbSampleData);

        }

    }

}
