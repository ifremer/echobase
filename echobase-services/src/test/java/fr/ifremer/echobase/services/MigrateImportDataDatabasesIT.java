/*
 * #%L
 * EchoBase :: Services
 * %%
 * Copyright (C) 2011 - 2012 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ifremer.echobase.services;

import com.google.common.collect.ImmutableMap;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.Description;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Map;
import java.util.zip.GZIPOutputStream;

/**
 * To upgrade all import databases (in src/test/resources/import-data).
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 4.0
 */
@Ignore
public class MigrateImportDataDatabasesIT extends EchoBaseServiceSupport {

    /** Logger. */
    private static final Log log = LogFactory.getLog(MigrateImportDataDatabasesIT.class);

    private static final Map<String, String> NAME_TO_DATABASE = ImmutableMap
            .<String, String>builder()
            .put("noData", ImportDataFixtures.IMPORT_DATA_ECHOBASE_NO_DATA.getDbPath())
            .put("commonData", ImportDataFixtures.IMPORT_DATA_ECHOBASE_COMMON_DATA.getDbPath())
            .put("operation", ImportDataFixtures.IMPORT_DATA_ECHOBASE_OPERATION.getDbPath())
            .put("operationWithTotalSamples", ImportDataFixtures.IMPORT_DATA_ECHOBASE_OPERATION_TOTAL_SAMPLES.getDbPath())
            .put("catches", ImportDataFixtures.IMPORT_DATA_ECHOBASE_CATCHES.getDbPath())
            .put("catchesWithVoyageResult", ImportDataFixtures.IMPORT_DATA_ECHOBASE_CATCHES_AND_VOYAGE_RESULT.getDbPath())
            .put("mooring", ImportDataFixtures.IMPORT_DATA_ECHOBASE_MOORING.getDbPath())
            .put("mooringResults", ImportDataFixtures.IMPORT_DATA_ECHOBASE_MOORING_RESULTS.getDbPath())
            .put("exportAtlantos", ImportDataFixtures.EXPORT_DATA_ECHOBASE_ATLANTOS.getDbPath())
            .build();

    @Rule
    public FakeEchoBaseServiceContext fakeServiceContext = new FakeEchoBaseServiceContext(null) {

        @Override
        protected void starting(Description description) {
            String methodName = description.getMethodName();
            String databaseLocation = NAME_TO_DATABASE.get(methodName);
            setInitDb(databaseLocation);
            super.starting(description);
        }
    };

    @Before
    public void setUp() throws Exception {

        this.serviceContext = fakeServiceContext;

    }

    @After
    public void tearDown() throws Exception {

        Path sourceDatabase = fakeServiceContext.getWorkingDatabaseFile();

        String methodName = fakeServiceContext.getMethodName();
        String databaseLocation = NAME_TO_DATABASE.get(methodName);

        fakeServiceContext.getEchoBaseUserApplicationContext().close();

        Path databaseTargetPath = Paths.get("").toAbsolutePath()
                                       .resolve("src")
                                       .resolve("test")
                                       .resolve("resources")
                                       .resolve("import-data")
                                       .resolve(Paths.get(databaseLocation).getName(1));
        if (log.isInfoEnabled()) {
            log.info("Migrates from:\n" + sourceDatabase + "\nto:\n" + databaseTargetPath);
        }

        try (GZIPOutputStream outputStream = new GZIPOutputStream(Files.newOutputStream(databaseTargetPath))) {
            Files.copy(sourceDatabase, outputStream);
        }

    }

    @Test
    public void noData() {
    }

    @Test
    public void commonData() {
    }

    @Test
    public void operation() {
    }

    @Test
    public void operationWithTotalSamples() {
    }

    @Test
    public void catches() {
    }

    @Test
    public void catchesWithVoyageResult() {
    }

    @Test
    public void mooring() {
    }

    @Test
    public void mooringResults() {
    }
    
    @Test
    public void exportAtlantos() {
    }

}
