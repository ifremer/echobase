/*
 * #%L
 * EchoBase :: Services
 * %%
 * Copyright (C) 2011 - 2012 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ifremer.echobase.services.service.importdata;

import com.google.common.collect.Iterables;
import fr.ifremer.echobase.entities.data.Mooring;
import fr.ifremer.echobase.services.ImportDataFixtures;
import fr.ifremer.echobase.services.service.importdata.configurations.MooringCommonsMooringImportConfiguration;

import java.io.IOException;

/**
 * @author Julien Ruchaud - ruchaud@codelutin.com
 * @since 4.0
 */
public class MooringCommonsMooringImportServiceIT extends AbstractImportDataServiceIT<MooringCommonsMooringImportConfiguration> {

    public MooringCommonsMooringImportServiceIT() {
        super(1);
    }

    @Override
    public ImportDataFixtures getImportDataFixture() {
        return ImportDataFixtures.IMPORT_DATA_ECHOBASE_NO_DATA;
    }

    @Override
    protected String[] getImportPath(String filename) {
        return new String[]{"/import-data", "mooring", filename};
    }

    @Override
    protected MooringCommonsMooringImportConfiguration createConfiguration() throws IOException {
        MooringCommonsMooringImportConfiguration conf = new MooringCommonsMooringImportConfiguration(getLocale());
        prepareInputFile(conf.getMooringFile(), getImportPath("mooring.csv.gz"));
        return conf;
    }

    @Override
    protected ImportDataService.MooringCommonsMooringImportDataAction newAction() throws IOException {
        return new ImportDataService.MooringCommonsMooringImportDataAction();
    }

    @Override
    protected void assertAfertImport(ImportDataResult<MooringCommonsMooringImportConfiguration> result) throws IOException {
        int nbMooring = fixtures.NB_MOORING();

        ImportDataFileResult importDataFileResult = Iterables.get(result.getImportResults(), 0);
        assertCsvImportResultPerEntity(importDataFileResult, Mooring.class, nbMooring, 0, nbMooring);
        assertCsvImportResult0(importDataFileResult, nbMooring);
    }

}
