/*
 * #%L
 * EchoBase :: Services
 * %%
 * Copyright (C) 2011 - 2012 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ifremer.echobase.services.service.importdata;

import com.google.common.collect.Iterables;
import fr.ifremer.echobase.entities.references.AncillaryInstrumentation;
import fr.ifremer.echobase.services.ImportDataFixtures;
import fr.ifremer.echobase.services.service.importdata.configurations.VoyageCommonsAncillaryInstrumentationImportConfiguration;

import java.io.IOException;

/**
 * @author Julien Ruchaud - ruchaud@codelutin.com
 * @since 4.0
 */
public class VoyageCommonsAncillaryInstrumentationImportServiceIT extends AbstractImportDataServiceIT<VoyageCommonsAncillaryInstrumentationImportConfiguration> {

    public VoyageCommonsAncillaryInstrumentationImportServiceIT() {
        super(1);
    }

    @Override
    public ImportDataFixtures getImportDataFixture() {
        return ImportDataFixtures.IMPORT_DATA_ECHOBASE_COMMON_DATA;
    }

    @Override
    protected String[] getImportPath(String filename) {
        return new String[]{"/import-data", "common", filename};
    }

    @Override
    protected VoyageCommonsAncillaryInstrumentationImportConfiguration createConfiguration() throws IOException {
        VoyageCommonsAncillaryInstrumentationImportConfiguration conf = new VoyageCommonsAncillaryInstrumentationImportConfiguration(getLocale());
        conf.setVoyageId(getVoyageId());
        prepareInputFile(conf.getAncillaryInstrumentationFile(), getImportPath("ancillaryInstrumentation.csv.gz"));
        return conf;
    }

    @Override
    protected ImportDataService.VoyageCommonsAncillaryInstrumentationImportDataAction newAction() throws IOException {
        return new ImportDataService.VoyageCommonsAncillaryInstrumentationImportDataAction();
    }

    @Override
    protected void assertAfertImport(ImportDataResult<VoyageCommonsAncillaryInstrumentationImportConfiguration> result) throws IOException {
        int nbAncillaryInstrumentation = fixtures.NB_ANCILLAY_INSTRUMENTATION();
        int updated = fixtures.NB_VOYAGE_ANCILLAY_INSTRUMENTATION();

        ImportDataFileResult importDataFileResult = Iterables.get(result.getImportResults(), 0);
        assertCsvImportResultPerEntity(importDataFileResult, AncillaryInstrumentation.class, 0, updated, nbAncillaryInstrumentation);
    }

}
