/*
 * #%L
 * EchoBase :: Services
 * %%
 * Copyright (C) 2011 - 2012 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ifremer.echobase.services.service.importdata;

import com.google.common.base.Charsets;
import com.google.common.base.Optional;
import com.google.common.base.Preconditions;
import com.google.common.collect.Collections2;
import com.google.common.collect.Sets;
import fr.ifremer.echobase.EchoBaseTechnicalException;
import fr.ifremer.echobase.entities.EchoBaseUser;
import fr.ifremer.echobase.entities.EchoBaseUserEntityEnum;
import fr.ifremer.echobase.entities.EchoBaseUserPersistenceContext;
import fr.ifremer.echobase.entities.data.Cell;
import fr.ifremer.echobase.entities.data.Cells;
import fr.ifremer.echobase.entities.data.DataAcousticProvider;
import fr.ifremer.echobase.entities.data.DataAcquisition;
import fr.ifremer.echobase.entities.data.DataProcessing;
import fr.ifremer.echobase.entities.data.Mooring;
import fr.ifremer.echobase.entities.data.Transect;
import fr.ifremer.echobase.entities.data.TransectImpl;
import fr.ifremer.echobase.entities.data.Transit;
import fr.ifremer.echobase.entities.data.TransitImpl;
import fr.ifremer.echobase.entities.data.Voyage;
import fr.ifremer.echobase.entities.references.AcousticInstrument;
import fr.ifremer.echobase.entities.references.CellType;
import fr.ifremer.echobase.entities.references.DataMetadata;
import fr.ifremer.echobase.io.InputFile;
import fr.ifremer.echobase.services.EchoBaseTestServiceSupport;
import fr.ifremer.echobase.services.FakeEchoBaseServiceContext;
import fr.ifremer.echobase.services.ImportDataFixtures;
import fr.ifremer.echobase.services.csv.EchoBaseCsvUtil;
import fr.ifremer.echobase.services.service.UserDbPersistenceService;
import fr.ifremer.echobase.services.service.importdata.configurations.ImportDataConfigurationSupport;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Assert;
import org.junit.Test;
import org.nuiton.csv.Import;
import org.nuiton.csv.ext.CsvReaders;
import org.nuiton.topia.persistence.TopiaEntity;
import org.nuiton.topia.persistence.TopiaException;
import org.nuiton.topia.service.csv.in.AbstractImportModel;
import org.nuiton.util.TimeLog;

import java.io.File;
import java.io.IOException;
import java.io.Reader;
import java.nio.file.Files;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;

/**
 * Abstrac import data test.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 0.3
 */
public abstract class AbstractImportDataServiceIT<C extends ImportDataConfigurationSupport> extends EchoBaseTestServiceSupport {

    /** Logger. */
    private static final Log log = LogFactory.getLog(AbstractImportDataServiceIT.class);

    public static final TimeLog TIME_LOG = new TimeLog(AbstractImportDataServiceIT.class);

    private final int nbFiles;

    protected AbstractImportDataServiceIT(int nbFiles) {
        this.nbFiles = nbFiles;
    }

    public abstract ImportDataFixtures getImportDataFixture();

    @Override
    protected final FakeEchoBaseServiceContext initContext() {
        return new FakeEchoBaseServiceContext(getImportDataFixture().getDbPath());
    }

    protected abstract C createConfiguration() throws IOException;

    protected abstract ImportDataService.ImportDataAction<C> newAction() throws IOException;

    protected void assertBeforeImport() {
        getImportDataFixture().assertBeforeImportExisting(fakeServiceContext, fixtures);
        getImportDataFixture().assertBeforeImportNotExisting(fakeServiceContext, fixtures);
    }

    protected abstract void assertAfertImport(ImportDataResult<C> result) throws IOException;

    protected abstract String[] getImportPath(String filename);

    @Test
    public final void doImport() throws Exception {

        assertBeforeImport();

        C configuration = createConfiguration();
        configuration.setWorkingDirectory(getConfiguration().getTemporaryDirectory());

        ImportDataService.ImportDataAction<C> importDataAction = newAction();

        ImportDataService service = newService(ImportDataService.class);

        EchoBaseUser fakeUser = createFakeUser();

        long s0 = TimeLog.getTime();

        ImportDataResult<C> importDataResult = importDataAction.doImport(service, configuration, fakeUser);
        String resume = importDataResult.getImportSummary();

        TIME_LOG.log(s0, "doImport");

        List<ImportDataFileResult> result = importDataResult.getImportResults();

        Assert.assertNotNull(result);
        Assert.assertEquals(nbFiles, result.size());
        assertConfProgressionToEnd(configuration);

        if (log.isInfoEnabled()) {
            if (log.isInfoEnabled()) {
                log.info('\n' + resume);
            }
        }

        assertAfertImport(importDataResult);
    }

    protected void addVoyageMissingEsduCells(String esduColumnName, String voyageId, InputFile inputFile) throws TopiaException, IOException {
        UserDbPersistenceService persistenceService = serviceContext.newService(UserDbPersistenceService.class);

        Voyage voyage = persistenceService.getVoyage(voyageId);
        Transit transit;
        Transect transect;

        if (voyage.isTransitEmpty()) {

            // create a fake transit
            transit = persistenceService.createTransit(new TransitImpl());
            voyage.addTransit(transit);
        } else {
            transit = voyage.getTransit().iterator().next();
        }

        if (transit.isTransectEmpty()) {

            // create a fake transect
            transect = persistenceService.createTransect(new TransectImpl());
            transit.addTransect(transect);
        } else {
            transect = transit.getTransect().iterator().next();
        }
        
        addDataAcquisitionMissingEsduCells(persistenceService, esduColumnName, transect, inputFile);
    }
    
    protected void addMooringMissingEsduCells(String esduColumnName, String mooringId, InputFile inputFile) throws TopiaException, IOException {
        UserDbPersistenceService persistenceService = serviceContext.newService(UserDbPersistenceService.class);

        Mooring mooring = persistenceService.getMooring(mooringId);
        addDataAcquisitionMissingEsduCells(persistenceService, esduColumnName, mooring, inputFile);
    }
    
    protected void addDataAcquisitionMissingEsduCells(UserDbPersistenceService persistenceService, String esduColumnName, 
            DataAcousticProvider<?> provider, InputFile inputFile) throws TopiaException, IOException {

        DataAcquisition dataAcquisition;
        DataProcessing dataProcessing;
        
        if (provider.isDataAcquisitionEmpty()) {

            // create a fake dataAcquisition
            AcousticInstrument acousticInstrument = persistenceService.getFirstAcousticInstrument();
            dataAcquisition = persistenceService.createDataAcquisition(acousticInstrument);
            provider.addDataAcquisition(dataAcquisition);
        } else {
            dataAcquisition = provider.getDataAcquisition().iterator().next();
        }
        
        if (dataAcquisition.isDataProcessingEmpty()) {

            // creates a fake dataProcessing
            dataProcessing = persistenceService.createDataProcessing("id", "pt");
            dataAcquisition.addDataProcessing(dataProcessing);
        } else {
            dataProcessing = dataAcquisition.getDataProcessing().iterator().next();
        }

        Set<String> cellsNames;

        if (dataProcessing.isCellEmpty()) {
            cellsNames = Sets.newHashSet();
        } else {
            cellsNames = Sets.newHashSet(Collections2.transform(dataProcessing.getCell(), Cells.CELL_BY_NAME));
        }

        ResultEdsuImportMockImportModel csvModel = new ResultEdsuImportMockImportModel(getCsvSeparator(), esduColumnName, inputFile.getFile());

        CellType esduCellType = persistenceService.getCellTypeById("Esdu");
//        Preconditions.checkNotNull(esduCellType);

        try (Reader reader = getInputFileReader(inputFile)) {

            try (Import<ResultEdsuImportMockImportModelRow> importer = Import.newImport(csvModel, reader)) {

                for (ResultEdsuImportMockImportModelRow row : importer) {

                    String esduCellId = row.getName();

                    if (!cellsNames.contains(esduCellId)) {

                        if (log.isDebugEnabled()) {
                            log.debug("Adding missing esdu cell with name " + esduCellId);
                        }

                        Cell cell = persistenceService.createCell(esduCellType, esduCellId, null);
                        dataProcessing.addCell(cell);
                        cellsNames.add(esduCellId);
                    }
                }
            }

            persistenceService.flush();

        }
    }

    protected Reader getInputFileReader(InputFile inputFile) throws EchoBaseTechnicalException {
        try {
            return Files.newBufferedReader(inputFile.getFile().toPath(), Charsets.UTF_8);
        } catch (IOException e) {
            throw new EchoBaseTechnicalException("Could not get import file " + inputFile.getFile(), e);
        }
    }

    public static class ResultEdsuImportMockImportModel extends AbstractImportModel<ResultEdsuImportMockImportModelRow> {

        public ResultEdsuImportMockImportModel(char separator, String esduColumnName, File file) {
            super(separator);

            newMandatoryColumn(esduColumnName, EchoBaseCsvUtil.CELL_NAME);

            String[] header = CsvReaders.getHeader(file, separator);

            for (String columnHeader : header) {

                if (!esduColumnName.equals(columnHeader) &&
                        !("\"" + esduColumnName + "\"").equals(columnHeader)) {
                    Matcher matcher = ImportDataService.REMOVE_DOUBLE_QUOTES_PATTERN.matcher(columnHeader);
                    if (matcher.matches()) {
                        newIgnoredColumn(matcher.group(1));
                    } else {
                        newIgnoredColumn(columnHeader);
                    }
                }
            }
        }

        @Override
        public ResultEdsuImportMockImportModelRow newEmptyInstance() {
            return new ResultEdsuImportMockImportModelRow();
        }
    }

    public static class ResultEdsuImportMockImportModelRow {

        protected String name;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }

    protected <E extends TopiaEntity> void assertCsvImportResultPerEntity(ImportDataFileResult actual,
                                                                          Class<E> entityType,
                                                                          int numberCreated,
                                                                          int numberUpdated,
                                                                          int nbCount) throws TopiaException {
        Assert.assertNotNull(actual);
        Set<EchoBaseUserEntityEnum> entityTypes = actual.getEntityTypes();
        EchoBaseUserEntityEnum expectedEntityType = EchoBaseUserEntityEnum.valueOf(entityType);
        Assert.assertTrue(entityTypes.contains(expectedEntityType));
        Assert.assertEquals(numberCreated, actual.getNumberCreated(expectedEntityType));
        Assert.assertEquals(numberUpdated, actual.getNumberUpdated(expectedEntityType));
        fakeServiceContext.assertNbEntities(entityType, nbCount);
    }

    protected void assertCsvImportResult0(ImportDataFileResult actual, int nbIds) throws TopiaException, IOException {

        Assert.assertNotNull(actual);

        Assert.assertEquals(nbIds, actual.sizeImportFileIds());

        InputFile processedImportFile = actual.getProcessedImportFile();
        String processedImportFileContent = new String(Files.readAllBytes(processedImportFile.getFile().toPath()));

        InputFile importedImportFile = actual.getImportedExportFile();
        String importedImportFileContent = new String(Files.readAllBytes(importedImportFile.getFile().toPath()));

        if (log.isInfoEnabled()) {
            log.info(String.format("processedImportFile: %s\nimportedImportFileContent: %s", processedImportFile.getFile(), importedImportFile.getFile()));
        }

        if (log.isDebugEnabled()) {
            log.debug(String.format("processedImportFile: %s\n:%s\nimportedImportFileContent: %s\n:%s",
                                    processedImportFile.getFileName(),
                                    processedImportFileContent,
                                    importedImportFile.getFileName(),
                                    importedImportFileContent));
        }

        Assert.assertEquals(processedImportFileContent, importedImportFileContent);

    }

    protected String getDataProcessingId() {
        DataProcessing dataProcessing = serviceContext.getEchoBaseUserPersistenceContext().getDataProcessingDao().findAll().get(0);
        Preconditions.checkNotNull(dataProcessing);
        return dataProcessing.getTopiaId();
    }

    protected void addMissingDataMetadata(String dataMetadataName) {

        EchoBaseUserPersistenceContext persistenceContext = this.serviceContext.getEchoBaseUserPersistenceContext();

        Optional<DataMetadata> optionalSampleDataType = persistenceContext.getDataMetadataDao().forNameEquals(dataMetadataName).tryFindUnique();
        if (!optionalSampleDataType.isPresent()) {

            if (log.isInfoEnabled()) {
                log.info("Add dataMetadata " +dataMetadataName);
            }
            persistenceContext.getDataMetadataDao().create(DataMetadata.PROPERTY_NAME,dataMetadataName);

            persistenceContext.commit();
        }

    }

}
