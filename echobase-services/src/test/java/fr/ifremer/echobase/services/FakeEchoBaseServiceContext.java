/*
 * #%L
 * EchoBase :: Services
 * %%
 * Copyright (C) 2011 - 2012 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ifremer.echobase.services;

import com.google.common.base.Preconditions;
import fr.ifremer.echobase.EchoBaseTechnicalException;
import fr.ifremer.echobase.config.EchoBaseConfiguration;
import fr.ifremer.echobase.config.EchoBaseConfigurationOption;
import fr.ifremer.echobase.entities.EchoBaseInternalPersistenceContext;
import fr.ifremer.echobase.entities.EchoBaseUserPersistenceContext;
import fr.ifremer.echobase.entities.EchoBaseUserTopiaApplicationContext;
import fr.ifremer.echobase.persistence.EchoBaseDbMeta;
import fr.ifremer.echobase.persistence.JdbcConfiguration;
import org.apache.commons.lang3.time.DateUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Assert;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;
import org.nuiton.topia.persistence.TopiaEntity;
import org.nuiton.topia.persistence.TopiaException;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Date;
import java.util.Locale;
import java.util.Properties;
import java.util.zip.GZIPInputStream;

/**
 * Provide an implementation of {@link EchoBaseServiceContext} suitable for repeatable,
 * isolated tests requiring a database.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 0.3
 */
public class FakeEchoBaseServiceContext extends TestWatcher implements EchoBaseServiceContext {

    private static final Log log =
            LogFactory.getLog(FakeEchoBaseServiceContext.class);

    /** A time-stamp, allow to make multiple build and keep the tests data. */
    protected static final String TIMESTAMP = String.valueOf(System.nanoTime());

    protected String initDb;

    protected Date fakeCurrentTime;

    protected File testDir;

    protected EchoBaseUserPersistenceContext echoBasePersistenceContext;

    protected EchoBaseInternalPersistenceContext echoBaseInternalPersistenceContext;

    protected EchoBaseConfiguration configuration;

    private EchoBaseUserTopiaApplicationContext echoBaseTopiaApplicationContext;

    private final EchobaseAieOC injector;
    protected String methodName;

    public FakeEchoBaseServiceContext(String initDb) {
        this.initDb = initDb;
        this.injector = new ServiceEchobaseAieOC();
    }

    public <E extends TopiaEntity> void assertNbEntities(Class<E> entityType, int expectedNumber) throws TopiaException {
        long count = getEchoBaseUserPersistenceContext().getDao(entityType).count();
        Assert.assertEquals(count, expectedNumber);
    }

    public void assertNoEntities(Class<?>... classes) throws TopiaException {
        for (Class<?> aClass : classes) {
            assertNbEntities((Class<? extends TopiaEntity>) aClass, 0);
        }
    }

    protected void setInitDb(String initDb) {
        this.initDb = initDb;
    }

    protected File getTestSpecificDirectory(Description description) {
        // Trying to look for the temporary folder to store data for the test
        String tempDirPath = System.getProperty("java.io.tmpdir");
        if (tempDirPath == null) {
            // can this really occur ?
            tempDirPath = "";
            if (log.isWarnEnabled()) {
                log.warn("'\"java.io.tmpdir\" not defined");
            }
        }
        File tempDirFile = new File(tempDirPath);

        // create the directory to store database data
        String dataBasePath = description.getClassName()
                + File.separator // a directory with the test class name
                + description.getMethodName()// a sub-directory with the method name
                + '_'
                + TIMESTAMP; // and a timestamp
        return new File(tempDirFile, dataBasePath);
    }

    public Path getWorkingDatabaseFile() {
        File dbDirectory = new File(testDir, "db");
        return new File(dbDirectory, "echobase.h2.db").toPath();
    }

    public String getMethodName() {
        return methodName;
    }

    @Override
    protected void starting(Description description) {
        super.starting(description);
        methodName = description.getMethodName();
        testDir = getTestSpecificDirectory(description);
        if (log.isInfoEnabled()) {
            log.info("Test dir = " + testDir);
        }

        if (initDb != null) {

            Path destinationDb = getWorkingDatabaseFile();

            try (InputStream inputStream = getClass().getResourceAsStream(initDb)) {
                Preconditions.checkNotNull(inputStream, "Could not find resource from " + initDb);

                Files.createDirectories(destinationDb.getParent());

                try (GZIPInputStream gzipStream = new GZIPInputStream(inputStream)) {
                    Files.copy(gzipStream, destinationDb);
                }

            } catch (IOException e) {
                throw new EchoBaseTechnicalException("Could not copy db", e);
            }

        }

        // init configuration

        Properties defaultProps = new Properties();
        defaultProps.put(EchoBaseConfigurationOption.DATA_DIRECTORY.getKey(), testDir);
        configuration = new EchoBaseConfiguration("echobase-services-test.properties", defaultProps);

        JdbcConfiguration dbConf = JdbcConfiguration.newEmbeddedConfig(testDir);
        echoBaseTopiaApplicationContext = EchoBaseUserTopiaApplicationContext.newApplicationContext(dbConf);

    }

    @Override
    protected void finished(Description description) {
        super.finished(description);
        if (echoBaseTopiaApplicationContext != null && !echoBaseTopiaApplicationContext.isClosed()) {
            echoBaseTopiaApplicationContext.close();
        }
    }

    @Override
    public EchoBaseUserTopiaApplicationContext getEchoBaseUserApplicationContext() {
        return echoBaseTopiaApplicationContext;
    }

    @Override
    public void setEchoBaseUserApplicationContext(EchoBaseUserTopiaApplicationContext applicationContext) {
        this.echoBaseTopiaApplicationContext = applicationContext;
    }

    @Override
    public EchoBaseUserPersistenceContext getEchoBaseUserPersistenceContext() {
        if (echoBasePersistenceContext == null) {
            echoBasePersistenceContext = echoBaseTopiaApplicationContext.newPersistenceContext();
        }
        return echoBasePersistenceContext;
    }

    @Override
    public void setEchoBaseUserPersistenceContext(EchoBaseUserPersistenceContext userTopiaPersistenceContext) {
        this.echoBasePersistenceContext = userTopiaPersistenceContext;
    }

    @Override
    public EchoBaseInternalPersistenceContext getEchoBaseInternalPersistenceContext() {
        return echoBaseInternalPersistenceContext;
    }

    @Override
    public void setEchoBaseInternalPersistenceContext(EchoBaseInternalPersistenceContext
                                                              internalTopiaPersistenceContext) {
        this.echoBaseInternalPersistenceContext = internalTopiaPersistenceContext;
    }

    @Override
    public String getUserDbUrl() {
        return echoBaseTopiaApplicationContext.getConfiguration().getJdbcConnectionUrl();
    }

    @Override
    public Locale getLocale() {
        return Locale.FRANCE;
    }

    @Override
    public EchoBaseDbMeta getDbMeta() {
        return EchoBaseDbMeta.newDbMeta();
    }

    @Override
    public EchoBaseConfiguration getConfiguration() {
        return configuration;
    }

    @Override
    public URL getCoserApiURL() {
        return getConfiguration().getCoserApiURL();
    }

//    @Override
//    public URL getCoserApiURL() {
//        try {
//            return new URL("http://localhost:8080/coser-web/json");
//        } catch (MalformedURLException e) {
//            throw new EchoBaseTechnicalException(e);
//        }
//    }

    @Override
    public <S extends EchoBaseService> S newService(Class<S> serviceClass) {
        // instantiate service using empty constructor
        S service;
        try {
            service = serviceClass.getConstructor().newInstance();
        } catch (InstantiationException e) {
            throw new EchoBaseTechnicalException(e);
        } catch (IllegalAccessException e) {
            throw new EchoBaseTechnicalException(e);
        } catch (InvocationTargetException e) {
            throw new EchoBaseTechnicalException(e);
        } catch (NoSuchMethodException e) {
            throw new EchoBaseTechnicalException(e);
        }

        inject(service);


        return service;
    }

    public void inject(Object service) {
        try {
            injector.inject(this, service);
        } catch (IllegalAccessException e) {
            throw new EchoBaseTechnicalException(e);
        } catch (ClassNotFoundException e) {
            throw new EchoBaseTechnicalException(e);
        }
    }

    @Override
    public Date newDate() {
        Date result = new Date();
        DateUtils.setMilliseconds(result, 0);
        return result;
    }

    public File getTestDir() {
        return testDir;
    }

    public void setConfiguration(EchoBaseConfiguration configuration) {
        this.configuration = configuration;
    }
}
