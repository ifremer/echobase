#! /bin/bash

# Script pour mettre à jour les bases de tests à partir d'une base avec juste
# le référentiel

if [ ! $# -eq 1 ]; then
  echo "usage: $0 importdb.zip"
  exit
fi

dbnameprefix="echobase"

dbDir=src/test/resources/import-data

function executeMaven() {  
  testId=$1
  dbdst=$2
  echo "execute mvn test -Dtest=$testId ..."
  mvn test -Dtest=$testId -Dmaven.surefire.debug &>/tmp/maven-$testId.log
  if [ $? -eq 1 ]; then
   echo "Error with test $testId, see /tmp/maven-$testId.log"
   exit 1
  fi  
  testTimestamp=`ls target/surefire-workdir/$testId`
  dbname=target/surefire-workdir/$testId/$testTimestamp/db/echobase.h2.db

  dst="$dbnameprefix-$dbdst.h2.db"

  rm -f $dbDir/$dst.gz
  echo "dbdir = $dbDir"
  echo "dst = $dst"
  (cp -v $dbname "$dbDir/$dst" ; cd "$dbDir" ; gzip "$dst")
}

rm -rf target

cp -v "$1" "src/test/resources/$dbnameprefix-importDb-referentiel.zip"

executeMaven fr.ifremer.echobase.services.service.importdb.ImportDbServiceTest nodata

executeMaven fr.ifremer.echobase.services.service.importdata.CommonAllImportServiceIT commonData

executeMaven fr.ifremer.echobase.services.service.importdata.OperationImportServiceIT operation

executeMaven fr.ifremer.echobase.services.service.importdata.CatchesImportServiceIT catches

executeMaven fr.ifremer.echobase.services.service.importdata.ResultsVoyageImportServiceIT catches-and-voyage-result

#executeMaven fr.ifremer.echobase.services.service.importdata.AcousticImportServiceIT catches-and-acoustic-and-voyage-result

#executeMaven fr.ifremer.echobase.services.service.importdata.ResultsEsduCellImportServiceIT

#executeMaven fr.ifremer.echobase.services.service.importdata.ResultsRegionCellImportServiceIT

#executeMaven fr.ifremer.echobase.services.service.importdata.ResultsMapFishCellImportServiceIT
