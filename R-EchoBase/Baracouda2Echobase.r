#All sA per echotype per ESDUs from old barac
#-------------------------
path='Q:/Projects/Acoustic biomass assessment/Data/ALL/barac2R/ESDU.txt'

esduall=read.table(path,sep=';',header=TRUE)
names(esduall)
aggregate(esduall$ENERGIE,list(esduall$CAMPAGNE),summary)

#-------------------------
#All sA per echotype per ESDUs from new barac
#-------------------------
path='Z:/Campagnes/PELGAS/Data/barac2R/resEsduEchotype.txt'

resduall=read.table(path,sep=';',header=TRUE)
names(resduall)
aggregate(resduall$ENERGIE,list(resduall$CAMPAGNE),summary)

#All ENERGIES converted to sA in this case... 

#Select PELGAS cruises
#-------------------------
resduall.PELGAS=resduall[substr(resduall$CAMPAGNE,1,6)=='PELGAS',]

# #Select PELGAS2000 and 2001 for sa correction
# resduall.PELGAS0001=resduall.PELGAS[resduall.PELGAS$CAMPAGNE%in%
#   c('PELGAS2000','PELGAS2001'),]
# names(resduall.PELGAS0001)
# 
# #sA<->En correspondence 
# #(after Diner, 2005. EVALUATION DE STOCK PAR ECHO-INTEGRATION
# #(r?actualisation de la note au CIEM en 1983 - revu en d?cembre 98 puis juillet 2005)
# # sA=0.02327*En/De where De=distance in nautical miles
# #convert En to NASC
# #--------------------------
# resduall.PELGAS0001$ENERGIE=resduall.PELGAS0001$ENERGIE*0.02327
# 
# #binds series
# #--------------------------
# dim(resduall.PELGAS)
# resduall.PELGAS=rbind(resduall.PELGAS[!resduall.PELGAS$CAMPAGNE%in%
#   c('PELGAS2000','PELGAS2001'),],resduall.PELGAS0001)
# dim(resduall.PELGAS)
# 
# aggregate(resduall.PELGAS$ENERGIE,list(resduall.PELGAS$CAMPAGNE),summary)
# 
#*************************************************************************
# #Impossible-> En have already been converted into Sa in baracouda... 
#*************************************************************************

#Rename columns
#--------------------------
names(resduall.PELGAS)=c('Voyage','name','echotype','NASC',
                         'ReferenceStationCatch','Formula','Flag')

#remove (or not?) null NASC
#--------------------------
dim(resduall.PELGAS)
resduall.PELGAS.wo0=resduall.PELGAS[resduall.PELGAS$NASC!=0,]
dim(resduall.PELGAS.wo0)

  #Export sA and hauls per esdu and echotype
  #-----------------
  path.export='Z:/Campagnes/PELGAS/Data/barac2R/'
  write.table(resduall.PELGAS,
              paste(path.export,'PELGAS0010resEsduEchotype4Echobase.csv',
              sep=''),sep=';',row.names=FALSE)

#Biometries
path='C:/Users/mdoray.IFR/Documents/Data/Echobase/export-SampleData.csv'
sampleData=read.table(path,sep=';',header=TRUE)
names(sampleData)
aggregate(resduall$ENERGIE,list(resduall$CAMPAGNE),summary)
rm(sampleData)

#----------------------------------------------------
#missing operations: 1 depthstratum problem and "unsorted" samples problems
#----------------------------------------------------
#Fishing data to be added to legacy data in Echobase format
#----------------------------------------------------
#import missing baracouda fishing data
path='~/.gvfs/donnees2 sur nantes/Campagnes/bd/Echobase/EchobaseLegacyCheck/BaracoudaExports/PECHE_verifImportEchobase.txt'
missing.fbara=read.table(path,sep=';',header=TRUE)
missing.fbara$PTRI=missing.fbara$PT
missing.fbaraH=missing.fbara[missing.fbara$SIGNEP=='H',]
head(missing.fbara)
dim(missing.fbara)
unique(missingPechei4Echobase$operationId)

missingPechei4Echobase[missingPechei4Echobase$operationId=='N0408',]

missingPechei4Echobase=missing.fbara[,c('NOSTA','GENR_ESP','SIGNEP','PT','NT',
                                        'LM','PM','MOULE','PTRI')]
names(missingPechei4Echobase)=c("operationId","baracoudaCode","sizeCategory",
                                "sampleWeight","numberSampled","meanLength",
                                "meanWeight","noPerKg","sortedWeight")
head(missingPechei4Echobase)
path.export='~/.gvfs/donnees2 sur nantes/Campagnes/bd/Echobase/EchobaseLegacyCheck/'
write.table(missingPechei4Echobase,paste(path.export,'missing_TotalSamplesR4Echobase.csv',sep=''),sep=';',
            row.names=FALSE)

#Biological measurements data to be added to legacy data in Echobase format
#----------------------------------------------------
#import missing baracouda fishing data
path='~/.gvfs/donnees2 sur nantes/Campagnes/bd/Echobase/EchobaseLegacyCheck/BaracoudaExports/MENS_checkEchobase.txt'
missing.mbara=read.table(path,sep=';',header=TRUE)
head(missing.mbara)
missing.mbara$Lcm=missing.mbara$TAILLE
missing.mbara$CATEG=missing.mbara$SIGNEP
missing.mbara$SEXE='N'
missing.mbara$UNITE=0
missing.mbara$INC=5
dim(missing.mbara)
missing.mbara[is.na(missing.mbara$POIDSTAILLE),'POIDSTAILLE']=missing.mbara[is.na(missing.mbara$POIDSTAILLE),"POIDSECHANT"]/
  missing.mbara[is.na(missing.mbara$POIDSTAILLE),'NBIND']

missing.mens4Echobase=missing.mbara[,c('NOSTA','GENR_ESP','CATEG','SEXE',
                                       'POIDSECHANT','NECHANT','Lcm','NBIND',
                                       'POIDSTAILLE','UNITE','INC')]
names(missing.mens4Echobase)=c('operationId','baracoudaCode',
                               'sizeCategory','sexCategory',
                               'sampleWeight','numberSampled',
                               'lengthClass','numberAtLength',
                               'weightAtLength','units','round')
head(missing.mens4Echobase)
path.export='~/.gvfs/donnees2 sur nantes/Campagnes/bd/Echobase/EchobaseLegacyCheck/'
write.table(missing.mens4Echobase,paste(path.export,'missing_subSamplesR4Echobase.csv',sep=''),sep=';',
            row.names=FALSE)

#Creates operations to be added to legacy data in Echobase format
#----------------------------------------------------
path='W:/Campagnes/bd/Echobase/EchobaseLegacyCheck/BaracoudaExports/missing_Operations4Echobase.txt'
path='~/.gvfs/donnees2 sur nantes/Campagnes/bd/Echobase/EchobaseLegacyCheck/BaracoudaExports/missing_Operations4Echobase.txt'

baracmope=read.table(path,sep=';',header=TRUE)
head(baracmope)
names(baracmope)

Operations=data.frame(vesselName=baracmope$NAVIRE,operationId=baracmope$NOSTA,
                      depthStratumId=baracmope$STA_IMAGES,
                      gearShootingStartTime=baracmope$DHTUDEB,
                      midHaulLatitude=baracmope$LATDD,
                      midHaulLongitude=baracmope$LGDD,
                      gearShootingStartLatitude=baracmope$LATDD,
                      gearShootingStartLongitude=baracmope$LGDD,
                      gearShootingEndTime=NA,
                      gearShootingEndLatitude=NA,
                      gearShootingEndLongitude=NA,
                      gearCode=baracmope$ENGIN)

Operations$gearShootingStartTime=paste(format(strptime(
  Operations$gearShootingStartTime,"%d/%m/%Y %H:%M:%S")),'.0000',sep='')

dim(Operations)
unique(Operations$vesselName)
unique(Operations$gearCode)
head(Operations)

#------------------
#Export OperationMetadataValue from baracouda to echobase
#------------------
OperationMetadataValue=rbind(data.frame(vesselName=baracmope$NAVIRE,
                                        operationId=baracmope$NOSTA,metadataType='MeanWaterDepth',
                                        operationMetadataValue=baracmope$SONDEDEB),
                             data.frame(vesselName=baracmope$NAVIRE,
                                        operationId=baracmope$NOSTA,metadataType='WaterDepthShoot',
                                        operationMetadataValue=as.numeric(baracmope$SONDEDEB)),
                             data.frame(vesselName=baracmope$NAVIRE,
                                        operationId=baracmope$NOSTA,metadataType='WaterDepthHaul',
                                        operationMetadataValue=as.numeric(baracmope$SONDEFIN)))           
head(OperationMetadataValue)
dim(OperationMetadataValue)
#remove rows with NAs
OperationMetadataValue=OperationMetadataValue[complete.cases(OperationMetadataValue),]
dim(OperationMetadataValue)
#------------------
#GearMetadataValue
#------------------
names(baracmope)

GearMetadataValue=rbind(data.frame(vesselName=baracmope$NAVIRE,
                                   operationId=baracmope$NOSTA,
                                   gearCode=baracmope$ENGIN,
                                   metadataType='CableLength',
                                   gearMetadataValue=as.numeric(baracmope$LFUNES)),
                        data.frame(vesselName=baracmope$NAVIRE,
                                   operationId=baracmope$NOSTA,
                                   gearCode=baracmope$ENGIN,
                                   metadataType='MinSpeed',
                                   gearMetadataValue=as.numeric(baracmope$VMIN)),
                        data.frame(vesselName=baracmope$NAVIRE,
                                   operationId=baracmope$NOSTA,
                                   gearCode=baracmope$ENGIN,
                                   metadataType='MaxSpeed',
                                   gearMetadataValue=as.numeric(baracmope$VMAX)))
head(GearMetadataValue)

dim(GearMetadataValue)
#remove rows with NAs
GearMetadataValue=GearMetadataValue[complete.cases(GearMetadataValue),]
dim(GearMetadataValue)

table(GearMetadataValue$metadataType)
table(as.character(GearMetadataValue$operationId))

#Export Operation data in Echobase format
path.export='~/.gvfs/donnees2 sur nantes/Campagnes/bd/Echobase/EchobaseLegacyCheck/'

write.table(Operations,paste(path.export,'missing_Operations4Echobase.csv',sep=''),
            row.names=FALSE,sep=';')
write.table(OperationMetadataValue,
            paste(path.export,'missing_OperationMetadataValue4Echobase.csv',sep=''),
            row.names=FALSE,sep=';')
write.table(GearMetadataValue,paste(path.export,
                                    'missing_GearMetadataValue4Echobase.csv',sep=''),row.names=FALSE,
            sep=';')


