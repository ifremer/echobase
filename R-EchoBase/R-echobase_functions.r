#This function reshape cells data from EchoBase into wide format 
#with proper numeric fields

cells4humans=function(df,v.names="data_value",idvar="EIcell_id",correcXY=FALSE,
                      timevar="data_type",snames=c("voyage_name","esdu_name",
                                                   "cell_type","cell_name","esdu_data_type",idvar,
                                                   "TimeStart","TimeEnd")){
  #Cells in wide format
  #-----------------------------------------
  df.wide=reshape(df,v.names =v.names , idvar = idvar,
                  timevar =timevar , direction = "wide")
  head(df.wide)
  names(df.wide)=gsub(paste(v.names,'.',sep=''),'',names(df.wide))
  #Position correction
  if (correcXY){
    df.wide$LongitudeStart=correct.positions(
      df=df.wide$LongitudeStart,xname="LongitudeStart",asNewColumn=FALSE)
    df.wide$LatitudeStart=correct.positions(
      df=df.wide$LatitudeStart,yname="LatitudeStart",asNewColumn=FALSE)
  }  
  #Set data column format to numeric
  #-----------------------------------------
  df.wide[,!names(df.wide)%in%snames]=
    apply(df.wide[,!names(df.wide)%in%snames],2,as.numeric)
  df.wide
}