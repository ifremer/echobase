<#--
 #%L
 EchoBase :: UI
 %%
 Copyright (C) 2011 - 2012 Ifremer, Codelutin
 %%
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 #L%
-->
<#--
/*
 * $Id$
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
-->
<ul
<#if parameters.name??>
        id="${parameters.name?html}_ul"<#rt/>
</#if>
        >
<@s.iterator value="parameters.list">
  <#if parameters.listKey??>
    <#assign itemKey = stack.findValue(parameters.listKey)/>
  <#else>
    <#assign itemKey = stack.findValue('top')/>
  </#if>
  <#assign itemKeyStr = itemKey.toString() />
  <#if parameters.listValue??>
    <#assign itemValue = stack.findString(parameters.listValue)/>
  <#else>
    <#assign itemValue = stack.findString('top')/>
  </#if>
  <li>
    <input type="radio"<#rt/>
      <#if parameters.name??>
           name="${parameters.name?html}"<#rt/>
      </#if>
           id="${parameters.id?html}${itemKeyStr?html}"<#rt/>
      <#if tag.contains(parameters.nameValue?default(''), itemKeyStr)>
           checked="checked"<#rt/>
      </#if>
      <#if itemKey??>
           value="${itemKeyStr?html}"<#rt/>
      </#if>
      <#if parameters.disabled?default(false)>
           disabled="disabled"<#rt/>
      </#if>
      <#if parameters.tabindex??>
           tabindex="${parameters.tabindex?html}"<#rt/>
      </#if>
      <#if parameters.cssClass??>
           class="${parameters.cssClass?html}"<#rt/>
      </#if>
      <#if parameters.cssStyle??>
           style="${parameters.cssStyle?html}"<#rt/>
      </#if>
      <#include "/${parameters.templateDir}/simple/css.ftl" />
      <#if parameters.title??>
           title="${parameters.title?html}"<#rt/>
      </#if>
      <#include "/${parameters.templateDir}/simple/scripting-events.ftl" />
      <#include "/${parameters.templateDir}/simple/common-attributes.ftl" />
            /><#rt/>
    <label for="${parameters.id?html}${itemKeyStr?html}"><#rt/>
    ${itemValue}<#t/>
    </label>
  </li>
</@s.iterator>
</ul>
