<#--
 #%L
 EchoBase :: UI
 %%
 Copyright (C) 2011 Ifremer, Codelutin
 %%
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 #L%
-->
<#--
	Only show message if errors are available.
	This will be done if ActionSupport is used.
-->
<#assign hasFieldErrors = parameters.name?? && fieldErrors?? && fieldErrors[parameters.name]??/>
<div <#rt/><#if parameters.id??>id="wwgrp_${parameters.id}"<#rt/></#if>
        class="wwgrp">

<#if hasFieldErrors>
  <div <#rt/><#if parameters.id??>id="wwerr_${parameters.id}"<#rt/></#if>
          class="wwerr">
    <#list fieldErrors[parameters.name] as error>
      <div<#rt/>
        <#if parameters.id??>
                errorFor="${parameters.id}"<#rt/>
        </#if>
                class="errorMessage">
      ${error?html}
      </div><#t/>
    </#list>
  </div><#t/>
</#if>

<#if parameters.label??>
  <#if parameters.labelposition?default("top") == 'top'>
    <div <#rt/>
  <#else>
          <span <#rt/>
  </#if>
  <#if parameters.id??>id="wwlbl_${parameters.id}"<#rt/></#if> class="wwlbl">
  <label <#t/>
    <#if parameters.id??>
            for="${parameters.id?html}" <#t/>
    </#if>
    <#if hasFieldErrors>
            class="errorLabel"<#t/>
    <#else>
            class="label"<#t/>
    </#if>
          ><#t/>

  ${parameters.label?html}&nbsp;${parameters.labelseparator!":"?html}
    <#if parameters.required?default(false)>
      <span class="required">*</span><#t/>
    </#if>
    <#include "/${parameters.templateDir}/xhtml/tooltip.ftl" />
  </label><#t/>
  <#if parameters.labelposition?default("top") == 'top'>
  </div> <#rt/>
  <#else>
  </span> <#rt/>
  </#if>
</#if>
