<#--
 #%L
 EchoBase :: UI
 %%
 Copyright (C) 2011 Ifremer, Codelutin
 %%
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 #L%
-->
<#--
NOTE: The 'header' stuff that follows is in this one file for checkbox due to the fact
that for checkboxes we do not want the label field to show up as checkboxes handle their own
lables
-->
<#assign hasFieldErrors = fieldErrors?? && fieldErrors[parameters.name]??/>
<div <#rt/><#if parameters.id??>id="wwgrp_${parameters.id}"<#rt/></#if>
        class="wwgrp">

<#if hasFieldErrors>
  <div <#rt/><#if parameters.id??>id="wwerr_${parameters.id}"<#rt/></#if>
          class="wwerr">
    <#list fieldErrors[parameters.name] as error>
      <div<#rt/>
        <#if parameters.id??>
                errorFor="${parameters.id}"<#rt/>
        </#if>
                class="errorMessage">
      ${error?html}
      </div><#t/>
    </#list>
  </div><#t/>
</#if>
<#if parameters.labelposition?default("left") == 'left'>
  <span <#rt/>
    <#if parameters.id??>id="wwlbl_${parameters.id}"<#rt/></#if> class="wwlbl">
<label<#t/>
  <#if parameters.id??>
          for="${parameters.id?html}"<#rt/>
  </#if>
  <#if hasFieldErrors>
          class="checkboxErrorLabel"<#rt/>
  <#else>
          class="label"<#rt/>
  </#if>
        >${parameters.label?html}
<#if parameters.tooltip??>
  <#include "/${parameters.templateDir}/xhtml/tooltip.ftl" />
</#if>
</label><#rt/>
</span>
</#if>

<#if parameters.labelposition?default("left") == 'top'>
  <div <#rt/>
<#else>
        <span <#rt/>
</#if>
<#if parameters.id??>id="wwctrl_${parameters.id}"<#rt/></#if> class="wwctrl">

<#if parameters.required?default(false)>
  <span class="required">*</span><#t/>
</#if>

<#include "/${parameters.templateDir}/simple/checkbox.ftl" />
<#if parameters.labelposition?default("left") != 'left'>
  <#if parameters.labelposition?default("left") == 'top'>
  </div> <#rt/>
  <#else>
  </span>  <#rt/>
  </#if>
  <#if parameters.label??>
    <#if parameters.labelposition?default("left") == 'top'>
    <div <#rt/>
    <#else>
            <span <#rt/>
    </#if>
    <#if parameters.id??>id="wwlbl_${parameters.id}"<#rt/></#if> class="wwlbl">
    <label<#t/>
      <#if parameters.id??>
              for="${parameters.id?html}"<#rt/>
      </#if>
      <#if hasFieldErrors>
              class="checkboxErrorLabel"<#rt/>
      <#else>
              class="checkboxLabel"<#rt/>
      </#if>
            >${parameters.label?html}</label><#rt/>
  </#if>
</#if>
<#if parameters.label??>
  <#if parameters.labelposition?default("left") == 'top'>
    </div> <#rt/>
  <#else>
  </span> <#rt/>
  </#if>
</#if>
</div>
