<#--
 #%L
 EchoBase :: UI
 %%
 Copyright (C) 2011 - 2013 Ifremer, Codelutin, Tony Chemit
 %%
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 #L%
-->
<#include "/${parameters.templateDir}/${parameters.theme}/controlheader.ftl" />
<label<#rt/>
<#if parameters.id??>
        id="${parameters.id?html}"<#rt/>
</#if>
<#if parameters.cssClass??>
        class="${parameters.cssClass?html}"<#rt/>
</#if>
<#if parameters.cssStyle??>
        style="${parameters.cssStyle?html}"<#rt/>
</#if>
<#if parameters.cssClass??>
        class="${parameters.cssClass?html}"<#rt/>
</#if>
<#if parameters.for??>
        for="${parameters.for?html}"<#rt/>
</#if>
        ><#rt/>
<#if parameters.nameValue??>
  <@s.property value="parameters.nameValue"/><#t/>
</#if>
</label>
<#include "/${parameters.templateDir}/css_xhtml/controlfooter.ftl" />
