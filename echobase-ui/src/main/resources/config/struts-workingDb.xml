<!--
  #%L
  EchoBase :: UI
  %%
  Copyright (C) 2011 - 2012 Ifremer, Codelutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  #L%
  -->
<!DOCTYPE struts PUBLIC
  "-//Apache Software Foundation//DTD Struts Configuration 2.3//EN"
  "http://struts.apache.org/dtds/struts-2.3.dtd">

<struts>

  <package name="workingDb" extends="loggued" namespace="/workingDb">

    <result-types>

      <result-type name="redirectToShowList"
                   class="org.apache.struts2.dispatcher.ServletActionRedirectResult">
        <param name="actionName">showList</param>
        <param name="namespace">/workingDb</param>
        <param name="conf.topiaId">${conf.topiaId}</param>
      </result-type>

      <result-type name="showList"
                   class="org.apache.struts2.dispatcher.ServletDispatcherResult">
        <param name="location">/WEB-INF/jsp/workingDb/manage.jsp</param>
        <param name="conf.topiaId">${conf.topiaId}</param>
      </result-type>
    </result-types>

    <!-- Get db connexion informations -->
    <action name="information"
            class="fr.ifremer.echobase.ui.actions.workingDb.Information">
      <interceptor-ref name="basicStackLogguedWithdb"/>
      <result>/WEB-INF/jsp/workingDb/information.jsp</result>
    </action>

    <!-- Display workingDbConfiguration main page -->
    <action name="showList"
            class="fr.ifremer.echobase.ui.actions.workingDb.ShowList">
      <result type="showList"/>
    </action>

    <!-- Create a new workingDbConfiguration -->
    <action name="create" method="createConf"
            class="fr.ifremer.echobase.ui.actions.workingDb.Create">
      <result name="input" type="showList"/>
      <result type="redirectToShowList"/>
    </action>

    <!-- Clone the workingDbConfiguration -->
    <action name="clone" method="cloneConf"
            class="fr.ifremer.echobase.ui.actions.workingDb.Create">
      <interceptor-ref name="prepareParamsStackLoggued"/>
      <result name="input" type="showList"/>
      <result type="redirectToShowList"/>
    </action>

    <!-- Save the workingDbConfiguration -->
    <action name="save" method="execute"
            class="fr.ifremer.echobase.ui.actions.workingDb.Create">
      <interceptor-ref name="prepareParamsStackLoggued"/>
      <result name="input" type="showList"/>
      <result type="redirectToShowList"/>
    </action>

    <!-- Confirm to delete the workingDbConfiguration -->
    <action name="confirmDelete" method="confirmDelete"
            class="fr.ifremer.echobase.ui.actions.workingDb.Delete">
      <interceptor-ref name="prepareParamsStackLoggued"/>
      <result>/WEB-INF/jsp/workingDb/confirmDelete.jsp</result>
    </action>

    <!-- Delete the workingDbConfiguration -->
    <action name="delete" method="execute"
            class="fr.ifremer.echobase.ui.actions.workingDb.Delete">
      <interceptor-ref name="prepareParamsStackLoggued"/>
      <result name="input" type="showList"/>
      <result type="redirectToShowList"/>
    </action>

    <!-- Create a new postgres database and connect to it -->
    <action name="createPostgresDb" method="execute"
            class="fr.ifremer.echobase.ui.actions.workingDb.CreatePostgresDb">
      <interceptor-ref name="prepareParamsStackLoggued"/>
      <result name="input" type="showList"/>
      <result type="redirectAction">
        <param name="actionName">information</param>
        <param name="namespace">/workingDb</param>
      </result>
    </action>

    <!-- Connect to selected workingDbConfiguration -->
    <action name="connect" method="execute"
            class="fr.ifremer.echobase.ui.actions.workingDb.Connect">
      <interceptor-ref name="prepareParamsStackLoggued"/>
      <result name="input" type="showList"/>
      <result type="redirectAction">
        <param name="actionName">information</param>
        <param name="namespace">/workingDb</param>
      </result>
    </action>

    <!-- Disconnect from current workingDbConfiguration -->
    <action name="disconnect"
            class="fr.ifremer.echobase.ui.actions.workingDb.Disconnect">
      <result type="redirectToShowList"/>
    </action>

    <!-- Add echobase spatial tables, functions and triggers to working db -->
    <action name="addSpatial"
            class="fr.ifremer.echobase.ui.actions.workingDb.AddSpatial">
      <result type="redirectAction">
        <param name="actionName">information</param>
        <param name="namespace">/workingDb</param>
      </result>
    </action>

    <!-- Get db modifications -->
    <action name="logs"
            class="fr.ifremer.echobase.ui.actions.EchoBaseActionSupport">
      <interceptor-ref name="basicStackLogguedWithdb"/>
      <result>/WEB-INF/jsp/workingDb/modifications.jsp</result>
    </action>

    <!-- Download jdbc pilot driver application archive -->
    <action name="downloadDriver"
            class="fr.ifremer.echobase.ui.actions.workingDb.DownloadDriver">
      <result type="stream">
        <param name="contentType">${contentType}</param>
        <param name="contentLength">${contentLength}</param>
        <param name="contentDisposition">attachment; filename="${fileName}"</param>
      </result>
    </action>

    <!-- Get modification logs entries -->
    <action name="getEntityModificationLogs" method="entityModificationLogs"
            class="fr.ifremer.echobase.ui.actions.dbeditor.GetEntities">
      <interceptor-ref name="basicStackLogguedWithdb"/>
      <result type="jsonWithPager"/>
    </action>

    <!-- Get dashboard values -->
    <action name="getDashboardVoyageImportLogs" method="dashboardVoyageImportLogs"
            class="fr.ifremer.echobase.ui.actions.dbeditor.GetEntities">
      <interceptor-ref name="basicStackLogguedWithdb"/>
      <result type="jsonWithPager"/>

    </action>
    <action name="getDashboardMooringImportLogs" method="dashboardMooringImportLogs"
            class="fr.ifremer.echobase.ui.actions.dbeditor.GetEntities">
      <interceptor-ref name="basicStackLogguedWithdb"/>
      <result type="jsonWithPager"/>

    </action>

    <!-- Get import log detail -->
    <action name="getDashboardImportLog" method="execute"
            class="fr.ifremer.echobase.ui.actions.workingDb.GetImportLogDetail">
      <interceptor-ref name="basicStackLogguedWithdb"/>
      <result type="json"/>
    </action>

    <!-- Get entity detail -->
    <action name="getDashboardVoyage" method="execute"
            class="fr.ifremer.echobase.ui.actions.workingDb.GetVoyage">
      <interceptor-ref name="basicStackLogguedWithdb"/>
      <result type="json"/>
    </action>
    <action name="getDashboardMooring" method="execute"
            class="fr.ifremer.echobase.ui.actions.workingDb.GetMooring">
      <interceptor-ref name="basicStackLogguedWithdb"/>
      <result type="json"/>
    </action>
    
    <!-- Show dashboard -->
    <action name="dashboard" method="execute"
            class="fr.ifremer.echobase.ui.actions.workingDb.DashBoard">
      <interceptor-ref name="basicStackLogguedWithdb"/>
      <result>/WEB-INF/jsp/workingDb/dashboard.jsp</result>
    </action>

    <!-- Download import file  -->
    <action name="downloadImportFile" method="execute"
            class="fr.ifremer.echobase.ui.actions.workingDb.DownloadImportFile">
      <interceptor-ref name="prepareParamsStackLogguedWithDb"/>
      <result type="stream">
        <param name="contentType">${contentType}</param>
        <param name="contentLength">${contentLength}</param>
        <param name="contentDisposition">attachment; filename="${filename}"</param>
      </result>
    </action>

    <!-- Download export file  -->
    <action name="downloadExportFile" method="execute"
            class="fr.ifremer.echobase.ui.actions.workingDb.DownloadExportFile">
      <interceptor-ref name="prepareParamsStackLogguedWithDb"/>
      <result type="stream">
        <param name="contentType">${contentType}</param>
        <param name="contentLength">${contentLength}</param>
        <param name="contentDisposition">attachment; filename="${filename}"</param>
      </result>
    </action>

  </package>

</struts>
