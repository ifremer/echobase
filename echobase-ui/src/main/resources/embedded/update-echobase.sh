#!/bin/sh

# version to deploy
ECHOBASE_VERSION=$1

TOMCAT_HOME_DIRECTORY=/var/lib/tomcat8
TOMCAT_WEBAPPS_DIRECTORY=${TOMCAT_HOME_DIRECTORY}/webapps

if [ ! -d ${TOMCAT_WEBAPPS_DIRECTORY} ]; then
 echo "Could not find tomcat 8 webapps directory at $TOMCAT_WEBAPPS_DIRECTORY"
 exit 1
fi

ECHOBASE_DIRECTORY=/var/local/echobase
if [ ! -d ${ECHOBASE_DIRECTORY} ]; then
 echo "Could not find echobase directory at $ECHOBASE_DIRECTORY"
 exit 1
fi

if [ ! -d ${ECHOBASE_DIRECTORY}/war ]; then
 echo "Could not find echobase war directory at $ECHOBASE_DIRECTORY/war"
 exit 1
fi

cd ${ECHOBASE_DIRECTORY}/war

WAR_FILENAME=echobase-ui-${ECHOBASE_VERSION}-full.war
WAR_URL=https://nexus.nuiton.org/nexus/content/repositories/other-releases/fr/ifremer/echobase/echobase-ui/${ECHOBASE_VERSION}/${WAR_FILENAME}

wget ${WAR_URL}
if [ ! -f ${WAR_FILENAME} ]; then
 echo "Could not find echobase war at $WAR_URL"
 exit 1
fi

rm -rf ${TOMCAT_WEBAPPS_DIRECTORY}/echobase.war
rm -rf ${TOMCAT_HOME_DIRECTORY}/Catalina/localhost/echobase

while [ -d ${TOMCAT_WEBAPPS_DIRECTORY}/echobase ]; do
  echo "Waiting for echobase to be nicely undeployed"
  sleep 1
done

service tomcat8 stop

cd ${ECHOBASE_DIRECTORY}

unlink echobase.war
ln -s war/${WAR_FILENAME} echobase.war
cp echobase.war ${TOMCAT_WEBAPPS_DIRECTORY}

chown tomcat8:echobase -R ${ECHOBASE_DIRECTORY}

service tomcat8 start
