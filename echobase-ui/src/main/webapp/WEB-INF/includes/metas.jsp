<%--
  #%L
  EchoBase :: UI
  %%
  Copyright (C) 2011 Ifremer, Codelutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  #L%
  --%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="d" uri="http://www.opensymphony.com/sitemesh/decorator" %>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
  <title>EchoBase - <d:title default="EchoBase"/></title>
  <d:head/>
  <link rel="stylesheet" type="text/css"
        href="<s:url value='/css/screen.css' />"/>
  <link rel="icon" type="image/png"
        href="<s:url value='/images/logo_codelutin.png' />"/>
  <sj:head jqueryui="true" jquerytheme="echobase-theme" debug="true" compressed="false"/>
  <script type="text/javascript">

    jQuery(document).ready(function () {

      $.addCheckFileSize = function(uploadFileMaxLength) {

        var locale = '<s:property value="locale"/>';
        console.info("Locale: "+locale);
        $('input[type="file"]').bind("change", function() {
          if (this.value) {
            var file = this.files[0];
            var length = file.size;

            if (length > uploadFileMaxLength) {
              var message = locale.indexOf('fr')>0
                      ?"File " + file.name + " exceeded allowed size limit!\n o Max size allowed: " + uploadFileMaxLength + "\n o file size: " + length
                      :"Le fichier " + file.name + " depasse la taille maximum autorisee!\n o taille maximum autorisee : " + uploadFileMaxLength + "\n o taille du fichier : " + length;
              alert(message);
              this.value = '';
            }
          }
        });
      };

    });
  </script>
</head>
