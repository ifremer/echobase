<%--
  #%L
  EchoBase :: UI
  %%
  Copyright (C) 2011 Ifremer, Codelutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  #L%
  --%>
<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>

<br/>
<fieldset>
  <legend>
    <s:property value="actionResumeTitle"/>
  </legend>

  <s:if test="error != null">
    <s:text name="echobase.common.actionError"/>
    <strong>
      <s:property value="error.message"/>
    </strong>
    <pre id="errorStack">
      <code>
        <s:property value="errorStack"/>
      </code>
    </pre>
  </s:if>
  <s:else>
    <pre>
    <s:property value="result"/>
    </pre>
  </s:else>
</fieldset>

