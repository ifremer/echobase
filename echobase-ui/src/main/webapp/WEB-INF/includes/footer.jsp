<%--
  #%L
  EchoBase :: UI
  %%
  Copyright (C) 2011 Ifremer, Codelutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  #L%
  --%>
<%@ page import="fr.ifremer.echobase.ui.actions.EchoBaseActionSupport" %>

<%@ taglib prefix="s" uri="/struts-tags" %>

<br/>
<hr/>
<div id='footer'>
  <ul class="clearfix">
    <li>
      <span style="font-size: 140%">
        EchoBase <s:property value="#application.echobaseApplicationContext.configuration.applicationVersion"/>
        &nbsp;&copy;&nbsp;2011-2017
      </span>
      <a href="http://www.ifremer.fr">Ifremer</a>
      <a href="http://www.codelutin.com" title="Code Lutin" target="_blank" rel="noopener noreferrer">Code Lutin</a>
    </li>
    <li>
      <s:a href="http://www.gnu.org/licenses/agpl.html" target='license'>
        <s:text name="echobase.info.license"/>
      </s:a>
    </li>
    <li>
      <s:a href="http://forge.codelutin.com/projects/echobase/issues/new"
           target='#doc'>
        <s:text name="echobase.info.reportBug"/>
      </s:a>
    </li>
    <li>
      <s:a
        href="http://list.forge.codelutin.com/cgi-bin/mailman/listinfo/echobase-users"
        target='#mailingList'>
        <s:text name="echobase.info.userSupport"/>
      </s:a>
    </li>

    <li>
      <s:a href="http://echobase.codelutin.com/v/latest/index.html" target='#doc'>
        <s:text name="echobase.info.documentation.short"/>
      </s:a>
    </li>
  </ul>
</div>
