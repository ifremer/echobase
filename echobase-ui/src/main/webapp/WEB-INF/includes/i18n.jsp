<%--
  #%L
  EchoBase :: UI
  %%
  Copyright (C) 2011 Ifremer, Codelutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  #L%
  --%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<s:set name="usingEnglish"><s:text name='echobase.action.usingEnglish'/></s:set>
<s:set name="usingFrench"><s:text name='echobase.action.usingFrench'/></s:set>
<s:set name="toEnglish"><s:text name='echobase.action.toEnglish'/></s:set>
<s:set name="toFrench"><s:text name='echobase.action.toFrench'/></s:set>

<s:if
  test="%{#session['WW_TRANS_I18N_LOCALE'] == null || #session['WW_TRANS_I18N_LOCALE'].language == 'fr'}">
  <s:a href='' cssClass="francais i18nActive" title="%{usingFrench}">
    <s:param name="request_locale">fr</s:param>
  </s:a>
</s:if>
<s:else>
  <s:a namespace="/" action="changeLang" cssClass="francais i18nNonActive"
       title="%{toFrench}">
    <s:param name="request_locale">fr</s:param>
  </s:a>
</s:else>

<s:if
  test="%{#session['WW_TRANS_I18N_LOCALE'] != null && #session['WW_TRANS_I18N_LOCALE'].language == 'en'}">
  <s:a href='' cssClass="anglais i18nActive" title="%{usingEnglish}">
    <s:param name="request_locale">en</s:param>
  </s:a>
</s:if>
<s:else>
  <s:a namespace="/" action="changeLang" cssClass="anglais i18nNonActive"
       title="%{toEnglish}">
    <s:param name="request_locale">en</s:param>
  </s:a>
</s:else>

