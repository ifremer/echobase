<%--
  #%L
  EchoBase :: UI
  %%
  Copyright (C) 2011 Ifremer, Codelutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  #L%
  --%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>

<div class='displayBlock top'>

  <%@ include file="i18n.jsp" %>

  <div class='floatLeft'>
    <s:a action="home" cssClass="logo" namespace="/">EchoBase</s:a>
  </div>

  <div id='headerRight'>
    <div>
      <ul>
        <li>
          <s:a action="logout" namespace="/user" id='logout'
               alt='%{getText("echobase.tooltip.logout")}'
               title='%{getText("echobase.tooltip.logout")}'>
            <s:text name="echobase.action.logout"/>
          </s:a>
        </li>
        <li>
          <s:text name="echobase.label.user.login">
            <s:param>
              <s:property value="#session.echoBaseSession.user.email"/>
            </s:param>
          </s:text>
        </li>
        <li>|</li>
        <s:if test="#session.echoBaseSession.workingDbSelected">
          <li>
            <s:a action="disconnect" namespace="/workingDb" id='disconnect'
                 title='%{getText("echobase.tooltip.disconnectWorkingDb")}'
                 alt='%{getText("echobase.tooltip.disconnectWorkingDb")}'>
              <s:text name="echobase.action.workingDbconfiguration.disconnect"/>
            </s:a>
          </li>
          <li>
            <s:text name="echobase.label.workingDbSelected">
              <s:param>
                <s:property
                  value="#session.echoBaseSession.workingDbConfiguration.url"/>
              </s:param>
            </s:text>
          </li>
        </s:if>
        <s:else>
          <li>
            <s:a action="showList" namespace="/workingDb" id='connect'
                 title='%{getText("echobase.tooltip.selectWorkingDb")}'
                 alt='%{getText("echobase.tooltip.selectWorkingDb")}'>
              <s:text name="echobase.action.workingDbconfiguration.connect"/>
            </s:a>
          </li>
          <li>
            <s:text name="echobase.info.no.workingDb.selected"/>
          </li>
        </s:else>
      </ul>
    </div>
    <br/>
    <br/>
  </div>
  <div class="cleanBoth menu">
    <ul>
      <s:if test="#session.echoBaseSession.user.admin">
        <s:if test="#session.echoBaseSession.workingDbSelected">
          <li>
            <s:a action="configureInput" namespace="/importDb" cssClass="impBase">
            <span><s:text name="echobase.menu.importDb"/></s:a></span>
          </li>
          <li>
            <s:a action="configureInput" namespace="/exportDb" cssClass="expBase">
            <span><s:text name="echobase.menu.exportDb"/></s:a></span>
          </li>
          <li>
            <s:a action="selectImportType" namespace="/importData"
                 method="input" cssClass="impDonnee">
            <span><s:text name="echobase.menu.importData"/></s:a></span>
          </li>
          <li>
            <s:a action="list" namespace="/exportQuery"
                 cssClass="expDonnee">
              <span><s:text name="echobase.menu.export"/></span>
            </s:a>
          </li>
          <li>
            <s:a action="importLogs" namespace="/removeData"
                 cssClass="delDonnee">
            <span><s:text name="echobase.menu.removeData"/></s:a></span>
          </li>
          <li>
            <s:a action="dbeditor" namespace="/dbeditor" cssClass="modif">
              <span><s:text name="echobase.menu.editData"/></span>
            </s:a>
          </li>
          <li>
            <s:a action="show" namespace="/spatial" cssClass="spatial">
              <span>
                <s:text name="echobase.menu.showSpatialData"/>
              </span>
            </s:a>
          </li>
          <li>
            <s:a action="logs" namespace="/workingDb" cssClass="journal">
              <span><s:text name="echobase.menu.logs"/></span>
            </s:a>
          </li>
          <li>
            <s:a action="dashboard" namespace="/workingDb" cssClass="dashboard">
              <span><s:text name="echobase.menu.dashboard"/></span>
            </s:a>
          </li>
          <li>
            <s:a action="information" namespace="/workingDb" cssClass="info">
              <span>
                <s:text name="echobase.menu.connectToDbInformations"/>
              </span>
            </s:a>
          </li>
          <li>
            <s:a action="configure" namespace="/embeddedApplication"
                 method="input" cssClass="appli">
            <span>
              <s:text name="echobase.menu.createEmbeddedApplication"/>
            </span>
            </s:a>
          </li>
          <li>
            <s:a action="configureInput" namespace="/exportAtlantos" cssClass="expBase">
            <span>
              <s:text name="echobase.menu.exportAtlantos"/>
            </span>
            </s:a>
          </li>
          <li>
            <s:a action="configureInput" namespace="/exportCoser" cssClass="expBase">
            <span>
              <s:text name="echobase.menu.exportCoser"/>
            </span>
            </s:a>
          </li>
        </s:if>
        <li>
          <s:a action="userList" namespace="/user" cssClass="user">
            <span><s:text name="echobase.menu.users"/></span>
          </s:a>
        </li>
      </s:if>
      <s:else>
        <s:if test="#session.echoBaseSession.workingDbSelected">
          <li>
            <s:a action="list" namespace="/exportQuery"
                 cssClass="expDonnee">
              <span><s:text name="echobase.menu.export"/></span>
            </s:a>
          </li>
          <li>
            <s:a action="dbeditor" namespace="/dbeditor" cssClass="modif">
              <span><s:text name="echobase.menu.viewData"/></span>
            </s:a>
          </li>
          <li>
            <s:a action="show" namespace="/spatial" cssClass="info">
              <span>
                <s:text name="echobase.menu.showSpatialData"/>
              </span>
            </s:a>
          </li>
          <li>
            <s:a action="logs" namespace="/workingDb" cssClass="journal">
              <span><s:text name="echobase.menu.logs"/></span>
            </s:a>
          </li>
          <li>
            <s:a action="information" namespace="/workingDb" cssClass="info">
              <span>
                <s:text name="echobase.menu.connectToDbInformations"/>
              </span>
            </s:a>
          </li>
          <li>
            <s:a action="dashboard" namespace="/workingDb" cssClass="dashboard">
              <span><s:text name="echobase.menu.dashboard"/></span>
            </s:a>
          </li>
        </s:if>
      </s:else>
    </ul>
  </div>
</div>
<hr/>
