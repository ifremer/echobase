<%--
  #%L
  EchoBase :: UI
  %%
  Copyright (C) 2011 Ifremer, Codelutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  #L%
  --%>
<%@ page language="java" contentType="text/html; charset=utf-8"
         pageEncoding="utf-8" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="d" uri="http://www.opensymphony.com/sitemesh/decorator" %>
<html xmlns="http://www.w3.org/1999/xhtml">

<%-- metas in head --%>
<%@ include file="/WEB-INF/includes/metas.jsp" %>

<script type="text/javascript">

  jQuery(document).ready(function () {

    // apply common style
    $('fieldset').attr('class', 'ui-corner-all')
  });
</script>

<body>

<%-- header --%>
<%@ include file="/WEB-INF/includes/header.jsp" %>

<%-- body --%>

<div>
  <div class="containerSize">
    <div id="body" class="ui-corner-all">

      <h2><d:title default="EchoBase"/></h2>

      <s:if test="%{#session.echoBaseSession.withFlashMessages}">
        <div class="info_success clearfix">
            <s:iterator value="%{#session.echoBaseSession.flashMessages}" var="message">
          <ul class="actionMessages fleft">

              <li><span><s:property value="#message" escapeHtml="false"/></span></li>

          </ul>
            </s:iterator>
        </div>
      </s:if>

      <s:if test="#session.echoBaseSession.withFlashWarnings">
        <div class="info_warning clearfix">
          <ul class="actionWarnings fleft">
            <s:iterator value="%{#session.echoBaseSession.flashWarnings}" var="message">
              <li><span><s:property value="#message" escapeHtml="false"/></span>
              </li>
            </s:iterator>
          </ul>
        </div>
      </s:if>
      <s:if test="#session.echoBaseSession.withFlashErrors">
        <div class="info_error clearfix">
          <ul class="actionErrors fleft">
            <s:iterator value="%{#session.echoBaseSession.flashErrors}" var="message">
              <li><span><s:property value="#message" escapeHtml="false"/></span>
              </li>
            </s:iterator>
          </ul>
        </div>
      </s:if>

      <%--<s:if test="hasActionMessages()">--%>
      <%--<div class="info_success">--%>
      <%--<s:actionmessage/>--%>
      <%--</div>--%>
      <%--</s:if>--%>

      <%--<s:if test="hasActionErrors()">--%>
      <%--<div class="info_error">--%>
      <%--<s:actionerror/>--%>
      <%--</div>--%>
      <%--</s:if>--%>

      <d:body/>
    </div>

    <%@ include file="/WEB-INF/includes/footer.jsp" %>
  </div>
</div>
</body>
</html>
