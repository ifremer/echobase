<%--
  #%L
  EchoBase :: UI
  %%
  Copyright (C) 2011 Ifremer, Codelutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  #L%
  --%>
<%@ page language="java" contentType="text/html" pageEncoding="utf-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<title><s:text name="echobase.title.dbEditor"/></title>

<fieldset>
  <legend><s:text name="echobase.info.dbeditor.propertyDiffsResult"/></legend>

  <s:label key="result.entityType"
           label="%{getText('echobase.common.entityType')}"/>

  <s:label key="result.importFileName"
           label="%{getText('echobase.label.importFile')}"/>

  <s:if test="hasActionErrors()">
    <s:label key="error.message" labelSeparator=""
             label="%{getText('echobase.common.importError')}"/>
    <pre id="errorStack">
      <code>
        <s:property value="errorStack"/>
      </code>
    </pre>
  </s:if>
  <s:else>

      <s:label key="result.numberCreated"
               label="%{getText('echobase.label.numberOfCreatedEntities')}"/>

      <s:label key="result.numberUpdated"
               label="%{getText('echobase.label.numberOfUpdatedEntities')}"/>
  </s:else>

</fieldset>
<s:a namespace="/dbeditor" action="dbeditor">
  <s:param name="entityType" value="%{result.entityType}"/>
  <s:text name="echobase.action.return"/>
</s:a>
