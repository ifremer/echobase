<%--
#%L
  EchoBase :: UI
  %%
  Copyright (C) 2011 Ifremer, Codelutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  #L%
--%>
<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags" %>

<script type="text/javascript"
        src="<s:url value='/js/gridHelper.js' />"></script>

<s:url id="getTableData" action="getTableData" namespace="/dbeditor"/>

<s:set name="tableSelected" value="%{entityType != null}"/>

<title>
  <s:if test="admin">
    <s:text name="echobase.title.dbEditor"/>
  </s:if>
  <s:else>
    <s:text name="echobase.title.dbEditor.read"/>
  </s:else>
</title>

<script type="text/javascript">

  jQuery(document).ready(function () {

    // Hack to make name=id on sj tags
    var combos = $('select[name=""]');
    combos.each(function (obj) {
      this.name = this.id;
    });

    var pickers = $('input.hasDatepicker');
    pickers.each(function (obj) {
      this.name = this.id;
    });

    // on table name change, let's reload reload this page
    $('[name="entityType"]').change(function (event) {
      window.location = '<s:url action="dbeditor" namespace="/dbeditor"/>?' +
                        $.param({entityType:this.value});
    });

    // on reset let's just reload the selected row
    $('[name="resetAction"]').click(function (event) {
      $.publish('datas-rowSelect')
    });

    // on grid selected clear, let's remove the edit form
    $.addClearSelectTopic('datas', function (event) {

      $('#noRowSelected').show();
      $('#editForm').hide();
    });

    $.addEvenAndOddClasses('datas');

    // on row selection, let's load the edit form
    $.addRowSelectTopic('datas', function (event) {

      $('#noRowSelected').hide();
      $('#editForm').show();

      // get selected id
      var id = jQuery.struts2_jquery['datas']['selectedRow'];

      // get table name
      var entityType = "${entityType}";

      // get entity value from json
      jQuery.getJSON("${getTableData}",
                     { "entityType":entityType, "id":id }, function (result) {

          // get metadatas from the response
          var metas = result.metas;

          // iterate on result to push in form the value to edit

          $.each(result.datas, function (propertyName, value) {

            if (propertyName == 'topiaId') {

              // push the id of row
              $("#" + propertyName).val(value);
              $("#entityId").val(value);
              return;
            }

            if (propertyName == 'length_') {

              // rename it to length
              propertyName = "length";
            }

            var meta = metas[propertyName];

            if (!meta) {

              // not a meta to treate
              return;
            }

            var type = meta['typeSimpleName'];
            if (meta['fK']) {
              type = 'FK';
            }

            // get concerned editor
            var editor = $("#" + propertyName);

            switch (type) {
              case 'boolean':
              case 'Boolean':
                editor.attr('checked', !!(value == "Y"));
                break;

              case 'Date':
                if (!!value) {
                  // Update date (only if exists)
                  updateDate(value, editor);
                }
                // always set value to simple text editor otherwise it
                // won't work anylonger after a blank previous value :(
                editor.val(value);
                break;

              case 'FK':
                // select value in the select box
                $('#' + propertyName + ' option[value="' + value + '"]').attr('selected', true);
                break;

              default:
                editor.val(value);
            }
          });
        });
    });
  });

  function formatEntityLabel(cellvalue, options, rowObject) {
    var columnName = options.colModel.name + "_lib";
    return rowObject[columnName];
  }

  function updateDate(value, editor) {

    // parse date formated like : dd/MM/yyyy HH:mm:ss
    var date = parseDate(value);

    // get timePicker instance
    var inst = $.datepicker._getInst(editor[0]);

    // set date
    $.datepicker._setDate(inst, date);

    // set time
    $.datepicker._setTime(inst, date);
  }

  function parseDate(value) {

    // parse date formated like : dd/MM/yyyy HH:mm:ss
    var dateString = value.substring(0, 10);
    var dateSplited = dateString.split("/");

    var hourString = value.substring(11, 19);
    var hourSplited = hourString.split(":");

    var day = parseInt(dateSplited[0]);

    // FIXME sletellier 20111122 : currently, one month more are displayed....
    var month = parseInt(dateSplited[1]) - 1;
    var year = parseInt(dateSplited[2]);
    var hour = parseInt(!!hourSplited[0] ? hourSplited[0] : 0);
    var minute = parseInt(!!hourSplited[1] ? hourSplited[1] : 0);
    var second = parseInt(hourSplited[2] ? hourSplited[2] : 0);

    // update date
    return new Date(year, month, day, hour, minute, second, 0);
  }
</script>

<s:select key="entityType" label='%{getText("echobase.common.entityType")}'
          list="entityTypes" headerKey="" headerValue=""/>

<br class="clearBoth"/>

<s:if test="tableSelected">

  <sj:tabbedpanel id="io" collapsible="true">
    <s:if test="admin">
      <sj:tab id="tab_importForm" target="importForm"
              key='echobase.title.importTable'/>

      <s:form id="importForm" namespace="/dbeditor" method="post"
              enctype="multipart/form-data">

        <s:hidden key="entityType" label=''/>
        <s:file key="importFile" requiredLabel="true"
                label="%{getText('echobase.label.importFile')}"/>

        <s:checkbox key='createIfNotFound' value='true'
                    label='%{getText("echobase.label.createIfNotFound")}'/>
        <br/>
        <s:submit key="echobase.action.importTable" action="doImport"
                  align="right"/>
      </s:form>
    </s:if>
    <s:if test="%{entityType.name() == 'Gear'}">
      <sj:tab id="tab_importGearCharacteristicValuesForm" target="importGearCharacteristicValuesForm"
              key="echobase.title.importGearCharacteristics"/>

      <s:form id="importGearCharacteristicValuesForm" namespace="/dbeditor" method="post"
              enctype="multipart/form-data">

        <s:hidden key="entityType" label=''/>
        <s:file key="importFile" requiredLabel="true" label="%{getText('echobase.label.importFile')}"/>

        <br/>
        <s:submit key="echobase.action.importTable" action="doImportGearCharacteristics"
                  align="right"/>
      </s:form>
    </s:if>
    <sj:tab id="tab_exportForm" target="exportForm"
            key="echobase.title.exportTable"/>

    <s:form id="exportForm" namespace="/dbeditor" method="post">
      <s:hidden key="entityType" label=''/>
      <s:textfield key="exportFileName" requiredLabel="true" size="100"
                   label="%{getText('echobase.label.exportFileName')}"/>

      <s:checkbox key='exportAsSeen' value='false'
                  label='%{getText("echobase.label.exportAsSeen")}'/>
      <br/>
      <s:submit key="echobase.action.exportTable" action="doExport"
                align="right"/>
    </s:form>

  </sj:tabbedpanel>

</s:if>
<br class="clearBoth"/>

<s:if test="tableSelected">
  <s:url id="loadUrl" action="getTableDatas" namespace="/dbeditor"
         escapeAmp="false">
    <s:param name="entityType" value="%{entityType}"/>
  </s:url>

  <s:set var="tableI18nName" value="%{entityTypes[entityType]}"/>

  <sjg:grid id="datas" dataType="json" href="%{loadUrl}" gridModel="datas"
            pager="true" pagerButtons="true" pagerInput="true" navigator="true"
            autowidth="true" rownumbers="false" viewrecords="true"
            navigatorEdit="false" navigatorSearch="true"
            navigatorDelete="false" navigatorAdd="false"
            rowList="10,15,20,50,100,250,500" rowNum="10"
            onSelectRowTopics="datas-rowSelect"
            onCompleteTopics="datas-clearSelect,datas-CompleteTopics"
            navigatorSearchOptions="{multipleGroup:false,multipleSearch:true,sopt:['eq','ne','cn','nc','bw','bn','ew','en','lt','le','gt','ge','nu','nn']}">

    <sjg:gridColumn name="id" title="id" hidden="true" editable="true"/>

    <s:iterator value="columnMetas" var="meta" status="status">

      <s:if test="#meta.fK">
        <sjg:gridColumn name="%{#meta.name}"
                        title="%{#meta.name}"
                        formatter='formatEntityLabel'
                        searchoptions="{sopt:['eq','ne','cn','nc','bw','bn','ew','en','nu','nn']}"
                        sortable="true"/>
      </s:if>
      <s:elseif test="#meta.name == 'id'">
        <sjg:gridColumn name="ID"
                        title="id"
                        sortable="true"/>
      </s:elseif>
      <s:elseif test="#meta.date || #meta.number">
        <sjg:gridColumn name="%{#meta.name}"
                        title="%{#meta.name}"
                        searchoptions="{sopt:['eq','ne','cn','nc','bw','bn','ew','en','lt','le','gt','ge','nu','nn']}"
                        sortable="true"/>
      </s:elseif>
      <s:else>
        <sjg:gridColumn name="%{#meta.name}"
                        title="%{#meta.name}"
                        searchoptions="{sopt:['eq','ne','cn','nc','bw','bn','ew','en','nu','nn']}"
                        sortable="true"/>
      </s:else>
    </s:iterator>

  </sjg:grid>

  <br class="clearBoth"/>

  <div class="showAtLoad hidden">
    <fieldset id="noRowSelected">
      <legend>
        <s:if test="admin">
          <s:text name="echobase.legend.dbeditor.edit">
            <s:param value="%{tableI18nName}"/>
          </s:text>
        </s:if>
        <s:else>
          <s:text name="echobase.legend.dbeditor.show">
            <s:param value="%{tableI18nName}"/>
          </s:text>
        </s:else>
      </legend>
      <i><s:text name="echobase.message.no.row.selected"/>.</i>
    </fieldset>

    <s:form id="editForm" namespace="/dbeditor">
      <fieldset>
        <legend>
          <s:if test="admin">
            <s:text name="echobase.legend.dbeditor.edit">
              <s:param value="%{tableI18nName}"/>
            </s:text>
          </s:if>
          <s:else>
            <s:text name="echobase.legend.dbeditor.show">
              <s:param value="%{tableI18nName}"/>
            </s:text>
          </s:else>
        </legend>

        <s:hidden key="entityType" label=''/>

        <s:textfield id="topiaId" name='topiaId' readonly="true"
                     label='%{getText("echobase.common.id")}' size="250"/>

        <s:iterator value="columnMetas" var="meta" status="status">
          <s:set var="metaName" value='%{#meta.name}'/>
          <s:if test="#meta.fK">
            <s:url id="urlName" action="getForeignEntities"
                   namespace="/dbeditor"
                   escapeAmp="false">
              <s:param name="entityType" value="%{#meta.typeSimpleName}"/>
            </s:url>
            <sj:select id="%{#metaName}" label="%{#metaName}"
                       readonly="%{!admin}"
                       href="%{urlName}" list="entities" emptyOption="true"/>
          </s:if>
          <s:else>
            <s:if test='#meta.columnType == "string"'>
              <s:textfield id="%{#metaName}" name="%{#metaName}"
                           label="%{#metaName}" disabled="%{#meta.fK}"
                           value="" size="250" readonly="%{!admin}"/>
            </s:if>
            <s:elseif test='#meta.columnType == "boolean"'>
              <s:checkbox id="%{#metaName}" name="%{#metaName}"
                          disabled="%{!admin}"
                          label="%{#metaName}" value="%{false}"/>
            </s:elseif>
            <s:elseif test='#meta.columnType == "date"'>
              <sj:datepicker id="%{#metaName}" name="%{#metaName}"
                             label="%{#metaName}"
                             value="%{new java.util.Date()}"
                             displayFormat="dd/mm/yy" timepicker="true"
                             timepickerShowSecond="true" readonly="%{!admin}"
                             timepickerFormat="hh:mm:ss"/>
            </s:elseif>
          </s:else>
          <br/>
        </s:iterator>
      </fieldset>
      <s:if test="admin">
        <div class="toolbar">
          <ul class="toolbar floatRight">
            <li><s:submit name='resetAction' key="echobase.action.reset"
                          theme="simple" onclick="return false;"/>
            </li>
            <li><s:submit name="submitAction" key="echobase.action.save"
                          action="editTableData" theme="simple"/>
            </li>
            <li>
              <s:submit name="deleteAction" key="echobase.action.deleteEntity" action="deleteTableData" theme="simple"
                        onclick="return confirm(\"%{getText('echobase.common.confirmDelete')}\");"/>
            </li>
          </ul>
        </div>
      </s:if>
    </s:form>

  </div>
</s:if>

<script type="text/javascript">

  jQuery(document).ready(function () {

    <s:if test="admin">
    $('#tab_importForm > a').click();
    </s:if>
    <s:else>
    $('#tab_exportForm > a').click();
    </s:else>

    $('.showAtLoad').removeClass('showAtLoad').removeClass('hidden').show();
  });
</script>
