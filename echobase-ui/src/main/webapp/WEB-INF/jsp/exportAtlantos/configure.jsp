<%--
  #%L
  EchoBase :: UI
  %%
  Copyright (C) 2011 Ifremer, Codelutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  #L%
  --%>
<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<title>
  <s:text name="echobase.title.exportAtlantos"/>
</title>

<script type="text/javascript"
        src="<s:url value='/js/gridHelper.js' />"></script>

<script type="text/javascript">

  jQuery(document).ready(function () {

    $.autoSelectVoyageAndVessel(
      $('[name="model.voyageId"]'),
      $('[name="model.vesselId"]'),
      '<s:url action="getVesselsForVoyage" namespace="/importData"/>',
      '<s:property value="model.voyageId"/>',
      '<s:property value="model.vesselId"/>'
    );
  });
</script>

<s:form namespace="/exportAtlantos" method="POST" enctype="multipart/form-data">

    <fieldset>
        <legend>
            <s:text name="echobase.legend.exportAtlantos.configure"/>
        </legend>

        <s:select key="model.voyageId" requiredLabel="true"
                  label='%{getText("echobase.common.voyage")}'
                  list="voyages" headerKey="" headerValue=""/>

        <sj:select key="model.vesselId" requiredLabel="true"
                   label='%{getText("echobase.common.vessel")}'/>

    </fieldset>
    <br/>
    <s:submit action="configure" value='%{getText("echobase.action.export")}'/>

</s:form>

