<%--
  #%L
  EchoBase :: UI
  %%
  Copyright (C) 2011 Ifremer, Codelutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  #L%
  --%>
<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>

<s:set var="noError" value="!hasErrors()"/>

<script type="text/javascript">

  function generateSqlQuery() {

    var libreOfficeQuery = $('[name="libreOfficeQuery"]').val();

    var canQuit = true;

    $.ajax(
      {
        url:'<s:url action="newLibreOfficeQuery" namespace="/exportQuery"/>',
        data:{libreOfficeQuery:libreOfficeQuery},
        async:false,
        dataType:"json",
        success:function (data, textStatus) {
          var libreOfficeQuery = data.libreOfficeQuery;
          var resultQuery = data.resultQuery;
          $('[name="query.sqlQuery"]').val(resultQuery);
        }
      });

    if (canQuit) {
      return cancel();
    }
    return false;
  }

  function cancel() {
    $('#newLibreOfficeDialog').dialog('close');
    return false;
  }

</script>

<title><s:text name="echobase.title.export"/></title>
<div class="dialogContainer ui-corner-all">

  <fieldset class="ui-corner-all">
    <legend><s:text name="echobase.legend.libreOfficeQuery"/></legend>

    <s:textarea name="libreOfficeQuery" requiredLabel="true" cols="500" rows="8"
                theme="simple" value=""/>

    <ul class="toolbar floatRight">
      <li>
        <s:submit onclick="return cancel();" theme="simple"
                  key="echobase.action.cancel"/>
      </li>
      <li>
        <s:submit onclick="return generateSqlQuery();" theme="simple"
                  key="echobase.action.generateSqlQuery"/>
      </li>
    </ul>
  </fieldset>
</div>
