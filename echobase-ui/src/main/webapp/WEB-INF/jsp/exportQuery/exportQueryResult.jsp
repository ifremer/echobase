<%--
  #%L
  EchoBase :: UI
  %%
  Copyright (C) 2011 Ifremer, Codelutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  #L%
  --%>
<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags" %>


<script type="text/javascript"
        src="<s:url value='/js/gridHelper.js' />"></script>


<script type="text/javascript">

  jQuery(document).ready(function () {

    $.addEvenAndOddClasses('datas');
  });

</script>
<s:if test="hasActionErrors()">
  <div class="info_error">
    <s:actionerror/>
  </div>
</s:if>
<s:else>
  <s:url id="loadUrl" action="getExportQueryResult" namespace="/exportQuery"
         escapeAmp="false">
    <s:param name="queryId" value="%{queryId}"/>
  </s:url>

  <s:form action="downloadExportQueryResult" namespace="/exportQuery">

    <fieldset class="ui-corner-all">
      <legend><s:text name="echobase.legend.sqlQuery.result"/></legend>

      <s:hidden key="queryId" label=""/>
      <s:textfield key="fileName" requiredLabel="true" size="100"
                   label="%{getText('echobase.label.exportFileName')}"/>
      <s:submit value="%{getText('echobase.action.exportSqlData')}" align="left"/>
      <br/>
      <sjg:grid id="rows" caption="%{getText('echobase.header.request.result')}"
                dataType="json" href="%{loadUrl}" gridModel="datas"
                pager="true" pagerButtons="true" pagerInput="true"
                navigator="true" autowidth="true" rownumbers="false"
                navigatorEdit="false" navigatorDelete="false"
                navigatorSearch="false" navigatorRefresh="false"
                navigatorAdd="false" rowList="10,15,20,50,100,250,500"
                onCompleteTopics="datas-CompleteTopics"
                rowNum="10" viewrecords="true">

        <s:iterator value="columnNames" var="name" status="status">

          <sjg:gridColumn name="%{#name}" title="%{#name}" sortable="false"/>

        </s:iterator>

      </sjg:grid>
    </fieldset>
  </s:form>
</s:else>




