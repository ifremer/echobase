<%--
  #%L
  EchoBase :: UI
  %%
  Copyright (C) 2011 Ifremer, Codelutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  #L%
  --%>
<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>

<s:set var="noError" value="!hasErrors()"/>

<script type="text/javascript">

  function reloadPage(id) {
    if (!id) {

      // no edit (show list)
      window.location = "<s:url action='list' namespace='/exportQuery'/>";
    } else {

      // edit query
      window.location = "<s:url action='edit' namespace='/exportQuery'/>?" + $.param({'query.topiaId': id});
    }
    return false;
  }

  function newQuery() {
    window.location = "<s:url action='create' namespace='/exportQuery'/>";
    return false;
  }

  function openNewLibreOfficeDialog() {
    var dialog = $("#newLibreOfficeDialog");
    var url = "<s:url action='openNewLibreOfficeQuery' namespace='/exportQuery'/>";
    dialog.html("");
    dialog.load(url);
    dialog.dialog('open');
    return false;
  }

  <s:if test="%{queryExists}">
  function openConfirmDeleteQueryDialog() {
    var dialog = $("#confirmDeleteDialog");
    var url = "<s:url action='confirmDelete' namespace='/exportQuery'/>?" +
              $.param({ 'query.topiaId': '<s:property value="query.topiaId"/>'});
    dialog.html("");
    dialog.load(url);
    dialog.dialog('open');
    return false;
  }
  </s:if>
</script>

<title><s:text name="echobase.title.export"/></title>

<s:form id="exportQueryForm" namespace="/exportQuery">

  <ul class="toolbar floatLeft">
    <li>
      <s:submit onclick="return newQuery();" theme="simple"
                key="echobase.action.newQuery"/>
    </li>
  </ul>

  <fieldset>
    <legend><s:text name="echobase.label.sqlQueries"/></legend>
    <s:select key="query.topiaId" cssStyle="font-size: 140%"
              label="%{getText('echobase.label.sqlQueries')}"
              list="queries" headerKey="" headerValue="" theme="simple"/>
  </fieldset>

  <s:if test="%{!#noError or queryExists or newQuery}">
    <fieldset>
      <legend><s:text name="echobase.legend.sqlQuery.configuration"/></legend>

      <s:textarea key="query.name" requiredLabel="true" cols="160" rows="1"
                  readonly="%{queryExists and !canUpdateQuery}"
                  label="%{getText('echobase.label.query.name')}"/>

      <s:textarea key="query.description" requiredLabel="true" cols="160"
                  rows="2"
                  readonly="%{queryExists and !canUpdateQuery}"
                  label="%{getText('echobase.label.query.description')}"/>

      <s:textarea key="query.sqlQuery" requiredLabel="true" cols="160" rows="4"
                  readonly="%{queryExists and !canUpdateQuery}"
                  label="%{getText('echobase.label.query.sql')}"/>

      <s:if test="queryExists">
        <s:label value="%{query.lastModifiedDate}" readonly="true"
                 label="%{getText('echobase.common.lastModifiedDate')}"/>

        <s:label value="%{query.lastModifiedUser}" readonly="true"
                 label="%{getText('echobase.common.lastModifiedUser')}"/>
      </s:if>

      <ul id="dbeditorToolbar" class="toolbar floatRight">
        <li>
          <s:submit onclick="return openNewLibreOfficeDialog();" theme="simple"
                    key="echobase.action.importLibreOfficeQuery"/>
        </li>
        <s:if test="queryExists">
          <li>
            <s:submit onclick="return reloadPage('%{query.topiaId}');"
                      theme="simple" key="echobase.action.reloadSqlQuery"/>
          </li>
          <li>
            <s:submit action="clone" theme="simple"
                      key="echobase.action.clone"/>
          </li>
          <s:if test="canUpdateQuery">
            <li>
              <s:submit onclick="return openConfirmDeleteQueryDialog();"
                        theme="simple" key="echobase.action.delete"/>
            </li>
            <li>
              <s:submit action="save" theme="simple"
                        key="echobase.action.saveSqlQuery"/>
            </li>
          </s:if>
        </s:if>
        <s:if test="%{!queryExists}">
          <li>
            <s:submit action="list" theme="simple"
                      key="echobase.action.cancel"/>
          </li>
          <li>
            <s:submit action="save" theme="simple"
                      key="echobase.action.createSqlQuery"/>
          </li>
        </s:if>
      </ul>
    </fieldset>
  </s:if>
</s:form>

<br/>

<div id="resultGrid" class="cleanBoth"></div>

<sj:dialog id="newLibreOfficeDialog" resizable="false"
           title="%{getText('echobase.title.newLibreOfficeQuery')}"
           autoOpen="false" modal="true" width="780"/>

<sj:dialog id="confirmDeleteDialog" resizable="false"
           title="%{getText('echobase.title.confirm.deleteQuery')}"
           autoOpen="false" modal="true" width="780"/>

<script type="text/javascript">

  jQuery(document).ready(function () {

    $('[name="query.topiaId"]').change(function (event) {
      reloadPage(this.value);
    });

    if (<s:property value='%{#noError and queryExists}'/>) {

      // load sql query
      $.ajax(
        {
          url: '<s:url action="exportQueryResult" namespace="/exportQuery"/>',
          data: {queryId: $('[name="query.topiaId"]').val()},
          success: function (data, textStatus, jqXHR) {
            $('#resultGrid').html(data);
          }
        });
    }
  });

</script>


