<%--
  #%L
  EchoBase :: UI
  %%
  Copyright (C) 2011 - 2012 Ifremer, Codelutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  #L%
  --%>
<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>


<script type="text/javascript">

  function cancel() {
    $('#confirmDeleteDialog').dialog('close');
    return false;
  }
</script>

<div class="dialogContainer ui-corner-all">

  <s:label key="echobase.confirm.delete.query" theme="simple"/>

  <fieldset class="ui-corner-all">



    <s:textarea name="query.name" requiredLabel="true" cols="160" rows="1"
                readonly="true"
                label="%{getText('echobase.label.query.name')}"/>

    <s:textarea name="query.description" requiredLabel="true" cols="160" rows="2"
                readonly="true"
                label="%{getText('echobase.label.query.description')}"/>

    <s:textarea name="query.sqlQuery" requiredLabel="true" cols="160" rows="4"
                label="%{getText('echobase.label.query.sql')}"/>

    <br/>

    <s:form namespace="/exportQuery">

      <s:hidden key="query.topiaId" label=''/>

      <ul class="toolbar floatRight">
        <li>
          <s:submit onclick="return cancel();" theme="simple"
                    key="echobase.action.cancel"/>
        </li>
        <li>
          <s:submit action="delete" theme="simple"
                    key="echobase.action.delete"/>
        </li>
      </ul>
    </s:form>
  </fieldset>

</div>
