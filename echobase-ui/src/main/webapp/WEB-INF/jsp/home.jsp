<%--
  #%L
  EchoBase :: UI
  %%
  Copyright (C) 2011 Ifremer, Codelutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  #L%
  --%>
<%@ page language="java" contentType="text/html" pageEncoding="utf-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<title><s:text name="echobase.title.welcome"/></title>

<div class="fontsize11">
  <fieldset>
    <legend>
      <s:text name="echobase.info.usefulLinks"/>
    </legend>
    <ul>
      <li>
        <s:a href="%{getDocumentation('index.html')}" target='#doc'>
          <s:text name="echobase.info.documentation"/>
        </s:a>
      </li>
      <li>
        <s:a href="https://forge.ifremer.fr/plugins/mediawiki/wiki/echor/index.php/Accueil" target='#doc'>
          <s:text name="echobase.info.echoR"/>
        </s:a>
      </li>
      <li>
        <s:a href="http://svn.codelutin.com/echobase/R-EchoBase/" target='#doc'>
          <s:text name="echobase.info.REchoBase"/>
        </s:a>
      </li>
      <li>
        <s:a href="https://forge.codelutin.com/projects/echobase/files" target='#doc'>
          <s:text name="echobase.info.downloadFiles"/>
        </s:a>
      </li>
    </ul>
  </fieldset>
</div>
