<%--
  #%L
  EchoBase :: UI
  %%
  Copyright (C) 2011 Ifremer, Codelutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  #L%
  --%>
<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>


<title><s:text name="echobase.title.createEmbeddedApplication"/></title>

<s:form namespace="/embeddedApplication">

  <br/>

  <fieldset>
    <legend><s:text name="echobase.legend.embeddedApplication.configuration"/></legend>

    <s:checkboxlist list="voyages" key="model.voyageIds"
                    template="mycheckboxlist"
                    label="%{getText('echobase.label.voyageToSelect')}"/>

    <s:textfield key="model.fileName" requiredLabel="true" size="100"
                 label="%{getText('echobase.label.embeddedApplicationFileName')} (*)"/>

    <s:label value="%{model.warLocation.name}" readonly="true" disabled="true"
             label="%{getText('echobase.label.embeddedWarFileName')}"/>

    <br/>
    <div class="cleanBoth">
        (*) <s:text name="echobase.info.exportDb.archive"/>
    </div>
  </fieldset>

  <br/>

  <s:if test="!hasActionErrors()">

    <s:submit key="echobase.action.createEmbeddedApplication"
              action="configure"
              align="right"/>

  </s:if>
</s:form>
