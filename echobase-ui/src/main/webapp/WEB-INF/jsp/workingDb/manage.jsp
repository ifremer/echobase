<%--
  #%L
  EchoBase :: UI
  %%
  Copyright (C) 2011 - 2012 Ifremer, Codelutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  #L%
  --%>
<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>

<s:set var="noError" value="!hasErrors()"/>

<script type="text/javascript">

  function confirmDelete() {
    var url = "<s:url action='confirmDelete' namespace='/workingDb'/>?" +
              $.param({ 'conf.topiaId':'<s:property value="conf.topiaId"/>'});
    var dialog = $("#confirmDeleteDialog");
    dialog.load(url);
    dialog.dialog('open');
    return false;
  }

  function reloadPage(params) {
    var url = "<s:url action='showList' namespace='/workingDb'/>?";
    window.location = url + $.param(params);
    return false;
  }

    function driverTypeChanged(driverTypeCombo) {
<s:if test="newConf and !clone">

        if (driverTypeCombo) {

            var url;

            <%-- add prefix in url --%>
            if (driverTypeCombo.selectedIndex ==0) {

                <%-- Base H2 --%>
                url ="jdbc:h2:file:dbPath/dbName";
            } else {

                <%-- Base PG --%>
                url ="jdbc:postgresql://serverName/dbName";
            }

            $("#confUrl").val(url);

        }

        </s:if>
    }

  jQuery(document).ready(function () {

      driverTypeChanged($("#confDriverType")[0]);

  });


</script>

<title><s:text name="echobase.title.workingDbConfiguration"/></title>

<s:if test="echoBaseSession.user.admin">
  <s:form id="createForm" namespace="/workingDb">
    <ul class="toolbar floatLeft">
      <li>
        <s:submit theme="simple" action="create"
                  key="echobase.action.newWorkingDbContiguration"/>
      </li>
    </ul>
  </s:form>
</s:if>

<s:form id="workginDbConfigurationForm" namespace="/workingDb">
  <fieldset>
    <legend><s:text name="echobase.label.workingDbConfigurations"/></legend>
    <s:select key="conf.topiaId" cssStyle="font-size: 140%"
              label="%{getText('echobase.label.workingDbConfigurations')}"
              list="confs" headerKey="" headerValue="" theme="simple"/>
  </fieldset>

  <s:if test="confExists or newConf">
    <fieldset>
      <legend>
        <s:if test="confExists">
          <s:text name="echobase.legend.workingDbConfiguration.use"/>
        </s:if>
        <s:else>
          <s:text name="echobase.legend.workingDbConfiguration.create"/>

        </s:else>
      </legend>

      <div class="cleanBoth help">
        <s:a href="%{getDocumentation('db.html', null)}" target="doc">
          <s:text name="echobase.action.show.workingDb.documentation"/>
        </s:a>
      </div>
      <br/>

        <s:if test="confExists or clone">
            <s:select cssStyle="font-size: 140%"
                      label="%{getText('echobase.label.driverType')}" requiredLabel="true"
                      list="driverTypes" disabled="true" value="conf.driverType"/>
            <s:hidden name="conf.driverType"/>
        </s:if>
        <s:else>
            <s:select key="conf.driverType" cssStyle="font-size: 140%" id="confDriverType"
                      label="%{getText('echobase.label.driverType')}" requiredLabel="true"
                      list="driverTypes" onchange="driverTypeChanged(this)" />
        </s:else>

      <s:textfield key="conf.url" requiredLabel="true" size="80" id="confUrl"
                   readonly="%{confExists}"
                   label="%{getText('echobase.label.workingDbConfiguration.url')}"/>

      <s:textarea key="conf.description" requiredLabel="true" cols="160" rows="4"
                  readonly="%{confExists}"
                  label="%{getText('echobase.label.workingDbConfiguration.description')}"/>

      <s:if test="confExists">

        <s:textfield key="login" requiredLabel="true" size="80"
                     label="%{getText('echobase.label.workingDbConfiguration.login')}"/>

        <s:password key="password" requiredLabel="true" size="80"
                    label="%{getText('echobase.label.workingDbConfiguration.password')}"/>
      </s:if>

      <ul id="dbeditorToolbar" class="toolbar floatRight">
        <s:if test="confExists">
          <li>
            <s:submit action="connect" theme="simple"
                      key="echobase.action.connectToWorkingDb"/>
          </li>
          <s:if test="echoBaseSession.user.admin">
            <li>
              <s:submit action="createPostgresDb" theme="simple"
                        key="echobase.action.createPostgresDb"/>
            </li>
            <li>
              <s:submit action="clone" theme="simple"
                        key="echobase.action.clone"/>
            </li>
            <li>
              <s:submit onclick="return confirmDelete();" theme="simple"
                        key="echobase.action.deleteConfiguration"/>
            </li>
          </s:if>
        </s:if>
        <s:else>
          <li>
            <s:submit action="showList" theme="simple"
                      key="echobase.action.cancel"/>
          </li>
          <li>
            <s:submit action="save" theme="simple"
                      key="echobase.action.createWorkingDbConfiguration"/>
          </li>
        </s:else>
      </ul>
    </fieldset>
  </s:if>
</s:form>

<br/>

<sj:dialog id="confirmDeleteDialog" resizable="false" autoOpen="false"
           title="%{getText('echobase.title.confirm.deleteWorkingDbConfiguration')}"
           modal="true" width="780"/>

<script type="text/javascript">

  jQuery(document).ready(function () {

    $('[name="conf.topiaId"]').change(function (event) {
      reloadPage({'conf.topiaId':this.value});
    });
  });

</script>


