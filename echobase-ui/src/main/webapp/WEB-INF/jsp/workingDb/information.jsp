<%--
#%L
  EchoBase :: UI
  %%
  Copyright (C) 2011 Ifremer, Codelutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  #L%
--%>
<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>

<script type="text/javascript">

  $(document).ready(function () {

    $('input[id^="dbConfiguration"]').css({'background-color': 'white', 'color': 'black', 'padding-right': '2px', 'padding-left': '2px' }).click(function () {
      $(this)[0].focus();
      $(this)[0].select();
    });

  });

</script>
<title><s:text name="echobase.title.connectToDbInformations"/></title>

<br/>
<fieldset>
  <legend><s:text name="echobase.legend.connectionToDb.detail"/></legend>
  <s:textfield key="dbConfiguration.url" cssClass="autoSelect" readonly="true"
               label='%{getText("echobase.common.jdbcUrl")} (*)'/>
  <s:textfield key="dbConfiguration.login" cssClass="autoSelect" readonly="true"
               label='%{getText("echobase.common.jdbcLogin")} (*)'/>
  <s:textfield key="dbConfiguration.password" cssClass="autoSelect" readonly="true"
               label='%{getText("echobase.common.jdbcPassword")} (*)'/>
  <s:textfield key="dbConfiguration.driverType.driverClass.name" cssClass="autoSelect" readonly="true"
               label='%{getText("echobase.common.jdbcDriver")} (*)'/>
  <s:label key="pilotVersion" label='%{getText("echobase.common.pilotVersion")}'/>

  <s:if test="spatialSupport">
    <s:checkbox key="spatialStructureFound" disabled="true"
                label='%{getText("echobase.common.spatialStructureFound")}'/>
  </s:if>
  <s:else>
    <s:label label='%{getText("echobase.common.spatialStructureFound")}' value='%{getText("echobase.message.db.no.spatial.support")}'/>
  </s:else>

  <br/>

  <div class="cleanBoth help">
    <label>
      (*) <s:text name="echobase.info.click.to.select"/>
    </label>
  </div>
</fieldset>

<br/>

<s:form id="createForm" namespace="/workingDb">
  <ul class="toolbar floatLeft">
    <li>
      <s:submit theme="simple" action="downloadDriver"
                key="echobase.action.workingDbconfiguration.downloadDriver"/>
    </li>
    <li>
      <s:submit theme="simple" action="disconnect"
                key="echobase.action.workingDbconfiguration.disconnect"/>
    </li>
    <s:if test="canAddSpatial">
      <li>
        <s:submit theme="simple" action="addSpatial"
                  key="echobase.action.workingDbconfiguration.addSpatial"/>
      </li>
    </s:if>
  </ul>
</s:form>




