<%--
#%L
  EchoBase :: UI
  %%
  Copyright (C) 2011 Ifremer, Codelutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  #L%
--%>
<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags" %>
<script type="text/javascript"
        src="<s:url value='/js/gridHelper.js' />"></script>
<s:set var="emptySelection">
  <s:text name="echobase.message.noEntrySelection"/>
</s:set>
<script type="text/javascript">

  jQuery(document).ready(function () {
    $.addRowSelectTopic('datas', function (event) {
      $('#extraInfos').show();
      var id = $("tr[aria-selected=true] td[aria-describedby='datas_entityId']").text();
      $('#modificationEntityId').html(id);

      var text = $("tr[aria-selected=true] td[aria-describedby='datas_modificationText']").text();
      $('#modificationTextContainter').html(text);
    });
    $.addClearSelectTopic('datas', function (event) {
      $('#extraInfos').hide();
      $('#modificationTextContainter').html("${emptySelection}");
    });
    $.addEvenAndOddClasses('datas');
  });
</script>

<title><s:text name="echobase.title.entityModificationLogs"/></title>
<s:url id="loadUrl" action="getEntityModificationLogs" namespace="/workingDb"
       escapeAmp="false"/>

<sjg:grid id="datas" dataType="json" href="%{loadUrl}" gridModel="datas"
          pager="true" pagerButtons="true" pagerInput="true" navigator="true"
          autowidth="true" rownumbers="false" viewrecords="true"
          navigatorEdit="false" navigatorSearch="true"
          navigatorDelete="false" navigatorAdd="false"
          rowList="10,15,20,50,100,250,500" rowNum="10"
          onSelectRowTopics='datas-rowSelect'
          onCompleteTopics="datas-clearSelect,datas-CompleteTopics"
          navigatorSearchOptions="{multipleGroup:false,multipleSearch:false,sopt:['eq','ne','cn','nc','bw','bn','ew','en','lt','le','gt','ge','nu','nn']}">

  <sjg:gridColumn name="entityType" sortable="true"
                  title="%{getText('echobase.common.entityType')}"/>
  <sjg:gridColumn name="entityId" sortable="true"
                  title="%{getText('echobase.common.entityId')}"/>
  <sjg:gridColumn name="modificationDate" sortable="true"
                  title="%{getText('echobase.common.modificationDate')}"/>
  <sjg:gridColumn name="modificationText" sortable="true"
                  title="%{getText('echobase.common.modificationText')}"/>
  <sjg:gridColumn name="modificationUser" sortable="true"
                  title="%{getText('echobase.common.modificationUser')}"/>

</sjg:grid>
<br/>
<fieldset>
  <legend><s:text name="echobase.title.modification.detail"/></legend>
  <div id="extraInfos">
    <s:label key='echobase.common.entityId' value=''/>
    <div class="clearBoth"/>
    <pre id='modificationEntityId' style="font-weight: bold;"></pre>
    <br/>
  </div>
  <s:label key='echobase.common.modificationText' value=''/>
  <div class="clearBoth"/>
  <br/>
  <pre id='modificationTextContainter' style="font-weight: bold;"></pre>
</fieldset>



