<%--
#%L
  EchoBase :: UI
  %%
  Copyright (C) 2011 Ifremer, Codelutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  #L%
--%>
<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags" %>
<script type="text/javascript"
        src="<s:url value='/js/gridHelper.js' />"></script>

<s:set var="importTooltip">
  <s:text name="echobase.message.clickToShowImportDefail"/>
</s:set>

<script type="text/javascript">

  function loadImportLogDetail(importLogId, entityId) {
    $.ajax(
      {
        url:'<s:url action="getDashboardImportLog" namespace="/workingDb"/>',
        data:{
            importLogId: importLogId,
            entityId: entityId
        },
        async:false,
        dataType:"json",
        success:function (data, textStatus) {
          
          $('#voyageDetail').hide();
          $('#mooringDetail').hide();
          $('#importLogDetail').show();
          
          var json = data.data;

          $('#importLogEntity').html(json['entityName']);
          $('#importLogText').html(json['importText']);
          $('#importLogUser').html(json['importUser']);
          $('#importLogDate').html(json['importDate']);
          $('#importLogImportType').html(json['importType']);

          var importFiles = json['importFile'];
          var importUrlPrefix = '<s:url action="downloadImportFile" namespace="/workingDb"/>';

          var importResult="";
          $(importFiles).each(function () {
            var importFile = this;
            var importFileId = importFile['topiaId'];
            var filename = importFile['name'];
            var importUrl = encodeURI($.prepareUrl(importUrlPrefix , {importFileId:importFileId}));

            importResult += "<li><a href='"+importUrl+"' class='fontsize11'>" + filename + "</a></li>";
          });

          if (importResult == "") {
            importResult = "<s:text name='echobase.message.noImportFilesFound'/>";
          }
          $('#importLogFiles').html(importResult);

          var exportResult = "";
          var exportUrlPrefix = '<s:url action="downloadExportFile" namespace="/workingDb"/>';
          $(importFiles).each(function () {
            var exportFile = this;
            var importFileId = exportFile['topiaId'];
            var filename = 'check-' + exportFile['name'];
            var importUrl = encodeURI($.prepareUrl(exportUrlPrefix, {importFileId: importFileId}));

            exportResult += "<li><a href='" + importUrl + "' class='fontsize11'>" + filename + "</a></li>";
          });

          if (exportResult == "") {
            exportResult = "<s:text name='echobase.message.noImportFilesFound'/>";
          }
          $('#exportLogFiles').html(exportResult);
        }
      });
  }

  function loadVoyageDetail(voyageId) {
    $.ajax(
      {
        url:'<s:url action="getDashboardVoyage" namespace="/workingDb"/>',
        data:{voyageId:voyageId},
        async:false,
        dataType:"json",
        success:function (data, textStatus) {
          var json = data.data;

          $('#voyageMission').html(json['mission_lbl']);
          $('#voyageAreaOfOperation').html(json['areaOfOperation_lbl']);
          $('#voyageStartEndPort').html(json['startPort_lbl'] + " - " + json['endPort_lbl']);
          $('#voyageStartEndDate').html(json['startDate'] + " - " + json['endDate']);
          $('#voyageDatum').html(json['datum']);
          $('#voyageName').html(json['name']);
          $('#voyageDescription').html(json['description']);
          
          $('#voyageDetail').show();
          $('#mooringDetail').hide();
        }
      });
    return false;
  }

  function loadMooringDetail(mooringId) {
    $.ajax(
      {
        url:'<s:url action="getDashboardMooring" namespace="/workingDb"/>',
        data:{mooringId: mooringId},
        async:false,
        dataType:"json",
        success:function (data, textStatus) {
          var json = data.data;

          $('#mooringMission').html(json['mission_lbl']);
          $('#mooringCode').html(json['code']);
          $('#mooringDescription').html(json['description']);
          
          $('#voyageDetail').hide();
          $('#mooringDetail').show();
        }
      });
    return false;
  }
  
  function formatImportLogs(cellvalue, options, rowObject) {
    var result = "<ul>";
    var importMapping = rowObject.importLogs;
    var entityId = rowObject.id;
    $(cellvalue).each(function () {
      var importId = this;
      var val = importMapping[importId];
      result += "<li onclick='loadImportLogDetail(\"" + importId + "\", \"" + entityId + "\")' " + 
                     "class='fontsize11'>" + val + "</li>";
    });
    result += "</ul>";
    return result;
  }

  function formatName(cellvalue, options, rowObject) {
    return rowObject.id_lbl;
  }

  var togglePosition = false;
  
  function toggleTab() {
    $('#voyageDetail').hide();
    $('#mooringDetail').hide();
    $('#importLogDetail').hide();
      
    if (togglePosition) {
      $('#voyagesTab').hide();
      $('#mooringsTab').show();
      $("#moorings").jqGrid && $("#moorings").jqGrid('setGridWidth', $("#mooringsTab")[0].offsetWidth);
      $("#toggleTabButton").text('<s:text name="echobase.view.voyages"/>');

    } else {
      $('#voyagesTab').show();
      $("#voyages").jqGrid && $("#voyages").jqGrid('setGridWidth', $("#voyagesTab")[0].offsetWidth);
      $('#mooringsTab').hide();
      $("#toggleTabButton").text('<s:text name="echobase.view.moorings"/>');
    }
    
    togglePosition = !togglePosition;
  }
  
  jQuery(document).ready(function () {
    toggleTab();
    
    // display voyage infos
    $.addRowSelectTopic('voyages', function (event) {
      var voyageId = $("tr[aria-selected=true] td[aria-describedby='voyages_id']").text();
      loadVoyageDetail(voyageId);
      
      if (event.originalEvent.iCol === 1 || event.originalEvent.cellcontent === "<ul></ul>") {
        $('#importLogDetail').hide();
      }
    });

    // display mooring infos
    $.addRowSelectTopic('moorings', function (event) {
      var mooringId = $("tr[aria-selected=true] td[aria-describedby='moorings_id']").text();
      loadMooringDetail(mooringId);
      
      if (event.originalEvent.iCol === 1 || event.originalEvent.cellcontent === "<ul></ul>") {
        $('#importLogDetail').hide();
      }
    });
  });
</script>

<title><s:text name="echobase.title.dashboard"/></title>

<div align="right">
    <button id="toggleTabButton" class="action" onclick="toggleTab()"><s:text name="echobase.view.voyages"/></button>
</div>
<br/>
    
<div id="voyagesTab">
    <s:url id="loadUrl" action="getDashboardVoyageImportLogs" namespace="/workingDb" escapeAmp="false"/>
    <sjg:grid id="voyages" dataType="json" href="%{loadUrl}" gridModel="datas"
              pager="true" pagerButtons="true" pagerInput="true" navigator="true"
              autowidth="true" rownumbers="false" viewrecords="true"
              navigatorEdit="false" navigatorSearch="false"
              navigatorDelete="false"
              navigatorAdd="false" rowList="10,15,20,50,100,250,500" rowNum="10"
              onSelectRowTopics="voyages-rowSelect"
              onCellSelectTopics="voyages-rowSelect"
              onCompleteTopics="voyages-clearSelect,voyages-CompleteTopics">

      <sjg:gridColumn name="id" title="id" hidden="true"/>
      <sjg:gridColumn name="name" sortable="true" formatter="formatName"
                      title="%{getText('echobase.common.voyage')}"/>

      <s:iterator value="%{importVoyageTypes}" var="entry">
        <sjg:gridColumn name="importType.%{#entry.name}" sortable="false"
                        title="%{#entry.label}" tooltip="%{#entry.title}"
                        formatter="formatImportLogs"/>
      </s:iterator>
    </sjg:grid>
</div>

<div id ="mooringsTab">
    <s:url id="loadUrl" action="getDashboardMooringImportLogs" namespace="/workingDb" escapeAmp="false"/>
    <sjg:grid id="moorings" dataType="json" href="%{loadUrl}" gridModel="datas"
              pager="true" pagerButtons="true" pagerInput="true" navigator="true"
              autowidth="false" rownumbers="false" viewrecords="true"
              navigatorEdit="false" navigatorSearch="false"
              navigatorDelete="false"
              navigatorAdd="false" rowList="10,15,20,50,100,250,500" rowNum="10"
              onSelectRowTopics="moorings-rowSelect"
              onCellSelectTopics="moorings-rowSelect"
              onCompleteTopics="moorings-clearSelect,moorings-CompleteTopics">

      <sjg:gridColumn name="id" title="id" hidden="true"/>
      <sjg:gridColumn name="name" sortable="true" formatter="formatName"
                      title="%{getText('echobase.common.mooring')}"/>

      <s:iterator value="%{importMooringTypes}" var="entry">
        <sjg:gridColumn name="importType.%{#entry.name}" sortable="false"
                        title="%{#entry.label}" tooltip="%{#entry.title}"
                        formatter="formatImportLogs"/>
      </s:iterator>
    </sjg:grid>
</div>

<br/>

<fieldset id="voyageDetail">
  <legend><s:text name="echobase.title.voyage.detail"/></legend>

  <div>
    <s:label key='echobase.common.name' value=''/>
    <pre id='voyageName' style="font-weight: bold;"></pre>
    <br/>

    <s:label key='echobase.common.description' value=''/>
    <pre id='voyageDescription' style="font-weight: bold;"></pre>
    <br/>

    <s:label key='echobase.common.mission' value=''/>
    <pre id='voyageMission' style="font-weight: bold;"></pre>
    <br/>

    <s:label key='echobase.common.areaOfOperation' value=''/>
    <pre id='voyageAreaOfOperation' style="font-weight: bold;"></pre>
    <br/>

    <s:label key='echobase.common.startEndPort' value=''/>
    <pre id='voyageStartEndPort' style="font-weight: bold;"></pre>
    <br/>

    <s:label key='echobase.common.startEndDate' value=''/>
    <pre id='voyageStartEndDate' style="font-weight: bold;"></pre>
    <br/>

    <s:label key='echobase.common.datum' value=''/>
    <pre id='voyageDatum' style="font-weight: bold;"></pre>
    <br/>

  </div>
</fieldset>

<fieldset id="mooringDetail">
  <legend><s:text name="echobase.title.mooring.detail"/></legend>
  <div>

    <s:label key='echobase.common.name' value=''/>
    <pre id='mooringCode' style="font-weight: bold;"></pre>
    <br/>

    <s:label key='echobase.common.description' value=''/>
    <pre id='mooringDescription' style="font-weight: bold;"></pre>
    <br/>

    <s:label key='echobase.common.mission' value=''/>
    <pre id='mooringMission' style="font-weight: bold;"></pre>
    <br/>

  </div>
</fieldset>

<div id="importLogDetail">
    <fieldset>
      <legend><s:text name="echobase.title.importLog.detail"/></legend>
      <div>

        <s:label key='echobase.common.importType' value=''/>
        <pre id='importLogImportType' style="font-weight: bold;"></pre>
        <br/>

        <s:label key='echobase.common.importEntity' value=''/>
        <pre id='importLogEntity' style="font-weight: bold;"></pre>
        <br/>

        <s:label key='echobase.common.importDate' value=''/>
        <pre id='importLogDate' style="font-weight: bold;"></pre>
        <br/>

        <s:label key='echobase.common.importText' value=''/>
        <div class="floatLeft">
          <pre id='importLogText' style="font-weight: bold;"></pre>
        </div>
        <br/>

        <div class="clearBoth">
            <s:label key='echobase.common.importUser' value=''/>
            <pre id='importLogUser' style="font-weight: bold;"></pre>
        </div>
      </div>
    </fieldset>

    <fieldset id="importLogFilesFieldset">
        <legend><s:text name="echobase.title.importLog.files"/></legend>
        <div>
            <ul id='importLogFiles' style="font-weight: bold;"></ul>
        </div>
    </fieldset>

    <fieldset id="exportLogFilesFieldset">
      <legend><s:text name="echobase.title.exportLog.files"/></legend>
      <div>
        <ul id='exportLogFiles' style="font-weight: bold;"></ul>
      </div>
    </fieldset>
</div>
