<%--
  #%L
  EchoBase :: UI
  %%
  Copyright (C) 2011 Ifremer, Codelutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  #L%
  --%>
<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>

<s:url id="downloadUrl" namespace="/exportDb" action="download"/>

<s:if test="error == null">
  <script type="text/javascript">

    jQuery(document).ready(function () {

      // start to download by it-self the result of import
      window.location="${downloadUrl}";
    });
  </script>
</s:if>

<title><s:text name="echobase.title.exportDbResult"/></title>

<%@ include file="/WEB-INF/includes/actionResult.jsp" %>

<br/>
<s:if test="error == null">
<div>
  Si le téléchargement n'a pas démarré automatiquement, suivez ce lien :
  <a href="${downloadUrl}" target="download">
    <s:text name="echobase.action.downloadExportDbFile"/>
  </a>
</div>
</s:if>

