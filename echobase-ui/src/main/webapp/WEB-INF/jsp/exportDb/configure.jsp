<%--
  #%L
  EchoBase :: UI
  %%
  Copyright (C) 2011 Ifremer, Codelutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  #L%
  --%>
<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>

<title><s:text name="echobase.title.exportDb"/></title>

<script type="text/javascript">

  jQuery(document).ready(function () {

    function reload(value) {
      if (value == "REFERENTIAL_AND_DATA") {
        $('#extraInfos').show();
      } else {
        $('#extraInfos').hide();
      }
    }

    // to change form when mode is changed
    $('[name="model.exportDbMode"]').change(function (event) {
      reload(this.value);
    });

    var incomingVal = $('[name="model.exportDbMode"][checked="checked"]').val();
    reload(incomingVal);
  });
</script>

<s:form namespace="/exportDb" method="POST">
  <fieldset>
    <legend>
      <s:text name="echobase.legend.exportDb.configuration.files"/>
    </legend>

    <div class="cleanBoth help">
      <s:a href="%{getDocumentation('exportDb.html', null)}" target="doc">
        <s:text name="echobase.action.show.exportDb.documentation"/>
      </s:a>
    </div>
    <br/>

    <s:radio id='mode' key='model.exportDbMode' list="modes"
             cssClass="cleanBoth" requiredLabel="true" template="myradiomap"
             label='%{getText("echobase.common.exportDbMode")}'/>

    <div class="cleanBoth"></div>
    <br/>
    <s:textfield key="model.fileName" requiredLabel="true" size="100"
                 label="%{getText('echobase.label.exportDbFileName')} (*)"/>

    <div class="cleanBoth"></div>
    <br/>

    <div id='extraInfos'>
      <s:checkboxlist list="voyages" key="model.voyageIds" id='voyages'
                      template="mycheckboxlist"
                      label="%{getText('echobase.label.voyageToSelect')}"/>
    </div>
    <div class="cleanBoth">
      <br/>
      (*) <s:text name="echobase.info.exportDb.archive"/>
    </div>
  </fieldset>
  <br/>
  <s:submit action="configure" value='%{getText("echobase.action.export")}'/>
</s:form>
