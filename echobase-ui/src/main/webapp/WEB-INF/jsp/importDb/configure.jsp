<%--
  #%L
  EchoBase :: UI
  %%
  Copyright (C) 2011 Ifremer, Codelutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  #L%
  --%>
<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>

<title><s:text name="echobase.title.importDb"/></title>

<script type="text/javascript">

  jQuery(document).ready(function () {
    $.addCheckFileSize(<s:property value="uploadFileMaxLength"/>);
  });
</script>

<s:form namespace="/importDb" method="POST" enctype="multipart/form-data">
  <fieldset>
    <legend>
      <s:text name="echobase.legend.importDb.configuration"/>
    </legend>
    <div class="cleanBoth help">
      <s:a href="%{getDocumentation('importDb.html',null)}"
           target="doc">
        <s:text name="echobase.action.show.importDb.documentation"/>
      </s:a>
    </div>
    <br/>
    <s:radio id='mode' key='model.importDbMode' list="modes"
             cssClass="cleanBoth" requiredLabel="true" template="myradiomap"
             label='%{getText("echobase.common.importDbMode")}'/>
    <div class="cleanBoth"></div>
    <br/>
    <s:file name="input" requiredLabel="true" key="echobase.common.importDbFile"/>

  </fieldset>
  <br/>
  <s:submit action="configure" value='%{getText("echobase.action.import")}'/>
</s:form>
