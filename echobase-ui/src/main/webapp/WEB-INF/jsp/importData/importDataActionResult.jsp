<%--
  #%L
  EchoBase :: UI
  %%
  Copyright (C) 2011 Ifremer, Codelutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  #L%
  --%>
<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>

<s:if test="error != null && error.checkImportError">

  <fieldset>
    <legend>
      <s:text name="echobase.mismatch.import.file.error"/>
    </legend>

    <p>
      <s:text name="echobase.mismatch.import.file">
        <s:param value="%{error.importFilename}"/>
      </s:text>
    </p>

    <br/>

    <div>
      <ul>
        <li>
          <s:a action="downloadCheckedFile" namespace="/importData">
            <s:param name="fileSuffix" value="%{error.expectedFileSuffix}"/>
            <s:param name="filename" value="%{error.expectedFileName}"/>
            <s:text name="echobase.processed.file"/>
          </s:a>
        </li>
        <li>
          <s:a action="downloadCheckedFile" namespace="/importData">
            <s:param name="fileSuffix" value="%{error.actualFileSuffix}"/>
            <s:param name="filename" value="%{error.actualFileName}"/>
            <s:text name="echobase.exported.file"/>
          </s:a>
        </li>
      </ul>
    </div>

  </fieldset>

</s:if>

<%@ include file="/WEB-INF/includes/actionResult.jsp" %>
