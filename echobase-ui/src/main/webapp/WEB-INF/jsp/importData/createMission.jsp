<%--
  #%L
  EchoBase :: UI
  %%
  Copyright (C) 2011 Ifremer, Codelutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  #L%
  --%>
<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>

<title><s:text name="echobase.title.createMission"/></title>

<s:form namespace="/importData" method="POST">

    <fieldset>
      <legend>
        <s:text name="echobase.legend.importData.createMission"/>
      </legend>

        <s:textfield key="mission.name" size="40" requiredLabel="true"
                     label='%{getText("echobase.common.name")}'/>

        <s:textfield key="mission.missionAbstract" size="40" requiredLabel="true"
                     label='%{getText("echobase.common.missionAbstract")}'/>

        <s:textfield key="mission.litteratureReferences" size="40"
                     label='%{getText("echobase.common.litteratureReferences")}'/>

        <s:textfield key="mission.project"
                     label='%{getText("echobase.common.project")}'/>

        <s:textfield key="mission.platform"
                     label='%{getText("echobase.common.platform")}'/>

        <s:textfield key="mission.institution"
                     label='%{getText("echobase.common.institution")}'/>

        <s:textfield key="mission.country"
                     label='%{getText("echobase.common.country")}'/>

        <s:textfield key="mission.keywords"
                     label='%{getText("echobase.common.keywords")}'/>

        <s:textfield key="mission.dataCentre"
                     label='%{getText("echobase.common.dataCentre")}'/>

        <s:textfield key="mission.dataCentreEmail"
                     label='%{getText("echobase.common.dataCentreEmail")}'/>

        <s:textfield key="mission.author"
                     label='%{getText("echobase.common.author")}'/>

        <s:textfield key="mission.authorEmail"
                     label='%{getText("echobase.common.authorEmail")}'/>

        <s:textfield key="mission.principalInvestigator"
                     label='%{getText("echobase.common.principalInvestigator")}'/>

        <s:textfield key="mission.principalInvestigatorEmail"
                     label='%{getText("echobase.common.principalInvestigatorEmail")}'/>

        <s:textfield key="mission.organisationReferences"
                     label='%{getText("echobase.common.organisationReferences")}'/>

        <s:textfield key="mission.distributionStatement"
                     label='%{getText("echobase.common.distributionStatement")}'/>

        <s:textfield key="mission.organisationLevelAcknowledgements"
                     label='%{getText("echobase.common.organisationLevelAcknowledgements")}'/>

        <s:textfield key="mission.source"
                     label='%{getText("echobase.common.source")}'/>

    </fieldset>

  <br/>

  <s:submit action="createMission" key='echobase.action.createMission'/>

</s:form>
