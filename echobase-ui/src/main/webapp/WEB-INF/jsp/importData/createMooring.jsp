<%--
  #%L
  EchoBase :: UI
  %%
  Copyright (C) 2011 Ifremer, Codelutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  #L%
  --%>
<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>

<title><s:text name="echobase.title.createMooring"/></title>

<s:form namespace="/importData" method="POST">

    <fieldset>
      <legend>
        <s:text name="echobase.legend.importData.createMooring"/>
      </legend>

        <s:select key="missionId" requiredLabel="true"
                  label='%{getText("echobase.common.mission")}'
                  list="missions" headerKey="" headerValue=""/>

        <s:textfield key="mooring.code" size="40" requiredLabel="true"
                     label='%{getText("echobase.common.code")}'/>

        <s:textfield key="mooring.description" size="40"
                     label='%{getText("echobase.common.description")}'/>

        <s:textfield key="mooring.depth"
                     label='%{getText("echobase.common.depth")}'/>

        <s:textfield key="mooring.northLimit"
                     label='%{getText("echobase.common.northLimit")}'/>

        <s:textfield key="mooring.eastLimit"
                     label='%{getText("echobase.common.eastLimit")}'/>

        <s:textfield key="mooring.southLimit"
                     label='%{getText("echobase.common.southLimit")}'/>

        <s:textfield key="mooring.westLimit"
                     label='%{getText("echobase.common.westLimit")}'/>

        <s:textfield key="mooring.upLimit"
                     label='%{getText("echobase.common.upLimit")}'/>

        <s:textfield key="mooring.downLimit"
                     label='%{getText("echobase.common.downLimit")}'/>

        <s:textfield key="mooring.units"
                     label='%{getText("echobase.common.units")}'/>

        <s:textfield key="mooring.zunits"
                     label='%{getText("echobase.common.zunits")}'/>

        <s:textfield key="mooring.projection"
                     label='%{getText("echobase.common.projection")}'/>

        <sj:datepicker label='%{getText("echobase.common.deploymentDate")}'
                       key="mooring.deploymentDate"
                       value="%{mooring.deploymentDate}"
                       displayFormat="dd/mm/yy" timepicker="true"
                       timepickerShowSecond="true"
                       timepickerFormat="hh:mm:ss"/>

        <sj:datepicker label='%{getText("echobase.common.retrievalDate")}'
                       key="mooring.retrievalDate"
                       value="%{mooring.retrievalDate}"
                       displayFormat="dd/mm/yy" timepicker="true"
                       timepickerShowSecond="true"
                       timepickerFormat="hh:mm:ss"/>

        <s:textfield key="mooring.siteName"
                     label='%{getText("echobase.common.siteName")}'/>

        <s:textfield key="mooring.operator"
                     label='%{getText("echobase.common.operator")}'/>

        <s:textfield key="mooring.comments" size="40"
                     label='%{getText("echobase.common.comments")}'/>

    </fieldset>

  <br/>

  <s:submit action="createMooring" key='echobase.action.createMooring'/>

</s:form>
