<%--
  #%L
  EchoBase :: UI
  %%
  Copyright (C) 2011 - 2012 Ifremer, Codelutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  #L%
  --%>
<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>

<meta http-equiv="refresh"
      content='1;url=<s:url action="importAncillaryInstrumentation" namespace="/importData"/>'/>

<title>
  <s:text name="echobase.common.importType.ancillaryInstrumentation"/>
  <s:text name="echobase.common.inProgress"/>
</title>

<%-- TODO letellier 20111104 : Add warn icon --%>
<p><s:text name="echobase.message.warnImportInProgress"/></p>

<br/>

<div>
  <sj:progressbar value="%{model.progress}"/>
</div>
