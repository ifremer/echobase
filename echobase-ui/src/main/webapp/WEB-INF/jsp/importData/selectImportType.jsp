<%--
  #%L
  EchoBase :: UI
  %%
  Copyright (C) 2011 Ifremer, Codelutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  #L%
  --%>
<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>

<title><s:text name="echobase.title.importData.selectImportType"/></title>

<s:form namespace="/importData" method="GET">

  <s:submit action="createMission" key='echobase.action.createNewMission'
            method="input"/>

  <fieldset>
    <legend>
      <s:text name="echobase.legend.importData.configuration.selectImportTypeVoyage"/>
    </legend>
    <s:radio key='voyageMode' list="voyageModes" cssClass="cleanBoth"
             label='%{getText("echobase.common.importDataMode")}'
             requiredLabel="true" template="myradiomap"/>

    <s:submit action="selectImportType" key='echobase.action.configureImport'
              align="right"/>
  </fieldset>

</s:form>

<s:form namespace="/importData" method="GET">

    <fieldset>
    <legend>
      <s:text name="echobase.legend.importData.configuration.selectImportTypeMooring"/>
    </legend>
    <s:radio key='mooringMode' list="mooringModes" cssClass="cleanBoth"
             label='%{getText("echobase.common.importDataMode")}'
             requiredLabel="true" template="myradiomap"/>

    <s:submit action="createMooring" key='echobase.action.createNewMooring'
              method="input" align="right"/>

    <s:submit action="selectImportType" key='echobase.action.configureImport'
              align="right"/>
  </fieldset>

</s:form>

