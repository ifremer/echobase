<%--
  #%L
  EchoBase :: UI
  %%
  Copyright (C) 2011 Ifremer, Codelutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  #L%
  --%>
<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<title>
  Configuration <s:text name="echobase.common.importType.acoustic"/>
</title>

<script type="text/javascript"
        src="<s:url value='/js/gridHelper.js' />"></script>

<script type="text/javascript">

  jQuery(document).ready(function () {

    $.addCheckFileSize(<s:property value="uploadFileMaxLength"/>);
    $.autoSelectVoyageAndVessel(
      $('[name="model.voyageId"]'),
      $('[name="model.vesselId"]'),
      '<s:url action="getVesselsForVoyage" namespace="/importData"/>',
      '<s:property value="model.voyageId"/>',
      '<s:property value="model.vesselId"/>'
    );
  });
</script>

<s:form namespace="/importData" method="POST" enctype="multipart/form-data">

  <fieldset>
    <legend>
      <s:text name="echobase.legend.importData.configure"/>
    </legend>

    <div class="cleanBoth help">
      <s:a href="%{getDocumentation('importData.html', null)}" target="doc">
        <s:text name="echobase.action.show.import.documentation"/>
      </s:a>
    </div>
    <br/>

    <s:select key="model.voyageId" requiredLabel="true"
              label='%{getText("echobase.common.voyage")}'
              list="voyages" headerKey="" headerValue=""/>

    <sj:select key="model.vesselId" requiredLabel="true"
               label='%{getText("echobase.common.vessel")}'/>

    <s:select key="model.cellPositionReference" requiredLabel="true"
              label='%{getText("echobase.common.cellPositionReference")}'
              list="cellPositionReferences" headerKey="" headerValue=""/>

    <s:checkbox key="model.addDataAcquisition"
                label='%{getText("echobase.common.addDataAcquisition")}'/>

    <s:textfield key="model.transceiverAcquisitionAbsorptionDescription"
                 size="80" requiredLabel="true"
                 label='%{getText("echobase.common.transceiverAcquisitionAbsorptionDescription")}'/>

    <s:textfield key="model.acquisitionSoftwareName" size="80"
                 requiredLabel="true"
                 label='%{getText("echobase.common.acquisitionSoftwareName")}'/>

    <s:textfield key="model.acquisitionSoftwareVersionER60" size="80"
                 requiredLabel="true"
                 label='%{getText("echobase.common.acquisitionSoftwareVersionER60")}'/>

    <s:textfield key="model.acquisitionSoftwareVersionME70" size="80"
                 requiredLabel="true"
                 label='%{getText("echobase.common.acquisitionSoftwareVersionME70")}'/>

    <s:textfield key="model.loggedDataFormat" size="80" requiredLabel="true"
                 label='%{getText("echobase.common.loggedDataFormat")}'/>

    <s:textfield key="model.loggedDataDatatype" size="80" requiredLabel="true"
                 label='%{getText("echobase.common.loggedDataDatatype")}'/>

    <s:textfield key="model.pingDutyCycle" size="80" requiredLabel="true"
                 label='%{getText("echobase.common.pingDutyCycle")}'/>

    <s:textfield key="model.soundSpeedCalculationsER60" size="80"
                 requiredLabel="true"
                 label='%{getText("echobase.common.soundSpeedCalculationsER60")}'/>

    <s:textfield key="model.soundSpeedCalculationsME70" size="80"
                 requiredLabel="true"
                 label='%{getText("echobase.common.soundSpeedCalculationsME70")}'/>

    <s:textfield key="model.sounderConstant" size="80" requiredLabel="true"
                 label='%{getText("echobase.common.sounderConstant")}'/>

    <s:textfield key="model.processingSoftwareName" size="80"
                 requiredLabel="true"
                 label='%{getText("echobase.common.processingSoftwareName")}'/>

    <s:textfield key="model.processingSoftwareVersion" size="80"
                 requiredLabel="true"
                 label='%{getText("echobase.common.processingSoftwareVersion")}'/>

    <s:textfield key="model.processingTemplate" size="80" requiredLabel="true"
                 label='%{getText("echobase.common.processingTemplate")}'/>

    <s:textfield key="model.processingDescription" size="80" requiredLabel="true"
                 label='%{getText("echobase.common.processingDescription")}'/>

    <s:textfield key="model.digitThreshold" size="80" requiredLabel="true"
                 label='%{getText("echobase.common.digitThreshold")}'/>

    <s:textfield key="model.acousticDensityUnit" size="80" requiredLabel="true"
                 label='%{getText("echobase.common.acousticDensityUnit")}'/>

    <s:textfield key="model.notes" size="80" requiredLabel="true"
                 label='%{getText("echobase.common.dataProcessingNotes")}'/>

    <s:file key="moviesFile" requiredLabel="true"
            label='%{getText("echobase.common.moviesFile")}'/>

    <s:file key="calibrationsFile"
            label='%{getText("echobase.common.calibrationsFile")}'/>

    <s:textarea key="model.importNotes" cols="80" rows="5"
                label='%{getText("echobase.common.importNotes")}'/>
  </fieldset>
  <br/>
  <s:submit action="configureAcoustic-execute" key='echobase.action.import'/>

</s:form>

