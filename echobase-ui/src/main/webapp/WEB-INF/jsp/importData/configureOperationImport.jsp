<%--
  #%L
  EchoBase :: UI
  %%
  Copyright (C) 2011 Ifremer, Codelutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  #L%
  --%>
<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<script type="text/javascript"
        src="<s:url value='/js/gridHelper.js' />"></script>

<title>
  Configuration <s:text name="echobase.common.importType.operation"/>
</title>

<script type="text/javascript">

  jQuery(document).ready(function () {
    $.addCheckFileSize(<s:property value="uploadFileMaxLength"/>);
  });
</script>

<s:form namespace="/importData" method="POST" enctype="multipart/form-data">

  <fieldset>
    <legend>
      <s:text
        name="echobase.legend.importData.configure"/>
    </legend>

    <div class="cleanBoth help">
      <s:a href="%{getDocumentation('importData.html', null)}" target="doc">
        <s:text name="echobase.action.show.import.documentation"/>
      </s:a>
    </div>
    <br/>

    <s:select key="model.voyageId" requiredLabel="true"
              label='%{getText("echobase.common.voyage")}'
              list="voyages" headerKey="" headerValue=""/>

    <s:file key="operationFile" requiredLabel="true"
            label='%{getText("echobase.common.operationFile")}'/>

    <s:file key="operationMetadataFile" requiredLabel="true"
            label='%{getText("echobase.common.operationMetadataFile")}'/>

    <s:file key="gearMetadataFile" requiredLabel="true"
            label='%{getText("echobase.common.gearMetadataFile")}'/>

    <s:textarea key="model.importNotes" cols="80" rows="5"
                label='%{getText("echobase.common.importNotes")}'/>

  </fieldset>

  <br/>
  <s:submit action="configureOperation-execute" key='echobase.action.import'/>

</s:form>
