<%--
  #%L
  EchoBase :: UI
  %%
  Copyright (C) 2011 Ifremer, Codelutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  #L%
  --%>
<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<title>
  Configuration <s:text name="echobase.common.importType.results"/>
</title>

<script type="text/javascript"
        src="<s:url value='/js/gridHelper.js' />"></script>

<script type="text/javascript">

  jQuery(document).ready(function () {

    $.addCheckFileSize(<s:property value="uploadFileMaxLength"/>);

    // to change form when mode is changed
    $('[name="model.importType"]').change(function (event) {
      $('[class~="importType"]').hide();
      $('#' + this.value).show();
      $('#' + this.value + ' .errorMessage').hide();
    });

    // show incoming mode
    var incomingVal = $('[name="model.importType"][checked="checked"]').val();
    $('#' + incomingVal).show();

    $.autoSelectProviderAndDataProcessing(
      $('#voyageSelectBox'),
      $('#dataProcessingSelectBox'),
      '<s:url action="getDataProcessingsForVoyage" namespace="/importData"/>',
      '<s:property value="model.voyageId"/>',
      '<s:property value="model.dataProcessingId"/>'
    );
  });
</script>

<s:radio id='mode' key='model.importType' list="importTypes"
         cssClass="cleanBoth" requiredLabel="true" template="myradiomap"
         label='%{getText("echobase.common.importType")}' />

<s:form id="RESULT_VOYAGE" namespace="/importData" method="POST"
        enctype="multipart/form-data" cssClass="hidden importType">

  <fieldset>
    <legend>
      <s:text name="echobase.legend.importData.configure"/>
    </legend>

    <div class="cleanBoth help">
      <s:a href="%{getDocumentation('importData.html', null)}" target="doc">
        <s:text name="echobase.action.show.import.documentation"/>
      </s:a>
    </div>
    <br/>

    <s:hidden key="model.importType" value="RESULT_VOYAGE" label=''/>

    <s:select key="model.voyageId" requiredLabel="true"
              label='%{getText("echobase.common.voyage")}'
              list="voyages" headerKey="" headerValue=""/>

    <s:label key="echobase.information.one.file.required" requiredLabel="true"
             value=''/>

    <s:file key="lengthAgeKeyFile"
            label='%{getText("echobase.common.lengthAgeKeyFile")}'/>

    <s:file key="lengthWeightKeyFile"
            label='%{getText("echobase.common.lengthWeightKeyFile")}'/>

    <s:file key="echotypeFile"
            label='%{getText("echobase.common.echotypeFile")}'/>

    <s:textarea key="model.importNotes" cols="80" rows="5"
                label='%{getText("echobase.common.importNotes")}'/>
  </fieldset>
  <br/>
  <s:submit action="configureResults-modeVoyage" key='echobase.action.import'/>
</s:form>

<s:form id="RESULT_ESDU" namespace="/importData" method="POST"
        enctype="multipart/form-data" cssClass="hidden importType">

  <fieldset>
    <legend>
      <s:text name="echobase.legend.importData.configure"/>
    </legend>

    <div class="cleanBoth help">
      <s:a href="%{getDocumentation('importData.html', null)}" target="doc">
        <s:text name="echobase.action.show.import.documentation"/>
      </s:a>
    </div>
    <br/>

    <s:hidden key="model.importType" value="RESULT_ESDU" label=''/>

    <s:select id='voyageSelectBox' key="model.voyageId" requiredLabel="true"
              label='%{getText("echobase.common.voyage")}'
              list="voyages" headerKey="" headerValue=""/>

    <sj:select id='dataProcessingSelectBox' key="model.dataProcessingId"
               requiredLabel="true"
               label='%{getText("echobase.common.dataProcessing")}'/>

    <s:textfield key="model.resultLabel" size="40" requiredLabel="true"
                 label='%{getText("echobase.common.resultLabel")}'/>

    <s:label key="echobase.information.one.file.required" requiredLabel="true"
             value=''/>

    <s:file key="esduByEchotypeFile"
            label='%{getText("echobase.common.esduByEchotypeFile")}'/>

    <s:file key="esduByEchotypeAndSpeciesCategoryFile"
            label='%{getText("echobase.common.esduByEchotypeAndSpeciesCategoryFile")}'/>

    <s:file key="esduByEchotypeAndSpeciesCategoryAndLengthFile"
            label='%{getText("echobase.common.esduByEchotypeAndSpeciesCategoryAndLengthFile")}'/>

    <s:file key="esduBySpeciesAndAgeCategoryFile"
            label='%{getText("echobase.common.esduBySpeciesAndAgeCategoryFile")}'/>

    <s:textarea key="model.importNotes" cols="80" rows="5"
                label='%{getText("echobase.common.importNotes")}'/>
  </fieldset>
  <br/>
  <s:submit action="configureResults-modeEsdu" key='echobase.action.import'/>
</s:form>

<s:form id="RESULT_REGION" namespace="/importData" method="POST"
        enctype="multipart/form-data" cssClass="hidden importType">

  <fieldset>
    <legend>
      <s:text name="echobase.legend.importData.configure"/>
    </legend>

    <div class="cleanBoth help">
      <s:a href="%{getDocumentation('importData.html', null)}" target="doc">
        <s:text name="echobase.action.show.import.documentation"/>
      </s:a>
    </div>
    <br/>

    <s:hidden key="model.importType" value="RESULT_REGION" label=''/>

    <s:select key="model.voyageId" requiredLabel="true"
              label='%{getText("echobase.common.voyage")}'
              list="voyages" headerKey="" headerValue=""/>

    <s:textfield key="model.resultLabel" size="40" requiredLabel="true"
                 label='%{getText("echobase.common.resultLabel")}'/>

    <s:file key="regionsFile" requiredLabel="true"
            label='%{getText("echobase.common.cellRegionsFile")}'/>

    <s:file key="regionAssociationFile" requiredLabel="true"
            label='%{getText("echobase.common.cellRegionAssociationFile")}'/>

    <s:file key="regionResultFile" requiredLabel="true"
            label='%{getText("echobase.common.cellRegionResultFile")}'/>

    <s:textarea key="model.importNotes" cols="80" rows="5"
                label='%{getText("echobase.common.importNotes")}'/>
  </fieldset>
  <br/>
  <s:submit action="configureResults-modeRegion" key='echobase.action.import'/>
</s:form>

<s:form id="RESULT_MAP_FISH" namespace="/importData" method="POST"
        enctype="multipart/form-data" cssClass="hidden importType">

  <fieldset>
    <legend>
      <s:text name="echobase.legend.importData.configure"/>
    </legend>

    <div class="cleanBoth help">
      <s:a href="%{getDocumentation('importData.html', null)}" target="doc">
        <s:text name="echobase.action.show.import.documentation"/>
      </s:a>
    </div>
    <br/>

    <s:hidden key="model.importType" value="RESULT_MAP_FISH" label=''/>

    <s:select key="model.voyageId" requiredLabel="true"
              label='%{getText("echobase.common.voyage")}'
              list="voyages" headerKey="" headerValue=""/>

    <s:textfield key="model.resultLabel" size="40" requiredLabel="true"
                 label='%{getText("echobase.common.resultLabel")}'/>

    <s:file key="mapsFile" requiredLabel="true"
            label='%{getText("echobase.common.cellMapsFishFile")}'/>

    <s:textarea key="model.importNotes" cols="80" rows="5"
                label='%{getText("echobase.common.importNotes")}'/>
  </fieldset>
  <br/>
  <s:submit action="configureResults-modeMapFish" key='echobase.action.import'/>
</s:form>

<s:form id="RESULT_MAP_OTHER" namespace="/importData" method="POST"
        enctype="multipart/form-data" cssClass="hidden importType">

  <fieldset>
    <legend>
      <s:text name="echobase.legend.importData.configure"/>
    </legend>

    <div class="cleanBoth help">
      <s:a href="%{getDocumentation('importData.html', null)}" target="doc">
        <s:text name="echobase.action.show.import.documentation"/>
      </s:a>
    </div>
    <br/>

    <s:hidden key="model.importType" value="RESULT_MAP_OTHER" label=''/>

    <s:select key="model.voyageId" requiredLabel="true"
              label='%{getText("echobase.common.voyage")}'
              list="voyages" headerKey="" headerValue=""/>

    <s:textfield key="model.resultLabel" size="40" requiredLabel="true"
                 label='%{getText("echobase.common.resultLabel")}'/>

    <s:file key="mapsFile" requiredLabel="true"
            label='%{getText("echobase.common.cellMapsOtherFile")}'/>

    <s:textarea key="model.importNotes" cols="80" rows="5"
                label='%{getText("echobase.common.importNotes")}'/>
  </fieldset>
  <br/>
  <s:submit action="configureResults-modeMapOther" key='echobase.action.import'/>
</s:form>
