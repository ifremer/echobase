<%--
  #%L
  EchoBase :: UI
  %%
  Copyright (C) 2011 Ifremer, Codelutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  #L%
  --%>
<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>

<title>
  Configuration <s:text name="echobase.common.importType.common"/>
</title>

<script type="text/javascript">

  jQuery(document).ready(function () {

    $('[name="model.importType"]').change(function (event) {
      $('[class~="importMode"]').hide();
      $('#' + this.value).show();
      $('#' + this.value + ' .errorMessage').hide();
    });

    var incomingVal = $('[name="model.importType"][checked="checked"]').val();
    $('#' + incomingVal).show();

    $.addCheckFileSize(<s:property value="uploadFileMaxLength"/>);
  });
</script>

<s:radio id='mode' key='model.importType' list="importTypes"
         cssClass="cleanBoth" label='%{getText("echobase.common.importType")}'
         requiredLabel="true" template="myradiomap"/>

<hr/>

<%--Import mode = COMMON_ALL--%>

<s:form id="COMMON_ALL" namespace="/importData" method="POST"
        enctype="multipart/form-data" cssClass="hidden importMode">

  <fieldset>
    <legend>
      <s:text name="echobase.legend.importData.configure"/>
    </legend>

    <div class="cleanBoth help">
      <s:a href="%{getDocumentation('importData.html', null)}" target="doc">
        <s:text name="echobase.action.show.import.documentation"/>
      </s:a>
    </div>
    <br/>

    <s:select key="modelAll.missionId" requiredLabel="true"
              label='%{getText("echobase.common.mission")}'
              list="missions" headerKey="" headerValue=""/>
    <br/>

    <s:hidden key="modelAll.importType" value="COMMON_ALL" label=''/>

    <s:select key="modelAll.areaOfOperationId" requiredLabel="true"
              label='%{getText("echobase.common.areaOfOperation")}'
              list="areaOfOperations" headerKey="" headerValue=""/>

    <s:textfield key="modelAll.voyageDescription" size="80" requiredLabel="true"
                 label='%{getText("echobase.common.voyageDescription")}'/>

    <s:textfield key="modelAll.datum" size="80" requiredLabel="true"
                 label='%{getText("echobase.common.datum")}'/>

    <s:textfield key="modelAll.transitRelatedActivity" size="80" requiredLabel="true"
                 label='%{getText("echobase.common.transitRelatedActivity")}'/>

    <s:textfield key="modelAll.transectLicence" size="80" requiredLabel="true"
                 label='%{getText("echobase.common.transectLicence")}'/>

    <s:textfield key="modelAll.transectGeospatialVerticalPositive" size="80"
                 requiredLabel="true"
                 label='%{getText("echobase.common.transectGeospatialVerticalPositive")}'/>

    <s:textfield key="modelAll.transectBinUnitsPingAxis" size="80" requiredLabel="true"
                 label='%{getText("echobase.common.transectBinUnitsPingAxis")}'/>

    <s:file key="modelAllVoyageFile" requiredLabel="true"
            label='%{getText("echobase.common.voyageFile")}'/>

    <s:file key="modelAllTransitFile" requiredLabel="true"
            label='%{getText("echobase.common.transitFile")}'/>

    <s:file key="modelAllTransectFile" requiredLabel="true"
            label='%{getText("echobase.common.transectFile")}'/>

    <s:textarea key="modelAll.importNotes" cols="80" rows="5"
                label='%{getText("echobase.common.importNotes")}'/>

  </fieldset>

  <br/>
  <s:submit action="configureCommon-modeAll" key='echobase.action.import'/>

</s:form>

<%--Import mode = COMMON_VOYAGE --%>

<s:form id="COMMON_VOYAGE" namespace="/importData" method="POST"
        enctype="multipart/form-data" cssClass="hidden importMode">

  <fieldset>
    <legend>
      <s:text name="echobase.legend.importData.configure"/>
    </legend>

    <div class="cleanBoth help">
      <s:a href="%{getDocumentation('importData.html', null)}" target="doc">
        <s:text name="echobase.action.show.import.documentation"/>
      </s:a>
    </div>
    <br/>

    <s:select key="modelVoyage.missionId" requiredLabel="true"
              label='%{getText("echobase.common.mission")}'
              list="missions" headerKey="" headerValue=""/>
    <br/>

    <s:hidden key="modelVoyage.importType" value="COMMON_VOYAGE" label=''/>

    <s:select key="modelVoyage.areaOfOperationId" requiredLabel="true"
              label='%{getText("echobase.common.areaOfOperation")}'
              list="areaOfOperations" headerKey="" headerValue=""/>

    <s:textfield key="modelVoyage.voyageDescription" size="80" requiredLabel="true"
                 label='%{getText("echobase.common.voyageDescription")}'/>

    <s:textfield key="modelVoyage.datum" size="80" requiredLabel="true"
                 label='%{getText("echobase.common.datum")}'/>

    <s:file key="modelVoyageVoyageFile" requiredLabel="true"
            label='%{getText("echobase.common.voyageFile")}'/>

    <s:textarea key="modelVoyage.importNotes" cols="80" rows="5"
                label='%{getText("echobase.common.importNotes")}'/>

  </fieldset>

  <br/>
  <s:submit action="configureCommon-modeVoyage" key='echobase.action.import'/>

</s:form>

<%--Import mode = COMMON_TRANSIT --%>

<s:form id="COMMON_TRANSIT" namespace="/importData" method="POST"
        enctype="multipart/form-data" cssClass="hidden importMode">

  <fieldset>
    <legend>
      <s:text name="echobase.legend.importData.configure"/>
    </legend>

    <div class="cleanBoth help">
      <s:a href="%{getDocumentation('importData.html', null)}" target="doc">
        <s:text name="echobase.action.show.import.documentation"/>
      </s:a>
    </div>
    <br/>

    <s:hidden key="modelTransit.importType" value="COMMON_TRANSIT" label=''/>

    <s:select key="modelTransit.voyageId" requiredLabel="true"
              label='%{getText("echobase.common.voyage")}'
              list="voyages" headerKey="" headerValue=""/>

    <s:textfield key="modelTransit.transitRelatedActivity" size="80" requiredLabel="true"
                 label='%{getText("echobase.common.transitRelatedActivity")}'/>

    <s:file key="modelTransitTransitFile" requiredLabel="true"
            label='%{getText("echobase.common.transitFile")}'/>

    <s:textarea key="modelTransit.importNotes" cols="80" rows="5"
                label='%{getText("echobase.common.importNotes")}'/>

  </fieldset>

  <br/>
  <s:submit action="configureCommon-modeTransit" key='echobase.action.import'/>

</s:form>

<%--Import mode = COMMON_TRANSECT --%>

<s:form id='COMMON_TRANSECT' namespace="/importData" method="POST"
        enctype="multipart/form-data" cssClass="hidden importMode">

  <fieldset>
    <legend>
      <s:text name="echobase.legend.importData.configure"/>
    </legend>

    <s:hidden key="modelTransect.importType" value="COMMON_TRANSECT" label=''/>

    <div class="cleanBoth help">
      <s:a href="%{getDocumentation('importData.html', null)}" target="doc">
        <s:text name="echobase.action.show.import.documentation"/>
      </s:a>
    </div>
    <br/>

    <s:select key="modelTransect.voyageId" requiredLabel="true"
              label='%{getText("echobase.common.voyage")}'
              list="voyages" headerKey="" headerValue=""/>

    <s:textfield key="modelTransect.datum" size="80" requiredLabel="true"
                 label='%{getText("echobase.common.datum")}'/>

    <s:textfield key="modelTransect.transectLicence" size="80" requiredLabel="true"
                 label='%{getText("echobase.common.transectLicence")}'/>

    <s:textfield key="modelTransect.transectGeospatialVerticalPositive" size="80"
                 requiredLabel="true"
                 label='%{getText("echobase.common.transectGeospatialVerticalPositive")}'/>

    <s:textfield key="modelTransect.transectBinUnitsPingAxis" size="80" requiredLabel="true"
                 label='%{getText("echobase.common.transectBinUnitsPingAxis")}'/>

    <s:file key="modelTransectTransectFile" requiredLabel="true"
            label='%{getText("echobase.common.transectFile")}'/>

    <s:textarea key="modelTransect.importNotes" cols="80" rows="5"
                label='%{getText("echobase.common.importNotes")}'/>

  </fieldset>

  <br/>
  <s:submit action="configureCommon-modeTransect" key='echobase.action.import'/>


</s:form>
