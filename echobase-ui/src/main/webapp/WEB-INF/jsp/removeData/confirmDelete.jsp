<%--
  #%L
  EchoBase :: UI
  %%
  Copyright (C) 2011 - 2012 Ifremer, Codelutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  #L%
  --%>
<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>

<title><s:text name="echobase.title.confirm.deleteImportLogs"/></title>

<br/>

<fieldset>
  <legend>
    <s:text name="echobase.confirm.delete.selected.importData"/>
  </legend>

  <ul class="">
    <s:iterator value="importLogs">
      <li>
        <s:property/>
      </li>
    </s:iterator>
  </ul>

  <br/>
  <ul class="toolbar floatRight">
    <li>
      <s:form namespace="/removeData">
        <s:submit action="importLogs" theme="simple"
                  key="echobase.action.cancel"/>
      </s:form>
    </li>
    <li>
      <s:form namespace="/removeData">
        <s:iterator value="model.importLogIds" var="id">
          <s:hidden name="model.importLogIds" value="%{#id}"/>
        </s:iterator>
        <s:submit action="confirmDelete" theme="simple"
                  key="echobase.action.delete"/>
      </s:form>
    </li>
  </ul>
</fieldset>
