<%--
#%L
  EchoBase :: UI
  %%
  Copyright (C) 2011 Ifremer, Codelutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  #L%
--%>
<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags" %>
<script type="text/javascript"
        src="<s:url value='/js/gridHelper.js' />"></script>

<script type="text/javascript">

  function reloadPage(params) {
    var url = "<s:url action='importLogs' namespace='/removeData'/>?";
    window.location = url + $.param(params);
    return false;
  }

  jQuery(document).ready(function () {

    $('[name="entityId"]').change(function (event) {
      reloadPage({'entityId':this.value});
    });
    
    $.addClearSelectTopic('importLogs', function (event) {

      $(':checkbox[name^="jqg"]').each(function () {
        var elem = $(this);
        var prefixLength = "jqg_importLogs_".length;
        var newId = this.id.substring(prefixLength);
        elem.attr("name", "model.importLogIds");
        elem.attr("value", newId);
      });
    });
    $.addEvenAndOddClasses('importLogs');
  });

  function checkOneRowSelected() {
    var checked = $(':checked[name="model.importLogIds"]');
    var result = !!checked.length;
    if (!result) {
      alert('<s:text name="echobase.warning.no.importLog.selected"/>');
    }
    return result;
  }

</script>

<title><s:text name="echobase.title.importLogs"/></title>

<fieldset>
  <legend><s:text name="echobase.legend.select"/></legend>
  <s:select key="entityId" cssStyle="font-size: 140%"
            label="%{getText('echobase.common.voyage')}"
            list="voyages" headerKey="" headerValue=""/>
  <s:select key="entityId" cssStyle="font-size: 140%"
            label="%{getText('echobase.common.mooring')}"
            list="moorings" headerKey="" headerValue=""/>
</fieldset>
<s:form method="post" namespace="/removeData" action="confirmDelete">

  <s:url id="loadUrl" action="getImportLogs" namespace="/removeData"
         escapeAmp="false">
    <s:param name="entityId" value="%{entityId}"/>
  </s:url>
  <sjg:grid id="importLogs" dataType="json" href="%{loadUrl}" gridModel="datas"
            pager="true" pagerButtons="true" pagerInput="true" navigator="true"
            autowidth="true" rownumbers="false" viewrecords="true"
            navigatorEdit="false" navigatorSearch="false"
            navigatorDelete="false"
            navigatorAdd="false" rowList="10,15,20,50,100,250,500" rowNum="10"
            multiselect="true"
            onCompleteTopics="importLogs-clearSelect,importLogs-CompleteTopics">

    <sjg:gridColumn name="id" title="id" hidden="true"/>
    <sjg:gridColumn name="importType" sortable="true"
                    title="%{getText('echobase.common.importType')}"/>
    <sjg:gridColumn name="entityId_lbl" sortable="true"
                    title="%{getText('echobase.common.importEntity')}"/>
    <sjg:gridColumn name="importDate" sortable="true"
                    title="%{getText('echobase.common.importDate')}"/>
    <sjg:gridColumn name="importText" sortable="true"
                    title="%{getText('echobase.common.importText')}"/>
    <sjg:gridColumn name="importUser" sortable="true"
                    title="%{getText('echobase.common.importUser')}"/>
  </sjg:grid>
  <br/>
  <s:submit id='confirmDelete' action="confirmDelete" method="input"
            onclick="return checkOneRowSelected();"
            key="echobase.action.delete.selectedImport"/>

</s:form>
