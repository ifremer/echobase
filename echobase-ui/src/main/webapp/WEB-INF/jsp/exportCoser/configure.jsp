<%--
  #%L
  EchoBase :: UI
  %%
  Copyright (C) 2011 Ifremer, Codelutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  #L%
  --%>
<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>

<title><s:text name="echobase.title.exportCoser"/></title>

<s:url id="getZonesUrl" namespace="/exportCoser" action="getZones"/>

<script type="text/javascript"
        src="<s:url value='/js/gridHelper.js' />"></script>

<script type="text/javascript">

  // selection tous les elements d'un select
  function coserSelectAll(select) {
    $(select).children("option").prop('selected', true);
    $(select).change();
  }

  // deselection tous les elements d'un select
  function coserUnSelectAll(select) {
    $(select).children("option").prop('selected', false);
    $(select).change();
  }

  function changeExtractIndicator(checkbox, select) {

      if ($(checkbox).is(":checked")) {
          $('#' + select).attr('disabled', false);
          $('.' + select).show();
      }
      else {
          $('#' + select).attr('disabled', 'disabled');
          $('.' + select).hide();
      }
  }

  $(document).ready(function () {

    $.autoSelectZones(
      $('#facadeSelectBox'),
      $('#zoneSelectBox'),
      '<s:url action="getZones" namespace="/exportCoser"/>',
      '<s:property value="model.facade"/>',
      '<s:property value="model.zone"/>'
    );

      $.autoSelectRegionIndicators(
          $('#missionField'),
          $('#communityIndicator'),
          $('#populationIndicator'),
          '<s:url action="getAvailableIndicatorsForMission" namespace="/exportCoser"/>',
          '<s:property value="model.missionId"/>');

    var communityChangefunction = function () { changeExtractIndicator('#extractCommunityIndicator', 'communityIndicator'); };
    $('#extractCommunityIndicator').change(communityChangefunction);
    communityChangefunction();

    var populationChangefunction = function () { changeExtractIndicator('#extractPopulationIndicator', 'populationIndicator'); };
    $('#extractPopulationIndicator').change(populationChangefunction);
    populationChangefunction();

  });
</script>

<s:if test="coserUnreachable">

    <s:text name="echobase.warning.coser.not.reachable">
        <s:param value="serviceContext.coserApiURL"/>
    </s:text>

</s:if>
<s:else>
    <s:form namespace="/exportCoser" method="POST">
        <fieldset>
            <legend>
                <s:text name="echobase.legend.exportCoser.configuration.files"/>
            </legend>

            <div class="cleanBoth help">
                <s:a href="%{getDocumentation('exportCoser')}" target="doc">
                    <s:text name="echobase.action.show.exportCoser.documentation"/>
                </s:a>
            </div>
            <br/>

            <s:select id='missionField' key="model.missionId" requiredLabel="true"
                      label='%{getText("echobase.common.mission")}'
                      list="missions" headerKey="" headerValue=""/>

            <div class="cleanBoth"></div>
            <br/>

            <s:select id='facadeSelectBox' key="model.facade" requiredLabel="true"
                      label='%{getText("echobase.common.facade")}'
                      list="facades" headerKey="" headerValue=""/>

            <div class="cleanBoth"></div>
            <br/>

            <s:select id='zoneSelectBox' key="model.zone" requiredLabel="true"
                      label='%{getText("echobase.common.zone")}'
                      list="zones" headerKey="" headerValue=""/>

            <div class="cleanBoth"></div>
            <br/>
            <s:checkbox name="model.publishable" label='%{getText("echobase.common.publishable")}'/>
            <s:checkbox name="model.extractMap" label='%{getText("echobase.common.extractMaps")}'/>
            <s:checkbox name="model.extractRawData" label='%{getText("echobase.common.extractRawData")}'/>
            <div class="cleanBoth"></div>
            <br/>
            <s:textfield name="model.userName" label='%{getText("echobase.common.userName")}'/>
            <div class="cleanBoth"></div>
            <s:textarea name="model.comment" label='%{getText("echobase.common.comment")}' cols="80" rows="5"/>
            <div class="cleanBoth"></div>
            <s:checkbox id='extractPopulationIndicator' name="model.extractPopulationIndicator" label='%{getText("echobase.common.extractPopulationIndicator")}'/>
            <div class="cleanBoth"></div>
    <span class="populationIndicator">
    <s:select id='populationIndicator' key="model.populationIndicator"
              label='%{getText("echobase.common.populationIndicator")}'
              list="communityIndicators" multiple="true" size="10"
              cssClass="ui-pg-input" requiredLabel="true"/>
    <br/>
    <span style="vertical-align:top">
      <img src="<s:url value='/images/stock_select_table.png' />"
           onClick="coserSelectAll($('#populationIndicator'))"
           title="<s:text name="echobase.common.selectall" />"/>
      <br/>
      <img src="<s:url value='/images/stock_select_clear.png' />"
           onClick="coserUnSelectAll($('#populationIndicator'))"
           title="<s:text name="echobase.common.selectnone" />"/>
    </span>
    <div class="cleanBoth"></div>
    <br/>
    </span>

            <s:checkbox id='extractCommunityIndicator' name="model.extractCommunityIndicator" label='%{getText("echobase.common.extractCommunityIndicator")}'/>
            <div class="cleanBoth"></div>
    <span class="communityIndicator">
    <s:select id='communityIndicator' key="model.communityIndicator"
              label='%{getText("echobase.common.communityIndicator")}'
              list="communityIndicators" multiple="true" size="10"
              cssClass="ui-pg-input" requiredLabel="true"/>
    <br/>
    <span style="vertical-align:top">
      <img src="<s:url value='/images/stock_select_table.png' />"
           onClick="coserSelectAll($('#communityIndicator'))"
           title="<s:text name="echobase.common.selectall" />"/>
      <br/>
      <img src="<s:url value='/images/stock_select_clear.png' />"
           onClick="coserUnSelectAll($('#communityIndicator'))"
           title="<s:text name="echobase.common.selectnone" />"/>
    </span>
    <div class="cleanBoth"></div>
    <br/>
    </span>

        </fieldset>
        <br/>
        <s:submit action="configure" value='%{getText("echobase.action.export")}'/>
    </s:form>
</s:else>

