<%--
  #%L
  EchoBase :: UI
  %%
  Copyright (C) 2011 - 2013 Ifremer, Codelutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  #L%
  --%>
<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<title>
    <s:text name="echobase.title.show.spatial"/>
</title>

<s:if test="spatialSupport">

    <%--Db supports spatial features --%>

    <s:if test="spatialStructureFound">

        <%-- db has spatial structures --%>

        <s:if test="spatialDataToComputeExists">

            <%-- there is some postigs data to compute --%>

            <s:form namespace="/spatial">
                <div class="toolbar">
                    <ul class="toolbar floatRight">
                        <li>
                            <s:submit action='computeSpatialData' key="echobase.action.computeSpatialData"/>
                        </li>
                    </ul>
                </div>
            </s:form>

        </s:if>
        <s:else>

            <s:if test="gisSupport">

                <fieldset>
                    <legend><s:text name="echobase.title.voyage.detail"/></legend>
                    <ul>
                        <s:iterator value="voyages" var="voyage">
                            <li>
                                <s:a action="showVoyageMap">
                                    <s:param name="voyageId" value="%{key}"/>
                                    <s:property value="value"/>
                                </s:a>
                            </li>
                        </s:iterator>
                    </ul>
                </fieldset>

            </s:if>
            <s:else>

                <%-- Application has no gis support --%>
                <p class="fontsize11">
                    <s:text name="echobase.message.application.no.gis.support"/>
                    <s:a href="%{getDocumentation('install.html')}"
                         target='#doc'>
                        <s:text name="echobase.message.gis.install"/>
                    </s:a>
                </p>

            </s:else>

        </s:else>

    </s:if>
    <s:else>

        <%--db is not spatialized--%>
        <p>
            <s:text name="echobase.message.db.no.spatial.structure"/>
        </p>

        <br/>
        <s:form namespace="/spatial">
            <div class="toolbar">
                <ul class="toolbar floatRight">
                    <li>
                        <s:submit action='addSpatial' key="echobase.action.workingDbconfiguration.addSpatial"/>
                    </li>
                </ul>
            </div>
        </s:form>

    </s:else>

</s:if>
<s:else>

    <%--Db has no spatial support --%>
    <p>
        <s:text name="echobase.message.db.no.spatial.support"/>
    </p>


</s:else>
