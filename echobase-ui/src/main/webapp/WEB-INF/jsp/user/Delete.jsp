<%--
  #%L
  EchoBase :: UI
  %%
  Copyright (C) 2011 Ifremer, Codelutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  #L%
  --%>
<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags" %>

<title><s:text name="echobase.label.admin.user.delete"/></title>

<s:form method="post" validate="true" namespace="/user" action="user-Delete">
  <fieldset>
    <legend>
      <s:text name="echobase.common.user"/>
    </legend>
    <s:hidden key="user.topiaId" label=""/>
    <s:textfield key="user.email" label="%{getText('echobase.common.email')}"
                 size="40" disabled="true"/>
    <s:checkbox value="%{user.admin}" key="echobase.common.admin"
                disabled="true"/>
  </fieldset>
  <ul class="toolbar floatRight">
    <li>
      <s:submit action="user-Delete" key="echobase.action.delete"
                theme="simple"/>
    </li>
    <li>
      <s:submit action="userList" key="echobase.action.backToUserList"
                theme="simple"/>
    </li>
  </ul>
</s:form>


