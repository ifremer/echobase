<%--
#%L
  EchoBase :: UI
  %%
  Copyright (C) 2011 Ifremer, Codelutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  #L%
--%>
<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags" %>
<script type="text/javascript"
        src="<s:url value='/js/gridHelper.js' />"></script>

<title><s:text name="echobase.title.users"/></title>

<s:url id="loadUrl" action="getUsers" namespace="/user" escapeAmp="false"/>

<s:url id="addUrl" action="user-Create" namespace="/user" escapeAmp="false" method="input"/>

<s:url id="updateUrl" action="user-Update" namespace="/user" escapeAmp="false" method="input"/>

<s:url id="delUrl" action="user-Delete" namespace="/user" escapeAmp="false" method="input"/>

<script type="text/javascript">

  jQuery(document).ready(function () {
    $.addRowSelectTopic('users');
    $.addClearSelectTopic('users');
    $.addAddRowTopic('users', '${addUrl}');
    $.addSingleRowTopic('users', 'Update', '${updateUrl}', 'user.topiaId');
    $.addSingleRowTopic('users', 'Delete', '${delUrl}', 'user.topiaId');
    $.addEvenAndOddClasses('users');
  });
</script>

<sjg:grid id="users" dataType="json" href="%{loadUrl}" gridModel="datas"
          pager="true" pagerButtons="true" pagerInput="true"
          navigator="true" rownumbers="false" autowidth="true"
          onSelectRowTopics='users-rowSelect'
          onCompleteTopics='users-cleanSelect,users-CompleteTopics'
          navigatorEdit="false" navigatorDelete="false"
          navigatorSearch="false" navigatorRefresh="false"
          navigatorAdd="false" viewrecords="true"
          rowList="10,15,20,50,100,250,500" rowNum="10"
          navigatorExtraButtons="{
                add: { title : 'Ajouter', icon: 'ui-icon-plus', topic: 'users-rowAdd' },
                update: { title : 'Mettre à jour', icon: 'ui-icon-pencil', topic: 'users-rowUpdate' },
                delete: { title : 'Supprimer', icon: 'ui-icon-trash', topic: 'users-rowDelete' }
        }">
  <sjg:gridColumn name="id" title="id" hidden="true"/>
  <sjg:gridColumn name="email" width="600" title='%{getText("echobase.common.email")}'
                  sortable="false"/>
  <sjg:gridColumn name="admin" title='%{getText("echobase.common.admin")}'
                  sortable="false" width="100" formatter="checkbox"/>
</sjg:grid>
