<%--
  #%L
  EchoBase :: UI
  %%
  Copyright (C) 2011 Ifremer, Codelutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  #L%
  --%>
<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags" %>

<title><s:text name="echobase.label.admin.user.edit"/></title>

<s:form method="post" validate="true" namespace="/user" action="user-Update">
  <fieldset>
    <legend>
      <s:text name="echobase.common.user"/>
    </legend>
    <s:hidden key="user.topiaId" label=""/>
    <s:textfield key="user.email" label="%{getText('echobase.common.email')}"
                 size="40" readonly="true"/>
    <s:password name="user.password" value=""
                label="%{getText('echobase.common.password')} (*)"
                size="40"/>
    <s:checkbox key="user.admin" label="%{getText('echobase.common.admin')}"/>
    <br/>

    <div class="cleanBoth">
      (*) <s:text name="echobase.info.update.user.password"/>
    </div>
  </fieldset>
  <ul class="toolbar floatRight">
    <li>
      <s:submit action="user-Update"
                key="echobase.action.save" theme="simple"/>
    </li>
    <li>
      <s:submit action="userList" key="echobase.action.backToUserList"
                theme="simple"/>
    </li>
  </ul>
</s:form>


