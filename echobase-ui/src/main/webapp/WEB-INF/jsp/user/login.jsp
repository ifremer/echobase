<%--
  #%L
  EchoBase :: UI
  %%
  Copyright (C) 2011 Ifremer, Codelutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  #L%
  --%>
<html xmlns="http://www.w3.org/1999/xhtml">
<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
  <title>Echobase - <s:text name="echobase.label.login"/></title>
  <link rel="stylesheet" type="text/css"
        href="<s:url value='/css/screen.css' />"/>
  <link rel="icon" type="image/png"
        href="<s:url value='/images/logo_codelutin.png' />"/>
  <sj:head jqueryui="true" jquerytheme="echobase-theme"/>
</head>

<body>

<%--header--%>

<div class='displayBlock top'>
  <%@ include file="/WEB-INF/includes/i18n.jsp" %>
  <div class='floatLeft'>
    <s:a action="home" cssClass="logo" namespace="/">Echobase</s:a>
  </div>
  <div id='headerRight'>

  </div>
</div>
<div class="cleanBoth"></div>
<div class="bodyHomeSize">
  <div class="containerHomeSize">
    <%-- content --%>
    <div class="containerHome ui-corner-all containerHomeSize">

      <h2><s:text name="echobase.title.login"/></h2>

      <s:form method="POST" namespace="/user">
        <fieldset class="ui-corner-all">
          <s:hidden key="redirectAction" label=""/>
          <s:textfield name="email" key="echobase.common.email"
                       requiredLabel="true" size="60"/>
          <s:password name="password" key="echobase.common.password"
                      requiredLabel="true" size="60"/>
        </fieldset>
        <br/>
        <s:submit action="login" key="echobase.action.login" align="right"/>
      </s:form>
    </div>

    <%-- footer --%>
    <%@ include file="/WEB-INF/includes/footer.jsp" %>
  </div>
</div>
</body>
</html>
