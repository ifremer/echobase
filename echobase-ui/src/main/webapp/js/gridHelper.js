/*
 * #%L
 * EchoBase :: UI
 * %%
 * Copyright (C) 2011 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
(function ($) {

    $.fn.extend(
            {
                addRowSelectTopic: function (gridId, callback) {
                    $.subscribe(gridId + '-rowSelect', function (event) {
                        if (event.originalEvent) {
                            var gridId = event.data.id;
                            var opts = jQuery.struts2_jquery[gridId] = {};

                            var id = event.originalEvent.id;
                            if (id) {
                                opts['selectedRow'] = id;
                            }
                        }
                        var callback = event.data.callback;
                        if (callback) {
                            callback(event);
                        }
                    }, {id: gridId, callback: callback});
                },

                addClearSelectTopic: function (gridId, callback) {
                    $.subscribe(gridId + '-clearSelect', function (event) {
                        var gridId = event.data.id;
                        jQuery.struts2_jquery[gridId] = {};
                        var callback = event.data.callback;
                        if (callback) {
                            callback(event);
                        }
                    }, {id: gridId, callback: callback});
                },

                addAddRowTopic: function (gridId, url) {
                    $.subscribe(gridId + '-rowAdd', function (event) {
                        window.location = event.data.url;
                    }, {id: gridId, url: url});
                },

                addSingleRowTopic: function (gridId, action, url, parameterName) {
                    $.subscribe(gridId + '-row' + action, function (event) {
                        var gridId = event.data.id;
                        var opts = jQuery.struts2_jquery[gridId];
                        if (opts && opts['selectedRow']) {
                            var selectedId = opts['selectedRow'];
                            var parameterName = event.data.parameterName;
                            var params = {};
                            params[parameterName] = selectedId;
                            window.location = $.prepareUrl(event.data.url, params);
                        }
                    }, {id: gridId, url: url, parameterName: parameterName});
                },

                addSingleRowTopic2: function (gridId, action, callback) {
                    $.subscribe(gridId + '-row' + action, function (event) {


                        var gridId = event.data.id;
                        var opts = jQuery.struts2_jquery[gridId];
                        var selectedId = opts['selectedRow'];
                        if (selectedId) {
                            event.data.callback(event, selectedId);
                        }
                    }, {id: gridId, callback: callback});
                },

                addMultiRowTopic: function (gridId, action, target, checkboxName, callback) {
                    $.subscribe(gridId + '-row' + action, function (event) {
                        var gridId = event.data.id;
                        var prefix = 'jqg_' + gridId + '_';
                        var prefixLength = prefix.length;

                        // get all selected ids
                        var inputs = $('table#' + gridId + ' :checked[id^="' + prefix + '"]');

                        if (inputs && inputs.length) {
                            var checkboxName = event.data.checkboxName;
                            inputs.each(function () {
                                var id = this.id;
                                var newId = id.substring(prefixLength);
                                $.attr(this, "name", checkboxName);
                                $.attr(this, "value", newId);
                                $.attr(this, "checked", "checked");
                            });
                            var callback = event.data.callback;
                            if (callback) {
                                callback();
                            }
                            event.data.target.click();
                        }
                    }, {id: gridId,
                                    target: target,
                                    checkboxName: checkboxName,
                                    callback: callback});
                },

                serializeCheckboxs: function (id, params, newId) {
                    var all = $(':checkbox[name="' + id + '"]');
                    var selected = $(':checked[name="' + id + '"]');

                    if (all.length != selected.length) {

                        // apply a year filter
                        var list = [];
                        selected.each(function () {
                            list.push(this.value);
                        });
                        if (!newId) {
                            newId = id;
                        }
                        params[newId] = list;
                    }
                },

                prepareUrl: function (url, params) {
                    var result = url;
                    if (url.indexOf("?") > -1) {
                        result += "&";
                    } else {
                        result += "?";
                    }
                    result += $.param(params);
                    return result;
                },

                updateSelectBoxContent: function (url, params, jsonTarget, target, callback) {

                    // call url to obtain datas to inject in target select box
                    jQuery.getJSON(url, params, function (result) {

                        $.fillSelectBox(result, jsonTarget, target, true, callback);

                        //// will contains selectBox html code
                        //var html = "<option/>";
                        //
                        //// iterate on found datas (each data gives a select option)
                        //$.each(result[jsonTarget], function (propertyName, value) {
                        //    html += '<option value="' + propertyName + '">' + value + '</option>'
                        //});
                        //
                        //// change the target html code
                        //target.html(html);
                        //
                        //// calback
                        //if (callback) {
                        //    callback();
                        //}
                    });
                },

                fillSelectBox: function (result, jsonTarget, target,addEmptyEntry,  callback) {

                        // will contains selectBox html code
                        var html = "";

                    if (addEmptyEntry) {

                        html += "<option/>";
                    }

                        // iterate on found datas (each data gives a select option)
                        $.each(result[jsonTarget], function (propertyName, value) {
                            html += '<option value="' + propertyName + '">' + value + '</option>'
                        });

                        // change the target html code
                        target.html(html);

                        // calback
                        if (callback) {
                            callback();
                        }
                },

                // Ajoute les classes even et odd à tous les tableaux de la page
                addEvenAndOddClasses: function (gridId) {

                    // On souscrit au topic qui appelle la fonction addEvenAndOddClasses
                    $.subscribe(gridId + '-CompleteTopics', function () {
                        $('table tr:even').addClass('even');
                        $('table tr:odd').addClass('odd');
                    });

                },

                // auto-selection des voyages - vessel
                autoSelectVoyageAndVessel: function (voyageSelectBox, vesselSelectBox, getUrl, voyageId, vesselId) {

                    voyageSelectBox.change({vesselSelectBox: vesselSelectBox,
                                               getUrl: getUrl}, function (event) {

                        $.updateSelectBoxContent(event.data.getUrl,
                                                 {voyageId: this.value},
                                                 'vessels',
                                                 event.data.vesselSelectBox
                        );
                    });

                    if (voyageId) {

                        voyageSelectBox.change(voyageId);

                        $.updateSelectBoxContent(getUrl,
                                                 {voyageId: voyageId},
                                                 'vessels',
                                                 vesselSelectBox, function () {

                                    if (vesselId) {

                                        vesselSelectBox.val(vesselId);
                                    }
                                }
                        );
                    }
                },

                // auto-selection des voyages/mooring - dataProcessing
                autoSelectProviderAndDataProcessing: function (selectBox, datapProcessingSelectBox, getUrl, entityId, dataProcessingId) {

                    selectBox.change({datapProcessingSelectBox: datapProcessingSelectBox,
                                               getUrl: getUrl}, function (event) {

                        $.updateSelectBoxContent(event.data.getUrl,
                                                 {entityId: this.value},
                                                 'dataProcessings',
                                                 event.data.datapProcessingSelectBox
                        );
                    });

                    if (entityId) {

                        selectBox.change(entityId);

                        $.updateSelectBoxContent(getUrl,
                                                 {entityId: entityId},
                                                 'dataProcessings',
                                                 datapProcessingSelectBox, function () {

                                    if (dataProcessingId) {
                                        datapProcessingSelectBox.val(dataProcessingId);
                                    }
                                }
                        );
                    }
                },
                // auto-selection des zones
                autoSelectZones: function (facadeSelectBox, zoneSelectBox, getUrl, facadeId, zoneId) {

                    facadeSelectBox.change({zoneSelectBox: zoneSelectBox,
                                               getUrl: getUrl}, function (event) {

                        $.updateSelectBoxContent(event.data.getUrl,
                                                 {facade: this.value},
                                                 'zones',
                                                 event.data.zoneSelectBox
                        );
                    });

                    if (facadeId) {

                        facadeSelectBox.change(facadeId);

                        $.updateSelectBoxContent(getUrl,
                                                 {facade: facadeId},
                                                 'zones',
                                                 zoneSelectBox, function () {

                                    if (zoneId) {

                                        zoneSelectBox.val(zoneId);
                                    }
                                }
                        );
                    }
                },
                // auto-selection des indices (pour une mission donnee)
                autoSelectRegionIndicators: function (missionSelectBox, communityIndicatorSelectBox, populationIndicatorSelectBox, getUrl, missionId) {

                    missionSelectBox.change({communityIndicatorSelectBox: communityIndicatorSelectBox,
                                                populationIndicatorSelectBox: populationIndicatorSelectBox,
                                               getUrl: getUrl}, function (event) {

                        // call url to obtain datas to inject in target select box
                        jQuery.getJSON(event.data.getUrl, {missionId: this.value}, function (result) {


                            $.fillSelectBox(result, 'communityIndicators', event.data.communityIndicatorSelectBox, false);
                            $.fillSelectBox(result, 'populationIndicators', event.data.populationIndicatorSelectBox, false);

                        });

                    });

                    if (missionId) {

                        missionSelectBox.change(missionId);

                        // call url to obtain datas to inject in target select box
                        jQuery.getJSON(getUrl, {missionId: missionId}, function (result) {

                            $.fillSelectBox(result, 'communityIndicators', communityIndicatorSelectBox);
                            $.fillSelectBox(result, 'populationIndicators', populationIndicatorSelectBox);

                        });

                    }
                }
                });

    $.extend({
                 addRowSelectTopic: function (gridId, callback) {
                     return $(document).addRowSelectTopic(gridId, callback);
                 },

                 addClearSelectTopic: function (gridId, callback) {
                     return $(document).addClearSelectTopic(gridId, callback);
                 },

                 addAddRowTopic: function (gridId, url) {
                     return $(document).addAddRowTopic(gridId, url);
                 },

                 addSingleRowTopic: function (gridId, action, url, parameterName) {
                     return $(document).addSingleRowTopic(gridId, action, url, parameterName);
                 },

                 addSingleRowTopic2: function (gridId, action, callback) {
                     return $(document).addSingleRowTopic2(gridId, action, callback);
                 },

                 addMultiRowTopic: function (gridId, action, target, checkboxName, calbback) {
                     return $(document).addMultiRowTopic(gridId, action, target, checkboxName, calbback);
                 },
                 serializeCheckboxs: function (id, params, newId) {
                     return $(document).serializeCheckboxs(id, params, newId);
                 },
                 prepareUrl: function (url, params) {
                     return $(document).prepareUrl(url, params);
                 },
                 updateSelectBoxContent: function (url, params, jsonTarget, target, callback) {
                     return $(document).updateSelectBoxContent(url, params, jsonTarget, target, callback);
                 },
                 fillSelectBox: function (result, jsonTarget, target, callback) {
                     return $(document).fillSelectBox(result, jsonTarget, target, callback);
                 },
                 addEvenAndOddClasses: function (gridId) {
                     return $(document).addEvenAndOddClasses(gridId);
                 },
                 autoSelectVoyageAndVessel: function (voyageSelectBox, vesselSelectBox, getUrl, voyageId, vesselId) {
                     return $(document).autoSelectVoyageAndVessel(voyageSelectBox, vesselSelectBox, getUrl, voyageId, vesselId);
                 },
                 autoSelectProviderAndDataProcessing: function (voyageSelectBox, dataProcessingSelectBox, getUrl, voyageId, dataProcessingId) {
                     return $(document).autoSelectProviderAndDataProcessing(voyageSelectBox, dataProcessingSelectBox, getUrl, voyageId, dataProcessingId);
                 },
                 autoSelectZones: function (facadeSelectBox, zoneSelectBox, getUrl, facadeId, zoneId) {
                     return $(document).autoSelectZones(facadeSelectBox, zoneSelectBox, getUrl, facadeId, zoneId);
                 },
                 autoSelectRegionIndicators: function (missionSelectBox, communityIndicatorSelectBox, populationIndicatorSelectBox, getUrl, missionId) {
                     return $(document).autoSelectRegionIndicators(missionSelectBox, communityIndicatorSelectBox, populationIndicatorSelectBox, getUrl, missionId);
                 }
             });
})(jQuery);

