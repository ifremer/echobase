=============================
EchoBase - ${project.version}
=============================

Premier démarrage
-----------------

Pour démarrer EchoBase :

- Il faut avoir une JDK d'installer sur la machine cible.

- la variable JDK_HOME doit être positionnée dans les variables d'environements.

En suite on lance

./startEchobase.sh (sous linux)

ou

startEchobase.bat (sous windows)

Pilotes jdbc
------------

Le répertoire 'drivers' contient les pilotes jdbc à positionner dans libre-office
pour pouvoir interroger l'application embarquée.
