#!/bin/sh

# Test if java exists
java -version

if [ ! $? -eq 0 ]; then
  echo "Do not find java, please install java on your computer"
  exit 1
fi

OLD_PWD=`pwd`
cd `dirname $0`
ECHOBASE_OPTS="$JAVA_OPTS -Xms512m -Xmx1024m -Dechobase.log.dir=./logs"
java $ECHOBASE_OPTS -jar ${embeddedWarName}.war $*
cd "$OLD_PWD"

