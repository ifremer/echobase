@REM ----------------------------------------------------------------------------
@REM EchoBase Start Up Batch script
@REM
@REM ----------------------------------------------------------------------------
@echo off

java -version

IF "%ERRORLEVEL%" == "0" GOTO launch

echo.
echo ERROR: "java.exe" not found.
echo Please install java on your computer.
echo.
GOTO error

:error
set ERROR_CODE=1
goto end

:launch
SET JAVA_COMMAND="java.exe"

SET ECHOBASE_OPTS="-Xms512m -Xmx1024m -Dechobase.log.dir=./logs"
ECHO java options used : %ECHOBASE_OPTS%

%JAVA_COMMAND% "%ECHOBASE_OPTS%" -jar ${embeddedWarName}.war
GOTO end

:end
PAUSE
CMD /C exit /B %ERROR_CODE%
