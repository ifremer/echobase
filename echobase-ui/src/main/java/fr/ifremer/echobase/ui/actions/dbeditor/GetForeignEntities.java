/*
 * #%L
 * EchoBase :: UI
 * %%
 * Copyright (C) 2011 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ifremer.echobase.ui.actions.dbeditor;

import fr.ifremer.echobase.entities.EchoBaseUserEntityEnum;
import fr.ifremer.echobase.services.service.UserDbPersistenceService;
import fr.ifremer.echobase.ui.actions.EchoBaseActionSupport;

import javax.inject.Inject;
import java.util.Map;

/**
 * Obtains the html select form for the given entity type.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 0.1
 */
public class GetForeignEntities extends EchoBaseActionSupport {

    private static final long serialVersionUID = 1L;

    /** Type of entity to load. */
    protected EchoBaseUserEntityEnum entityType;

    public void setEntityType(EchoBaseUserEntityEnum entityType) {
        this.entityType = entityType;
    }

    /** Entities loaded (key are id, values are decoration of entity). */
    protected Map<String, String> entities;

    public Map<String, String> getEntities() {
        return entities;
    }

    @Override
    public String execute() throws Exception {

        entities = userDbPersistenceService.loadSortAndDecorate(entityType.getContract());

        return SUCCESS;
    }

    //------------------------------------------------------------------------//
    //-- Injected objects                                                     //
    //------------------------------------------------------------------------//

    @Inject
    protected transient UserDbPersistenceService userDbPersistenceService;
}
