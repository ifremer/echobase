/*
 * #%L
 * EchoBase :: UI
 * %%
 * Copyright (C) 2011 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ifremer.echobase.ui.actions.user;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.interceptor.I18nInterceptor;
import fr.ifremer.echobase.entities.EchoBaseUser;
import fr.ifremer.echobase.services.service.UserService;
import fr.ifremer.echobase.ui.EchoBaseSession;
import fr.ifremer.echobase.ui.actions.EchoBaseActionSupport;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.interceptor.SessionAware;

import javax.inject.Inject;
import java.util.Locale;
import java.util.Map;

/**
 * Login and Logout action.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 0.1
 */
public class Login extends EchoBaseActionSupport implements SessionAware {

    protected static final Log log = LogFactory.getLog(Login.class);

    private static final long serialVersionUID = 1L;

    protected String email;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    protected String password;

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    protected String redirectAction;

    public String getRedirectAction() {
        return redirectAction;
    }

    public void setRedirectAction(String redirectAction) {
        this.redirectAction = redirectAction;
    }

    @Override
    public String execute() throws Exception {

        EchoBaseUser user = userService.getUserByEmail(email);

        EchoBaseSession userSession = getEchoBaseSession();

        // user is authorized, keep it in his echoBaseSession
        userSession.setUser(user);

        // let's register
        getEchoBaseApplicationContext().registerEchoBaseSession(userSession);

        // add locale in echoBaseSession if required
        Object o = session.get(I18nInterceptor.DEFAULT_SESSION_ATTRIBUTE);
        if (o == null) {

            // push the locale in echoBaseSession to be able to use it everywhere
            Locale locale = ActionContext.getContext().getLocale();
            session.put(I18nInterceptor.DEFAULT_SESSION_ATTRIBUTE, locale);
        }

        // redecode parameters
        if (log.isInfoEnabled()) {
            log.info("success login for user " + email +
                     ", will redirect to " + redirectAction);
        }
        return "redirect";
    }

    //------------------------------------------------------------------------//
    //-- Injected objects                                                     //
    //------------------------------------------------------------------------//

    private transient Map<String, Object> session;

    @Override
    public void setSession(Map<String, Object> session) {
        this.session = session;
    }

    @Inject
    protected transient UserService userService;
}
