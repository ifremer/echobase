package fr.ifremer.echobase.ui.actions;

/*
 * #%L
 * EchoBase :: UI
 * %%
 * Copyright (C) 2011 - 2012 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.opensymphony.xwork2.interceptor.annotations.InputConfig;
import fr.ifremer.echobase.services.AbstractEchobaseActionConfiguration;
import fr.ifremer.echobase.services.service.DecoratorService;
import fr.ifremer.echobase.services.service.UserDbPersistenceService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.inject.Inject;
import java.io.IOException;

/**
 * BAse action for long action configuration.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.1
 */
public abstract class AbstractConfigureAction<M extends AbstractEchobaseActionConfiguration> extends EchoBaseActionSupport {

    private static final long serialVersionUID = 1L;

    /** Logger. */
    private static final Log log =
            LogFactory.getLog(AbstractConfigureAction.class);

    private final Class<M> modelType;

    private M model;

    protected abstract M createModel();

    protected abstract void prepareExecuteAction(M model) throws IOException;

    protected AbstractConfigureAction(Class<M> modelType) {
        this.modelType = modelType;
    }

    @Override
    public final String input() throws Exception {

        // always remove configuration from session anytime coming here
        getEchoBaseSession().removeActionConfiguration(modelType);

        prepareInputAction(getModel());

        return INPUT;
    }

    @InputConfig(methodName = "input")
    @Override
    public final String execute() throws Exception {

        prepareExecuteAction(getModel());

        setInSession(getModel());
        return SUCCESS;
    }

    public final M getModel() {
        if (model == null) {
            if (log.isInfoEnabled()) {
                log.info("Will create model " + modelType.getName());
            }
            model = createModel();
        }
        return model;
    }

    protected void prepareInputAction(M model) {
        // by default nothing to prepare
    }

    protected final void setInSession(M model) {
        getEchoBaseSession().setActionConfiguration(model);
    }

    protected final M getFromSession() {
        return getEchoBaseSession().getActionConfiguration(modelType);
    }

    //------------------------------------------------------------------------//
    //-- Injected objects                                                     //
    //------------------------------------------------------------------------//

    @Inject
    protected transient UserDbPersistenceService userDbPersistenceService;

    @Inject
    protected transient DecoratorService decoratorService;
}
