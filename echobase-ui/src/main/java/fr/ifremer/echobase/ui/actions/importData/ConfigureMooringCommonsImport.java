/*
 * #%L
 * EchoBase :: UI
 * %%
 * Copyright (C) 2011 - 2012 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ifremer.echobase.ui.actions.importData;

import fr.ifremer.echobase.services.service.importdata.configurations.MooringCommonsMooringImportConfiguration;

import java.io.File;

/**
 * Configure a data import for mooring.
 *
 * @author Julien Ruchaud - ruchaud@codelutin.com
 * @since 0.4
 */
public class ConfigureMooringCommonsImport extends AbstractConfigureImport<MooringCommonsMooringImportConfiguration> {

    private static final long serialVersionUID = 1L;


    public ConfigureMooringCommonsImport() {
        super(MooringCommonsMooringImportConfiguration.class);
    }

    @Override
    protected MooringCommonsMooringImportConfiguration createModel() {
        return new MooringCommonsMooringImportConfiguration(getLocale());
    }

    public void setMooringFile(File file) {
        getModel().getMooringFile().setFile(file);
    }

    public void setMooringFileContentType(String contentType) {
        getModel().getMooringFile().setContentType(contentType);
    }

    public void setMooringFileFileName(String fileName) {
        getModel().getMooringFile().setFileName(fileName);
    }
}
