package fr.ifremer.echobase.ui.actions.exportCoser;

/*
 * #%L
 * EchoBase :: UI
 * %%
 * Copyright (C) 2011 - 2014 Ifremer, Codelutin, Chemit Tony
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.echobase.services.service.CoserApiService;
import fr.ifremer.echobase.ui.actions.EchoBaseActionSupport;

import javax.inject.Inject;
import java.util.Map;

/**
 * Created on 3/23/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.5
 */
public class GetZones extends EchoBaseActionSupport {

    private static final long serialVersionUID = 1L;

    @Inject
    private transient CoserApiService coserApiService;

    protected Map<String, String> zones;

    protected String facade;

    public void setFacade(String facade) {
        this.facade = facade;
    }

    public Map<String, String> getZones() {
        if (zones == null) {

            zones = coserApiService.getZonesForFacade(facade);
        }
        return zones;
    }
}
