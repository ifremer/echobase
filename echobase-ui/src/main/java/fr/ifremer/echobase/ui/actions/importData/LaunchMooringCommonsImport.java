/*
 * #%L
 * EchoBase :: UI
 * %%
 * Copyright (C) 2011 - 2012 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ifremer.echobase.ui.actions.importData;

import fr.ifremer.echobase.entities.EchoBaseUser;
import fr.ifremer.echobase.services.service.importdata.ImportDataResult;
import fr.ifremer.echobase.services.service.importdata.ImportDataService;
import fr.ifremer.echobase.services.service.importdata.ImportException;
import fr.ifremer.echobase.services.service.importdata.configurations.MooringCommonsMooringImportConfiguration;

/**
 * Launch a commons data import for mooring.
 *
 * @author Julien Ruchaud - ruchaud@codelutin.com
 * @since 4.0
 */
public class LaunchMooringCommonsImport extends AbstractLaunchImport<MooringCommonsMooringImportConfiguration, ImportDataService> {

    private static final long serialVersionUID = 1L;

    public LaunchMooringCommonsImport() {
        super(MooringCommonsMooringImportConfiguration.class, ImportDataService.class);
    }

    @Override
    protected ImportDataResult<MooringCommonsMooringImportConfiguration> doImport(ImportDataService service, MooringCommonsMooringImportConfiguration model, EchoBaseUser user) throws ImportException {
        return service.doImportMooringCommonsMooring(model, user);
    }
}
