package fr.ifremer.echobase.ui.actions.workingDb;

/*
 * #%L
 * EchoBase :: UI
 * %%
 * Copyright (C) 2011 - 2012 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.echobase.entities.EchoBaseUserEntityEnum;
import fr.ifremer.echobase.entities.ImportLog;
import fr.ifremer.echobase.entities.data.Voyage;
import fr.ifremer.echobase.entities.references.AreaOfOperation;
import fr.ifremer.echobase.entities.references.Mission;
import fr.ifremer.echobase.entities.references.Port;
import fr.ifremer.echobase.services.service.DbEditorService;
import fr.ifremer.echobase.services.service.DecoratorService;
import fr.ifremer.echobase.services.service.UserDbPersistenceService;
import fr.ifremer.echobase.ui.actions.EchoBaseActionSupport;
import org.nuiton.topia.persistence.metadata.TableMeta;

import javax.inject.Inject;
import java.util.Map;

/**
 * Obtain details of a given {@link ImportLog}.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.3
 */
public class GetVoyage extends EchoBaseActionSupport {

    private static final long serialVersionUID = 1L;

    protected String voyageId;

    public void setVoyageId(String voyageId) {
        this.voyageId = voyageId;
    }

    protected Map data;

    public Map<?, ?> getData() {
        return data;
    }

    @Override
    public String execute() throws Exception {

        TableMeta<EchoBaseUserEntityEnum> tableMeta =
                dbEditorService.getTableMeta(EchoBaseUserEntityEnum.Voyage);
        data = dbEditorService.getData(tableMeta, voyageId);

        // decorate foreign keys
        String missionId = (String) data.get(Voyage.PROPERTY_MISSION);
        Mission mission = userDbPersistenceService.getMission(missionId);
        decoratorService.decorateForeignKey(data, Voyage.PROPERTY_MISSION, mission, null);

        String areaOfOperationId = (String) data.get(Voyage.PROPERTY_AREA_OF_OPERATION);
        AreaOfOperation areaOfOperation = userDbPersistenceService.getAreaOfOperation(areaOfOperationId);
        decoratorService.decorateForeignKey(data, Voyage.PROPERTY_AREA_OF_OPERATION, areaOfOperation, null);

        String startPortId = (String) data.get(Voyage.PROPERTY_START_PORT);
        Port startPort = userDbPersistenceService.getPort(startPortId);
        decoratorService.decorateForeignKey(data, Voyage.PROPERTY_START_PORT, startPort, null);
        
        String endPortId = (String) data.get(Voyage.PROPERTY_END_PORT);
        Port endPort = userDbPersistenceService.getPort(endPortId);
        decoratorService.decorateForeignKey(data, Voyage.PROPERTY_END_PORT, endPort, null);
        
        return SUCCESS;
    }

    //------------------------------------------------------------------------//
    //-- Injected objects                                                     //
    //------------------------------------------------------------------------//

    @Inject
    protected transient UserDbPersistenceService userDbPersistenceService;
    @Inject
    protected transient DecoratorService decoratorService;
    @Inject
    protected transient DbEditorService dbEditorService;
}
