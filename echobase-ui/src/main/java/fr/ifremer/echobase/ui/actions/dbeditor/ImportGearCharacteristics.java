package fr.ifremer.echobase.ui.actions.dbeditor;

/*
 * #%L
 * EchoBase :: UI
 * %%
 * Copyright (C) 2011 - 2016 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.opensymphony.xwork2.interceptor.annotations.InputConfig;
import fr.ifremer.echobase.entities.EchoBaseUserEntityEnum;
import fr.ifremer.echobase.services.service.importdb.ImportService;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.service.csv.in.CsvImportResult;

import javax.inject.Inject;
import java.io.File;

/**
 * Created on 14/05/16.
 *
 * @author Tony Chemit - chemit@codelutin.com
 */
public class ImportGearCharacteristics extends AbstractLoadPage  {

    private static final long serialVersionUID = 1L;

    /** Logger. */
    private static final Log log = LogFactory.getLog(ImportGearCharacteristics.class);

    /** WTF ? */
    protected File importFile;

    /** WTF ? */
    protected String importFileContentType;

    /** WTF ? */
    protected String importFileFileName;

    /** CSV import result */
    private CsvImportResult<EchoBaseUserEntityEnum> result;

    private Exception error;

    public CsvImportResult<EchoBaseUserEntityEnum> getResult() {
        return result;
    }

    public File getImportFile() {
        return importFile;
    }

    public void setImportFile(File importFile) {
        this.importFile = importFile;
    }

    public String getImportFileContentType() {
        return importFileContentType;
    }

    public void setImportFileContentType(String importFileContentType) {
        this.importFileContentType = importFileContentType;
    }

    public String getImportFileFileName() {
        return importFileFileName;
    }

    public void setImportFileFileName(String importFileFileName) {
        this.importFileFileName = importFileFileName;
    }

    public Exception getError() {
        return error;
    }

    public String getErrorStack() {

        String errorStack = null;
        if (error != null) {

            errorStack = ExceptionUtils.getStackTrace(error);
        }
        return errorStack;
    }

    @Override
    public String input() throws Exception {
        load();
        return INPUT;
    }

    @Override
    @InputConfig(methodName = "input")
    public String execute() throws Exception {

        try {
            result = importService.importGearCharacteristicValues(
                    importFileFileName,
                    importFile,
                    getEchoBaseSession().getUser());

        } catch (Exception eee) {

            result = CsvImportResult.newResult(EchoBaseUserEntityEnum.GearCharacteristic,
                                               importFileFileName,
                                               false);
            error = eee;

            addFlashError(t("echobase.info.import.failed"));

            if (log.isErrorEnabled()) {
                log.error("Error while import ", eee);
            }
        }

        return SUCCESS;
    }

    //------------------------------------------------------------------------//
    //-- Injected objects                                                     //
    //------------------------------------------------------------------------//

    @Inject
    protected transient ImportService importService;

}
