/*
 * #%L
 * EchoBase :: UI
 * %%
 * Copyright (C) 2011 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ifremer.echobase.ui.actions.exportQuery;

import fr.ifremer.echobase.EchoBaseTechnicalException;
import fr.ifremer.echobase.entities.ExportQuery;
import fr.ifremer.echobase.services.service.exportquery.ExportQueryService;
import fr.ifremer.echobase.ui.actions.EchoBaseActionSupport;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.inject.Inject;

/**
 * Load export sql result fragment page.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 0.1
 */
public class ExportQueryResult extends EchoBaseActionSupport {

    private static final long serialVersionUID = 1L;

    /** Logger. */
    private static final Log log = LogFactory.getLog(ExportQueryResult.class);

    /**
     * Id of the query to call.
     *
     * @see ExportQuery#getTopiaId()
     * @since 1.4
     */
    protected String queryId;

    public void setQueryId(String queryId) {
        this.queryId = queryId;
    }

    public String getQueryId() {
        return queryId;
    }

    /** Default file name for export. */
    protected String fileName;

    public String getFileName() {
        return fileName;
    }

    /** names of columns from the executed sql request. */
    protected String[] columnNames;

    public String[] getColumnNames() {
        return columnNames;
    }

    @Override
    public String execute() throws Exception {

        // obtain columNames from the request

        String sql = exportQueryService.getSqlQuery(queryId);
        try {

            columnNames = exportQueryService.getColumnNames(sql);
        } catch (EchoBaseTechnicalException e) {
            //TODO add a real nice message
            addFlashError(e.getMessage());
            if (log.isErrorEnabled()) {
                log.error("coudl not execute query " + sql, e);
            }
        }

        // create default file name

        fileName = "export.csv";
        return SUCCESS;
    }

    //------------------------------------------------------------------------//
    //-- Injected objects                                                     //
    //------------------------------------------------------------------------//

    @Inject
    protected transient ExportQueryService exportQueryService;
}
