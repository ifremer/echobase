package fr.ifremer.echobase.ui.actions.workingDb;

/*
 * #%L
 * EchoBase :: UI
 * %%
 * Copyright (C) 2011 - 2015 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.echobase.entities.ImportFile;
import fr.ifremer.echobase.io.EchoBaseIOUtil;
import fr.ifremer.echobase.services.service.UserDbPersistenceService;
import fr.ifremer.echobase.ui.actions.EchoBaseActionSupport;

import javax.inject.Inject;
import java.io.InputStream;
import java.net.URLDecoder;
import java.sql.Blob;
import java.util.zip.GZIPInputStream;

/**
 * Created on 12/30/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.7.2
 */
public abstract class DownloadFileSupport extends EchoBaseActionSupport {

    private static final long serialVersionUID = 1L;

    /** id of import file to download. */
    protected String importFileId;

    protected String filename;

    public void setImportFileId(String importFileId) {
        this.importFileId = importFileId;
    }

    public String getImportFileId() {
        return importFileId;
    }

    public String getFilename() {
        return filename;
    }

    /** Input stream of the file to download. */
    protected transient InputStream inputStream;

    public InputStream getInputStream() {
        return inputStream;
    }

    /** Length of the file to download. */
    protected long contentLength;

    public long getContentLength() {
        return contentLength;
    }

    /** Content type of the file to download. */
    protected String contentType;

    public String getContentType() {
        return contentType;
    }

    protected abstract Blob getFile(ImportFile importFile);
    protected abstract String getFilename(ImportFile importFile);

    @Override
    public String execute() throws Exception {

        //FIXME Find out why we need to decode id?
        String id = URLDecoder.decode(importFileId, "UTF-8");
        ImportFile importFile = userDbPersistenceService.getImportFile(id);

        filename = getFilename(importFile);

        contentType = "text/csv";
        Blob blob = getFile(importFile);
        contentLength = EchoBaseIOUtil.blobLength(blob);

        inputStream = new GZIPInputStream(blob.getBinaryStream(), 65535);

        return SUCCESS;

    }

    //------------------------------------------------------------------------//
    //-- Injected objects                                                     //
    //------------------------------------------------------------------------//

    @Inject
    protected transient UserDbPersistenceService userDbPersistenceService;

}