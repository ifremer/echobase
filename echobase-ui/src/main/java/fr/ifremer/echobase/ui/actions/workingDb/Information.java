package fr.ifremer.echobase.ui.actions.workingDb;

/*
 * #%L
 * EchoBase :: UI
 * %%
 * Copyright (C) 2011 - 2012 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.echobase.config.EchoBaseConfiguration;
import fr.ifremer.echobase.entities.EchoBaseUserPersistenceContext;
import fr.ifremer.echobase.persistence.JdbcConfiguration;
import fr.ifremer.echobase.ui.actions.EchoBaseActionSupport;

import javax.inject.Inject;

/**
 * Display a page to show connection details.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 0.1
 */
public class Information extends EchoBaseActionSupport {

    private static final long serialVersionUID = 1L;

    protected JdbcConfiguration dbConfiguration;

    protected String pilotVersion;

    protected boolean spatialSupport;
    protected boolean spatialStructureFound;

    protected boolean canAddSpatial;

    public JdbcConfiguration getDbConfiguration() {
        return dbConfiguration;
    }

    public String getPilotVersion() {
        return pilotVersion;
    }

    public boolean isSpatialStructureFound() {
        return spatialStructureFound;
    }

    public boolean isSpatialSupport() {
        return spatialSupport;
    }

    public boolean isCanAddSpatial() {
        return canAddSpatial;
    }

    @Override
    public String execute() throws Exception {

        EchoBaseConfiguration configuration =
                getEchoBaseApplicationContext().getConfiguration();

        dbConfiguration = getEchoBaseSession().getWorkingDbConfiguration();

        dbConfiguration = JdbcConfiguration.newConfig(
                dbConfiguration.getDriverType(),
                dbConfiguration.getUrl().replaceAll("\\\\", "/"),
                dbConfiguration.getLogin(),
                dbConfiguration.getPassword());

        pilotVersion = dbConfiguration.getDriverType().getPilotVersion(configuration);

        spatialSupport = db.isSpatialSupport();

        spatialStructureFound = db.isSpatialStructureFound();

        // can add spatial struture ?
        canAddSpatial = spatialSupport && !spatialStructureFound;

        return SUCCESS;
    }

    //------------------------------------------------------------------------//
    //-- Injected objects                                                     //
    //------------------------------------------------------------------------//

    @Inject
    protected transient EchoBaseUserPersistenceContext db;
}
