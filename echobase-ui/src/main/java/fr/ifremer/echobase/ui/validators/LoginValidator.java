/*
 * #%L
 * EchoBase :: UI
 * %%
 * Copyright (C) 2011 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ifremer.echobase.ui.validators;

import com.opensymphony.xwork2.validator.ValidationException;
import fr.ifremer.echobase.entities.EchoBaseUser;
import fr.ifremer.echobase.services.service.UserService;
import fr.ifremer.echobase.ui.actions.EchoBaseActionSupport;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Check user login.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 0.1
 */
public class LoginValidator extends EchoBaseFieldValidatorSupport {

    private static final Log log = LogFactory.getLog(LoginValidator.class);

    @Override
    public void validateWhenNotSkip(Object object) throws ValidationException {

        EchoBaseActionSupport action = (EchoBaseActionSupport) object;

        UserService userService =
                action.getServiceContext().newService(UserService.class);

        String login = (String) getFieldValue("email", object);
        String password = (String) getFieldValue("password", object);

        if (log.isInfoEnabled()) {
            log.info("try to log for user " + login);
        }

        try {
            // check in db that user is ok
            EchoBaseUser user = userService.getUserByEmail(login);

            if (user == null) {

                // user not found
                addFieldError("email", translate("echobase.error.login.unknown"));
                return;
            }

            boolean passwordOk = userService.checkPassword(user, password);

            if (!passwordOk) {
                addFieldError("password", translate("echobase.error.bad.password"));
            }
        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Could not validate login", e);
            }
            throw new ValidationException("Could not validate login : " + e.getMessage());
        }
    }

    @Override
    public String getValidatorType() {
        return "login";
    }
}
