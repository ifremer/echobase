/*
 * #%L
 * EchoBase :: UI
 * %%
 * Copyright (C) 2011 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ifremer.echobase.ui.actions.dbeditor;

import com.google.common.collect.Maps;
import fr.ifremer.echobase.entities.EchoBaseUserEntityEnum;
import fr.ifremer.echobase.services.service.DbEditorService;
import fr.ifremer.echobase.ui.actions.EchoBaseActionSupport;
import org.nuiton.topia.persistence.metadata.ColumnMeta;
import org.nuiton.topia.persistence.metadata.TableMeta;

import javax.inject.Inject;
import java.util.List;
import java.util.Map;

/**
 * To obtain the data for the given entity.
 *
 * @author Sylvain Letellier
 * @since 0.1
 */
public class GetEntity extends EchoBaseActionSupport {

    private static final long serialVersionUID = 1L;

    /** Type of entity to load. */
    protected EchoBaseUserEntityEnum entityType;

    public void setEntityType(EchoBaseUserEntityEnum entityType) {
        this.entityType = entityType;
    }

    /** Id of entity to load. */
    protected String id;

    public void setId(String id) {
        this.id = id;
    }

    /** Datas of the given table. */
    protected Map<?, ?> datas;

    /** Universe of columns metat datas of the selected entity type. */
    protected Map<String, ColumnMeta> metas;

    public Map<?, ?> getDatas() {
        return datas;
    }

    public Map<String, ColumnMeta> getMetas() {
        return metas;
    }

    @Override
    public String execute() throws Exception {

        TableMeta<EchoBaseUserEntityEnum> table =
                dbEditorService.getTableMeta(entityType);

        datas = dbEditorService.getData(table, id);

        Object length = datas.remove("length");
        if (length != null) {

            // replace it by length_ (ovtherwise javascript won't work)
            ((Map) datas).put("length_", length);
        }
        List<ColumnMeta> columnMetas = table.getColumns();
        metas = Maps.newHashMap();
        for (ColumnMeta columnMeta : columnMetas) {
            metas.put(columnMeta.getName(), columnMeta);
        }
        return SUCCESS;
    }

    //------------------------------------------------------------------------//
    //-- Injected objects                                                     //
    //------------------------------------------------------------------------//
    @Inject
    protected transient DbEditorService dbEditorService;
}
