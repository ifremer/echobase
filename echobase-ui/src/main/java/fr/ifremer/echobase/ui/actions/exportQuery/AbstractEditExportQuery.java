package fr.ifremer.echobase.ui.actions.exportQuery;

/*
 * #%L
 * EchoBase :: UI
 * %%
 * Copyright (C) 2011 - 2013 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.echobase.entities.EchoBaseUser;
import fr.ifremer.echobase.entities.ExportQuery;
import fr.ifremer.echobase.services.service.exportquery.ExportQueryService;
import fr.ifremer.echobase.ui.actions.EchoBaseActionSupport;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.inject.Inject;
import java.util.Map;

/**
 * To save a export query.
 *
 * Created on 11/9/13.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.5
 */
public abstract class AbstractEditExportQuery extends EchoBaseActionSupport {

    /** Logger. */
    private static final Log log =
            LogFactory.getLog(AbstractEditExportQuery.class);

    private static final long serialVersionUID = 1L;

    /** Selected query loaded from database if his id is not empty. */
    protected ExportQuery query;

    public ExportQuery getQuery() {
        if (query == null) {
            query = exportQueryService.newExportQuery();
        }
        return query;
    }

    protected boolean canUpdateQuery;

    protected boolean queryExist;

    public boolean isCanUpdateQuery() {
        return canUpdateQuery;
    }

    public boolean isQueryExists() {
        return StringUtils.isNotEmpty(getQuery().getTopiaId());
    }

    public boolean isNewQuery() {
        return !isQueryExists();
    }

    /** All available queries from database. */
    protected Map<String, String> queries;

    public Map<String, String> getQueries() {
        return queries;
    }

    @Override
    public String input() throws Exception {

        // come back here when validation failed
        queries = exportQueryService.loadSortAndDecorate(ExportQuery.class);

        EchoBaseUser echoBaseUser = getEchoBaseSession().getUser();

        canUpdateQuery = echoBaseUser.isAdmin() ||
                         echoBaseUser.getEmail().equals(
                                 query.getLastModifiedUser());

        if (isNewQuery()) {

            // new query in progress
            addFlashMessage(t("echobase.info.new.sqlQuery.inprogress"));
        }
        return INPUT;
    }

    @Override
    public String execute() throws Exception {

        query = exportQueryService.createOrUpdate(
                getQuery(), getEchoBaseSession().getUser());

        return SUCCESS;
    }

    @Override
    public void validate() {
        super.validate();
        if (!hasFieldErrors()) {
            if (!exportQueryService.isQueryNameValid(getQuery())) {
                addFieldError("query.name",
                              t("echobase.error.query.invalid.name"));
            } else if (!exportQueryService.isQueryNameAvailable(getQuery())) {
                addFieldError("query.name",
                              t("echobase.error.query.name.already.exists"));
            } else {
                try {
                    exportQueryService.testSql(getQuery().getSqlQuery());
                } catch (Exception e) {
                    Throwable cause = e.getCause();
                    if (log.isWarnEnabled()) {
                        log.warn("Invalid sql ", cause);
                    }
                    addFieldError("query.sqlQuery",
                                  t("echobase.error.invalid.sql", cause.getMessage()));
                }
            }
        }
    }

    //------------------------------------------------------------------------//
    //-- Injected objects                                                     //
    //------------------------------------------------------------------------//

    @Inject
    protected transient ExportQueryService exportQueryService;
}
