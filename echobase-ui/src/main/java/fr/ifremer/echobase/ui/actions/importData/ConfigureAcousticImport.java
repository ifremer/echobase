/*
 * #%L
 * EchoBase :: UI
 * %%
 * Copyright (C) 2011 - 2012 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ifremer.echobase.ui.actions.importData;

import fr.ifremer.echobase.entities.data.Voyage;
import fr.ifremer.echobase.services.service.importdata.configurations.VoyageAcousticsImportConfiguration;
import fr.ifremer.echobase.services.service.importdata.CellPositionReference;

import java.io.File;
import java.util.Map;

/**
 * Configure a accoustic data import.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 0.3
 */
public class ConfigureAcousticImport extends AbstractConfigureImport<VoyageAcousticsImportConfiguration> {

    private static final long serialVersionUID = 1L;

    /** Universe of existing voyages. */
    protected Map<String, String> voyages;

    protected Map<String, String> cellPositionReferences;

    public ConfigureAcousticImport() {
        super(VoyageAcousticsImportConfiguration.class);
    }

    @Override
    protected VoyageAcousticsImportConfiguration createModel() {
        return new VoyageAcousticsImportConfiguration(getLocale());
    }

    @Override
    protected void prepareInputAction(VoyageAcousticsImportConfiguration model) {
        voyages = userDbPersistenceService.loadSortAndDecorate(Voyage.class);
        cellPositionReferences = decoratorService.decorateEnums(CellPositionReference.values());

        if (model.getCellPositionReference() == null) {
            model.setCellPositionReference(CellPositionReference.START);
        }
    }

    public Map<String, String> getVoyages() {
        return voyages;
    }

    public Map<String, String> getCellPositionReferences() {
        return cellPositionReferences;
    }

    public void setMoviesFile(File file) {
        getModel().getMoviesFile().setFile(file);
    }

    public void setMoviesFileContentType(String contentType) {
        getModel().getMoviesFile().setContentType(contentType);
    }

    public void setMoviesFileFileName(String fileName) {
        getModel().getMoviesFile().setFileName(fileName);
    }

    public void setCalibrationsFile(File file) {
        getModel().getCalibrationsFile().setFile(file);
    }

    public void setCalibrationsFileContentType(String contentType) {
        getModel().getCalibrationsFile().setContentType(contentType);
    }

    public void setCalibrationsFileFileName(String fileName) {
        getModel().getCalibrationsFile().setFileName(fileName);
    }
}
