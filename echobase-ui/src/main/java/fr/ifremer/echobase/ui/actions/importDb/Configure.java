/*
 * #%L
 * EchoBase :: UI
 * %%
 * Copyright (C) 2011 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ifremer.echobase.ui.actions.importDb;

import com.opensymphony.xwork2.interceptor.annotations.InputConfig;
import fr.ifremer.echobase.io.EchoBaseIOUtil;
import fr.ifremer.echobase.io.InputFile;
import fr.ifremer.echobase.services.service.importdb.ImportDbConfiguration;
import fr.ifremer.echobase.services.service.importdb.ImportDbMode;
import fr.ifremer.echobase.ui.actions.AbstractConfigureAction;
import org.apache.commons.io.FileUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.util.FileUtil;

import java.io.File;
import java.io.IOException;
import java.util.Map;

/**
 * Configure a db import.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 0.2
 */
public class Configure extends AbstractConfigureAction<ImportDbConfiguration> {

    private static final long serialVersionUID = 1L;

    /** Logger. */
    private static final Log log = LogFactory.getLog(Configure.class);

    /** Universe of possible import modes. */
    protected Map<String, String> modes;

    public Map<String, String> getModes() {
        return modes;
    }

    public long getUploadFileMaxLength() {
        return getEchoBaseApplicationContext().getUploadFileMaxLength();
    }

    public Configure() {
        super(ImportDbConfiguration.class);
    }

    @Override
    protected ImportDbConfiguration createModel() {
        return new ImportDbConfiguration(getLocale());
    }

    @Override
    protected void prepareInputAction(ImportDbConfiguration model) {
        modes = decoratorService.decorateEnums(ImportDbMode.values());

        if (model.getImportDbMode() == null) {

            model.setImportDbMode(ImportDbMode.REFERENTIAL);
        }
    }

    @Override
    protected void prepareExecuteAction(ImportDbConfiguration model) throws IOException {

        File tempDirectory = FileUtils.getTempDirectory();
        File dataDirectory = new File(
                tempDirectory,
                "echobase-importDb-" + System.currentTimeMillis());
        FileUtil.createDirectoryIfNecessary(dataDirectory);
        model.setWorkingDirectory(dataDirectory);
        if (log.isInfoEnabled()) {
            log.info("Temporary directory to use : " + dataDirectory);
        }

        InputFile input = model.getInput();
        EchoBaseIOUtil.copyFile(input, dataDirectory);
    }

    @InputConfig(methodName = "input")
    public String configure() throws Exception {
        return execute();
    }

    public void setInputFileName(String fileName) {
        getModel().getInput().setFileName(fileName);
    }

    public void setInput(File file) {
        getModel().getInput().setFile(file);
    }

    public void setInputContentType(String contentType) {
        getModel().getInput().setContentType(contentType);
    }
}
