package fr.ifremer.echobase.ui.actions.workingDb;

/*
 * #%L
 * EchoBase :: UI
 * %%
 * Copyright (C) 2011 - 2013 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.opensymphony.xwork2.interceptor.annotations.InputConfig;
import fr.ifremer.echobase.entities.DriverType;
import fr.ifremer.echobase.persistence.EchoBaseEntityHelper;
import fr.ifremer.echobase.persistence.JdbcConfiguration;

import java.sql.SQLException;

/**
 * To create a new fresh postgres database.
 *
 * Created on 11/3/13.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.4
 */
public class CreatePostgresDb extends AbstractWorkingDbAction {

    private static final long serialVersionUID = 1L;

    protected String login;

    protected String password;

    protected JdbcConfiguration jdbcConf;

    protected JdbcConfiguration metaJdbcConf;

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @InputConfig(methodName = INPUT)
    @Override
    public String execute() throws Exception {

        EchoBaseEntityHelper.checkJdbcConnection(metaJdbcConf);

        try {

            // connect to meta database
            getEchoBaseSession().initUserDb(metaJdbcConf, false);

            // create database
            workingDbConfigurationService.createDb(jdbcConf);

            // connect to new database
            getEchoBaseSession().initUserDb(jdbcConf, true);

            addFlashMessage(t("echobase.info.workingDb.created", conf.getUrl()));

        } catch (Exception e) {

            try {
                //close working db if something is wrong.
                getEchoBaseSession().releaseUserDb();
            } finally {
                metaJdbcConf = null;
                jdbcConf = null;
            }

            throw e;
        }
        return SUCCESS;
    }

    @Override
    public void validate() {

        if (!DriverType.POSTGRESQL.equals(getConf().getDriverType())) {
            addFieldError(
                    "conf.driverType",
                    t("echobase.error.workingDbConfiguration.createOnlyOnPostgresql"));
        } else {
            jdbcConf = JdbcConfiguration.newConfig(getConf().getDriverType(), getConf().getUrl(), login, password);

            // gets the meta db configuration
            metaJdbcConf = EchoBaseEntityHelper.newWorkingDbJdbcConfiguration(jdbcConf);

            // check connection is ok
            try {
                EchoBaseEntityHelper.checkJdbcConnection(metaJdbcConf);
            } catch (SQLException e) {
                jdbcConf = null;
                addFieldError(
                        "login",
                        t("echobase.error.workingDbConfiguration.couldNotConnect",
                          e.getMessage()));
                addFieldError(
                        "password",
                        t("echobase.error.workingDbConfiguration.couldNotConnect",
                          e.getMessage()));
            }
        }

    }

}
