/*
 * #%L
 * EchoBase :: UI
 * %%
 * Copyright (C) 2011 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ifremer.echobase.ui.actions;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.struts2.json.JSONException;
import org.apache.struts2.json.JSONUtil;
import org.nuiton.topia.persistence.pager.TopiaPagerBean;
import org.nuiton.topia.persistence.pager.TopiaPagerBeanBuilder;

import java.util.Collection;
import java.util.Map;

/**
 * Abstract JSON action with pagination support.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 0.1
 */
public abstract class AbstractJSONPaginedAction extends EchoBaseActionSupport {

    private static final long serialVersionUID = 1L;

    protected TopiaPagerBean pager = new TopiaPagerBean();

    // sorting order - asc or desc
    protected String sord;

    // get index row - i.e. user click to sort.
    protected String sidx;

    protected String filters;

    protected String searchField;

    protected String searchString;

    protected String searchOper;

    public final Integer getRows() {
        return pager.getPageSize();
    }

    public final Integer getPage() {
        return pager.getPageIndex();
    }

    public final Long getTotal() {
        return pager.getPagesNumber();
    }

    public final Long getRecords() {
        return pager.getRecords();
    }

    public void setRows(Integer rows) {
        pager.setPageSize(rows);
    }

    public void setPage(Integer page) {
        pager.setPageIndex(page);
    }

    public String getSord() {
        return sord;
    }

    public void setSord(String sord) {
        this.sord = sord;
    }

    public String getSidx() {
        return sidx;
    }

    public void setSidx(String sidx) {
        this.sidx = sidx;
    }

    public String getFilters() {
        return filters;
    }

    public void setFilters(String filters) {
        this.filters = filters;
    }

    public void setSearchField(String searchField) {
        this.searchField = searchField;
    }

    public void setSearchString(String searchString) {
        this.searchString = searchString;
    }

    public void setSearchOper(String searchOper) {
        this.searchOper = searchOper;
    }

    protected String getSortColumn() {
        String result = null;
        if (useSort()) {
            result = sidx.equals("ID")?"id":sidx;
        }
        return result;
    }

    protected Boolean isSortAscendant() {
        Boolean result = null;
        if (useSort()) {
            result = "asc".equals(sord);
        }
        return result;
    }

    protected boolean useSort() {
        return StringUtils.isNotEmpty(sidx);
    }

    protected void initFilter() throws JSONException {

        TopiaPagerBeanBuilder builder = new TopiaPagerBeanBuilder(pager);

        if (useSort()) {
            builder.setSortAscendant(isSortAscendant())
                    .setSortcolumn(getSortColumn());
        }

        if (StringUtils.isNotEmpty(filters)) {
            Map<String, Object> filterObject =
                    (Map<String, Object>) JSONUtil.deserialize(filters);

            String groupOp = (String) filterObject.get("groupOp");
            Collection<Map<String, String>> rules =
                    (Collection<Map<String, String>>) filterObject.get("rules");

            if (CollectionUtils.isNotEmpty(rules)) {
                for (Map<String, String> rule : rules) {
                    //TODO tchemit-2012-10-17 Fix this bug
                    if ("ID".equals(rule.get("field"))) {
                        rule.put("field", "id");
                    }
                }
            }
            pager = builder.
                    setFilterOperationGroup(groupOp).
                    addRules(rules).
                    toBean();
        } else {

            // could be a single search

            if (StringUtils.isNotEmpty(searchField)) {
                pager = builder.
                        setFilterOperationGroup("AND").
                        addRule(searchOper, searchField, searchString).
                        toBean();
            }
        }
    }
}
