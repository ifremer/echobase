package fr.ifremer.echobase.ui.actions.user;

/*
 * #%L
 * EchoBase :: UI
 * %%
 * Copyright (C) 2011 - 2012 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.opensymphony.xwork2.ActionContext;
import fr.ifremer.echobase.ui.EchoBaseSession;
import fr.ifremer.echobase.ui.actions.EchoBaseActionSupport;

/**
 * Log out from EchoBase.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.1
 */
public class Logout extends EchoBaseActionSupport {

    private static final long serialVersionUID = 1L;

    @Override
    public String execute() throws Exception {

        EchoBaseSession userSession = getEchoBaseSession();

        // remove echoBaseSession from application session
        getEchoBaseApplicationContext().destroyEchoBaseSession(userSession);

        // clean it from ActionContext (we never know...)
        EchoBaseSession.removeEchoBaseSession(ActionContext.getContext());

        return SUCCESS;
    }
}
