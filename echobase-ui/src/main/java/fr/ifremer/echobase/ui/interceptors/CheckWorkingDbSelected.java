package fr.ifremer.echobase.ui.interceptors;

/*
 * #%L
 * EchoBase :: UI
 * %%
 * Copyright (C) 2011 - 2012 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.opensymphony.xwork2.ActionInvocation;
import fr.ifremer.echobase.ui.EchoBaseSession;
import fr.ifremer.echobase.ui.actions.EchoBaseActionSupport;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import static org.nuiton.i18n.I18n.l;

/**
 * Checks that a working db was selected, otherwise redirect to home.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.3
 */
public class CheckWorkingDbSelected extends AbstractCheckInterceptor {

    private static final long serialVersionUID = 1L;

    /** Logger. */
    private static final Log log =
            LogFactory.getLog(CheckLogguedInterceptor.class);

    @Override
    protected boolean doCheck(ActionInvocation invocation) {
        EchoBaseActionSupport action = (EchoBaseActionSupport)
                invocation.getAction();

        EchoBaseSession echoBaseSession = EchoBaseSession.getEchoBaseSession(
                invocation.getInvocationContext());
        boolean dbSelected = echoBaseSession.isWorkingDbSelected();
        if (!dbSelected) {
            action.addFlashMessage(l(action.getLocale(),
                                      "echobase.info.no.workingDb.selected"));
            if (log.isDebugEnabled()) {
                log.debug("No working db selected, will redirect to home");
            }
        }
        return dbSelected;
    }
}
