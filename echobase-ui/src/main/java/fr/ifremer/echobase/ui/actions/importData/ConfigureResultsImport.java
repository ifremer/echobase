/*
 * #%L
 * EchoBase :: UI
 * %%
 * Copyright (C) 2011 - 2012 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ifremer.echobase.ui.actions.importData;

import com.opensymphony.xwork2.interceptor.annotations.InputConfig;
import fr.ifremer.echobase.entities.ImportType;
import fr.ifremer.echobase.entities.data.Voyage;
import fr.ifremer.echobase.entities.references.Vessel;
import fr.ifremer.echobase.services.service.importdata.configurations.VoyageResultsImportConfiguration;

import java.io.File;
import java.util.Map;

/**
 * Configure a "results" import.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 0.3
 */
public class ConfigureResultsImport extends AbstractConfigureImport<VoyageResultsImportConfiguration> {

    private static final long serialVersionUID = 1L;

    /** Universe of existing voyages. */
    protected Map<String, String> voyages;

    /** Universe of existing vessels. */
    protected Map<String, String> vessels;

    /** Universe of possible import modes. */
    protected Map<String, String> importTypes;

    protected String resultLabel;

    public ConfigureResultsImport() {
        super(VoyageResultsImportConfiguration.class);
    }

    @Override
    protected VoyageResultsImportConfiguration createModel() {
        return new VoyageResultsImportConfiguration(getLocale());
    }

    @Override
    protected void prepareInputAction(VoyageResultsImportConfiguration model) {
        voyages = userDbPersistenceService.loadSortAndDecorate(Voyage.class);
        vessels = userDbPersistenceService.loadSortAndDecorate(Vessel.class);
        importTypes = decoratorService.decorateEnums(ImportType.getResultImportType());

        if (model.getImportType() == null) {

            model.setImportType(ImportType.RESULT_VOYAGE);
        }
    }

    @InputConfig(methodName = "input")
    public String modeVoyage() throws Exception {

        return execute();
    }

    @InputConfig(methodName = "input")
    public String modeEsdu() throws Exception {

        return execute();
    }

    @InputConfig(methodName = "input")
    public String modeRegion() throws Exception {

        return execute();
    }

    @InputConfig(methodName = "input")
    public String modeMapFish() throws Exception {

        return execute();
    }

    @InputConfig(methodName = "input")
    public String modeMapOther() throws Exception {

        return execute();
    }

    public Map<String, String> getVoyages() {
        return voyages;
    }

    public Map<String, String> getVessels() {
        return vessels;
    }

    public Map<String, String> getImportTypes() {
        return importTypes;
    }


    public String getResultLabel() {
        return resultLabel;
    }

    public void setResultLabel(String resultLabel) {
        this.resultLabel = resultLabel;
    }

    public void setRegionsFile(File file) {
        getModel().getRegionsFile().setFile(file);
    }

    public void setRegionsFileContentType(String contentType) {
        getModel().getRegionsFile().setContentType(contentType);
    }

    public void setRegionsFileFileName(String fileName) {
        getModel().getRegionsFile().setFileName(fileName);
    }

    public void setRegionAssociationFile(File file) {
        getModel().getRegionAssociationFile().setFile(file);
    }

    public void setRegionAssociationFileContentType(String contentType) {
        getModel().getRegionAssociationFile().setContentType(contentType);
    }

    public void setRegionAssociationFileFileName(String fileName) {
        getModel().getRegionAssociationFile().setFileName(fileName);
    }

    public void setRegionResultFile(File file) {
        getModel().getRegionResultFile().setFile(file);
    }

    public void setRegionResultFileContentType(String contentType) {
        getModel().getRegionResultFile().setContentType(contentType);
    }

    public void setRegionResultFileFileName(String fileName) {
        getModel().getRegionResultFile().setFileName(fileName);
    }

    public void setMapsFile(File file) {
        getModel().getMapsFile().setFile(file);
    }

    public void setMapsFileContentType(String contentType) {
        getModel().getMapsFile().setContentType(contentType);
    }

    public void setMapsFileFileName(String fileName) {
        getModel().getMapsFile().setFileName(fileName);
    }

    public void setLengthAgeKeyFile(File file) {
        getModel().getLengthAgeKeyFile().setFile(file);
    }

    public void setLengthAgeKeyFileContentType(String contentType) {
        getModel().getLengthAgeKeyFile().setContentType(contentType);
    }

    public void setLengthAgeKeyFileFileName(String fileName) {
        getModel().getLengthAgeKeyFile().setFileName(fileName);
    }

    public void setLengthWeightKeyFile(File file) {
        getModel().getLengthWeightKeyFile().setFile(file);
    }

    public void setLengthWeightKeyFileContentType(String contentType) {
        getModel().getLengthWeightKeyFile().setContentType(contentType);
    }

    public void setLengthWeightKeyFileFileName(String fileName) {
        getModel().getLengthWeightKeyFile().setFileName(fileName);
    }

    public void setEchotypeFile(File file) {
        getModel().getEchotypeFile().setFile(file);
    }

    public void setEchotypeFileContentType(String contentType) {
        getModel().getEchotypeFile().setContentType(contentType);
    }

    public void setEchotypeFileFileName(String fileName) {
        getModel().getEchotypeFile().setFileName(fileName);
    }

    public void setEsduByEchotypeFile(File file) {
        getModel().getEsduByEchotypeFile().setFile(file);
    }

    public void setEsduByEchotypeFileContentType(String contentType) {
        getModel().getEsduByEchotypeFile().setContentType(contentType);
    }

    public void setEsduByEchotypeFileFileName(String fileName) {
        getModel().getEsduByEchotypeFile().setFileName(fileName);
    }

    public void setEsduByEchotypeAndSpeciesCategoryFile(File file) {
        getModel().getEsduByEchotypeAndSpeciesCategoryFile().setFile(file);
    }

    public void setEsduByEchotypeAndSpeciesCategoryFileContentType(String contentType) {
        getModel().getEsduByEchotypeAndSpeciesCategoryFile().setContentType(contentType);
    }

    public void setEsduByEchotypeAndSpeciesCategoryFileFileName(String fileName) {
        getModel().getEsduByEchotypeAndSpeciesCategoryFile().setFileName(fileName);
    }

    public void setEsduByEchotypeAndSpeciesCategoryAndLengthFile(File file) {
        getModel().getEsduByEchotypeAndSpeciesCategoryAndLengthFile().setFile(file);
    }

    public void setEsduByEchotypeAndSpeciesCategoryAndLengthFileContentType(String contentType) {
        getModel().getEsduByEchotypeAndSpeciesCategoryAndLengthFile().setContentType(contentType);
    }

    public void setEsduByEchotypeAndSpeciesCategoryAndLengthFileFileName(String fileName) {
        getModel().getEsduByEchotypeAndSpeciesCategoryAndLengthFile().setFileName(fileName);
    }

    public void setEsduBySpeciesAndAgeCategoryFile(File file) {
        getModel().getEsduBySpeciesAndAgeCategoryFile().setFile(file);
    }

    public void setEsduBySpeciesAndAgeCategoryFileContentType(String contentType) {
        getModel().getEsduBySpeciesAndAgeCategoryFile().setContentType(contentType);
    }

    public void setEsduBySpeciesAndAgeCategoryFileFileName(String fileName) {
        getModel().getEsduBySpeciesAndAgeCategoryFile().setFileName(fileName);
    }
}
