/*
 * #%L
 * EchoBase :: UI
 * %%
 * Copyright (C) 2011 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ifremer.echobase.ui.actions;

import com.google.common.base.Preconditions;
import fr.ifremer.echobase.services.EchoBaseService;
import fr.ifremer.echobase.services.EchoBaseServiceContext;
import fr.ifremer.echobase.ui.EchoBaseApplicationContext;
import fr.ifremer.echobase.ui.EchoBaseSession;
import org.apache.commons.lang3.StringUtils;
import org.nuiton.web.struts2.BaseAction;

import javax.inject.Inject;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * EchoBase action support.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 0.1
 */
public class EchoBaseActionSupport extends BaseAction {

    private static final long serialVersionUID = 1L;

    private static final SimpleDateFormat dateFormat =
            new SimpleDateFormat("dd/MM/yyyy");

    private static final SimpleDateFormat monthFormat =
            new SimpleDateFormat("mm-yyyy");

    protected <E extends EchoBaseService> E newService(Class<E> serviceClass) {
        return getServiceContext().newService(serviceClass);
    }

    public String formatDate(Date date) {
        return dateFormat.format(date);
    }

    public String formatMonth(Date date) {
        return monthFormat.format(date);
    }

    public void addFlashMessage(String message) {
        getEchoBaseSession().addMessage(EchoBaseSession.SESSION_TOKEN_MESSAGES, message);
    }

    public void addFlashError(String message) {
        getEchoBaseSession().addMessage(EchoBaseSession.SESSION_TOKEN_ERRORS, message);
    }

    public void addFlashWarning(String message) {
        getEchoBaseSession().addMessage(EchoBaseSession.SESSION_TOKEN_WARNINGS, message);
    }

    public String getDocumentation(String page) {
        return getDocumentation(page, null);
    }

    public String getDocumentation(String page, String anchor) {
        Locale locale = getLocale();
        String result = getEchoBaseApplicationContext().getConfiguration().getDocumentationUrl(locale);
        result += page;
        if (StringUtils.isNotBlank(anchor)) {
            result += "#" + anchor;
        }
        return result;
    }

    //------------------------------------------------------------------------//
    //-- Injected objects                                                     //
    //------------------------------------------------------------------------//

    /** EchoBase Application context. */
    @Inject
    private transient EchoBaseApplicationContext applicationContext;

    /** EchoBase User session. */
    @Inject
    private transient EchoBaseSession echoBaseSession;

    /**
     * Service context used to access and create services.
     *
     * @since 1.0
     */
    @Inject
    private transient EchoBaseServiceContext serviceContext;

    public EchoBaseApplicationContext getEchoBaseApplicationContext() {
        Preconditions.checkNotNull("No applicationContext in your action " + this);
        return applicationContext;
    }

    public EchoBaseSession getEchoBaseSession() {
        Preconditions.checkNotNull("No echoBaseSession in your action " + this);
        return echoBaseSession;
    }

    public EchoBaseServiceContext getServiceContext() {
        Preconditions.checkNotNull("No serviceContext in your action " + this);
        return serviceContext;
    }

}
