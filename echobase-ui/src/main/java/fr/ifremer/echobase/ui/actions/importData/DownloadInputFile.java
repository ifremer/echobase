package fr.ifremer.echobase.ui.actions.importData;

/*
 * #%L
 * EchoBase :: UI
 * %%
 * Copyright (C) 2011 - 2016 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.echobase.io.EchoBaseIOUtil;
import fr.ifremer.echobase.ui.actions.EchoBaseActionSupport;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.zip.GZIPInputStream;

/**
 * Created on 08/05/16.
 *
 * @author Tony Chemit - chemit@codelutin.com
 */
public class DownloadInputFile extends EchoBaseActionSupport {

    private static final long serialVersionUID = 1L;

    /** Prefix of input file to download. */
    protected String fileSuffix;

    /** Input stream of the file to download. */
    protected transient InputStream inputStream;

    /** Length of the file to download. */
    protected long contentLength;

    /** Content type of the file to download. */
    protected String contentType;

    private String filename;

    public void setFileSuffix(String fileSuffix) {
        this.fileSuffix = fileSuffix;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public String getFilename() {
        return filename;
    }

    public InputStream getInputStream() {
        return inputStream;
    }

    public long getContentLength() {
        return contentLength;
    }

    public String getContentType() {
        return contentType;
    }

    @Override
    public String execute() throws Exception {

        File dataDirectory = getEchoBaseApplicationContext().getConfiguration().getDataDirectory();
        File file = new File(dataDirectory, fileSuffix);

        contentType = "text/csv";
        contentLength = EchoBaseIOUtil.fileLength(file);

        inputStream = new GZIPInputStream(new FileInputStream(file), 65535);

        return SUCCESS;

    }

}
