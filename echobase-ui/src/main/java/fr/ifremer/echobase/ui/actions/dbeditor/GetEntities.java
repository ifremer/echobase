/*
 * #%L
 * EchoBase :: UI
 * %%
 * Copyright (C) 2011 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ifremer.echobase.ui.actions.dbeditor;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Maps;
import com.google.common.collect.Multimap;
import fr.ifremer.echobase.entities.EchoBaseUserEntityEnum;
import fr.ifremer.echobase.entities.ImportLog;
import fr.ifremer.echobase.entities.ImportType;
import fr.ifremer.echobase.services.service.DbEditorService;
import fr.ifremer.echobase.services.service.DecoratorService;
import fr.ifremer.echobase.services.service.UserDbPersistenceService;
import fr.ifremer.echobase.ui.actions.AbstractJSONPaginedAction;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.decorator.Decorator;
import org.nuiton.topia.persistence.pager.FilterRule;
import org.nuiton.topia.persistence.pager.FilterRuleGroupOperator;
import org.nuiton.topia.persistence.pager.FilterRuleOperator;

import javax.inject.Inject;
import java.util.Collection;
import java.util.Collections;
import java.util.Map;
import org.nuiton.topia.persistence.TopiaEntity;

/**
 * To obtain the data for the given request.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 0.1
 */
public class GetEntities extends AbstractJSONPaginedAction {

    private static final long serialVersionUID = 1L;

    /** Logger. */
    private static final Log log = LogFactory.getLog(GetEntities.class);

    protected String entityId;

    public void setEntityId(String entityId) {
        this.entityId = entityId;
    }

    /** Type of entity to load. */
    protected EchoBaseUserEntityEnum entityType;

    public void setEntityType(EchoBaseUserEntityEnum entityType) {
        this.entityType = entityType;
    }

    /** Datas of the given table. */
    protected Map<?, ?>[] datas;

    public Map<?, ?>[] getDatas() {
        return datas;
    }

    @Override
    public String execute() throws Exception {

        Boolean ascendantOrder = isSortAscendant();
        String sortColumn = getSortColumn();

        initFilter();

        if (log.isDebugEnabled()) {
            log.debug("filters      = " + getFilters());
            log.debug("sidx         = " + sortColumn);
            log.debug("sord         = " + ascendantOrder);
        }

        datas = dbEditorService.getData(entityType, pager);

        if (log.isDebugEnabled()) {
            log.debug("Total page = " + getTotal());
        }

        return SUCCESS;
    }


    public String entityModificationLogs() throws Exception {
        entityType = EchoBaseUserEntityEnum.EntityModificationLog;
        execute();
        return SUCCESS;
    }

    public String entityImportLogs() throws Exception {
        entityType = EchoBaseUserEntityEnum.ImportLog;

        if (StringUtils.isNotBlank(entityId)) {

            // add a filter on entityId
            pager.setRules(Collections.singletonList(new FilterRule(
                    FilterRuleOperator.eq, ImportLog.PROPERTY_ENTITY_ID, entityId
            )));
            pager.setGroupOp(FilterRuleGroupOperator.AND);
        }
        execute();
        Map<String, String> importTypes = decoratorService.decorateEnums(ImportType.values());

        for (Map row : datas) {
            String importType = (String) row.get(ImportLog.PROPERTY_IMPORT_TYPE);
            String importTypeToString = importTypes.get(importType);
            row.put(ImportLog.PROPERTY_IMPORT_TYPE, importTypeToString);

            String entityId = (String) row.get(ImportLog.PROPERTY_ENTITY_ID);
            if (entityId != null && !entityId.isEmpty()) {
                TopiaEntity entity = userDbPersistenceService.getEntity(entityId);
                decoratorService.decorateForeignKey(row, ImportLog.PROPERTY_ENTITY_ID, entity, null);
            }
        }
        return SUCCESS;
    }

    public String dashboardVoyageImportLogs() throws Exception {
        return dashboardImportLogs(EchoBaseUserEntityEnum.Voyage);
    }

    public String dashboardMooringImportLogs() throws Exception {
        return dashboardImportLogs(EchoBaseUserEntityEnum.Mooring);
    }

    public String dashboardImportLogs(EchoBaseUserEntityEnum type) throws Exception {
        entityType = type;
        execute();

        Decorator<ImportLog> importLogDecorator = decoratorService.getDecorator(ImportLog.class, DecoratorService.DATE_ONLY);

        Multimap<String, ImportLog> importLogsByEntity = userDbPersistenceService.indexImportLogByEntityId();

        for (Map row : datas) {

            String id = (String) row.get("id");

            Collection<ImportLog> importLogs = importLogsByEntity.get(id);

            TopiaEntity entity = userDbPersistenceService.getEntity(id);
            decoratorService.decorateForeignKey(row, "id", entity, null);

            // to keep importLogs decorated, indexed by their topiaId
            Map<String, String> imports = Maps.newHashMap();

            // to keep importLogs ids indexed by their importType
            Multimap<ImportType, String> result = ArrayListMultimap.create();

            for (ImportLog importLog : importLogs) {
                String importLogToString = importLogDecorator.toString(importLog);
                String importId = importLog.getTopiaId();
                imports.put(importId, importLogToString);
                result.put(importLog.getImportType(), importId);
            }
            row.put("importLogs", imports);

            for (ImportType importType : result.keySet()) {
                row.put("importType." + importType.name(), result.get(importType));
            }

        }
        return SUCCESS;
    }

    //------------------------------------------------------------------------//
    //-- Injected objects                                                     //
    //------------------------------------------------------------------------//

    @Inject
    protected transient UserDbPersistenceService userDbPersistenceService;
    @Inject
    protected transient DbEditorService dbEditorService;
    @Inject
    protected transient DecoratorService decoratorService;
}
