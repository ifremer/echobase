package fr.ifremer.echobase.ui.actions.spatial;

/*
 * #%L
 * EchoBase :: UI
 * %%
 * Copyright (C) 2011 - 2015 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.echobase.entities.EchoBaseUserPersistenceContext;
import fr.ifremer.echobase.entities.data.Voyage;
import fr.ifremer.echobase.persistence.JdbcConfiguration;
import fr.ifremer.echobase.services.service.UserDbPersistenceService;
import fr.ifremer.echobase.services.service.spatial.GisService;
import fr.ifremer.echobase.services.service.spatial.SpatialDataService;
import fr.ifremer.echobase.ui.actions.EchoBaseActionSupport;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.inject.Inject;

/**
 * Created on 1/14/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.8
 */
public class ShowVoyageMap extends EchoBaseActionSupport {

    private static final long serialVersionUID = 1L;

    /** Logger. */
    private static final Log log = LogFactory.getLog(ShowVoyageMap.class);

    @Inject
    protected transient GisService gisService;

    @Inject
    protected transient SpatialDataService spatialDataService;

    @Inject
    protected transient EchoBaseUserPersistenceContext userPersistenceContext;

    @Inject
    private transient UserDbPersistenceService persistenceService;

    /**
     * Id of voyage to display.
     */
    protected String voyageId;

    /**
     * Is current db support spatial data? Means db is pg.
     */
    protected boolean spatialSupport;

    /**
     * Is current db has spatial structures filled ?
     */
    protected boolean spatialStructureFound;

    /**
     * Is application has gis support? Means lizmap is installed.
     */
    protected boolean gisSupport;

    /**
     * Is there some spatial data to compute?
     */
    protected boolean spatialDataToComputeExists;

    /**
     * Url to access to gis.
     */
    protected String lizmapUrl;

    protected Voyage voyage;

    public String getVoyageId() {
        return voyageId;
    }

    public void setVoyageId(String voyageId) {
        this.voyageId = voyageId;
    }

    public String getLizmapUrl() {
        return lizmapUrl;
    }

    public boolean isSpatialStructureFound() {
        return spatialStructureFound;
    }

    public boolean isSpatialSupport() {
        return spatialSupport;
    }

    public boolean isSpatialDataToComputeExists() {
        return spatialDataToComputeExists;
    }

    public boolean isGisSupport() {
        return gisSupport;
    }

    public Voyage getVoyage() {
        return voyage;
    }

    @Override
    public String execute() throws Exception {

        // Get Voyage to display
        voyage = persistenceService.getVoyage(voyageId);

        spatialSupport = userPersistenceContext.isSpatialSupport();

        if (spatialSupport) {

            if (log.isInfoEnabled()) {
                log.info("Db has spatial supports");
            }

            spatialStructureFound = userPersistenceContext.isSpatialStructureFound();

            if (spatialStructureFound) {

                if (log.isInfoEnabled()) {
                    log.info("Spatial structure found.");
                }

                spatialDataToComputeExists = spatialDataService.isSpatialDataToComputeExist();

                if (log.isInfoEnabled()) {
                    log.info("Is there some spatial data to compute? " + spatialDataToComputeExists);
                }

                gisSupport = getEchoBaseApplicationContext().isGisSupport();

                if (gisSupport) {

                    JdbcConfiguration dbConf = getEchoBaseSession().getWorkingDbConfiguration();

                    // generate if required the maps configuration for this voyage

                    gisService.generateMap(dbConf, voyage);

                    // get gis data access url
                    lizmapUrl = gisService.getVoyageMapUrl(dbConf, voyage);

                    if (log.isInfoEnabled()) {
                        log.info("Gis url access: " + lizmapUrl);
                    }

                } else {

                    if (log.isInfoEnabled()) {
                        log.info("Application does not support gis features.");
                    }

                }

            }
        } else {

            if (log.isInfoEnabled()) {
                log.info("Db has no spatial support.");
            }
        }

        return SUCCESS;

    }
}
