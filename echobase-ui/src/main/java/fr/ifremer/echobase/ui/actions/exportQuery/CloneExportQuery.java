package fr.ifremer.echobase.ui.actions.exportQuery;

/*
 * #%L
 * EchoBase :: UI
 * %%
 * Copyright (C) 2011 - 2013 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.opensymphony.xwork2.interceptor.annotations.InputConfig;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * To clone an export query.
 *
 * Created on 11/9/13.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.5
 */
public class CloneExportQuery extends AbstractEditExportQuery {

    /** Logger. */
    private static final Log log = LogFactory.getLog(CloneExportQuery.class);

    private static final long serialVersionUID = 1L;

    @Override
    public void validate() {

        // remove his id
        query.setTopiaId(null);

        // rename it
        query.setName(getQuery().getName() + "-clone");
        super.validate();
    }

    @InputConfig(methodName = INPUT)
    @Override
    public String execute() throws Exception {

        String selectedQueryId = getQuery().getTopiaId();

        if (log.isInfoEnabled()) {
            log.info("Will clone query: " + selectedQueryId);
        }

        String result = super.execute();
        addFlashMessage(t("echobase.info.query.cloned"));
        return result;
    }

}
