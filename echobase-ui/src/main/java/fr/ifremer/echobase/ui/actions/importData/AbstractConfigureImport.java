/*
 * #%L
 * EchoBase :: UI
 * %%
 * Copyright (C) 2011 - 2012 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ifremer.echobase.ui.actions.importData;

import fr.ifremer.echobase.io.EchoBaseIOUtil;
import fr.ifremer.echobase.io.InputFile;
import fr.ifremer.echobase.services.service.importdata.configurations.ImportDataConfigurationSupport;
import fr.ifremer.echobase.ui.actions.AbstractConfigureAction;
import org.apache.commons.io.FileUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.util.FileUtil;

import java.io.File;
import java.io.IOException;

/**
 * Configure a accoustic data import.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 0.3
 */
public abstract class AbstractConfigureImport<M extends ImportDataConfigurationSupport> extends AbstractConfigureAction<M> {

    private static final long serialVersionUID = 1L;

    /** Logger. */
    private static final Log log =
            LogFactory.getLog(AbstractConfigureImport.class);

    protected AbstractConfigureImport(Class<M> modelType) {
        super(modelType);
    }

    public long getUploadFileMaxLength() {
        return getEchoBaseApplicationContext().getUploadFileMaxLength();
    }

    @Override
    protected void prepareExecuteAction(M model) throws IOException {
        File tempDirectory = FileUtils.getTempDirectory();
        File dataDirectory = new File(
                tempDirectory,
                "echobase-importData-" + System.currentTimeMillis());
        FileUtil.createDirectoryIfNecessary(dataDirectory);
        model.setWorkingDirectory(dataDirectory);
        if (log.isInfoEnabled()) {
            log.info("Temporary directory to use : " + dataDirectory);
        }

        for (InputFile inputFile : model.getInputFiles()) {
            if (inputFile.hasFile()) {
                EchoBaseIOUtil.copyFile(inputFile, dataDirectory);
            }
        }
    }
}
