/*
 * #%L
 * EchoBase :: UI
 * %%
 * Copyright (C) 2011 - 2012 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ifremer.echobase.ui.actions.importData;

import fr.ifremer.echobase.entities.data.Voyage;
import fr.ifremer.echobase.services.service.importdata.configurations.VoyageCatchesImportConfiguration;

import java.io.File;
import java.util.Map;

/**
 * Configure a "catches data" import.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 0.3
 */
public class ConfigureCatchesImport extends AbstractConfigureImport<VoyageCatchesImportConfiguration> {

    private static final long serialVersionUID = 1L;

    /** Universe of existing voyages. */
    protected Map<String, String> voyages;

    public ConfigureCatchesImport() {
        super(VoyageCatchesImportConfiguration.class);
    }

    @Override
    protected VoyageCatchesImportConfiguration createModel() {
        return new VoyageCatchesImportConfiguration(getLocale());
    }

    @Override
    protected void prepareInputAction(VoyageCatchesImportConfiguration model) {
        voyages = userDbPersistenceService.loadSortAndDecorate(Voyage.class);
    }

    public Map<String, String> getVoyages() {
        return voyages;
    }

    public void setTotalSampleFile(File file) {
        getModel().getTotalSampleFile().setFile(file);
    }

    public void setTotalSampleFileContentType(String contentType) {
        getModel().getTotalSampleFile().setContentType(contentType);
    }

    public void setTotalSampleFileFileName(String fileName) {
        getModel().getTotalSampleFile().setFileName(fileName);
    }

    public void setSubSampleFile(File file) {
        getModel().getSubSampleFile().setFile(file);
    }

    public void setSubSampleFileContentType(String contentType) {
        getModel().getSubSampleFile().setContentType(contentType);
    }

    public void setSubSampleFileFileName(String fileName) {
        getModel().getSubSampleFile().setFileName(fileName);
    }

    public void setBiometrySampleFile(File file) {
        getModel().getBiometrySampleFile().setFile(file);
    }

    public void setBiometrySampleFileContentType(String contentType) {
        getModel().getBiometrySampleFile().setContentType(contentType);
    }

    public void setBiometrySampleFileFileName(String fileName) {
        getModel().getBiometrySampleFile().setFileName(fileName);
    }
}
