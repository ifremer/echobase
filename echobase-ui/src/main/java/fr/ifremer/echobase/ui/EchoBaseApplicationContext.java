/*
 * #%L
 * EchoBase :: UI
 * %%
 * Copyright (C) 2011 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ifremer.echobase.ui;

import com.google.common.base.Charsets;
import com.google.common.base.Preconditions;
import com.google.common.collect.Sets;
import com.google.common.io.Files;
import com.opensymphony.xwork2.ActionContext;
import fr.ifremer.echobase.EchoBaseTechnicalException;
import fr.ifremer.echobase.config.EchoBaseConfiguration;
import fr.ifremer.echobase.converter.FloatConverter;
import fr.ifremer.echobase.entities.DriverType;
import fr.ifremer.echobase.entities.EchoBaseInternalPersistenceContext;
import fr.ifremer.echobase.entities.EchoBaseInternalTopiaApplicationContext;
import fr.ifremer.echobase.entities.EchoBaseInternalTopiaPersistenceContext;
import fr.ifremer.echobase.entities.EchoBaseUser;
import fr.ifremer.echobase.entities.EchoBaseUserPersistenceContext;
import fr.ifremer.echobase.entities.EchoBaseUserTopiaApplicationContext;
import fr.ifremer.echobase.io.EchoBaseIOUtil;
import fr.ifremer.echobase.persistence.EchoBaseDbMeta;
import fr.ifremer.echobase.persistence.EchoBaseEntityHelper;
import fr.ifremer.echobase.services.DefaultEchoBaseServiceContext;
import fr.ifremer.echobase.services.EchoBaseServiceContext;
import fr.ifremer.echobase.services.service.UserService;
import fr.ifremer.echobase.services.service.embeddedapplication.EmbeddedApplicationService;
import fr.ifremer.echobase.services.service.spatial.GisService;
import fr.ifremer.echobase.services.service.workingDb.WorkingDbConfigurationService;
import org.apache.commons.beanutils.ConvertUtils;
import org.apache.commons.beanutils.Converter;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.log4j.LogManager;
import org.apache.log4j.PropertyConfigurator;
import org.apache.struts2.StrutsConstants;
import org.nuiton.i18n.I18n;
import org.nuiton.i18n.init.DefaultI18nInitializer;
import org.nuiton.topia.persistence.TopiaException;
import org.nuiton.util.RecursiveProperties;
import org.nuiton.util.SortedProperties;
import org.nuiton.converter.ConverterUtil;

import javax.servlet.ServletContext;
import java.beans.Introspector;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Tony Chemit - chemit@codelutin.com
 * @since 0.1
 */
public class EchoBaseApplicationContext {

    /** Logger. */
    private static Log log =
            LogFactory.getLog(EchoBaseApplicationContext.class);

    /** Key to store the single instance of the application context */
    private static final String APPLICATION_CONTEXT_PARAMETER = "echobaseApplicationContext";

    /** Set of all loggued user sessions to be close at shutdown time. */
    protected Set<EchoBaseSession> sessions;

    /**
     * Is application has gis support.
     */
    protected boolean gisSupport;

    public static EchoBaseApplicationContext getApplicationContext(ActionContext actionContext) {
        Map<String, Object> application = actionContext.getApplication();
        return (EchoBaseApplicationContext) application.get(
                APPLICATION_CONTEXT_PARAMETER);
    }

    public static EchoBaseApplicationContext getApplicationContext(ServletContext servletContext) {
        return (EchoBaseApplicationContext) servletContext.getAttribute(
                APPLICATION_CONTEXT_PARAMETER);
    }

    public static void setApplicationContext(ServletContext servletContext,
                                             EchoBaseApplicationContext applicationContext) {
        servletContext.setAttribute(APPLICATION_CONTEXT_PARAMETER,
                                    applicationContext);
    }

    public static void removeApplicationContext(ServletContext servletContext) {
        servletContext.removeAttribute(APPLICATION_CONTEXT_PARAMETER);
    }

    protected EchoBaseConfiguration configuration;

    protected EchoBaseDbMeta dbMeta;

    /** Root context for the internal database. */
    protected EchoBaseInternalTopiaApplicationContext internalTopiaApplicationContext;

    /**
     * Flag setted to true when internal db was just created (should then
     * display in ui created user password).
     *
     * @since 1.1
     */
    protected boolean defaultUsersCreated;

    /**
     * Max upload file size.
     */
    protected long uploadFileMaxLength;

    public Set<EchoBaseSession> getEchoBaseSessions() {
        return sessions;
    }

    public synchronized void registerEchoBaseSession(EchoBaseSession session) {
        Preconditions.checkNotNull(session);
        Preconditions.checkNotNull(session.getUser());
        if (sessions == null) {
            sessions = Sets.newHashSet();
        }
        if (log.isInfoEnabled()) {
            log.info("Register user session for [" +
                    session.getUser().getEmail() + "]");
        }
        sessions.add(session);
    }

    public synchronized void destroyEchoBaseSession(EchoBaseSession session) {
        Preconditions.checkNotNull(session);
        Preconditions.checkNotNull(session.getUser());
        Preconditions.checkNotNull(sessions);
        if (log.isInfoEnabled()) {
            log.info("Destroy user session for [" +
                    session.getUser().getEmail() + "]");
        }
        // remove session from active ones
        sessions.remove(session);
        // close session
        session.close();
    }

    public boolean isGisSupport() {
        return gisSupport;
    }

    public void init() {

        // init I18n
        DefaultI18nInitializer i18nInitializer =
                new DefaultI18nInitializer("echobase-i18n");
        i18nInitializer.setMissingKeyReturnNull(true);
        I18n.init(i18nInitializer, Locale.getDefault());

        // init converters
        Converter converter = ConverterUtil.getConverter(Float.class);
        if (converter != null) {
            ConvertUtils.deregister(Float.class);
        }
        ConvertUtils.register(new FloatConverter(), Float.class);

        // initialize configuration
        configuration = new EchoBaseConfiguration();

        try {
            initLog(configuration);
        } catch (IOException e) {
            Logger.getAnonymousLogger().log(Level.ALL,
                    "Could not init logger.", e);
        }

        if (log.isInfoEnabled()) {
            log.info(configuration.printConfig());
        }

        // initialize internal root context
        internalTopiaApplicationContext =
                EchoBaseInternalTopiaApplicationContext.newApplicationContext(
                        configuration.getInternalDbDirectory());

        dbMeta = EchoBaseDbMeta.newDbMeta();

        // create a service context
        EchoBaseServiceContext serviceContext =
                DefaultEchoBaseServiceContext.newContext(
                        Locale.getDefault(),
                        getConfiguration(),
                        getDbMeta());

        // init database (and create minimal admin user if required)
        initInternalDatabase(serviceContext);

        // init bin directory
        try {
            initBinDirectory();
        } catch (IOException e) {
            throw new TopiaException("Could not init bin directory", e);
        }

        // extract files to library directory if required
        try {
            extractFiles();
        } catch (IOException e) {
            throw new TopiaException("Could not extract files (drivers + embedded war)", e);
        }

        File lizmapConfig = getConfiguration().getLizmapApplicationConfigFile();

        gisSupport = lizmapConfig.exists();

        if (gisSupport) {

            //TODO Check lizmap instance is reachable

            initGisFiles();

        }

        URL resource = getClass().getResource("/struts.properties");
        Properties strutsProperties = new Properties();
        try (InputStream inputStream = resource.openStream()) {
            strutsProperties.load(inputStream);
        } catch (IOException e) {
            throw new TopiaException("Could not load struts configuration", e);
        }
        String property = strutsProperties.getProperty(StrutsConstants.STRUTS_MULTIPART_MAXSIZE);
        uploadFileMaxLength = Long.valueOf(property);
        log.info("uploadFileMaxLength: " + uploadFileMaxLength);

    }

    public EchoBaseConfiguration getConfiguration() {
        return configuration;
    }

    public EchoBaseDbMeta getDbMeta() {
        return dbMeta;
    }

    public EchoBaseInternalTopiaApplicationContext getInternalTopiaApplicationContext() {
        return internalTopiaApplicationContext;
    }

    public boolean isDefaultUsersCreated() {
        return defaultUsersCreated;
    }

    public EchoBaseServiceContext newServiceContext(Locale locale,
                                                    EchoBaseInternalPersistenceContext topiaInternalContext,
                                                    EchoBaseUserPersistenceContext topiaUserContext,
                                                    EchoBaseUserTopiaApplicationContext topiaApplicationContext) {

        EchoBaseServiceContext newServiceContext =
                DefaultEchoBaseServiceContext.newContext(
                        locale,
                        configuration,
                        dbMeta);

        newServiceContext.setEchoBaseInternalPersistenceContext(topiaInternalContext);
        newServiceContext.setEchoBaseUserApplicationContext(topiaApplicationContext);
        newServiceContext.setEchoBaseUserPersistenceContext(topiaUserContext);

        return newServiceContext;
    }

    public void close() {

        try {
            if (internalTopiaApplicationContext != null) {
                // release internal db
                EchoBaseEntityHelper.releaseApplicationContext(internalTopiaApplicationContext);
            }
        } finally {

            try {
                // release all user sessions
                if (CollectionUtils.isNotEmpty(sessions)) {
                    for (EchoBaseSession session : sessions) {
                        destroyEchoBaseSession(session);
                    }
                }
            } finally {
                // see http://wiki.apache.org/commons/Logging/FrequentlyAskedQuestions#A_memory_leak_occurs_when_undeploying.2Fredeploying_a_webapp_that_uses_Commons_Logging._How_do_I_fix_this.3F
                ClassLoader contextClassLoader = Thread.currentThread().getContextClassLoader();
                LogFactory.release(contextClassLoader);

                Introspector.flushCaches();
            }
        }
    }

    /**
     * Init the internal database, says :
     * <ul>
     * <li>If no schema found or if asked to updateSchema using the
     * {@code updateSchema} configuration option is on.</li>
     * <li>If no user found is db, create a administrator user
     * {@code admin/admin}</li>
     * </ul>
     */
    protected void initInternalDatabase(EchoBaseServiceContext serviceContext) throws TopiaException {

        Preconditions.checkNotNull(configuration);
        Preconditions.checkNotNull(dbMeta);
        Preconditions.checkNotNull(internalTopiaApplicationContext);

        if (configuration.isUpdateSchema()) {
            if (log.isInfoEnabled()) {
                log.info("Will update schema...");
            }
            internalTopiaApplicationContext.updateSchema();
        }

        EchoBaseInternalTopiaPersistenceContext tx = internalTopiaApplicationContext.newPersistenceContext();
        try {
            serviceContext.setEchoBaseInternalPersistenceContext(tx);

            UserService service = serviceContext.newService(UserService.class);

            List<EchoBaseUser> users = service.getUsers();

            if (CollectionUtils.isEmpty(users)) {

                // no users in database create the admin user
                if (log.isInfoEnabled()) {
                    log.info("No user in database, will create default users.");
                }

                service.createDefaultUsers();
            }

            if (configuration.isEmbedded()) {

                if (log.isInfoEnabled()) {
                    log.info("Will try to create default working db configuration for internal db.");
                }
                // try to create a default embedded working db configuration
                serviceContext.newService(WorkingDbConfigurationService.class).
                        createEmbeddedWorkingDbConfiguration();
            }
        } finally {
            serviceContext.setEchoBaseInternalPersistenceContext(null);
            EchoBaseEntityHelper.closeConnection(tx);
        }
    }

    protected void initLog(EchoBaseConfiguration configuration) throws IOException {

        // get log configuration
        File logFile = configuration.getLogConfigFile();

        if (!logFile.exists()) {
            EmbeddedApplicationService.copyEmbeddedBinaryFile(
                    logFile.getName(),
                    logFile.getParentFile());

            RecursiveProperties properties = new RecursiveProperties();


            try (BufferedReader reader = Files.newReader(logFile, Charsets.UTF_8)) {
                properties.load(reader);
            }

            // set default log directory
            properties.setProperty(
                    "echobase.log.dir",
                    configuration.getDefaultLogDirectory().getAbsolutePath());
            // copy configuration (with substitutions)
            Properties p2 = new SortedProperties();
            for (Map.Entry<Object, Object> entry : properties.entrySet()) {
                String key = String.valueOf(entry.getKey());

                p2.setProperty(key, properties.getProperty(key));
            }
            p2.remove("echobase.log.dir");
            // write back log configuration file

            try (BufferedWriter writer = Files.newWriter(logFile, Charsets.UTF_8);) {
                p2.store(writer, "Generated by " + getClass().getName());
            }
        }
        // reste logger configuration
        LogManager.resetConfiguration();
        // use generate log config file
        PropertyConfigurator.configure(logFile.getAbsolutePath());
        log = LogFactory.getLog(EchoBaseApplicationContext.class);
        if (log.isInfoEnabled()) {
            log.info("Use now logFile: " + logFile);
        }
    }

    protected void extractFiles() throws IOException {

        // copy drivers
        File libDirectory = configuration.getLibDirectory();
        for (DriverType driverType : DriverType.values()) {
            String pilotFileName = driverType.getPilotFileName(configuration);

            // copy it from class-path
            if (log.isInfoEnabled()) {
                log.info("Copy embedded resource " + pilotFileName +
                        " to directory " + libDirectory);
            }
            EmbeddedApplicationService.copyEmbeddedBinaryFile(pilotFileName, libDirectory);
        }

        if (!getConfiguration().isEmbedded()) {
            // copy embedded war
            File warLocation = configuration.getWarLocation();
            File embeddedWarDirectory = warLocation.getParentFile();
            String embeddedWarFileName = warLocation.getName();

            // copy it from class-path
            if (log.isInfoEnabled()) {
                log.info("Copy embedded war " + embeddedWarFileName +
                        " to directory " + embeddedWarDirectory);
            }
            EmbeddedApplicationService.copyEmbeddedBinaryFile(embeddedWarFileName, embeddedWarDirectory);
        }
    }

    protected void initBinDirectory() throws IOException {

        // copy drivers
        File binDirectory = configuration.getBinDirectory();
        EchoBaseIOUtil.forceMkdir(binDirectory);
        File updateExecutablePath = configuration.getUpdateExecutablePath();
        String updateExecutablePathName = updateExecutablePath.getName();
        // copy it from class-path
        if (log.isInfoEnabled()) {
            log.info("Copy embedded resource " + updateExecutablePathName + " to directory " + binDirectory);
        }
        EmbeddedApplicationService.copyEmbeddedBinaryFile(updateExecutablePathName, binDirectory);

    }

    protected void initGisFiles() {

        try {

            GisService.copyQgisDefaultTemplateFileIfNecessary(configuration);
            GisService.copyQgisResourcesIfNecessary(configuration);
            GisService.copyLizmapDefaultTemplateFileIfNecessary(configuration);

            EchoBaseIOUtil.forceMkdir(configuration.getLizmapProjectsDirectory());

        } catch (IOException e) {
            throw new EchoBaseTechnicalException("Could not create gis support files", e);
        }

        // check templates files are found

        if (!configuration.getQgisTemplateFile().exists()) {
            if (log.isWarnEnabled()) {
                log.warn("disable gisSupport : Could not found Qgis template at: " + configuration.getQgisTemplateFile());
            }
        }

        if (!configuration.getLizmapTemplateFile().exists()) {
            if (log.isWarnEnabled()) {
                log.warn("disable gisSupport : Could not found Lizmap template at: " + configuration.getLizmapTemplateFile());
            }
        }

    }

    public long getUploadFileMaxLength() {
        return uploadFileMaxLength;
    }
}
