/*
 * #%L
 * EchoBase :: UI
 * %%
 * Copyright (C) 2011 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ifremer.echobase.ui.actions.importData;

import fr.ifremer.echobase.I18nAble;
import fr.ifremer.echobase.entities.ImportType;

import static org.nuiton.i18n.I18n.n;

/**
 * To define the mode of import data for mooring (says files to be imported).
 *
 * @author Julien Ruchaud - ruchaud@codelutin.com
 * @since 4.0
 */
public enum ImportMooringMode implements I18nAble {

    /** Import mooring **/
    MooringCommons(n("echobase.common.importType.mooring")),
    
    /** Import ancillary instrumentation. */
    MooringAncillaryInstrumentation(ImportType.MOORING_ANCILLARY_INSTRUMENTATION.getI18nKey()),
    
    /** Import accoustic data (Cells ESDU and Elementary). */
    MooringAcoustic(ImportType.ACOUSTIC.getI18nKey()),

    /** Import results. */
    MooringResults(n("echobase.common.importType.results"));

    private final String i18nKey;

    ImportMooringMode(String i18nKey) {
        this.i18nKey = i18nKey;
    }

    @Override
    public String getI18nKey() {
        return i18nKey;
    }
}
