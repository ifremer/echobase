/*
 * #%L
 * EchoBase :: UI
 * %%
 * Copyright (C) 2011 - 2012 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ifremer.echobase.ui.actions.importData;

import fr.ifremer.echobase.entities.EchoBaseUser;
import fr.ifremer.echobase.entities.EchoBaseUserPersistenceContext;
import fr.ifremer.echobase.services.EchoBaseServiceSupport;
import fr.ifremer.echobase.services.service.importdata.ImportDataResult;
import fr.ifremer.echobase.services.service.importdata.ImportException;
import fr.ifremer.echobase.services.service.importdata.configurations.ImportDataConfigurationSupport;
import fr.ifremer.echobase.ui.actions.AbstractWaitAndExecAction;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.inject.Inject;

/**
 * Abstract action to launch an data import.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 0.3
 */
public abstract class AbstractLaunchImport<M extends ImportDataConfigurationSupport, S extends EchoBaseServiceSupport> extends AbstractWaitAndExecAction<M, S> {

    private static final long serialVersionUID = 1L;

    /** Logger. */
    private static final Log log = LogFactory.getLog(AbstractLaunchImport.class);

    @Inject
    protected transient EchoBaseUserPersistenceContext persistenceContext;

    protected AbstractLaunchImport(Class<M> modelType, Class<S> serviceType) {
        super(modelType, serviceType);
    }

    @Override
    protected String getSuccesMessage() {
        return t("echobase.info.importData.succeded");
    }

    @Override
    protected String getErrorMessage() {
        return t("echobase.info.importData.failed");
    }

    protected abstract ImportDataResult<M> doImport(S service, M model, EchoBaseUser user) throws ImportException;

    @Override
    protected String getResultMessage(M model) {
        String message = t("echobase.message.importData.result", model.getActionTime(), model.getResultMessage());
        if (log.isInfoEnabled()) {
            log.info("Result: " + message);
        }
        return message;
    }

    @Override
    public String getActionResumeTitle() {
        return t("echobase.legend.importData.result.resume");
    }

    @Override
    protected void startAction(S service, M model) throws Exception {
        if (log.isInfoEnabled()) {
            log.info("Start imports for " + getModel());
        }

        try {
            ImportDataResult<M> importResult = doImport(service, model, getEchoBaseSession().getUser());
            model.setResultMessage(importResult.getImportSummary());
        } catch (Exception error) {
            log.error("Error during import", error);
            throw new ImportException("Error during import", error);
        }

    }

    @Override
    protected void closeAction(M model) throws Exception {
        destroyModel(model);
    }

}
