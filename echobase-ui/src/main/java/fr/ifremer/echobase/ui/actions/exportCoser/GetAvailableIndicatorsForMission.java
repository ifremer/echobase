package fr.ifremer.echobase.ui.actions.exportCoser;

/*
 * #%L
 * EchoBase :: UI
 * %%
 * Copyright (C) 2011 - 2015 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.echobase.entities.references.DataMetadata;
import fr.ifremer.echobase.services.service.CoserApiService;
import fr.ifremer.echobase.services.service.CoserIndicators;
import fr.ifremer.echobase.services.service.DecoratorService;
import fr.ifremer.echobase.services.service.UserDbPersistenceService;
import fr.ifremer.echobase.ui.actions.EchoBaseActionSupport;
import org.apache.commons.lang3.StringUtils;

import javax.inject.Inject;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created on 1/21/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.9
 */
public class GetAvailableIndicatorsForMission extends EchoBaseActionSupport {

    private static final long serialVersionUID = 1L;

    @Inject
    protected transient UserDbPersistenceService userDbPersistenceService;

    @Inject
    private transient CoserApiService coserApiService;

    @Inject
    protected transient DecoratorService decoratorService;

    protected Map<String, String> communityIndicators;

    protected Map<String, String> populationIndicators;

    protected String missionId;

    public void setMissionId(String missionId) {
        this.missionId = missionId;
    }

    public Map<String, String> getCommunityIndicators() {
        return communityIndicators;
    }

    public Map<String, String> getPopulationIndicators() {
        return populationIndicators;
    }

    @Override
    public String execute() throws Exception {

        if (StringUtils.isBlank(missionId)) {

            this.communityIndicators = Collections.emptyMap();
            this.populationIndicators = Collections.emptyMap();

        } else {

            Set<String> indicatorNames = coserApiService.getIndicators();

            List<DataMetadata> dataMetadatas = userDbPersistenceService.getDataMetadatasInName(indicatorNames);

            CoserIndicators coserIndicators = userDbPersistenceService.getRegionIndicators(missionId, dataMetadatas);

            this.communityIndicators = decoratorService.sortAndDecorate(coserIndicators.getCommunityDataMetadatasList(), null);
            this.populationIndicators = decoratorService.sortAndDecorate(coserIndicators.getPopulationDataMetadatasList(), null);

        }

        return super.execute();

    }
}