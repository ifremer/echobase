package fr.ifremer.echobase.ui.actions.removeData;

/*
 * #%L
 * EchoBase :: UI
 * %%
 * Copyright (C) 2011 - 2012 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.base.Optional;
import com.google.common.collect.Lists;
import fr.ifremer.echobase.entities.ImportLog;
import fr.ifremer.echobase.entities.ImportType;
import fr.ifremer.echobase.services.service.removedata.RemoveDataConfiguration;
import fr.ifremer.echobase.ui.actions.AbstractConfigureAction;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import org.nuiton.topia.persistence.TopiaEntity;

/**
 * Confirm to delete a import log.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.2
 */
public class ConfirmDelete extends AbstractConfigureAction<RemoveDataConfiguration> {

    private static final long serialVersionUID = 1L;

    /** Logger. */
    private static final Log log = LogFactory.getLog(ConfirmDelete.class);

    protected Map<String, String> importTypes;

    protected List<String> importLogs;

    public ConfirmDelete() {
        super(RemoveDataConfiguration.class);
    }

    public List<String> getImportLogs() {
        return importLogs;
    }

    @Override
    protected RemoveDataConfiguration createModel() {
        return new RemoveDataConfiguration();
    }

    @Override
    protected void prepareExecuteAction(RemoveDataConfiguration model) throws IOException {

        // nothing special to do here
    }

    @Override
    protected void prepareInputAction(RemoveDataConfiguration model) {

        importTypes = decoratorService.decorateEnums(ImportType.values());
        importLogs = Lists.newArrayList();

        for (String id : model.getImportLogIds()) {
            if (log.isInfoEnabled()) {
                log.info("Load import log " + id);
            }

            Optional<ImportLog> optionalImportLog = userDbPersistenceService.getOptionalImportLog(id);
            if (optionalImportLog.isPresent()) {
                ImportLog importLog = optionalImportLog.get();

                String entityDecorated = "";
                String entityId = importLog.getEntityId();
                if (entityId != null) {
                    TopiaEntity entity = userDbPersistenceService.getEntity(entityId);
                    entityDecorated = decoratorService.decorate(entity, null);
                }

                String importType = importTypes.get(importLog.getImportType().name());
                String result = t("echobase.common.importLogToDelete",
                                  entityDecorated,
                                  importType,
                                  importLog.getImportDate());
                importLogs.add(result);

            }

        }
    }

}
