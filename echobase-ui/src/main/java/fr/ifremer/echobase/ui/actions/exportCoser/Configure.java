package fr.ifremer.echobase.ui.actions.exportCoser;

/*
 * #%L
 * EchoBase :: UI
 * %%
 * Copyright (C) 2011 - 2014 Ifremer, Codelutin, Chemit Tony
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.echobase.EchoBaseTechnicalException;
import fr.ifremer.echobase.entities.references.Mission;
import fr.ifremer.echobase.persistence.JdbcConfiguration;
import fr.ifremer.echobase.services.service.CoserApiService;
import fr.ifremer.echobase.services.service.exportCoser.ExportCoserConfiguration;
import fr.ifremer.echobase.ui.EchoBaseSession;
import fr.ifremer.echobase.ui.actions.AbstractConfigureAction;
import org.apache.commons.io.FileUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.util.FileUtil;

import javax.inject.Inject;
import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.Map;

/**
 * Created on 3/1/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.6
 */
public class Configure extends AbstractConfigureAction<ExportCoserConfiguration> {

    private static final long serialVersionUID = 1L;

    /** Logger. */
    private static final Log log = LogFactory.getLog(Configure.class);

    @Inject
    private transient CoserApiService coserApiService;

    /**
     * Universe of missions.
     */
    protected Map<String, String> missions;

    /**
     * Universe of facades (coming from Coser).
     */
    protected Map<String, String> facades;

    /**
     * If coser services are not reachable.
     */
    protected boolean coserUnreachable;

    public Configure() {
        super(ExportCoserConfiguration.class);
    }

    public Map<String, String> getMissions() {
        return missions;
    }

    public Map<String, String> getFacades() {
        return facades;
    }

    public Map<String, String> getZones() {
        return Collections.emptyMap();
    }

    public Map<String, String> getPopulationIndicators() {
        return Collections.emptyMap();
    }

    public Map<String, String> getCommunityIndicators() {
        return Collections.emptyMap();
    }

    public boolean isCoserUnreachable() {
        return coserUnreachable;
    }

    @Override
    protected ExportCoserConfiguration createModel() {
        ExportCoserConfiguration result = new ExportCoserConfiguration();
        result.setExtractCommunityIndicator(true);
        result.setExtractPopulationIndicator(true);
        result.setExtractMap(true);
        result.setExtractRawData(true);
        result.setPublishable(true);

        EchoBaseSession userSession = getEchoBaseSession();
        JdbcConfiguration workingDbConfiguration = userSession.getWorkingDbConfiguration();
        result.setDbConfiguration(workingDbConfiguration);
        result.setUserName(userSession.getUser().getEmail());
        result.setComment("Created by Echobase application (db: " + workingDbConfiguration.getUrl() + ")");
        return result;
    }

    @Override
    protected void prepareInputAction(ExportCoserConfiguration model) {
        loadInputs();
    }

    @Override
    protected void prepareExecuteAction(ExportCoserConfiguration model) throws IOException {

        File tempDirectory = FileUtils.getTempDirectory();
        File dataDirectory = new File(tempDirectory,
                                      "echobase-exportCoser-" + System.currentTimeMillis());
        FileUtil.createDirectoryIfNecessary(dataDirectory);
        model.setWorkingDirectory(dataDirectory);
        if (log.isInfoEnabled()) {
            log.info("Temporary directory to use : " + dataDirectory);
        }
        loadInputs();
    }

    protected void loadInputs() {

        // Get missions
        missions = userDbPersistenceService.loadSortAndDecorate(Mission.class);

        // Get facades from Coser
        try {
            facades = coserApiService.getFacades();
        } catch (EchoBaseTechnicalException e) {
            // could not get
            if (log.isErrorEnabled()) {
                log.error(e);
            }
            coserUnreachable = true;
        }

    }
}
