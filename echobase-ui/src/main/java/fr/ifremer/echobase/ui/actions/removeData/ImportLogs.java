package fr.ifremer.echobase.ui.actions.removeData;

/*
 * #%L
 * EchoBase :: UI
 * %%
 * Copyright (C) 2011 - 2012 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.echobase.entities.data.Mooring;
import fr.ifremer.echobase.entities.data.Voyage;
import fr.ifremer.echobase.services.service.UserDbPersistenceService;
import fr.ifremer.echobase.ui.actions.EchoBaseActionSupport;

import javax.inject.Inject;
import java.util.Map;

/**
 * To display imports logs page.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.3
 */
public class ImportLogs extends EchoBaseActionSupport {

    private static final long serialVersionUID = 1L;

    protected Map<String, String> voyages;

    public Map<String, String> getVoyages() {
        return voyages;
    }

    protected Map<String, String> moorings;

    public Map<String, String> getMoorings() {
        return moorings;
    }
    
    protected String entityId;

    public String getEntityId() {
        return entityId;
    }

    public void setEntityId(String entityId) {
        this.entityId = entityId;
    }
    
    @Override
    public String execute() throws Exception {
        voyages = userDbPersistenceService.loadSortAndDecorate(Voyage.class);
        moorings = userDbPersistenceService.loadSortAndDecorate(Mooring.class);

        return SUCCESS;
    }

    //------------------------------------------------------------------------//
    //-- Injected objects                                                     //
    //------------------------------------------------------------------------//

    @Inject
    protected transient UserDbPersistenceService userDbPersistenceService;
}
