package fr.ifremer.echobase.ui.actions.workingDb;

/*
 * #%L
 * EchoBase :: UI
 * %%
 * Copyright (C) 2011 - 2012 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.Lists;
import fr.ifremer.echobase.entities.EchoBaseUserEntityEnum;
import fr.ifremer.echobase.entities.ImportType;
import fr.ifremer.echobase.ui.actions.EchoBaseActionSupport;

import java.io.Serializable;
import java.util.List;

/**
 * DashBoard of existing voyages.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.3
 */
public class DashBoard extends EchoBaseActionSupport {

    private static final long serialVersionUID = 1L;

    protected List<ImportTypeEntry> importVoyageTypes;

    public List<ImportTypeEntry> getImportVoyageTypes() {
        return importVoyageTypes;
    }

    protected List<ImportTypeEntry> importMooringTypes;

    public List<ImportTypeEntry> getImportMooringTypes() {
        return importMooringTypes;
    }

    @Override
    public String execute() throws Exception {
        importVoyageTypes = Lists.newLinkedList();
        importMooringTypes = Lists.newLinkedList();

        for (ImportType importType : ImportType.values()) {
            ImportTypeEntry entry = new ImportTypeEntry(
                    importType.name(),
                    t(importType.getShortI18nKey()),
                    t(importType.getI18nKey())
            );
            
            if (importType.getEntityType() == EchoBaseUserEntityEnum.Voyage) {
                importVoyageTypes.add(entry);
            } else {
                importMooringTypes.add(entry);
            }
        }
        return SUCCESS;
    }

    public static class ImportTypeEntry implements Serializable {

        private static final long serialVersionUID = 1L;

        protected String name;

        protected String label;

        protected String title;

        public ImportTypeEntry(String name, String label, String title) {
            this.name = name;
            this.label = label;
            this.title = title;
        }

        public String getName() {
            return name;
        }

        public String getLabel() {
            return label;
        }

        public String getTitle() {
            return title;
        }
    }
}
