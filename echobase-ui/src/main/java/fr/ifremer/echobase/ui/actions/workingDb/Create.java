package fr.ifremer.echobase.ui.actions.workingDb;

/*
 * #%L
 * EchoBase :: UI
 * %%
 * Copyright (C) 2011 - 2012 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.opensymphony.xwork2.interceptor.annotations.InputConfig;
import fr.ifremer.echobase.entities.WorkingDbConfiguration;
import org.apache.commons.lang3.StringUtils;

/**
 * To create a new {@link WorkingDbConfiguration}.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.1
 */
public class Create extends AbstractWorkingDbAction {

    private static final long serialVersionUID = 1L;

    public boolean isNewConf() {
        return true;
    }

    public String createConf() throws Exception {

        // new workingDbConfiguration in progress
        addFlashMessage(
                t("echobase.info.new.workingDbConfiguration.inprogress"));

        return INPUT;
    }

    protected boolean clone;

    public boolean isClone() {
        return clone;
    }

    public String cloneConf() throws Exception {

        clone = true;

        getConf().setTopiaId(null);

        // new workingDbConfiguration in progress
        addFlashMessage(
                t("echobase.info.new.workingDbConfiguration.inprogress"));

        return INPUT;
    }

    @InputConfig(methodName = INPUT)
    @Override
    public String execute() throws Exception {

        conf = workingDbConfigurationService.create(getConf());

        addFlashMessage(t("echobase.info.workingDbconfiguration.created",
                          conf.getUrl()));

        return SUCCESS;
    }

    @Override
    public void validate() {

        if (StringUtils.isBlank(getConf().getUrl())) {
            addFieldError(
                    "conf.url",
                    t("echobase.error.workingDbConfiguration.url.required"));
        } else {

            if (!isConfExists()) {

                // when creating a request
                // check this url does not exists

                boolean urlUsed = workingDbConfigurationService.isUrlAlreadyUsed(getConf().getUrl());
                if (urlUsed) {
                    addFieldError("conf.url",
                                  t("echobase.error.workingDbConfiguration.url.already.exists"));
                }
            }
        }

        if (StringUtils.isBlank(getConf().getDescription())) {
            addFieldError(
                    "conf.description",
                    t("echobase.error.workingDbConfiguration.description.required"));
        }
    }
}
