/*
 * #%L
 * EchoBase :: UI
 * %%
 * Copyright (C) 2011 - 2012 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ifremer.echobase.ui.actions.user;

import fr.ifremer.echobase.entities.EchoBaseUser;
import fr.ifremer.echobase.services.service.UserService;
import fr.ifremer.echobase.ui.actions.EchoBaseActionSupport;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.inject.Inject;

/**
 * To create a user.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 0.2
 */
public class Create extends EchoBaseActionSupport {

    protected static final Log log = LogFactory.getLog(Create.class);

    private static final long serialVersionUID = 1L;

    protected EchoBaseUser user;

    public EchoBaseUser getUser() {
        if (user == null) {
            user = userService.newUser();
        }
        return user;
    }

    @Override
    public String execute() throws Exception {
        EchoBaseUser userToCreate = getUser();
        String userEmail = userToCreate.getEmail();

        if (log.isInfoEnabled()) {
            log.info("will create user  " + userEmail);
        }

        // create user
        userService.createOrUpdate(userToCreate);

        // add info message
        addFlashMessage(t("echobase.info.user.create", userEmail));
        return SUCCESS;
    }

    @Override
    public void validate() {

        EchoBaseUser userToValidate = getUser();
        String userEmail = userToValidate.getEmail();

        // login + password required
        if (StringUtils.isEmpty(userEmail)) {

            // empty user login
            addFieldError("user.email",
                          t("echobase.error.required.email"));
        } else {

            // check login not already used
            EchoBaseUser login;
            try {
                login = userService.getUserByEmail(userEmail);
            } catch (Exception e) {

                // could not get user
                throw new IllegalStateException(
                        "Could not obtain user " + userEmail, e);
            }
            if (login != null) {
                addFieldError("user.email",
                              t("echobase.error.email.already.used"));
            }
        }

        String userPassword = userToValidate.getPassword();
        if (StringUtils.isEmpty(userPassword)) {

            // empty user password
            addFieldError("user.password",
                          t("echobase.error.required.password"));
        }

    }

    //------------------------------------------------------------------------//
    //-- Injected objects                                                     //
    //------------------------------------------------------------------------//

    @Inject
    protected transient UserService userService;
}
