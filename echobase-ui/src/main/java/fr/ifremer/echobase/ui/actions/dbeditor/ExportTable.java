/*
 * #%L
 * EchoBase :: UI
 * %%
 * Copyright (C) 2011 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ifremer.echobase.ui.actions.dbeditor;

import fr.ifremer.echobase.entities.EchoBaseUserEntityEnum;
import fr.ifremer.echobase.services.service.DbEditorService;
import fr.ifremer.echobase.services.service.exportdb.ExportService;
import fr.ifremer.echobase.ui.actions.EchoBaseActionSupport;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.persistence.metadata.TableMeta;

import javax.inject.Inject;
import java.io.ByteArrayInputStream;
import java.io.InputStream;

/**
 * To export datas for the given request.
 *
 * @author Sylvain Letellier
 * @since 0.1
 */
public class ExportTable extends EchoBaseActionSupport {

    private static final long serialVersionUID = 1L;

    /** Logger. */
    private static final Log log = LogFactory.getLog(ImportTable.class);

    /** Type of entity to export. */
    protected EchoBaseUserEntityEnum entityType;

    public void setEntityType(EchoBaseUserEntityEnum entityType) {
        this.entityType = entityType;
    }

    /** Default file name to create. */
    protected String exportFileName;

    public String getExportFileName() {
        return exportFileName;
    }

    public void setExportFileName(String exportFileName) {
        this.exportFileName = exportFileName;
    }

    /** Export datas as seen on screen (no topiaId). */
    protected boolean exportAsSeen;

    public void setExportAsSeen(boolean exportAsSeen) {
        this.exportAsSeen = exportAsSeen;
    }

    /** Stream of the file to export. */
    protected transient InputStream inputStream;

    public InputStream getInputStream() {
        return inputStream;
    }

    /** Length of the file to export. */
    protected long contentLength;

    public long getContentLength() {
        return contentLength;
    }

    /** Type of the file to export. */
    protected String contentType;

    public String getContentType() {
        return contentType;
    }

    @Override
    public String execute() throws Exception {

        TableMeta<EchoBaseUserEntityEnum> table = dbEditorService.getTableMeta(entityType);
        if (log.isInfoEnabled()) {
            log.info("Will generate content to export to file: " + exportFileName);
        }
        String content = exportService.exportData(table, exportAsSeen);

        if (log.isDebugEnabled()) {
            log.debug("file to export " + content);
        }
        byte[] bytes = content.getBytes();
        contentLength = bytes.length;
        if (log.isDebugEnabled()) {
            log.debug("Export content length: " + contentLength);
        }
        contentType = "text/csv";

        inputStream = new ByteArrayInputStream(bytes);
        return SUCCESS;
    }

    //------------------------------------------------------------------------//
    //-- Injected objects                                                     //
    //------------------------------------------------------------------------//

    @Inject
    protected transient DbEditorService dbEditorService;

    @Inject
    protected transient ExportService exportService;
}
