/*
 * #%L
 * EchoBase :: UI
 * %%
 * Copyright (C) 2011 - 2012 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ifremer.echobase.ui.actions.embeddedApplication;

import fr.ifremer.echobase.config.EchoBaseConfiguration;
import fr.ifremer.echobase.entities.data.Voyage;
import fr.ifremer.echobase.services.service.embeddedapplication.EmbeddedApplicationConfiguration;
import fr.ifremer.echobase.ui.actions.AbstractConfigureAction;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.io.FileUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.util.FileUtil;
import org.nuiton.version.Version;

import java.io.File;
import java.io.IOException;
import java.util.Map;

/**
 * Configure the embedded application creation.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 0.2
 */
public class Configure extends AbstractConfigureAction<EmbeddedApplicationConfiguration> {

    private static final long serialVersionUID = 1L;

    /** Logger. */
    private static final Log log = LogFactory.getLog(Configure.class);

    /** Universe of voyages to export in db. */
    protected Map<String, String> voyages;

    public Map<String, String> getVoyages() {
        return voyages;
    }

    public Configure() {
        super(EmbeddedApplicationConfiguration.class);
    }

    @Override
    protected EmbeddedApplicationConfiguration createModel() {
        return new EmbeddedApplicationConfiguration();
    }

    @Override
    protected void prepareInputAction(EmbeddedApplicationConfiguration model) {

        // give default embedded application archive file name
        EchoBaseConfiguration configuration =
                getEchoBaseApplicationContext().getConfiguration();
        Version version = configuration.getApplicationVersion();
        String fileName = "echobase-embedded-" + version.toString();
        model.setFileName(fileName);

        // gets war location
        File warLocation = configuration.getWarLocation();
        if (!warLocation.exists()) {
            addFlashError(t("echobase.error.warlocation.notFound",
                            warLocation));
        }
        model.setWarLocation(warLocation);

        voyages = userDbPersistenceService.loadSortAndDecorate(Voyage.class);

        if (MapUtils.isEmpty(voyages)) {
            addFlashMessage(t("echobase.info.no.voyagee.found"));
        }
    }

    @Override
    protected void prepareExecuteAction(EmbeddedApplicationConfiguration model) throws IOException {

        // creates a new temporary working directory
        File tempDirectory = FileUtils.getTempDirectory();
        File dataDirectory = new File(
                tempDirectory,
                "echobase-embeddedApplication-" + System.currentTimeMillis());
        FileUtil.createDirectoryIfNecessary(dataDirectory);
        model.setWorkingDirectory(dataDirectory);
        if (log.isInfoEnabled()) {
            log.info("Temporary directory to use : " + dataDirectory);
        }

        File warLocation = getEchoBaseApplicationContext().getConfiguration().getWarLocation();
        model.setWarLocation(warLocation);
    }
}
