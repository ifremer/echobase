/*
 * #%L
 * EchoBase :: UI
 * %%
 * Copyright (C) 2011 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ifremer.echobase.ui.actions.importDb;

import fr.ifremer.echobase.entities.EchoBaseUser;
import fr.ifremer.echobase.services.service.importdb.ImportDbConfiguration;
import fr.ifremer.echobase.services.service.importdb.ImportDbMode;
import fr.ifremer.echobase.services.service.importdb.ImportDbService;
import fr.ifremer.echobase.ui.actions.AbstractWaitAndExecAction;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Start the import db.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 0.2
 */
public class Import extends AbstractWaitAndExecAction<ImportDbConfiguration, ImportDbService> {

    private static final long serialVersionUID = 1L;

    /** Logger. */
    private static final Log log = LogFactory.getLog(Import.class);

    public Import() {
        super(ImportDbConfiguration.class, ImportDbService.class);
    }

    @Override
    protected String getSuccesMessage() {
        return t("echobase.info.importDb.succeded");
    }

    @Override
    protected String getErrorMessage() {
        return t("echobase.info.importDb.failed");
    }

    @Override
    protected String getResultMessage(ImportDbConfiguration model) {
        String message;
        switch (model.getImportDbMode()) {
            case REFERENTIAL:
                message = t("echobase.importDb.referentialResult",
                            model.getActionTime());

                break;
            case FREE:
                message = t("echobase.importDb.freeResult",
                            model.getActionTime());
                break;
            default:
                throw new IllegalStateException("Can't reach here...");
        }
        if (log.isInfoEnabled()) {
            log.info("Result: " + message);
        }
        return message;
    }

    @Override
    public String getActionResumeTitle() {
        return t("echobase.legend.importDb.resume");
    }

    @Override
    protected void startAction(ImportDbService service,
                               ImportDbConfiguration model) throws Exception {
        if (log.isInfoEnabled()) {
            log.info("Start imports with file " +
                     model.getInput().getFileName());
        }

        EchoBaseUser user = getEchoBaseSession().getUser();
        service.doImport(model, user);
    }

    @Override
    protected void closeAction(ImportDbConfiguration model) throws Exception {
        destroyModel(model);
    }
}
