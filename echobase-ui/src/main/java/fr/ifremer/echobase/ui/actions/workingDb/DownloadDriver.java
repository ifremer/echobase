package fr.ifremer.echobase.ui.actions.workingDb;

/*
 * #%L
 * EchoBase :: UI
 * %%
 * Copyright (C) 2011 - 2012 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.echobase.config.EchoBaseConfiguration;
import fr.ifremer.echobase.persistence.JdbcConfiguration;
import fr.ifremer.echobase.ui.actions.EchoBaseActionSupport;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;

/**
 * Download jdbc driver.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.1
 */
public class DownloadDriver extends EchoBaseActionSupport {

    private static final long serialVersionUID = 1L;

    /** File name of the download. */
    protected String fileName;

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getFileName() {
        return fileName;
    }

    /** Input stream of the file to download. */
    protected transient InputStream inputStream;

    public InputStream getInputStream() {
        return inputStream;
    }

    /** Length of the file to download. */
    protected long contentLength;

    public long getContentLength() {
        return contentLength;
    }

    /** Content type of the file to download. */
    protected String contentType;

    public String getContentType() {
        return contentType;
    }

    @Override
    public String execute() throws Exception {

        EchoBaseConfiguration configuration =
                getEchoBaseApplicationContext().getConfiguration();

        JdbcConfiguration dbConfiguration =
                getEchoBaseSession().getWorkingDbConfiguration();

        fileName = dbConfiguration.getDriverType().getPilotFileName(configuration);

        File file = new File(configuration.getLibDirectory(), fileName);

        contentType = "application/java-archive";
        contentLength = file.length();
        inputStream = new BufferedInputStream(new FileInputStream(file));
        return SUCCESS;
    }
}
