/*
 * #%L
 * EchoBase :: UI
 * %%
 * Copyright (C) 2011 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ifremer.echobase.ui.actions.dbeditor;

import com.google.common.collect.Maps;
import fr.ifremer.echobase.entities.EchoBaseUserEntityEnum;
import fr.ifremer.echobase.entities.EchoBaseUserPersistenceContext;
import fr.ifremer.echobase.services.service.DbEditorService;
import fr.ifremer.echobase.ui.actions.EchoBaseActionSupport;
import org.apache.struts2.interceptor.ParameterAware;
import org.nuiton.topia.persistence.TopiaEntity;
import org.nuiton.topia.persistence.metadata.TableMeta;

import javax.inject.Inject;
import java.util.List;
import java.util.Map;

/**
 * Save the edition of a row of a table.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 0.1
 */
public class SaveEntity extends EchoBaseActionSupport implements ParameterAware {

    private static final long serialVersionUID = 1L;

    /** Type of entity to save. */
    protected EchoBaseUserEntityEnum entityType;

    public void setEntityType(EchoBaseUserEntityEnum entityType) {
        this.entityType = entityType;
    }

    public EchoBaseUserEntityEnum getEntityType() {
        return entityType;
    }

    /** id of entity to save. */
    protected String topiaId;

    public void setTopiaId(String topiaId) {
        this.topiaId = topiaId;
    }

    @Override
    public String execute() throws Exception {

        TableMeta<EchoBaseUserEntityEnum> tableMeta =
                dbEditorService.getTableMeta(entityType);
        Map<String, String> properties = Maps.newHashMap();
        List<String> columnNames = tableMeta.getColumnNames();
        for (String columnName : columnNames) {
            if (parameters.containsKey(columnName)) {

                // there is a such property to edit
                String[] values = parameters.get(columnName);

                if (values.length > 0) {

                    // take only the first value
                    String propertyValue = values[0];

                    // only keep properties with no empty value
                    properties.put(columnName, propertyValue);
                }
            }
        }
        properties.put(TopiaEntity.PROPERTY_TOPIA_ID, topiaId);

        dbEditorService.saveEntity(tableMeta,
                                   properties,
                                   getEchoBaseSession().getUser());

        return SUCCESS;
    }

    //------------------------------------------------------------------------//
    //-- Injected objects                                                     //
    //------------------------------------------------------------------------//

    private transient Map<String, String[]> parameters;

    @Override
    public void setParameters(Map<String, String[]> parameters) {
        this.parameters = parameters;
    }

    @Inject
    protected transient DbEditorService dbEditorService;

    @Inject
    protected transient EchoBaseUserPersistenceContext persistenceContext;
}

