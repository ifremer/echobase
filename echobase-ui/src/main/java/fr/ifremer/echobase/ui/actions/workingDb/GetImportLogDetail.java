package fr.ifremer.echobase.ui.actions.workingDb;

/*
 * #%L
 * EchoBase :: UI
 * %%
 * Copyright (C) 2011 - 2012 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.echobase.entities.EchoBaseUserEntityEnum;
import fr.ifremer.echobase.entities.ImportFile;
import fr.ifremer.echobase.entities.ImportLog;
import fr.ifremer.echobase.entities.ImportType;
import fr.ifremer.echobase.services.service.DbEditorService;
import fr.ifremer.echobase.services.service.DecoratorService;
import fr.ifremer.echobase.services.service.UserDbPersistenceService;
import fr.ifremer.echobase.ui.actions.EchoBaseActionSupport;
import org.nuiton.topia.persistence.metadata.TableMeta;

import javax.inject.Inject;
import java.util.Collection;
import java.util.Map;
import org.nuiton.topia.persistence.TopiaEntity;

/**
 * Obtain details of a given {@link ImportLog}.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.3
 */
public class GetImportLogDetail extends EchoBaseActionSupport {

    private static final long serialVersionUID = 1L;

    protected String importLogId;

    public void setImportLogId(String importLogId) {
        this.importLogId = importLogId;
    }
    
    protected String entityId;

    public void setEntityId(String entityId) {
        this.entityId = entityId;
    }
    
    protected Map data;

    public Map<?, ?> getData() {
        return data;
    }

    @Override
    public String execute() throws Exception {

        TableMeta<EchoBaseUserEntityEnum> tableMeta =
                dbEditorService.getTableMeta(EchoBaseUserEntityEnum.ImportLog);
        data = dbEditorService.getData(tableMeta, importLogId);

        // decorate import type
        Map<String, String> importTypes = decoratorService.decorateEnums(ImportType.values());
        String importType = (String) data.get(ImportLog.PROPERTY_IMPORT_TYPE);
        data.put(ImportLog.PROPERTY_IMPORT_TYPE, importTypes.get(importType));

        // decorate foreign keys
        TopiaEntity entity = userDbPersistenceService.getEntity(entityId);
        String entityName = decoratorService.decorate(entity, null);
        data.put("entityName", entityName);

        // get import files
        Collection<ImportFile> importFiles = userDbPersistenceService.getImportFiles(importLogId);
        data.put(ImportLog.PROPERTY_IMPORT_FILE, importFiles);

        return SUCCESS;

    }

    //------------------------------------------------------------------------//
    //-- Injected objects                                                     //
    //------------------------------------------------------------------------//

    @Inject
    protected transient UserDbPersistenceService userDbPersistenceService;

    @Inject
    protected transient DbEditorService dbEditorService;

    @Inject
    protected transient DecoratorService decoratorService;
}
