package fr.ifremer.echobase.ui.actions.exportQuery;

/*
 * #%L
 * EchoBase :: UI
 * %%
 * Copyright (C) 2011 - 2013 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.echobase.entities.ExportQuery;
import fr.ifremer.echobase.services.service.exportquery.ExportQueryService;
import fr.ifremer.echobase.ui.actions.EchoBaseActionSupport;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.inject.Inject;

/**
 * To confirm delete an export query.
 *
 * Created on 11/9/13.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.5
 */
public class ConfirmDeleteExportQuery extends EchoBaseActionSupport {

    private static final long serialVersionUID = 1L;

    /** Logger. */
    private static final Log log =
            LogFactory.getLog(ConfirmDeleteExportQuery.class);

    /** Selected query loaded from database if his id is not empty. */
    protected ExportQuery query;

    public ExportQuery getQuery() {
        if (query == null) {
            query = exportQueryService.newExportQuery();
        }
        return query;
    }

    @Override
    public String execute() throws Exception {

        String selectedQueryId = getQuery().getTopiaId();

        if (log.isInfoEnabled()) {
            log.info("Will confirm to delete query: " + selectedQueryId);
        }

        // get query
        query = exportQueryService.getExportQuery(selectedQueryId);
        return SUCCESS;
    }

    //------------------------------------------------------------------------//
    //-- Injected objects                                                     //
    //------------------------------------------------------------------------//

    @Inject
    protected transient ExportQueryService exportQueryService;
}
