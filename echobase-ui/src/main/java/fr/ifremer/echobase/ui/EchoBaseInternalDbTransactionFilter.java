/*
 * #%L
 * EchoBase :: UI
 * %%
 * Copyright (C) 2011 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ifremer.echobase.ui;

import com.opensymphony.xwork2.ActionContext;
import fr.ifremer.echobase.entities.EchoBaseInternalPersistenceContext;
import fr.ifremer.echobase.entities.EchoBaseInternalTopiaApplicationContext;
import fr.ifremer.echobase.entities.EchoBaseInternalTopiaPersistenceContext;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.StrutsStatics;
import org.nuiton.web.filter.TypedTopiaTransactionFilter;

import javax.servlet.FilterConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;

/**
 * Inject in each request a new transaction from the internal db.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 0.1
 */
public class EchoBaseInternalDbTransactionFilter extends TypedTopiaTransactionFilter<EchoBaseInternalPersistenceContext> {

    /** Logger. */
    private static final Log log =
            LogFactory.getLog(EchoBaseInternalDbTransactionFilter.class);

    public static EchoBaseInternalPersistenceContext getPersistenceContext(ActionContext context) {
        HttpServletRequest request = (HttpServletRequest)
                context.get(StrutsStatics.HTTP_REQUEST);
        return EchoBaseInternalDbTransactionFilter.getPersistenceContext(request);
    }

    public EchoBaseInternalDbTransactionFilter() {
        super(EchoBaseInternalPersistenceContext.class);
    }

    protected ServletContext servletContext;

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        super.init(filterConfig);

        servletContext = filterConfig.getServletContext();
    }

    @Override
    protected EchoBaseInternalPersistenceContext beginTransaction(ServletRequest request) {

        EchoBaseApplicationContext applicationContext =
                EchoBaseApplicationContext.getApplicationContext(servletContext);

        EchoBaseInternalTopiaApplicationContext rootContext = applicationContext.getInternalTopiaApplicationContext();
        EchoBaseInternalTopiaPersistenceContext transaction = rootContext.newPersistenceContext();
        if (log.isDebugEnabled()) {
            log.debug("Starts a new echo transaction " + transaction);
        }
        return transaction;
    }

    /**
     * Hook method to close the topia transaction of the request at the end of
     * the request when all filter has been consumed.
     *
     * @param transaction the transaction to close (can be null if transaction
     *                    was not required while the current request)
     * @since 1.0
     */
//    protected void onCloseTransaction(TopiaContext transaction) {
//        //FIXME Check if this necessary any longer ?
//        if (transaction!=null) {
//            transaction.rollbackTransaction();
//        }
////        EchoBaseEntityHelper.closeConnection(transaction);
//    }

//    public static void closeConnection(TopiaContext transaction) {
//        if (transaction == null) {
//            if (log.isTraceEnabled()) {
//                log.trace("no transaction to close");
//            }
//        } else if (transaction.isClosed()) {
//            if (log.isTraceEnabled()) {
//                log.trace("transaction " + transaction + " is already closed");
//            }
//        } else {
//            if (log.isDebugEnabled()) {
//                log.debug("closing transaction " + transaction);
//            }
//
//            try {
//                Transaction tx = transaction.getHibernateSession().getPersistenceContext();
//                if (!tx.wasCommitted() && !tx.wasRolledBack()) {
//                    if (log.isDebugEnabled()) {
//                        log.debug("rollback transaction!");
//                    }
//                    tx.rollback();
//                }
//                transaction.closeContext();
//            } catch (TopiaException e) {
//                throw new TopiaRuntimeException(e);
//            }
//        }
//    }
}
