/*
 * #%L
 * EchoBase :: UI
 * %%
 * Copyright (C) 2011 - 2012 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ifremer.echobase.ui.actions.importData;

import com.google.common.base.Preconditions;
import com.google.common.collect.Maps;
import fr.ifremer.echobase.entities.data.Voyage;
import fr.ifremer.echobase.entities.references.Vessel;
import fr.ifremer.echobase.services.service.DecoratorService;
import fr.ifremer.echobase.services.service.UserDbPersistenceService;
import fr.ifremer.echobase.ui.actions.EchoBaseActionSupport;
import org.apache.commons.lang3.StringUtils;
import org.nuiton.decorator.Decorator;

import javax.inject.Inject;
import java.util.Map;
import java.util.Set;

/**
 * Get all vessels used in a voyage.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 0.3
 */
public class GetVesselsForVoyage extends EchoBaseActionSupport {

    private static final long serialVersionUID = 1L;

    /** Selected voyage id. */
    protected String voyageId;

    public void setVoyageId(String voyageId) {
        this.voyageId = voyageId;
    }

    protected Map<String, String> vessels;

    public Map<String, String> getVessels() {
        return vessels;
    }

    @Override
    public String execute() throws Exception {

        vessels = Maps.newLinkedHashMap();

        if (StringUtils.isNotEmpty(voyageId)) {

            Voyage voyage = userDbPersistenceService.getVoyage(voyageId);

            Preconditions.checkNotNull(voyage,
                                       "Could not find voyage with id " + voyageId);
            if (!voyage.isTransitEmpty()) {

                Decorator<Vessel> decorator =
                        decoratorService.getDecorator(Vessel.class, null);

                Set<Vessel> allVessels = voyage.getAllVessels();

                for (Vessel vessel : allVessels) {
                    String value = decorator.toString(vessel);
                    vessels.put(vessel.getTopiaId(), value);
                }

            }
        }

        return SUCCESS;
    }

    //------------------------------------------------------------------------//
    //-- Injected objects                                                     //
    //------------------------------------------------------------------------//

    @Inject
    protected transient UserDbPersistenceService userDbPersistenceService;

    @Inject
    protected transient DecoratorService decoratorService;
}
