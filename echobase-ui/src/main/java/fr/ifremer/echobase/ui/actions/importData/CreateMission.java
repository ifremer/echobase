/*
 * #%L
 * EchoBase :: UI
 * %%
 * Copyright (C) 2011 - 2012 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ifremer.echobase.ui.actions.importData;

import com.google.common.base.Preconditions;
import fr.ifremer.echobase.entities.references.Mission;
import fr.ifremer.echobase.entities.references.MissionImpl;
import fr.ifremer.echobase.services.service.importdata.MissionNameAlreadyExistException;
import fr.ifremer.echobase.services.service.importdata.MissionService;
import fr.ifremer.echobase.ui.actions.EchoBaseActionSupport;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.inject.Inject;

/**
 * To create a new mission
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 0.3
 */
public class CreateMission extends EchoBaseActionSupport {

    private static final long serialVersionUID = 1L;

    /** Logger. */
    private static final Log log = LogFactory.getLog(CreateMission.class);

    /** Mission to create. */
    protected Mission mission;

    public Mission getMission() {
        if (mission == null) {
            mission = new MissionImpl();
        }
        return mission;
    }

    @Override
    public String execute() throws Exception {

        Preconditions.checkNotNull(mission);

        String result = INPUT;

        try {
            Mission missionSaved = missionService.createMission(mission);

            if (log.isInfoEnabled()) {
                log.info("Created mission : " + missionSaved.getTopiaId());
            }
            addFlashMessage(
                    t("echobase.information.mission.created", missionSaved.getName()));
            result = SUCCESS;
        } catch (MissionNameAlreadyExistException e) {
            addFieldError("mission.name",
                          t("echobase.error.mission.name.already.exist"));
        }
        return result;
    }

    //------------------------------------------------------------------------//
    //-- Injected objects                                                     //
    //------------------------------------------------------------------------//

    @Inject
    protected transient MissionService missionService;
}
