/*
 * #%L
 * EchoBase :: UI
 * %%
 * Copyright (C) 2011 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ifremer.echobase.ui;

import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.opensymphony.xwork2.ActionContext;
import fr.ifremer.echobase.entities.EchoBaseUser;
import fr.ifremer.echobase.entities.EchoBaseUserTopiaApplicationContext;
import fr.ifremer.echobase.persistence.EchoBaseEntityHelper;
import fr.ifremer.echobase.persistence.JdbcConfiguration;
import fr.ifremer.echobase.services.AbstractEchobaseActionConfiguration;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.persistence.TopiaNotFoundException;

import javax.servlet.http.HttpSession;
import java.io.Closeable;
import java.io.Serializable;
import java.util.Collection;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

/**
 * The session object of EchoBase to put in servlet session.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 0.1
 */
public class EchoBaseSession implements Closeable, Serializable {

    /** Logger. */
    private static final Log log = LogFactory.getLog(EchoBaseSession.class);

    /** Key to store the {@link EchoBaseSession} instance in the session's map. */
    private static final String SESSION_PARAMETER = "echoBaseSession";

    /** Key to set User connected in this session. */
    private static final String PROPERTY_USER = "echobaseUser";

    /** Key to set root context of working db selected by user (if any). */
    private static final String PROPERTY_USER_DB_CONFIGURATION = "userDbConfiguration";

    /** Key to set root context of working db selected by user (if any). */
    private static final String PROPERTY_USER_DB_APPLICATION_CONTEXT = "userDbApplicationContext";

    public static final String SESSION_TOKEN_MESSAGES = "messages";

    public static final String SESSION_TOKEN_ERRORS = "errors";

    public static final String SESSION_TOKEN_WARNINGS = "warnings";

    private static final long serialVersionUID = 1;

    /** To store all properties in this session. */
    protected Map<String, Object> store;

    /**
     * Obtain the user T3 session.
     *
     * If not found in application session, then will instanciate it and
     * push it in it.
     *
     * At the creation time the session, it will also set his T3 factory.
     *
     * @param actionContext where to find user session
     * @return the user T3 session (never null)
     */
    public static EchoBaseSession getEchoBaseSession(ActionContext actionContext) {
        Map<String, Object> session = actionContext.getSession();
        EchoBaseSession userSession = (EchoBaseSession)
                session.get(SESSION_PARAMETER);
        if (userSession == null) {
            // let's create it
            userSession = new EchoBaseSession();
            session.put(SESSION_PARAMETER, userSession);
        }
        return userSession;
    }

    public static EchoBaseSession getEchoBaseSession(HttpSession session) {
        EchoBaseSession userSession = (EchoBaseSession)
                session.getAttribute(EchoBaseSession.SESSION_PARAMETER);
        if (userSession == null) {
            // let's create it
            userSession = new EchoBaseSession();
            session.setAttribute(SESSION_PARAMETER, userSession);
        }
        return userSession;
    }

    public static void removeEchoBaseSession(ActionContext actionContext) {
        Map<String, Object> session = actionContext.getSession();
        session.remove(EchoBaseSession.SESSION_PARAMETER);
    }

    /**
     * Gets the informations of user as soon as the user is loggued.
     *
     * @return the informations of loggued user or {@code null} if not in session
     */
    public EchoBaseUser getUser() {
        return get(PROPERTY_USER, EchoBaseUser.class);
    }

    public boolean isAdmin() {
        EchoBaseUser user = getUser();
        return user != null && user.isAdmin();
    }

    /**
     * Sets in this session the loggued user.
     *
     * @param user the user loggued to use in this session
     */
    public void setUser(EchoBaseUser user) {
        set(PROPERTY_USER, user);
    }

    public JdbcConfiguration getWorkingDbConfiguration() {
        return get(PROPERTY_USER_DB_CONFIGURATION, JdbcConfiguration.class);
    }

    public EchoBaseUserTopiaApplicationContext getUserDbApplicationContext() {
        return get(PROPERTY_USER_DB_APPLICATION_CONTEXT, EchoBaseUserTopiaApplicationContext.class);
    }

    public boolean isWorkingDbSelected() {
        return contains(PROPERTY_USER_DB_CONFIGURATION);
    }

    /**
     * Initialize the t3 database configuration from the given connection
     * configuration.
     *
     * The given configuration only give the url, login and pasword to use.
     * Complete configuration (for ToPIA) will be loaded interanlly and match
     * only a postgresql db.
     *
     * @param jdbcConfiguration the connection configuration to use
     * @throws TopiaNotFoundException if could not create root context
     */
    public void initUserDb(JdbcConfiguration jdbcConfiguration,
                           boolean createSchema) throws TopiaNotFoundException {

        // close any previous db
        releaseUserDb();

        // creates a new topia root context from configuration
        EchoBaseUserTopiaApplicationContext applicationContext =
                EchoBaseUserTopiaApplicationContext.newApplicationContext(
                        jdbcConfiguration, createSchema);

        // keep configuration is session
        set(PROPERTY_USER_DB_CONFIGURATION, jdbcConfiguration);

        // store it in session
        set(PROPERTY_USER_DB_APPLICATION_CONTEXT, applicationContext);
        if (log.isInfoEnabled()) {
            log.info("User database initialized at " +
                     jdbcConfiguration.getUrl());
        }
    }

    public <C extends AbstractEchobaseActionConfiguration> C getActionConfiguration(Class<C> configurationType) {
        return get(configurationType.getName(), configurationType);
    }

    public <C extends AbstractEchobaseActionConfiguration> void setActionConfiguration(C configurationType) {
        set(configurationType.getClass().getName(), configurationType);
    }

    public <C extends AbstractEchobaseActionConfiguration> void removeActionConfiguration(Class<C> configurationType) {
        remove(configurationType.getName());
    }

    public void releaseUserDb() {

        if (isWorkingDbSelected()) {

            try {
                EchoBaseUserTopiaApplicationContext applicationContext = getUserDbApplicationContext();
                EchoBaseEntityHelper.releaseApplicationContext(applicationContext);
            } finally {
                remove(PROPERTY_USER_DB_CONFIGURATION);
                remove(PROPERTY_USER_DB_APPLICATION_CONTEXT);
            }
        }
    }

    public <T extends Serializable> T getDynamicData(String token) {
        Object result = getDynamicData().get(token);
        return (T) result;
    }

    public <T extends Serializable> Set<T> getDynamicSetData(String token) {
        Object result = getDynamicData().get(token);
        return (Set<T>) result;
    }

    public <T extends Serializable> T consumeDynamicData(String token) {
        T result = getDynamicData(token);
        if (result != null) {
            removeDynamicData(token);
        }
        return result;
    }

    public <T extends Serializable> Set<T> consumeDynamicSetData(String token) {
        Set<T> result = getDynamicSetData(token);
        if (result != null) {
            removeDynamicData(token);
        }
        return result;
    }

    public <T extends Serializable> void putDynamicSetData(String token, Set<T> data) {
        getDynamicData().put(token, (Serializable) data);
        if (log.isDebugEnabled()) {
            log.debug("Dynamic attributes size : " + getDynamicData().size());
        }
    }

    public void putDynamicData(String token, Serializable data) {
        getDynamicData().put(token, data);
        if (log.isDebugEnabled()) {
            log.debug("Dynamic attributes size : " + getDynamicData().size());
        }
    }

    public void removeDynamicData(String token) {
        getDynamicData().remove(token);
        if (log.isDebugEnabled()) {
            log.debug("Dynamic attributes size : " + getDynamicData().size());
        }
    }

    public void clearDynamicData() {
        if (store != null) {
            store.clear();
            if (log.isDebugEnabled()) {
                log.debug("Dynamic attributes size : " + store.size());
            }
        }
    }

    public void addMessage(String messageScope, String message) {
        Set<String> messages = getDynamicSetData(messageScope);
        if (messages == null) {
            messages = Sets.newHashSet(message);
            putDynamicSetData(messageScope, messages);
        } else {
            messages.add(message);
        }
    }

    protected Map<String, Object> getDynamicData() {
        if (store == null) {
            store = Maps.newHashMap();
        }
        return store;
    }

    /** Protect session constructor. */
    protected EchoBaseSession() {
        store = new TreeMap<>();
    }

    /**
     * Remove form this session, the object from his given key and returns it.
     *
     * @param key the key of object to remove from this session
     * @return the removed object from session, or {@code null} if not found
     */
    protected Object remove(String key) {
        Object remove = store.remove(key);
        if (log.isInfoEnabled()) {
            log.info("Remove from user session data [" + key + "] = " + remove);
        }
        return remove;
    }

    /**
     * Tests if for a given key, there is an associated object in this sessio!!!n.
     *
     * @param key the key to test in this session
     * @return {@code true} if an object was found in this session,
     * {@code false} otherwise
     */
    protected boolean contains(String key) {
        return store.containsKey(key);
    }

    /**
     * Gets from this session an object given his key and his type.
     *
     * @param key  the key of object to obtain from this session
     * @param type the type of object to obtain from this session
     * @param <T>  the type of object to obtain from this session
     * @return the object found in this session, or {@code null} if not found
     */
    protected <T> T get(String key, Class<T> type) {
        Object o = store.get(key);
        if (o != null && !type.isInstance(o)) {
            throw new ClassCastException(
                    "parameter " + key + " should be of type " +
                    type.getName() + " but was " + o.getClass().getName());
        }
        return (T) o;
    }

    /**
     * Sets in this session the igven object using the given key.
     *
     * @param key   the key where to store the object in this session
     * @param value the object to store in this session
     */
    protected void set(String key, Object value) {
        if (value == null) {
            remove(key);
        } else {
            store.put(key, value);
            if (log.isInfoEnabled()) {
                log.info("Set in user session data [" + key + "] = " + value);
            }
        }
    }

    /**
     * Release any resources contained in the user session.
     *
     * Will also close any service (like the databaseService which contains
     * connexions to db).
     *
     * @since 1.1
     */
    @Override
    public void close() {

        if (log.isInfoEnabled()) {
            log.info("Close user session for [" + getUser().getEmail() + "]");
        }
        try {
            releaseUserDb();
        } finally {

            Set<String> keys = Sets.newHashSet(store.keySet());
            for (String key : keys) {
                remove(key);
            }
        }
    }

    public boolean isWithFlashMessages() {
        Collection<String> result = getDynamicSetData(EchoBaseSession.SESSION_TOKEN_MESSAGES);
        return CollectionUtils.isNotEmpty(result);
    }

    public boolean isWithFlashErrors() {
        Collection<String> result = getDynamicSetData(EchoBaseSession.SESSION_TOKEN_ERRORS);
        return CollectionUtils.isNotEmpty(result);
    }

    public boolean isWithFlashWarnings() {
        Collection<String> result = getDynamicSetData(EchoBaseSession.SESSION_TOKEN_WARNINGS);
        return CollectionUtils.isNotEmpty(result);
    }

    public Collection<String> getFlashMessages() {
        return consumeDynamicSetData(EchoBaseSession.SESSION_TOKEN_MESSAGES);
    }

    public Collection<String> getFlashErrors() {
        return consumeDynamicSetData(EchoBaseSession.SESSION_TOKEN_ERRORS);
    }

    public Collection<String> getFlashWarnings() {
        return consumeDynamicSetData(EchoBaseSession.SESSION_TOKEN_WARNINGS);
    }
}
