/*
 * #%L
 * EchoBase :: UI
 * %%
 * Copyright (C) 2011 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ifremer.echobase.ui.actions.exportDb;

import fr.ifremer.echobase.services.service.exportdb.ExportDbConfiguration;
import fr.ifremer.echobase.ui.actions.EchoBaseActionSupport;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;

/**
 * Download the result of the complete db export.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 0.2
 */
public class Download extends EchoBaseActionSupport {

    private static final long serialVersionUID = 1L;

    /** Input stream of the file to download. */
    protected transient InputStream inputStream;

    /** File name of the download. */
    protected String fileName;

    /** Length of the file to download. */
    protected long contentLength;

    /** Content type of the file to download. */
    protected String contentType;

    public InputStream getInputStream() {
        return inputStream;
    }

    public String getFileName() {
        return fileName;
    }

    public long getContentLength() {
        return contentLength;
    }

    public String getContentType() {
        return contentType;
    }

    @Override
    public String execute() throws Exception {

        ExportDbConfiguration model =
                getEchoBaseSession().getActionConfiguration(ExportDbConfiguration.class);

        if (model == null) {
            addFlashError(t("echobase.error.no.exportDb.configurationFound"));
            return ERROR;
        }

        File exportFile = model.getExportFile();
        if (exportFile == null) {
            addFlashError(t("echobase.error.no.exportDb.exportFileFound"));
            return ERROR;
        }

        fileName = exportFile.getName();
        contentType = "application/zip";
        contentLength = exportFile.length();
        inputStream = new BufferedInputStream(new FileInputStream(exportFile));

        return SUCCESS;
    }
}
