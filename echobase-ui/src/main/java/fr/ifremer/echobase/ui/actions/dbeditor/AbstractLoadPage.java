/*
 * #%L
 * EchoBase :: UI
 * %%
 * Copyright (C) 2011 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ifremer.echobase.ui.actions.dbeditor;

import com.google.common.collect.Maps;
import fr.ifremer.echobase.entities.EchoBaseUserEntityEnum;
import fr.ifremer.echobase.services.service.DbEditorService;
import fr.ifremer.echobase.ui.actions.EchoBaseActionSupport;
import org.nuiton.topia.persistence.metadata.ColumnMeta;
import org.nuiton.topia.persistence.metadata.DbMeta;
import org.nuiton.topia.persistence.metadata.TableMeta;

import javax.inject.Inject;
import java.util.List;
import java.util.Map;

/**
 * A abstract action which can load the table names and a selected table
 * meta datas.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 0.1
 */
public abstract class AbstractLoadPage extends EchoBaseActionSupport {

    private static final long serialVersionUID = 1L;

    /** Type of entity to load. */
    protected EchoBaseUserEntityEnum entityType;

    public void setEntityType(EchoBaseUserEntityEnum entityType) {
        this.entityType = entityType;
    }

    public EchoBaseUserEntityEnum getEntityType() {
        return entityType;
    }

    /** All entities availables. */
    protected Map<String, String> entityTypes;

    public Map<String, String> getEntityTypes() {
        return entityTypes;
    }

    /** Metas of the table. */
    protected TableMeta<EchoBaseUserEntityEnum> tableMeta;

    public List<ColumnMeta> getColumnMetas() {
        return tableMeta.getColumns();
    }

    public void load() throws Exception {
        entityTypes = Maps.newTreeMap();
        DbMeta<EchoBaseUserEntityEnum> dbMeta = getEchoBaseApplicationContext().getDbMeta();
        for (TableMeta<EchoBaseUserEntityEnum> meta : dbMeta) {

            // keep only editable metas
            if (dbMeta.isEditable(meta)) {
                String name = meta.getName();
                entityTypes.put(name, name);
            }
        }
        if (entityType == null) {

            // no table selected
            addFlashMessage(t("echobase.info.no.table.selected"));
        } else {

            // load table metas
            tableMeta = dbEditorService.getTableMeta(entityType);
        }
    }

    //------------------------------------------------------------------------//
    //-- Injected objects                                                     //
    //------------------------------------------------------------------------//

    @Inject
    protected transient DbEditorService dbEditorService;
}
