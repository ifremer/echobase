/*
 * #%L
 * EchoBase :: UI
 * %%
 * Copyright (C) 2011 - 2012 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ifremer.echobase.ui.actions.user;

import com.opensymphony.xwork2.Preparable;
import fr.ifremer.echobase.entities.EchoBaseUser;
import fr.ifremer.echobase.services.service.UserService;
import fr.ifremer.echobase.ui.actions.EchoBaseActionSupport;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.interceptor.ParameterAware;

import javax.inject.Inject;
import java.util.Map;

/**
 * To delete a user.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 0.2
 */
public class Delete extends EchoBaseActionSupport implements Preparable, ParameterAware {

    protected static final Log log = LogFactory.getLog(Delete.class);

    private static final long serialVersionUID = 1L;

    protected EchoBaseUser user;

    public EchoBaseUser getUser() {
        if (user == null) {
            user = userService.newUser();
        }
        return user;
    }

    @Override
    public void prepare() throws Exception {
        String userId = parameters.get("user.topiaId")[0];
        if (!StringUtils.isEmpty(userId)) {

            // load user
            user = userService.getUserById(userId);
            // do not want to have the password
            user.setPassword("");
            if (log.isInfoEnabled()) {
                log.info("Selected user " + user.getEmail());
            }
        }
    }

    @Override
    public String execute() throws Exception {

        EchoBaseUser userToDelete = getUser();
        String userEmail = userToDelete.getEmail();

        if (log.isInfoEnabled()) {
            log.info("will delete user  " + userEmail);
        }

        // delete user
        userService.delete(userToDelete);

        // add info message
        addFlashMessage(t("echobase.info.user.delete", userEmail));
        return SUCCESS;
    }

    //------------------------------------------------------------------------//
    //-- Injected objects                                                     //
    //------------------------------------------------------------------------//

    private transient Map<String, String[]> parameters;

    @Override
    public void setParameters(Map<String, String[]> parameters) {
        this.parameters = parameters;
    }

    @Inject
    protected transient UserService userService;
}
