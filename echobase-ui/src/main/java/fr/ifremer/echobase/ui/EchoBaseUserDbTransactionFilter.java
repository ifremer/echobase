package fr.ifremer.echobase.ui;

/*
 * #%L
 * EchoBase :: UI
 * %%
 * Copyright (C) 2011 - 2012 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.base.Preconditions;
import com.opensymphony.xwork2.ActionContext;
import fr.ifremer.echobase.entities.EchoBaseUserPersistenceContext;
import fr.ifremer.echobase.entities.EchoBaseUserTopiaApplicationContext;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.StrutsStatics;
import org.nuiton.web.filter.TypedTopiaTransactionFilter;

import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * To inject a transaction coming from the user connected working db.
 *
 * If user has no working db selected, then do nothing.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.1
 */
public class EchoBaseUserDbTransactionFilter extends TypedTopiaTransactionFilter<EchoBaseUserPersistenceContext> {

    /** Logger. */
    private static final Log log =
            LogFactory.getLog(EchoBaseUserDbTransactionFilter.class);

    public static final String USER_TRANSACTION = "userTransaction";

    public EchoBaseUserDbTransactionFilter() {
        super(EchoBaseUserPersistenceContext.class);
    }

    public static EchoBaseUserPersistenceContext getPersistenceContext(ActionContext context) {
        HttpServletRequest request = (HttpServletRequest)
                context.get(StrutsStatics.HTTP_REQUEST);
        return (EchoBaseUserPersistenceContext) request.getAttribute(USER_TRANSACTION);
    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        super.init(filterConfig);
        setRequestAttributeName(USER_TRANSACTION);
    }

    @Override
    public EchoBaseUserPersistenceContext beginTransaction(ServletRequest request) {

        HttpSession session = ((HttpServletRequest) request).getSession();
        Preconditions.checkNotNull(session);
        EchoBaseSession userSession = EchoBaseSession.getEchoBaseSession(session);
        Preconditions.checkNotNull(userSession);
        EchoBaseUserTopiaApplicationContext rootContext = userSession.getUserDbApplicationContext();
        Preconditions.checkNotNull(rootContext);
        EchoBaseUserPersistenceContext transaction = rootContext.newPersistenceContext();
        if (log.isDebugEnabled()) {
            log.debug("Starts a new working db transaction " + transaction);
        }
        return transaction;
    }

}
