/*
 * #%L
 * EchoBase :: UI
 * %%
 * Copyright (C) 2011 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ifremer.echobase.ui.actions.embeddedApplication;

import fr.ifremer.echobase.services.service.embeddedapplication.EmbeddedApplicationConfiguration;
import fr.ifremer.echobase.ui.actions.EchoBaseActionSupport;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;

/**
 * Download the result of sql request in csv format.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 0.1
 */
public class Download extends EchoBaseActionSupport {

    private static final long serialVersionUID = 1L;

    /** Input stream of the file to download. */
    protected transient InputStream inputStream;

    /** File name of the download. */
    protected String fileName;

    /** Length of the file to download. */
    protected long contentLength;

    /** Content type of the file to download. */
    protected String contentType;

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public InputStream getInputStream() {
        return inputStream;
    }

    public long getContentLength() {
        return contentLength;
    }

    public String getContentType() {
        return contentType;
    }

    public String getFileName() {
        return fileName;
    }

    @Override
    public String execute() throws Exception {

        EmbeddedApplicationConfiguration model =
                getEchoBaseSession().getActionConfiguration(EmbeddedApplicationConfiguration.class);

        if (model == null) {
            addFlashError(t("echobase.error.no.embeddedApplication.configurationFound"));
            return ERROR;
        }

        File file = model.getEmbeddedApplicationFile();
        if (file == null) {
            addFlashError(t("echobase.error.no.embeddedApplication.exportFileFound"));
            return ERROR;
        }

        fileName = file.getName();
        contentType = "application/zip";
        contentLength = file.length();
        inputStream = new BufferedInputStream(new FileInputStream(file));
        return SUCCESS;
    }
}
