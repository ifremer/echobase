/*
 * #%L
 * EchoBase :: UI
 * %%
 * Copyright (C) 2011 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ifremer.echobase.ui.actions;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.Preparable;
import fr.ifremer.echobase.entities.EchoBaseUserPersistenceContext;
import fr.ifremer.echobase.persistence.EchoBaseEntityHelper;
import fr.ifremer.echobase.services.AbstractEchobaseActionConfiguration;
import fr.ifremer.echobase.services.EchoBaseService;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.nuiton.topia.persistence.internal.AbstractTopiaPersistenceContext;

import java.io.IOException;

/**
 * Abstract long action using the exec and wait interceptor.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 0.2
 */
public abstract class AbstractWaitAndExecAction<M extends AbstractEchobaseActionConfiguration, S extends EchoBaseService> extends EchoBaseActionSupport implements Preparable {

    private static final long serialVersionUID = 1L;

    /**
     * Model type ot use.
     *
     * @since 1.1
     */
    private final Class<M> modelType;

    /**
     * Service type to use in this action.
     *
     * @since 1.1
     */
    private final Class<S> serviceType;

    /** Configuration of the action. */
    private M model;

    /** Shared actionContext to reuse all invocation of this same action. */
    private ActionContext actionContext;

    private String result;

    public final String getResult() {
        return result;
    }

    public final Exception getError() {
        return model.getError();
    }

    public String getErrorStack() {

        String errorStack = null;
        if (model.hasError()) {

            errorStack = ExceptionUtils.getStackTrace(model.getError());
        }

        return errorStack;
    }

    protected AbstractWaitAndExecAction(Class<M> modelType,
                                        Class<S> serviceType) {
        this.modelType = modelType;
        this.serviceType = serviceType;
    }

    protected abstract void startAction(S service, M model) throws Exception;

    public abstract String getActionResumeTitle();

    protected abstract String getSuccesMessage();

    protected abstract String getErrorMessage();

    protected abstract String getResultMessage(M model);

    public final M getModel() {
        return model;
    }

    @Override
    public void prepare() throws Exception {

        if (actionContext == null) {

            // keep it since exec and wait then use another thread
            actionContext = ActionContext.getContext();
        } else {

            // having the action context here means we already came here,
            // now we need to propagate it
            ActionContext.setContext(actionContext);
        }
        model = getEchoBaseSession().getActionConfiguration(modelType);
    }

    public final String result() throws Exception {

        try {
            if (model.hasError()) {

                addFlashError(getErrorMessage());
            } else {

                // ok no error, compute result message
                result = getResultMessage(model);
                addFlashMessage(getSuccesMessage());
            }
            return SUCCESS;
        } finally {
            closeAction(model);
        }
    }

    @Override
    public final String execute() throws Exception {

        model.beginAction();

        // having the action context here means we already came here,
        // now we need to propagate it
        ActionContext.setContext(actionContext);

        // we must use a standalone transaction since it will pass through
        // in more than one request

        EchoBaseUserPersistenceContext tx =
                getServiceContext().getEchoBaseUserApplicationContext().newPersistenceContext();

        try {
            getServiceContext().setEchoBaseUserPersistenceContext(tx);

            S service = getServiceContext().newService(serviceType);

            startAction(service, model);

        } catch (Exception e) {
            model.setError(e);
        } finally {

            model.endAction();

            EchoBaseEntityHelper.closeConnection((AbstractTopiaPersistenceContext) tx);
        }

        return SUCCESS;
    }

    protected void closeAction(M model) throws Exception {
        // by default do nothing
    }

    protected void destroyModel(M model) throws IOException {

        try {
            // clean configuration
            model.destroy();
        } finally {

            // remove configuration from session
            getEchoBaseSession().removeActionConfiguration(modelType);
        }
    }
}
