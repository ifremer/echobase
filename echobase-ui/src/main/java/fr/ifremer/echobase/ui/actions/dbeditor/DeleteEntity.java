package fr.ifremer.echobase.ui.actions.dbeditor;

/*
 * #%L
 * EchoBase :: UI
 * %%
 * Copyright (C) 2011 - 2014 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.echobase.EchoBaseTechnicalException;
import fr.ifremer.echobase.entities.EchoBaseUserEntityEnum;
import fr.ifremer.echobase.services.service.DbEditorService;
import fr.ifremer.echobase.services.service.spatial.SpatialDataService;
import fr.ifremer.echobase.ui.actions.EchoBaseActionSupport;

import javax.inject.Inject;

/**
 * Created on 12/8/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.6.1
 */
public class DeleteEntity extends EchoBaseActionSupport {

    private static final long serialVersionUID = 1L;

    /** Type of entity to delete. */
    protected EchoBaseUserEntityEnum entityType;

    public void setEntityType(EchoBaseUserEntityEnum entityType) {
        this.entityType = entityType;
    }

    public EchoBaseUserEntityEnum getEntityType() {
        return entityType;
    }

    /** id of entity to delete. */
    protected String topiaId;

    public void setTopiaId(String topiaId) {
        this.topiaId = topiaId;
    }

    @Override
    public String execute() throws Exception {

        try {
            dbEditorService.deleteEntity(entityType, topiaId, getEchoBaseSession().getUser());
            addFlashMessage(t("echobase.info.delete.entity", entityType));
        } catch (EchoBaseTechnicalException e) {
            throw e;
        } catch (Exception e) {
            addFlashError(t("echobase.error.delete.entity", entityType, topiaId));
        }

        return SUCCESS;
    }

    //------------------------------------------------------------------------//
    //-- Injected objects                                                     //
    //------------------------------------------------------------------------//

    @Inject
    protected transient DbEditorService dbEditorService;

    @Inject
    protected transient SpatialDataService spatialDataService;
}
