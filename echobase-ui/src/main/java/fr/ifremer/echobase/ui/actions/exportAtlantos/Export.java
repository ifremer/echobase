package fr.ifremer.echobase.ui.actions.exportAtlantos;

/*
 * #%L
 * EchoBase :: UI
 * %%
 * Copyright (C) 2011 - 2014 Ifremer, Codelutin, Chemit Tony
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.echobase.services.service.atlantos.ExportAtlantosConfiguration;
import fr.ifremer.echobase.services.service.atlantos.ExportAtlantosService;
import fr.ifremer.echobase.ui.actions.AbstractWaitAndExecAction;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import static org.nuiton.i18n.I18n.t;

/**
 * Created on 3/1/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.6
 */
public class Export extends AbstractWaitAndExecAction<ExportAtlantosConfiguration, ExportAtlantosService> {

    private static final long serialVersionUID = 1L;

    /** Logger. */
    private static final Log log = LogFactory.getLog(Export.class);

    public Export() {
        super(ExportAtlantosConfiguration.class, ExportAtlantosService.class);
    }

    @Override
    protected void startAction(ExportAtlantosService service,
                               ExportAtlantosConfiguration model) throws Exception {
        if (log.isInfoEnabled()) {
            log.info("Start Atlantos export for voyage" + model.getVoyageId());
        }
        service.doXmlExport(model);
    }

    @Override
    protected String getSuccesMessage() {
        return t("echobase.info.exportAtlantos.succeded");
    }

    @Override
    protected String getErrorMessage() {
        return t("echobase.info.exportAtlantos.failed");
    }

    @Override
    public String getActionResumeTitle() {
        return t("echobase.legend.exportAtlantos.resume");
    }

    @Override
    protected String getResultMessage(ExportAtlantosConfiguration model) {
        String result = t("echobase.message.exportAtantos.result",
                          model.getExportFile().getName(),
                          model.getActionTime());

        if (log.isInfoEnabled()) {
            log.info("Result: " + result);
        }
        return result;
    }
}
