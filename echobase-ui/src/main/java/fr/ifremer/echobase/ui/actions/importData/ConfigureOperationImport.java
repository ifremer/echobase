/*
 * #%L
 * EchoBase :: UI
 * %%
 * Copyright (C) 2011 - 2012 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ifremer.echobase.ui.actions.importData;

import fr.ifremer.echobase.entities.data.Voyage;
import fr.ifremer.echobase.services.service.importdata.configurations.VoyageOperationsImportConfiguration;

import java.io.File;
import java.util.Map;

/**
 * Configure a "Operation" import.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 0.3
 */
public class ConfigureOperationImport extends AbstractConfigureImport<VoyageOperationsImportConfiguration> {

    private static final long serialVersionUID = 1L;

    /** Universe of existing voyages. */
    protected Map<String, String> voyages;

    public ConfigureOperationImport() {
        super(VoyageOperationsImportConfiguration.class);
    }

    @Override
    protected VoyageOperationsImportConfiguration createModel() {
        return new VoyageOperationsImportConfiguration(getLocale());
    }

    @Override
    protected void prepareInputAction(VoyageOperationsImportConfiguration model) {
        voyages = userDbPersistenceService.loadSortAndDecorate(Voyage.class);
    }

    public Map<String, String> getVoyages() {
        return voyages;
    }

    public void setOperationFile(File file) {
        getModel().getOperationFile().setFile(file);
    }

    public void setOperationFileContentType(String contentType) {
        getModel().getOperationFile().setContentType(contentType);
    }

    public void setOperationFileFileName(String fileName) {
        getModel().getOperationFile().setFileName(fileName);
    }

    public void setOperationMetadataFile(File file) {
        getModel().getOperationMetadataFile().setFile(file);
    }

    public void setOperationMetadataFileContentType(String contentType) {
        getModel().getOperationMetadataFile().setContentType(contentType);
    }

    public void setOperationMetadataFileFileName(String fileName) {
        getModel().getOperationMetadataFile().setFileName(fileName);
    }

    public void setGearMetadataFile(File file) {
        getModel().getGearMetadataFile().setFile(file);
    }

    public void setGearMetadataFileContentType(String contentType) {
        getModel().getGearMetadataFile().setContentType(contentType);
    }

    public void setGearMetadataFileFileName(String fileName) {
        getModel().getGearMetadataFile().setFileName(fileName);
    }
}
