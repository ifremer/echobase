/*
 * #%L
 * EchoBase :: UI
 * %%
 * Copyright (C) 2011 - 2012 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ifremer.echobase.ui.actions.importData;

import com.google.common.base.Preconditions;
import com.google.common.collect.Maps;
import fr.ifremer.echobase.entities.data.Mooring;
import fr.ifremer.echobase.services.service.UserDbPersistenceService;
import fr.ifremer.echobase.ui.actions.EchoBaseActionSupport;
import org.apache.commons.lang3.StringUtils;

import javax.inject.Inject;
import java.util.Map;

/**
 * Gets all dataProcessings of the selected mooring.
 *
 * @author Julien Ruchaud - ruchaud@codelutin.com
 * @since 4.0
 */
public class GetDataProcessingsForMooring extends EchoBaseActionSupport {

    private static final long serialVersionUID = 1L;

    /** Selected mooring id. */
    protected String entityId;

    protected Map<String, String> dataProcessings;

    public void setEntityId(String entityId) {
        this.entityId = entityId;
    }

    public Map<String, String> getDataProcessings() {
        return dataProcessings;
    }

    @Override
    public String execute() throws Exception {
        if (StringUtils.isEmpty(entityId)) {
            dataProcessings = Maps.newLinkedHashMap();
            
        } else {
            Mooring mooring = userDbPersistenceService.getMooring(entityId);
            Preconditions.checkNotNull(mooring, "Could not find voyage with id " + entityId);

            dataProcessings = userDbPersistenceService.getDataProcessings(mooring);
        }

        return SUCCESS;
    }

    //------------------------------------------------------------------------//
    //-- Injected objects                                                     //
    //------------------------------------------------------------------------//

    @Inject
    protected transient UserDbPersistenceService userDbPersistenceService;
}
