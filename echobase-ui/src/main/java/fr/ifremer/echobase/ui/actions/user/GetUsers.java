/*
 * #%L
 * EchoBase :: UI
 * %%
 * Copyright (C) 2011 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ifremer.echobase.ui.actions.user;

import fr.ifremer.echobase.entities.EchoBaseUser;
import fr.ifremer.echobase.services.service.UserService;
import fr.ifremer.echobase.ui.actions.AbstractJSONPaginedAction;
import org.nuiton.util.beans.Binder;
import org.nuiton.util.beans.BinderFactory;

import javax.inject.Inject;
import java.util.List;
import java.util.Map;

/**
 * Obtains all users of the echobase internal database.
 *
 * @author Sylvain Letellier
 * @since 0.1
 */
public class GetUsers extends AbstractJSONPaginedAction {

    private static final long serialVersionUID = 1L;

    protected transient Map<String, Object>[] datas;

    public Map<String, Object>[] getDatas() {
        return datas;
    }

    @Override
    public String execute() throws Exception {

        List<EchoBaseUser> allUsers = userService.getUsers(pager);

        datas = new Map[allUsers.size()];
        Binder<EchoBaseUser, EchoBaseUser> binder =
                BinderFactory.newBinder(EchoBaseUser.class);

        int index = 0;
        for (EchoBaseUser user : allUsers) {
            Map<String, Object> data = binder.obtainProperties(user);
            data.put("id", user.getTopiaId());
            datas[index++] = data;
        }

        return SUCCESS;
    }

    //------------------------------------------------------------------------//
    //-- Injected objects                                                     //
    //------------------------------------------------------------------------//

    @Inject
    protected transient UserService userService;
}
