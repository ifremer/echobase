/*
 * #%L
 * EchoBase :: UI
 * %%
 * Copyright (C) 2011 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ifremer.echobase.ui.actions.dbeditor;

/**
 * To load the db editor pages.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 0.1
 */
public class LoadEntities extends AbstractLoadPage {

    private static final long serialVersionUID = 1L;

    /** Default name of the export file. */
    protected String exportFileName;

    public String getExportFileName() {
        return exportFileName;
    }

    @Override
    public String execute() throws Exception {

        load();
        if (tableMeta != null) {
            exportFileName = "export-" + tableMeta.getEntityType().getSimpleName() + ".csv";
        }
        return SUCCESS;
    }

    public boolean isAdmin() {
        return getEchoBaseSession().isAdmin();
    }
}
