package fr.ifremer.echobase.ui.actions.workingDb;

/*
 * #%L
 * EchoBase :: UI
 * %%
 * Copyright (C) 2011 - 2012 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.echobase.entities.WorkingDbConfiguration;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * To delete a selected {@link WorkingDbConfiguration}.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.1
 */
public class Delete extends AbstractWorkingDbAction {

    private static final long serialVersionUID = 1L;

    /** Logger. */
    private static final Log log = LogFactory.getLog(Delete.class);

    public String confirmDelete() {

        return SUCCESS;
    }

    @Override
    public String execute() throws Exception {

        String result = INPUT;
        try {
            workingDbConfigurationService.delete(getConf().getTopiaId());
            addFlashMessage(t("echobase.info.workingDbconfiguration.deleted",
                              conf.getUrl()));

            conf = null;
            result = SUCCESS;
        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Could not delete workgingDbconfiguration", e);
            }
        }

        return result;
    }
}
