/*
 * #%L
 * EchoBase :: UI
 * %%
 * Copyright (C) 2011 - 2012 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ifremer.echobase.ui.actions.importData;

import fr.ifremer.echobase.entities.data.Voyage;
import fr.ifremer.echobase.services.service.importdata.configurations.VoyageCommonsAncillaryInstrumentationImportConfiguration;

import java.io.File;
import java.util.Map;

/**
 * Configure a "AncillaryInstrumentation" import.
 *
 * @author Julien Ruchaud - ruchaud@codelutin.com
 * @since 4.0
 */
public class ConfigureAncillaryInstrumentationImport extends AbstractConfigureImport<VoyageCommonsAncillaryInstrumentationImportConfiguration> {

    private static final long serialVersionUID = 1L;

    /** Universe of existing voyages. */
    protected Map<String, String> voyages;

    public ConfigureAncillaryInstrumentationImport() {
        super(VoyageCommonsAncillaryInstrumentationImportConfiguration.class);
    }

    @Override
    protected VoyageCommonsAncillaryInstrumentationImportConfiguration createModel() {
        return new VoyageCommonsAncillaryInstrumentationImportConfiguration(getLocale());
    }

    @Override
    protected void prepareInputAction(VoyageCommonsAncillaryInstrumentationImportConfiguration model) {
        voyages = userDbPersistenceService.loadSortAndDecorate(Voyage.class);
    }

    public Map<String, String> getVoyages() {
        return voyages;
    }

    public void setAncillaryInstrumentationFile(File file) {
        getModel().getAncillaryInstrumentationFile().setFile(file);
    }

    public void setAncillaryInstrumentationFileContentType(String contentType) {
        getModel().getAncillaryInstrumentationFile().setContentType(contentType);
    }

    public void setAncillaryInstrumentationFileFileName(String fileName) {
        getModel().getAncillaryInstrumentationFile().setFileName(fileName);
    }

}
