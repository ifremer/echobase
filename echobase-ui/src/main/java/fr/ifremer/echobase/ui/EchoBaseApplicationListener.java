/*
 * #%L
 * EchoBase :: UI
 * %%
 * Copyright (C) 2011 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ifremer.echobase.ui;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import java.util.Date;

/**
 * To listen start or end of the application.
 *
 * On start we will load the configuration and check connection to internal
 * database, creates schema and create an admin user in none found in database.
 *
 * On stop, just release the application configuration.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 0.1
 */
public class EchoBaseApplicationListener implements ServletContextListener {

    /** Logger. */
    protected static final Log log =
            LogFactory.getLog(EchoBaseApplicationListener.class);

    static {
        try {
            Class.forName("org.postgresql.Driver");
        } catch (ClassNotFoundException e) {
            if (log.isErrorEnabled()) {
                log.error("Could not find pg driver", e);
            }
        }
        try {
            Class.forName("org.h2.Driver");
        } catch (ClassNotFoundException e) {
            if (log.isErrorEnabled()) {
                log.error("Could not find h2 driver", e);
            }
        }
        try {
            Class.forName("org.sqlite.JDBC");
        } catch (ClassNotFoundException e) {
            if (log.isErrorEnabled()) {
                log.error("Could not find sqlite driver", e);
            }
        }

    }

    @Override
    public void contextInitialized(ServletContextEvent sce) {

        if (log.isInfoEnabled()) {
            log.info("Application starting at " + new Date() + "...");
        }

        // initialize application context
        EchoBaseApplicationContext applicationContext =
                new EchoBaseApplicationContext();

        EchoBaseApplicationContext.setApplicationContext(
                sce.getServletContext(), applicationContext);

        applicationContext.init();
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        if (log.isInfoEnabled()) {
            log.info("Application is ending at " + new Date() + "...");
        }

        ServletContext servletContext = sce.getServletContext();

        // get application context
        EchoBaseApplicationContext applicationContext =
                EchoBaseApplicationContext.getApplicationContext(servletContext);

        // remove application context from container
        EchoBaseApplicationContext.removeApplicationContext(servletContext);

        applicationContext.close();
    }
}
