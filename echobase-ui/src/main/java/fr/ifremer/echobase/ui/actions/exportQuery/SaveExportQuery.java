package fr.ifremer.echobase.ui.actions.exportQuery;

/*
 * #%L
 * EchoBase :: UI
 * %%
 * Copyright (C) 2011 - 2013 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.opensymphony.xwork2.interceptor.annotations.InputConfig;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * To save a query.
 *
 * Created on 11/9/13.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.5
 */
public class SaveExportQuery extends AbstractEditExportQuery {

    /** Logger. */
    private static final Log log = LogFactory.getLog(SaveExportQuery.class);

    private static final long serialVersionUID = 1L;

    @InputConfig(methodName = INPUT)
    @Override
    public String execute() throws Exception {

        boolean newQuery = isNewQuery();
        if (log.isInfoEnabled()) {
            if (newQuery) {

                log.info("Will create query: " + getQuery().getName());
            } else {
                log.info("Will save query with id: " + getQuery().getTopiaId());
            }
        }

        String result = super.execute();
        if (newQuery) {
            addFlashMessage(t("echobase.info.query.created"));
        } else {
            addFlashMessage(t("echobase.info.query.saved"));
        }
        return result;
    }

}
