package fr.ifremer.echobase.ui.actions.exportAtlantos;

/*
 * #%L
 * EchoBase :: UI
 * %%
 * Copyright (C) 2011 - 2014 Ifremer, Codelutin, Chemit Tony
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.echobase.entities.data.Voyage;
import fr.ifremer.echobase.services.service.atlantos.ExportAtlantosConfiguration;
import fr.ifremer.echobase.services.service.atlantos.ExportAtlantosService;
import fr.ifremer.echobase.ui.actions.AbstractConfigureAction;
import org.apache.commons.io.FileUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.util.FileUtil;

import javax.inject.Inject;
import java.io.File;
import java.io.IOException;
import java.util.Map;

import static org.nuiton.i18n.I18n.t;

/**
 * Created on 3/1/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.6
 */
public class Configure extends AbstractConfigureAction<ExportAtlantosConfiguration> {

    private static final long serialVersionUID = 1L;

    protected Map<String, String> voyages;
        
    /** Logger. */
    private static final Log log = LogFactory.getLog(Configure.class);

    @Inject
    private transient ExportAtlantosService atlantosService;

    public Configure() {
        super(ExportAtlantosConfiguration.class);
    }

    @Override
    protected ExportAtlantosConfiguration createModel() {
        return new ExportAtlantosConfiguration();
    }

    @Override
    protected void prepareInputAction(ExportAtlantosConfiguration model) {
        voyages = userDbPersistenceService.loadSortAndDecorate(Voyage.class);
    }

    @Override
    protected void prepareExecuteAction(ExportAtlantosConfiguration model) throws IOException {
        File tempDirectory = FileUtils.getTempDirectory();
        File dataDirectory = new File(tempDirectory,
                                      "echobase-ices-" + System.currentTimeMillis());
        FileUtil.createDirectoryIfNecessary(dataDirectory);
        model.setWorkingDirectory(dataDirectory);
        if (log.isInfoEnabled()) {
            log.info("Temporary directory to use : " + dataDirectory);
        }
    }

    public Map<String, String> getVoyages() {
        return voyages;
    }
    
}
