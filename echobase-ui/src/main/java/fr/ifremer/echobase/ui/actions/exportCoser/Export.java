package fr.ifremer.echobase.ui.actions.exportCoser;

/*
 * #%L
 * EchoBase :: UI
 * %%
 * Copyright (C) 2011 - 2014 Ifremer, Codelutin, Chemit Tony
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.echobase.services.service.exportCoser.ExportCoserConfiguration;
import fr.ifremer.echobase.services.service.exportCoser.ExportCoserService;
import fr.ifremer.echobase.services.service.exportCoser.GenerateCoserMapException;
import fr.ifremer.echobase.ui.actions.AbstractWaitAndExecAction;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Created on 3/1/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.6
 */
public class Export extends AbstractWaitAndExecAction<ExportCoserConfiguration, ExportCoserService> {

    private static final long serialVersionUID = 1L;

    /** Logger. */
    private static final Log log = LogFactory.getLog(Export.class);

    public Export() {
        super(ExportCoserConfiguration.class, ExportCoserService.class);
    }

    @Override
    protected void startAction(ExportCoserService service,
                               ExportCoserConfiguration model) throws Exception {
        if (log.isInfoEnabled()) {
            log.info("Start Coser export for mission" + model.getMissionId());
        }
        try {
            service.doExport(model);
        } catch (GenerateCoserMapException e) {

            model.setScriptErrorLog(e.getLog());
            throw e;
        }

    }

    @Override
    protected String getSuccesMessage() {
        return t("echobase.info.exportCoser.succeded");
    }

    @Override
    protected String getErrorMessage() {
        return t("echobase.info.exportCoser.failed");
    }

    @Override
    public String getActionResumeTitle() {
        return t("echobase.legend.exportCoser.resume");
    }

    @Override
    protected String getResultMessage(ExportCoserConfiguration model) {
        String result = t("echobase.message.exportCoser.result",
                          model.getExportFile().getName(),
                          model.getActionTime());

        if (log.isInfoEnabled()) {
            log.info("Result: " + result);
        }
        return result;
    }

    @Override
    public String getErrorStack() {
        String errorStack = super.getErrorStack();
        String scriptErrorLog = getModel().getScriptErrorLog();
        if (scriptErrorLog != null) {
            errorStack += "\n" + scriptErrorLog;
        }
        return errorStack;
    }
}
