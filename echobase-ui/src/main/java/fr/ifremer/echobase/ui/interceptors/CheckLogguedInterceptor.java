/*
 * #%L
 * EchoBase :: UI
 * %%
 * Copyright (C) 2011 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ifremer.echobase.ui.interceptors;

import com.opensymphony.xwork2.ActionInvocation;
import fr.ifremer.echobase.config.EchoBaseConfiguration;
import fr.ifremer.echobase.ui.EchoBaseApplicationContext;
import fr.ifremer.echobase.ui.EchoBaseSession;
import fr.ifremer.echobase.ui.actions.user.Login;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * To check user is loggued. If not, then redirect to the {@link #loginAction}.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 0.1
 */
public class CheckLogguedInterceptor extends AbstractCheckInterceptor {

    private static final long serialVersionUID = 1L;

    /** Logger. */
    private static final Log log =
            LogFactory.getLog(CheckLogguedInterceptor.class);

    /** Where to redirect if user is not authenticated. */
    protected String loginAction;

    public void setLoginAction(String loginAction) {
        this.loginAction = loginAction;
    }

    protected static final String URL_PATTERN = "%s?redirectAction=%s";

    @Override
    protected boolean doCheck(ActionInvocation invocation) {

        EchoBaseSession echoBaseSession = EchoBaseSession.getEchoBaseSession(
                invocation.getInvocationContext());

        boolean userLoggued = echoBaseSession.getUser() != null;

        if (!userLoggued) {

            boolean autoLogin =
                    EchoBaseApplicationContext.getApplicationContext(invocation.getInvocationContext())
                            .getConfiguration()
                            .getOptionAsBoolean(EchoBaseConfiguration.OPTION_AUTO_LOGIN);

            if (autoLogin) {
                // by-pass login

                if (log.isInfoEnabled()) {
                    log.info("AutotLogin with admin/admin user");
                }
                Login logAction = new Login();
                logAction.setSession(invocation.getInvocationContext().getSession());
                logAction.setEmail("admin");
                logAction.setPassword("admin");
                try {
                    logAction.execute();
                } catch (Exception eee) {
                    if (log.isErrorEnabled()) {
                        log.error("Could not auto-login", eee);
                    }
                }
                return true;
            }
        }
        if (!userLoggued) {
            if (log.isDebugEnabled()) {
                log.debug("User is not loggued, will redirect to home");
            }
        }
        return userLoggued;
    }

    @Override
    protected String getRedirectUrl() {

        return String.format(URL_PATTERN,
                             loginAction,
                             redirectAction
        );
    }

}
