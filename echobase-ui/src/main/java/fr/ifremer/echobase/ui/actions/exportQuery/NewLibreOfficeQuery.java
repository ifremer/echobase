/*
 * #%L
 * EchoBase :: UI
 * %%
 * Copyright (C) 2011 - 2012 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ifremer.echobase.ui.actions.exportQuery;

import fr.ifremer.echobase.services.service.exportquery.ExportQueryService;
import fr.ifremer.echobase.ui.actions.EchoBaseActionSupport;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.inject.Inject;

/**
 * To deal with a new libre office request.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 0.3
 */
public class NewLibreOfficeQuery extends EchoBaseActionSupport {

    private static final long serialVersionUID = 1L;

    /** Logger. */
    private static final Log log = LogFactory.getLog(NewLibreOfficeQuery.class);

    protected String libreOfficeQuery;

    public void setLibreOfficeQuery(String libreOfficeQuery) {
        this.libreOfficeQuery = libreOfficeQuery;
    }

    public String getLibreOfficeQuery() {
        return libreOfficeQuery;
    }

    protected String resultQuery;

    public void setResultQuery(String resultQuery) {
        this.resultQuery = resultQuery;
    }

    public String getResultQuery() {
        return resultQuery;
    }

    @Override
    public String execute() {

        if (log.isInfoEnabled()) {
            log.info("Incoming query = " + libreOfficeQuery);
        }

        resultQuery = exportQueryService.processLibreOfficeSqlQuery(libreOfficeQuery);

        if (log.isInfoEnabled()) {
            log.info("Processed query " + resultQuery);
        }
        return SUCCESS;
    }

    //------------------------------------------------------------------------//
    //-- Injected objects                                                     //
    //------------------------------------------------------------------------//

    @Inject
    protected transient ExportQueryService exportQueryService;
}
