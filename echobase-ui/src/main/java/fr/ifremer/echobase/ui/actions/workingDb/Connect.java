package fr.ifremer.echobase.ui.actions.workingDb;

/*
 * #%L
 * EchoBase :: UI
 * %%
 * Copyright (C) 2011 - 2012 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.opensymphony.xwork2.interceptor.annotations.InputConfig;
import fr.ifremer.echobase.entities.DriverType;
import fr.ifremer.echobase.persistence.EchoBaseEntityHelper;
import fr.ifremer.echobase.persistence.JdbcConfiguration;
import org.apache.commons.lang3.StringUtils;

import java.sql.SQLException;

/**
 * Connects to an existing working db.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.1
 */
public class Connect extends AbstractWorkingDbAction {

    private static final long serialVersionUID = 1L;

    protected String login;

    protected String password;

    protected JdbcConfiguration jdbcConf;

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @InputConfig(methodName = INPUT)
    @Override
    public String execute() throws Exception {

        String result = INPUT;

        try {
            EchoBaseEntityHelper.checkJdbcConnection(jdbcConf);
            getEchoBaseSession().initUserDb(jdbcConf, true);
            addFlashMessage(t("echobase.info.workingDbconfiguration.connected",
                              conf.getUrl()));
            result = SUCCESS;

        } catch (SQLException e) {
            jdbcConf = null;
            addFieldError(
                    "login",
                    t("echobase.error.workingDbConfiguration.couldNotConnect",
                      e.getMessage()));
            addFieldError(
                    "password",
                    t("echobase.error.workingDbConfiguration.couldNotConnect",
                      e.getMessage()));
        }

        return result;
    }

    @Override
    public void validate() {

        jdbcConf = JdbcConfiguration.newConfig(
                getConf().getDriverType(), getConf().getUrl(),
                login, password);

        // check connexion is ok
        try {
            EchoBaseEntityHelper.checkJdbcConnection(jdbcConf);
        } catch (SQLException e) {
            jdbcConf = null;
            addFieldError(
                    "login",
                    t("echobase.error.workingDbConfiguration.couldNotConnect",
                      e.getMessage()));
            addFieldError(
                    "password",
                    t("echobase.error.workingDbConfiguration.couldNotConnect",
                      e.getMessage()));
        }
    }

    @Override
    public void prepare() throws Exception {

        super.prepare();

        if (DriverType.H2.equals(conf.getDriverType()) &&
            StringUtils.isBlank(getPassword())) {

            // use default h2 login
            setLogin("sa");

            // use a default h2 password
            setPassword("sa");
        }

    }
}
