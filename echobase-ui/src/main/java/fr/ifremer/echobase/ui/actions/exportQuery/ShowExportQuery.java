/*
 * #%L
 * EchoBase :: UI
 * %%
 * Copyright (C) 2011 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ifremer.echobase.ui.actions.exportQuery;

import fr.ifremer.echobase.entities.ExportQuery;
import fr.ifremer.echobase.entities.ExportQueryImpl;
import fr.ifremer.echobase.services.service.exportquery.ExportQueryNotFoundException;
import fr.ifremer.echobase.services.service.exportquery.ExportQueryService;
import fr.ifremer.echobase.ui.actions.EchoBaseActionSupport;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.inject.Inject;
import java.util.Map;

/**
 * To show all {@link ExportQuery}.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 0.1
 */
public class ShowExportQuery extends EchoBaseActionSupport {

    private static final long serialVersionUID = 1L;

    /** Logger. */
    private static final Log log = LogFactory.getLog(ShowExportQuery.class);

    /** Selected query loaded from database if his id is not empty. */
    protected final ExportQuery query = new ExportQueryImpl();

    public ExportQuery getQuery() {
//        if (query == null) {
//            query = exportQueryService.newExportQuery();
//        }
        return query;
    }

    public boolean isNewQuery() {
        return false;
    }

    public boolean isQueryExists() {
        return false;
    }

    public boolean isCanUpdateQuery() {
        return false;
    }

    /** All available queries from database. */
    protected Map<String, String> queries;

    public Map<String, String> getQueries() {
        return queries;
    }

    @Override
    public String execute() throws ExportQueryNotFoundException {

        if (log.isInfoEnabled()) {
            log.info("Will show all queries");
        }

        queries = exportQueryService.loadSortAndDecorate(ExportQuery.class);

        if (queries.isEmpty()) {

            // no query saved
            addFlashMessage(t("echobase.info.no.sqlQuery.saved"));
        }
        return SUCCESS;
    }

    //------------------------------------------------------------------------//
    //-- Injected objects                                                     //
    //------------------------------------------------------------------------//

    @Inject
    protected transient ExportQueryService exportQueryService;
}
