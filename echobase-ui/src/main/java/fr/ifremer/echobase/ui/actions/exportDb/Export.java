/*
 * #%L
 * EchoBase :: UI
 * %%
 * Copyright (C) 2011 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ifremer.echobase.ui.actions.exportDb;

import fr.ifremer.echobase.services.service.exportdb.ExportDbConfiguration;
import fr.ifremer.echobase.services.service.exportdb.ExportDbService;
import fr.ifremer.echobase.ui.actions.AbstractWaitAndExecAction;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import static org.nuiton.i18n.I18n.t;

/**
 * Launch the complete db export.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 0.2
 */
public class Export extends AbstractWaitAndExecAction<ExportDbConfiguration, ExportDbService> {

    private static final long serialVersionUID = 1L;

    /** Logger. */
    private static final Log log = LogFactory.getLog(Export.class);

    public Export() {
        super(ExportDbConfiguration.class, ExportDbService.class);
    }

    @Override
    protected void startAction(ExportDbService service,
                               ExportDbConfiguration model) throws Exception {
        if (log.isInfoEnabled()) {
            log.info("Start export db to file " + model.getFileName());
        }

        service.doExport(model);
    }

    @Override
    protected String getSuccesMessage() {
        return t("echobase.info.exportDb.succeded");
    }

    @Override
    protected String getErrorMessage() {
        return t("echobase.info.exportDb.failed");
    }

    @Override
    public String getActionResumeTitle() {
        return t("echobase.legend.exportDb.resume");
    }

    @Override
    protected String getResultMessage(ExportDbConfiguration model) {
        String exportType = t(model.getExportDbMode().getI18nKey());
        String result = t("echobase.message.exportDb.result",
                          exportType,
                          model.getFileName(),
                          model.getActionTime());

        if (log.isInfoEnabled()) {
            log.info("Result: " + result);
        }
        return result;
    }

}
