/*
 * #%L
 * EchoBase :: UI
 * %%
 * Copyright (C) 2011 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ifremer.echobase.ui.actions.exportQuery;

import fr.ifremer.echobase.entities.EchoBaseUser;
import fr.ifremer.echobase.entities.ExportQuery;
import fr.ifremer.echobase.services.service.exportquery.ExportQueryNotFoundException;
import fr.ifremer.echobase.services.service.exportquery.ExportQueryService;
import fr.ifremer.echobase.ui.actions.EchoBaseActionSupport;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.inject.Inject;
import java.util.Map;

/**
 * To edit a {@link ExportQuery}.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 0.1
 */
public class EditExportQuery extends EchoBaseActionSupport {

    private static final long serialVersionUID = 1L;

    /** Logger. */
    private static final Log log = LogFactory.getLog(EditExportQuery.class);

    /** Selected query loaded from database if his id is not empty. */
    protected ExportQuery query;

    public ExportQuery getQuery() {
        if (query == null) {
            query = exportQueryService.newExportQuery();
        }
        return query;
    }

    public boolean isNewQuery() {
        return false;
    }

    public boolean isQueryExists() {
        return StringUtils.isNotEmpty(getQuery().getTopiaId());
    }

    protected boolean canUpdateQuery;

    public boolean isCanUpdateQuery() {
        return canUpdateQuery;
    }

    /** All available queries from database. */
    protected Map<String, String> queries;

    public Map<String, String> getQueries() {
        return queries;
    }

    @Override
    public String execute() throws ExportQueryNotFoundException {

        queries = exportQueryService.loadSortAndDecorate(ExportQuery.class);

        String selectedQueryId = getQuery().getTopiaId();


        // load query from database
        query = exportQueryService.getExportQuery(selectedQueryId);

        EchoBaseUser echoBaseUser =
                getEchoBaseSession().getUser();
        canUpdateQuery = echoBaseUser.isAdmin() ||
                         echoBaseUser.getEmail().equals(
                                 query.getLastModifiedUser());

        if (!canUpdateQuery) {

            // this user can not update selected query
            addFlashMessage(
                    t("echobase.info.sqlQuery.not.modifiable"));
        }

        // test query
        checkQuery();

        return SUCCESS;
    }

    private boolean checkQuery() {

        boolean result;
        try {
            exportQueryService.testSql(getQuery().getSqlQuery());
            result = true;
        } catch (Exception e) {
            Throwable cause = e.getCause();
            if (log.isWarnEnabled()) {
                log.warn("Invalid sql ", cause);
            }
            addFieldError("query.sqlQuery",
                          t("echobase.error.invalid.sql", cause.getMessage()));
            result = false;
        }
        return result;
    }

    //------------------------------------------------------------------------//
    //-- Injected objects                                                     //
    //------------------------------------------------------------------------//

    @Inject
    protected transient ExportQueryService exportQueryService;
}
