/*
 * #%L
 * EchoBase :: UI
 * %%
 * Copyright (C) 2011 - 2012 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ifremer.echobase.ui.actions.embeddedApplication;

import fr.ifremer.echobase.services.service.embeddedapplication.EmbeddedApplicationConfiguration;
import fr.ifremer.echobase.services.service.embeddedapplication.EmbeddedApplicationService;
import fr.ifremer.echobase.ui.actions.AbstractWaitAndExecAction;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Build the embedded application archive.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 0.2
 */
public class Build extends AbstractWaitAndExecAction<EmbeddedApplicationConfiguration, EmbeddedApplicationService> {

    private static final long serialVersionUID = 1L;

    /** Logger. */
    private static final Log log = LogFactory.getLog(Build.class);

    public Build() {
        super(EmbeddedApplicationConfiguration.class,
              EmbeddedApplicationService.class);
    }

    @Override
    protected String getSuccesMessage() {
        return t("echobase.info.createEmbedded.succeded");
    }

    @Override
    protected String getErrorMessage() {
        return t("echobase.info.createEmbedded.failed");
    }

    @Override
    protected String getResultMessage(EmbeddedApplicationConfiguration model) {
        return t("echobase.message.createEmbedded.result",
                 model.getActionTime());
    }

    @Override
    public String getActionResumeTitle() {
        return t("echobase.legend.createEmbedded.resume");
    }

    @Override
    protected void startAction(EmbeddedApplicationService service,
                               EmbeddedApplicationConfiguration model) throws Exception {
        if (log.isInfoEnabled()) {
            log.info("Start build of embedded application to file " +
                     model.getFileName());
        }

        service.createEmbeddedApplication(model);
    }

}
