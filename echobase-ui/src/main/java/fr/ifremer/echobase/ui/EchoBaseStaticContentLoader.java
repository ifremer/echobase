package fr.ifremer.echobase.ui;

/*
 * #%L
 * EchoBase :: UI
 * %%
 * Copyright (C) 2011 - 2013 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.base.Charsets;
import com.opensymphony.xwork2.util.logging.Logger;
import com.opensymphony.xwork2.util.logging.LoggerFactory;
import org.apache.commons.io.IOUtils;
import org.apache.struts2.dispatcher.DefaultStaticContentLoader;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.Charset;
import java.util.Calendar;


/**
 * To fix some javascript loading problem.
 *
 * Some javascript files have some strange first caracter (ckeditor for example) +
 * problem while loading charsets for french translate files.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.0
 */
public class EchoBaseStaticContentLoader extends DefaultStaticContentLoader {

    /** Logger. */
    private final Logger log =
            LoggerFactory.getLogger(EchoBaseStaticContentLoader.class);

    @Override
    protected void process(InputStream is,
                           String path,
                           HttpServletRequest request,
                           HttpServletResponse response) throws IOException {
        if (is != null) {
            Calendar cal = Calendar.getInstance();

            // check for if-modified-since, prior to any other headers
            long ifModifiedSince = 0;
            try {
                ifModifiedSince = request.getDateHeader("If-Modified-Since");
            } catch (Exception e) {
                log.warn("Invalid If-Modified-Since header value: '"
                         + request.getHeader("If-Modified-Since") + "', ignoring");
            }
            long lastModifiedMillis = lastModifiedCal.getTimeInMillis();
            long now = cal.getTimeInMillis();
            cal.add(Calendar.DAY_OF_MONTH, 1);
            long expires = cal.getTimeInMillis();

            if (ifModifiedSince > 0 && ifModifiedSince <= lastModifiedMillis) {
                // not modified, content is not sent - only basic
                // headers and status SC_NOT_MODIFIED
                response.setDateHeader("Expires", expires);
                response.setStatus(HttpServletResponse.SC_NOT_MODIFIED);
                is.close();
                return;
            }

            // set the content-type header
            String contentType = getContentType(path);
            if (contentType != null) {
                response.setContentType(contentType);
            }

            if (serveStaticBrowserCache) {
                // set heading information for caching static content
                response.setDateHeader("Date", now);
                response.setDateHeader("Expires", expires);
                response.setDateHeader("Retry-After", expires);
                response.setHeader("Cache-Control", "public");
                response.setDateHeader("Last-Modified", lastModifiedMillis);
            } else {
                response.setHeader("Cache-Control", "no-cache");
                response.setHeader("Pragma", "no-cache");
                response.setHeader("Expires", "-1");
            }

            try {
                copy(is, response.getOutputStream(), path);

            } finally {
                is.close();
            }
        }
    }

    protected void copy(InputStream input,
                        OutputStream output,
                        String path) throws IOException {

        if (path.contains(".js")) {
            String content = IOUtils.toString(input, Charset.defaultCharset());
            if (content.indexOf("/") == 1) {
                // fix nasty first strange caracter for ckeditor (only on firefox :()
                content = content.substring(1);
            }
            if (log.isDebugEnabled()) {
                log.info("Content:\n" + content);
            }
            // always want to have file in IS0 (even if we serve UTF8 files)
            IOUtils.write(content, output, Charsets.ISO_8859_1);
        } else {

            // no special tratment for other files
            IOUtils.copy(input, output);
        }
    }
}
