package fr.ifremer.echobase.ui.actions.removeData;

/*
 * #%L
 * EchoBase :: UI
 * %%
 * Copyright (C) 2011 - 2012 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.echobase.entities.EchoBaseUserPersistenceContext;
import fr.ifremer.echobase.services.service.removedata.RemoveDataConfiguration;
import fr.ifremer.echobase.services.service.removedata.RemoveDataService;
import fr.ifremer.echobase.ui.actions.AbstractWaitAndExecAction;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.inject.Inject;

/**
 * Delete a selected import Log.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.2
 */
public class Delete extends AbstractWaitAndExecAction<RemoveDataConfiguration, RemoveDataService> {

    private static final long serialVersionUID = 1L;

    /** Logger. */
    private static final Log log = LogFactory.getLog(Delete.class);

    @Inject
    protected transient EchoBaseUserPersistenceContext persistenceContext;

    public Delete() {
        super(RemoveDataConfiguration.class, RemoveDataService.class);
    }

    @Override
    protected String getSuccesMessage() {
        return t("echobase.info.removeData.succeded");
    }

    @Override
    protected String getErrorMessage() {
        return t("echobase.info.removeData.failed");
    }

    @Override
    protected String getResultMessage(RemoveDataConfiguration model) {
        String message = t("echobase.message.removeData.result",
                           model.getActionTime(), model.getResultMessage());
        if (log.isInfoEnabled()) {
            log.info("Result: " + message);
        }
        return message;
    }

    @Override
    public String getActionResumeTitle() {
        return t("echobase.legend.removeData.resume");
    }

    @Override
    protected void startAction(RemoveDataService service,
                               RemoveDataConfiguration model) throws Exception {

        String resultMessage = service.removeImport(
                model, getEchoBaseSession().getUser());
        model.setResultMessage(resultMessage);
    }

    @Override
    protected void closeAction(RemoveDataConfiguration model) throws Exception {
        destroyModel(model);
    }
}
