/*
 * #%L
 * EchoBase :: UI
 * %%
 * Copyright (C) 2011 - 2012 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ifremer.echobase.ui.actions.importData;

import com.opensymphony.xwork2.interceptor.annotations.InputConfig;
import fr.ifremer.echobase.entities.ImportType;
import fr.ifremer.echobase.entities.data.Voyage;
import fr.ifremer.echobase.entities.references.AreaOfOperation;
import fr.ifremer.echobase.entities.references.Mission;
import fr.ifremer.echobase.services.service.importdata.configurations.VoyageCommonsImportConfiguration;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.File;
import java.util.Map;

/**
 * Configure a common data (voyage / transit / transect) import.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 0.3
 */
public class ConfigureCommonImport extends AbstractConfigureImport<VoyageCommonsImportConfiguration> {

    private static final long serialVersionUID = 1L;

    /** Logger. */
    private static final Log log = LogFactory.getLog(ConfigureCommonImport.class);

    /** Universe of existing missions. */
    protected Map<String, String> missions;

    /** Universe of existing voyages. */
    protected Map<String, String> voyages;

    /** Universe of existing area of operations. */
    protected Map<String, String> areaOfOperations;

    /** Universe of possible import modes. */
    protected Map<String, String> importTypes;

    public ConfigureCommonImport() {
        super(VoyageCommonsImportConfiguration.class);
    }

    @Override
    protected VoyageCommonsImportConfiguration createModel() {
        return new VoyageCommonsImportConfiguration(getLocale());
    }

    @Override
    protected void prepareInputAction(VoyageCommonsImportConfiguration model) {
        missions = userDbPersistenceService.loadSortAndDecorate(Mission.class);
        voyages = userDbPersistenceService.loadSortAndDecorate(Voyage.class);
        areaOfOperations = userDbPersistenceService.loadSortAndDecorate(AreaOfOperation.class);
        importTypes = decoratorService.decorateEnums(ImportType.getCommonImportType());

        if (model.getImportType() == null) {

            model.setImportType(ImportType.COMMON_ALL);
        }
    }

    @InputConfig(methodName = "input")
    public String modeAll() throws Exception {

        return execute();
    }

    @InputConfig(methodName = "input")
    public String modeVoyage() throws Exception {

        return execute();
    }

    @InputConfig(methodName = "input")
    public String modeTransit() throws Exception {

        return execute();
    }

    @InputConfig(methodName = "input")
    public String modeTransect() throws Exception {

        return execute();
    }

    public Map<String, String> getMissions() {
        return missions;
    }

    public Map<String, String> getVoyages() {
        return voyages;
    }

    public Map<String, String> getAreaOfOperations() {
        return areaOfOperations;
    }

    public Map<String, String> getImportTypes() {
        return importTypes;
    }

    // mode Voyage

    public VoyageCommonsImportConfiguration getModelVoyage() {
        return getModel();
    }

    public void setModelVoyageVoyageFile(File file) {
        getModel().getVoyageFile().setFile(file);
    }

    public void setModelVoyageVoyageFileContentType(String contentType) {
        getModel().getVoyageFile().setContentType(contentType);
    }

    public void setModelVoyageVoyageFileFileName(String fileName) {
        getModel().getVoyageFile().setFileName(fileName);
    }

    // mode transit

    public VoyageCommonsImportConfiguration getModelTransit() {
        return getModel();
    }

    public void setModelTransitTransitFile(File file) {
        getModel().getTransitFile().setFile(file);
    }

    public void setModelTransitTransitFileContentType(String contentType) {
        getModel().getTransitFile().setContentType(contentType);
    }

    public void setModelTransitTransitFileFileName(String fileName) {
        getModel().getTransitFile().setFileName(fileName);
    }

    // mode transect


    public VoyageCommonsImportConfiguration getModelTransect() {
        return getModel();
    }

    public void setModelTransectTransectFile(File file) {
        getModel().getTransectFile().setFile(file);
    }

    public void setModelTransectTransectFileContentType(String contentType) {
        getModel().getTransectFile().setContentType(contentType);
    }

    public void setModelTransectTransectFileFileName(String fileName) {
        getModel().getTransectFile().setFileName(fileName);
    }

    // mode All

    public VoyageCommonsImportConfiguration getModelAll() {
        return getModel();
    }

    public void setModelAllVoyageFile(File file) {
        getModel().getVoyageFile().setFile(file);
    }

    public void setModelAllVoyageFileContentType(String contentType) {
        getModel().getVoyageFile().setContentType(contentType);
    }

    public void setModelAllVoyageFileFileName(String fileName) {
        getModel().getVoyageFile().setFileName(fileName);
    }

    public void setModelAllTransitFile(File file) {
        getModel().getTransitFile().setFile(file);
    }

    public void setModelAllTransitFileContentType(String contentType) {
        getModel().getTransitFile().setContentType(contentType);
    }

    public void setModelAllTransitFileFileName(String fileName) {
        getModel().getTransitFile().setFileName(fileName);
    }

    public void setModelAllTransectFile(File file) {
        getModel().getTransectFile().setFile(file);
    }

    public void setModelAllTransectFileContentType(String contentType) {
        getModel().getTransectFile().setContentType(contentType);
    }

    public void setModelAllTransectFileFileName(String fileName) {
        getModel().getTransectFile().setFileName(fileName);
    }

}
