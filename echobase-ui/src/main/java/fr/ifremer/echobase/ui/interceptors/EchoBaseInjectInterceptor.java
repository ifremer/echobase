package fr.ifremer.echobase.ui.interceptors;

/*
 * #%L
 * EchoBase :: UI
 * %%
 * Copyright (C) 2011 - 2013 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.base.Preconditions;
import com.opensymphony.xwork2.ActionInvocation;
import com.opensymphony.xwork2.interceptor.Interceptor;
import fr.ifremer.echobase.entities.EchoBaseInternalPersistenceContext;
import fr.ifremer.echobase.entities.EchoBaseUserPersistenceContext;
import fr.ifremer.echobase.entities.EchoBaseUserTopiaApplicationContext;
import fr.ifremer.echobase.services.EchoBaseService;
import fr.ifremer.echobase.services.EchoBaseServiceContext;
import fr.ifremer.echobase.services.EchobaseAieOC;
import fr.ifremer.echobase.ui.EchoBaseApplicationContext;
import fr.ifremer.echobase.ui.EchoBaseInternalDbTransactionFilter;
import fr.ifremer.echobase.ui.EchoBaseSession;
import fr.ifremer.echobase.ui.EchoBaseUserDbTransactionFilter;
import fr.ifremer.echobase.ui.actions.EchoBaseActionSupport;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.lang.reflect.Field;
import java.util.Locale;

public class EchoBaseInjectInterceptor implements Interceptor {

    private static final Log log =
            LogFactory.getLog(EchoBaseInjectInterceptor.class);

    private static final long serialVersionUID = 1L;

    @Override
    public void init() {

        if (log.isInfoEnabled()) {
            log.info("init " + this);
        }
    }

    @Override
    public String intercept(ActionInvocation invocation) throws Exception {

        Object action = invocation.getAction();

        if (action instanceof EchoBaseActionSupport) {
            EchoBaseActionSupport echoBaseActionSupport = (EchoBaseActionSupport) action;

            EchoBaseServiceContext serviceContext = newServiceContext(
                    invocation,
                    echoBaseActionSupport.getLocale());

            ActionEchobaseAieOC injector = new ActionEchobaseAieOC(invocation);
            injector.inject(serviceContext, action);
        }
        return invocation.invoke();
    }

    protected EchoBaseSession getEchoBaseSession(ActionInvocation invocation) {

        return EchoBaseSession.getEchoBaseSession(
                invocation.getInvocationContext());

    }

    protected EchoBaseApplicationContext getEchoBaseApplicationContext(ActionInvocation invocation) {

        EchoBaseApplicationContext applicationContext =
                EchoBaseApplicationContext.getApplicationContext(
                        invocation.getInvocationContext());

        Preconditions.checkNotNull(
                "application context must be initialized before calling an action",
                applicationContext);

        return applicationContext;

    }

    protected EchoBaseServiceContext newServiceContext(ActionInvocation invocation,
                                                       Locale locale) {


        EchoBaseInternalPersistenceContext topiaInternalContext =
                EchoBaseInternalDbTransactionFilter.getPersistenceContext(
                        invocation.getInvocationContext());

        EchoBaseUserPersistenceContext topiaContext =
                EchoBaseUserDbTransactionFilter.getPersistenceContext(
                        invocation.getInvocationContext());

        EchoBaseApplicationContext applicationContext =
                getEchoBaseApplicationContext(invocation);

        EchoBaseSession echoBaseSession = getEchoBaseSession(invocation);
        EchoBaseUserTopiaApplicationContext workingDbRootContext =
                echoBaseSession.getUserDbApplicationContext();

        return applicationContext.newServiceContext(locale,
                                            topiaInternalContext,
                                            topiaContext,
                                            workingDbRootContext);

    }

    @Override
    public void destroy() {

        if (log.isInfoEnabled()) {
            log.info("destroy " + this);
        }

    }

    protected class ActionEchobaseAieOC extends EchobaseAieOC {

        private final ActionInvocation invocation;

        public ActionEchobaseAieOC(ActionInvocation invocation) {
            this.invocation= invocation;
        }

        @Override
        protected Object toInject(EchoBaseServiceContext serviceContext,
                                  Field field) {

            Class<?> propertyType = field.getType();

            Object toInject = null;
            if (EchoBaseUserPersistenceContext.class.isAssignableFrom(propertyType)) {
                toInject = serviceContext.getEchoBaseUserPersistenceContext();

            } else if (EchoBaseServiceContext.class.isAssignableFrom(propertyType)) {
                toInject = serviceContext;

            } else if (EchoBaseService.class.isAssignableFrom(propertyType)) {

                Class<? extends EchoBaseService> serviceClass =
                        (Class<? extends EchoBaseService>) propertyType;
                toInject = serviceContext.newService(serviceClass);

            } else if (EchoBaseSession.class.isAssignableFrom(propertyType)) {

                toInject = getEchoBaseSession(invocation);

            } else if (EchoBaseApplicationContext.class.isAssignableFrom(propertyType)) {

                toInject = getEchoBaseApplicationContext(invocation);
            }

            return toInject;
        }
    }
}
