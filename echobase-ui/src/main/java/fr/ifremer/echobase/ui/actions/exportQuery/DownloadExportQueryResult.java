/*
 * #%L
 * EchoBase :: UI
 * %%
 * Copyright (C) 2011 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ifremer.echobase.ui.actions.exportQuery;

import fr.ifremer.echobase.entities.ExportQuery;
import fr.ifremer.echobase.services.service.exportquery.ExportQueryService;
import fr.ifremer.echobase.ui.actions.EchoBaseActionSupport;

import javax.inject.Inject;
import java.io.ByteArrayInputStream;
import java.io.InputStream;

/**
 * Download the result of sql request in csv format.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 0.1
 */
public class DownloadExportQueryResult extends EchoBaseActionSupport {

    private static final long serialVersionUID = 1L;

    /**
     * Id of the query to call.
     *
     * @see ExportQuery#getTopiaId()
     * @since 1.4
     */
    protected String queryId;

    public void setQueryId(String queryId) {
        this.queryId = queryId;
    }

    /** Input stream of the file to download. */
    protected transient InputStream inputStream;

    public InputStream getInputStream() {
        return inputStream;
    }

    /** File name of the download. */
    protected String fileName;

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getFileName() {
        return fileName;
    }

    /** Length of the file to download. */
    protected int contentLength;

    public int getContentLength() {
        return contentLength;
    }

    /** Content type of the file to download. */
    protected String contentType;

    public String getContentType() {
        return contentType;
    }

    @Override
    public String execute() throws Exception {

        contentType = "text/csv";
        String content = exportQueryService.createCsvFileContent(queryId);
        byte[] bytes = content.getBytes();
        contentLength = bytes.length;
        inputStream = new ByteArrayInputStream(bytes);

        return SUCCESS;
    }

    //------------------------------------------------------------------------//
    //-- Injected objects                                                     //
    //------------------------------------------------------------------------//

    @Inject
    protected transient ExportQueryService exportQueryService;
}
