/*
 * #%L
 * EchoBase :: UI
 * %%
 * Copyright (C) 2011 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ifremer.echobase.ui.validators;

import org.nuiton.validator.xwork2.field.NuitonFieldValidatorSupport;

import java.util.Arrays;

/**
 * Base EchoBase validator.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 0.1
 */
public abstract class EchoBaseFieldValidatorSupport extends NuitonFieldValidatorSupport {

    /**
     * Add directly a field error with the given sentence wihtout any translation from the xml validator file.
     *
     * @param propertyName name of the property where to push the error
     * @param error        the error
     */
    protected void addFieldError(String propertyName, String error) {
        getValidatorContext().addFieldError(propertyName, error);
    }

    /**
     * Translate the given i18n key with his optional arguments.
     *
     * <strong>Note:</strong> This method name is fixed to be detected via the
     * nuiton i18n system, do NOT change this method name.
     *
     * @param key  the i18n key to translate
     * @param args the optional arguments of the sentence
     * @return the translated sentence
     */
    protected String translate(String key, Object... args) {
        return getValidatorContext().getText(key, Arrays.asList(args));
    }
}
