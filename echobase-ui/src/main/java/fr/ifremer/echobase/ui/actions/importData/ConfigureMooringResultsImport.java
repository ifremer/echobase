/*
 * #%L
 * EchoBase :: UI
 * %%
 * Copyright (C) 2011 - 2012 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ifremer.echobase.ui.actions.importData;

import com.opensymphony.xwork2.interceptor.annotations.InputConfig;
import fr.ifremer.echobase.entities.ImportType;
import fr.ifremer.echobase.entities.data.Mooring;
import fr.ifremer.echobase.services.service.importdata.configurations.MooringResultsImportConfiguration;

import java.io.File;
import java.util.Map;

/**
 * Configure a "results" import for mooring.
 *
 * @author Julien Ruchaud - ruchaud@codelutin.com
 * @since 4.0
 */
public class ConfigureMooringResultsImport extends AbstractConfigureImport<MooringResultsImportConfiguration> {

    private static final long serialVersionUID = 1L;

    /** Universe of existing moorings. */
    protected Map<String, String> moorings;

    /** Universe of possible import modes. */
    protected Map<String, String> importTypes;

    protected String resultLabel;

    public ConfigureMooringResultsImport() {
        super(MooringResultsImportConfiguration.class);
    }

    @Override
    protected MooringResultsImportConfiguration createModel() {
        return new MooringResultsImportConfiguration(getLocale());
    }

    @Override
    protected void prepareInputAction(MooringResultsImportConfiguration model) {
        moorings = userDbPersistenceService.loadSortAndDecorate(Mooring.class);
        importTypes = decoratorService.decorateEnums(ImportType.getMooringResultImportType());

        if (model.getImportType() == null) {
            model.setImportType(ImportType.RESULT_MOORING);
        }
    }

    @InputConfig(methodName = "input")
    public String modeMooring() throws Exception {
        return execute();
    }

    @InputConfig(methodName = "input")
    public String modeMooringEsdu() throws Exception {
        return execute();
    }

    public Map<String, String> getMoorings() {
        return moorings;
    }

    public Map<String, String> getImportTypes() {
        return importTypes;
    }


    public String getResultLabel() {
        return resultLabel;
    }

    public void setResultLabel(String resultLabel) {
        this.resultLabel = resultLabel;
    }

    public void setEchotypeFile(File file) {
        getModel().getEchotypeFile().setFile(file);
    }

    public void setEchotypeFileContentType(String contentType) {
        getModel().getEchotypeFile().setContentType(contentType);
    }

    public void setEchotypeFileFileName(String fileName) {
        getModel().getEchotypeFile().setFileName(fileName);
    }

    public void setEsduByEchotypeFile(File file) {
        getModel().getEsduByEchotypeFile().setFile(file);
    }

    public void setEsduByEchotypeFileContentType(String contentType) {
        getModel().getEsduByEchotypeFile().setContentType(contentType);
    }

    public void setEsduByEchotypeFileFileName(String fileName) {
        getModel().getEsduByEchotypeFile().setFileName(fileName);
    }

    public void setEsduByEchotypeAndSpeciesCategoryFile(File file) {
        getModel().getEsduByEchotypeAndSpeciesCategoryFile().setFile(file);
    }

    public void setEsduByEchotypeAndSpeciesCategoryFileContentType(String contentType) {
        getModel().getEsduByEchotypeAndSpeciesCategoryFile().setContentType(contentType);
    }

    public void setEsduByEchotypeAndSpeciesCategoryFileFileName(String fileName) {
        getModel().getEsduByEchotypeAndSpeciesCategoryFile().setFileName(fileName);
    }

}
