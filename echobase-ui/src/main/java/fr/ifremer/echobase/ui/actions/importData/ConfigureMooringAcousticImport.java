/*
 * #%L
 * EchoBase :: UI
 * %%
 * Copyright (C) 2011 - 2012 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ifremer.echobase.ui.actions.importData;

import fr.ifremer.echobase.entities.data.Mooring;
import fr.ifremer.echobase.services.service.importdata.CellPositionReference;
import fr.ifremer.echobase.services.service.importdata.configurations.MooringAcousticsImportConfiguration;

import java.io.File;
import java.util.Map;

/**
 * Configure a accoustic data import for mooring.
 *
 * @author Julien Ruchaud - ruchaud@codelutin.com
 * @since 0.5
 */
public class ConfigureMooringAcousticImport extends AbstractConfigureImport<MooringAcousticsImportConfiguration> {

    private static final long serialVersionUID = 1L;

    /** Universe of existing moorings. */
    protected Map<String, String> moorings;

    protected Map<String, String> cellPositionReferences;

    public ConfigureMooringAcousticImport() {
        super(MooringAcousticsImportConfiguration.class);
    }

    @Override
    protected MooringAcousticsImportConfiguration createModel() {
        return new MooringAcousticsImportConfiguration(getLocale());
    }

    @Override
    protected void prepareInputAction(MooringAcousticsImportConfiguration model) {
        moorings = userDbPersistenceService.loadSortAndDecorate(Mooring.class);
        cellPositionReferences = decoratorService.decorateEnums(CellPositionReference.values());

        if (model.getCellPositionReference() == null) {
            model.setCellPositionReference(CellPositionReference.START);
        }
    }

    public Map<String, String> getMoorings() {
        return moorings;
    }

    public Map<String, String> getCellPositionReferences() {
        return cellPositionReferences;
    }

    public void setMoviesFile(File file) {
        getModel().getMoviesFile().setFile(file);
    }

    public void setMoviesFileContentType(String contentType) {
        getModel().getMoviesFile().setContentType(contentType);
    }

    public void setMoviesFileFileName(String fileName) {
        getModel().getMoviesFile().setFileName(fileName);
    }
}
