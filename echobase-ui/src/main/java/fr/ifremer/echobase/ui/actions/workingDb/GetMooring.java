package fr.ifremer.echobase.ui.actions.workingDb;

/*
 * #%L
 * EchoBase :: UI
 * %%
 * Copyright (C) 2011 - 2012 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.echobase.entities.EchoBaseUserEntityEnum;
import fr.ifremer.echobase.entities.ImportLog;
import fr.ifremer.echobase.entities.data.Mooring;
import fr.ifremer.echobase.entities.references.Mission;
import fr.ifremer.echobase.services.service.DbEditorService;
import fr.ifremer.echobase.services.service.DecoratorService;
import fr.ifremer.echobase.services.service.UserDbPersistenceService;
import fr.ifremer.echobase.ui.actions.EchoBaseActionSupport;
import org.nuiton.topia.persistence.metadata.TableMeta;

import javax.inject.Inject;
import java.util.Map;

/**
 * Obtain details of a given {@link ImportLog}.
 *
 * @author Julien Ruchaud - ruchaud@codelutin.com
 * @since 4.0
 */
public class GetMooring extends EchoBaseActionSupport {

    private static final long serialVersionUID = 1L;

    protected String mooringId;

    public void setMooringId(String mooringId) {
        this.mooringId = mooringId;
    }

    protected Map data;

    public Map<?, ?> getData() {
        return data;
    }

    @Override
    public String execute() throws Exception {

        TableMeta<EchoBaseUserEntityEnum> tableMeta = dbEditorService.getTableMeta(EchoBaseUserEntityEnum.Mooring);
        data = dbEditorService.getData(tableMeta, mooringId);

        // decorate foreign keys
        Mission mission = userDbPersistenceService.getMission((String) data.get(Mooring.PROPERTY_MISSION));
        decoratorService.decorateForeignKey(data, Mooring.PROPERTY_MISSION, mission, null);

        return SUCCESS;
    }

    //------------------------------------------------------------------------//
    //-- Injected objects                                                     //
    //------------------------------------------------------------------------//

    @Inject
    protected transient UserDbPersistenceService userDbPersistenceService;
    @Inject
    protected transient DecoratorService decoratorService;
    @Inject
    protected transient DbEditorService dbEditorService;
}
