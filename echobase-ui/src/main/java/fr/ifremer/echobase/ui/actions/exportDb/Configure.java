/*
 * #%L
 * EchoBase :: UI
 * %%
 * Copyright (C) 2011 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ifremer.echobase.ui.actions.exportDb;

import fr.ifremer.echobase.entities.data.Voyage;
import fr.ifremer.echobase.services.service.exportdb.ExportDbConfiguration;
import fr.ifremer.echobase.services.service.exportdb.ExportDbMode;
import fr.ifremer.echobase.ui.actions.AbstractConfigureAction;
import org.apache.commons.io.FileUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.util.FileUtil;
import org.nuiton.util.StringUtil;

import java.io.File;
import java.io.IOException;
import java.util.Map;
import java.util.Set;

/**
 * Configure the complete db export.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 0.2
 */
public class Configure extends AbstractConfigureAction<ExportDbConfiguration> {

    private static final long serialVersionUID = 1L;

    /** Logger. */
    private static final Log log = LogFactory.getLog(Configure.class);

    /** Universe of possible export modes. */
    protected Map<String, String> modes;

    /** Universe of voyages to export in db. */
    protected Map<String, String> voyages;

    public Configure() {
        super(ExportDbConfiguration.class);
    }

    public Map<String, String> getModes() {
        return modes;
    }

    public Map<String, String> getVoyages() {
        return voyages;
    }

    public void setFileName(String fileName) {
        getModel().setFileName(fileName);
    }

    @Override
    protected ExportDbConfiguration createModel() {
        ExportDbConfiguration result = new ExportDbConfiguration();
        result.setFileName("echobase");
        result.setComputeSteps(true);
        result.setVoyageIds(StringUtil.EMPTY_STRING_ARRAY);
        return result;
    }

    @Override
    protected void prepareInputAction(ExportDbConfiguration model) {

        modes = decoratorService.decorateEnums(ExportDbMode.values());

        if (model.getExportDbMode() == null) {

            model.setExportDbMode(ExportDbMode.ALL);
        }

        voyages = userDbPersistenceService.loadSortAndDecorate(Voyage.class);
    }

    @Override
    protected void prepareExecuteAction(ExportDbConfiguration model) throws IOException {

        File tempDirectory = FileUtils.getTempDirectory();
        File dataDirectory = new File(tempDirectory,
                                      "echobase-exportDb-" +
                                      System.currentTimeMillis());
        FileUtil.createDirectoryIfNecessary(dataDirectory);
        model.setWorkingDirectory(dataDirectory);
        if (log.isInfoEnabled()) {
            log.info("Temporary directory to use : " + dataDirectory);
        }

        if (model.getExportDbMode() == ExportDbMode.ALL) {
            voyages = userDbPersistenceService.loadSortAndDecorate(Voyage.class);
            Set<String> ids = voyages.keySet();
            model.setVoyageIds(ids.toArray(new String[ids.size()]));
        }
    }
}
