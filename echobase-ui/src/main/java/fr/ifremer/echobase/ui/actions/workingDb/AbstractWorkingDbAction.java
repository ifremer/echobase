package fr.ifremer.echobase.ui.actions.workingDb;

/*
 * #%L
 * EchoBase :: UI
 * %%
 * Copyright (C) 2011 - 2012 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.opensymphony.xwork2.Preparable;
import fr.ifremer.echobase.entities.DriverType;
import fr.ifremer.echobase.entities.WorkingDbConfiguration;
import fr.ifremer.echobase.services.service.DecoratorService;
import fr.ifremer.echobase.services.service.exportquery.ExportQueryService;
import fr.ifremer.echobase.services.service.workingDb.WorkingDbConfigurationService;
import fr.ifremer.echobase.ui.actions.EchoBaseActionSupport;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.interceptor.ParameterAware;

import javax.inject.Inject;
import java.util.Map;

/**
 * Connects to an existing working db.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.1
 */
public abstract class AbstractWorkingDbAction extends EchoBaseActionSupport implements Preparable, ParameterAware {

    private static final long serialVersionUID = 1L;

    /** Logger. */
    private static final Log log =
            LogFactory.getLog(AbstractWorkingDbAction.class);

    /** All available working db configuration from database. */
    protected Map<String, String> confs;

    /** All available driver types. */
    protected Map<String, String> driverTypes;

    /** Current working db configuration. */
    protected WorkingDbConfiguration conf;

    public boolean isNewConf() {
        return false;
    }

    public final Map<String, String> getConfs() {
        return confs;
    }

    public final WorkingDbConfiguration getConf() {
        if (conf == null) {
            conf = workingDbConfigurationService.newConfiguration();
        }
        return conf;
    }

    public Map<String, String> getDriverTypes() {
        return driverTypes;
    }

    public final boolean isConfExists() {
        return StringUtils.isNotBlank(getConf().getTopiaId());
    }

    @Override
    public void prepare() throws Exception {

        driverTypes = decoratorService.decorateEnums(DriverType.values());

        confs = exportQueryService.loadSortAndDecorate(WorkingDbConfiguration.class);

        if (confs.isEmpty()) {

            // no conf saved
            addFlashMessage(
                    t("echobase.info.no.workingDbConfiguration.saved"));
        }

        String[] ids = parameters.get("conf.topiaId");
        String id = ids == null || ids.length == 0 ? null : ids[0];
        if (!StringUtils.isEmpty(id)) {

            // load conf
            conf = workingDbConfigurationService.getEditableConf(id);
            if (log.isInfoEnabled()) {
                log.info("Selected conf " + conf.getUrl());
            }
        }
    }

    //------------------------------------------------------------------------//
    //-- Injected objects                                                     //
    //------------------------------------------------------------------------//

    private transient Map<String, String[]> parameters;

    @Override
    public void setParameters(Map<String, String[]> parameters) {
        this.parameters = parameters;
    }

    @Inject
    protected transient WorkingDbConfigurationService workingDbConfigurationService;
    @Inject
    protected transient DecoratorService decoratorService;
    @Inject
    protected transient ExportQueryService exportQueryService;
}
