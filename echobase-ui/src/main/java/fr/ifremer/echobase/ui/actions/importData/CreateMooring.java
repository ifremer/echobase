/*
 * #%L
 * EchoBase :: UI
 * %%
 * Copyright (C) 2011 - 2012 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ifremer.echobase.ui.actions.importData;

import com.google.common.base.Preconditions;
import com.opensymphony.xwork2.interceptor.annotations.InputConfig;
import fr.ifremer.echobase.entities.data.Mooring;
import fr.ifremer.echobase.entities.data.MooringImpl;
import fr.ifremer.echobase.entities.references.Mission;
import fr.ifremer.echobase.services.service.UserDbPersistenceService;
import fr.ifremer.echobase.services.service.importdata.MooringCodeAlreadyExistException;
import fr.ifremer.echobase.services.service.importdata.MooringService;
import fr.ifremer.echobase.ui.actions.EchoBaseActionSupport;
import java.util.Map;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.inject.Inject;

/**
 * To create a new mooring
 *
 * @author Julien Ruchaud - ruchaud@codelutin.com
 * @since 0.5
 */
public class CreateMooring extends EchoBaseActionSupport {

    private static final long serialVersionUID = 1L;

    /** Logger. */
    private static final Log log = LogFactory.getLog(CreateMooring.class);

    /** Mooring to create. */
    protected Mooring mooring;
    
    protected String missionId;

    public Mooring getMooring() {
        if (mooring == null) {
            mooring = new MooringImpl();
        }
        return mooring;
    }
    
    /** Universe of existing missions. */
    protected Map<String, String> missions;

    public Map<String, String> getMissions() {
        return missions;
    }

    public void setMissionId(String missionId) {
        this.missionId = missionId;
    }

    public String getMissionId() {
        return missionId;
    }
    
    @Override
    public final String input() throws Exception {
        missions = userDbPersistenceService.loadSortAndDecorate(Mission.class);
        return INPUT;
    }
    
    @InputConfig(methodName = "input")
    @Override
    public String execute() throws Exception {

        Preconditions.checkNotNull(mooring);

        String result = INPUT;

        try {
            Mission mission = userDbPersistenceService.getMission(this.missionId);
            mooring.setMission(mission);
            Mooring mooringSaved = mooringService.createMooring(mooring);

            if (log.isInfoEnabled()) {
                log.info("Created mooring : " + mooringSaved.getTopiaId());
            }
            addFlashMessage(
                    t("echobase.information.mooring.created", mooringSaved.getCode()));
            result = SUCCESS;
        } catch (MooringCodeAlreadyExistException e) {
            addFieldError("mooring.code",
                          t("echobase.error.mooring.code.already.exist"));
        }
        return result;
    }

    //------------------------------------------------------------------------//
    //-- Injected objects                                                     //
    //------------------------------------------------------------------------//

    @Inject
    protected transient MooringService mooringService;
    
    @Inject
    protected transient UserDbPersistenceService userDbPersistenceService;
}
