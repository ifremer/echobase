#! /bin/sh
#mvn package
cp target/echobase-full-*.war /var/local/echobase
v=$( mvn help:evaluate -Dexpression=project.version | grep -v "INFO" | grep -v "WARNING" | grep -v "Download" )
echo "version=$v"
(cd /var/local/echobase; ./deploy.sh $v )