/*
 * #%L
 * EchoBase :: Domain
 * %%
 * Copyright (C) 2011 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ifremer.echobase;

import fr.ifremer.echobase.entities.EntityModificationLog;
import fr.ifremer.echobase.entities.EntityModificationLogImpl;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.beanutils.ConvertUtils;
import org.apache.commons.beanutils.Converter;
import org.junit.Assert;
import org.junit.Test;

import java.util.Calendar;
import java.util.Date;

/**
 * Tests the {@link EchoBaseDateConverter}.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 0.1
 */
public class EchoBaseDateConverterTest {

    public static final String DATE = "2011-11-08T20:54:59";

    @Test
    public void testConvert() throws Exception {
        EchoBaseDateConverter converter = new EchoBaseDateConverter();
        Object date = converter.convert(Date.class, DATE);
        assertDate(date);
    }

    // This test is not isolated and can failed... In fact we just need to
    // make sure next test is ok, not this one...
//    @Test(expected = ConversionException.class)
//    public void testConvertFromDefaultBeanUtilsConverter() throws Exception {
//
//        BeanUtils.setProperty(
//                new EntityModificationLogImpl(),
//                EntityModificationLog.PROPERTY_MODIFICATION_DATE, DATE);
//    }

    @Test
    public void testConvertFromBeanUtilsConverterWithOur() throws Exception {

        Converter oldDateConverter = EchoBaseDateConverter.initDateConverter();
        try {
            EntityModificationLog bean = new EntityModificationLogImpl();
            BeanUtils.setProperty(bean, EntityModificationLog.PROPERTY_MODIFICATION_DATE, DATE);
            Date date = bean.getModificationDate();
            assertDate(date);
        } finally {
            if (oldDateConverter != null) {
                ConvertUtils.deregister(Date.class);
                ConvertUtils.register(oldDateConverter, Date.class);
            }
        }
    }

    private void assertDate(Object date) {
        Assert.assertNotNull(date);
        Assert.assertTrue(date instanceof Date);
        Calendar calendar = Calendar.getInstance();
        calendar.setTime((Date) date);
        Assert.assertEquals(2011, calendar.get(Calendar.YEAR));
        Assert.assertEquals(10, calendar.get(Calendar.MONTH));
        Assert.assertEquals(8, calendar.get(Calendar.DAY_OF_MONTH));
        Assert.assertEquals(20, calendar.get(Calendar.HOUR_OF_DAY));
        Assert.assertEquals(54, calendar.get(Calendar.MINUTE));
        Assert.assertEquals(59, calendar.get(Calendar.SECOND));
    }

}
