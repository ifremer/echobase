/*
 * #%L
 * EchoBase :: Domain
 * %%
 * Copyright (C) 2011 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ifremer.echobase.persistence;

import com.google.common.collect.Lists;
import fr.ifremer.echobase.entities.EchoBaseUserEntityEnum;
import fr.ifremer.echobase.entities.data.Voyage;
import fr.ifremer.echobase.entities.references.Mission;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.nuiton.topia.persistence.TopiaEntity;

import java.util.ArrayList;
import java.util.List;

/**
 * To test the class {@link EchoBaseDbMeta}.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 0.2
 */
public class EchoBaseDbMetaTest {


    protected EchoBaseDbMeta dbMetas;

    @Before
    public void setUp() throws Exception {
        dbMetas = EchoBaseDbMeta.newDbMeta();
    }


    @Test
    public void getReferenceTypes() {

        List<EchoBaseUserEntityEnum> types =
                dbMetas.getReferenceTypes();

        List<EchoBaseUserEntityEnum> expectedTypes =
                Lists.newArrayList(getContractsOf(Mission.class.getPackage()));

        Assert.assertNotNull(types);

        for (EchoBaseUserEntityEnum type : expectedTypes) {
            Assert.assertTrue("Missing type : " + type, types.contains(type));
        }
        Assert.assertEquals(expectedTypes.size(), types.size());
    }

    @Test
    public void getDataTypes() {

        List<EchoBaseUserEntityEnum> types =
                dbMetas.getDataTypes();

        List<EchoBaseUserEntityEnum> expectedTypes =
                Lists.newArrayList(getContractsOf(Voyage.class.getPackage()));

        Assert.assertNotNull(types);

        for (EchoBaseUserEntityEnum type : expectedTypes) {
            Assert.assertTrue("Missing type : " + type, types.contains(type));
        }
        Assert.assertEquals(expectedTypes.size(), types.size());
    }

    /**
     * Used to get all contract of a package
     *
     * @param entitiesPackage package contening desired entities
     * @return contracts contained in entitiesPackage
     */
    public static EchoBaseUserEntityEnum[] getContractsOf(Package entitiesPackage) {
        EchoBaseUserEntityEnum[] echoBaseEntityEnums = EchoBaseUserEntityEnum.values();
        List<EchoBaseUserEntityEnum> refClasses = new ArrayList<>();
        for (EchoBaseUserEntityEnum echoBaseEntityEnum : echoBaseEntityEnums) {

            // Get all entities in package fr.ifremer.echobase.entities.references
            Class<? extends TopiaEntity> contract = echoBaseEntityEnum.getContract();
            if (entitiesPackage.equals(contract.getPackage())) {
                refClasses.add(echoBaseEntityEnum);
            }
        }
        return refClasses.toArray(new EchoBaseUserEntityEnum[refClasses.size()]);
    }
}
