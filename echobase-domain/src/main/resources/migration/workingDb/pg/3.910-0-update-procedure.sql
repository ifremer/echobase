---
-- #%L
-- EchoBase :: Domain
-- %%
-- Copyright (C) 2011 - 2016 Ifremer, Codelutin
-- %%
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Affero General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
-- 
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
-- 
-- You should have received a copy of the GNU Affero General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
-- #L%
---

CREATE OR REPLACE FUNCTION echobase_create_echobase_cell_spatial_row(
  cell_id          VARCHAR,
  coordinateText   VARCHAR,
  coordinate3dText VARCHAR,
  shapeText        VARCHAR)
  RETURNS VOID AS $$
DECLARE
  cellSpatialRow      RECORD;
  voyageId            VARCHAR;
  voyageName          VARCHAR;
  transitId           VARCHAR;
  transitName         VARCHAR;
  transectId          VARCHAR;
  transectName        VARCHAR;
  dataAcquisitionId   VARCHAR;
  dataAcquisitionName VARCHAR;
  dataProcessingId    VARCHAR;
  dataProcessingName  VARCHAR;
  cellName            VARCHAR;
  cellTypeId          VARCHAR;
  cellTypeName        VARCHAR;
  cellParentId        VARCHAR;
  cellRow             RECORD;
  coordinateData      GEOMETRY;
  coordinate3dData    GEOMETRY;
  shapeData           GEOMETRY;
BEGIN
  IF coordinateText IS NULL AND coordinate3dText IS NULL AND
     shapeText IS NULL
  THEN
    RAISE DEBUG 'Could not find spatial data for cell %', cell_id;
    RETURN;
  END IF;
  IF coordinateText IS NOT NULL
  THEN
    BEGIN
      coordinateData = ST_GeomFromEWKT(coordinateText);
      EXCEPTION WHEN internal_error
      THEN

        RAISE LOG 'Could not create coordinate spatial data %', coordinateText;
        RETURN;
    END;
  ELSEIF coordinate3dText IS NOT NULL
    THEN
      BEGIN
        coordinate3dData = ST_GeomFromEWKT(coordinate3dText);
        EXCEPTION WHEN internal_error
        THEN

          RAISE LOG 'Could not create coordinate3D spatial data %', coordinate3dText;
          RETURN;
      END;
  ELSEIF shapeText IS NOT NULL
    THEN
      BEGIN
        shapeData = ST_GeomFromEWKT(shapeText);
        EXCEPTION WHEN internal_error
        THEN

          RAISE LOG 'Could not create shape spatial data %', shapeText;
          RETURN;
      END;
  END IF;

  SELECT
    *
  INTO cellSpatialRow
  FROM echobase_cell_spatial cs
  WHERE cs.cellid = cell_id;
  IF NOT FOUND
  THEN
-- create row
    RAISE DEBUG 'Will create spatial cell %', cell_id;
    SELECT
      cell_id
    INTO cellParentId;
    LOOP
      IF dataProcessingId IS NULL
      THEN
-- try to get dataprocessingId from this cell
        SELECT
          dp.topiaid,
          dp.processingdescription
        INTO dataProcessingId
        FROM dataprocessing dp, cell c
        WHERE c.topiaid = cellParentId AND dp.topiaid = c.dataprocessing;
      END IF;
      SELECT
        topiaid,
        cell
      INTO cellRow
      FROM cell
      WHERE topiaid = cellParentId;
      EXIT WHEN cellRow.cell IS NULL;
      SELECT
        cellRow.cell
      INTO cellParentId;
    END LOOP;
    RAISE DEBUG 'use cell parentId %', cellParentId;
-- get cell infos
    SELECT
      c.name,
      ct.name,
      ct.topiaid
    INTO cellName, cellTypeName, cellTypeId
    FROM cell c, celltype ct
    WHERE c.topiaid = cell_id AND c.celltype = ct.topiaid;

    IF dataProcessingId IS NULL
    THEN
-- get voyage infos
      SELECT
        v.topiaid,
        v.name
      INTO voyageId, voyageName
      FROM voyage v, cell c
      WHERE c.topiaid = cellParentId AND v.topiaid = c.voyage;
    ELSE

-- get dataAcquisition infos
      SELECT
        da.topiaid,
        da.acousticinstrument
      INTO dataAcquisitionId, dataAcquisitionName
      FROM dataacquisition da, dataprocessing dp
      WHERE dp.topiaId = dataProcessingId AND da.topiaid = dp.dataacquisition;
-- get transect infos
      SELECT
        t.topiaid,
        t.vessel
      INTO transectId, transitName
      FROM transect t, dataacquisition da
      WHERE da.topiaid = dataAcquisitionId AND t.topiaid = da.transect;
-- get transit infos
      SELECT
        t.topiaid,
        (t.starttime || ' - ' || t.endtime)
      INTO transitId, transitName
      FROM transit t, transect tt
      WHERE tt.topiaid = transectId AND t.topiaid = tt.transit;
-- get voyage infos
      SELECT
        v.topiaid,
        v.name
      INTO voyageId, voyageName
      FROM voyage v, transit t
      WHERE t.topiaid = transitId AND v.topiaid = t.voyage;
    END IF;

    IF voyageId IS NOT NULL
    THEN
      INSERT INTO echobase_cell_spatial (lastUpdateDate, voyageid, voyagename, transitid, transitname, transectid, transectname, dataacquisitionid, dataacquisitionname, dataprocessingid, dataprocessingname, celltypeid, celltypename, cellid, cellname, coordinate, coordinate3d, shape)
        VALUES (now(), voyageId, voyageName, transitId, transitName, transectId, transectName, dataAcquisitionId, dataAcquisitionName, dataProcessingId, dataProcessingName, cellTypeId, cellTypeName, cell_id, cellName, coordinateData, coordinate3dData, shapeData);
    END IF;
  ELSE
-- update row
    RAISE DEBUG 'Will update spatial cell % ', cell_id;
    UPDATE echobase_cell_spatial
    SET coordinate = coordinateData,
      coordinate3d = coordinate3dData,
      shape = shapeData
    WHERE cellid = cell_id;
  END IF;
END
$$ LANGUAGE plpgsql;
