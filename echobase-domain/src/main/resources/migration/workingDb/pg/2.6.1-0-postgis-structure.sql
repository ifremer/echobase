---
-- #%L
-- EchoBase :: Domain
-- %%
-- Copyright (C) 2011 - 2014 Ifremer, Codelutin
-- %%
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Affero General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
-- 
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
-- 
-- You should have received a copy of the GNU Affero General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
-- #L%
---
--
-- echobase_delete_voyage trigger
--
DROP TRIGGER IF EXISTS echobase_delete_voyage ON voyage;

CREATE OR REPLACE FUNCTION echobase_delete_voyage()
  RETURNS TRIGGER AS $$
BEGIN

  RAISE DEBUG 'Delete voyage % , delete cascade in echobase_cell_spatial_table, echobase_operation_spatial', OLD.topiaid;

  DELETE FROM echobase_cell_spatial WHERE voyageid = OLD.topiaid;
  DELETE FROM echobase_operation_spatial WHERE voyageid = OLD.topiaid;

  RETURN OLD;
END
$$
LANGUAGE 'plpgsql';

CREATE TRIGGER echobase_delete_voyage
BEFORE DELETE ON voyage
FOR EACH ROW EXECUTE PROCEDURE echobase_delete_voyage();

--
-- echobase_delete_transit trigger
--
DROP TRIGGER IF EXISTS echobase_delete_transit ON transit;

CREATE OR REPLACE FUNCTION echobase_delete_transit()
  RETURNS TRIGGER AS $$
BEGIN

  RAISE DEBUG 'Delete transit % , delete cascade in echobase_cell_spatial_table, echobase_operation_spatial', OLD.topiaid;

  DELETE FROM echobase_cell_spatial WHERE transitid = OLD.topiaid;
  DELETE FROM echobase_operation_spatial WHERE transitid = OLD.topiaid;

  RETURN OLD;
END
$$
LANGUAGE 'plpgsql';

CREATE TRIGGER echobase_delete_transit
BEFORE DELETE ON transit
FOR EACH ROW EXECUTE PROCEDURE echobase_delete_transit();

--
-- echobase_delete_transect trigger
--
DROP TRIGGER IF EXISTS echobase_delete_transect ON transect;

CREATE OR REPLACE FUNCTION echobase_delete_transect()
  RETURNS TRIGGER AS $$
BEGIN

  RAISE DEBUG 'Delete transect % , delete cascade in echobase_cell_spatial_table, echobase_operation_spatial', OLD.topiaid;

  DELETE FROM echobase_cell_spatial WHERE transectid = OLD.topiaid;
  DELETE FROM echobase_operation_spatial WHERE transectid = OLD.topiaid;

  RETURN OLD;
END
$$
LANGUAGE 'plpgsql';

CREATE TRIGGER echobase_delete_transect
BEFORE DELETE ON transect
FOR EACH ROW EXECUTE PROCEDURE echobase_delete_transect();

--
-- echobase_delete_operation trigger
--
DROP TRIGGER IF EXISTS echobase_delete_operation ON operation;
CREATE OR REPLACE FUNCTION echobase_delete_operation()
  RETURNS TRIGGER AS $$
BEGIN

  RAISE DEBUG 'Delete operation % , delete cascade in echobase_operation_spatial_table', OLD.topiaid;

  DELETE FROM echobase_operation_spatial_temp WHERE operationid = OLD.topiaid;
  DELETE FROM echobase_operation_spatial WHERE operationid = OLD.topiaid;

  RETURN OLD;
END
$$
LANGUAGE 'plpgsql';

CREATE TRIGGER echobase_delete_operation
BEFORE DELETE ON operation
FOR EACH ROW EXECUTE PROCEDURE echobase_delete_operation();

--
-- echobase_delete_dataProcessing trigger
--
DROP TRIGGER IF EXISTS echobase_delete_dataProcessing ON dataprocessing;

CREATE OR REPLACE FUNCTION echobase_delete_dataProcessing()
  RETURNS TRIGGER AS $$
BEGIN

  RAISE DEBUG 'Delete dataprocessing % , delete cascade in echobase_cell_spatial_table', OLD.topiaid;

  DELETE FROM echobase_cell_spatial WHERE dataprocessingid = OLD.topiaid;

  RETURN OLD;
END
$$
LANGUAGE 'plpgsql';

CREATE TRIGGER echobase_delete_dataProcessing
BEFORE DELETE ON dataprocessing
FOR EACH ROW EXECUTE PROCEDURE echobase_delete_dataProcessing();

--
-- echobase_delete_dataAcquisition trigger
--
DROP TRIGGER IF EXISTS echobase_delete_dataAcquisition ON dataAcquisition;

CREATE OR REPLACE FUNCTION echobase_delete_dataAcquisition()
  RETURNS TRIGGER AS $$
BEGIN

  RAISE DEBUG 'Delete dataAcquisition % , delete cascade in echobase_cell_spatial_table', OLD.topiaid;

  DELETE FROM echobase_cell_spatial WHERE dataacquisitionid = OLD.topiaid;

  RETURN OLD;
END
$$
LANGUAGE 'plpgsql';

CREATE TRIGGER echobase_delete_dataAcquisition
BEFORE DELETE ON dataAcquisition
FOR EACH ROW EXECUTE PROCEDURE echobase_delete_dataAcquisition();

--
-- echobase_delete_cell trigger
--
DROP TRIGGER IF EXISTS echobase_delete_cell ON cell;

CREATE OR REPLACE FUNCTION echobase_delete_cell()
  RETURNS TRIGGER AS $$
BEGIN

  RAISE DEBUG 'Delete cell % , delete cascade in echobase_cell_spatial_table', OLD.topiaid;

  DELETE FROM echobase_cell_spatial_temp WHERE cellid = OLD.topiaid;
  DELETE FROM echobase_cell_spatial WHERE cellid = OLD.topiaid;

  RETURN OLD;
END
$$
LANGUAGE 'plpgsql';

CREATE TRIGGER echobase_delete_cell
BEFORE DELETE ON cell
FOR EACH ROW EXECUTE PROCEDURE echobase_delete_cell();

CREATE OR REPLACE FUNCTION echobase_create_echobase_operation_spatial_row(
  operation_id     VARCHAR,
  coordinateText   VARCHAR)
  RETURNS VOID AS $$
DECLARE
  operationSpatialRow RECORD;
  voyageId            VARCHAR;
  voyageName          VARCHAR;
  transitId           VARCHAR;
  transitName         VARCHAR;
  transectId          VARCHAR;
  transectName        VARCHAR;
  operationId         VARCHAR;
  operationName       VARCHAR;
  operationRow        RECORD;
  coordinateData      GEOMETRY;
BEGIN
  IF coordinateText IS NULL
  THEN
    RAISE DEBUG 'Could not find spatial data for operation %', operation_id;
    RETURN;
  END IF;
  BEGIN
    coordinateData = ST_GeomFromEWKT(coordinateText);
    EXCEPTION WHEN internal_error
    THEN
      RAISE LOG 'Could not create coordinate operation spatial data %', coordinateText;
      RETURN;
  END;

  SELECT * INTO operationSpatialRow FROM echobase_operation_spatial os
  WHERE os.operationid = operation_id;
  IF NOT FOUND
  THEN
-- create row
    RAISE DEBUG 'Will create spatial operation %', operation_id;
-- get operation infos
    SELECT op.id, op.topiaid
    INTO operationName, operationId
    FROM operation op
    WHERE op.topiaid = operation_id;
-- get transect infos
    SELECT
      t.topiaid,
      t.vessel
    INTO transectId, transitName
    FROM transect t, operation op
    WHERE op.topiaid = operation_id AND t.topiaid = op.transect;
-- get transit infos
    SELECT
      t.topiaid,
      (t.starttime || ' - ' || t.endtime)
    INTO transitId, transitName
    FROM transit t, transect tt
    WHERE tt.topiaid = transectId AND t.topiaid = tt.transit;
-- get voyage infos
    SELECT
      v.topiaid,
      v.name
    INTO voyageId, voyageName
    FROM voyage v, transit t
    WHERE t.topiaid = transitId AND v.topiaid = t.voyage;

    INSERT INTO echobase_operation_spatial (lastUpdateDate, voyageid, voyagename, transitid, transitname, transectid, transectname, operationid, operationname, coordinate)
      VALUES (now(), voyageId, voyageName, transitId, transitName, transectId, transectName, operationId, operationName, coordinateData);
  ELSE
-- update row
    RAISE DEBUG 'Will update spatial operation % ', operation_id;
    UPDATE echobase_operation_spatial AS eos
    SET coordinate = coordinateData
    WHERE eos.operationid = operation_id;
  END IF;
END
$$ LANGUAGE plpgsql;