---
-- #%L
-- EchoBase :: Domain
-- %%
-- Copyright (C) 2011 - 2015 Ifremer, Codelutin
-- %%
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Affero General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
-- 
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
-- 
-- You should have received a copy of the GNU Affero General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
-- #L%
---
ALTER TABLE IMPORTFILE ADD checkFile OID;
ALTER TABLE IMPORTFILE ADD importText text;
DROP TABLE IF EXISTS IMPORTFILE_IMPORTID;

CREATE TABLE IMPORTFILEID ( TOPIAID character varying(255) NOT NULL, TOPIAVERSION BIGINT NOT NULL, TOPIACREATEDATE TIMESTAMP, IMPORTFILE character varying(255) NOT NULL, ENTITYID character varying(255) NOT NULL, LINENUMBER INTEGER NOT NULL, IMPORTORDER INTEGER NOT NULL);

ALTER TABLE IMPORTFILEID ADD CONSTRAINT PK_IMPORTFILEID PRIMARY KEY(TOPIAID);
ALTER TABLE IMPORTFILEID ADD CONSTRAINT FK_IMPORTFILEID_IMPORTFILE FOREIGN KEY(IMPORTFILE) REFERENCES IMPORTFILE(TOPIAID);
CREATE UNIQUE INDEX UK_IMPORTFILEID ON IMPORTFILEID (ENTITYID);
CREATE INDEX IDX_IMPORTFILE_IMPORTFILEID ON IMPORTFILEID (IMPORTFILE);
