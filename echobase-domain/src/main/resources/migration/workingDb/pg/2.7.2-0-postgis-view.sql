---
-- #%L
-- EchoBase :: Domain
-- %%
-- Copyright (C) 2011 - 2015 Ifremer, Codelutin
-- %%
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Affero General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
-- 
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
-- 
-- You should have received a copy of the GNU Affero General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
-- #L%
---
DROP FUNCTION IF EXISTS echobase_refresh_views() CASCADE;

CREATE OR REPLACE FUNCTION echobase_refresh_views()
  RETURNS VOID AS $$
BEGIN
    RAISE NOTICE 'reload TotalSampleView';
    REFRESH MATERIALIZED VIEW TotalSampleView;
    REINDEX INDEX TotalSampleView_idx;

  RAISE NOTICE 'reload cellEsduViewEchotype';
    REFRESH MATERIALIZED VIEW cellEsduViewEchotype;
    REINDEX INDEX cellEsduViewEchotype_idx;

    RAISE NOTICE 'reload cellEsduViewSpecies';
    REFRESH MATERIALIZED VIEW cellEsduViewSpecies;
    REINDEX INDEX cellEsduViewSpecies_idx;

    RAISE NOTICE 'reload cellEsduViewSpeciesResultGrouped';
    REFRESH MATERIALIZED VIEW cellEsduViewSpeciesResultGrouped;
    REINDEX INDEX cellEsduViewSpeciesResultGrouped_idx;

    RAISE NOTICE 'reload cellEsduViewEchotypeNasc';
    REFRESH MATERIALIZED VIEW cellEsduViewEchotypeNasc;

    RAISE NOTICE 'reload cellEsduViewSpeciesBiomass';
    REFRESH MATERIALIZED VIEW cellEsduViewSpeciesBiomass;
    REINDEX INDEX cellEsduViewSpeciesBiomass_uidx;

    RAISE NOTICE 'reload cellEsduViewSpeciesAbundance';
    REFRESH MATERIALIZED VIEW cellEsduViewSpeciesAbundance;
    REINDEX INDEX cellEsduViewSpeciesAbundance_uidx;

    RAISE NOTICE 'reload cellEsduViewSpeciesMeanLength';
    REFRESH MATERIALIZED VIEW cellEsduViewSpeciesMeanLength;
    REINDEX INDEX cellEsduViewSpeciesMeanLength_uidx;

    RAISE NOTICE 'reload cellmapview';
    REFRESH MATERIALIZED VIEW cellmapview;
    REINDEX INDEX cellmapview_idx;
    REINDEX INDEX cellmapview_uidx;
END
$$
LANGUAGE 'plpgsql';