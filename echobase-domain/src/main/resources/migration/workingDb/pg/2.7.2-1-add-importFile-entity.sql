---
-- #%L
-- EchoBase :: Domain
-- %%
-- Copyright (C) 2011 - 2015 Ifremer, Codelutin
-- %%
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Affero General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
-- 
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
-- 
-- You should have received a copy of the GNU Affero General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
-- #L%
---
CREATE TABLE IMPORTFILE( topiaid character varying(255) NOT NULL,topiaversion BIGINT NOT NULL, topiacreatedate DATE, name character varying(255), file OID, importLog character varying(255));
ALTER TABLE IMPORTFILE ADD CONSTRAINT PK_IMPORTFILE PRIMARY KEY(TOPIAID);
ALTER TABLE IMPORTFILE ADD CONSTRAINT FK_IMPORTFILE_IMPORTLOG FOREIGN KEY(IMPORTLOG) REFERENCES IMPORTLOG(TOPIAID);
