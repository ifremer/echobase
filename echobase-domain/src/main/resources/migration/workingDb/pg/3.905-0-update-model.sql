---
-- #%L
-- EchoBase :: Domain
-- %%
-- Copyright (C) 2011 - 2016 Ifremer, Codelutin
-- %%
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Affero General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
-- 
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
-- 
-- You should have received a copy of the GNU Affero General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
-- #L%
---
-- PORT
create table port (topiaId character varying(255) not null, topiaVersion bigint not null, code character varying(255) not null, topiaCreateDate timestamp, name character varying(255) not null, shortName character varying(255), lastModification timestamp);

alter table port add constraint PK_PORT primary key (topiaId);
alter table port add constraint UK_PORT_CODE unique (code);

-- VOYAGE
alter table voyage add column code character varying(255);
alter table voyage add column northLimit real;
alter table voyage add column eastLimit real;
alter table voyage add column southLimit real;
alter table voyage add column westLimit real;
alter table voyage add column upLimit real;
alter table voyage add column downLimit real;
alter table voyage add column units character varying(255);
alter table voyage add column zUnits character varying(255);
alter table voyage add column comments character varying(255);

update voyage set northLimit = 0, eastLimit = 0, southLimit = 0, westLimit = 0, upLimit = 0, downLimit = 0;

-- Change ports name
alter table voyage rename column startPort to startPortName;
alter table voyage rename column endPort to endPortName;

alter table voyage add column startPort character varying(255);
alter table voyage add column endPort character varying(255);

alter table voyage add constraint FK_VOYAGE_STARTPORT foreign key (startPort) references port;
alter table voyage add constraint FK_VOYAGE_ENDPORT foreign key (endPort) references port;

-- ANCILLARY INSTRUMENTATION
create table ancillaryInstrumentation (topiaId character varying(255) not null, topiaVersion bigint not null, serialNumber character varying(255) not null, topiaCreateDate timestamp, name character varying(255) not null);

alter table ancillaryInstrumentation add constraint PK_ANCILLARYINST primary key (topiaId);
alter table ancillaryInstrumentation add constraint UK_ANCILLARYINST_SERIALNUMBER unique (serialNumber);

-- TRANSECT
alter table transect rename column comment to comments;
alter table transect add column relatedActivity character varying(255);
alter table transect add column units character varying(255);
alter table transect add column zUnits character varying(255);

create table ancillaryinstrumentation_transect (transect character varying(255) not null, ancillaryInstrumentation character varying(255) not null);
alter table ancillaryinstrumentation_transect add constraint FK_ANCILLARYINSTTRANSECT_ANCILLARYINST foreign key (ancillaryInstrumentation) references ancillaryInstrumentation;
alter table ancillaryinstrumentation_transect add constraint FK_ANCILLARYINSTTRANSECT_TRANSECT foreign key (transect) references transect;

-- VESSEL
alter table vessel add column breadth real;
alter table vessel add column comments character varying(255);

update vessel set breadth = 0;

-- MOORING
create table mooring (topiaId character varying(255) not null, topiaVersion bigint not null, code character varying(255) not null, topiaCreateDate timestamp, description character varying(255) not null, depth real, northLimit real, eastLimit real, southLimit real, westLimit real, upLimit real, downLimit real, units character varying(255), zUnits character varying(255), projection character varying(255), deploymentDate timestamp, retrievalDate timestamp, siteName character varying(255), operator character varying(255), comments character varying(255), mission character varying(255) not null);

alter table mooring add constraint PK_MOORING primary key (topiaId);
alter table mooring add constraint UK_MOORING_CODE unique (code);
alter table mooring add constraint FK_MOORING_MISSION foreign key (mission) references mission;

create table ancillaryinstrumentation_mooring (mooring character varying(255) not null, ancillaryInstrumentation character varying(255) not null);
alter table ancillaryinstrumentation_mooring add constraint FK_ANCILLARYINSTMOORING_ANCILLARYINST foreign key (ancillaryInstrumentation) references ancillaryInstrumentation;
alter table ancillaryinstrumentation_mooring add constraint FK_ANCILLARYINSTMOORING_MOORING foreign key (mooring) references mooring;

-- DATA ACQUISITION
alter table dataAcquisition add column softwareName character varying(255);
alter table dataAcquisition add column storedDataFormat character varying(255);
alter table dataAcquisition add column comments character varying(255);
alter table dataAcquisition add column mooring character varying(255);

alter table dataAcquisition add constraint FK_DATAACQUISITION_MOORING foreign key (mooring) references mooring;

-- ACOUSTIC INSTRUMENT
alter table acousticInstrument add column transducerFrequency real;
alter table acousticInstrument add column transducerPsi real;
alter table acousticInstrument add column transducerBeamAngleMajor real;
alter table acousticInstrument add column transducerBeamAngleMinor real;
alter table acousticInstrument add column transducerBeamManufactuer character varying(255);
alter table acousticInstrument add column comments character varying(255);

update acousticInstrument set transducerFrequency = 0, transducerPsi = 0, transducerBeamAngleMajor = 0, transducerBeamAngleMinor = 0;

-- DATA PROCESSING
alter table dataProcessing add column softwareName character varying(255);
alter table dataProcessing add column channelId character varying(255);
alter table dataProcessing add column bandWith real;
alter table dataProcessing add column frequency real;
alter table dataProcessing add column transceiverPower real;
alter table dataProcessing add column transmitPulseLength real;
alter table dataProcessing add column transceiverGainUnits character varying(255);
alter table dataProcessing add column comments character varying(255);

update dataProcessing set bandWith = 0, frequency = 0, transceiverPower = 0, transmitPulseLength = 0;

-- CALIBRATION
alter table calibration add column comments character varying(255);
