---
-- #%L
-- EchoBase :: Domain
-- %%
-- Copyright (C) 2011 - 2013 Ifremer, Codelutin
-- %%
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Affero General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
-- 
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
-- 
-- You should have received a copy of the GNU Affero General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
-- #L%
---
-- ajout de vues liés aux tables spatiales

DROP VIEW IF EXISTS echobase_cell_spatial_result;
CREATE OR REPLACE VIEW echobase_cell_spatial_result AS
  SELECT
    s.*,
    m.topiaId     AS metaDataId,
    m.name        AS metaDataName,
    r.resultvalue AS resultValue,
    r.topiaid     AS resultId
  FROM
    echobase_cell_spatial s,
    result r,
    datametadata m
  WHERE
    s.cellid = r.cell AND
    r.datametadata = m.topiaid;

DROP VIEW IF EXISTS echobase_cell_spatial_data;
CREATE OR REPLACE VIEW echobase_cell_spatial_data AS
  SELECT
    s.*,
    m.topiaId   AS metaDataId,
    m.name      AS metaDataName,
    d.datavalue AS dataValue,
    d.topiaid   AS dataId
  FROM
    echobase_cell_spatial s,
    data d,
    datametadata m
  WHERE
    s.cellid = d.cell AND
    d.datametadata = m.topiaid;

DROP VIEW IF EXISTS TotalSampleEngrEnc;
CREATE OR REPLACE VIEW TotalSampleEngrEnc AS

  SELECT
    echobase_operation_spatial.voyageid,
    operation.id      AS operation_id,
    echobase_operation_spatial.coordinate,
    sampletype.name   AS sampletype_name,
    sample.sampleweight,
    sizecategory.name AS sizecategory_name,
    species.baracoudacode,
    sample.topiaid
  FROM
    echobase_operation_spatial,
    operation,
    sample,
    speciescategory,
    sizecategory,
    species,
    sampletype
  WHERE
    echobase_operation_spatial.operationid = operation.topiaid AND
    operation.topiaid = sample.operation AND
    sample.speciescategory = speciescategory.topiaid AND
    sample.sampletype = sampletype.topiaid AND
    speciescategory.sizecategory = sizecategory.topiaid AND
    speciescategory.species = species.topiaid AND
    sampletype.name = 'Total' AND
    species.baracoudacode = 'ENGR-ENC';

DROP VIEW IF EXISTS TotalSampleSardPil;
CREATE OR REPLACE VIEW TotalSampleSardPil AS
  SELECT
    echobase_operation_spatial.voyageid,
    operation.id      AS operation_id,
    echobase_operation_spatial.coordinate,
    sampletype.name   AS sampletype_name,
    sample.sampleweight,
    sizecategory.name AS sizecategory_name,
    species.baracoudacode,
    sample.topiaid
  FROM
    echobase_operation_spatial,
    operation,
    sample,
    speciescategory,
    sizecategory,
    species,
    sampletype
  WHERE
    echobase_operation_spatial.operationid = operation.topiaid AND
    operation.topiaid = sample.operation AND
    sample.speciescategory = speciescategory.topiaid AND
    sample.sampletype = sampletype.topiaid AND
    speciescategory.sizecategory = sizecategory.topiaid AND
    speciescategory.species = species.topiaid AND
    sampletype.name = 'Total' AND
    species.baracoudacode = 'SARD-PIL';

DROP VIEW IF EXISTS TotalSampleTracTru;
CREATE OR REPLACE VIEW TotalSampleTracTru AS
  SELECT
    echobase_operation_spatial.voyageid,
    operation.id      AS operation_id,
    echobase_operation_spatial.coordinate,
    sampletype.name   AS sampletype_name,
    sample.sampleweight,
    sizecategory.name AS sizecategory_name,
    species.baracoudacode,
    sample.topiaid
  FROM
    echobase_operation_spatial,
    operation,
    sample,
    speciescategory,
    sizecategory,
    species,
    sampletype
  WHERE
    echobase_operation_spatial.operationid = operation.topiaid AND
    operation.topiaid = sample.operation AND
    sample.speciescategory = speciescategory.topiaid AND
    sample.sampletype = sampletype.topiaid AND
    speciescategory.sizecategory = sizecategory.topiaid AND
    speciescategory.species = species.topiaid AND
    sampletype.name = 'Total' AND
    species.baracoudacode = 'TRAC-TRU';

DROP VIEW IF EXISTS TotalSampleSpraSpr;
CREATE OR REPLACE VIEW TotalSampleSpraSpr AS
  SELECT
    echobase_operation_spatial.voyageid,
    operation.id      AS operation_id,
    echobase_operation_spatial.coordinate,
    sampletype.name   AS sampletype_name,
    sample.sampleweight,
    sizecategory.name AS sizecategory_name,
    species.baracoudacode,
    sample.topiaid
  FROM
    echobase_operation_spatial,
    operation,
    sample,
    speciescategory,
    sizecategory,
    species,
    sampletype
  WHERE
    echobase_operation_spatial.operationid = operation.topiaid AND
    operation.topiaid = sample.operation AND
    sample.speciescategory = speciescategory.topiaid AND
    sample.sampletype = sampletype.topiaid AND
    speciescategory.sizecategory = sizecategory.topiaid AND
    speciescategory.species = species.topiaid AND
    sampletype.name = 'Total' AND
    species.baracoudacode = 'SPRA-SPR';

DROP VIEW IF EXISTS TotalSampleScomSco;
CREATE OR REPLACE VIEW TotalSampleScomSco AS
  SELECT
    echobase_operation_spatial.voyageid,
    operation.id      AS operation_id,
    echobase_operation_spatial.coordinate,
    sampletype.name   AS sampletype_name,
    sample.sampleweight,
    sizecategory.name AS sizecategory_name,
    species.baracoudacode,
    sample.topiaid
  FROM
    echobase_operation_spatial,
    operation,
    sample,
    speciescategory,
    sizecategory,
    species,
    sampletype
  WHERE
    echobase_operation_spatial.operationid = operation.topiaid AND
    operation.topiaid = sample.operation AND
    sample.speciescategory = speciescategory.topiaid AND
    sample.sampletype = sampletype.topiaid AND
    speciescategory.sizecategory = sizecategory.topiaid AND
    speciescategory.species = species.topiaid AND
    sampletype.name = 'Total' AND
    species.baracoudacode = 'SCOM-SCO';

DROP VIEW IF EXISTS TotalSampleMicrPou;
CREATE OR REPLACE VIEW TotalSampleMicrPou AS
  SELECT
    echobase_operation_spatial.voyageid,
    operation.id      AS operation_id,
    echobase_operation_spatial.coordinate,
    sampletype.name   AS sampletype_name,
    sample.sampleweight,
    sizecategory.name AS sizecategory_name,
    species.baracoudacode,
    sample.topiaid
  FROM
    echobase_operation_spatial,
    operation,
    sample,
    speciescategory,
    sizecategory,
    species,
    sampletype
  WHERE
    echobase_operation_spatial.operationid = operation.topiaid AND
    operation.topiaid = sample.operation AND
    sample.speciescategory = speciescategory.topiaid AND
    sample.sampletype = sampletype.topiaid AND
    speciescategory.sizecategory = sizecategory.topiaid AND
    speciescategory.species = species.topiaid AND
    sampletype.name = 'Total' AND
    species.baracoudacode = 'MICR-POU';

-- Create a view with species catches as columns

DROP VIEW IF EXISTS TotalCatchSpOpWide;
CREATE OR REPLACE VIEW TotalCatchSpOpWide AS
  SELECT
    echobase_operation_spatial.voyagename,
    echobase_operation_spatial.operationname,
    echobase_operation_spatial.coordinate,
    (SELECT
    SUM(totalsampleengrenc.sampleweight)
     FROM totalsampleengrenc
     WHERE totalsampleengrenc.operation_id =
           echobase_operation_spatial.operationname) AS TotalCatchENGRENC,
    (SELECT
    SUM(totalsamplesardpil.sampleweight)
     FROM totalsamplesardpil
     WHERE totalsamplesardpil.operation_id =
           echobase_operation_spatial.operationname) AS TotalCatchSARDPIL,
    (SELECT
    SUM(totalsamplespraspr.sampleweight)
     FROM totalsamplespraspr
     WHERE totalsamplespraspr.operation_id =
           echobase_operation_spatial.operationname) AS TotalCatchSPRASPR,
    (SELECT
    SUM(totalsamplemicrpou.sampleweight)
     FROM totalsamplemicrpou
     WHERE totalsamplemicrpou.operation_id =
           echobase_operation_spatial.operationname) AS TotalCatchMICRPOU,
    (SELECT
    SUM(totalsamplescomsco.sampleweight)
     FROM totalsamplescomsco
     WHERE totalsamplescomsco.operation_id =
           echobase_operation_spatial.operationname) AS TotalCatchSCOMSCO,
    (SELECT
    SUM(totalsampletractru.sampleweight)
     FROM totalsampletractru
     WHERE totalsampletractru.operation_id =
           echobase_operation_spatial.operationname) AS TotalCatchTRACTRU,
    echobase_operation_spatial.operationid
  FROM echobase_operation_spatial;

DROP VIEW IF EXISTS cellmapview;
CREATE OR REPLACE VIEW cellmapview AS
  SELECT
    echobase_cell_spatial.voyagename,
    echobase_cell_spatial.coordinate,
    echobase_cell_spatial.shape,
    datametadata.name,
    result.resultvalue,
    species.baracoudacode,
    echobase_cell_spatial.cellname,
    result.topiaid AS resultid
  FROM echobase_cell_spatial, result, datametadata, category, speciescategory,
    species
  WHERE echobase_cell_spatial.cellid = result.cell
        AND result.datametadata  = datametadata.topiaid
        AND result.category = category.topiaid
        AND category.speciescategory = speciescategory.topiaid
        AND speciescategory.species = species.topiaid
        AND echobase_cell_spatial.celltypename = 'Map cell';
