---
-- #%L
-- EchoBase :: Domain
-- %%
-- Copyright (C) 2011 - 2016 Ifremer, Codelutin
-- %%
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Affero General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
-- 
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
-- 
-- You should have received a copy of the GNU Affero General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
-- #L%
---

alter table importlog alter column importtext type text;

alter table operation add column gearHaulingEndLatitude real;
alter table operation add column gearHaulingEndLongitude real;

update operation SET gearHaulingEndLatitude = gearShootingEndLatitude;
update operation SET gearHaulingEndLongitude = gearShootingEndLongitude;

alter table operation drop column gearShootingEndLatitude;
alter table operation drop column gearShootingEndLongitude;

insert into vocabularyciem (topiaId, topiaVersion, topiaCreateDate, label, code) values ('fr.ifremer.echobase.entities.references.VocabularyCIEM#5553610280597#0.93226150280', 0, '2016-04-29 00:00:00.000', 'Method_ER60', 'AC_ProcessingMethod_ER60');
insert into vocabularyciem (topiaId, topiaVersion, topiaCreateDate, label, code) values ('fr.ifremer.echobase.entities.references.VocabularyCIEM#5553610280597#0.93226150281', 0, '2016-04-29 00:00:00.000', 'Software_ER60', 'AC_DataAcquisitionSoftwareName_ER60');
insert into vocabularyciem (topiaId, topiaVersion, topiaCreateDate, label, code) values ('fr.ifremer.echobase.entities.references.VocabularyCIEM#5553610280597#0.93226150282', 0, '2016-04-29 00:00:00.000', 'Sex_1', 'AC_Sex_F');
insert into vocabularyciem (topiaId, topiaVersion, topiaCreateDate, label, code) values ('fr.ifremer.echobase.entities.references.VocabularyCIEM#5553610280597#0.93226150283', 0, '2016-04-29 00:00:00.000', 'Sex_2', 'AC_Sex_M');
insert into vocabularyciem (topiaId, topiaVersion, topiaCreateDate, label, code) values ('fr.ifremer.echobase.entities.references.VocabularyCIEM#5553610280597#0.93226150284', 0, '2016-04-29 00:00:00.000', 'Sex_3', 'AC_Sex_NA');
insert into vocabularyciem (topiaId, topiaVersion, topiaCreateDate, label, code) values ('fr.ifremer.echobase.entities.references.VocabularyCIEM#5553610280597#0.93226150285', 0, '2016-04-29 00:00:00.000', 'Sex_4', 'AC_Sex_U');
insert into vocabularyciem (topiaId, topiaVersion, topiaCreateDate, label, code) values ('fr.ifremer.echobase.entities.references.VocabularyCIEM#5553610280597#0.93226150286', 0, '2016-04-29 00:00:00.000', 'Maturity_1', 'AC_MaturityCode_61');
insert into vocabularyciem (topiaId, topiaVersion, topiaCreateDate, label, code) values ('fr.ifremer.echobase.entities.references.VocabularyCIEM#5553610280597#0.93226150287', 0, '2016-04-29 00:00:00.000', 'Maturity_2', 'AC_MaturityCode_62');
insert into vocabularyciem (topiaId, topiaVersion, topiaCreateDate, label, code) values ('fr.ifremer.echobase.entities.references.VocabularyCIEM#5553610280597#0.93226150288', 0, '2016-04-29 00:00:00.000', 'Maturity_3', 'AC_MaturityCode_62');
insert into vocabularyciem (topiaId, topiaVersion, topiaCreateDate, label, code) values ('fr.ifremer.echobase.entities.references.VocabularyCIEM#5553610280597#0.93226150289', 0, '2016-04-29 00:00:00.000', 'Maturity_4', 'AC_MaturityCode_63');
insert into vocabularyciem (topiaId, topiaVersion, topiaCreateDate, label, code) values ('fr.ifremer.echobase.entities.references.VocabularyCIEM#5553610280597#0.93226150290', 0, '2016-04-29 00:00:00.000', 'Maturity_5', 'AC_MaturityCode_64');
insert into vocabularyciem (topiaId, topiaVersion, topiaCreateDate, label, code) values ('fr.ifremer.echobase.entities.references.VocabularyCIEM#5553610280597#0.93226150291', 0, '2016-04-29 00:00:00.000', 'Maturity_6', 'AC_MaturityCode_65');

update vocabularyciem SET code = 'Gear_PMT_57x52' WHERE label = '57x52';
update vocabularyciem SET code = 'Gear_PMT_76x70' WHERE label = '76x70';
update vocabularyciem SET code = 'AC_AcquisitionMethod_SS' WHERE label = 'ER60';
