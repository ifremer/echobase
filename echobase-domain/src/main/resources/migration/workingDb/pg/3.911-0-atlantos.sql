---
-- #%L
-- EchoBase :: Domain
-- %%
-- Copyright (C) 2011 - 2016 Ifremer, Codelutin
-- %%
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Affero General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
-- 
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
-- 
-- You should have received a copy of the GNU Affero General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
-- #L%
---

create table vocabularyciem (topiaId character varying(255) not null, topiaVersion bigint not null, topiaCreateDate timestamp, label character varying(255) not null, code character varying(255) not null);
alter table vocabularyciem add constraint PK_VOCA primary key (topiaId);
alter table vocabularyciem add constraint UK_VOCA_LABEL unique (label);

alter table mission add column country character varying(255);
alter table species add column wormscode character varying(255);

insert into vocabularyciem (topiaId, topiaVersion, topiaCreateDate, label, code) values ('fr.ifremer.echobase.entities.references.VocabularyCIEM#5553610280597#0.93226150250', 0, '2016-04-29 00:00:00.000', 'Hull', 'AC_TransducerLocation_AA');
insert into vocabularyciem (topiaId, topiaVersion, topiaCreateDate, label, code) values ('fr.ifremer.echobase.entities.references.VocabularyCIEM#5553610280597#0.93226150251', 0, '2016-04-29 00:00:00.000', 'Split', 'AC_TransducerBeamType_S2');
insert into vocabularyciem (topiaId, topiaVersion, topiaCreateDate, label, code) values ('fr.ifremer.echobase.entities.references.VocabularyCIEM#5553610280597#0.93226150252', 0, '2016-04-29 00:00:00.000', '.hac and .raw formats', 'AC_StoredDataFormat_HAC');
insert into vocabularyciem (topiaId, topiaVersion, topiaCreateDate, label, code) values ('fr.ifremer.echobase.entities.references.VocabularyCIEM#5553610280597#0.93226150253', 0, '2016-04-29 00:00:00.000', 'ER60', 'AC_DataAcquisitionSoftwareName_ER60');
insert into vocabularyciem (topiaId, topiaVersion, topiaCreateDate, label, code) values ('fr.ifremer.echobase.entities.references.VocabularyCIEM#5553610280597#0.93226150254', 0, '2016-04-29 00:00:00.000', 'Movies3D', 'AC_DataProcessingSoftwareName_Movies3D');
insert into vocabularyciem (topiaId, topiaVersion, topiaCreateDate, label, code) values ('fr.ifremer.echobase.entities.references.VocabularyCIEM#5553610280597#0.93226150255', 0, '2016-04-29 00:00:00.000', 'PELGAS', 'AC_Survey_PELGAS');
insert into vocabularyciem (topiaId, topiaVersion, topiaCreateDate, label, code) values ('fr.ifremer.echobase.entities.references.VocabularyCIEM#5553610280597#0.93226150256', 0, '2016-04-29 00:00:00.000', 'France', 'ISO_3166_FR');
insert into vocabularyciem (topiaId, topiaVersion, topiaCreateDate, label, code) values ('fr.ifremer.echobase.entities.references.VocabularyCIEM#5553610280597#0.93226150257', 0, '2016-04-29 00:00:00.000', '35HT', 'SHIPC_35HT');
insert into vocabularyciem (topiaId, topiaVersion, topiaCreateDate, label, code) values ('fr.ifremer.echobase.entities.references.VocabularyCIEM#5553610280597#0.93226150258', 0, '2016-04-29 00:00:00.000', 'D1', 'AC_SaCategory_D1');
insert into vocabularyciem (topiaId, topiaVersion, topiaCreateDate, label, code) values ('fr.ifremer.echobase.entities.references.VocabularyCIEM#5553610280597#0.93226150259', 0, '2016-04-29 00:00:00.000', 'D2', 'AC_SaCategory_D2');
insert into vocabularyciem (topiaId, topiaVersion, topiaCreateDate, label, code) values ('fr.ifremer.echobase.entities.references.VocabularyCIEM#5553610280597#0.93226150260', 0, '2016-04-29 00:00:00.000', 'D3', 'AC_SaCategory_D3');
insert into vocabularyciem (topiaId, topiaVersion, topiaCreateDate, label, code) values ('fr.ifremer.echobase.entities.references.VocabularyCIEM#5553610280597#0.93226150261', 0, '2016-04-29 00:00:00.000', 'D4', 'AC_SaCategory_D4');
insert into vocabularyciem (topiaId, topiaVersion, topiaCreateDate, label, code) values ('fr.ifremer.echobase.entities.references.VocabularyCIEM#5553610280597#0.93226150262', 0, '2016-04-29 00:00:00.000', 'D5', 'AC_SaCategory_D5');
insert into vocabularyciem (topiaId, topiaVersion, topiaCreateDate, label, code) values ('fr.ifremer.echobase.entities.references.VocabularyCIEM#5553610280597#0.93226150263', 0, '2016-04-29 00:00:00.000', 'D6', 'AC_SaCategory_D6');
insert into vocabularyciem (topiaId, topiaVersion, topiaCreateDate, label, code) values ('fr.ifremer.echobase.entities.references.VocabularyCIEM#5553610280597#0.93226150264', 0, '2016-04-29 00:00:00.000', 'D7', 'AC_SaCategory_D7');
insert into vocabularyciem (topiaId, topiaVersion, topiaCreateDate, label, code) values ('fr.ifremer.echobase.entities.references.VocabularyCIEM#5553610280597#0.93226150265', 0, '2016-04-29 00:00:00.000', 'D8', 'AC_SaCategory_D8');
insert into vocabularyciem (topiaId, topiaVersion, topiaCreateDate, label, code) values ('fr.ifremer.echobase.entities.references.VocabularyCIEM#5553610280597#0.93226150266', 0, '2016-04-29 00:00:00.000', 'D9', 'AC_SaCategory_D9');
insert into vocabularyciem (topiaId, topiaVersion, topiaCreateDate, label, code) values ('fr.ifremer.echobase.entities.references.VocabularyCIEM#5553610280597#0.93226150267', 0, '2016-04-29 00:00:00.000', 'D10', 'AC_SaCategory_D10');
insert into vocabularyciem (topiaId, topiaVersion, topiaCreateDate, label, code) values ('fr.ifremer.echobase.entities.references.VocabularyCIEM#5553610280597#0.93226150268', 0, '2016-04-29 00:00:00.000', 'D11', 'AC_SaCategory_D11');
insert into vocabularyciem (topiaId, topiaVersion, topiaCreateDate, label, code) values ('fr.ifremer.echobase.entities.references.VocabularyCIEM#5553610280597#0.93226150269', 0, '2016-04-29 00:00:00.000', 'D12', 'AC_SaCategory_D12');
insert into vocabularyciem (topiaId, topiaVersion, topiaCreateDate, label, code) values ('fr.ifremer.echobase.entities.references.VocabularyCIEM#5553610280597#0.93226150270', 0, '2016-04-29 00:00:00.000', 'Standard sphere, in-situ', 'AC_AcquisitionMethod_SS');
insert into vocabularyciem (topiaId, topiaVersion, topiaCreateDate, label, code) values ('fr.ifremer.echobase.entities.references.VocabularyCIEM#5553610280597#0.93226150271', 0, '2016-04-29 00:00:00.000', 'ER', 'AC_ProcessingMethod_ER60');
insert into vocabularyciem (topiaId, topiaVersion, topiaCreateDate, label, code) values ('fr.ifremer.echobase.entities.references.VocabularyCIEM#5553610280597#0.93226150272', 0, '2016-04-29 00:00:00.000', '57x52', 'Gear_57x52');
insert into vocabularyciem (topiaId, topiaVersion, topiaCreateDate, label, code) values ('fr.ifremer.echobase.entities.references.VocabularyCIEM#5553610280597#0.93226150273', 0, '2016-04-29 00:00:00.000', '76x70', 'Gear_76x70');
insert into vocabularyciem (topiaId, topiaVersion, topiaCreateDate, label, code) values ('fr.ifremer.echobase.entities.references.VocabularyCIEM#5553610280597#0.93226150274', 0, '2016-04-29 00:00:00.000', '942OBS', 'Gear_PAR');
insert into vocabularyciem (topiaId, topiaVersion, topiaCreateDate, label, code) values ('fr.ifremer.echobase.entities.references.VocabularyCIEM#5553610280597#0.93226150275', 0, '2016-04-29 00:00:00.000', '0', 'AC_CatchCategory_1');
insert into vocabularyciem (topiaId, topiaVersion, topiaCreateDate, label, code) values ('fr.ifremer.echobase.entities.references.VocabularyCIEM#5553610280597#0.93226150276', 0, '2016-04-29 00:00:00.000', 'G', 'AC_CatchCategory_2');
insert into vocabularyciem (topiaId, topiaVersion, topiaCreateDate, label, code) values ('fr.ifremer.echobase.entities.references.VocabularyCIEM#5553610280597#0.93226150277', 0, '2016-04-29 00:00:00.000', 'P', 'AC_CatchCategory_3');
insert into vocabularyciem (topiaId, topiaVersion, topiaCreateDate, label, code) values ('fr.ifremer.echobase.entities.references.VocabularyCIEM#5553610280597#0.93226150278', 0, '2016-04-29 00:00:00.000', 'TP', 'AC_CatchCategory_4');
