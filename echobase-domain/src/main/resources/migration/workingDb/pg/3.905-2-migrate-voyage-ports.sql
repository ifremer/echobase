---
-- #%L
-- EchoBase :: Domain
-- %%
-- Copyright (C) 2011 - 2016 Ifremer, Codelutin
-- %%
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Affero General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
-- 
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
-- 
-- You should have received a copy of the GNU Affero General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
-- #L%
---

-- Change ports name
update voyage as v SET startPort = p.topiaid FROM ( select pp.topiaId, vv.topiaId as voyageId from port as pp, voyage as vv WHERE upper(pp.name) = upper(vv.startPortName) ) as p WHERE v.startPortName is not null and v.topiaId = p.voyageId;
update voyage as v SET endPort = p.topiaid FROM ( select pp.topiaId, vv.topiaId as voyageId from port as pp, voyage as vv WHERE upper(pp.name) = upper(vv.endPortName) ) as p WHERE v.endPortName is not null and v.topiaId = p.voyageId;

-- Remove old port name columns
alter table voyage drop column startPortName;
alter table voyage drop column endPortName;