/*
 * #%L
 * EchoBase :: Domain
 * %%
 * Copyright (C) 2011 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ifremer.echobase.config;

import org.nuiton.config.ConfigOptionDef;
import org.nuiton.version.Version;

import java.io.File;
import java.net.URL;

import static org.nuiton.i18n.I18n.n;

/**
 * All EchoBase configuration options.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 0.1
 */
public enum EchoBaseConfigurationOption implements ConfigOptionDef {

    ECHOBASE_BASE_DIRECTORY("echobase.base.directory",
                            n("echobase.config.base.directory.description"),
                            "/var/local/echobase",
                            File.class),

    /** Main directory where to put echobase data (logs, and others...). */
    DATA_DIRECTORY("echobase.data.directory",
                   n("echobase.config.data.directory.description"),
                   "${echobase.base.directory}/data",
                   File.class),
    LIBRARY_DIRECTORY("echobase.lib.directory",
                      n("echobase.config.lib.directory.description"),
                      "${echobase.data.directory}/lib",
                      File.class),
    BIN_DIRECTORY("echobase.bin.directory",
                  n("echobase.config.bin.directory.description"),
                  "${echobase.base.directory}/bin",
                  File.class),
    INTERNAL_DB_DIRECTORY(
            "echobase.internal.db.directory",
            n("echobase.config.internal.db.directory.description"),
            "${echobase.data.directory}/internaldb",
            File.class),
    TEMPORARY_DIRECTORY("echobase.data.temporary.directory",
                        n("echobase.config.data.temporary.directory.description"),
                        "${echobase.data.directory}/temp",
                        File.class),
    UPDATE_EXECTUABLE_PATH(
            "echobase.update.executable.path",
            n("echobase.config.update.executable.path.description"),
            "${echobase.bin.directory}/update-echobase.sh",
            File.class),
    RSCRIPT_EXECTUABLE_PATH(
            "echobase.Rscript.executable.path",
            n("echobase.config.Rscript.executable.path.description"),
            "/usr/bin/Rscript",
            File.class),
    EMBEDDED("echobase.embedded",
             n("echobase.config.embedded.description"),
             "false",
             boolean.class),
    COSER_API_URL("echobase.coser.api.url",
                  n("echobase.config.coser.api.url"),
                  "http://www.ifremer.fr/SIH-indices-campagnes/json",
                  URL.class),
    VERSION("echobase.version",
            n("echobase.config.version.description"),
            "",
            Version.class),
    H2_VERSION("echobase.h2Version",
               n("echobase.config.h2Version.description"),
               "",
               String.class),
    POSTGRESQL_VERSION("echobase.postgresqlVersion",
                       n("echobase.config.postgresqlVersion.description"),
                       "",
                       String.class),
    CSV_SEPARATOR("echobase.csv.separator",
                  n("echobase.config.csv.separator.description"),
                  ";",
                  char.class),
    WAR_LOCATION("echobase.war.location",
                 n("echobase.config.war.location.description"),
                 "${echobase.lib.directory}/echobase-embedded-${echobase.version}.war",
                 File.class),
    LOG_CONFIG_FILE("echobase.log.config.file",
                    n("echobase.config.log.config.file.description"),
                    "${echobase.data.directory}/echobase-log.config",
                    File.class),
    DOCUMENTATION_URL("echobase.documentationUrl",
                      n("echobase.config.documentationUrl.description"),
                      "http://echobase.codelutin.com/v/latest",
                      URL.class),
    GIS_DIRECTORY("echobase.gis.directory",
                  n("echobase.config.gis.directory.description"),
                  "${echobase.data.directory}/gis",
                  File.class),
    GIS_TEMPLATES_DIRECTORY("echobase.gis.templates.directory",
                            n("echobase.config.gis.templates.directory.description"),
                            "${echobase.gis.directory}/templates",
                            File.class),
    QGIS_TEMPLATE_FILE("echobase.qgis.template.file",
                       n("echobase.config.qgis.template.file.description"),
                       "${echobase.gis.templates.directory}/" + EchoBaseConfiguration.QGIS_DEFAULT_TEMPLATE_NAME,
                       File.class),
    QGIS_RESOURCES_DIRECTORY("echobase.qgis.resources.directory",
                             n("echobase.config.qgis.resources.directory.description"),
                             "${echobase.gis.directory}/resources/",
                             File.class),
    LIZMAP_TEMPLATE_FILE("echobase.lizmap.template.file",
                         n("echobase.config.lizmap.template.file.description"),
                         "${echobase.gis.templates.directory}/" + EchoBaseConfiguration.LIZMAP_DEFAULT_TEMPLATE_NAME,
                         File.class),
    LIZMAP_PROJECTS_DIRECTORY("echobase.lizmap.projects.directory",
                              n("echobase.config.lizmap.projects.directory.description"),
                              "${echobase.gis.directory}/projects",
                              File.class),
    LIZMAP_APPLICATION_BASEDIR("echobase.lizmap.application.basedir",
                               n("echobase.config.lizmap.application.basedir.description"),
                               "/var/www/html/echobase-map",
                               File.class),
    LIZMAP_APPLICATION_CONFIG_FILE("echobase.lizmap.application.config.file",
                                   n("echobase.config.lizmap.application.config.file.description"),
                                   "${echobase.lizmap.application.basedir}/lizmap/var/config/lizmapConfig.ini.php",
                                   File.class),
    LIZMAP_APPLICATION_MAP_URL("echobase.lizmap.application.map.url",
                               n("echobase.config.lizmap.application.map.url.description"),
                               "http://localhost/echobase-map/lizmap/www/index.php/view/map",
                               String.class),
    LIZMAP_APPLICATION_JDBC_URL("echobase.lizmap.application.jdbc.url",
                                n("echobase.config.lizmap.application.jdbc.url.description"),
                                "jdbc:sqlite:${echobase.lizmap.application.basedir}/lizmap/var/jauth.db",
                                String.class),
    LIZMAP_REPOSITORY_NAME("echobase.lizmap.repositoryName",
                           n("echobase.lizmap.repositoryName.description"),
                           "echobase",
                           String.class);

    /** Configuration key. */
    protected final String key;

    /** I18n key of option description */
    protected final String description;

    /** Type of option */
    protected final Class<?> type;

    /** Default value of option. */
    protected String defaultValue;

    EchoBaseConfigurationOption(String key,
                                String description,
                                String defaultValue,
                                Class<?> type) {
        this.key = key;
        this.description = description;
        this.defaultValue = defaultValue;
        this.type = type;
    }

    @Override
    public String getKey() {
        return key;
    }

    @Override
    public Class<?> getType() {
        return type;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public String getDefaultValue() {
        return defaultValue;
    }

    @Override
    public boolean isTransient() {
        return true;
    }

    @Override
    public boolean isFinal() {
        return true;
    }

    @Override
    public void setDefaultValue(String defaultValue) {
        this.defaultValue = defaultValue;
    }

    @Override
    public void setTransient(boolean newValue) {
        // not used
    }

    @Override
    public void setFinal(boolean newValue) {
        // not used
    }
}
