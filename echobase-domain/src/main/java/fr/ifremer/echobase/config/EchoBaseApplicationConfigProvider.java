package fr.ifremer.echobase.config;

/*
 * #%L
 * EchoBase :: Domain
 * %%
 * Copyright (C) 2011 - 2012 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import org.nuiton.config.ApplicationConfigProvider;
import org.nuiton.config.ConfigActionDef;
import org.nuiton.config.ConfigOptionDef;

import java.util.Locale;

import static org.nuiton.i18n.I18n.l;

/**
 * To generate configuration report.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.1
 */
public class EchoBaseApplicationConfigProvider implements ApplicationConfigProvider {

    @Override
    public String getName() {
        return "echobase";
    }

    @Override
    public String getDescription(Locale locale) {
        return l(locale, "echobase.configuration.description");
    }

    @Override
    public ConfigOptionDef[] getOptions() {
        return EchoBaseConfigurationOption.values();
    }

    @Override
    public ConfigActionDef[] getActions() {
        return new ConfigActionDef[0];
    }
}
