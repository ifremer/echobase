/*
 * #%L
 * EchoBase :: Domain
 * %%
 * Copyright (C) 2011 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ifremer.echobase.config;

import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;
import fr.ifremer.echobase.EchoBaseDateConverter;
import fr.ifremer.echobase.EchoBaseTechnicalException;
import org.apache.commons.io.FileUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.config.ApplicationConfig;
import org.nuiton.config.ArgumentsParserException;
import org.nuiton.util.FileUtil;
import org.nuiton.version.Version;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;

/**
 * EchoBase configuration.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 0.1
 */
public class EchoBaseConfiguration {

    /** Logger. */
    protected static final Log log =
            LogFactory.getLog(EchoBaseConfiguration.class);

    public static final String OPTION_UPDATE_SCHEMA = "updateSchema";

    public static final String OPTION_AUTO_LOGIN = "autoLogin";

    public static final String QGIS_DEFAULT_TEMPLATE_NAME = "EchoBaseQGisTemplate.qgs";

    public static final String LIZMAP_DEFAULT_TEMPLATE_NAME = "EchoBaseLizmapTemplate.qgs.cfg";

    /** Delegate application config object containing configuration. */
    protected ApplicationConfig applicationConfig;

    public EchoBaseConfiguration() {
        this(null);
    }

    public EchoBaseConfiguration(Properties propos) {
        this("echobase.properties", propos);
    }

    public EchoBaseConfiguration(String configFileName, Properties propos) {

        EchoBaseDateConverter.initDateConverter();

        applicationConfig = new ApplicationConfig();
        applicationConfig.setConfigFileName(configFileName);

        if (log.isInfoEnabled()) {
            log.info(this + " is initializing...");
        }
        try {
            applicationConfig.loadDefaultOptions(
                    EchoBaseConfigurationOption.values());
            if (propos != null) {
                for (Map.Entry<Object, Object> entry : propos.entrySet()) {

                    applicationConfig.setDefaultOption(
                            String.valueOf(entry.getKey()),
                            String.valueOf(entry.getValue())
                    );
                }
            }
            applicationConfig.parse();

            File dataDirectory = getDataDirectory();
            String dataDirectoryPath = dataDirectory.getAbsolutePath();
            if (log.isInfoEnabled()) {
                log.info("Data directory = " + dataDirectoryPath);
            }
            if (dataDirectoryPath.endsWith(".")) {
                dataDirectoryPath = dataDirectory.getParentFile().getAbsolutePath();
                if (log.isInfoEnabled()) {
                    log.info("Not absolute data directory " + dataDirectory + " will use " + dataDirectoryPath);
                }

                // always use the absolue path (in cas of embedded db,
                // we wants to have it.
                applicationConfig.setOption(
                        EchoBaseConfigurationOption.DATA_DIRECTORY.key,
                        dataDirectoryPath
                );
            }

        } catch (ArgumentsParserException e) {
            throw new EchoBaseTechnicalException(
                    "Could not parse configuration", e);
        }

        createDirectory(EchoBaseConfigurationOption.DATA_DIRECTORY);
        createDirectory(EchoBaseConfigurationOption.TEMPORARY_DIRECTORY);
        try {
            FileUtils.forceMkdir(getLibDirectory());
        } catch (IOException e) {
            throw new EchoBaseTechnicalException(e);
        }

        if (log.isDebugEnabled()) {
            log.debug("parsed options in config file" +
                      applicationConfig.getOptions());
        }
    }

    public static final Comparator<EchoBaseConfigurationOption> CONFIGURATION_OPTION_COMPARATOR = new Comparator<EchoBaseConfigurationOption>() {
        @Override
        public int compare(EchoBaseConfigurationOption o1, EchoBaseConfigurationOption o2) {
            return o1.key.compareTo(o2.key);
        }
    };

    public String printConfig() {
        StringBuilder builder = new StringBuilder();
        List<EchoBaseConfigurationOption> options = Lists.newArrayList(EchoBaseConfigurationOption.values());
        Collections.sort(options, CONFIGURATION_OPTION_COMPARATOR);
        builder.append("Echobase configuration:");
        builder.append("\n--------------------------------------------------------------------------------\n");
        for (EchoBaseConfigurationOption option : options) {
            builder.append(String.format("\n%1$-40s = %2$s",
                                         option.getKey(),
                                         applicationConfig.getOption(option)));
        }
        builder.append("\n--------------------------------------------------------------------------------");
        return builder.toString();
    }

    public Properties getProperties() {
        return applicationConfig.getFlatOptions();
    }

    public File getDataDirectory() {
        File file = applicationConfig.getOptionAsFile(
                EchoBaseConfigurationOption.DATA_DIRECTORY.key);
        Preconditions.checkNotNull(file);
        return file;
    }

    public File getInternalDbDirectory() {
        File file = applicationConfig.getOptionAsFile(
                EchoBaseConfigurationOption.INTERNAL_DB_DIRECTORY.key);
        Preconditions.checkNotNull(file);
        return file;
    }

    public File getLibDirectory() {
        File file = applicationConfig.getOptionAsFile(
                EchoBaseConfigurationOption.LIBRARY_DIRECTORY.key);
        Preconditions.checkNotNull(file);
        return file;
    }

    public File getBinDirectory() {
        File file = applicationConfig.getOptionAsFile(
                EchoBaseConfigurationOption.BIN_DIRECTORY.key);
        Preconditions.checkNotNull(file);
        return file;
    }

    public File getTemporaryDirectory() {
        File file = applicationConfig.getOptionAsFile(
                EchoBaseConfigurationOption.TEMPORARY_DIRECTORY.key);
        Preconditions.checkNotNull(file);
        return file;
    }

    public File getWarLocation() {
        File file = applicationConfig.getOptionAsFile(
                EchoBaseConfigurationOption.WAR_LOCATION.key);
        Preconditions.checkNotNull(file);
        return file;
    }

    public File getUpdateExecutablePath() {
        File file = applicationConfig.getOptionAsFile(
                EchoBaseConfigurationOption.UPDATE_EXECTUABLE_PATH.key);
        Preconditions.checkNotNull(file);
        return file;
    }  
    
    public File getRscriptExecutablePath() {
        File file = applicationConfig.getOptionAsFile(
                EchoBaseConfigurationOption.RSCRIPT_EXECTUABLE_PATH.key);
        Preconditions.checkNotNull(file);
        return file;
    }

    public File getLogConfigFile() {
        File file = applicationConfig.getOptionAsFile(
                EchoBaseConfigurationOption.LOG_CONFIG_FILE.key);
        Preconditions.checkNotNull(file);
        return file;
    }

    public File getDefaultLogDirectory() {
        return new File(getDataDirectory(), "log");
    }

    public char getCsvSeparator() {
        char csvSeparator = applicationConfig.getOption(
                char.class, EchoBaseConfigurationOption.CSV_SEPARATOR.key);
        Preconditions.checkNotNull(csvSeparator);
        return csvSeparator;
    }

    public Version getApplicationVersion() {
        Version v = applicationConfig.getOptionAsVersion(
                EchoBaseConfigurationOption.VERSION.key);
        Preconditions.checkNotNull(v);
        return v;
    }

    public String getH2Version() {
        String v = applicationConfig.getOption(
                EchoBaseConfigurationOption.H2_VERSION.key);
        Preconditions.checkNotNull(v);
        return v;
    }

    public String getPostgresqlVersion() {
        String v = applicationConfig.getOption(
                EchoBaseConfigurationOption.POSTGRESQL_VERSION.key);
        Preconditions.checkNotNull(v);
        return v;
    }

    public boolean isEmbedded() {
        return applicationConfig.getOptionAsBoolean(
                EchoBaseConfigurationOption.EMBEDDED.key);
    }

    public URL getDocumentationUrl() {
        return applicationConfig.getOptionAsURL(EchoBaseConfigurationOption.DOCUMENTATION_URL.key);
    }

    public String getDocumentationUrl(Locale locale) {
        String result = getDocumentationUrl().toString();
        if (!result.endsWith("/")) {
            result += "/";
        }
        if (Locale.ENGLISH.getLanguage().equals(locale.getLanguage())) {
            result += "en/";
        }
        return result;
    }

    public URL getCoserApiURL() {
        return applicationConfig.getOptionAsURL(
                EchoBaseConfigurationOption.COSER_API_URL.key);
    }

    public boolean getOptionAsBoolean(String propertyName) {
        return applicationConfig.getOptionAsBoolean(propertyName);
    }

    public boolean isUpdateSchema() {
        return applicationConfig.getOptionAsBoolean(OPTION_UPDATE_SCHEMA);
    }

    public File getGisTemplatesDirectory() {
        File file = applicationConfig.getOptionAsFile(
                EchoBaseConfigurationOption.GIS_TEMPLATES_DIRECTORY.key);
        Preconditions.checkNotNull(file);
        return file;
    }

    public File getQgisDefaultTemplateFile() {
        return new File(getGisTemplatesDirectory(), QGIS_DEFAULT_TEMPLATE_NAME);
    }

    public File getLizmapDefaultTemplateFile() {
        return new File(getGisTemplatesDirectory(), LIZMAP_DEFAULT_TEMPLATE_NAME);
    }

    public File getQgisTemplateFile() {
        File file = applicationConfig.getOptionAsFile(
                EchoBaseConfigurationOption.QGIS_TEMPLATE_FILE.key);
        Preconditions.checkNotNull(file);
        return file;
    }

    public File getQgisResourcesDirectory() {
        File file = applicationConfig.getOptionAsFile(
                EchoBaseConfigurationOption.QGIS_RESOURCES_DIRECTORY.key);
        Preconditions.checkNotNull(file);
        return file;
    }

    public File getLizmapTemplateFile() {
        File file = applicationConfig.getOptionAsFile(
                EchoBaseConfigurationOption.LIZMAP_TEMPLATE_FILE.key);
        Preconditions.checkNotNull(file);
        return file;
    }

    public File getLizmapProjectsDirectory() {
        File file = applicationConfig.getOptionAsFile(
                EchoBaseConfigurationOption.LIZMAP_PROJECTS_DIRECTORY.key);
        Preconditions.checkNotNull(file);
        return file;
    }

    public File getLizmapApplicationConfigFile() {
        File file = applicationConfig.getOptionAsFile(
                EchoBaseConfigurationOption.LIZMAP_APPLICATION_CONFIG_FILE.key);
        Preconditions.checkNotNull(file);
        return file;
    }

    public String getLizmapApplicationJdbcUrl() {
        return applicationConfig.getOption(
                EchoBaseConfigurationOption.LIZMAP_APPLICATION_JDBC_URL.key);
    }

    public String getLizmapApplicationMapUrl() {
        return applicationConfig.getOption(EchoBaseConfigurationOption.LIZMAP_APPLICATION_MAP_URL.key);
    }

    public String getLizmapRepositoryName() {
        return applicationConfig.getOption(EchoBaseConfigurationOption.LIZMAP_REPOSITORY_NAME.key);
    }

    /**
     * Creates a directory given the configuration given key.
     *
     * @param key the configuration option key which contains the location of
     *            the directory to create
     */
    private void createDirectory(EchoBaseConfigurationOption key) {

        File directory = applicationConfig.getOptionAsFile(key.getKey());

        Preconditions.checkNotNull(
                directory,
                "Could not find directory " + directory + " (key " +
                key +
                "in your configuration file named echobase.properties)"
        );
        if (log.isDebugEnabled()) {
            log.debug(key + " = " + directory);
        }
        FileUtil.createDirectoryIfNecessary(directory);
    }
}
