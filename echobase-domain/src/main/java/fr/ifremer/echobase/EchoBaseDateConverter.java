/*
 * #%L
 * EchoBase :: Domain
 * %%
 * Copyright (C) 2011 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ifremer.echobase;

import org.apache.commons.beanutils.ConvertUtils;
import org.apache.commons.beanutils.Converter;
import org.apache.commons.beanutils.converters.DateTimeConverter;
import org.nuiton.converter.ConverterUtil;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * A date converter to make possible convert from string using our default
 * format pattern.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 0.1
 */
public class EchoBaseDateConverter extends DateTimeConverter {

    public static Converter initDateConverter() {
        Converter converter = ConverterUtil.getConverter(Date.class);
        if (converter != null && !(converter instanceof EchoBaseDateConverter)) {
            ConvertUtils.deregister(Date.class);
            ConvertUtils.register(new EchoBaseDateConverter(), Date.class);

        } else {
            converter = null;
        }
        return converter;
    }

    protected final DateFormat format =
            new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss");


    @Override
    protected Class getDefaultType() {
        return Date.class;
    }

    @Override
    protected Object convertToType(Class targetType, Object value) throws Exception {
        if (getDefaultType().equals(targetType) && value instanceof String) {

            // let's do our own stuff
            return format.parse((String) value);
        }
        return super.convertToType(targetType, value);
    }
}
