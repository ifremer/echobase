package fr.ifremer.echobase.entities.references;

/*
 * #%L
 * EchoBase :: Domain
 * %%
 * Copyright (C) 2011 - 2015 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.base.Function;
import com.google.common.base.Predicate;

/**
 * Created on 12/16/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.7.2
 */
public class Species2 {

    public static final Function<Species, String> SPECIES_BARACOUDA_CODE = new Function<Species, String>() {
        @Override
        public String apply(Species input) {
            return input.getBaracoudaCode();
        }
    };

    public static Predicate<Species> newCommunityIndicatorSpeciesPredicate() {
        return new Predicate<Species>() {
            @Override
            public boolean apply(Species input) {
                return "0".equals(input.getTaxonSystematicLevel());
            }
        };
    }

    public static Predicate<Species> newPopulationIndicatorSpeciesPredicate() {
        return new Predicate<Species>() {
            @Override
            public boolean apply(Species input) {
                return !"0".equals(input.getTaxonSystematicLevel()) && !"COMPLEM".equals(input.getCodeMemo());
            }
        };
    }
    private Species2() {
    }

}
