package fr.ifremer.echobase.entities;

/*
 * #%L
 * EchoBase :: Domain
 * %%
 * Copyright (C) 2011 - 2016 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.echobase.entities.data.Operation;
import fr.ifremer.echobase.entities.data.Sample;
import fr.ifremer.echobase.entities.data.SampleData;
import fr.ifremer.echobase.entities.references.SpeciesCategory;

/**
 * Created on 02/05/16.
 *
 * @author Tony Chemit - chemit@codelutin.com
 */
public class ImportedSampleDataResult {

    private final int lineNumber;
    private final Operation operation;
    private final Sample sample;
    private final SampleData sampleData;
    private final SpeciesCategory speciesCategory;

    public ImportedSampleDataResult(int lineNumber, Operation operation, Sample sample,SpeciesCategory speciesCategory, SampleData sampleData) {
        this.lineNumber = lineNumber;
        this.operation = operation;
        this.sample = sample;
        this.speciesCategory = speciesCategory;
        this.sampleData = sampleData;
    }

    public int getLineNumber() {
        return lineNumber;
    }

    public Operation getOperation() {
        return operation;
    }

    public Sample getSample() {
        return sample;
    }

    public SpeciesCategory getSpeciesCategory() {
        return speciesCategory;
    }

    public SampleData getSampleData() {
        return sampleData;
    }
}
