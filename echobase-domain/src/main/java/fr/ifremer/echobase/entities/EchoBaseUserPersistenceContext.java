package fr.ifremer.echobase.entities;

/*
 * #%L
 * EchoBase :: Domain
 * %%
 * Copyright (C) 2011 - 2014 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import org.nuiton.topia.persistence.TopiaPersistenceContext;
import org.nuiton.topia.persistence.support.TopiaHibernateSupport;
import org.nuiton.topia.persistence.support.TopiaSqlSupport;

/**
 * Created on 12/19/13.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.6
 */
public interface EchoBaseUserPersistenceContext extends TopiaPersistenceContext, EchoBaseUserTopiaDaoSupplier {

    /**
     * @return {@code true} if db support spatial features, {@code false} otherwise.
     * @since 2.8
     */
    boolean isSpatialSupport();

    /**
     * @return {@code true} if db have spatial structures, {@code false} otherwise.
     * @since 2.8
     */
    boolean isSpatialStructureFound();

    void setSpatialSupport(boolean spatialSupport);

    void setSpatialStructureFound(boolean spatialStructureFound);

    boolean isPostgresql();

    String getUrl();

    TopiaHibernateSupport getHibernateSupport();

    TopiaSqlSupport getSqlSupport();
}
