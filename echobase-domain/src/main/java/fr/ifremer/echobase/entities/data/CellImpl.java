/*
 * #%L
 * EchoBase :: Domain
 * %%
 * Copyright (C) 2011 - 2012 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ifremer.echobase.entities.data;

import com.google.common.base.MoreObjects;
import com.google.common.base.Preconditions;
import fr.ifremer.echobase.entities.references.CellType;
import fr.ifremer.echobase.entities.references.DataQuality;
import org.nuiton.topia.persistence.TopiaEntityVisitor;

import java.util.Collection;

public class CellImpl extends CellAbstract {

    private static final long serialVersionUID = 1L;

    @Override
    public Cell getChildByName(String name) {
        Preconditions.checkNotNull(name);

        Cell cell = null;
        if (!isChildsEmpty()) {
            for (Cell child : childs) {
                if (name.equals(child.getName())) {
                    cell = child;
                    break;
                }
            }
        }
        return cell;
    }

    @Override
    public void acceptWithNoChild(TopiaEntityVisitor visitor) {
        visitor.start(this);
        visitor.visit(this, PROPERTY_NAME, String.class, name);
        visitor.visit(this, PROPERTY_DATA, Collection.class, Data.class, data);
        visitor.visit(this, PROPERTY_CELL_TYPE, CellType.class, cellType);
        visitor.visit(this, PROPERTY_DATA_QUALITY, DataQuality.class, dataQuality);
//        visitor.visit(this, PROPERTY_CHILDS, Collection.class, Cell.class, childs);
        visitor.visit(this, PROPERTY_RESULT, Collection.class, Result.class, result);
        visitor.end(this);
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(Cell.class)
                          .add(PROPERTY_NAME, name)
                          .add(PROPERTY_CELL_TYPE, cellType)
//                          .add(PROPERTY_TOPIA_ID, topiaId)
                          .toString();
    }
}
