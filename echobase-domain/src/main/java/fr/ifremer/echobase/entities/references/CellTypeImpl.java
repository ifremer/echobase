package fr.ifremer.echobase.entities.references;

/*
 * #%L
 * EchoBase :: Domain
 * %%
 * Copyright (C) 2011 - 2013 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.base.MoreObjects;

public class CellTypeImpl extends CellTypeAbstract {

    public static final String MAP = "Map";

    public static final String REGION = "Region";

    public static final String REGION_SURF = "RegionSURF";

    public static final String REGION_CLAS = "RegionCLAS";

    public static final String ELEMENTARY = "Elementary";

    public static final String ESDU = "Esdu";

    public static final String SHOAL = "Shoal";

    private static final long serialVersionUID = 1L;

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(CellType.class)
                          .add(PROPERTY_ID, id)
                          .add(PROPERTY_NAME, name)
//                          .add(PROPERTY_TOPIA_ID, topiaId)
                          .toString();
    }
}
