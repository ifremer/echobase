package fr.ifremer.echobase.entities.spatial;

/*
 * #%L
 * EchoBase :: Domain
 * %%
 * Copyright (C) 2011 - 2013 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import org.apache.commons.lang3.StringUtils;

import java.io.Serializable;

/**
 * TODO
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.2
 */
public class SpatialConfiguration implements Serializable {

    private static final long serialVersionUID = 1L;

    /** Selected jdbc url (can't be null). */
    protected String jdbcUrl;

    /** Selected voyage id (can't be null). */
    protected String voyageId;

    protected String dataMetadataIdForEsduData;

    protected String dataMetadataIdForEsduResult;

    protected String dataMetadataIdForMapData;

    protected String dataMetadataIdForMapResult;

    protected boolean showOperationLayer;

    protected boolean showCellEsduDataLayer;

    protected boolean showCellEsduResultLayer;

    protected boolean showCellMapDataLayer;

    protected boolean showCellMapResultLayer;

    public boolean isAtLeastOneLayerSelected() {
        return showOperationLayer ||
               showCellEsduDataLayer ||
               showCellEsduResultLayer ||
               showCellMapDataLayer ||
               showCellMapResultLayer;
    }

    public boolean isEsduDataLayerSane() {
        return !showCellEsduDataLayer || StringUtils.isNotBlank(dataMetadataIdForEsduData);
    }

    public boolean isEsduResultLayerSane() {
        return !showCellEsduResultLayer || StringUtils.isNotBlank(dataMetadataIdForEsduResult);
    }

    public boolean isMapDataLayerSane() {
        return !showCellMapDataLayer || StringUtils.isNotBlank(dataMetadataIdForMapData);
    }

    public boolean isMapResultLayerSane() {
        return !showCellMapResultLayer || StringUtils.isNotBlank(dataMetadataIdForMapResult);
    }

    public String getJdbcUrl() {
        return jdbcUrl;
    }

    public void setJdbcUrl(String jdbcUrl) {
        this.jdbcUrl = jdbcUrl;
    }

    public String getVoyageId() {
        return voyageId;
    }

    public void setVoyageId(String voyageId) {
        this.voyageId = voyageId;
    }

    public String getDataMetadataIdForEsduData() {
        return dataMetadataIdForEsduData;
    }

    public void setDataMetadataIdForEsduData(String dataMetadataIdForEsduData) {
        this.dataMetadataIdForEsduData = dataMetadataIdForEsduData;
    }

    public String getDataMetadataIdForEsduResult() {
        return dataMetadataIdForEsduResult;
    }

    public void setDataMetadataIdForEsduResult(String dataMetadataIdForEsduResult) {
        this.dataMetadataIdForEsduResult = dataMetadataIdForEsduResult;
    }

    public String getDataMetadataIdForMapData() {
        return dataMetadataIdForMapData;
    }

    public void setDataMetadataIdForMapData(String dataMetadataIdForMapData) {
        this.dataMetadataIdForMapData = dataMetadataIdForMapData;
    }

    public String getDataMetadataIdForMapResult() {
        return dataMetadataIdForMapResult;
    }

    public void setDataMetadataIdForMapResult(String dataMetadataIdForMapResult) {
        this.dataMetadataIdForMapResult = dataMetadataIdForMapResult;
    }

    public boolean isShowOperationLayer() {
        return showOperationLayer;
    }

    public void setShowOperationLayer(boolean showOperationLayer) {
        this.showOperationLayer = showOperationLayer;
    }

    public boolean isShowCellEsduDataLayer() {
        return showCellEsduDataLayer;
    }

    public void setShowCellEsduDataLayer(boolean showCellEsduDataLayer) {
        this.showCellEsduDataLayer = showCellEsduDataLayer;
    }

    public boolean isShowCellEsduResultLayer() {
        return showCellEsduResultLayer;
    }

    public void setShowCellEsduResultLayer(boolean showCellEsduResultLayer) {
        this.showCellEsduResultLayer = showCellEsduResultLayer;
    }

    public boolean isShowCellMapDataLayer() {
        return showCellMapDataLayer;
    }

    public void setShowCellMapDataLayer(boolean showCellMapDataLayer) {
        this.showCellMapDataLayer = showCellMapDataLayer;
    }

    public boolean isShowCellMapResultLayer() {
        return showCellMapResultLayer;
    }

    public void setShowCellMapResultLayer(boolean showCellMapResultLayer) {
        this.showCellMapResultLayer = showCellMapResultLayer;
    }
}
