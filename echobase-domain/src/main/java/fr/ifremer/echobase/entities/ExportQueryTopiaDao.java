package fr.ifremer.echobase.entities;

/*
 * #%L
 * EchoBase :: Domain
 * %%
 * Copyright (C) 2011 - 2012 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import org.nuiton.topia.persistence.TopiaException;

public class ExportQueryTopiaDao extends AbstractExportQueryTopiaDao<ExportQuery> {

    public boolean isQueryExists(String queryName) throws TopiaException {
        return forNameEquals(queryName).exists();
    }

    public boolean isQueryExists(String id, String queryName) throws TopiaException {
        return forNameEquals(queryName).
                addNotEquals(ExportQuery.PROPERTY_TOPIA_ID, id).
                exists();
    }
}
