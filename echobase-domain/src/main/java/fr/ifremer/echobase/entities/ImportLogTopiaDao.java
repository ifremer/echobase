package fr.ifremer.echobase.entities;

/*
 * #%L
 * EchoBase :: Domain
 * %%
 * Copyright (C) 2011 - 2016 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import java.util.LinkedList;

public class ImportLogTopiaDao extends AbstractImportLogTopiaDao<ImportLog> {

    @Override
    public void delete(ImportLog entity) {

        ImportFileTopiaDao importFileDao = topiaDaoSupplier.getDao(ImportFile.class, ImportFileTopiaDao.class);
        for (ImportFile importFile : new LinkedList<>(entity.getImportFile())) {
            entity.removeImportFile(importFile);
            importFileDao.delete(importFile);
        }
        super.delete(entity);

    }
}
