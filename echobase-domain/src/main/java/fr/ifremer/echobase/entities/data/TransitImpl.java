/*
 * #%L
 * EchoBase :: Domain
 * %%
 * Copyright (C) 2011 - 2012 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ifremer.echobase.entities.data;

import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;
import fr.ifremer.echobase.entities.references.Vessel;

import java.util.Collection;

public class TransitImpl extends TransitAbstract {

    private static final long serialVersionUID = 3775200869632533299L;

    @Override
    public Collection<Transect> getTransects(Vessel vessel) {
        Preconditions.checkNotNull(vessel);
        Collection<Transect> result = Lists.newArrayList();
        if (!isTransectEmpty()) {
            for (Transect t : transect) {
                if (vessel.equals(t.getVessel())) {
                    result.add(t);
                }
            }
        }
        return result;
    }

    @Override
    public Transect getTransect(Vessel vessel, String gearCode) {
        Preconditions.checkNotNull(vessel);
        Transect result = null;
        if (!isTransectEmpty()) {
            for (Transect t : transect) {
                if (vessel.equals(t.getVessel()) && gearCode.equals(t.getTransectAbstract())) {
                    result = t;
                    break;
                }
            }
        }
        return result;
    }
} //TransitImpl
