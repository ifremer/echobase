package fr.ifremer.echobase.entities;

/*
 * #%L
 * EchoBase :: Domain
 * %%
 * Copyright (C) 2011 - 2014 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import org.hibernate.cfg.Environment;
import org.nuiton.topia.persistence.internal.AbstractTopiaPersistenceContextConstructorParameter;

public class EchoBaseUserTopiaPersistenceContext extends AbstractEchoBaseUserTopiaPersistenceContext {

    /**
     * Is database has spatial support?
     *
     * @since 2.8
     */
    protected boolean spatialSupport;

    /**
     * Is database spatial structures are found?
     *
     * @since 2.8
     */
    protected boolean spatialStructureFound;

    public EchoBaseUserTopiaPersistenceContext(AbstractTopiaPersistenceContextConstructorParameter newContextParams) {
        super(newContextParams);
    }

    @Override
    public boolean isSpatialSupport() {
        return spatialSupport;
    }

    @Override
    public void setSpatialSupport(boolean spatialSupport) {
        this.spatialSupport = spatialSupport;
    }

    @Override
    public boolean isSpatialStructureFound() {
        return spatialStructureFound;
    }

    @Override
    public void setSpatialStructureFound(boolean spatialStructureFound) {
        this.spatialStructureFound = spatialStructureFound;
    }

    @Override
    public String getUrl() {
        return getHibernateSupport().getHibernateConfiguration().getProperty(Environment.URL);
    }

    public boolean isPostgresql() {

        String dialect = getHibernateSupport().getHibernateConfiguration().getProperty(Environment.DIALECT);
        return DriverType.POSTGRESQL.getDialectClass().getName().equals(dialect);

    }
}
