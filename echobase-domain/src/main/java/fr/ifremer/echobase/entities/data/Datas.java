package fr.ifremer.echobase.entities.data;

/*
 * #%L
 * EchoBase :: Domain
 * %%
 * Copyright (C) 2011 - 2016 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.base.Function;
import com.google.common.base.Predicate;
import fr.ifremer.echobase.entities.references.DataMetadata;

/**
 * Created on 03/04/16.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 4.0
 */
public class Datas {

    public static final Function<Data,DataMetadata> TO_DATA_METADATA = new Function<Data, DataMetadata>() {
        @Override
        public DataMetadata apply(Data input) {
            return input.getDataMetadata();
        }
    };

    public static Predicate<Data> newPredicateByDataMetadata(DataMetadata dataMetadata) {
        return new IsDataPerDataMetadata(dataMetadata);
    }

    public static class IsDataPerDataMetadata implements Predicate<Data> {

        private final DataMetadata dataMetadata;

        public IsDataPerDataMetadata(DataMetadata dataMetadata) {
            this.dataMetadata = dataMetadata;
        }

        @Override
        public boolean apply(Data input) {
            return dataMetadata.equals(input.getDataMetadata());
        }

    }

}
