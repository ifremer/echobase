/*
 * #%L
 * EchoBase :: Domain
 * %%
 * Copyright (C) 2011 - 2012 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ifremer.echobase.entities.data;

import com.google.common.base.Preconditions;
import com.google.common.collect.Collections2;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import fr.ifremer.echobase.entities.references.AncillaryInstrumentation;
import fr.ifremer.echobase.entities.references.Vessel;

import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Set;

/**
 * Default implementation of {@link Voyage}.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 0.3
 */
public class VoyageImpl extends VoyageAbstract {

    private static final long serialVersionUID = 1L;

    @Override
    public Transit getTransit(Date startTime, Date endDate) {

        Preconditions.checkNotNull(startTime);
        Preconditions.checkNotNull(endDate);
        Transit result = null;
        if (!isTransitEmpty()) {
            for (Transit t : getTransit()) {
                Date transitStartTime = t.getStartTime();
                Date transitEndTime = t.getEndTime();
                if (transitEndTime.before(startTime)) {
                    // transit before required range
                    continue;
                }
                if (transitStartTime.after(endDate)) {

                    // transit after required range
                    continue;
                }

                // ok transit contains required range
                result = t;
                break;
            }
        }
        return result;
    }

    @Override
    public Transit getTransit(Date startTime) {

        Preconditions.checkNotNull(startTime);
        Transit result = null;
        if (!isTransitEmpty()) {
            for (Transit t : getTransit()) {
                Date transitEndTime = t.getEndTime();
                if (transitEndTime.before(startTime)) {
                    // transit before required range
                    continue;
                }

                // ok transit contains required range
                result = t;
                break;
            }
        }
        return result;
    }

    @Override
    public Set<Vessel> getAllVessels() {

        Set<Vessel> result = Sets.newHashSet();

        if (!isTransitEmpty()) {
            for (Transit transit : getTransit()) {

                Set<Vessel> vesselsOfTransit =
                        Sets.newLinkedHashSet(Iterables.transform(
                                transit.getTransect(),
                                Transects.TRANSECT_BY_VESSEL));
                result.addAll(vesselsOfTransit);
            }
        }
        return result;
    }

    @Override
    public Collection<Operation> getAllOperations() {
        Collection<Operation> result = Lists.newArrayList();
        if (!isTransitEmpty()) {
            for (Transit transit : getTransit()) {
                if (!transit.isTransectEmpty()) {
                    for (Transect transect : transit.getTransect()) {
                        if (!transect.isOperationEmpty()) {
                            result.addAll(transect.getOperation());
                        }
                    }
                }
            }
        }
        return result;
    }

    @Override
    public Collection<Operation> getOperations(Vessel vessel) {

        Preconditions.checkNotNull(vessel);

        Collection<Operation> result = Lists.newArrayList();

        if (!isTransitEmpty()) {
            for (Transit t : transit) {

                Collection<Transect> transects = t.getTransects(vessel);
                for (Transect transect:transects) {
                    if (transect != null) {
                        Collection<Operation> operations = transect.getOperation();
                        result.addAll(operations);
                    }
                }
            }
        }
        return result;
    }

    @Override
    public Collection<Cell> getRegionCells() {
        Collection<Cell> result;
        if (isPostCellEmpty()) {
            result = Lists.newArrayList();
        } else {
            result = Collections2.filter(getPostCell(),
                                         Cells.IS_REGION_CELL);
        }
        return result;
    }

    @Override
    public Collection<Cell> getMapCells() {
        Collection<Cell> result;
        if (isPostCellEmpty()) {
            result = Lists.newArrayList();
        } else {
            result = Collections2.filter(getPostCell(),
                                         Cells.IS_MAP_CELL);
        }
        return result;
    }

    @Override
    public boolean isDataAcquisitionEmpty() {
        throw new UnsupportedOperationException();
    }

    @Override
    public Collection<DataAcquisition> getDataAcquisition() {
        throw new UnsupportedOperationException();
    }

    @Override
    public void addDataAcquisition(DataAcquisition dataAcquisition) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Voyage getEntity() {
        return this;
    }

    @Override
    public AncillaryInstrumentation getAncillaryInstrumentationByTopiaId(String topiaId) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void addAncillaryInstrumentation(AncillaryInstrumentation ancillaryInstrumentation) {
        throw new UnsupportedOperationException();
    }
}
