package fr.ifremer.echobase.entities.data;

/*
 * #%L
 * EchoBase :: Domain
 * %%
 * Copyright (C) 2011 - 2015 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import fr.ifremer.echobase.entities.data.Cell;
import fr.ifremer.echobase.entities.references.CellType;
import fr.ifremer.echobase.entities.references.CellTypes;

/**
 * Created on 1/21/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.9
 */
public class Cells {


    public static final Predicate<Cell> IS_REGION_CELL = new Predicate<Cell>() {

        @Override
        public boolean apply(Cell input) {
            CellType cellType = input.getCellType();
            return cellType != null &&
                             CellTypes.IS_REGION_CELL_TYPE.apply(cellType);
        }
    };

    public static final Predicate<Cell> IS_MAP_CELL = new Predicate<Cell>() {
        @Override
        public boolean apply(Cell input) {
            CellType cellType = input.getCellType();
            return cellType != null &&
                             CellTypes.IS_MAP_CELL_TYPE.apply(cellType);
        }
    };

    public static final Predicate<Cell> IS_ELEMENTARY_CELL = new Predicate<Cell>() {
        @Override
        public boolean apply(Cell input) {
            CellType cellType = input.getCellType();
            return cellType != null &&
                             CellTypes.IS_ELEMENTARY_CELL_TYPE.apply(cellType);
        }
    };

    public static final Predicate<Cell> IS_ESDU_CELL = new Predicate<Cell>() {
        @Override
        public boolean apply(Cell input) {
            CellType cellType = input.getCellType();
            return cellType != null &&
                             CellTypes.IS_ESDU_CELL_TYPE.apply(cellType);
        }
    };

    public static final Predicate<Cell> IS_POINT_CELL = Predicates.or(IS_ELEMENTARY_CELL, IS_ESDU_CELL);

    public static final Function<Cell, String> CELL_BY_NAME = new Function<Cell, String>() {
        @Override
        public String apply(Cell input) {
            return input.getName();
        }
    };
}
