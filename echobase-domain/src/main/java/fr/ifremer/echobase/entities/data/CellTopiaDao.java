package fr.ifremer.echobase.entities.data;

/*
 * #%L
 * EchoBase :: Domain
 * %%
 * Copyright (C) 2011 - 2013 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import org.nuiton.topia.persistence.TopiaException;
import org.nuiton.topia.persistence.support.TopiaSqlQuery;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

public class CellTopiaDao extends AbstractCellTopiaDao<Cell> {

    public long countVoyageOrphanCells(Voyage voyage) {
        TopiaSqlQuery<Long> query = newCountVoyageOrphanCellsQuery(voyage);
        return topiaSqlSupport.findSingleResult(query);
    }

    public long countMooringOrphanCells(Mooring mooring) {
        TopiaSqlQuery<Long> query = newCountMooringOrphanCellsQuery(mooring);
        return topiaSqlSupport.findSingleResult(query);
    }

    public long countVoyageCellResults(Voyage voyage) {
        TopiaSqlQuery<Long> query = newCountVoyageCellResultsQuery(voyage);
        return topiaSqlSupport.findSingleResult(query);
    }

    public long countMooringCellResults(Mooring mooring) {
        TopiaSqlQuery<Long> query = newCountMooringCellResultsQuery(mooring);
        return topiaSqlSupport.findSingleResult(query);
    }

    public List<String> getOrphanCellIds() throws TopiaException {
        TopiaSqlQuery<String> query = newOrphanCellIdsQuery();
        return topiaSqlSupport.findMultipleResult(query);
    }

    public List<String> getVoyageCellIds(Voyage voyage) throws TopiaException {
        TopiaSqlQuery<String> query = newVoyageCellIdsQuery(voyage);

        return topiaSqlSupport.findMultipleResult(query);
    }

    public List<String> getMooringCellIds(Mooring mooring) throws TopiaException {
        TopiaSqlQuery<String> query = newMooringCellIdsQuery(mooring);

        return topiaSqlSupport.findMultipleResult(query);
    }

    public Boolean containsCellByName(String name) throws TopiaException {
        TopiaSqlQuery<Boolean> query = containsCellByNameQuery(name);

        Boolean result = topiaSqlSupport.findSingleResult(query);
        return result != null;
    }

    protected TopiaSqlQuery<Long> newCountVoyageOrphanCellsQuery(final Voyage voyage) {
        return new TopiaSqlQuery<Long>() {
            @Override
            public PreparedStatement prepareQuery(Connection connection) throws SQLException {
                String hql = "SELECT count(c2.topiaid) FROM Transit ta, " +
                             "                     Transect te, " +
                             "                     DataAcquisition da, " +
                             "                     DataProcessing dp, " +
                             "                     Cell c, Cell c2 " +
                             "WHERE ta.voyage  = ? " +
                             "AND   ta.topiaId = te.transit " +
                             "AND   te.topiaId = da.transect " +
                             "AND   da.topiaId = dp.dataacquisition " +
                             "AND   dp.topiaId = c.dataprocessing " +
                             "AND   c.topiaId = c2.cell";
                PreparedStatement result = connection.prepareStatement(hql);
                result.setString(1, voyage.getTopiaId());
                return result;
            }

            @Override
            public Long prepareResult(ResultSet set) throws SQLException {
                return set.getLong(1);
            }
        };
    }

    protected TopiaSqlQuery<Long> newCountMooringOrphanCellsQuery(Mooring mooring) {
        return new TopiaSqlQuery<Long>() {
            @Override
            public PreparedStatement prepareQuery(Connection connection) throws SQLException {
                String hql = "SELECT count(c2.topiaid) FROM DataAcquisition da, " +
                             "                     DataProcessing dp, " +
                             "                     Cell c, Cell c2 " +
                             "WHERE da.mooring  = ? " +
                             "AND   da.topiaId = dp.dataacquisition " +
                             "AND   dp.topiaId = c.dataprocessing " +
                             "AND   c.topiaId = c2.cell";
                PreparedStatement result = connection.prepareStatement(hql);
                result.setString(1, mooring.getTopiaId());
                return result;
            }

            @Override
            public Long prepareResult(ResultSet set) throws SQLException {
                return set.getLong(1);
            }
        };
    }

    protected TopiaSqlQuery<Long> newCountVoyageCellResultsQuery(final Voyage voyage) {
        return new TopiaSqlQuery<Long>() {
            @Override
            public PreparedStatement prepareQuery(Connection connection) throws SQLException {
                String hql = "SELECT count(c.topiaid) FROM Transit ta, " +
                             "                     Transect te, " +
                             "                     DataAcquisition da, " +
                             "                     DataProcessing dp, " +
                             "                     Cell c " +
                             "WHERE ta.voyage  = ? " +
                             "AND   ta.topiaId = te.transit " +
                             "AND   te.topiaId = da.transect " +
                             "AND   da.topiaId = dp.dataacquisition " +
                             "AND   dp.topiaId = c.dataprocessing";
                PreparedStatement result = connection.prepareStatement(hql);
                result.setString(1, voyage.getTopiaId());
                return result;
            }

            @Override
            public Long prepareResult(ResultSet set) throws SQLException {
                return set.getLong(1);
            }
        };
    }

    protected TopiaSqlQuery<Long> newCountMooringCellResultsQuery(Mooring mooring) {
        return new TopiaSqlQuery<Long>() {
            @Override
            public PreparedStatement prepareQuery(Connection connection) throws SQLException {
                String hql = "SELECT count(c.topiaid) FROM DataAcquisition da, " +
                             "                     DataProcessing dp, " +
                             "                     Cell c " +
                             "WHERE da.mooring = ?" +
                             "AND   da.topiaId = dp.dataacquisition " +
                             "AND   dp.topiaId = c.dataprocessing";
                PreparedStatement result = connection.prepareStatement(hql);
                result.setString(1, mooring.getTopiaId());
                return result;
            }

            @Override
            public Long prepareResult(ResultSet set) throws SQLException {
                return set.getLong(1);
            }
        };
    }

    protected TopiaSqlQuery<String> newOrphanCellIdsQuery() {
        return new TopiaSqlQuery<String>() {
            @Override
            public PreparedStatement prepareQuery(Connection connection) throws SQLException {
                String hql = "SELECT c.topiaid FROM Cell c " +
                             "WHERE c.cell IS NULL AND c.dataprocessing IS NULL AND c.voyage IS NULL";
                return connection.prepareStatement(hql);
            }

            @Override
            public String prepareResult(ResultSet set) throws SQLException {
                return set.getString(1);
            }
        };
    }

    protected TopiaSqlQuery<String> newVoyageCellIdsQuery(final Voyage voyage) {
        return new TopiaSqlQuery<String>() {
            @Override
            public PreparedStatement prepareQuery(Connection connection) throws SQLException {
                String hql = "SELECT c.topiaid FROM Transit ta, " +
                             "                     Transect te, " +
                             "                     DataAcquisition da, " +
                             "                     DataProcessing dp, " +
                             "                     Cell c " +
                             "WHERE ta.voyage  = ? " +
                             "AND   ta.topiaId = te.transit " +
                             "AND   te.topiaId = da.transect " +
                             "AND   da.topiaId = dp.dataacquisition " +
                             "AND   dp.topiaId = c.dataprocessing";
                PreparedStatement result = connection.prepareStatement(hql);
                result.setString(1, voyage.getTopiaId());
                return result;
            }

            @Override
            public String prepareResult(ResultSet set) throws SQLException {
                return set.getString(1);
            }
        };
    }

    protected TopiaSqlQuery<String> newMooringCellIdsQuery(Mooring mooring) {
        return new TopiaSqlQuery<String>() {
            @Override
            public PreparedStatement prepareQuery(Connection connection) throws SQLException {
                String hql = "SELECT c.topiaid FROM DataAcquisition da, " +
                             "                     DataProcessing dp, " +
                             "                     Cell c " +
                             "WHERE da.mooring  = ? " +
                             "AND   da.topiaId = dp.dataacquisition " +
                             "AND   dp.topiaId = c.dataprocessing";
                PreparedStatement result = connection.prepareStatement(hql);
                result.setString(1, mooring.getTopiaId());
                return result;
            }

            @Override
            public String prepareResult(ResultSet set) throws SQLException {
                return set.getString(1);
            }
        };
    }

    protected TopiaSqlQuery<Boolean> containsCellByNameQuery(String name) {
        return new TopiaSqlQuery<Boolean>() {
            @Override
            public PreparedStatement prepareQuery(Connection connection) throws SQLException {
                String hql = "SELECT 1 FROM Cell c " +
                             "WHERE c.name = ? " +
                             "LIMIT 1";
                PreparedStatement result = connection.prepareStatement(hql);
                result.setString(1, name);
                return result;
            }

            @Override
            public Boolean prepareResult(ResultSet set) throws SQLException {
                return set.getInt(1) == 1;
            }
        };
    }
}
