package fr.ifremer.echobase.entities.data;

import fr.ifremer.echobase.entities.references.AncillaryInstrumentation;
import java.util.Collection;
import org.nuiton.topia.persistence.TopiaEntity;

/*
 * #%L
 * EchoBase :: Domain
 * %%
 * Copyright (C) 2011 - 2015 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

/**
 * Common part on Voyage or Mooring or Transect.
 * 
 * @author Julien Ruchaud - ruchaud@codelutin.com
 * @since 4.0
 */
public interface DataAcousticProvider<E extends TopiaEntity> {
    public String getName();
    
    E getEntity();
    
    // For Acoustic import
    boolean isDataAcquisitionEmpty();
            
    Collection<DataAcquisition> getDataAcquisition();
    
    void addDataAcquisition(DataAcquisition dataAcquisition);
    

    // For Echotype import
    boolean isEchotypeEmpty();

    Collection<Echotype> getEchotype();

    void addEchotype(Echotype echotype);
    
    // For AncillaryInstrumentation import
    AncillaryInstrumentation getAncillaryInstrumentationByTopiaId(String topiaId);
    
    void addAncillaryInstrumentation(AncillaryInstrumentation ancillaryInstrumentation);
}
