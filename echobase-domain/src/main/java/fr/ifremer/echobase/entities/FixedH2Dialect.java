/*
 * #%L
 * EchoBase :: Domain
 * %%
 * Copyright (C) 2016 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ifremer.echobase.entities;

import org.hibernate.dialect.H2Dialect;

import java.sql.Types;

/**
 * Override the H2Dialect to fix detected wrong value.
 *
 * org.nuiton.topia.persistence.SchemaValidationTopiaException:
 * Wrong column type in ECHOBASE.PUBLIC.ENTITYMODIFICATIONLOG for column modificationText. Found: varchar, expected: longvarchar
 *
 * org.nuiton.topia.persistence.SchemaValidationTopiaException:
 * Wrong column type in ECHOBASE.PUBLIC.ACOUSTICINSTRUMENT for column transducerDepth. Found: double, expected: float
 */
public class FixedH2Dialect extends H2Dialect {

    public FixedH2Dialect() {
        registerColumnType(Types.LONGVARCHAR, String.format("varchar(%d)", Integer.MAX_VALUE));
        registerColumnType(Types.FLOAT, "real");
    }

}

