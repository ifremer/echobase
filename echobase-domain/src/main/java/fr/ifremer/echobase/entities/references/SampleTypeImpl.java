package fr.ifremer.echobase.entities.references;

/*
 * #%L
 * EchoBase :: Domain
 * %%
 * Copyright (C) 2011 - 2012 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.base.MoreObjects;

public class SampleTypeImpl extends SampleTypeAbstract {

    private static final long serialVersionUID = 1L;

    public static final String TOTAL_SAMPLE_TYPE = "Total";

    public static final String UNSORTED_SAMPLE_TYPE = "Unsorted";

    public static final String SORTED_SAMPLE_TYPE = "Sorted";

    public static final String SUB_SAMPLE_TYPE = "Subsample";

    public static final String INDIVIDUAL_SAMPLE_TYPE = "Individual";

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(SampleType.class)
                          .add(PROPERTY_NAME, name)
//                          .add(PROPERTY_TOPIA_ID, topiaId)
                          .toString();
    }
}
