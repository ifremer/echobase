package fr.ifremer.echobase.entities.references;

/*
 * #%L
 * EchoBase :: Domain
 * %%
 * Copyright (C) 2011 - 2015 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.base.Function;

/**
 * Created on 1/21/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since XXX
 */
public class DataQualities {
    public static final Function<DataQuality, String> DATA_QUALITY_NAME = new Function<DataQuality, String>() {
        @Override
        public String apply(DataQuality input) {
            return String.valueOf(input.getQualityDataFlagValues());
        }
    };

    public static final Function<DataQuality, Integer> DATA_QUALITY_ID = new Function<DataQuality, Integer>() {
        @Override
        public Integer apply(DataQuality input) {
            return input.getQualityDataFlagValues();
        }
    };
}
