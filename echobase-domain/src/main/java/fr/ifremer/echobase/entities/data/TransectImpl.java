package fr.ifremer.echobase.entities.data;

/*
 * #%L
 * EchoBase :: Domain
 * %%
 * Copyright (C) 2011 - 2016 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import java.util.Collection;

/**
 * Default implementation of {@link Transect}.
 *
 * @author Julien Ruchaud - ruchaud@codelutin.com
 * @since 4.0
 */
public class TransectImpl extends TransectAbstract {

    private static final long serialVersionUID = 7016949489850474806L;

    @Override
    public String getName() {
        return getTransit().getVoyage().getName() + " - " + getVessel().getName();
    }

    @Override
    public Transect getEntity() {
        return this;
    }

    @Override
    public boolean isEchotypeEmpty() {
        return getTransit().getVoyage().isEchotypeEmpty();
    }

    @Override
    public Collection<Echotype> getEchotype() {
        return getTransit().getVoyage().getEchotype();
    }

    @Override
    public void addEchotype(Echotype echotype) {
        getTransit().getVoyage().addEchotype(echotype);
    }
    
} //TransectImpl
