package fr.ifremer.echobase.entities;

/*
 * #%L
 * EchoBase :: Domain
 * %%
 * Copyright (C) 2011 - 2014 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import org.nuiton.topia.persistence.TopiaEntity;
import org.nuiton.topia.persistence.TopiaNoResultException;
import org.nuiton.topia.persistence.internal.AbstractTopiaDao;

import java.util.List;
import java.util.Map;

/**
 * Created on 12/19/13.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.6
 */
public abstract class AbstractEchoBaseDao<E extends TopiaEntity> extends AbstractTopiaDao<E> {

    @Override
    public InnerTopiaQueryBuilderRunQueryStep<E> forHql(String hql, Map<String, Object> hqlParameters) {
        return super.forHql(hql, hqlParameters);
    }

    @Override
    public String newFromClause(String alias) {
        return super.newFromClause(alias);
    }

    @Override
    public <K> K findAny(String hql, Map<String, Object> hqlParameters) throws TopiaNoResultException {
        return super.findAny(hql, hqlParameters);
    }

    @Override
    public <K> List<K> find(String hql, Map<String, Object> hqlParameters, int startIndex, int endIndex) {
        return super.find(hql, hqlParameters, startIndex, endIndex);
    }
}
