package fr.ifremer.echobase.entities.data;

/*
 * #%L
 * EchoBase :: Domain
 * %%
 * Copyright (C) 2011 - 2013 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.echobase.EchoBaseTechnicalException;
import org.nuiton.topia.persistence.TopiaException;
import org.nuiton.topia.persistence.util.TopiaUtil;

import java.util.List;

public class CategoryTopiaDao extends AbstractCategoryTopiaDao<Category> {

    public List<Category> getCategoryUsingEchotype(Voyage voyage) throws TopiaException {
        String hql = "SELECT DISTINCT c FROM VoyageImpl v, CategoryImpl c WHERE " +
                     "v = :voyage AND c.echotype in elements(v.echotype)";
        return forHql(hql, "voyage", voyage).findAll();
    }

    public long countCategoryUsingEchotype(Voyage voyage) {
        String hql = "SELECT COUNT(DISTINCT c) FROM VoyageImpl v, CategoryImpl c WHERE " +
                     "v = :voyage AND c.echotype in elements(v.echotype)";
        try {
            return findUnique(hql, TopiaUtil.convertPropertiesArrayToMap("voyage", voyage));
        } catch (TopiaException e) {
            throw new EchoBaseTechnicalException(e);
        }
    }

} //CategoryDAOImpl<E extends Category>
