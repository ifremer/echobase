package fr.ifremer.echobase.entities;

/*
 * #%L
 * EchoBase :: Domain
 * %%
 * Copyright (C) 2011 - 2012 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.echobase.I18nAble;

import static org.nuiton.i18n.I18n.n;

/**
 * Define what import has been done.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.2
 */
public enum ImportType implements I18nAble {

    /** Import a voyage with all his data. *//** Import a voyage with all his data. */
    VOYAGE(
            EchoBaseUserEntityEnum.Voyage,
            n("echobase.common.importType.voyage"),
            n("echobase.common.importType.voyage.short")
    ),

    /** Import Voyage / Transit / Transect. */
    COMMON_ALL(
            EchoBaseUserEntityEnum.Voyage,
            n("echobase.common.importType.commonDataAll"),
            n("echobase.common.importType.commonDataAll.short")
    ),

    /** Import Voyage. */
    COMMON_VOYAGE(
            EchoBaseUserEntityEnum.Voyage,
            n("echobase.common.importType.commonDataVoyage"),
            n("echobase.common.importType.commonDataVoyage.short")
    ),

    /** Import Transit. */
    COMMON_TRANSIT(
            EchoBaseUserEntityEnum.Voyage,
            n("echobase.common.importType.commonDataTransit"),
            n("echobase.common.importType.commonDataTransit.short")
    ),

    /** Import Transect. */
    COMMON_TRANSECT(
            EchoBaseUserEntityEnum.Voyage,
            n("echobase.common.importType.commonDataTransect"),
            n("echobase.common.importType.commonDataTransect.short")
    ),

    /** Import ancillary instrumentation data. */
    COMMON_ANCILLARY_INSTRUMENTATION(
            EchoBaseUserEntityEnum.Voyage,
            n("echobase.common.importType.ancillaryInstrumentationVoyage"),
            n("echobase.common.importType.ancillaryInstrumentationVoyage.short")
    ),

    /** Import Operations. */
    OPERATION(
            EchoBaseUserEntityEnum.Voyage,
            n("echobase.common.importType.operation"),
            n("echobase.common.importType.operation.short")
    ),

    /** Import Catches (unsorted, total, but no individual ones). */
    CATCHES(
            EchoBaseUserEntityEnum.Voyage,
            n("echobase.common.importType.catches"),
            n("echobase.common.importType.catches.short")
    ),

    /** Import accoustic data (Cells ESDU and Elementary). */
    ACOUSTIC(
            EchoBaseUserEntityEnum.Voyage,
            n("echobase.common.importType.acoustic"),
            n("echobase.common.importType.acoustic.short")
    ),

    /** Import results at voyage level. */
    RESULT_VOYAGE(
            EchoBaseUserEntityEnum.Voyage,
            n("echobase.common.importType.resultsVoyage"),
            n("echobase.common.importType.resultsVoyage.short")
    ),

    /** Import esdu results by echotype. */
    RESULT_ESDU(
            EchoBaseUserEntityEnum.Voyage,
            n("echobase.common.importType.resultsEsdu"),
            n("echobase.common.importType.resultsEsdu.short")
    ),

    /** Import cells Region. */
    RESULT_REGION(
            EchoBaseUserEntityEnum.Voyage,
            n("echobase.common.importType.resultsRegion"),
            n("echobase.common.importType.resultsRegion.short")
    ),

    /** Import cells Map Fish. */
    RESULT_MAP_FISH(
            EchoBaseUserEntityEnum.Voyage,
            n("echobase.common.importType.resultsMapFish"),
            n("echobase.common.importType.resultsMapFish.short")
    ),

    /** Import cells Map Other. */
    RESULT_MAP_OTHER(
            EchoBaseUserEntityEnum.Voyage,
            n("echobase.common.importType.resultsMapOther"),
            n("echobase.common.importType.resultsMapOther.short")
    ),
    
    /** Import mooring data. */
    MOORING_COMMONS(
            EchoBaseUserEntityEnum.Mooring,
            n("echobase.common.importType.mooring"),
            n("echobase.common.importType.mooring.short")
    ),

    /** Import ancillary instrumentation data. */
    MOORING_ANCILLARY_INSTRUMENTATION(
            EchoBaseUserEntityEnum.Mooring,
            n("echobase.common.importType.ancillaryInstrumentationMooring"),
            n("echobase.common.importType.ancillaryInstrumentationMooring.short")
    ),
    
    /** Import accoustic data (Cells ESDU and Elementary). */
    MOORING_ACOUSTIC(
            EchoBaseUserEntityEnum.Mooring,
            n("echobase.common.importType.acoustic"),
            n("echobase.common.importType.acoustic.short")
    ),
    
    /** Import results at voyage level. */
    RESULT_MOORING(
            EchoBaseUserEntityEnum.Mooring,
            n("echobase.common.importType.resultsMooring"),
            n("echobase.common.importType.resultsMooring.short")
    ),

    /** Import esdu results by echotype. */
    RESULT_MOORING_ESDU(
            EchoBaseUserEntityEnum.Mooring,
            n("echobase.common.importType.resultsMooringEsdu"),
            n("echobase.common.importType.resultsMooringEsdu.short")
    );
    
    /**
     * All common import types.
     *
     * @since 1.2
     */
    protected static final ImportType[] COMMON_IMPORT_TYPES = new ImportType[]{
            COMMON_ALL,
            COMMON_VOYAGE,
            COMMON_TRANSIT,
            COMMON_TRANSECT
    };

    /**
     * All result import types.
     *
     * @since 1.2
     */
    protected static final ImportType[] RESULT_IMPORT_TYPES = new ImportType[]{
            RESULT_VOYAGE,
            RESULT_ESDU,
            RESULT_REGION,
            RESULT_MAP_FISH,
            RESULT_MAP_OTHER
    };

    /**
     * All result import types for moorings.
     *
     * @since 4.0
     */
    protected static final ImportType[] RESULT_MOORING_IMPORT_TYPES = new ImportType[]{
            RESULT_MOORING,
            RESULT_MOORING_ESDU
    };
    
    private final EchoBaseUserEntityEnum entityType;

    private final String i18nKey;

    private final String shortI18nKey;

    ImportType(EchoBaseUserEntityEnum entityType, String i18nKey, String shortI18nKey) {
        this.entityType = entityType;
        this.i18nKey = i18nKey;
        this.shortI18nKey = shortI18nKey;
    }

    @Override
    public String getI18nKey() {
        return i18nKey;
    }

    public String getShortI18nKey() {
        return shortI18nKey;
    }

    public EchoBaseUserEntityEnum getEntityType() {
        return entityType;
    }

    public static ImportType[] getCommonImportType() {
        return COMMON_IMPORT_TYPES;
    }

    public static ImportType[] getResultImportType() {
        return RESULT_IMPORT_TYPES;
    }
    
    public static ImportType[] getMooringResultImportType() {
        return RESULT_MOORING_IMPORT_TYPES;
    }
}
