package fr.ifremer.echobase.entities;

/*
 * #%L
 * EchoBase :: Domain
 * %%
 * Copyright (C) 2011 - 2016 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.echobase.entities.data.Category;
import fr.ifremer.echobase.entities.data.Cell;
import fr.ifremer.echobase.entities.data.Result;

/**
 * Created on 02/05/16.
 *
 * @author Tony Chemit - chemit@codelutin.com
 */
public class ImportedCellResult {

    private final int lineNumber;
    private final Cell cell;
    private final Category category;
    private final Result result;

    public ImportedCellResult(int lineNumber, Cell cell, Category category, Result result) {
        this.lineNumber = lineNumber;
        this.cell = cell;
        this.category = category;
        this.result = result;
    }

    public int getLineNumber() {
        return lineNumber;
    }

    public Cell getCell() {
        return cell;
    }

    public Category getCategory() {
        return category;
    }

    public Result getResult() {
        return result;
    }
}
