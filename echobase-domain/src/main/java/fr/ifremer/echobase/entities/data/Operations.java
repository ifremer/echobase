package fr.ifremer.echobase.entities.data;

/*
 * #%L
 * EchoBase :: Domain
 * %%
 * Copyright (C) 2011 - 2015 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.google.common.collect.Sets;
import fr.ifremer.echobase.entities.data.Operation;
import fr.ifremer.echobase.entities.data.Sample;
import fr.ifremer.echobase.entities.references.SampleTypeImpl;
import org.apache.commons.collections4.CollectionUtils;

import java.util.Collection;
import java.util.Set;

/**
 * Created on 1/21/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.9
 */
public class Operations {
    public static final Predicate<Operation> OPERATION_WITH_TOTAL_OR_UNSORTED_SAMPLE = new Predicate<Operation>() {
        @Override
        public boolean apply(Operation input) {
            Collection<Sample> samples = input.getSample();

            Set<String> acceptedTypes = Sets.newHashSet(SampleTypeImpl.TOTAL_SAMPLE_TYPE, SampleTypeImpl.UNSORTED_SAMPLE_TYPE);
            boolean result = false;
            if (CollectionUtils.isNotEmpty(samples)) {
                for (Sample sample : samples) {
                    String sampleType = sample.getSampleType().getName();
                    if (acceptedTypes.contains(sampleType)) {
                        result = true;
                        break;
                    }
                }
            }
            return result;
        }
    };

    public static final Function<Operation, String> OPERATION_ID = new Function<Operation, String>() {
        @Override
        public String apply(Operation input) {
            return input.getId();
        }
    };
}
