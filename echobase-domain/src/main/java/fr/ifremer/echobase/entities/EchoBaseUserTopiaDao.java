/*
 * #%L
 * EchoBase :: Domain
 * %%
 * Copyright (C) 2011 - 2012 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ifremer.echobase.entities;

import org.nuiton.topia.persistence.TopiaException;
import org.nuiton.util.pagination.PaginationParameter;

import java.util.List;

public class EchoBaseUserTopiaDao extends GeneratedEchoBaseUserTopiaDao<EchoBaseUser> {

    public List<EchoBaseUser> findAll(PaginationParameter pager) throws TopiaException {
        List<EchoBaseUser> users;

        if (pager == null) {

            users = findAll();
        } else {

            //FIXME Let's init the pager
            users = forHql(newFromClause()).find(pager);
        }
        return users;
    }
}
