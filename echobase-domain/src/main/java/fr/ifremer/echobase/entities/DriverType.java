package fr.ifremer.echobase.entities;

import fr.ifremer.echobase.I18nAble;
import fr.ifremer.echobase.config.EchoBaseConfiguration;
import org.hibernate.dialect.PostgreSQL82Dialect;

import java.sql.Driver;

import static org.nuiton.i18n.I18n.n;

/*
 * #%L
 * EchoBase :: Domain
 * %%
 * Copyright (C) 2011 - 2012 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

/**
 * To define a type of driver (h2 or postgres for the moment).
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.1
 */
public enum DriverType implements I18nAble {

    H2(org.h2.Driver.class,
       FixedH2Dialect.class,
       n("echobase.common.driverType.h2")) {
        @Override
        public String getPilotVersion(EchoBaseConfiguration config) {
            return config.getH2Version();
        }
    },
    POSTGRESQL(org.postgresql.Driver.class,
               PostgreSQL82Dialect.class,
               n("echobase.common.driverType.postgres")) {
        @Override
        public String getPilotVersion(EchoBaseConfiguration config) {
            return config.getPostgresqlVersion();
        }
    };

    private final Class<? extends Driver> driverClass;

    private final Class<?> dialectClass;

    private final String i18nKey;

    DriverType(Class<? extends Driver> driverClass,
               Class<?> dialectClass,
               String i18nKey) {
        this.driverClass = driverClass;
        this.dialectClass = dialectClass;
        this.i18nKey = i18nKey;
    }

    public Class<? extends Driver> getDriverClass() {
        return driverClass;
    }

    public Class<?> getDialectClass() {
        return dialectClass;
    }

    public abstract String getPilotVersion(EchoBaseConfiguration config);

    public final String getPilotFileName(EchoBaseConfiguration config) {
        String version = getPilotVersion(config);
        return version == null ? null : name().toLowerCase() + "-" + version + ".jar";
    }

    @Override
    public String getI18nKey() {
        return i18nKey;
    }

    public static DriverType valueOfDriverName(String driverName) {
        DriverType result = null;
        for (DriverType driverType : values()) {
            if (driverName.equals(driverType.getDriverClass().getName())) {
                result = driverType;
                break;
            }
        }
        return result;
    }

}
