/*
 * #%L
 * EchoBase :: Domain
 * %%
 * Copyright (C) 2011 - 2012 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ifremer.echobase.entities.references;

public class SpeciesCategoryImpl extends SpeciesCategoryAbstract {

    private static final long serialVersionUID = 1L;

    @Override
    public String getSizeCategoryLabel() {
        return sizeCategory == null ? "" :
               ("sizeCategory: " + sizeCategory.getName());
    }

    @Override
    public String getAgeCategoryLabel() {
        return ageCategory == null ? "" :
               ("ageCategory: " + ageCategory.getName());
    }

    @Override
    public String getSexCategoryLabel() {
        return sexCategory == null ? "" :
               ("sexCategory: " + sexCategory.getName());
    }
} //SpeciesCategoryImpl
