package fr.ifremer.echobase.entities.references;

/*
 * #%L
 * EchoBase :: Domain
 * %%
 * Copyright (C) 2011 - 2013 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.base.MoreObjects;

public class SampleDataTypeImpl extends SampleDataTypeAbstract {

    public static final String MEAN_LENGTHCM = "MeanLengthcm";

    public static final String MEAN_WEIGHTG = "MeanWeightg";

    public static final String NO_PER_KG = "NoPerKg";

    public static final String SPECIMEN_INDEX = "specimenIndex";

    public static final String WEIGHT_AT_LENGTHKG = "WeightAtLengthkg";

    public static final String NUMBER_AT_LENGTH = "NumberAtLength";

    private static final long serialVersionUID = 1L;

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(SampleDataType.class)
                          .add(PROPERTY_NAME, name)
                          .add(PROPERTY_PRECISION, precision)
                          .add(PROPERTY_UNITS, units)
//                          .add(PROPERTY_TOPIA_ID, topiaId)
                          .toString();
    }

}
