package fr.ifremer.echobase.entities.data;

/*
 * #%L
 * EchoBase :: Domain
 * %%
 * Copyright (C) 2011 - 2013 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.echobase.entities.references.DataMetadata;
import fr.ifremer.echobase.entities.references.Mission;

import java.util.List;
import java.util.Set;

public class ResultTopiaDao extends AbstractResultTopiaDao<Result> {

    public List<Result> findAllWithCategoryCellAndDataMetadata(Set<String> cellIds,
                                                               DataMetadata requiredDataMetadata,
                                                               List<String> categoryIds) {
        return forEquals(Result.PROPERTY_DATA_METADATA, requiredDataMetadata).
                addTopiaIdIn(Result.PROPERTY_CELL, cellIds).
                addTopiaIdIn(Result.PROPERTY_CATEGORY, categoryIds).
                findAll();
    }

    public List<Result> findAllWithNoCategoryCellAndDataMetadata(Set<String> cellIds,
                                                                 DataMetadata requiredDataMetadata) {
        return forEquals(Result.PROPERTY_DATA_METADATA, requiredDataMetadata).
                addTopiaIdIn(Result.PROPERTY_CELL, cellIds).
                addNull(Result.PROPERTY_CATEGORY).
                findAll();
    }

    public List<Result> findAllWithMissionAndDatametadata(Mission mission, DataMetadata dataMetadata) {
        return forEquals(Result.PROPERTY_CELL + "." + Cell.PROPERTY_VOYAGE + "." + Voyage.PROPERTY_MISSION, mission).
                addEquals(Result.PROPERTY_DATA_METADATA, dataMetadata).
                findAll();
    }
}
