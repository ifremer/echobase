package fr.ifremer.echobase.entities;

/*
 * #%L
 * EchoBase :: Domain
 * %%
 * Copyright (C) 2011 - 2014 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.echobase.entities.data.VoyageImpl;
import fr.ifremer.echobase.persistence.JdbcConfiguration;
import fr.ifremer.echobase.persistence.migration.workingDb.WorkingDbMigrationCallbackForH2;
import fr.ifremer.echobase.persistence.migration.workingDb.WorkingDbMigrationCallbackForPostgresql;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.cfg.Environment;
import org.nuiton.topia.migration.TopiaMigrationEngine;
import org.nuiton.topia.migration.TopiaMigrationService;
import org.nuiton.topia.persistence.BeanTopiaConfiguration;
import org.nuiton.topia.persistence.TopiaConfiguration;
import org.nuiton.topia.persistence.internal.LegacyTopiaIdFactory;
import org.nuiton.topia.persistence.internal.TopiaConnectionProvider;

import java.util.LinkedHashMap;
import java.util.Map;

public class EchoBaseUserTopiaApplicationContext extends AbstractEchoBaseUserTopiaApplicationContext {

    /** Logger. */
    private static final Log log = LogFactory.getLog(EchoBaseUserTopiaApplicationContext.class);

    /**
     * Is database has spatial support?
     *
     * @since 2.8
     */
    protected boolean spatialSupport;

    /**
     * Is database spatial structures are found?
     *
     * @since 2.8
     */
    protected boolean spatialStructureFound;

    protected boolean init;

    /**
     * Open a new topia root context from the given jdbc configuration.
     *
     * @param jdbcConfiguration jdbc configuration
     * @return the new fresh root context
     */
    public static EchoBaseUserTopiaApplicationContext newApplicationContext(JdbcConfiguration jdbcConfiguration) {
        return newApplicationContext(jdbcConfiguration, true);
    }

    /**
     * Open a new topia root context from the given jdbc configuration and
     * create schema if asked and schema does not exist.
     *
     * @param jdbcConfiguration jdbc configuration
     * @return the new fresh root context
     * @since 2.4
     */
    public static EchoBaseUserTopiaApplicationContext newApplicationContext(JdbcConfiguration jdbcConfiguration, boolean createSchema) {

        BeanTopiaConfiguration configuration = new BeanTopiaConfiguration();
        configuration.setJdbcConnectionUrl(jdbcConfiguration.getUrl());
        configuration.setJdbcConnectionUser(jdbcConfiguration.getLogin());
        configuration.setJdbcConnectionPassword(jdbcConfiguration.getPassword());
        configuration.setTopiaIdFactoryClass(LegacyTopiaIdFactory.class);
        DriverType driverType = jdbcConfiguration.getDriverType();
        configuration.setJdbcDriverClass(driverType.getDriverClass());

        Map<String, String> hibernateExtraProperties = new LinkedHashMap<>();
        hibernateExtraProperties.put(Environment.CONNECTION_PROVIDER, TopiaConnectionProvider.class.getName());
        hibernateExtraProperties.put(Environment.SHOW_SQL, Boolean.FALSE.toString());
        hibernateExtraProperties.put(Environment.FORMAT_SQL, Boolean.FALSE.toString());
        hibernateExtraProperties.put(Environment.USE_SQL_COMMENTS, Boolean.FALSE.toString());
        hibernateExtraProperties.put(Environment.DIALECT, driverType.getDialectClass().getName());

        configuration.setHibernateExtraConfiguration(hibernateExtraProperties);

        Map<String, String> migrationServiceOptions = new LinkedHashMap<>();
        migrationServiceOptions.put(TopiaMigrationService.MIGRATION_SHOW_SQL, Boolean.TRUE.toString());

        if (DriverType.POSTGRESQL == driverType) {
            migrationServiceOptions.put(TopiaMigrationService.MIGRATION_CALLBACK, WorkingDbMigrationCallbackForPostgresql.class.getName());
        } else {
            migrationServiceOptions.put(TopiaMigrationService.MIGRATION_CALLBACK, WorkingDbMigrationCallbackForH2.class.getName());
        }

        configuration.addDeclaredService("migration", TopiaMigrationEngine.class, migrationServiceOptions);

        if (log.isInfoEnabled()) {
            log.info("Starts a db at : " + configuration.getJdbcConnectionUrl());
        }
        EchoBaseUserTopiaApplicationContext result = new EchoBaseUserTopiaApplicationContext(configuration);

        if (createSchema && !result.isTableExists(VoyageImpl.class)) {

            if (log.isInfoEnabled()) {
                log.info("Will create schema for " + jdbcConfiguration.getUrl());
            }
            result.createSchema();
        }
        return result;

    }

    private EchoBaseUserTopiaApplicationContext(TopiaConfiguration topiaConfiguration) {
        super(topiaConfiguration);
        initInternalSpatialStates();
    }

    @Override
    public EchoBaseUserTopiaPersistenceContext newPersistenceContext() {

        return newPersistenceContext(true);


    }

    protected EchoBaseUserTopiaPersistenceContext newPersistenceContext(boolean computeStates) {

        if (!init && computeStates) {

            initInternalSpatialStates();

        }

        EchoBaseUserTopiaPersistenceContext persistenceContext = super.newPersistenceContext();

        persistenceContext.setSpatialSupport(spatialSupport);
        persistenceContext.setSpatialStructureFound(spatialStructureFound);

        return persistenceContext;

    }

    public boolean isSpatialStructureFound() {
        return spatialStructureFound;
    }

    public boolean isSpatialSupport() {
        return spatialSupport;
    }

    public void setSpatialStructureFound(boolean spatialStructureFound) {
        this.spatialStructureFound = spatialStructureFound;
    }

    public void initInternalSpatialStates() {

        if (!init) {

            try (EchoBaseUserTopiaPersistenceContext persistenceContext = newPersistenceContext(false)) {

                spatialSupport = computeSpatialSupport(persistenceContext);

                if (log.isInfoEnabled()) {
                    log.info("spatialSupport: " + spatialSupport);
                }

                if (spatialSupport) {

                    spatialStructureFound = computeSpatialStructureFound(persistenceContext);
                    if (log.isInfoEnabled()) {
                        log.info("spatialStructureFound: " + spatialStructureFound);
                    }

                } else {

                    // no spatial support, so no spatial structure
                    spatialStructureFound = false;

                }

            }

            init = true;

        }


    }

    private boolean computeSpatialSupport(EchoBaseUserTopiaPersistenceContext persistenceContext) {

        String dialect = persistenceContext.getHibernateSupport().getHibernateConfiguration().getProperty(Environment.DIALECT);
        return DriverType.POSTGRESQL.getDialectClass().getName().equals(dialect);

    }

    private boolean computeSpatialStructureFound(EchoBaseUserTopiaPersistenceContext persistenceContext) {

        boolean result;
        try {

            persistenceContext.getSqlSupport().executeSql("select count(*) from echobase_cell_spatial;");
            result = true;

        } catch (Exception e) {

            // table not found (or other, ...)
            result = false;

            // rollback (otherwise transaction will stay dirty)
            persistenceContext.rollback();

        }

        return result;

    }

}
