package fr.ifremer.echobase.entities;

/*
 * #%L
 * EchoBase :: Domain
 * %%
 * Copyright (C) 2011 - 2016 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.echobase.persistence.migration.internalDb.InternalDbMigrationCallback;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.cfg.Environment;
import org.nuiton.topia.migration.TopiaMigrationEngine;
import org.nuiton.topia.migration.TopiaMigrationService;
import org.nuiton.topia.persistence.BeanTopiaConfiguration;
import org.nuiton.topia.persistence.TopiaConfiguration;
import org.nuiton.topia.persistence.internal.LegacyTopiaIdFactory;
import org.nuiton.topia.persistence.internal.TopiaConnectionProvider;

import java.io.File;
import java.util.LinkedHashMap;
import java.util.Map;

public class EchoBaseInternalTopiaApplicationContext extends AbstractEchoBaseInternalTopiaApplicationContext {

    /** Logger. */
    private static final Log log = LogFactory.getLog(EchoBaseInternalTopiaApplicationContext.class);

    /**
     * Open a new topia root context for the internal db of echobase (this should be a h2 db used only for security).
     *
     * @param internalDbDirectory directory of the internal db
     * @return the new fresh root context of the internal db
     */
    public static EchoBaseInternalTopiaApplicationContext newApplicationContext(File internalDbDirectory) {

        BeanTopiaConfiguration configuration = new BeanTopiaConfiguration();
        configuration.setJdbcConnectionUrl("jdbc:h2:file:" + internalDbDirectory.getAbsolutePath() + "/echobase-user");
        configuration.setJdbcConnectionUser("sa");
        configuration.setJdbcConnectionPassword("sa");
        configuration.setTopiaIdFactoryClass(LegacyTopiaIdFactory.class);
        DriverType driverType = DriverType.H2;
        configuration.setJdbcDriverClass(driverType.getDriverClass());

        Map<String, String> hibernateExtraProperties = new LinkedHashMap<>();
        hibernateExtraProperties.put(Environment.CONNECTION_PROVIDER, TopiaConnectionProvider.class.getName());
        hibernateExtraProperties.put(Environment.SHOW_SQL, Boolean.FALSE.toString());
        hibernateExtraProperties.put(Environment.FORMAT_SQL, Boolean.FALSE.toString());
        hibernateExtraProperties.put(Environment.USE_SQL_COMMENTS, Boolean.FALSE.toString());
        hibernateExtraProperties.put(Environment.DIALECT, driverType.getDialectClass().getName());

        configuration.setHibernateExtraConfiguration(hibernateExtraProperties);

        Map<String, String> migrationServiceOptions = new LinkedHashMap<>();
        migrationServiceOptions.put(TopiaMigrationService.MIGRATION_SHOW_SQL, Boolean.TRUE.toString());
        migrationServiceOptions.put(TopiaMigrationService.MIGRATION_CALLBACK, InternalDbMigrationCallback.class.getName());

        configuration.addDeclaredService("migration", TopiaMigrationEngine.class, migrationServiceOptions);

        if (log.isInfoEnabled()) {
            log.info("Starts a db at : " + configuration.getJdbcConnectionUrl());
        }
        EchoBaseInternalTopiaApplicationContext result = new EchoBaseInternalTopiaApplicationContext(configuration);
        if (!result.isTableExists(EchoBaseUserImpl.class)) {

            if (log.isInfoEnabled()) {
                log.info("Will create schema for " + configuration.getJdbcConnectionUrl());
            }
            result.createSchema();
        }
        return result;
    }

    private EchoBaseInternalTopiaApplicationContext(TopiaConfiguration topiaConfiguration) {
        super(topiaConfiguration);
    }

}
