package fr.ifremer.echobase.entities;

/*
 * #%L
 * EchoBase :: Domain
 * %%
 * Copyright (C) 2011 - 2013 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import java.util.regex.Pattern;

/**
 * Useful methods aroud {@link ExportQuery}.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.2
 */
public class ExportQueries {

    protected static final Pattern NAME_PATTERN = Pattern.compile("[\\d\\w_-]+");

    public static boolean isQueryNameValid(String queryName) {
        return NAME_PATTERN.matcher(queryName).matches();
    }
}
