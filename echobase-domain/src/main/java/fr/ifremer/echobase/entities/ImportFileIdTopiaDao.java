package fr.ifremer.echobase.entities;

/*
 * #%L
 * EchoBase :: Domain
 * %%
 * Copyright (C) 2011 - 2016 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.ImmutableMap;
import fr.ifremer.echobase.entities.data.Cell;
import fr.ifremer.echobase.entities.data.Result;
import fr.ifremer.echobase.entities.data.SampleData;
import org.nuiton.topia.persistence.HqlAndParametersBuilder;
import org.nuiton.topia.persistence.TopiaEntity;

public class ImportFileIdTopiaDao extends AbstractImportFileIdTopiaDao<ImportFileId> {

    public Iterable<ImportFileId> getImportFileIdsForImportFile(ImportFile importFile) {

        return forImportFileEquals(importFile)
                .setOrderByArguments(ImportFileId.PROPERTY_IMPORT_ORDER)
                .findAllLazy();

    }

    public <E extends TopiaEntity> Iterable<ImportFileId> getImportFileIdsForImportFileAndType(ImportFile importFile, Class<E> entityType) {

        HqlAndParametersBuilder<ImportFileId> builder = newHqlAndParametersBuilder();
        builder.addEquals(ImportFileId.PROPERTY_IMPORT_FILE, importFile);
        builder.addWhereClause(ImportFileId.PROPERTY_ENTITY_ID + " LIKE :" + ImportFileId.PROPERTY_ENTITY_ID, ImmutableMap.<String, Object>builder().put(ImportFileId.PROPERTY_ENTITY_ID, entityType.getName() + "#%").build());
        builder.setOrderByArguments(ImportFileId.PROPERTY_IMPORT_ORDER);
        return findAllLazy(builder.getHql(), builder.getHqlParameters());

    }

    public long getImportFileIdsCountForImportFile(ImportFile importFile) {
        return forImportFileEquals(importFile).count();
    }

    public <E extends TopiaEntity> Iterable<E> getImportedEntities(ImportFile importFile, Class<E> entityType) {
        return findAllLazy(importFile, entityType, "e");
    }

    public Iterable<ImportedCellResult> getImportedCellResults(ImportFile importFile) {
        return findAllLazy(importFile, Result.class, "new fr.ifremer.echobase.entities.ImportedCellResult(i.lineNumber, e.cell, e.category, e)");
    }

    public Iterable<ImportedSampleDataResult> getImportedSampleDataResults(ImportFile importFile) {
        return findAllLazy(importFile, SampleData.class, "new fr.ifremer.echobase.entities.ImportedSampleDataResult(i.lineNumber, e.sample.operation, e.sample, e.sample.speciesCategory, e)");
    }

    public Iterable<ImportedCell> getImportedAcousticCells(ImportFile importFile) {
        return findAllLazy(importFile, Cell.class, "new fr.ifremer.echobase.entities.ImportedCell(i.lineNumber, e)");
    }

    protected <T, E extends TopiaEntity> Iterable<T> findAllLazy(ImportFile importFile, Class<E> entityType, String constructor) {

        String hql
                = " Select " + constructor + " From " + entityType.getName() + " As e, " + ImportFileId.class.getName() + " As i"
                + " Where e.topiaId = i." + ImportFileId.PROPERTY_ENTITY_ID
                + " And i." + ImportFileId.PROPERTY_IMPORT_FILE + " = :" + ImportFileId.PROPERTY_IMPORT_FILE
                + " And i." + ImportFileId.PROPERTY_ENTITY_ID + " Like :" + ImportFileId.PROPERTY_ENTITY_ID
                + " Order By i." + ImportFileId.PROPERTY_IMPORT_ORDER;

        ImmutableMap<String, Object> hqlParameters = ImmutableMap.<String, Object>builder()
                .put(ImportFileId.PROPERTY_IMPORT_FILE, importFile)
                .put(ImportFileId.PROPERTY_ENTITY_ID, entityType.getName() + "#%")
                .build();

        return findAllLazy(hql, hqlParameters, 10000);

    }

}
