/*
 * #%L
 * EchoBase :: Domain
 * %%
 * Copyright (C) 2011 - 2012 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ifremer.echobase.entities.data;

import com.google.common.base.MoreObjects;
import fr.ifremer.echobase.entities.references.SampleType;
import fr.ifremer.echobase.entities.references.SpeciesCategory;

public class OperationImpl extends OperationAbstract {

    private static final long serialVersionUID = 1L;

    @Override
    public Sample getSample(SpeciesCategory speciesCategory, SampleType sampleType) {

        Sample result = null;
        if (isSampleNotEmpty()) {
            for (Sample s : sample) {
                if (sampleType.equals(s.getSampleType()) && speciesCategory.equals(s.getSpeciesCategory())) {
                    result = s;
                    break;
                }
            }
        }
        return result;

    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(Operation.class)
                          .add(PROPERTY_ID, id)
                          .add(PROPERTY_GEAR, gear)
//                          .add(PROPERTY_TOPIA_ID, topiaId)
                          .toString();
    }

}
