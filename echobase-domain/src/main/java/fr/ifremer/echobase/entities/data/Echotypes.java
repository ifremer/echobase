package fr.ifremer.echobase.entities.data;

/*
 * #%L
 * EchoBase :: Domain
 * %%
 * Copyright (C) 2011 - 2015 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.base.Function;
import com.google.common.base.Predicate;

/**
 * Created on 1/21/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.9
 */
public class Echotypes {

    public static final Function<Echotype, String> ECHOTYPE_NAME = new Function<Echotype, String>() {
        @Override
        public String apply(Echotype input) {
            return input.getName();
        }
    };

    public static Predicate<Echotype> newEchotypeByNamePredicate(String name) {
        return new EchotypeByNamePredicate(name);
    }

    public static class EchotypeByNamePredicate implements Predicate<Echotype> {
        private final String echotypeName;

        public EchotypeByNamePredicate(String echotypeName) {
            this.echotypeName = echotypeName;
        }

        @Override
        public boolean apply(Echotype input) {
            return echotypeName.equals(input.getName());
        }
    }
}
