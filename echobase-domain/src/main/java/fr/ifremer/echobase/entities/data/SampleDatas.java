package fr.ifremer.echobase.entities.data;

/*
 * #%L
 * EchoBase :: Domain
 * %%
 * Copyright (C) 2011 - 2016 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.base.Function;
import fr.ifremer.echobase.entities.references.SampleDataType;

/**
 * Created on 01/04/16.
 *
 * @author Tony Chemit - chemit@codelutin.com
 */
public class SampleDatas {

    public static final Function<SampleData, SampleDataType> TO_SAMPLE_DATA_TYPE = new Function<SampleData, SampleDataType>() {
        @Override
        public SampleDataType apply(SampleData input) {
            return input.getSampleDataType();
        }
    };
}
