package fr.ifremer.echobase.entities;

/*
 * #%L
 * EchoBase :: Domain
 * %%
 * Copyright (C) 2011 - 2016 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.ImmutableMap;

public class ImportFileTopiaDao extends AbstractImportFileTopiaDao<ImportFile> {

    @Override
    public void delete(ImportFile entity) {

        // delete all ImportFileId before

        {
            ImmutableMap<String, Object> parameters = ImmutableMap.<String, Object>builder().put(ImportFileId.PROPERTY_IMPORT_FILE, entity).build();
            topiaJpaSupport.execute("Delete From " + ImportFileId.class.getName() + " Where " + ImportFileId.PROPERTY_IMPORT_FILE + " = :" + ImportFileId.PROPERTY_IMPORT_FILE, parameters);
            topiaHibernateSupport.getHibernateSession().flush();

        }

        {
            ImmutableMap<String, Object> parameters = ImmutableMap.<String, Object>builder().put(ImportFile.PROPERTY_TOPIA_ID, entity.getTopiaId()).build();
            topiaJpaSupport.execute("Delete From " + ImportFile.class.getName() + " Where " + ImportFile.PROPERTY_TOPIA_ID + " = :" + ImportFile.PROPERTY_TOPIA_ID, parameters);
            topiaHibernateSupport.getHibernateSession().flush();
        }
//        super.delete(entity);
    }
}
