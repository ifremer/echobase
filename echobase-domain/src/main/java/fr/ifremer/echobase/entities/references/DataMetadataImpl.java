package fr.ifremer.echobase.entities.references;

/*
 * #%L
 * EchoBase :: Domain
 * %%
 * Copyright (C) 2011 - 2013 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.base.MoreObjects;

public class DataMetadataImpl extends DataMetadataAbstract {

    public static final String REGION_ENV_COORDINATES = "RegionEnvCoordinates";

    public static final String SURFACE = "Surface";

    public static final String GRID_CELL_LONGITUDE = "GridCellLongitude";

    public static final String GRID_CELL_LATITUDE = "GridCellLatitude";

    public static final String GRID_CELL_DEPTH = "GridCellDepth";

    public static final String GRID_LONGITUDE_LAG = "GridLongitudeLag";

    public static final String GRID_LATITUDE_LAG = "GridLatitudeLag";

    public static final String GRID_DEPTH_LAG = "GridDepthLag";

    public static final String RADIAL_NUMBER = "RadialNumber";

    private static final long serialVersionUID = 1L;

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(DataMetadata.class)
                          .add(PROPERTY_NAME, name)
//                          .add(PROPERTY_TOPIA_ID, topiaId)
                          .toString();
    }
}
