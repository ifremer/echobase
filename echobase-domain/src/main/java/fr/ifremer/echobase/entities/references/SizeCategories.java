package fr.ifremer.echobase.entities.references;

/*
 * #%L
 * EchoBase :: Domain
 * %%
 * Copyright (C) 2011 - 2015 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.base.Function;
import com.google.common.base.Predicate;

/**
 * Created on 1/21/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.9
 */
public class SizeCategories {

    public static final Predicate<SizeCategory> IS_HORS_VRAC = new Predicate<SizeCategory>() {
        @Override
        public boolean apply(SizeCategory input) {
            return input.getName().toUpperCase().equals("H");
        }
    };

    public static final Function<SizeCategory, String> SIZE_CATEGORY_NAME = new Function<SizeCategory, String>() {
        @Override
        public String apply(SizeCategory input) {
            return input.getName();
        }
    };
}
