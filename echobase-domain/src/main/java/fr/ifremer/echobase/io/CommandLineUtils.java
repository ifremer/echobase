package fr.ifremer.echobase.io;

/*
 * #%L
 * EchoBase :: Domain
 * %%
 * Copyright (C) 2011 - 2014 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import org.apache.commons.exec.CommandLine;
import org.apache.commons.exec.DefaultExecutor;
import org.apache.commons.exec.ExecuteException;
import org.apache.commons.exec.LogOutputStream;
import org.apache.commons.exec.PumpStreamHandler;
import org.apache.commons.logging.Log;

import java.io.File;
import java.io.IOException;

/**
 * Utils method around a command line execution.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.8
 */
public class CommandLineUtils {

    public static CommandLine newCommand(File exec, Object... args) {
        return newCommand(exec.getAbsolutePath(), args);
    }

    public static CommandLine newCommand(String exec, Object... args) {
        CommandLine result = new CommandLine(exec);

        if (args != null) {
            for (Object arg : args) {
                String argument;
                if (arg instanceof File) {
                    argument = ((File) arg).getAbsolutePath();
                } else {
                    argument = "\"" + arg.toString() + "\"";
                }
                result.addArgument(argument, true);
            }
        }
        return result;
    }

    public static void invokeCommandeLine(Log logger, CommandLine commandLine, File workDir) throws IOException {

        DefaultExecutor executor = new DefaultExecutor();
        ExecutorLog executorLog = new ExecutorLog(logger);
        executor.setStreamHandler(new PumpStreamHandler(executorLog));

        // set resource directory
        executor.setWorkingDirectory(workDir);

        // invoke
        if (logger.isInfoEnabled()) {
            logger.info("Command line '" + commandLine.toString() + "' will be execute");
        }

        try {
            executor.execute(commandLine);
        } catch (ExecuteException eee) {

            int exitValue = eee.getExitValue();
            String message = "Command line '" + commandLine.toString() + "' failed with exit code " + exitValue + "\nError Log:\n" + executorLog.getLog();
            throw new IOException(message, eee);

        }

    }

    protected static class ExecutorLog extends LogOutputStream {

        final Log logger;

        protected StringBuilder executorLog = new StringBuilder();

        public ExecutorLog(Log logger) {
            this.logger = logger;
        }

        @Override
        protected void processLine(String line, int level) {
            executorLog.append("\n").append(line);
            if (logger.isDebugEnabled()) {
                logger.debug(line);
            }
        }

        public String getLog() {
            return executorLog.toString();
        }

    }
}
