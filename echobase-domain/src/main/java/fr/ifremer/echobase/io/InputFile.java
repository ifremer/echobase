/*
 * #%L
 * EchoBase :: Domain
 * %%
 * Copyright (C) 2011 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ifremer.echobase.io;

import com.google.common.base.MoreObjects;

import java.io.File;
import java.io.Serializable;

/**
 * Object to receive a uploaded file.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 0.2
 */
public class InputFile implements Serializable {

    private static final long serialVersionUID = 1L;

    /** Name of the file (from the client side). */
    protected String fileName;

    /** Location of the uploaded file (from the server side). */
    protected File file;

    /** Content type of the uploaded file. */
    protected String contentType;

    /** Label of the import. */
    protected String label;

    public static InputFile newFile(String label) {
        return new InputFile(label);
    }

    protected InputFile(String label) {
        this.label = label;
    }

    public String getFileName() {
        return fileName;
    }

    public File getFile() {
        return file;
    }

    public String getLabel() {
        return label;
    }

    public String getContentType() {
        return contentType;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public void setFile(File file) {
        this.file = file;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public boolean hasFile() {
        return file != null;
    }

    @Override
    public String toString() {
        MoreObjects.ToStringHelper toStringHelper = MoreObjects.toStringHelper(this);
        if (fileName != null) {
            toStringHelper.add("fileName", fileName);
        }
        if (file != null) {
            toStringHelper.add("file", file);
        }
        if (contentType != null) {
            toStringHelper.add("contentType", contentType);
        }
        if (label != null) {
            toStringHelper.add("label", label);
        }
        return toStringHelper.toString();
    }
}
