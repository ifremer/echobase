/*
 * #%L
 * EchoBase :: Domain
 * %%
 * Copyright (C) 2011 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ifremer.echobase.io;

import com.google.common.base.Charsets;
import com.google.common.base.Preconditions;
import com.google.common.io.Files;
import fr.ifremer.echobase.EchoBaseTechnicalException;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.util.GZUtil;
import org.nuiton.util.ZipUtil;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.LineNumberReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.nio.charset.StandardCharsets;
import java.sql.Blob;
import java.sql.SQLException;
import java.util.Collection;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

/**
 * Usefull methods on io.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 0.2
 */
public class EchoBaseIOUtil {

    /** Logger. */
    private static final Log log = LogFactory.getLog(EchoBaseIOUtil.class);

    public static void compressZipFile(File zipFile, File directory) throws IOException {
        compressZipFile(zipFile, directory, true);
    }

    public static void compressZipFile(File zipFile, File directory, boolean delete) throws IOException {
        try {
            ZipUtil.compress(zipFile, directory);
        } catch (IOException eee) {
            throw new EchoBaseTechnicalException(
                    "Can not create zip file " + zipFile, eee);
        } finally {
            if (delete) {
                FileUtils.deleteDirectory(directory);
            }
        }
    }

    public static void copyFile(File destinationFile,
                                Collection<File> filesToMerge) throws IOException {
        if (!CollectionUtils.isEmpty(filesToMerge)) {

            if (filesToMerge.size() == 1) {

                // simple copy

                File source = filesToMerge.iterator().next();
                if (source.getName().endsWith(".gz")) {
                    BufferedWriter writer = new BufferedWriter(Files.newWriter(destinationFile, Charsets.UTF_8));
                    try {
                        BufferedReader reader = new BufferedReader(new InputStreamReader(new GZIPInputStream(new FileInputStream(source)), Charsets.UTF_8));
                        try {
                            IOUtils.copy(reader, writer);
                            reader.close();
                        } finally {
                            IOUtils.closeQuietly(reader);
                        }
                        writer.close();
                    } finally {
                        IOUtils.closeQuietly(writer);
                    }
                } else {
                    FileUtils.copyFile(source, destinationFile);
                }
            } else {

                // must merge all files
                mergeFiles(destinationFile, filesToMerge);
            }
        }
    }

    public static void mergeFiles(File destination,
                                  Iterable<File> fileToMerges) throws IOException {

        BufferedWriter writer = new BufferedWriter(new FileWriter(destination));
        try {
            boolean firstLine = false;
            for (File toMergeFile : fileToMerges) {
                BufferedReader reader;
                if (toMergeFile.getName().endsWith(".gz")) {
                    reader = new BufferedReader(new InputStreamReader(new GZIPInputStream(new FileInputStream(toMergeFile)), Charsets.UTF_8));
                } else {
                    reader = new BufferedReader(Files.newReader(toMergeFile, Charsets.UTF_8));
                }
                try {
                    // pass the first line (header)
                    String line = reader.readLine();
                    if (!firstLine) {

                        // add the csv header
                        writer.write(line);

                        firstLine = true;
                    }
                    while ((line = reader.readLine()) != null) {
                        writer.newLine();
                        writer.write(line);
                    }
                } finally {
                    reader.close();
                }
            }
        } finally {
            writer.close();
        }
    }

    public static int countLines(File file) {
        InputStream inputStream = null;
        try {
            inputStream = FileUtils.openInputStream(file);
            int result = countLines(inputStream);
            inputStream.close();
            return result;
        } catch (IOException e) {
            throw new EchoBaseTechnicalException("Could not countLines", e);
        } finally {
            if (inputStream != null) {
                IOUtils.closeQuietly(inputStream);
            }
        }
    }

    public static long countLines(ZipFile zipFile,
                                  Iterable<ZipEntry> entries) throws IOException {
        long result = 0l;
        for (ZipEntry entry : entries) {
            InputStream inputStream = zipFile.getInputStream(entry);
            try {
                int i = countLines(inputStream);
                if (log.isDebugEnabled()) {
                    log.debug("entry: " + entry.getName() + " - " + i);
                }
                result += i;
                inputStream.close();
            } finally {
                if (inputStream != null) {
                    IOUtils.closeQuietly(inputStream);
                }
            }
        }
        return result;
    }

    public static int countLines(InputStream file) {
        int result = 0;

        LineNumberReader reader = null;
        try {
            reader = new LineNumberReader(new InputStreamReader(file));
            while (reader.readLine() != null) {
            }
            result = reader.getLineNumber();
            reader.close();

        } catch (IOException e) {
            throw new EchoBaseTechnicalException(
                    "Could not count lines of file " + file, e);
        } finally {
            if (reader != null) {
                IOUtils.closeQuietly(reader);
            }
        }
        return result;
    }

    public static void copyFile(InputFile inputFile,
                                File dataDirectory) throws IOException {
        Preconditions.checkNotNull(inputFile);
        Preconditions.checkNotNull(dataDirectory);
        File source = inputFile.getFile();
        Preconditions.checkNotNull(source);

        File target = new File(dataDirectory, inputFile.getFileName());
        if (log.isInfoEnabled()) {
            log.info("Copy file " + source + " to " + target);
        }
        FileUtils.copyFile(source, target);

        // keep target (source file will be removed at the end of this action)
        inputFile.setFile(target);
    }

    public static String loadScript(String scriptPath) {

        // get file
        InputStream inputStream =
                EchoBaseIOUtil.class.getResourceAsStream(scriptPath);

        Preconditions.checkNotNull(
                inputStream,
                "Could not find resource in classpath " + scriptPath);

        String sql;
        try {
            sql = IOUtils.toString(inputStream, Charsets.UTF_8);
        } catch (IOException e) {
            throw new EchoBaseTechnicalException(
                    "Could not load file " + scriptPath, e);
        }
        return sql;
    }

    public static boolean isExecutableFile(File file) {
        return (file != null && file.exists() && file.canExecute() && !file.isDirectory());
    }

    public static void forceMkdir(File directory) {
        try {
            FileUtils.forceMkdir(directory);
        } catch (IOException e) {
            throw new EchoBaseTechnicalException("Could not create directory: " + directory, e);
        }
    }

    public static void copyResource(String resourcePath,
                                    File outputFile,
                                    boolean executable) throws IOException {
        InputStream inputStream = EchoBaseIOUtil.class.getResourceAsStream(resourcePath);
        Preconditions.checkNotNull(inputStream,
                                   "could not find resource " + resourcePath);

        forceMkdir(outputFile.getParentFile());

        try {
            if (log.isDebugEnabled()) {
                log.debug("Copy file from classpath " + resourcePath + " to " + outputFile);
            }
            OutputStreamWriter outputStream = new OutputStreamWriter(
                    new FileOutputStream(outputFile), Charsets.UTF_8);
            try {
                IOUtils.copy(inputStream, outputStream);
            } finally {
                outputStream.close();
            }
            if (executable) {
                outputFile.setExecutable(true);
            }
        } finally {
            inputStream.close();
        }
    }

    public static File copyAndGzipFileToDirectory(File sourceFile, File targetDirectory) throws IOException {

        try (InputStream inputStream = java.nio.file.Files.newInputStream(sourceFile.toPath())) {

            FileUtils.forceMkdir(targetDirectory);
            File targetFile = new File(targetDirectory, sourceFile.getName());

            try (OutputStream outputStream = new GZIPOutputStream(java.nio.file.Files.newOutputStream(targetFile.toPath()))) {
                IOUtils.copy(inputStream, outputStream);
            }

            return targetFile;

        }

    }

    public static long fileLength(File sourceFile) throws IOException {

        boolean gzip;

        try (InputStream inputStream = new BufferedInputStream(new FileInputStream(sourceFile))) {
            gzip = GZUtil.isGzipStream(inputStream);
        }

        long fileLength;

        if (gzip) {

            try (GZIPInputStream inputStream = new GZIPInputStream(new FileInputStream(sourceFile))) {

                byte[] bytes = IOUtils.toString(inputStream, StandardCharsets.UTF_8).getBytes();
                fileLength = new String(bytes).length();

            }

        } else {

            fileLength = sourceFile.length();

        }

        return fileLength;

    }

    public static long blobLength(Blob sourceFile) throws IOException, SQLException {

        boolean gzip;

        try (InputStream inputStream = new BufferedInputStream(sourceFile.getBinaryStream())) {
            gzip = GZUtil.isGzipStream(inputStream);
        }

        long fileLength;

        if (gzip) {

            try (GZIPInputStream inputStream = new GZIPInputStream(sourceFile.getBinaryStream())) {

                byte[] bytes = IOUtils.toString(inputStream, StandardCharsets.UTF_8).getBytes();
                fileLength = new String(bytes).length();

            }

        } else {

            fileLength = sourceFile.length();

        }

        return fileLength;

    }

    protected EchoBaseIOUtil() {
        // no instance of helper class
    }
}
