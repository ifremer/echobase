package fr.ifremer.echobase.converter;

/*
 * #%L
 * EchoBase :: Domain
 * %%
 * Copyright (C) 2011 - 2014 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import org.apache.commons.beanutils.ConversionException;
import org.apache.commons.beanutils.Converter;

import static org.nuiton.i18n.I18n.t;

/**
 * Created on 3/3/14.
 *
 * A float converter which is not dependant on user locale to obtain the locale
 * {@code dot} representation.
 *
 * It can transform {@code 0.2} and also {@code 0,2}.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.6
 */
public class FloatConverter implements Converter {

    @Override
    public <T> T convert(Class<T> aClass, Object value) {
        if (value == null) {
            throw new ConversionException(
                    t("echobase.error.convertor.noValue", this));
        }
        if (isEnabled(aClass)) {
            T result;
            if (isEnabled(value.getClass())) {
                result = (T) value;
                return result;
            }
            if (value instanceof String) {
                result = (T) valueOf((String) value);
                return result;
            }
        }
        throw new ConversionException(
                t("echobase.error.no.convertor", aClass.getName(), value));
    }

    protected Float valueOf(String value) {
        try {
            if (value.contains(",")) {
                value = value.replaceAll(",", ".");
            }
            Float result;
            result = Float.valueOf(value);
            return result;
        } catch (NumberFormatException e) {
            throw new ConversionException(
                    t("echobase.error.float.convertor", value, this, e.getMessage()));
        }
    }


    protected boolean isEnabled(Class<?> aClass) {
        return Float.class.equals(aClass);
    }

    public Class<?> getType() {
        return Float.class;
    }
}
