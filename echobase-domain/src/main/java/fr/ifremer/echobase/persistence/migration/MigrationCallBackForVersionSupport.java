package fr.ifremer.echobase.persistence.migration;

/*
 * #%L
 * EchoBase :: Domain
 * %%
 * Copyright (C) 2011 - 2016 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.base.MoreObjects;
import com.google.common.base.Preconditions;
import fr.ifremer.echobase.entities.DriverType;
import fr.ifremer.echobase.io.EchoBaseIOUtil;
import org.nuiton.topia.migration.TopiaMigrationCallbackByClassNG;

import java.util.List;

/**
 * Created on 10/04/16.
 *
 * @author Tony Chemit - chemit@codelutin.com
 */
public abstract class MigrationCallBackForVersionSupport extends TopiaMigrationCallbackByClassNG.MigrationCallBackForVersion {

    protected DriverType driverType = DriverType.H2;

    protected abstract String getMigrationPath(String script);

    public final void setDriverType(DriverType driverType) {
        Preconditions.checkNotNull(driverType);
        this.driverType = driverType;
    }

    protected final boolean isH2() {
        return driverType.equals(DriverType.H2);
    }

    protected final boolean isPostgresql() {
        return driverType.equals(DriverType.POSTGRESQL);
    }

    protected final void addScript(String script, List<String> queries) {

        String scriptPath = getMigrationPath(script);
        loadScript(scriptPath, queries);

    }

    protected final void addSpecificScript(String script, List<String> queries) {

        String scriptPath = getSpecificMigrationPath(script);
        loadScript(scriptPath, queries);

    }

    protected final void loadSpecificScript(String script, List<String> queries) {
        String scriptPath = getSpecificMigrationPath(script);
        String scriptContent = EchoBaseIOUtil.loadScript(scriptPath);
        queries.add(scriptContent);
    }

    protected void loadScript(String scriptPath, List<String> queries) {
        String scriptContent = EchoBaseIOUtil.loadScript(scriptPath);
        //FIXME Utiliser le splitter de requète dans nuiton-utils
        for (String sqlQuery : scriptContent.split("\\n")) {
            if (sqlQuery.startsWith("--")) {
                continue;
            }
            queries.add(sqlQuery);
        }
    }

    protected final String getSpecificMigrationPath(String script) {

        String path;

        if (isPostgresql()) {

            path = getMigrationPath("pg/" + script);

        } else {

            path = getMigrationPath("h2/" + script);

        }

        return path;

    }

    @Override
    public final String toString() {
        return MoreObjects.toStringHelper(this)
                          .add("driverType", driverType)
                          .add("version", getVersion())
                          .toString();
    }

}
