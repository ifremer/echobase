package fr.ifremer.echobase.persistence;
/*
 * #%L
 * EchoBase :: Domain
 * %%
 * Copyright (C) 2011 - 2012 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import fr.ifremer.echobase.entities.EchoBaseUserEntityEnum;
import org.nuiton.topia.persistence.metadata.AssociationMeta;
import org.nuiton.topia.persistence.metadata.DbMeta;
import org.nuiton.topia.persistence.metadata.TableMeta;

import java.util.Arrays;
import java.util.List;

/**
 * Metasof the working db.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.1
 */
public class EchoBaseDbMeta extends DbMeta<EchoBaseUserEntityEnum> {

    public static EchoBaseDbMeta newDbMeta() {
        return new EchoBaseDbMeta(new EchoBasePersistenceHelper());
    }

    private static final List<EchoBaseUserEntityEnum> REFERENCE_TYPES = ImmutableList.copyOf(Arrays.asList(

            // with no dep
            EchoBaseUserEntityEnum.SexCategory,
            EchoBaseUserEntityEnum.AgeCategory,
            EchoBaseUserEntityEnum.SizeCategory,
            EchoBaseUserEntityEnum.Species,
            EchoBaseUserEntityEnum.VesselType,
            EchoBaseUserEntityEnum.Mission,
            EchoBaseUserEntityEnum.DepthStratum,
            EchoBaseUserEntityEnum.EchotypeCategory,
            EchoBaseUserEntityEnum.AreaOfOperation,
            EchoBaseUserEntityEnum.ReferenceDatumType,
            EchoBaseUserEntityEnum.DataType,
            EchoBaseUserEntityEnum.DataQuality,
            EchoBaseUserEntityEnum.CellMethod,
            EchoBaseUserEntityEnum.CellType,
            EchoBaseUserEntityEnum.OperationEvent,
            EchoBaseUserEntityEnum.SampleDataType,
            EchoBaseUserEntityEnum.SampleType,
            EchoBaseUserEntityEnum.GearCharacteristic,
            EchoBaseUserEntityEnum.CategoryMeaning,
            EchoBaseUserEntityEnum.CategoryType,
            EchoBaseUserEntityEnum.MeasureType,
            EchoBaseUserEntityEnum.MeasurementMetadata,
            EchoBaseUserEntityEnum.ReferencingMethod,
            EchoBaseUserEntityEnum.Port,
            EchoBaseUserEntityEnum.AncillaryInstrumentation,

            // with deps
            EchoBaseUserEntityEnum.Strata,
            EchoBaseUserEntityEnum.AcousticInstrument,
            EchoBaseUserEntityEnum.Calibration,
            EchoBaseUserEntityEnum.Vessel,
            EchoBaseUserEntityEnum.ReferenceDatum,
            EchoBaseUserEntityEnum.OperationMetadata,
            EchoBaseUserEntityEnum.GearMetadata,
            EchoBaseUserEntityEnum.GearCharacteristicValue,
            EchoBaseUserEntityEnum.Gear,

            EchoBaseUserEntityEnum.SpeciesCategory,
            EchoBaseUserEntityEnum.TSParameters,
            EchoBaseUserEntityEnum.DataMetadata,
            EchoBaseUserEntityEnum.CategoryRef,
            EchoBaseUserEntityEnum.Impacte,
            EchoBaseUserEntityEnum.DataProtocol,
            
            EchoBaseUserEntityEnum.VocabularyCIEM));

    private static final List<EchoBaseUserEntityEnum> DATA_TYPES = ImmutableList.copyOf(Arrays.asList(
            
            EchoBaseUserEntityEnum.Voyage,
            EchoBaseUserEntityEnum.Mooring,
            EchoBaseUserEntityEnum.Transit,
            EchoBaseUserEntityEnum.Transect,
            
            EchoBaseUserEntityEnum.Operation,
            EchoBaseUserEntityEnum.OperationMetadataValue,
            EchoBaseUserEntityEnum.GearMetadataValue,

            EchoBaseUserEntityEnum.Sample,
            EchoBaseUserEntityEnum.SampleData,

            EchoBaseUserEntityEnum.DataAcquisition,
            EchoBaseUserEntityEnum.DataProcessing,

            EchoBaseUserEntityEnum.Cell,
            EchoBaseUserEntityEnum.Data,

            EchoBaseUserEntityEnum.Echotype,
            EchoBaseUserEntityEnum.Category,
            EchoBaseUserEntityEnum.Result,

            EchoBaseUserEntityEnum.LengthAgeKey,
            EchoBaseUserEntityEnum.LengthWeightKey
    ));


    private final List<TableMeta<EchoBaseUserEntityEnum>> referenceTables;

    private final List<AssociationMeta<EchoBaseUserEntityEnum>> referenceAssociations;

    private final List<TableMeta<EchoBaseUserEntityEnum>> dataTables;

    private final List<AssociationMeta<EchoBaseUserEntityEnum>> dataAssociations;

    EchoBaseDbMeta(EchoBasePersistenceHelper persistenceHelper) {
        super(persistenceHelper,
              EchoBaseUserEntityEnum.values(),
              EchoBaseUserEntityEnum.EntityModificationLog,
              EchoBaseUserEntityEnum.ImportLog);

        {
            // reference tables
            List<TableMeta<EchoBaseUserEntityEnum>> result = Lists.newArrayList();
            addTables(result, REFERENCE_TYPES);
            referenceTables = ImmutableList.copyOf(result);
        }

        {
            // reference associations
            List<AssociationMeta<EchoBaseUserEntityEnum>> result = Lists.newArrayList();
            addAssociations(result, REFERENCE_TYPES);
            referenceAssociations = ImmutableList.copyOf(result);
        }

        {
            // data tables
            List<TableMeta<EchoBaseUserEntityEnum>> result = Lists.newArrayList();
            addTables(result, DATA_TYPES);
            dataTables = ImmutableList.copyOf(result);
        }

        {
            // data associations
            List<AssociationMeta<EchoBaseUserEntityEnum>> result = Lists.newArrayList();
            addAssociations(result, DATA_TYPES);
            dataAssociations = ImmutableList.copyOf(result);
        }

        nonEditableTypes.add(EchoBaseUserEntityEnum.ImportLog);
        nonEditableTypes.add(EchoBaseUserEntityEnum.ImportFile);
        nonEditableTypes.add(EchoBaseUserEntityEnum.ImportFileId);
        
    }

    public int getEntriesSize() {
        return getAllTables().size() + getAllAssociations().size();
    }

    public List<TableMeta<EchoBaseUserEntityEnum>> getAllTables() {

        List<TableMeta<EchoBaseUserEntityEnum>> result = Lists.newArrayList();

        result.addAll(referenceTables);
        result.addAll(dataTables);
        return ImmutableList.copyOf(result);
    }

    public List<AssociationMeta<EchoBaseUserEntityEnum>> getAllAssociations() {
        List<AssociationMeta<EchoBaseUserEntityEnum>> result = Lists.newArrayList();
        result.addAll(referenceAssociations);
        result.addAll(dataAssociations);
        return ImmutableList.copyOf(result);
    }

    public int getReferenceEntriesSize() {
        return getReferenceTables().size() + getReferenceAssociations().size();
    }

    public List<TableMeta<EchoBaseUserEntityEnum>> getReferenceTables() {
        return referenceTables;
    }

    public List<AssociationMeta<EchoBaseUserEntityEnum>> getReferenceAssociations() {
        return referenceAssociations;
    }

    public List<TableMeta<EchoBaseUserEntityEnum>> getDataTables() {
        return dataTables;
    }

    public List<AssociationMeta<EchoBaseUserEntityEnum>> getDataAssociations() {
        return dataAssociations;
    }

    /**
     * @return the entity types of data in correct order for replication
     *         purpose.
     */
    public List<EchoBaseUserEntityEnum> getDataTypes() {
        return DATA_TYPES;
    }

    /**
     * @return the entity types of reference in correct order for copy
     *         purpose (says import / export of all a database.
     */
    public List<EchoBaseUserEntityEnum> getReferenceTypes() {
        return REFERENCE_TYPES;
    }

    @Override
    public EchoBasePersistenceHelper getPersistenceHelper() {
        return (EchoBasePersistenceHelper) super.getPersistenceHelper();
    }
}
