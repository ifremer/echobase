package fr.ifremer.echobase.persistence.migration.workingDb;

/*
 * #%L
 * EchoBase :: Domain
 * %%
 * Copyright (C) 2011 - 2013 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.persistence.TopiaException;
import org.nuiton.topia.persistence.support.TopiaSqlSupport;
import org.nuiton.version.Version;
import org.nuiton.version.Versions;

import java.util.List;

/**
 * Migrate workgin db to version {@code 2.2}.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.2
 */
public class WorkingDbMigrationCallBackForVersion2_2 extends WorkingDbMigrationCallBackForVersionSupport {

    /** Logger. */
    private static final Log log = LogFactory.getLog(WorkingDbMigrationCallBackForVersion2_2.class);

    @Override
    public Version getVersion() {
        return Versions.valueOf("2.2");
    }

    @Override
    protected void prepareMigrationScript(TopiaSqlSupport sqlSupport,
                                          List<String> queries,
                                          boolean showSql,
                                          boolean showProgression) throws TopiaException {

        // remove result category NotNull Constrainst (http://forge.codelutin.com/issues/3029)
        removeResultCategoryNotNullConstrainst(queries);

        // migrate import type id (http://forge.codelutin.com/issues/3028)
        migrateImportTypeId(queries);

        // compute all spatial data (http://forge.codelutin.com/issues/3037)
        updatePostgis(sqlSupport);
    }

    protected void migrateImportTypeId(List<String> queries) {
        queries.add("UPDATE ImportLog SET importType = importType + 2 WHERE importType BETWEEN 2 AND 9;");
    }

    protected void removeResultCategoryNotNullConstrainst(List<String> queries) {
        queries.add("ALTER TABLE Result ALTER COLUMN category DROP NOT NULL;");
    }

    protected void updatePostgis(TopiaSqlSupport sqlSupport) {
        boolean spatialAware = isSpatialStructureFound(sqlSupport);

        if (spatialAware) {
            try {
                // compute all spatial data
                sqlSupport.executeSql("SELECT echobase_compute_all_spatial_data();");
            } catch (Exception e) {
                if (log.isErrorEnabled()) {
                    log.error("Could not update spatial datas", e);
                }
            }
        }
    }
}
