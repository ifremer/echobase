package fr.ifremer.echobase.persistence.migration.workingDb;

import org.nuiton.topia.persistence.TopiaException;
import org.nuiton.topia.persistence.support.TopiaSqlSupport;
import org.nuiton.version.Version;
import org.nuiton.version.Versions;

import java.util.List;

/*
 * #%L
 * EchoBase :: Domain
 * %%
 * Copyright (C) 2011 - 2015 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

/**
 * Created on 12/16/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.7.2
 */
public class WorkingDbMigrationCallBackForVersion2_7_2 extends WorkingDbMigrationCallBackForVersionSupport {

    @Override
    public Version getVersion() {
        return Versions.valueOf("2.7.2");
    }

    @Override
    protected void prepareMigrationScript(TopiaSqlSupport sqlSupport,
                                          List<String> queries,
                                          boolean showSql,
                                          boolean showProgression) throws TopiaException {

        boolean spatialAware = isSpatialStructureFound(sqlSupport);

        if (spatialAware) {

            // fix reload TotalSampleView (see https://forge.codelutin.com/issues/6318)
            updatePostgis(queries);
        }

        // add FileImport entity (see https://forge.codelutin.com/issues/6367)
        addFileImportEntity(queries);

    }

    protected void updatePostgis(List<String> queries) {
        addSpecificScript("2.7.2-0-postgis-view.sql", queries);
    }

    protected void addFileImportEntity(List<String> queries) {
        addSpecificScript("2.7.2-1-add-importFile-entity.sql", queries);
    }

}