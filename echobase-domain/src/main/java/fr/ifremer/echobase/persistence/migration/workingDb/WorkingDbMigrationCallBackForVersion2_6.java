package fr.ifremer.echobase.persistence.migration.workingDb;

import org.nuiton.topia.persistence.TopiaException;
import org.nuiton.topia.persistence.support.TopiaSqlQuery;
import org.nuiton.topia.persistence.support.TopiaSqlSupport;
import org.nuiton.version.Version;
import org.nuiton.version.Versions;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/*
 * #%L
 * EchoBase :: Domain
 * %%
 * Copyright (C) 2011 - 2014 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

/**
 * Created on 2/26/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.6
 */
public class WorkingDbMigrationCallBackForVersion2_6 extends WorkingDbMigrationCallBackForVersionSupport {

    @Override
    public Version getVersion() {
        return Versions.valueOf("2.6");
    }

    @Override
    protected void prepareMigrationScript(TopiaSqlSupport sqlSupport,
                                          List<String> queries,
                                          boolean showSql,
                                          boolean showProgression) throws TopiaException {

        boolean spatialAware = isSpatialStructureFound(sqlSupport);

        if (spatialAware) {

            // add new postigs view (see https://forge.codelutin.com/issues/4194)
            updatePostgis(queries);
        }

        // update datametadata referential (see https://forge.codelutin.com/issues/4583)
        updateDataMedataReferential(sqlSupport, queries);
    }

    protected void updatePostgis(List<String> queries) {
        addSpecificScript("2.6-0-postgis-view.sql", queries);
    }

    protected void updateDataMedataReferential(TopiaSqlSupport sqlSupport, List<String> queries) {

        String dataId;
        dataId = sqlSupport.findSingleResult(new GetdataMetadataSqlQuery("meanMapcellBiomass"));
        if (dataId == null) {
            // add it
            queries.add("INSERT INTO datametadata (topiaId,topiaversion, topiacreatedate, addOffset, comment, fillValue, longName, name, scaleFactor, units, validMax, validMin) VALUES('fr.ifremer.echobase.entities.references.DataMetadata#4443610280597#0.9322615025965290', 0, '2014-02-27 17:01:48.355', 0.0, 'Mean biomass averaged in a grid map cell using a block averaging procedure', 0, 'Mean biomass in map cell', 'meanMapcellBiomass', 1.0, 'tons', 999999999, 0);");
        }

        dataId = sqlSupport.findSingleResult(new GetdataMetadataSqlQuery("stdevMapcellBiomass"));
        if (dataId == null) {
            // add it
            queries.add("INSERT INTO datametadata (topiaId,topiaversion, topiacreatedate, addOffset, comment, fillValue, longName, name, scaleFactor, units, validMax, validMin) VALUES('fr.ifremer.echobase.entities.references.DataMetadata#4443610280597#0.9322615025965291', 0, '2014-02-27 17:01:48.355', 0.0, 'Biomass standard deviation in a grid map cell produced from a block averaging procedure', 0, 'Biomass standard deviation in map cell', 'stdevMapcellBiomass', 1.0, 'tons', 999999999, 0);");
        }

        dataId = sqlSupport.findSingleResult(new GetdataMetadataSqlQuery("NsampleMapcell"));
        if (dataId == null) {
            // add it
            queries.add("INSERT INTO datametadata (topiaId,topiaversion, topiacreatedate, addOffset, comment, fillValue, longName, name, scaleFactor, units, validMax, validMin) VALUES('fr.ifremer.echobase.entities.references.DataMetadata#4443610280597#0.9322615025965292', 0, '2014-02-27 17:01:48.355', 0.0, 'No. of data samples in a grid map cell in a block averaging procedure', 0, 'No. of samples in map cell', 'NsampleMapcell', 1.0, 'tons', 999999999, 0);");
        }
    }

    private static class GetdataMetadataSqlQuery extends TopiaSqlQuery<String> {

        private final String dataName;

        private GetdataMetadataSqlQuery(String dataName) {
            this.dataName = dataName;
        }

        @Override
        public PreparedStatement prepareQuery(Connection connection) throws SQLException {
            String hql = "SELECT topiaid FROM datametadata " +
                         "WHERE name = ?";
            PreparedStatement result = connection.prepareStatement(hql);
            result.setString(1, dataName);
            return result;
        }

        @Override
        public String prepareResult(ResultSet set) throws SQLException {
            return set.getString(1);
        }
    }
}
