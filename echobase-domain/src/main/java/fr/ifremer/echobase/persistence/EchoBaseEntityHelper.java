package fr.ifremer.echobase.persistence;

import com.google.common.base.Preconditions;
import fr.ifremer.echobase.entities.DriverType;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.persistence.TopiaApplicationContext;
import org.nuiton.topia.persistence.TopiaException;
import org.nuiton.topia.persistence.internal.AbstractTopiaPersistenceContext;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/*
 * #%L
 * EchoBase :: Domain
 * %%
 * Copyright (C) 2011 - 2012 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

/**
 * helper about topia context and jdbc connections.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.1
 */
public class EchoBaseEntityHelper {

    /** Logger. */
    private static final Log log = LogFactory.getLog(EchoBaseEntityHelper.class);

    public static void releaseApplicationContext(TopiaApplicationContext context) {

        if (log.isInfoEnabled()) {
            log.info("release database " + context.getConfiguration().getJdbcConnectionUrl());
        }
        try {
            releaseContext(context);
        } catch (TopiaException e) {
            // we don't want this to throw an exception, wants to close
            // all possible opened contexts
            if (log.isErrorEnabled()) {
                log.error("Could not close context " + context, e);
            }
        }
    }

    public static void releaseContext(TopiaApplicationContext rootContext) throws TopiaException {
        if (rootContext != null && !rootContext.isClosed()) {
            rootContext.close();
        }
    }

    public static void checkJdbcConnection(JdbcConfiguration configuration) throws SQLException {

        String jdbcUrl = configuration.getUrl();
        String login = configuration.getLogin();
        String password = configuration.getPassword();

        try (Connection conn = DriverManager.getConnection(jdbcUrl, login, password)) {
            if (log.isDebugEnabled()) {
                log.debug("connexion reussie (" + conn + ") pour l'utilisateur " + login + " at  [" + jdbcUrl + ']');
            }
        }

    }

    /**
     * Create a configuration to connect to the {@code postgres} database
     * from a normal database configuration.
     *
     * This is used to create a new concrete database.
     *
     * <strong>Note: </strong>This only works for postgresql database.
     *
     * @param jdbcConfiguration jdbc configuration
     * @return the jdbcl configuration to {@code postgres} database.
     * @since 2.4
     */
    public static JdbcConfiguration newWorkingDbJdbcConfiguration(JdbcConfiguration jdbcConfiguration) {

        String url = jdbcConfiguration.getUrl();
        String dbName = StringUtils.substringAfterLast(url, "/");
        String connectUrl = StringUtils.removeEnd(url, dbName) + "postgres";

        if (log.isInfoEnabled()) {
            log.info("Use meta database conttection url: " + connectUrl);
        }
        DriverType driverType = jdbcConfiguration.getDriverType();

        Preconditions.checkArgument(
                DriverType.POSTGRESQL.equals(driverType),
                "Can't create a meta configuration for a none postgresql database.");

        return JdbcConfiguration.newConfig(driverType, connectUrl, jdbcConfiguration.getLogin(), jdbcConfiguration.getPassword());

    }

    public static void closeConnection(AbstractTopiaPersistenceContext transaction) {

        if (transaction == null) {
            if (log.isTraceEnabled()) {
                log.trace("no transaction to close");
            }
        } else if (transaction.isClosed()) {
            if (log.isTraceEnabled()) {
                log.trace("transaction " + transaction + " is already closed");
            }
        } else {
            if (log.isDebugEnabled()) {
                log.debug("closing transaction " + transaction);
            }
            transaction.close();
        }

    }

}
