package fr.ifremer.echobase.persistence.migration.internalDb;

import fr.ifremer.echobase.entities.ExportQueries;
import fr.ifremer.echobase.entities.ExportQuery;
import fr.ifremer.echobase.entities.ExportQueryImpl;
import org.nuiton.topia.persistence.TopiaException;
import org.nuiton.topia.persistence.support.TopiaSqlQuery;
import org.nuiton.topia.persistence.support.TopiaSqlSupport;
import org.nuiton.version.Version;
import org.nuiton.version.Versions;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/*
 * #%L
 * EchoBase :: Domain
 * %%
 * Copyright (C) 2011 - 2013 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

/**
 * Migrate internal db to version {@code 2.2}.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.2
 */
public class InternalDbMigrationCallBackForVersion2_2 extends InternalDbMigrationCallBackForVersionSupport {
    @Override
    public Version getVersion() {
        return Versions.valueOf("2.2");
    }

    public static final String EXPORT_QUERY_UPDATE =
            "UPDATE exportquery SET name = '%s', topiaversion = topiaversion + 1 WHERE topiaid = '%s'";

    @Override
    protected void prepareMigrationScript(TopiaSqlSupport sqlSupport,
                                          List<String> queries,
                                          boolean showSql,
                                          boolean showProgression) throws TopiaException {

        // normalize queries names (http://forge.codelutin.com/issues/2875)
        normalizeExportQueryNames(sqlSupport, queries);

        // remove login from workingdbconfiguration (http://forge.codelutin.com/issues/2245)
        removeLoginField(queries);
    }

    protected void removeLoginField(List<String> queries) {
        queries.add("ALTER TABLE WorkingDbConfiguration DROP COLUMN login");
    }

    protected void normalizeExportQueryNames(TopiaSqlSupport tx, List<String> queries) {

        TopiaSqlQuery<ExportQuery> query = new TopiaSqlQuery<ExportQuery>() {
            @Override
            public PreparedStatement prepareQuery(Connection connection) throws SQLException {
                return connection.prepareStatement("select topiaid, name from exportquery;");
            }

            @Override
            public ExportQuery prepareResult(ResultSet set) throws SQLException {
                ExportQuery result = new ExportQueryImpl();
                result.setTopiaId(set.getString(1));
                result.setName(set.getString(2));
                return result;
            }
        };

        List<ExportQuery> exportQueries = tx.findMultipleResult(query);

        for (ExportQuery exportQuery : exportQueries) {
            String queryName = exportQuery.getName();
            if (!ExportQueries.isQueryNameValid(queryName)) {

                // rename query (replace all none acceptable caracters by *_*)
                StringBuilder nameBuilder = new StringBuilder();
                boolean nameHasChanged = false;
                for (char c : queryName.toCharArray()) {
                    if (ExportQueries.isQueryNameValid(String.valueOf(c))) {
                        nameBuilder.append(c);
                    } else {
                        nameBuilder.append('_');
                        nameHasChanged = true;
                    }
                }
                if (nameHasChanged) {

                    // resave with normalized name
                    String newQueryName = nameBuilder.toString();

                    queries.add(String.format(EXPORT_QUERY_UPDATE, newQueryName, exportQuery.getTopiaId()));
                }
            }
        }
    }


}
