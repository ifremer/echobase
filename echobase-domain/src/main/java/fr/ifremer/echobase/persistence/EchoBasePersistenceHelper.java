package fr.ifremer.echobase.persistence;

import org.nuiton.topia.persistence.TopiaEntity;
import org.nuiton.topia.persistence.TopiaEntityEnumProvider;

/*
 * #%L
 * EchoBase :: Domain
 * %%
 * Copyright (C) 2011 - 2012 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.echobase.entities.EchoBaseUserEntityEnum;

/**
 * Persistence helper for working db.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.1
 */
public class EchoBasePersistenceHelper implements TopiaEntityEnumProvider<EchoBaseUserEntityEnum> {

    @Override
    public <E extends TopiaEntity> EchoBaseUserEntityEnum getEntityEnum(Class<E> type) {
        return EchoBaseUserEntityEnum.valueOf(type);
    }

    @Override
    public EchoBaseUserEntityEnum getEntityEnum(String enumName) {
        return EchoBaseUserEntityEnum.valueOf(enumName);
    }

}
