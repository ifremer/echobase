package fr.ifremer.echobase.persistence.migration;

/*
 * #%L
 * EchoBase :: Domain
 * %%
 * Copyright (C) 2011 - 2012 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.base.Preconditions;
import fr.ifremer.echobase.EchoBaseTechnicalException;
import fr.ifremer.echobase.entities.DriverType;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.migration.TopiaMigrationCallbackByClassNG;
import org.nuiton.version.Version;
import org.nuiton.version.VersionComparator;
import org.reflections.Reflections;

import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

/**
 * Migration callBack resolver using service loader to discover version to
 * apply and using also a generic migration call back.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.1
 */
public class EchoBaseMigrationCallBackResolver<C extends MigrationCallBackForVersionSupport> implements TopiaMigrationCallbackByClassNG.MigrationCallBackForVersionResolver {

    /** Logger. */
    private static final Log log = LogFactory.getLog(EchoBaseMigrationCallBackResolver.class);

    public static <C extends MigrationCallBackForVersionSupport> EchoBaseMigrationCallBackResolver<C> newResolver(Class<C> resolverType, DriverType driverType) {
        return new EchoBaseMigrationCallBackResolver<>(resolverType, driverType);
    }

    protected final Map<Version, C> versionMigrationMapping;

    protected EchoBaseMigrationCallBackResolver(Class<C> migrationType, DriverType driverType) {

        this.versionMigrationMapping = new TreeMap<>(new VersionComparator());

        //Reflections reflections = Reflections.collect();
        Reflections reflections = new Reflections("fr.ifremer.echobase");
        Set<Class<? extends C>> subTypesOf = reflections.getSubTypesOf(migrationType);
        Preconditions.checkState(!subTypesOf.isEmpty());

        for (Class<? extends C> aClass : subTypesOf) {

            try {
                C callBackForVersion = aClass.newInstance();
                callBackForVersion.setDriverType(driverType);
                if (log.isInfoEnabled()) {
                    log.info("Detects migration " + callBackForVersion);
                }
                versionMigrationMapping.put(callBackForVersion.getVersion(), callBackForVersion);
            } catch (InstantiationException | IllegalAccessException e) {
                throw new EchoBaseTechnicalException("can't instanciate migration callBack: " + aClass, e);
            }

        }

    }

    @Override
    public C getCallBack(Version version) {
        return versionMigrationMapping.get(version);
    }

    @Override
    public Set<Version> getAllVersions() {
        return versionMigrationMapping.keySet();
    }

}
