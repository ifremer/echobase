/*
 * #%L
 * EchoBase :: Domain
 * %%
 * Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ifremer.echobase.persistence;

import java.io.File;
import java.io.Serializable;

import org.nuiton.topia.persistence.TopiaApplicationContext;
import org.nuiton.topia.persistence.TopiaConfiguration;

import fr.ifremer.echobase.entities.DriverType;

public class JdbcConfiguration implements Serializable {

    public static JdbcConfiguration newConfig(TopiaApplicationContext context) {
        TopiaConfiguration config = context.getConfiguration();
        JdbcConfiguration result = new JdbcConfiguration();
        result.setDriverType(DriverType.valueOfDriverName((String) config.getJdbcDriverClass().getName()));
        result.setUrl((String) config.getJdbcConnectionUrl());
        result.setLogin((String) config.getJdbcConnectionUser());
        result.setPassword((String) config.getJdbcConnectionPassword());
        return result;
    }

    public static JdbcConfiguration newConfig(DriverType driverType,
                                              String url,
                                              String login,
                                              String password) {
        JdbcConfiguration result = new JdbcConfiguration();
        result.setDriverType(driverType);
        result.setUrl(url);
        result.setLogin(login);
        result.setPassword(password);
        return result;
    }

    public static JdbcConfiguration newEmbeddedConfig(File directory) {

        File databaseFile = new File(directory, "db");

        String databaseAbsolutePath = databaseFile.getAbsolutePath();
        String url = "jdbc:h2:file:" + databaseAbsolutePath +
                     "/echobase;LOG=0;CACHE_SIZE=65536;LOCK_MODE=0;UNDO_LOG=0";
        return newConfig(DriverType.H2, url, "sa", "sa");
    }

    private static final long serialVersionUID = 1L;

    public static final String PROPERTY_URL = "url";

    public static final String PROPERTY_LOGIN = "login";

    public static final String PROPERTY_PASSWORD = "password";

    /** Jdbc url. */
    protected String url;

    /** Jdbc login */
    protected String login;

    /** Jdbc password */
    protected String password;

    /** Jdbc driver. */
    protected DriverType driverType;

    protected JdbcConfiguration() {
        // avoid constructor (use the static factory methods)
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public DriverType getDriverType() {
        return driverType;
    }

    public void setDriverType(DriverType driverType) {
        this.driverType = driverType;
    }
}
