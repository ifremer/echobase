package fr.ifremer.echobase.persistence.migration.workingDb;

/*
 * #%L
 * EchoBase :: Domain
 * %%
 * Copyright (C) 2011 - 2012 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.echobase.persistence.migration.MigrationCallBackForVersionSupport;
import org.nuiton.topia.persistence.support.TopiaSqlSupport;

/**
 * Base migration support for a working db.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.1
 */
public abstract class WorkingDbMigrationCallBackForVersionSupport extends MigrationCallBackForVersionSupport {

    protected String getMigrationPath(String script) {
        return "/migration/workingDb/" + script;
    }

    /**
     * Since verion 3.0 with topia 3.0, application context is not available anymore in migration classes.
     *
     * @param sqlSupport topia sql support
     * @return {@code true} if spatial support in detected
     */
    protected boolean isSpatialStructureFound(TopiaSqlSupport sqlSupport) {
        boolean result;
        try {

            sqlSupport.executeSql("select count(*) from echobase_cell_spatial;");
            result = true;

        } catch (Exception e) {

            // table not found (or other, ...)
            result = false;

        }

        return result;
    }

}
