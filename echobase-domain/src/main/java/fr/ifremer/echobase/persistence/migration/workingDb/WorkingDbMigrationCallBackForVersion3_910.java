package fr.ifremer.echobase.persistence.migration.workingDb;

/*
 * #%L
 * EchoBase :: Domain
 * %%
 * Copyright (C) 2011 - 2016 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import org.nuiton.topia.persistence.TopiaException;
import org.nuiton.topia.persistence.support.TopiaSqlSupport;
import org.nuiton.version.Version;
import org.nuiton.version.Versions;

import java.util.List;

/**
 * Created on 26/04/16.
 *
 * @author Julien Ruchaud - ruchaud@codelutin.com
 * @since 4.0
 */
public class WorkingDbMigrationCallBackForVersion3_910 extends WorkingDbMigrationCallBackForVersionSupport {

    @Override
    public Version getVersion() {
        return Versions.valueOf("3.910");
    }
    
    @Override
    protected void prepareMigrationScript(TopiaSqlSupport sqlSupport,
                                          List<String> queries,
                                          boolean showSql,
                                          boolean showProgression) throws TopiaException {

        boolean spatialAware = isSpatialStructureFound(sqlSupport);
        if (spatialAware) {
            loadSpecificScript("3.910-0-update-procedure.sql", queries);
        }
    }

}
